## 2017-09-18

Package | Advisory | Notes
------- | -------- | -----
oracle-instantclient11.2-12.2-2.el7.cern | &nbsp; &nbsp; | &nbsp;
oracle-instantclient11.2-12.2-3.el7.cern | &nbsp; &nbsp; | &nbsp;
oracle-instantclient11.2-compat-12.2-2.el7.cern | &nbsp; &nbsp; | &nbsp;
oracle-instantclient11.2-compat-12.2-3.el7.cern | &nbsp; &nbsp; | &nbsp;
oracle-instantclient11.2-meta-12.2-2.el7.cern | &nbsp; &nbsp; | &nbsp;
oracle-instantclient11.2-meta-12.2-3.el7.cern | &nbsp; &nbsp; | &nbsp;
oracle-instantclient12.1-12.2-2.el7.cern | &nbsp; &nbsp; | &nbsp;
oracle-instantclient12.1-12.2-3.el7.cern | &nbsp; &nbsp; | &nbsp;
oracle-instantclient-12.2-2.el7.cern | &nbsp; &nbsp; | &nbsp;
oracle-instantclient-12.2-3.el7.cern | &nbsp; &nbsp; | &nbsp;
oracle-instantclient12.2-compat-12.2-2.el7.cern | &nbsp; &nbsp; | &nbsp;
oracle-instantclient12.2-compat-12.2-3.el7.cern | &nbsp; &nbsp; | &nbsp;
oracle-instantclient12.2-meta-12.2-2.el7.cern | &nbsp; &nbsp; | &nbsp;
oracle-instantclient12.2-meta-12.2-3.el7.cern | &nbsp; &nbsp; | &nbsp;
oracle-instantclient-basic-12.2-2.el7.cern | &nbsp; &nbsp; | &nbsp;
oracle-instantclient-basic-12.2-3.el7.cern | &nbsp; &nbsp; | &nbsp;
oracle-instantclient-basiclite-12.2-2.el7.cern | &nbsp; &nbsp; | &nbsp;
oracle-instantclient-basiclite-12.2-3.el7.cern | &nbsp; &nbsp; | &nbsp;
oracle-instantclient-devel-12.2-2.el7.cern | &nbsp; &nbsp; | &nbsp;
oracle-instantclient-devel-12.2-3.el7.cern | &nbsp; &nbsp; | &nbsp;
oracle-instantclient-jdbc-12.2-2.el7.cern | &nbsp; &nbsp; | &nbsp;
oracle-instantclient-jdbc-12.2-3.el7.cern | &nbsp; &nbsp; | &nbsp;
oracle-instantclient-odbc-12.2-2.el7.cern | &nbsp; &nbsp; | &nbsp;
oracle-instantclient-odbc-12.2-3.el7.cern | &nbsp; &nbsp; | &nbsp;
oracle-instantclient-precomp-12.2-2.el7.cern | &nbsp; &nbsp; | &nbsp;
oracle-instantclient-precomp-12.2-3.el7.cern | &nbsp; &nbsp; | &nbsp;
oracle-instantclient-sqlplus-12.2-2.el7.cern | &nbsp; &nbsp; | &nbsp;
oracle-instantclient-sqlplus-12.2-3.el7.cern | &nbsp; &nbsp; | &nbsp;
oracle-instantclient-tools-12.2-2.el7.cern | &nbsp; &nbsp; | &nbsp;
oracle-instantclient-tools-12.2-3.el7.cern | &nbsp; &nbsp; | &nbsp;
perl-DBD-Oracle-1.74-4.el7.cern | &nbsp; &nbsp; | &nbsp;
php-oci8-2.0.8-4.el7.cern | &nbsp; &nbsp; | &nbsp;
php-oracle-1.0-5.el7.cern | &nbsp; &nbsp; | &nbsp;
tora-2.1.3-5.el7.cern | &nbsp; &nbsp; | &nbsp;
