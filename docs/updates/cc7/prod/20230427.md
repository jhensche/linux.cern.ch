## 2023-04-27


Package | Advisory | Notes
------- | -------- | -----
nfs-ganesha-4.4-1.el7 | &nbsp; &nbsp; | &nbsp;
nfs-ganesha-ceph-4.4-1.el7 | &nbsp; &nbsp; | &nbsp;
nfs-ganesha-gluster-4.4-1.el7 | &nbsp; &nbsp; | &nbsp;
nfs-ganesha-gpfs-4.4-1.el7 | &nbsp; &nbsp; | &nbsp;
nfs-ganesha-mem-4.4-1.el7 | &nbsp; &nbsp; | &nbsp;
nfs-ganesha-nullfs-4.4-1.el7 | &nbsp; &nbsp; | &nbsp;
nfs-ganesha-proxy-v4-4.4-1.el7 | &nbsp; &nbsp; | &nbsp;
nfs-ganesha-rados-grace-4.4-1.el7 | &nbsp; &nbsp; | &nbsp;
nfs-ganesha-rados-urls-4.4-1.el7 | &nbsp; &nbsp; | &nbsp;
nfs-ganesha-rgw-4.4-1.el7 | &nbsp; &nbsp; | &nbsp;
nfs-ganesha-utils-4.4-1.el7 | &nbsp; &nbsp; | &nbsp;
nfs-ganesha-vfs-4.4-1.el7 | &nbsp; &nbsp; | &nbsp;
firefox-102.10.0-1.el7.centos | &nbsp; &nbsp; | &nbsp;
java-11-openjdk-11.0.19.0.7-1.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2023:1875" target="secadv">RHSA-2023:1875</a> | &nbsp;
java-11-openjdk-demo-11.0.19.0.7-1.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2023:1875" target="secadv">RHSA-2023:1875</a> | &nbsp;
java-11-openjdk-devel-11.0.19.0.7-1.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2023:1875" target="secadv">RHSA-2023:1875</a> | &nbsp;
java-11-openjdk-headless-11.0.19.0.7-1.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2023:1875" target="secadv">RHSA-2023:1875</a> | &nbsp;
java-11-openjdk-javadoc-11.0.19.0.7-1.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2023:1875" target="secadv">RHSA-2023:1875</a> | &nbsp;
java-11-openjdk-javadoc-zip-11.0.19.0.7-1.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2023:1875" target="secadv">RHSA-2023:1875</a> | &nbsp;
java-11-openjdk-jmods-11.0.19.0.7-1.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2023:1875" target="secadv">RHSA-2023:1875</a> | &nbsp;
java-11-openjdk-src-11.0.19.0.7-1.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2023:1875" target="secadv">RHSA-2023:1875</a> | &nbsp;
java-11-openjdk-static-libs-11.0.19.0.7-1.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2023:1875" target="secadv">RHSA-2023:1875</a> | &nbsp;
thunderbird-102.10.0-2.el7.centos | &nbsp; &nbsp; | &nbsp;

