## 2021-06-10


Package | Advisory | Notes
------- | -------- | -----
cern-krb5-conf-1.3-6.el7.cern | &nbsp; &nbsp; | &nbsp;
cern-krb5-conf-atlas-1.3-6.el7.cern | &nbsp; &nbsp; | &nbsp;
cern-krb5-conf-cms-1.3-6.el7.cern | &nbsp; &nbsp; | &nbsp;
cern-krb5-conf-defaults-cernch-1.3-6.el7.cern | &nbsp; &nbsp; | &nbsp;
cern-krb5-conf-defaults-ipadev-1.3-6.el7.cern | &nbsp; &nbsp; | &nbsp;
cern-krb5-conf-ipadev-1.3-6.el7.cern | &nbsp; &nbsp; | &nbsp;
cern-krb5-conf-realm-cernch-1.3-6.el7.cern | &nbsp; &nbsp; | &nbsp;
cern-krb5-conf-realm-cernch-atlas-1.3-6.el7.cern | &nbsp; &nbsp; | &nbsp;
cern-krb5-conf-realm-cernch-cms-1.3-6.el7.cern | &nbsp; &nbsp; | &nbsp;
cern-krb5-conf-realm-cernch-tn-1.3-6.el7.cern | &nbsp; &nbsp; | &nbsp;
cern-krb5-conf-realm-ipadev-1.3-6.el7.cern | &nbsp; &nbsp; | &nbsp;
cern-krb5-conf-tn-1.3-6.el7.cern | &nbsp; &nbsp; | &nbsp;
cern-linuxsupport-access-1.5-1.el7.cern | &nbsp; &nbsp; | &nbsp;
python-manila-tests-tempest-doc-1.4.0-1.el7 | &nbsp; &nbsp; | &nbsp;
python2-manila-tests-tempest-1.4.0-1.el7 | &nbsp; &nbsp; | &nbsp;
centos-release-xen-410-9-2.el7.centos | &nbsp; &nbsp; | &nbsp;
centos-release-xen-412-9-2.el7.centos | &nbsp; &nbsp; | &nbsp;
centos-release-xen-413-9-2.el7.centos | &nbsp; &nbsp; | &nbsp;
centos-release-xen-414-9-2.el7.centos | &nbsp; &nbsp; | &nbsp;
centos-release-xen-48-9-2.el7.centos | &nbsp; &nbsp; | &nbsp;
centos-release-xen-9-2.el7.centos | &nbsp; &nbsp; | &nbsp;
centos-release-xen-common-9-2.el7.centos | &nbsp; &nbsp; | &nbsp;
docker-1.13.1-206.git7d71120.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2021:2144" target="secadv">RHSA-2021:2144</a> | &nbsp;
docker-1.13.1-208.git7d71120.el7_9 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2021:2276" target="secadv">RHBA-2021:2276</a> | &nbsp;
docker-client-1.13.1-206.git7d71120.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2021:2144" target="secadv">RHSA-2021:2144</a> | &nbsp;
docker-client-1.13.1-208.git7d71120.el7_9 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2021:2276" target="secadv">RHBA-2021:2276</a> | &nbsp;
docker-common-1.13.1-206.git7d71120.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2021:2144" target="secadv">RHSA-2021:2144</a> | &nbsp;
docker-common-1.13.1-208.git7d71120.el7_9 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2021:2276" target="secadv">RHBA-2021:2276</a> | &nbsp;
docker-logrotate-1.13.1-206.git7d71120.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2021:2144" target="secadv">RHSA-2021:2144</a> | &nbsp;
docker-logrotate-1.13.1-208.git7d71120.el7_9 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2021:2276" target="secadv">RHBA-2021:2276</a> | &nbsp;
docker-lvm-plugin-1.13.1-206.git7d71120.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2021:2144" target="secadv">RHSA-2021:2144</a> | &nbsp;
docker-lvm-plugin-1.13.1-208.git7d71120.el7_9 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2021:2276" target="secadv">RHBA-2021:2276</a> | &nbsp;
docker-novolume-plugin-1.13.1-206.git7d71120.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2021:2144" target="secadv">RHSA-2021:2144</a> | &nbsp;
docker-novolume-plugin-1.13.1-208.git7d71120.el7_9 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2021:2276" target="secadv">RHBA-2021:2276</a> | &nbsp;
docker-v1.10-migrator-1.13.1-206.git7d71120.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2021:2144" target="secadv">RHSA-2021:2144</a> | &nbsp;
docker-v1.10-migrator-1.13.1-208.git7d71120.el7_9 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2021:2276" target="secadv">RHBA-2021:2276</a> | &nbsp;
runc-1.0.0-69.rc10.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2021:2145" target="secadv">RHSA-2021:2145</a> | &nbsp;
rh-mariadb103-galera-25.3.32-2.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2021:2040" target="secadv">RHSA-2021:2040</a> | &nbsp;
rh-mariadb103-mariadb-10.3.28-2.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2021:2040" target="secadv">RHSA-2021:2040</a> | &nbsp;
rh-mariadb103-mariadb-backup-10.3.28-2.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2021:2040" target="secadv">RHSA-2021:2040</a> | &nbsp;
rh-mariadb103-mariadb-backup-syspaths-10.3.28-2.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2021:2040" target="secadv">RHSA-2021:2040</a> | &nbsp;
rh-mariadb103-mariadb-common-10.3.28-2.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2021:2040" target="secadv">RHSA-2021:2040</a> | &nbsp;
rh-mariadb103-mariadb-config-10.3.28-2.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2021:2040" target="secadv">RHSA-2021:2040</a> | &nbsp;
rh-mariadb103-mariadb-config-syspaths-10.3.28-2.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2021:2040" target="secadv">RHSA-2021:2040</a> | &nbsp;
rh-mariadb103-mariadb-connect-engine-10.3.28-2.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2021:2040" target="secadv">RHSA-2021:2040</a> | &nbsp;
rh-mariadb103-mariadb-devel-10.3.28-2.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2021:2040" target="secadv">RHSA-2021:2040</a> | &nbsp;
rh-mariadb103-mariadb-errmsg-10.3.28-2.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2021:2040" target="secadv">RHSA-2021:2040</a> | &nbsp;
rh-mariadb103-mariadb-gssapi-server-10.3.28-2.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2021:2040" target="secadv">RHSA-2021:2040</a> | &nbsp;
rh-mariadb103-mariadb-oqgraph-engine-10.3.28-2.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2021:2040" target="secadv">RHSA-2021:2040</a> | &nbsp;
rh-mariadb103-mariadb-server-10.3.28-2.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2021:2040" target="secadv">RHSA-2021:2040</a> | &nbsp;
rh-mariadb103-mariadb-server-galera-10.3.28-2.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2021:2040" target="secadv">RHSA-2021:2040</a> | &nbsp;
rh-mariadb103-mariadb-server-galera-syspaths-10.3.28-2.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2021:2040" target="secadv">RHSA-2021:2040</a> | &nbsp;
rh-mariadb103-mariadb-server-syspaths-10.3.28-2.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2021:2040" target="secadv">RHSA-2021:2040</a> | &nbsp;
rh-mariadb103-mariadb-server-utils-10.3.28-2.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2021:2040" target="secadv">RHSA-2021:2040</a> | &nbsp;
rh-mariadb103-mariadb-server-utils-syspaths-10.3.28-2.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2021:2040" target="secadv">RHSA-2021:2040</a> | &nbsp;
rh-mariadb103-mariadb-syspaths-10.3.28-2.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2021:2040" target="secadv">RHSA-2021:2040</a> | &nbsp;
rh-mariadb103-mariadb-test-10.3.28-2.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2021:2040" target="secadv">RHSA-2021:2040</a> | &nbsp;

