## 2021-01-28


Package | Advisory | Notes
------- | -------- | -----
cern-krb5-conf-1.3-3.el7.cern | &nbsp; &nbsp; | &nbsp;
cern-krb5-conf-atlas-1.3-3.el7.cern | &nbsp; &nbsp; | &nbsp;
cern-krb5-conf-cms-1.3-3.el7.cern | &nbsp; &nbsp; | &nbsp;
cern-krb5-conf-defaults-cernch-1.3-3.el7.cern | &nbsp; &nbsp; | &nbsp;
cern-krb5-conf-defaults-ipadev-1.3-3.el7.cern | &nbsp; &nbsp; | &nbsp;
cern-krb5-conf-ipadev-1.3-3.el7.cern | &nbsp; &nbsp; | &nbsp;
cern-krb5-conf-realm-cernch-1.3-3.el7.cern | &nbsp; &nbsp; | &nbsp;
cern-krb5-conf-realm-cernch-atlas-1.3-3.el7.cern | &nbsp; &nbsp; | &nbsp;
cern-krb5-conf-realm-cernch-cms-1.3-3.el7.cern | &nbsp; &nbsp; | &nbsp;
cern-krb5-conf-realm-cernch-tn-1.3-3.el7.cern | &nbsp; &nbsp; | &nbsp;
cern-krb5-conf-realm-ipadev-1.3-3.el7.cern | &nbsp; &nbsp; | &nbsp;
cern-krb5-conf-tn-1.3-3.el7.cern | &nbsp; &nbsp; | &nbsp;
python2-manila-tests-tempest-1.2.0-1.el7 | &nbsp; &nbsp; | &nbsp;
python-manila-tests-tempest-doc-1.2.0-1.el7 | &nbsp; &nbsp; | &nbsp;
glusterfs-9.0-1.el7 | &nbsp; &nbsp; | &nbsp;
glusterfs-cli-9.0-1.el7 | &nbsp; &nbsp; | &nbsp;
glusterfs-client-xlators-9.0-1.el7 | &nbsp; &nbsp; | &nbsp;
glusterfs-cloudsync-plugins-9.0-1.el7 | &nbsp; &nbsp; | &nbsp;
glusterfs-events-9.0-1.el7 | &nbsp; &nbsp; | &nbsp;
glusterfs-extra-xlators-9.0-1.el7 | &nbsp; &nbsp; | &nbsp;
glusterfs-fuse-9.0-1.el7 | &nbsp; &nbsp; | &nbsp;
glusterfs-ganesha-9.0-1.el7 | &nbsp; &nbsp; | &nbsp;
glusterfs-geo-replication-9.0-1.el7 | &nbsp; &nbsp; | &nbsp;
glusterfs-resource-agents-9.0-1.el7 | &nbsp; &nbsp; | &nbsp;
glusterfs-server-9.0-1.el7 | &nbsp; &nbsp; | &nbsp;
glusterfs-thin-arbiter-9.0-1.el7 | &nbsp; &nbsp; | &nbsp;
libgfapi0-9.0-1.el7 | &nbsp; &nbsp; | &nbsp;
libgfapi-devel-9.0-1.el7 | &nbsp; &nbsp; | &nbsp;
libgfchangelog0-9.0-1.el7 | &nbsp; &nbsp; | &nbsp;
libgfchangelog-devel-9.0-1.el7 | &nbsp; &nbsp; | &nbsp;
libgfrpc0-9.0-1.el7 | &nbsp; &nbsp; | &nbsp;
libgfrpc-devel-9.0-1.el7 | &nbsp; &nbsp; | &nbsp;
libgfxdr0-9.0-1.el7 | &nbsp; &nbsp; | &nbsp;
libgfxdr-devel-9.0-1.el7 | &nbsp; &nbsp; | &nbsp;
libglusterd0-9.0-1.el7 | &nbsp; &nbsp; | &nbsp;
libglusterfs0-9.0-1.el7 | &nbsp; &nbsp; | &nbsp;
libglusterfs-devel-9.0-1.el7 | &nbsp; &nbsp; | &nbsp;
python2-gluster-9.0-1.el7 | &nbsp; &nbsp; | &nbsp;
389-ds-base-1.3.10.2-9.el7_9 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2021:0077" target="secadv">RHBA-2021:0077</a> | &nbsp;
389-ds-base-devel-1.3.10.2-9.el7_9 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2021:0077" target="secadv">RHBA-2021:0077</a> | &nbsp;
389-ds-base-libs-1.3.10.2-9.el7_9 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2021:0077" target="secadv">RHBA-2021:0077</a> | &nbsp;
389-ds-base-snmp-1.3.10.2-9.el7_9 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2021:0077" target="secadv">RHBA-2021:0077</a> | &nbsp;
dnsmasq-2.76-16.el7_9.1 | &nbsp; &nbsp; | &nbsp;
dnsmasq-utils-2.76-16.el7_9.1 | &nbsp; &nbsp; | &nbsp;
java-11-openjdk-11.0.10.0.9-0.el7_9 | &nbsp; &nbsp; | &nbsp;
java-11-openjdk-demo-11.0.10.0.9-0.el7_9 | &nbsp; &nbsp; | &nbsp;
java-11-openjdk-devel-11.0.10.0.9-0.el7_9 | &nbsp; &nbsp; | &nbsp;
java-11-openjdk-headless-11.0.10.0.9-0.el7_9 | &nbsp; &nbsp; | &nbsp;
java-11-openjdk-javadoc-11.0.10.0.9-0.el7_9 | &nbsp; &nbsp; | &nbsp;
java-11-openjdk-javadoc-zip-11.0.10.0.9-0.el7_9 | &nbsp; &nbsp; | &nbsp;
java-11-openjdk-jmods-11.0.10.0.9-0.el7_9 | &nbsp; &nbsp; | &nbsp;
java-11-openjdk-src-11.0.10.0.9-0.el7_9 | &nbsp; &nbsp; | &nbsp;
java-11-openjdk-static-libs-11.0.10.0.9-0.el7_9 | &nbsp; &nbsp; | &nbsp;
java-1.8.0-openjdk-1.8.0.282.b08-1.el7_9 | &nbsp; &nbsp; | &nbsp;
java-1.8.0-openjdk-accessibility-1.8.0.282.b08-1.el7_9 | &nbsp; &nbsp; | &nbsp;
java-1.8.0-openjdk-demo-1.8.0.282.b08-1.el7_9 | &nbsp; &nbsp; | &nbsp;
java-1.8.0-openjdk-devel-1.8.0.282.b08-1.el7_9 | &nbsp; &nbsp; | &nbsp;
java-1.8.0-openjdk-headless-1.8.0.282.b08-1.el7_9 | &nbsp; &nbsp; | &nbsp;
java-1.8.0-openjdk-javadoc-1.8.0.282.b08-1.el7_9 | &nbsp; &nbsp; | &nbsp;
java-1.8.0-openjdk-javadoc-zip-1.8.0.282.b08-1.el7_9 | &nbsp; &nbsp; | &nbsp;
java-1.8.0-openjdk-src-1.8.0.282.b08-1.el7_9 | &nbsp; &nbsp; | &nbsp;
net-snmp-5.7.2-49.el7_9.1 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2020:5350" target="secadv">RHSA-2020:5350</a> | &nbsp;
net-snmp-agent-libs-5.7.2-49.el7_9.1 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2020:5350" target="secadv">RHSA-2020:5350</a> | &nbsp;
net-snmp-devel-5.7.2-49.el7_9.1 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2020:5350" target="secadv">RHSA-2020:5350</a> | &nbsp;
net-snmp-gui-5.7.2-49.el7_9.1 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2020:5350" target="secadv">RHSA-2020:5350</a> | &nbsp;
net-snmp-libs-5.7.2-49.el7_9.1 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2020:5350" target="secadv">RHSA-2020:5350</a> | &nbsp;
net-snmp-perl-5.7.2-49.el7_9.1 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2020:5350" target="secadv">RHSA-2020:5350</a> | &nbsp;
net-snmp-python-5.7.2-49.el7_9.1 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2020:5350" target="secadv">RHSA-2020:5350</a> | &nbsp;
net-snmp-sysvinit-5.7.2-49.el7_9.1 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2020:5350" target="secadv">RHSA-2020:5350</a> | &nbsp;
net-snmp-utils-5.7.2-49.el7_9.1 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2020:5350" target="secadv">RHSA-2020:5350</a> | &nbsp;
sudo-1.8.23-10.el7_9.1 | &nbsp; &nbsp; | &nbsp;
sudo-devel-1.8.23-10.el7_9.1 | &nbsp; &nbsp; | &nbsp;
tzdata-2020f-1.el7 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2021:0013" target="secadv">RHBA-2021:0013</a> | &nbsp;
tzdata-2021a-1.el7 | &nbsp; &nbsp; | &nbsp;
tzdata-java-2020f-1.el7 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2021:0013" target="secadv">RHBA-2021:0013</a> | &nbsp;
tzdata-java-2021a-1.el7 | &nbsp; &nbsp; | &nbsp;
xstream-1.3.1-12.el7_9 | &nbsp; &nbsp; | &nbsp;
xstream-javadoc-1.3.1-12.el7_9 | &nbsp; &nbsp; | &nbsp;

