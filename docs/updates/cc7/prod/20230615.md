## 2023-06-15


Package | Advisory | Notes
------- | -------- | -----
CERN-CA-certs-20230604-1.el7.cern | &nbsp; &nbsp; | &nbsp;
cern-get-keytab-1.5.3-1.el7.cern | &nbsp; &nbsp; | &nbsp;
kernel-rt-3.10.0-1160.92.1.rt56.1237.el7 | &nbsp; &nbsp; | &nbsp;
kernel-rt-debug-3.10.0-1160.92.1.rt56.1237.el7 | &nbsp; &nbsp; | &nbsp;
kernel-rt-debug-devel-3.10.0-1160.92.1.rt56.1237.el7 | &nbsp; &nbsp; | &nbsp;
kernel-rt-debug-kvm-3.10.0-1160.92.1.rt56.1237.el7 | &nbsp; &nbsp; | &nbsp;
kernel-rt-devel-3.10.0-1160.92.1.rt56.1237.el7 | &nbsp; &nbsp; | &nbsp;
kernel-rt-doc-3.10.0-1160.92.1.rt56.1237.el7 | &nbsp; &nbsp; | &nbsp;
kernel-rt-kvm-3.10.0-1160.92.1.rt56.1237.el7 | &nbsp; &nbsp; | &nbsp;
kernel-rt-trace-3.10.0-1160.92.1.rt56.1237.el7 | &nbsp; &nbsp; | &nbsp;
kernel-rt-trace-devel-3.10.0-1160.92.1.rt56.1237.el7 | &nbsp; &nbsp; | &nbsp;
kernel-rt-trace-kvm-3.10.0-1160.92.1.rt56.1237.el7 | &nbsp; &nbsp; | &nbsp;

