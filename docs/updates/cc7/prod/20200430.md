## 2020-04-30


Package | Advisory | Notes
------- | -------- | -----
FastX3-3.0.47-683.rhel7 | &nbsp; &nbsp; | &nbsp;
python27-python-debug-2.7.13-3.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2017:1162" target="secadv">RHSA-2017:1162</a> | &nbsp;
python27-python-debug-2.7.13-5.el7 | <div class="adv_e">[E]</div> <a href="https://access.redhat.com/errata/RHEA-2018:1952" target="secadv">RHEA-2018:1952</a> | &nbsp;
python27-python-debug-2.7.16-4.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2019:1260" target="secadv">RHSA-2019:1260</a> | &nbsp;
python27-python-debug-2.7.16-6.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2019:1700" target="secadv">RHSA-2019:1700</a> | &nbsp;
python27-python-debug-2.7.17-2.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2019:3948" target="secadv">RHSA-2019:3948</a> | &nbsp;
python27-python-debug-2.7.8-14.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2016:1166" target="secadv">RHSA-2016:1166</a> | &nbsp;
python27-python-debug-2.7.8-16.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2016:1628" target="secadv">RHSA-2016:1628</a> | &nbsp;
python27-python-debug-2.7.8-3.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2015:1064" target="secadv">RHSA-2015:1064</a> | &nbsp;
rh-nodejs6-nodejs-debug-2.2.0-4.el7 | <div class="adv_e">[E]</div> <a href="https://access.redhat.com/errata/RHEA-2017:1145" target="secadv">RHEA-2017:1145</a> | &nbsp;
rh-nodejs6-nodejs-debug-2.2.0-5.el7 | <div class="adv_e">[E]</div> <a href="https://access.redhat.com/errata/RHEA-2018:2105" target="secadv">RHEA-2018:2105</a> | &nbsp;
rh-nodejs6-nodejs-debuglog-1.0.1-6.el7 | <div class="adv_e">[E]</div> <a href="https://access.redhat.com/errata/RHEA-2017:1145" target="secadv">RHEA-2017:1145</a> | &nbsp;
rh-nodejs6-nodejs-debuglog-1.0.1-7.el7 | <div class="adv_e">[E]</div> <a href="https://access.redhat.com/errata/RHEA-2018:2105" target="secadv">RHEA-2018:2105</a> | &nbsp;
rh-nodejs8-nodejs-debug-2.2.0-5.el7 | <div class="adv_e">[E]</div> <a href="https://access.redhat.com/errata/RHEA-2017:3029" target="secadv">RHEA-2017:3029</a> | &nbsp;
rh-perl524-perl-B-Debug-1.23-367.el7 | <div class="adv_e">[E]</div> <a href="https://access.redhat.com/errata/RHEA-2016:2729" target="secadv">RHEA-2016:2729</a> | &nbsp;
rh-perl524-perl-B-Debug-1.23-379.el7 | &nbsp; &nbsp; | &nbsp;
rh-perl526-perl-B-Debug-1.26-2.el7 | <div class="adv_e">[E]</div> <a href="https://access.redhat.com/errata/RHEA-2018:1303" target="secadv">RHEA-2018:1303</a> | &nbsp;
rh-php73-php-pecl-xdebug-2.7.2-1.el7 | <div class="adv_e">[E]</div> <a href="https://access.redhat.com/errata/RHEA-2019:4123" target="secadv">RHEA-2019:4123</a> | &nbsp;
rh-python35-python-debug-3.5.1-11.el7 | <div class="adv_e">[E]</div> <a href="https://access.redhat.com/errata/RHEA-2016:2720" target="secadv">RHEA-2016:2720</a> | &nbsp;
rh-python35-python-debug-3.5.1-13.el7 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2019:1272" target="secadv">RHBA-2019:1272</a> | &nbsp;
rh-python35-python-debug-3.5.1-6.el7 | <div class="adv_e">[E]</div> <a href="https://access.redhat.com/errata/RHEA-2016:1163" target="secadv">RHEA-2016:1163</a> | &nbsp;
rh-python36-python-debug-3.6.2-3.el7 | &nbsp; &nbsp; | &nbsp;
rh-python36-python-debug-3.6.3-1.el7 | <div class="adv_e">[E]</div> <a href="https://access.redhat.com/errata/RHEA-2017:3021" target="secadv">RHEA-2017:3021</a> | &nbsp;
rh-python36-python-debug-3.6.3-3.el7 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2018:0096" target="secadv">RHBA-2018:0096</a> | &nbsp;
rh-python36-python-debug-3.6.3-7.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2019:0765" target="secadv">RHSA-2019:0765</a> | &nbsp;
rh-python36-python-debug-3.6.9-2.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2019:3725" target="secadv">RHSA-2019:3725</a> | &nbsp;
rh-ror42-rubygem-debug_inspector-0.0.2-6.el7 | <div class="adv_e">[E]</div> <a href="https://access.redhat.com/errata/RHEA-2016:1175" target="secadv">RHEA-2016:1175</a> | &nbsp;
rh-ror42-rubygem-debug_inspector-doc-0.0.2-6.el7 | <div class="adv_e">[E]</div> <a href="https://access.redhat.com/errata/RHEA-2016:1175" target="secadv">RHEA-2016:1175</a> | &nbsp;
rh-ror50-rubygem-debug_inspector-0.0.2-7.el7 | <div class="adv_e">[E]</div> <a href="https://access.redhat.com/errata/RHEA-2017:1144" target="secadv">RHEA-2017:1144</a> | &nbsp;
rh-ror50-rubygem-debug_inspector-doc-0.0.2-7.el7 | <div class="adv_e">[E]</div> <a href="https://access.redhat.com/errata/RHEA-2017:1144" target="secadv">RHEA-2017:1144</a> | &nbsp;
rust-toolset-7-rust-debugger-common-1.20.0-2.el7 | &nbsp; &nbsp; | &nbsp;
rust-toolset-7-rust-debugger-common-1.26.2-3.el7 | &nbsp; &nbsp; | &nbsp;

