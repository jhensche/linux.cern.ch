## 2020-05-14


Package | Advisory | Notes
------- | -------- | -----
kernel-rt-3.10.0-1127.8.2.rt56.1103.el7 | &nbsp; &nbsp; | &nbsp;
kernel-rt-debug-3.10.0-1127.8.2.rt56.1103.el7 | &nbsp; &nbsp; | &nbsp;
kernel-rt-debug-devel-3.10.0-1127.8.2.rt56.1103.el7 | &nbsp; &nbsp; | &nbsp;
kernel-rt-debug-kvm-3.10.0-1127.8.2.rt56.1103.el7 | &nbsp; &nbsp; | &nbsp;
kernel-rt-devel-3.10.0-1127.8.2.rt56.1103.el7 | &nbsp; &nbsp; | &nbsp;
kernel-rt-doc-3.10.0-1127.8.2.rt56.1103.el7 | &nbsp; &nbsp; | &nbsp;
kernel-rt-kvm-3.10.0-1127.8.2.rt56.1103.el7 | &nbsp; &nbsp; | &nbsp;
kernel-rt-trace-3.10.0-1127.8.2.rt56.1103.el7 | &nbsp; &nbsp; | &nbsp;
kernel-rt-trace-devel-3.10.0-1127.8.2.rt56.1103.el7 | &nbsp; &nbsp; | &nbsp;
kernel-rt-trace-kvm-3.10.0-1127.8.2.rt56.1103.el7 | &nbsp; &nbsp; | &nbsp;
firefox-68.8.0-1.el7.centos | &nbsp; &nbsp; | &nbsp;
squid-3.5.20-15.el7_8.1 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2020:2040" target="secadv">RHSA-2020:2040</a> | &nbsp;
squid-migration-script-3.5.20-15.el7_8.1 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2020:2040" target="secadv">RHSA-2020:2040</a> | &nbsp;
squid-sysvinit-3.5.20-15.el7_8.1 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2020:2040" target="secadv">RHSA-2020:2040</a> | &nbsp;

