## 2016-09-01

Package | Advisory | Notes
------- | -------- | -----
sclo-ror42-rubygem-actionpack-4.2.5.1-2.el7 | &nbsp; &nbsp; | &nbsp;
sclo-ror42-rubygem-actionpack-doc-4.2.5.1-2.el7 | &nbsp; &nbsp; | &nbsp;
sclo-ror42-rubygem-actionview-4.2.5.1-3.el7 | &nbsp; &nbsp; | &nbsp;
sclo-ror42-rubygem-actionview-doc-4.2.5.1-3.el7 | &nbsp; &nbsp; | &nbsp;
sclo-ror42-rubygem-activerecord-4.2.5.1-3.el7 | &nbsp; &nbsp; | &nbsp;
sclo-ror42-rubygem-activerecord-doc-4.2.5.1-3.el7 | &nbsp; &nbsp; | &nbsp;
sclo-vagrant1-vagrant-1.8.1-7.el7 | &nbsp; &nbsp; | &nbsp;
sclo-vagrant1-vagrant-doc-1.8.1-7.el7 | &nbsp; &nbsp; | &nbsp;
java-1.6.0-openjdk-1.6.0.40-1.13.12.5.el7_2 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2016:1776" target="secadv">RHSA-2016:1776</a> | &nbsp;
java-1.6.0-openjdk-demo-1.6.0.40-1.13.12.5.el7_2 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2016:1776" target="secadv">RHSA-2016:1776</a> | &nbsp;
java-1.6.0-openjdk-devel-1.6.0.40-1.13.12.5.el7_2 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2016:1776" target="secadv">RHSA-2016:1776</a> | &nbsp;
java-1.6.0-openjdk-javadoc-1.6.0.40-1.13.12.5.el7_2 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2016:1776" target="secadv">RHSA-2016:1776</a> | &nbsp;
java-1.6.0-openjdk-src-1.6.0.40-1.13.12.5.el7_2 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2016:1776" target="secadv">RHSA-2016:1776</a> | &nbsp;
pcs-0.9.143-15.el7.centos | &nbsp; &nbsp; | &nbsp;
libcacard-devel-ev-2.3.0-31.el7_2.21.1 | &nbsp; &nbsp; | &nbsp;
libcacard-ev-2.3.0-31.el7_2.21.1 | &nbsp; &nbsp; | &nbsp;
libcacard-tools-ev-2.3.0-31.el7_2.21.1 | &nbsp; &nbsp; | &nbsp;
qemu-img-ev-2.3.0-31.el7_2.21.1 | &nbsp; &nbsp; | &nbsp;
qemu-kvm-common-ev-2.3.0-31.el7_2.21.1 | &nbsp; &nbsp; | &nbsp;
qemu-kvm-ev-2.3.0-31.el7_2.21.1 | &nbsp; &nbsp; | &nbsp;
qemu-kvm-tools-ev-2.3.0-31.el7_2.21.1 | &nbsp; &nbsp; | &nbsp;
vhostmd-0.5-11.el7 | &nbsp; &nbsp; | &nbsp;
vm-dump-metrics-0.5-11.el7 | &nbsp; &nbsp; | &nbsp;
vm-dump-metrics-devel-0.5-11.el7 | &nbsp; &nbsp; | &nbsp;
xen-4.6.3-1.el7 | &nbsp; &nbsp; | &nbsp;
xen-devel-4.6.3-1.el7 | &nbsp; &nbsp; | &nbsp;
xen-doc-4.6.3-1.el7 | &nbsp; &nbsp; | &nbsp;
xen-hypervisor-4.6.3-1.el7 | &nbsp; &nbsp; | &nbsp;
xen-libs-4.6.3-1.el7 | &nbsp; &nbsp; | &nbsp;
xen-licenses-4.6.3-1.el7 | &nbsp; &nbsp; | &nbsp;
xen-ocaml-4.6.3-1.el7 | &nbsp; &nbsp; | &nbsp;
xen-ocaml-devel-4.6.3-1.el7 | &nbsp; &nbsp; | &nbsp;
xen-runtime-4.6.3-1.el7 | &nbsp; &nbsp; | &nbsp;
