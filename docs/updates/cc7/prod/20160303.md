## 2016-03-03

Package | Advisory | Notes
------- | -------- | -----
aims2-client-2.13.1-1.el7.cern | &nbsp; &nbsp; | &nbsp;
aims2-client-2.13-1.el7.cern | &nbsp; &nbsp; | &nbsp;
aims2-server-2.13.1-1.el7.cern | &nbsp; &nbsp; | &nbsp;
aims2-server-2.13-1.el7.cern | &nbsp; &nbsp; | &nbsp;
cern-linuxsupport-access-0.6-4.el7.cern | &nbsp; &nbsp; | &nbsp;
cern-linuxsupport-access-0.6-5.el7.cern | &nbsp; &nbsp; | &nbsp;
cern-wrappers-1-15.el7.cern | &nbsp; &nbsp; | &nbsp;
cern-wrappers-passwd-1-15.el7.cern | &nbsp; &nbsp; | &nbsp;
java-1.8.0-oracle-1.8.0.74-2.el7.cern | &nbsp; &nbsp; | &nbsp;
java-1.8.0-oracle-1.8.0.74-3.el7.cern | &nbsp; &nbsp; | &nbsp;
java-1.8.0-oracle-devel-1.8.0.74-2.el7.cern | &nbsp; &nbsp; | &nbsp;
java-1.8.0-oracle-devel-1.8.0.74-3.el7.cern | &nbsp; &nbsp; | &nbsp;
java-1.8.0-oracle-headless-1.8.0.74-2.el7.cern | &nbsp; &nbsp; | &nbsp;
java-1.8.0-oracle-headless-1.8.0.74-3.el7.cern | &nbsp; &nbsp; | &nbsp;
java-1.8.0-oracle-javafx-1.8.0.74-2.el7.cern | &nbsp; &nbsp; | &nbsp;
java-1.8.0-oracle-javafx-1.8.0.74-3.el7.cern | &nbsp; &nbsp; | &nbsp;
java-1.8.0-oracle-plugin-1.8.0.74-2.el7.cern | &nbsp; &nbsp; | &nbsp;
java-1.8.0-oracle-plugin-1.8.0.74-3.el7.cern | &nbsp; &nbsp; | &nbsp;
java-1.8.0-oracle-src-1.8.0.74-2.el7.cern | &nbsp; &nbsp; | &nbsp;
java-1.8.0-oracle-src-1.8.0.74-3.el7.cern | &nbsp; &nbsp; | &nbsp;
ncm-krb5clt-2.1.8.1-1.el7.cern | &nbsp; &nbsp; | &nbsp;
yum-autoupdate-4.4.4-1.el7.cern | &nbsp; &nbsp; | &nbsp;
yum-autoupdate-4.4.5-1.el7.cern | &nbsp; &nbsp; | &nbsp;
yum-autoupdate-4.4.5-2.el7.cern | &nbsp; &nbsp; | &nbsp;
yum-firstboot-4.4.4-1.el7.cern | &nbsp; &nbsp; | &nbsp;
yum-firstboot-4.4.5-1.el7.cern | &nbsp; &nbsp; | &nbsp;
yum-firstboot-4.4.5-2.el7.cern | &nbsp; &nbsp; | &nbsp;
centos-release-ceph-hammer-1.0-5.el7.centos | &nbsp; &nbsp; | &nbsp;
openssl-1.0.1e-51.el7_2.4 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2016:0301" target="secadv">RHSA-2016:0301</a> | &nbsp;
openssl-devel-1.0.1e-51.el7_2.4 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2016:0301" target="secadv">RHSA-2016:0301</a> | &nbsp;
openssl-libs-1.0.1e-51.el7_2.4 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2016:0301" target="secadv">RHSA-2016:0301</a> | &nbsp;
openssl-perl-1.0.1e-51.el7_2.4 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2016:0301" target="secadv">RHSA-2016:0301</a> | &nbsp;
openssl-static-1.0.1e-51.el7_2.4 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2016:0301" target="secadv">RHSA-2016:0301</a> | &nbsp;
