## 2023-11-23


Package | Advisory | Notes
------- | -------- | -----
java-1.8.0-openjdk-1.8.0.392.b08-2.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2023:5761" target="secadv">RHSA-2023:5761</a> | &nbsp;
java-1.8.0-openjdk-accessibility-1.8.0.392.b08-2.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2023:5761" target="secadv">RHSA-2023:5761</a> | &nbsp;
java-1.8.0-openjdk-demo-1.8.0.392.b08-2.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2023:5761" target="secadv">RHSA-2023:5761</a> | &nbsp;
java-1.8.0-openjdk-devel-1.8.0.392.b08-2.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2023:5761" target="secadv">RHSA-2023:5761</a> | &nbsp;
java-1.8.0-openjdk-headless-1.8.0.392.b08-2.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2023:5761" target="secadv">RHSA-2023:5761</a> | &nbsp;
java-1.8.0-openjdk-javadoc-1.8.0.392.b08-2.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2023:5761" target="secadv">RHSA-2023:5761</a> | &nbsp;
java-1.8.0-openjdk-javadoc-zip-1.8.0.392.b08-2.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2023:5761" target="secadv">RHSA-2023:5761</a> | &nbsp;
java-1.8.0-openjdk-src-1.8.0.392.b08-2.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2023:5761" target="secadv">RHSA-2023:5761</a> | &nbsp;
java-11-openjdk-11.0.21.0.9-1.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2023:5736" target="secadv">RHSA-2023:5736</a> | &nbsp;
java-11-openjdk-demo-11.0.21.0.9-1.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2023:5736" target="secadv">RHSA-2023:5736</a> | &nbsp;
java-11-openjdk-devel-11.0.21.0.9-1.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2023:5736" target="secadv">RHSA-2023:5736</a> | &nbsp;
java-11-openjdk-headless-11.0.21.0.9-1.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2023:5736" target="secadv">RHSA-2023:5736</a> | &nbsp;
java-11-openjdk-javadoc-11.0.21.0.9-1.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2023:5736" target="secadv">RHSA-2023:5736</a> | &nbsp;
java-11-openjdk-javadoc-zip-11.0.21.0.9-1.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2023:5736" target="secadv">RHSA-2023:5736</a> | &nbsp;
java-11-openjdk-jmods-11.0.21.0.9-1.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2023:5736" target="secadv">RHSA-2023:5736</a> | &nbsp;
java-11-openjdk-src-11.0.21.0.9-1.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2023:5736" target="secadv">RHSA-2023:5736</a> | &nbsp;
java-11-openjdk-static-libs-11.0.21.0.9-1.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2023:5736" target="secadv">RHSA-2023:5736</a> | &nbsp;
python-2.7.5-94.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2023:6885" target="secadv">RHSA-2023:6885</a> | &nbsp;
python-debug-2.7.5-94.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2023:6885" target="secadv">RHSA-2023:6885</a> | &nbsp;
python-devel-2.7.5-94.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2023:6885" target="secadv">RHSA-2023:6885</a> | &nbsp;
python-libs-2.7.5-94.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2023:6885" target="secadv">RHSA-2023:6885</a> | &nbsp;
python-test-2.7.5-94.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2023:6885" target="secadv">RHSA-2023:6885</a> | &nbsp;
python-tools-2.7.5-94.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2023:6885" target="secadv">RHSA-2023:6885</a> | &nbsp;
python3-3.6.8-21.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2023:6823" target="secadv">RHSA-2023:6823</a> | &nbsp;
python3-debug-3.6.8-21.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2023:6823" target="secadv">RHSA-2023:6823</a> | &nbsp;
python3-devel-3.6.8-21.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2023:6823" target="secadv">RHSA-2023:6823</a> | &nbsp;
python3-idle-3.6.8-21.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2023:6823" target="secadv">RHSA-2023:6823</a> | &nbsp;
python3-libs-3.6.8-21.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2023:6823" target="secadv">RHSA-2023:6823</a> | &nbsp;
python3-test-3.6.8-21.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2023:6823" target="secadv">RHSA-2023:6823</a> | &nbsp;
python3-tkinter-3.6.8-21.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2023:6823" target="secadv">RHSA-2023:6823</a> | &nbsp;
squid-3.5.20-17.el7_9.9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2023:6805" target="secadv">RHSA-2023:6805</a> | &nbsp;
squid-migration-script-3.5.20-17.el7_9.9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2023:6805" target="secadv">RHSA-2023:6805</a> | &nbsp;
squid-sysvinit-3.5.20-17.el7_9.9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2023:6805" target="secadv">RHSA-2023:6805</a> | &nbsp;
tkinter-2.7.5-94.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2023:6885" target="secadv">RHSA-2023:6885</a> | &nbsp;
xorg-x11-server-Xdmx-1.20.4-24.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2023:6802" target="secadv">RHSA-2023:6802</a> | &nbsp;
xorg-x11-server-Xephyr-1.20.4-24.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2023:6802" target="secadv">RHSA-2023:6802</a> | &nbsp;
xorg-x11-server-Xnest-1.20.4-24.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2023:6802" target="secadv">RHSA-2023:6802</a> | &nbsp;
xorg-x11-server-Xorg-1.20.4-24.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2023:6802" target="secadv">RHSA-2023:6802</a> | &nbsp;
xorg-x11-server-Xvfb-1.20.4-24.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2023:6802" target="secadv">RHSA-2023:6802</a> | &nbsp;
xorg-x11-server-Xwayland-1.20.4-24.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2023:6802" target="secadv">RHSA-2023:6802</a> | &nbsp;
xorg-x11-server-common-1.20.4-24.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2023:6802" target="secadv">RHSA-2023:6802</a> | &nbsp;
xorg-x11-server-devel-1.20.4-24.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2023:6802" target="secadv">RHSA-2023:6802</a> | &nbsp;
xorg-x11-server-source-1.20.4-24.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2023:6802" target="secadv">RHSA-2023:6802</a> | &nbsp;

