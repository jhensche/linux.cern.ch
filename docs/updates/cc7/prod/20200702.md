## 2020-07-02


Package | Advisory | Notes
------- | -------- | -----
bpftool-3.10.0-1127.13.1.el7.centos.plus | &nbsp; &nbsp; | &nbsp;
kernel-plus-3.10.0-1127.13.1.el7.centos.plus | &nbsp; &nbsp; | &nbsp;
kernel-plus-abi-whitelists-3.10.0-1127.13.1.el7.centos.plus | &nbsp; &nbsp; | &nbsp;
kernel-plus-devel-3.10.0-1127.13.1.el7.centos.plus | &nbsp; &nbsp; | &nbsp;
kernel-plus-doc-3.10.0-1127.13.1.el7.centos.plus | &nbsp; &nbsp; | &nbsp;
kernel-plus-headers-3.10.0-1127.13.1.el7.centos.plus | &nbsp; &nbsp; | &nbsp;
kernel-plus-tools-3.10.0-1127.13.1.el7.centos.plus | &nbsp; &nbsp; | &nbsp;
kernel-plus-tools-libs-3.10.0-1127.13.1.el7.centos.plus | &nbsp; &nbsp; | &nbsp;
kernel-plus-tools-libs-devel-3.10.0-1127.13.1.el7.centos.plus | &nbsp; &nbsp; | &nbsp;
kmod-openafs-1.6.22.3-1.3.10.0_1127.13.1.el7.centos.plus | &nbsp; &nbsp; | &nbsp;
perf-3.10.0-1127.13.1.el7.centos.plus | &nbsp; &nbsp; | &nbsp;
python-perf-3.10.0-1127.13.1.el7.centos.plus | &nbsp; &nbsp; | &nbsp;
cloud-init-18.5-6.el7.centos.5.0.1 | &nbsp; &nbsp; | &nbsp;
libgudev1-219-73.el7_8.8 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2020:2659" target="secadv">RHBA-2020:2659</a> | &nbsp;
libgudev1-devel-219-73.el7_8.8 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2020:2659" target="secadv">RHBA-2020:2659</a> | &nbsp;
microcode_ctl-2.1-61.10.el7_8 | <div class="adv_e">[E]</div> <a href="https://access.redhat.com/errata/RHEA-2020:2743" target="secadv">RHEA-2020:2743</a> | &nbsp;
selinux-policy-3.13.1-266.el7_8.1 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2020:2662" target="secadv">RHBA-2020:2662</a> | &nbsp;
selinux-policy-devel-3.13.1-266.el7_8.1 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2020:2662" target="secadv">RHBA-2020:2662</a> | &nbsp;
selinux-policy-doc-3.13.1-266.el7_8.1 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2020:2662" target="secadv">RHBA-2020:2662</a> | &nbsp;
selinux-policy-minimum-3.13.1-266.el7_8.1 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2020:2662" target="secadv">RHBA-2020:2662</a> | &nbsp;
selinux-policy-mls-3.13.1-266.el7_8.1 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2020:2662" target="secadv">RHBA-2020:2662</a> | &nbsp;
selinux-policy-sandbox-3.13.1-266.el7_8.1 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2020:2662" target="secadv">RHBA-2020:2662</a> | &nbsp;
selinux-policy-targeted-3.13.1-266.el7_8.1 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2020:2662" target="secadv">RHBA-2020:2662</a> | &nbsp;
systemd-219-73.el7_8.8 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2020:2659" target="secadv">RHBA-2020:2659</a> | &nbsp;
systemd-devel-219-73.el7_8.8 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2020:2659" target="secadv">RHBA-2020:2659</a> | &nbsp;
systemd-journal-gateway-219-73.el7_8.8 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2020:2659" target="secadv">RHBA-2020:2659</a> | &nbsp;
systemd-libs-219-73.el7_8.8 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2020:2659" target="secadv">RHBA-2020:2659</a> | &nbsp;
systemd-networkd-219-73.el7_8.8 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2020:2659" target="secadv">RHBA-2020:2659</a> | &nbsp;
systemd-python-219-73.el7_8.8 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2020:2659" target="secadv">RHBA-2020:2659</a> | &nbsp;
systemd-resolved-219-73.el7_8.8 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2020:2659" target="secadv">RHBA-2020:2659</a> | &nbsp;
systemd-sysv-219-73.el7_8.8 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2020:2659" target="secadv">RHBA-2020:2659</a> | &nbsp;
kernel-azure-debug-3.10.0-1127.13.1.el7.azure | &nbsp; &nbsp; | &nbsp;
kernel-azure-debug-devel-3.10.0-1127.13.1.el7.azure | &nbsp; &nbsp; | &nbsp;

