## 2022-09-15


Package | Advisory | Notes
------- | -------- | -----
thunderbird-91.13.0-1.el7.centos.plus | &nbsp; &nbsp; | &nbsp;
cern-linuxsupport-access-1.8-1.el7.cern | &nbsp; &nbsp; | &nbsp;
podman-1.6.4-36.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2022:6119" target="secadv">RHSA-2022:6119</a> | &nbsp;
podman-docker-1.6.4-36.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2022:6119" target="secadv">RHSA-2022:6119</a> | &nbsp;
podman-remote-1.6.4-36.el7_9 | &nbsp; &nbsp; | &nbsp;
podman-tests-1.6.4-36.el7_9 | &nbsp; &nbsp; | &nbsp;
rh-python38-python-3.8.13-1.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2022:1664" target="secadv">RHSA-2022:1664</a> | &nbsp;
rh-python38-python-debug-3.8.13-1.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2022:1664" target="secadv">RHSA-2022:1664</a> | &nbsp;
rh-python38-python-devel-3.8.13-1.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2022:1664" target="secadv">RHSA-2022:1664</a> | &nbsp;
rh-python38-python-idle-3.8.13-1.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2022:1664" target="secadv">RHSA-2022:1664</a> | &nbsp;
rh-python38-python-libs-3.8.13-1.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2022:1664" target="secadv">RHSA-2022:1664</a> | &nbsp;
rh-python38-python-rpm-macros-3.8.13-1.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2022:1664" target="secadv">RHSA-2022:1664</a> | &nbsp;
rh-python38-python-srpm-macros-3.8.13-1.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2022:1664" target="secadv">RHSA-2022:1664</a> | &nbsp;
rh-python38-python-test-3.8.13-1.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2022:1664" target="secadv">RHSA-2022:1664</a> | &nbsp;
rh-python38-python-tkinter-3.8.13-1.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2022:1664" target="secadv">RHSA-2022:1664</a> | &nbsp;
firefox-91.13.0-1.el7.centos | &nbsp; &nbsp; | &nbsp;
libgudev1-219-78.el7_9.7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2022:6160" target="secadv">RHSA-2022:6160</a> | &nbsp;
libgudev1-devel-219-78.el7_9.7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2022:6160" target="secadv">RHSA-2022:6160</a> | &nbsp;
open-vm-tools-11.0.5-3.el7_9.4 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2022:6381" target="secadv">RHSA-2022:6381</a> | &nbsp;
open-vm-tools-desktop-11.0.5-3.el7_9.4 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2022:6381" target="secadv">RHSA-2022:6381</a> | &nbsp;
open-vm-tools-devel-11.0.5-3.el7_9.4 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2022:6381" target="secadv">RHSA-2022:6381</a> | &nbsp;
open-vm-tools-test-11.0.5-3.el7_9.4 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2022:6381" target="secadv">RHSA-2022:6381</a> | &nbsp;
rsync-3.1.2-11.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2022:6170" target="secadv">RHSA-2022:6170</a> | &nbsp;
systemd-219-78.el7_9.7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2022:6160" target="secadv">RHSA-2022:6160</a> | &nbsp;
systemd-devel-219-78.el7_9.7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2022:6160" target="secadv">RHSA-2022:6160</a> | &nbsp;
systemd-journal-gateway-219-78.el7_9.7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2022:6160" target="secadv">RHSA-2022:6160</a> | &nbsp;
systemd-libs-219-78.el7_9.7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2022:6160" target="secadv">RHSA-2022:6160</a> | &nbsp;
systemd-networkd-219-78.el7_9.7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2022:6160" target="secadv">RHSA-2022:6160</a> | &nbsp;
systemd-python-219-78.el7_9.7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2022:6160" target="secadv">RHSA-2022:6160</a> | &nbsp;
systemd-resolved-219-78.el7_9.7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2022:6160" target="secadv">RHSA-2022:6160</a> | &nbsp;
systemd-sysv-219-78.el7_9.7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2022:6160" target="secadv">RHSA-2022:6160</a> | &nbsp;
thunderbird-91.13.0-1.el7.centos | &nbsp; &nbsp; | &nbsp;
tzdata-2022c-1.el7 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2022:6138" target="secadv">RHBA-2022:6138</a> | &nbsp;
tzdata-java-2022c-1.el7 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2022:6138" target="secadv">RHBA-2022:6138</a> | &nbsp;

