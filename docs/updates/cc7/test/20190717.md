## 2019-07-17

Package | Advisory | Notes
------- | -------- | -----
openstack-cinder-14.0.1-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-cinder-doc-14.0.1-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-nova-17.0.11-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-nova-api-17.0.11-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-nova-cells-17.0.11-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-nova-common-17.0.11-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-nova-compute-17.0.11-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-nova-conductor-17.0.11-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-nova-console-17.0.11-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-nova-doc-17.0.11-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-nova-migration-17.0.11-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-nova-network-17.0.11-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-nova-novncproxy-17.0.11-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-nova-placement-api-17.0.11-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-nova-scheduler-17.0.11-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-nova-serialproxy-17.0.11-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-nova-spicehtml5proxy-17.0.11-1.el7 | &nbsp; &nbsp; | &nbsp;
python2-cinder-14.0.1-1.el7 | &nbsp; &nbsp; | &nbsp;
python2-cinderclient-4.2.1-1.el7 | &nbsp; &nbsp; | &nbsp;
python2-cinder-tests-14.0.1-1.el7 | &nbsp; &nbsp; | &nbsp;
python2-os-brick-2.8.2-1.el7 | &nbsp; &nbsp; | &nbsp;
python-cinderclient-doc-4.2.1-1.el7 | &nbsp; &nbsp; | &nbsp;
python-nova-17.0.11-1.el7 | &nbsp; &nbsp; | &nbsp;
python-nova-tests-17.0.11-1.el7 | &nbsp; &nbsp; | &nbsp;
