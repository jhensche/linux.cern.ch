## 2014-11-10

Package | Advisory | Notes
------- | -------- | -----
kernel-plus-3.10.0-123.9.3.el7.centos.plus | &nbsp; &nbsp; | &nbsp;
kernel-plus-abi-whitelists-3.10.0-123.9.3.el7.centos.plus | &nbsp; &nbsp; | &nbsp;
kernel-plus-devel-3.10.0-123.9.3.el7.centos.plus | &nbsp; &nbsp; | &nbsp;
kernel-plus-doc-3.10.0-123.9.3.el7.centos.plus | &nbsp; &nbsp; | &nbsp;
kernel-plus-headers-3.10.0-123.9.3.el7.centos.plus | &nbsp; &nbsp; | &nbsp;
kernel-plus-tools-3.10.0-123.9.3.el7.centos.plus | &nbsp; &nbsp; | &nbsp;
kernel-plus-tools-libs-3.10.0-123.9.3.el7.centos.plus | &nbsp; &nbsp; | &nbsp;
kernel-plus-tools-libs-devel-3.10.0-123.9.3.el7.centos.plus | &nbsp; &nbsp; | &nbsp;
perf-3.10.0-123.9.3.el7.centos.plus | &nbsp; &nbsp; | &nbsp;
python-perf-3.10.0-123.9.3.el7.centos.plus | &nbsp; &nbsp; | &nbsp;
docker-1.2.0-1.8.el7.centos | &nbsp; &nbsp; | &nbsp;
docker-devel-1.2.0-1.8.el7.centos | &nbsp; &nbsp; | &nbsp;
