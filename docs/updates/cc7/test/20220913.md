## 2022-09-13


Package | Advisory | Notes
------- | -------- | -----
cern-linuxsupport-access-1.8-1.el7.cern | &nbsp; &nbsp; | &nbsp;
rh-python38-python-3.8.13-1.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2022:1664" target="secadv">RHSA-2022:1664</a> | &nbsp;
rh-python38-python-debug-3.8.13-1.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2022:1664" target="secadv">RHSA-2022:1664</a> | &nbsp;
rh-python38-python-devel-3.8.13-1.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2022:1664" target="secadv">RHSA-2022:1664</a> | &nbsp;
rh-python38-python-idle-3.8.13-1.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2022:1664" target="secadv">RHSA-2022:1664</a> | &nbsp;
rh-python38-python-libs-3.8.13-1.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2022:1664" target="secadv">RHSA-2022:1664</a> | &nbsp;
rh-python38-python-rpm-macros-3.8.13-1.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2022:1664" target="secadv">RHSA-2022:1664</a> | &nbsp;
rh-python38-python-srpm-macros-3.8.13-1.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2022:1664" target="secadv">RHSA-2022:1664</a> | &nbsp;
rh-python38-python-test-3.8.13-1.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2022:1664" target="secadv">RHSA-2022:1664</a> | &nbsp;
rh-python38-python-tkinter-3.8.13-1.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2022:1664" target="secadv">RHSA-2022:1664</a> | &nbsp;

