## 2018-06-19

Package | Advisory | Notes
------- | -------- | -----
openstack-ceilometer-api-9.0.6-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-ceilometer-central-9.0.6-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-ceilometer-collector-9.0.6-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-ceilometer-common-9.0.6-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-ceilometer-compute-9.0.6-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-ceilometer-ipmi-9.0.6-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-ceilometer-notification-9.0.6-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-ceilometer-polling-9.0.6-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-mistral-all-5.2.4-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-mistral-api-5.2.4-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-mistral-common-5.2.4-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-mistral-doc-5.2.4-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-mistral-engine-5.2.4-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-mistral-event-engine-5.2.4-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-mistral-executor-5.2.4-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-mistral-ui-5.2.4-1.el7 | &nbsp; &nbsp; | &nbsp;
python2-django-openstack-auth-3.6.1-1.el7 | &nbsp; &nbsp; | &nbsp;
python2-oslo-utils-3.28.3-1.el7 | &nbsp; &nbsp; | &nbsp;
python-ceilometer-9.0.6-1.el7 | &nbsp; &nbsp; | &nbsp;
python-ceilometer-tests-9.0.6-1.el7 | &nbsp; &nbsp; | &nbsp;
python-mistral-5.2.4-1.el7 | &nbsp; &nbsp; | &nbsp;
python-mistral-tests-5.2.4-1.el7 | &nbsp; &nbsp; | &nbsp;
python-mistral-ui-doc-5.2.4-1.el7 | &nbsp; &nbsp; | &nbsp;
python-oslo-utils-doc-3.28.3-1.el7 | &nbsp; &nbsp; | &nbsp;
python-oslo-utils-lang-3.28.3-1.el7 | &nbsp; &nbsp; | &nbsp;
python-oslo-utils-tests-3.28.3-1.el7 | &nbsp; &nbsp; | &nbsp;
