## 2014-09-19

Package | Advisory | Notes
------- | -------- | -----
gskcrypt64-8.0-14.43 | &nbsp; &nbsp; | &nbsp;
gskssl64-8.0-14.43 | &nbsp; &nbsp; | &nbsp;
kmod-tty-kraven-2.0-3.el7.cern | &nbsp; &nbsp; | &nbsp;
TIVsm-API64-7.1.1-0 | &nbsp; &nbsp; | &nbsp;
TIVsm-APIcit-7.1.1-0 | &nbsp; &nbsp; | &nbsp;
TIVsm-BA-7.1.1-0 | &nbsp; &nbsp; | &nbsp;
TIVsm-BAcit-7.1.1-0 | &nbsp; &nbsp; | &nbsp;
TIVsm-BAhdw-7.1.1-0 | &nbsp; &nbsp; | &nbsp;
TIVsm-filepath-7.1.1-0 | &nbsp; &nbsp; | &nbsp;
TIVsm-JBB-7.1.1-0 | &nbsp; &nbsp; | &nbsp;
ca-certificates-2014.1.98-70.0.el7_0 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2014:1259" target="secadv">RHBA-2014:1259</a> | &nbsp;
firefox-31.1.0-6.el7.centos | &nbsp; &nbsp; | &nbsp;
NetworkManager-0.9.9.1-26.git20140326.4dba720.el7_0 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2014:1257" target="secadv">RHBA-2014:1257</a> | &nbsp;
NetworkManager-config-server-0.9.9.1-26.git20140326.4dba720.el7_0 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2014:1257" target="secadv">RHBA-2014:1257</a> | &nbsp;
NetworkManager-devel-0.9.9.1-26.git20140326.4dba720.el7_0 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2014:1257" target="secadv">RHBA-2014:1257</a> | &nbsp;
NetworkManager-glib-0.9.9.1-26.git20140326.4dba720.el7_0 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2014:1257" target="secadv">RHBA-2014:1257</a> | &nbsp;
NetworkManager-glib-devel-0.9.9.1-26.git20140326.4dba720.el7_0 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2014:1257" target="secadv">RHBA-2014:1257</a> | &nbsp;
NetworkManager-tui-0.9.9.1-26.git20140326.4dba720.el7_0 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2014:1257" target="secadv">RHBA-2014:1257</a> | &nbsp;
