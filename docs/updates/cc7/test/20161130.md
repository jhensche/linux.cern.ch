## 2016-11-30

Package | Advisory | Notes
------- | -------- | -----
cern-get-sso-cookie-0.5.8-1.el7.cern | &nbsp; &nbsp; | &nbsp;
perl-WWW-CERNSSO-Auth-0.5.8-1.el7.cern | &nbsp; &nbsp; | &nbsp;
