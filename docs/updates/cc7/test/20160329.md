## 2016-03-29

Package | Advisory | Notes
------- | -------- | -----
java-1.8.0-oracle-1.8.0.77-1.el7.cern | &nbsp; &nbsp; | &nbsp;
java-1.8.0-oracle-devel-1.8.0.77-1.el7.cern | &nbsp; &nbsp; | &nbsp;
java-1.8.0-oracle-headless-1.8.0.77-1.el7.cern | &nbsp; &nbsp; | &nbsp;
java-1.8.0-oracle-javafx-1.8.0.77-1.el7.cern | &nbsp; &nbsp; | &nbsp;
java-1.8.0-oracle-plugin-1.8.0.77-1.el7.cern | &nbsp; &nbsp; | &nbsp;
java-1.8.0-oracle-src-1.8.0.77-1.el7.cern | &nbsp; &nbsp; | &nbsp;
centos-release-scl-2-1.el7.centos | &nbsp; &nbsp; | &nbsp;
centos-release-scl-rh-2-1.el7.centos | &nbsp; &nbsp; | &nbsp;
java-1.7.0-openjdk-1.7.0.99-2.6.5.0.el7_2 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2016:0512" target="secadv">RHSA-2016:0512</a> | &nbsp;
java-1.7.0-openjdk-accessibility-1.7.0.99-2.6.5.0.el7_2 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2016:0512" target="secadv">RHSA-2016:0512</a> | &nbsp;
java-1.7.0-openjdk-demo-1.7.0.99-2.6.5.0.el7_2 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2016:0512" target="secadv">RHSA-2016:0512</a> | &nbsp;
java-1.7.0-openjdk-devel-1.7.0.99-2.6.5.0.el7_2 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2016:0512" target="secadv">RHSA-2016:0512</a> | &nbsp;
java-1.7.0-openjdk-headless-1.7.0.99-2.6.5.0.el7_2 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2016:0512" target="secadv">RHSA-2016:0512</a> | &nbsp;
java-1.7.0-openjdk-javadoc-1.7.0.99-2.6.5.0.el7_2 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2016:0512" target="secadv">RHSA-2016:0512</a> | &nbsp;
java-1.7.0-openjdk-src-1.7.0.99-2.6.5.0.el7_2 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2016:0512" target="secadv">RHSA-2016:0512</a> | &nbsp;
java-1.8.0-openjdk-1.8.0.77-0.b03.el7_2 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2016:0513" target="secadv">RHSA-2016:0513</a> | &nbsp;
java-1.8.0-openjdk-accessibility-1.8.0.77-0.b03.el7_2 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2016:0513" target="secadv">RHSA-2016:0513</a> | &nbsp;
java-1.8.0-openjdk-accessibility-debug-1.8.0.77-0.b03.el7_2 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2016:0513" target="secadv">RHSA-2016:0513</a> | &nbsp;
java-1.8.0-openjdk-debug-1.8.0.77-0.b03.el7_2 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2016:0513" target="secadv">RHSA-2016:0513</a> | &nbsp;
java-1.8.0-openjdk-demo-1.8.0.77-0.b03.el7_2 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2016:0513" target="secadv">RHSA-2016:0513</a> | &nbsp;
java-1.8.0-openjdk-demo-debug-1.8.0.77-0.b03.el7_2 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2016:0513" target="secadv">RHSA-2016:0513</a> | &nbsp;
java-1.8.0-openjdk-devel-1.8.0.77-0.b03.el7_2 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2016:0513" target="secadv">RHSA-2016:0513</a> | &nbsp;
java-1.8.0-openjdk-devel-debug-1.8.0.77-0.b03.el7_2 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2016:0513" target="secadv">RHSA-2016:0513</a> | &nbsp;
java-1.8.0-openjdk-headless-1.8.0.77-0.b03.el7_2 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2016:0513" target="secadv">RHSA-2016:0513</a> | &nbsp;
java-1.8.0-openjdk-headless-debug-1.8.0.77-0.b03.el7_2 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2016:0513" target="secadv">RHSA-2016:0513</a> | &nbsp;
java-1.8.0-openjdk-javadoc-1.8.0.77-0.b03.el7_2 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2016:0513" target="secadv">RHSA-2016:0513</a> | &nbsp;
java-1.8.0-openjdk-javadoc-debug-1.8.0.77-0.b03.el7_2 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2016:0513" target="secadv">RHSA-2016:0513</a> | &nbsp;
java-1.8.0-openjdk-src-1.8.0.77-0.b03.el7_2 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2016:0513" target="secadv">RHSA-2016:0513</a> | &nbsp;
java-1.8.0-openjdk-src-debug-1.8.0.77-0.b03.el7_2 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2016:0513" target="secadv">RHSA-2016:0513</a> | &nbsp;
tzdata-2016c-1.el7 | <div class="adv_e">[E]</div> <a href="https://access.redhat.com/errata/RHEA-2016:0517" target="secadv">RHEA-2016:0517</a> | &nbsp;
tzdata-java-2016c-1.el7 | <div class="adv_e">[E]</div> <a href="https://access.redhat.com/errata/RHEA-2016:0517" target="secadv">RHEA-2016:0517</a> | &nbsp;
