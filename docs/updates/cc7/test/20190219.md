## 2019-02-19

Package | Advisory | Notes
------- | -------- | -----
cx_Oracle-7.1-1.el7.cern | &nbsp; &nbsp; | &nbsp;
cx_Oracle-7.1-2.el7.cern | &nbsp; &nbsp; | &nbsp;
gnocchi-api-4.3.2-1.el7 | &nbsp; &nbsp; | &nbsp;
gnocchi-common-4.3.2-1.el7 | &nbsp; &nbsp; | &nbsp;
gnocchi-doc-4.3.2-1.el7 | &nbsp; &nbsp; | &nbsp;
gnocchi-metricd-4.3.2-1.el7 | &nbsp; &nbsp; | &nbsp;
gnocchi-statsd-4.3.2-1.el7 | &nbsp; &nbsp; | &nbsp;
python-gnocchi-4.3.2-1.el7 | &nbsp; &nbsp; | &nbsp;
python-gnocchi-tests-4.3.2-1.el7 | &nbsp; &nbsp; | &nbsp;
