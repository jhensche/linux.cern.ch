## 2014-10-29

Package | Advisory | Notes
------- | -------- | -----
kernel-plus-3.10.0-123.9.2.el7.centos.plus | &nbsp; &nbsp; | &nbsp;
kernel-plus-abi-whitelists-3.10.0-123.9.2.el7.centos.plus | &nbsp; &nbsp; | &nbsp;
kernel-plus-devel-3.10.0-123.9.2.el7.centos.plus | &nbsp; &nbsp; | &nbsp;
kernel-plus-doc-3.10.0-123.9.2.el7.centos.plus | &nbsp; &nbsp; | &nbsp;
kernel-plus-headers-3.10.0-123.9.2.el7.centos.plus | &nbsp; &nbsp; | &nbsp;
kernel-plus-tools-3.10.0-123.9.2.el7.centos.plus | &nbsp; &nbsp; | &nbsp;
kernel-plus-tools-libs-3.10.0-123.9.2.el7.centos.plus | &nbsp; &nbsp; | &nbsp;
kernel-plus-tools-libs-devel-3.10.0-123.9.2.el7.centos.plus | &nbsp; &nbsp; | &nbsp;
perf-3.10.0-123.9.2.el7.centos.plus | &nbsp; &nbsp; | &nbsp;
python-perf-3.10.0-123.9.2.el7.centos.plus | &nbsp; &nbsp; | &nbsp;
kmod-openafs-1.6.9-2.3.10.0_123.9.2.el7 | &nbsp; &nbsp; | &nbsp;
kernel-3.10.0-123.9.2.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2014:1724" target="secadv">RHSA-2014:1724</a> | &nbsp;
kernel-abi-whitelists-3.10.0-123.9.2.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2014:1724" target="secadv">RHSA-2014:1724</a> | &nbsp;
kernel-debug-3.10.0-123.9.2.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2014:1724" target="secadv">RHSA-2014:1724</a> | &nbsp;
kernel-debug-devel-3.10.0-123.9.2.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2014:1724" target="secadv">RHSA-2014:1724</a> | &nbsp;
kernel-devel-3.10.0-123.9.2.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2014:1724" target="secadv">RHSA-2014:1724</a> | &nbsp;
kernel-doc-3.10.0-123.9.2.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2014:1724" target="secadv">RHSA-2014:1724</a> | &nbsp;
kernel-headers-3.10.0-123.9.2.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2014:1724" target="secadv">RHSA-2014:1724</a> | &nbsp;
kernel-tools-3.10.0-123.9.2.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2014:1724" target="secadv">RHSA-2014:1724</a> | &nbsp;
kernel-tools-libs-3.10.0-123.9.2.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2014:1724" target="secadv">RHSA-2014:1724</a> | &nbsp;
kernel-tools-libs-devel-3.10.0-123.9.2.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2014:1724" target="secadv">RHSA-2014:1724</a> | &nbsp;
perf-3.10.0-123.9.2.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2014:1724" target="secadv">RHSA-2014:1724</a> | &nbsp;
python-perf-3.10.0-123.9.2.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2014:1724" target="secadv">RHSA-2014:1724</a> | &nbsp;
scl-utils-20130529-9.el7_0 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2014:1730" target="secadv">RHBA-2014:1730</a> | &nbsp;
scl-utils-build-20130529-9.el7_0 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2014:1730" target="secadv">RHBA-2014:1730</a> | &nbsp;
