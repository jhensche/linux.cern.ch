## 2018-08-22

Package | Advisory | Notes
------- | -------- | -----
buildah-1.2-2.gitbe87762.el7 | <div class="adv_e">[E]</div> <a href="https://access.redhat.com/errata/RHEA-2018:2480" target="secadv">RHEA-2018:2480</a> | &nbsp;
cockpit-172-2.el7.centos | &nbsp; &nbsp; | &nbsp;
cockpit-bridge-172-2.el7.centos | &nbsp; &nbsp; | &nbsp;
cockpit-dashboard-172-2.el7.centos | &nbsp; &nbsp; | &nbsp;
cockpit-doc-172-2.el7.centos | &nbsp; &nbsp; | &nbsp;
cockpit-docker-172-2.el7.centos | &nbsp; &nbsp; | &nbsp;
cockpit-kubernetes-172-2.el7.centos | &nbsp; &nbsp; | &nbsp;
cockpit-machines-172-2.el7.centos | &nbsp; &nbsp; | &nbsp;
cockpit-machines-ovirt-172-2.el7.centos | &nbsp; &nbsp; | &nbsp;
cockpit-packagekit-172-2.el7.centos | &nbsp; &nbsp; | &nbsp;
cockpit-pcp-172-2.el7.centos | &nbsp; &nbsp; | &nbsp;
cockpit-selinux-172-2.el7.centos | &nbsp; &nbsp; | &nbsp;
cockpit-storaged-172-2.el7.centos | &nbsp; &nbsp; | &nbsp;
cockpit-system-172-2.el7.centos | &nbsp; &nbsp; | &nbsp;
cockpit-tests-172-2.el7.centos | &nbsp; &nbsp; | &nbsp;
cockpit-ws-172-2.el7.centos | &nbsp; &nbsp; | &nbsp;
containernetworking-plugins-0.7.1-1.el7 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2018:2477" target="secadv">RHBA-2018:2477</a> | &nbsp;
container-selinux-2.68-1.el7 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2018:2481" target="secadv">RHBA-2018:2481</a> | &nbsp;
container-storage-setup-0.11.0-2.git5eaf76c.el7 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2018:2474" target="secadv">RHBA-2018:2474</a> | &nbsp;
docker-1.13.1-74.git6e3bb8e.el7.centos | &nbsp; &nbsp; | &nbsp;
docker-client-1.13.1-74.git6e3bb8e.el7.centos | &nbsp; &nbsp; | &nbsp;
docker-common-1.13.1-74.git6e3bb8e.el7.centos | &nbsp; &nbsp; | &nbsp;
docker-logrotate-1.13.1-74.git6e3bb8e.el7.centos | &nbsp; &nbsp; | &nbsp;
docker-lvm-plugin-1.13.1-74.git6e3bb8e.el7.centos | &nbsp; &nbsp; | &nbsp;
docker-novolume-plugin-1.13.1-74.git6e3bb8e.el7.centos | &nbsp; &nbsp; | &nbsp;
docker-v1.10-migrator-1.13.1-74.git6e3bb8e.el7.centos | &nbsp; &nbsp; | &nbsp;
oci-systemd-hook-0.1.17-2.git83283a0.el7 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2018:2476" target="secadv">RHBA-2018:2476</a> | &nbsp;
podman-0.7.3-1.git0791210.el7 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2018:2479" target="secadv">RHBA-2018:2479</a> | &nbsp;
rhel-system-roles-1.0-2.el7 | <div class="adv_e">[E]</div> <a href="https://access.redhat.com/errata/RHEA-2018:2385" target="secadv">RHEA-2018:2385</a> | &nbsp;
rhel-system-roles-techpreview-1.0-2.el7 | <div class="adv_e">[E]</div> <a href="https://access.redhat.com/errata/RHEA-2018:2385" target="secadv">RHEA-2018:2385</a> | &nbsp;
runc-1.0.0-37.rc5.dev.gitad0f525.el7 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2018:2478" target="secadv">RHBA-2018:2478</a> | &nbsp;
