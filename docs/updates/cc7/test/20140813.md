## 2014-08-13

Package | Advisory | Notes
------- | -------- | -----
libreswan-3.8-6.el7_0 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2014:1049" target="secadv">RHBA-2014:1049</a> | &nbsp;
yum-NetworkManager-dispatcher-1.1.31-25.el7_0 | <div class="adv_e">[E]</div> <a href="https://access.redhat.com/errata/RHEA-2014:1046" target="secadv">RHEA-2014:1046</a> | &nbsp;
yum-plugin-aliases-1.1.31-25.el7_0 | <div class="adv_e">[E]</div> <a href="https://access.redhat.com/errata/RHEA-2014:1046" target="secadv">RHEA-2014:1046</a> | &nbsp;
yum-plugin-auto-update-debug-info-1.1.31-25.el7_0 | <div class="adv_e">[E]</div> <a href="https://access.redhat.com/errata/RHEA-2014:1046" target="secadv">RHEA-2014:1046</a> | &nbsp;
yum-plugin-changelog-1.1.31-25.el7_0 | <div class="adv_e">[E]</div> <a href="https://access.redhat.com/errata/RHEA-2014:1046" target="secadv">RHEA-2014:1046</a> | &nbsp;
yum-plugin-fastestmirror-1.1.31-25.el7_0 | <div class="adv_e">[E]</div> <a href="https://access.redhat.com/errata/RHEA-2014:1046" target="secadv">RHEA-2014:1046</a> | &nbsp;
yum-plugin-filter-data-1.1.31-25.el7_0 | <div class="adv_e">[E]</div> <a href="https://access.redhat.com/errata/RHEA-2014:1046" target="secadv">RHEA-2014:1046</a> | &nbsp;
yum-plugin-fs-snapshot-1.1.31-25.el7_0 | <div class="adv_e">[E]</div> <a href="https://access.redhat.com/errata/RHEA-2014:1046" target="secadv">RHEA-2014:1046</a> | &nbsp;
yum-plugin-keys-1.1.31-25.el7_0 | <div class="adv_e">[E]</div> <a href="https://access.redhat.com/errata/RHEA-2014:1046" target="secadv">RHEA-2014:1046</a> | &nbsp;
yum-plugin-list-data-1.1.31-25.el7_0 | <div class="adv_e">[E]</div> <a href="https://access.redhat.com/errata/RHEA-2014:1046" target="secadv">RHEA-2014:1046</a> | &nbsp;
yum-plugin-local-1.1.31-25.el7_0 | <div class="adv_e">[E]</div> <a href="https://access.redhat.com/errata/RHEA-2014:1046" target="secadv">RHEA-2014:1046</a> | &nbsp;
yum-plugin-merge-conf-1.1.31-25.el7_0 | <div class="adv_e">[E]</div> <a href="https://access.redhat.com/errata/RHEA-2014:1046" target="secadv">RHEA-2014:1046</a> | &nbsp;
yum-plugin-post-transaction-actions-1.1.31-25.el7_0 | <div class="adv_e">[E]</div> <a href="https://access.redhat.com/errata/RHEA-2014:1046" target="secadv">RHEA-2014:1046</a> | &nbsp;
yum-plugin-priorities-1.1.31-25.el7_0 | <div class="adv_e">[E]</div> <a href="https://access.redhat.com/errata/RHEA-2014:1046" target="secadv">RHEA-2014:1046</a> | &nbsp;
yum-plugin-protectbase-1.1.31-25.el7_0 | <div class="adv_e">[E]</div> <a href="https://access.redhat.com/errata/RHEA-2014:1046" target="secadv">RHEA-2014:1046</a> | &nbsp;
yum-plugin-ps-1.1.31-25.el7_0 | <div class="adv_e">[E]</div> <a href="https://access.redhat.com/errata/RHEA-2014:1046" target="secadv">RHEA-2014:1046</a> | &nbsp;
yum-plugin-remove-with-leaves-1.1.31-25.el7_0 | <div class="adv_e">[E]</div> <a href="https://access.redhat.com/errata/RHEA-2014:1046" target="secadv">RHEA-2014:1046</a> | &nbsp;
yum-plugin-rpm-warm-cache-1.1.31-25.el7_0 | <div class="adv_e">[E]</div> <a href="https://access.redhat.com/errata/RHEA-2014:1046" target="secadv">RHEA-2014:1046</a> | &nbsp;
yum-plugin-show-leaves-1.1.31-25.el7_0 | <div class="adv_e">[E]</div> <a href="https://access.redhat.com/errata/RHEA-2014:1046" target="secadv">RHEA-2014:1046</a> | &nbsp;
yum-plugin-tmprepo-1.1.31-25.el7_0 | <div class="adv_e">[E]</div> <a href="https://access.redhat.com/errata/RHEA-2014:1046" target="secadv">RHEA-2014:1046</a> | &nbsp;
yum-plugin-tsflags-1.1.31-25.el7_0 | <div class="adv_e">[E]</div> <a href="https://access.redhat.com/errata/RHEA-2014:1046" target="secadv">RHEA-2014:1046</a> | &nbsp;
yum-plugin-upgrade-helper-1.1.31-25.el7_0 | <div class="adv_e">[E]</div> <a href="https://access.redhat.com/errata/RHEA-2014:1046" target="secadv">RHEA-2014:1046</a> | &nbsp;
yum-plugin-verify-1.1.31-25.el7_0 | <div class="adv_e">[E]</div> <a href="https://access.redhat.com/errata/RHEA-2014:1046" target="secadv">RHEA-2014:1046</a> | &nbsp;
yum-plugin-versionlock-1.1.31-25.el7_0 | <div class="adv_e">[E]</div> <a href="https://access.redhat.com/errata/RHEA-2014:1046" target="secadv">RHEA-2014:1046</a> | &nbsp;
yum-updateonboot-1.1.31-25.el7_0 | <div class="adv_e">[E]</div> <a href="https://access.redhat.com/errata/RHEA-2014:1046" target="secadv">RHEA-2014:1046</a> | &nbsp;
yum-utils-1.1.31-25.el7_0 | <div class="adv_e">[E]</div> <a href="https://access.redhat.com/errata/RHEA-2014:1046" target="secadv">RHEA-2014:1046</a> | &nbsp;
