## 2017-04-05

Package | Advisory | Notes
------- | -------- | -----
ImageMagick-6.7.8.9-15.el7_2.cern | &nbsp; &nbsp; | &nbsp;
ImageMagick-6.7.8.9-15.el7.cern | &nbsp; &nbsp; | &nbsp;
ImageMagick-c++-6.7.8.9-15.el7_2.cern | &nbsp; &nbsp; | &nbsp;
ImageMagick-c++-6.7.8.9-15.el7.cern | &nbsp; &nbsp; | &nbsp;
ImageMagick-c++-devel-6.7.8.9-15.el7_2.cern | &nbsp; &nbsp; | &nbsp;
ImageMagick-c++-devel-6.7.8.9-15.el7.cern | &nbsp; &nbsp; | &nbsp;
ImageMagick-devel-6.7.8.9-15.el7_2.cern | &nbsp; &nbsp; | &nbsp;
ImageMagick-devel-6.7.8.9-15.el7.cern | &nbsp; &nbsp; | &nbsp;
ImageMagick-doc-6.7.8.9-15.el7_2.cern | &nbsp; &nbsp; | &nbsp;
ImageMagick-doc-6.7.8.9-15.el7.cern | &nbsp; &nbsp; | &nbsp;
ImageMagick-perl-6.7.8.9-15.el7_2.cern | &nbsp; &nbsp; | &nbsp;
ImageMagick-perl-6.7.8.9-15.el7.cern | &nbsp; &nbsp; | &nbsp;
NetworkManager-DUID-LLT-0.2-1.el7.cern | &nbsp; &nbsp; | &nbsp;
cloud-init-0.7.9-3.el7 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2017:0871" target="secadv">RHBA-2017:0871</a> | &nbsp;
cloud-utils-growpart-0.29-1.el7 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2017:0871" target="secadv">RHBA-2017:0871</a> | &nbsp;
pyserial-2.6-5.el7 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2017:0871" target="secadv">RHBA-2017:0871</a> | &nbsp;
xen-4.6.3-12.el7 | &nbsp; &nbsp; | &nbsp;
xen-devel-4.6.3-12.el7 | &nbsp; &nbsp; | &nbsp;
xen-doc-4.6.3-12.el7 | &nbsp; &nbsp; | &nbsp;
xen-hypervisor-4.6.3-12.el7 | &nbsp; &nbsp; | &nbsp;
xen-libs-4.6.3-12.el7 | &nbsp; &nbsp; | &nbsp;
xen-licenses-4.6.3-12.el7 | &nbsp; &nbsp; | &nbsp;
xen-ocaml-4.6.3-12.el7 | &nbsp; &nbsp; | &nbsp;
xen-ocaml-devel-4.6.3-12.el7 | &nbsp; &nbsp; | &nbsp;
xen-runtime-4.6.3-12.el7 | &nbsp; &nbsp; | &nbsp;
