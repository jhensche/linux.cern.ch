## 2018-10-10

Package | Advisory | Notes
------- | -------- | -----
centos-release-7-5.1804.5.el7.centos | &nbsp; &nbsp; | &nbsp;
firefox-60.2.2-1.el7.centos | &nbsp; &nbsp; | &nbsp;
