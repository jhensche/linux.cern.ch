## 2021-03-04


Package | Advisory | Notes
------- | -------- | -----
grub2-2.02-0.87.el7.centos.2 | &nbsp; &nbsp; | &nbsp;
grub2-common-2.02-0.87.el7.centos.2 | &nbsp; &nbsp; | &nbsp;
grub2-efi-aa64-modules-2.02-0.87.el7.centos.2 | &nbsp; &nbsp; | &nbsp;
grub2-efi-ia32-2.02-0.87.el7.centos.2 | &nbsp; &nbsp; | &nbsp;
grub2-efi-ia32-cdboot-2.02-0.87.el7.centos.2 | &nbsp; &nbsp; | &nbsp;
grub2-efi-ia32-modules-2.02-0.87.el7.centos.2 | &nbsp; &nbsp; | &nbsp;
grub2-efi-x64-2.02-0.87.el7.centos.2 | &nbsp; &nbsp; | &nbsp;
grub2-efi-x64-cdboot-2.02-0.87.el7.centos.2 | &nbsp; &nbsp; | &nbsp;
grub2-efi-x64-modules-2.02-0.87.el7.centos.2 | &nbsp; &nbsp; | &nbsp;
grub2-i386-modules-2.02-0.87.el7.centos.2 | &nbsp; &nbsp; | &nbsp;
grub2-pc-2.02-0.87.el7.centos.2 | &nbsp; &nbsp; | &nbsp;
grub2-pc-modules-2.02-0.87.el7.centos.2 | &nbsp; &nbsp; | &nbsp;
grub2-ppc-modules-2.02-0.87.el7.centos.2 | &nbsp; &nbsp; | &nbsp;
grub2-ppc64-modules-2.02-0.87.el7.centos.2 | &nbsp; &nbsp; | &nbsp;
grub2-ppc64le-modules-2.02-0.87.el7.centos.2 | &nbsp; &nbsp; | &nbsp;
grub2-tools-2.02-0.87.el7.centos.2 | &nbsp; &nbsp; | &nbsp;
grub2-tools-extra-2.02-0.87.el7.centos.2 | &nbsp; &nbsp; | &nbsp;
grub2-tools-minimal-2.02-0.87.el7.centos.2 | &nbsp; &nbsp; | &nbsp;

