## 2017-01-25

Package | Advisory | Notes
------- | -------- | -----
sclo-php56-php-pecl-igbinary-2.0.1-1.el7 | &nbsp; &nbsp; | &nbsp;
sclo-php56-php-pecl-igbinary-devel-2.0.1-1.el7 | &nbsp; &nbsp; | &nbsp;
sclo-php70-php-pecl-igbinary-2.0.1-1.el7 | &nbsp; &nbsp; | &nbsp;
sclo-php70-php-pecl-igbinary-devel-2.0.1-1.el7 | &nbsp; &nbsp; | &nbsp;
