## 2020-07-17


Package | Advisory | Notes
------- | -------- | -----
python2-oslo-policy-2.1.3-1.el7 | &nbsp; &nbsp; | &nbsp;
python2-oslo-policy-tests-2.1.3-1.el7 | &nbsp; &nbsp; | &nbsp;
python-networking-arista-2018.1.16-1.el7 | &nbsp; &nbsp; | &nbsp;
python-oslo-policy-doc-2.1.3-1.el7 | &nbsp; &nbsp; | &nbsp;
python-oslo-policy-lang-2.1.3-1.el7 | &nbsp; &nbsp; | &nbsp;
thunderbird-68.10.0-1.el7.centos | &nbsp; &nbsp; | &nbsp;

