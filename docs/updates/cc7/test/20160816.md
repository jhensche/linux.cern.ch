## 2016-08-16

Package | Advisory | Notes
------- | -------- | -----
libcacard-1.5.3-105.el7_2.7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2016:1606" target="secadv">RHSA-2016:1606</a> | &nbsp;
libcacard-devel-1.5.3-105.el7_2.7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2016:1606" target="secadv">RHSA-2016:1606</a> | &nbsp;
libcacard-tools-1.5.3-105.el7_2.7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2016:1606" target="secadv">RHSA-2016:1606</a> | &nbsp;
mariadb-5.5.50-1.el7_2 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2016:1602" target="secadv">RHSA-2016:1602</a> | &nbsp;
mariadb-bench-5.5.50-1.el7_2 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2016:1602" target="secadv">RHSA-2016:1602</a> | &nbsp;
mariadb-devel-5.5.50-1.el7_2 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2016:1602" target="secadv">RHSA-2016:1602</a> | &nbsp;
mariadb-embedded-5.5.50-1.el7_2 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2016:1602" target="secadv">RHSA-2016:1602</a> | &nbsp;
mariadb-embedded-devel-5.5.50-1.el7_2 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2016:1602" target="secadv">RHSA-2016:1602</a> | &nbsp;
mariadb-libs-5.5.50-1.el7_2 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2016:1602" target="secadv">RHSA-2016:1602</a> | &nbsp;
mariadb-server-5.5.50-1.el7_2 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2016:1602" target="secadv">RHSA-2016:1602</a> | &nbsp;
mariadb-test-5.5.50-1.el7_2 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2016:1602" target="secadv">RHSA-2016:1602</a> | &nbsp;
php-5.4.16-36.3.el7_2 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2016:1613" target="secadv">RHSA-2016:1613</a> | &nbsp;
php-bcmath-5.4.16-36.3.el7_2 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2016:1613" target="secadv">RHSA-2016:1613</a> | &nbsp;
php-cli-5.4.16-36.3.el7_2 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2016:1613" target="secadv">RHSA-2016:1613</a> | &nbsp;
php-common-5.4.16-36.3.el7_2 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2016:1613" target="secadv">RHSA-2016:1613</a> | &nbsp;
php-dba-5.4.16-36.3.el7_2 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2016:1613" target="secadv">RHSA-2016:1613</a> | &nbsp;
php-devel-5.4.16-36.3.el7_2 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2016:1613" target="secadv">RHSA-2016:1613</a> | &nbsp;
php-embedded-5.4.16-36.3.el7_2 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2016:1613" target="secadv">RHSA-2016:1613</a> | &nbsp;
php-enchant-5.4.16-36.3.el7_2 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2016:1613" target="secadv">RHSA-2016:1613</a> | &nbsp;
php-fpm-5.4.16-36.3.el7_2 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2016:1613" target="secadv">RHSA-2016:1613</a> | &nbsp;
php-gd-5.4.16-36.3.el7_2 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2016:1613" target="secadv">RHSA-2016:1613</a> | &nbsp;
php-intl-5.4.16-36.3.el7_2 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2016:1613" target="secadv">RHSA-2016:1613</a> | &nbsp;
php-ldap-5.4.16-36.3.el7_2 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2016:1613" target="secadv">RHSA-2016:1613</a> | &nbsp;
php-mbstring-5.4.16-36.3.el7_2 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2016:1613" target="secadv">RHSA-2016:1613</a> | &nbsp;
php-mysql-5.4.16-36.3.el7_2 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2016:1613" target="secadv">RHSA-2016:1613</a> | &nbsp;
php-mysqlnd-5.4.16-36.3.el7_2 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2016:1613" target="secadv">RHSA-2016:1613</a> | &nbsp;
php-odbc-5.4.16-36.3.el7_2 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2016:1613" target="secadv">RHSA-2016:1613</a> | &nbsp;
php-pdo-5.4.16-36.3.el7_2 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2016:1613" target="secadv">RHSA-2016:1613</a> | &nbsp;
php-pgsql-5.4.16-36.3.el7_2 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2016:1613" target="secadv">RHSA-2016:1613</a> | &nbsp;
php-process-5.4.16-36.3.el7_2 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2016:1613" target="secadv">RHSA-2016:1613</a> | &nbsp;
php-pspell-5.4.16-36.3.el7_2 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2016:1613" target="secadv">RHSA-2016:1613</a> | &nbsp;
php-recode-5.4.16-36.3.el7_2 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2016:1613" target="secadv">RHSA-2016:1613</a> | &nbsp;
php-snmp-5.4.16-36.3.el7_2 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2016:1613" target="secadv">RHSA-2016:1613</a> | &nbsp;
php-soap-5.4.16-36.3.el7_2 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2016:1613" target="secadv">RHSA-2016:1613</a> | &nbsp;
php-xml-5.4.16-36.3.el7_2 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2016:1613" target="secadv">RHSA-2016:1613</a> | &nbsp;
php-xmlrpc-5.4.16-36.3.el7_2 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2016:1613" target="secadv">RHSA-2016:1613</a> | &nbsp;
qemu-img-1.5.3-105.el7_2.7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2016:1606" target="secadv">RHSA-2016:1606</a> | &nbsp;
qemu-kvm-1.5.3-105.el7_2.7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2016:1606" target="secadv">RHSA-2016:1606</a> | &nbsp;
qemu-kvm-common-1.5.3-105.el7_2.7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2016:1606" target="secadv">RHSA-2016:1606</a> | &nbsp;
qemu-kvm-tools-1.5.3-105.el7_2.7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2016:1606" target="secadv">RHSA-2016:1606</a> | &nbsp;
