## 2017-08-02

Package | Advisory | Notes
------- | -------- | -----
kernel-rt-3.10.0-693.rt56.617.el7 | &nbsp; &nbsp; | &nbsp;
kernel-rt-debug-3.10.0-693.rt56.617.el7 | &nbsp; &nbsp; | &nbsp;
kernel-rt-debug-devel-3.10.0-693.rt56.617.el7 | &nbsp; &nbsp; | &nbsp;
kernel-rt-debug-kvm-3.10.0-693.rt56.617.el7 | &nbsp; &nbsp; | &nbsp;
kernel-rt-devel-3.10.0-693.rt56.617.el7 | &nbsp; &nbsp; | &nbsp;
kernel-rt-doc-3.10.0-693.rt56.617.el7 | &nbsp; &nbsp; | &nbsp;
kernel-rt-kvm-3.10.0-693.rt56.617.el7 | &nbsp; &nbsp; | &nbsp;
kernel-rt-trace-3.10.0-693.rt56.617.el7 | &nbsp; &nbsp; | &nbsp;
kernel-rt-trace-devel-3.10.0-693.rt56.617.el7 | &nbsp; &nbsp; | &nbsp;
kernel-rt-trace-kvm-3.10.0-693.rt56.617.el7 | &nbsp; &nbsp; | &nbsp;
rteval-2.14-1.el7 | &nbsp; &nbsp; | &nbsp;
rteval-common-2.14-1.el7 | &nbsp; &nbsp; | &nbsp;
rteval-loads-1.4-1.el7 | &nbsp; &nbsp; | &nbsp;
rt-tests-1.0-11.el7 | &nbsp; &nbsp; | &nbsp;
virt-p2v-1.36.3-2.el7 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2017:2023" target="secadv">RHBA-2017:2023</a> | &nbsp;
