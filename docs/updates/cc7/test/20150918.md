## 2015-09-18

Package | Advisory | Notes
------- | -------- | -----
kernel-plus-3.10.0-229.14.1.el7.centos.plus | &nbsp; &nbsp; | &nbsp;
kernel-plus-abi-whitelists-3.10.0-229.14.1.el7.centos.plus | &nbsp; &nbsp; | &nbsp;
kernel-plus-devel-3.10.0-229.14.1.el7.centos.plus | &nbsp; &nbsp; | &nbsp;
kernel-plus-doc-3.10.0-229.14.1.el7.centos.plus | &nbsp; &nbsp; | &nbsp;
kernel-plus-headers-3.10.0-229.14.1.el7.centos.plus | &nbsp; &nbsp; | &nbsp;
kernel-plus-tools-3.10.0-229.14.1.el7.centos.plus | &nbsp; &nbsp; | &nbsp;
kernel-plus-tools-libs-3.10.0-229.14.1.el7.centos.plus | &nbsp; &nbsp; | &nbsp;
kernel-plus-tools-libs-devel-3.10.0-229.14.1.el7.centos.plus | &nbsp; &nbsp; | &nbsp;
kmod-openafs-1.6.9-2.3.10.0_229.14.1.el7.centos.plus | &nbsp; &nbsp; | &nbsp;
perf-3.10.0-229.14.1.el7.centos.plus | &nbsp; &nbsp; | &nbsp;
python-perf-3.10.0-229.14.1.el7.centos.plus | &nbsp; &nbsp; | &nbsp;
lpadmincern-1.3.15-1.el7.cern | &nbsp; &nbsp; | &nbsp;
atomic-1.0-115.el7 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2015:1782" target="secadv">RHBA-2015:1782</a> | &nbsp;
cockpit-0.71-1.el7 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2015:1781" target="secadv">RHBA-2015:1781</a> | &nbsp;
cockpit-bridge-0.71-1.el7 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2015:1781" target="secadv">RHBA-2015:1781</a> | &nbsp;
cockpit-doc-0.71-1.el7 | &nbsp; &nbsp; | &nbsp;
cockpit-docker-0.71-1.el7 | &nbsp; &nbsp; | &nbsp;
cockpit-kubernetes-0.71-1.el7 | &nbsp; &nbsp; | &nbsp;
cockpit-pcp-0.71-1.el7 | &nbsp; &nbsp; | &nbsp;
cockpit-selinux-policy-0.71-1.el7 | &nbsp; &nbsp; | &nbsp;
cockpit-shell-0.71-1.el7 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2015:1781" target="secadv">RHBA-2015:1781</a> | &nbsp;
cockpit-storaged-0.71-1.el7 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2015:1781" target="secadv">RHBA-2015:1781</a> | &nbsp;
cockpit-subscriptions-0.71-1.el7 | &nbsp; &nbsp; | &nbsp;
cockpit-ws-0.71-1.el7 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2015:1781" target="secadv">RHBA-2015:1781</a> | &nbsp;
docker-1.7.1-115.el7 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2015:1782" target="secadv">RHBA-2015:1782</a> | &nbsp;
docker-logrotate-1.7.1-115.el7 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2015:1782" target="secadv">RHBA-2015:1782</a> | &nbsp;
docker-python-1.4.0-115.el7 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2015:1782" target="secadv">RHBA-2015:1782</a> | &nbsp;
docker-selinux-1.7.1-115.el7 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2015:1782" target="secadv">RHBA-2015:1782</a> | &nbsp;
docker-unit-test-1.7.1-115.el7 | &nbsp; &nbsp; | &nbsp;
etcd-2.1.1-2.el7 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2015:1783" target="secadv">RHBA-2015:1783</a> | &nbsp;
kubernetes-1.0.3-0.1.gitb9a88a7.el7 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2015:1776" target="secadv">RHBA-2015:1776</a> | &nbsp;
kubernetes-client-1.0.3-0.1.gitb9a88a7.el7 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2015:1776" target="secadv">RHBA-2015:1776</a> | &nbsp;
kubernetes-master-1.0.3-0.1.gitb9a88a7.el7 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2015:1776" target="secadv">RHBA-2015:1776</a> | &nbsp;
kubernetes-node-1.0.3-0.1.gitb9a88a7.el7 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2015:1776" target="secadv">RHBA-2015:1776</a> | &nbsp;
kubernetes-unit-test-1.0.3-0.1.gitb9a88a7.el7 | &nbsp; &nbsp; | &nbsp;
libev-4.15-6.el7 | <div class="adv_e">[E]</div> <a href="https://access.redhat.com/errata/RHEA-2015:1797" target="secadv">RHEA-2015:1797</a> | &nbsp;
libev-devel-4.15-6.el7 | <div class="adv_e">[E]</div> <a href="https://access.redhat.com/errata/RHEA-2015:1797" target="secadv">RHEA-2015:1797</a> | &nbsp;
libev-libevent-devel-4.15-6.el7 | &nbsp; &nbsp; | &nbsp;
libev-source-4.15-6.el7 | &nbsp; &nbsp; | &nbsp;
python-websocket-client-0.32.0-115.el7 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2015:1782" target="secadv">RHBA-2015:1782</a> | &nbsp;
