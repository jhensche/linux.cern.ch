## 2020-12-04


Package | Advisory | Notes
------- | -------- | -----
ansible-role-collect-logs-1.2.0-1.el7 | &nbsp; &nbsp; | &nbsp;
python2-cinder-tests-tempest-1.1.0-1.el7 | &nbsp; &nbsp; | &nbsp;

