## 2022-02-08


Package | Advisory | Notes
------- | -------- | -----
log4j-1.2.17-18.el7_4 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2022:0442" target="secadv">RHSA-2022:0442</a> | &nbsp;
log4j-javadoc-1.2.17-18.el7_4 | &nbsp; &nbsp; | &nbsp;
log4j-manual-1.2.17-18.el7_4 | &nbsp; &nbsp; | &nbsp;

