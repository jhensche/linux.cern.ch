## 2014-10-27

Package | Advisory | Notes
------- | -------- | -----
oracle-instantclient-basic-12.1-3.el7.cern | &nbsp; &nbsp; | &nbsp;
oracle-instantclient-devel-12.1-3.el7.cern | &nbsp; &nbsp; | &nbsp;
docker-1.1.2-13.el7.centos | &nbsp; &nbsp; | &nbsp;
emacs-golang-1.3.3-1.el7.centos | &nbsp; &nbsp; | &nbsp;
golang-1.3.3-1.el7.centos | &nbsp; &nbsp; | &nbsp;
golang-github-coreos-go-systemd-devel-2-1.el7.centos | &nbsp; &nbsp; | &nbsp;
golang-github-godbus-dbus-devel-0-0.1.gitcb98efb.el7.centos | &nbsp; &nbsp; | &nbsp;
golang-github-gorilla-context-devel-0-0.23.gitb06ed15.el7.centos | &nbsp; &nbsp; | &nbsp;
golang-github-gorilla-mux-devel-0-0.13.git136d54f.el7.centos | &nbsp; &nbsp; | &nbsp;
golang-github-kr-pty-devel-0-0.19.git67e2db2.el7.centos | &nbsp; &nbsp; | &nbsp;
golang-github-syndtr-gocapability-devel-0-0.5.git3454319.el7.centos | &nbsp; &nbsp; | &nbsp;
golang-googlecode-net-devel-0-0.13.hg84a4013f96e0.el7.centos | &nbsp; &nbsp; | &nbsp;
golang-googlecode-sqlite-devel-0-0.9.hg74691fb6f837.el7.centos | &nbsp; &nbsp; | &nbsp;
golang-pkg-bin-linux-amd64-1.3.3-1.el7.centos | &nbsp; &nbsp; | &nbsp;
golang-pkg-darwin-386-1.3.3-1.el7.centos | &nbsp; &nbsp; | &nbsp;
golang-pkg-darwin-amd64-1.3.3-1.el7.centos | &nbsp; &nbsp; | &nbsp;
golang-pkg-freebsd-386-1.3.3-1.el7.centos | &nbsp; &nbsp; | &nbsp;
golang-pkg-freebsd-amd64-1.3.3-1.el7.centos | &nbsp; &nbsp; | &nbsp;
golang-pkg-freebsd-arm-1.3.3-1.el7.centos | &nbsp; &nbsp; | &nbsp;
golang-pkg-linux-386-1.3.3-1.el7.centos | &nbsp; &nbsp; | &nbsp;
golang-pkg-linux-amd64-1.3.3-1.el7.centos | &nbsp; &nbsp; | &nbsp;
golang-pkg-linux-arm-1.3.3-1.el7.centos | &nbsp; &nbsp; | &nbsp;
golang-pkg-netbsd-386-1.3.3-1.el7.centos | &nbsp; &nbsp; | &nbsp;
golang-pkg-netbsd-amd64-1.3.3-1.el7.centos | &nbsp; &nbsp; | &nbsp;
golang-pkg-netbsd-arm-1.3.3-1.el7.centos | &nbsp; &nbsp; | &nbsp;
golang-pkg-openbsd-386-1.3.3-1.el7.centos | &nbsp; &nbsp; | &nbsp;
golang-pkg-openbsd-amd64-1.3.3-1.el7.centos | &nbsp; &nbsp; | &nbsp;
golang-pkg-plan9-386-1.3.3-1.el7.centos | &nbsp; &nbsp; | &nbsp;
golang-pkg-plan9-amd64-1.3.3-1.el7.centos | &nbsp; &nbsp; | &nbsp;
golang-pkg-windows-386-1.3.3-1.el7.centos | &nbsp; &nbsp; | &nbsp;
golang-pkg-windows-amd64-1.3.3-1.el7.centos | &nbsp; &nbsp; | &nbsp;
golang-src-1.3.3-1.el7.centos | &nbsp; &nbsp; | &nbsp;
golang-vim-1.3.3-1.el7.centos | &nbsp; &nbsp; | &nbsp;
kexec-tools-2.0.4-32.el7.centos.5 | &nbsp; &nbsp; | &nbsp;
kexec-tools-eppic-2.0.4-32.el7.centos.5 | &nbsp; &nbsp; | &nbsp;
libgudev1-208-11.el7_0.4 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2014:1701" target="secadv">RHBA-2014:1701</a> | &nbsp;
libgudev1-devel-208-11.el7_0.4 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2014:1701" target="secadv">RHBA-2014:1701</a> | &nbsp;
systemd-208-11.el7_0.4 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2014:1701" target="secadv">RHBA-2014:1701</a> | &nbsp;
systemd-devel-208-11.el7_0.4 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2014:1701" target="secadv">RHBA-2014:1701</a> | &nbsp;
systemd-journal-gateway-208-11.el7_0.4 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2014:1701" target="secadv">RHBA-2014:1701</a> | &nbsp;
systemd-libs-208-11.el7_0.4 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2014:1701" target="secadv">RHBA-2014:1701</a> | &nbsp;
systemd-python-208-11.el7_0.4 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2014:1701" target="secadv">RHBA-2014:1701</a> | &nbsp;
systemd-sysv-208-11.el7_0.4 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2014:1701" target="secadv">RHBA-2014:1701</a> | &nbsp;
