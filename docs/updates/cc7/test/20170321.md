## 2017-03-21

Package | Advisory | Notes
------- | -------- | -----
sclo-php56-php-imap-5.6.28-1.el7 | &nbsp; &nbsp; | &nbsp;
sclo-php56-php-mcrypt-5.6.28-1.el7 | &nbsp; &nbsp; | &nbsp;
sclo-php56-php-smbclient-0.9.0-1.el7 | &nbsp; &nbsp; | &nbsp;
sclo-php56-php-tidy-5.6.25-1.el7 | &nbsp; &nbsp; | &nbsp;
sclo-php70-php-imap-7.0.14-1.el7 | &nbsp; &nbsp; | &nbsp;
sclo-php70-php-mcrypt-7.0.16-1.el7 | &nbsp; &nbsp; | &nbsp;
sclo-php70-php-smbclient-0.9.0-1.el7 | &nbsp; &nbsp; | &nbsp;
sclo-php70-php-tidy-7.0.10-1.el7 | &nbsp; &nbsp; | &nbsp;
firefox-52.0-5.el7.centos | &nbsp; &nbsp; | &nbsp;
spice-html5-0.1.7-1.el7 | &nbsp; &nbsp; | &nbsp;
