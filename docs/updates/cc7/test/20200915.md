## 2020-09-15


Package | Advisory | Notes
------- | -------- | -----
openstack-cloudkitty-api-11.1.0-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-cloudkitty-api-9.0.1-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-cloudkitty-common-11.1.0-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-cloudkitty-common-9.0.1-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-cloudkitty-processor-11.1.0-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-cloudkitty-processor-9.0.1-1.el7 | &nbsp; &nbsp; | &nbsp;
python2-cloudkitty-tests-11.1.0-1.el7 | &nbsp; &nbsp; | &nbsp;
python2-cloudkitty-tests-9.0.1-1.el7 | &nbsp; &nbsp; | &nbsp;
dovecot-2.2.36-6.el7_8.1 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2020:3617" target="secadv">RHSA-2020:3617</a> | &nbsp;
dovecot-devel-2.2.36-6.el7_8.1 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2020:3617" target="secadv">RHSA-2020:3617</a> | &nbsp;
dovecot-mysql-2.2.36-6.el7_8.1 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2020:3617" target="secadv">RHSA-2020:3617</a> | &nbsp;
dovecot-pgsql-2.2.36-6.el7_8.1 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2020:3617" target="secadv">RHSA-2020:3617</a> | &nbsp;
dovecot-pigeonhole-2.2.36-6.el7_8.1 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2020:3617" target="secadv">RHSA-2020:3617</a> | &nbsp;
thunderbird-68.12.0-1.el7.centos | &nbsp; &nbsp; | &nbsp;

