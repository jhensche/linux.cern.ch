## 2020-10-16


Package | Advisory | Notes
------- | -------- | -----
xen-4.10.4.87.gf58caa40cd-2.el7 | &nbsp; &nbsp; | &nbsp;
xen-4.12.3.79.g0186e76a62-1.el7 | &nbsp; &nbsp; | &nbsp;
xen-devel-4.10.4.87.gf58caa40cd-2.el7 | &nbsp; &nbsp; | &nbsp;
xen-devel-4.12.3.79.g0186e76a62-1.el7 | &nbsp; &nbsp; | &nbsp;
xen-doc-4.10.4.87.gf58caa40cd-2.el7 | &nbsp; &nbsp; | &nbsp;
xen-doc-4.12.3.79.g0186e76a62-1.el7 | &nbsp; &nbsp; | &nbsp;
xen-hypervisor-4.10.4.87.gf58caa40cd-2.el7 | &nbsp; &nbsp; | &nbsp;
xen-hypervisor-4.12.3.79.g0186e76a62-1.el7 | &nbsp; &nbsp; | &nbsp;
xen-libs-4.10.4.87.gf58caa40cd-2.el7 | &nbsp; &nbsp; | &nbsp;
xen-libs-4.12.3.79.g0186e76a62-1.el7 | &nbsp; &nbsp; | &nbsp;
xen-licenses-4.10.4.87.gf58caa40cd-2.el7 | &nbsp; &nbsp; | &nbsp;
xen-licenses-4.12.3.79.g0186e76a62-1.el7 | &nbsp; &nbsp; | &nbsp;
xen-livepatch-build-tools-4.10.4.87.gf58caa40cd-2.el7 | &nbsp; &nbsp; | &nbsp;
xen-livepatch-build-tools-4.12.3.79.g0186e76a62-1.el7 | &nbsp; &nbsp; | &nbsp;
xen-ocaml-4.10.4.87.gf58caa40cd-2.el7 | &nbsp; &nbsp; | &nbsp;
xen-ocaml-4.12.3.79.g0186e76a62-1.el7 | &nbsp; &nbsp; | &nbsp;
xen-ocaml-devel-4.10.4.87.gf58caa40cd-2.el7 | &nbsp; &nbsp; | &nbsp;
xen-ocaml-devel-4.12.3.79.g0186e76a62-1.el7 | &nbsp; &nbsp; | &nbsp;
xen-runtime-4.10.4.87.gf58caa40cd-2.el7 | &nbsp; &nbsp; | &nbsp;
xen-runtime-4.12.3.79.g0186e76a62-1.el7 | &nbsp; &nbsp; | &nbsp;

