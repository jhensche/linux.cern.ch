## 2018-07-12

Package | Advisory | Notes
------- | -------- | -----
glusterfs-3.12.11-1.el7 | &nbsp; &nbsp; | &nbsp;
glusterfs-api-3.12.11-1.el7 | &nbsp; &nbsp; | &nbsp;
glusterfs-api-devel-3.12.11-1.el7 | &nbsp; &nbsp; | &nbsp;
glusterfs-cli-3.12.11-1.el7 | &nbsp; &nbsp; | &nbsp;
glusterfs-client-xlators-3.12.11-1.el7 | &nbsp; &nbsp; | &nbsp;
glusterfs-devel-3.12.11-1.el7 | &nbsp; &nbsp; | &nbsp;
glusterfs-events-3.12.11-1.el7 | &nbsp; &nbsp; | &nbsp;
glusterfs-extra-xlators-3.12.11-1.el7 | &nbsp; &nbsp; | &nbsp;
glusterfs-fuse-3.12.11-1.el7 | &nbsp; &nbsp; | &nbsp;
glusterfs-geo-replication-3.12.11-1.el7 | &nbsp; &nbsp; | &nbsp;
glusterfs-gnfs-3.12.11-1.el7 | &nbsp; &nbsp; | &nbsp;
glusterfs-libs-3.12.11-1.el7 | &nbsp; &nbsp; | &nbsp;
glusterfs-rdma-3.12.11-1.el7 | &nbsp; &nbsp; | &nbsp;
glusterfs-resource-agents-3.12.11-1.el7 | &nbsp; &nbsp; | &nbsp;
glusterfs-server-3.12.11-1.el7 | &nbsp; &nbsp; | &nbsp;
python2-gluster-3.12.11-1.el7 | &nbsp; &nbsp; | &nbsp;
firefox-60.1.0-4.el7.centos | &nbsp; &nbsp; | &nbsp;
