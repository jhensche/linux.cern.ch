## 2017-02-23

Package | Advisory | Notes
------- | -------- | -----
kmod-openafs-1.6.9-2.3.10.0_514.6.2.el7 | &nbsp; &nbsp; | &nbsp;
ansible-2.2.1.0-1.el7 | &nbsp; &nbsp; | &nbsp;
apiextractor-0.10.10-11.el7 | &nbsp; &nbsp; | &nbsp;
apiextractor-devel-0.10.10-11.el7 | &nbsp; &nbsp; | &nbsp;
boost159-1.59.0-2.el7 | &nbsp; &nbsp; | &nbsp;
boost159-atomic-1.59.0-2.el7 | &nbsp; &nbsp; | &nbsp;
boost159-build-1.59.0-2.el7 | &nbsp; &nbsp; | &nbsp;
boost159-chrono-1.59.0-2.el7 | &nbsp; &nbsp; | &nbsp;
boost159-container-1.59.0-2.el7 | &nbsp; &nbsp; | &nbsp;
boost159-context-1.59.0-2.el7 | &nbsp; &nbsp; | &nbsp;
boost159-coroutine-1.59.0-2.el7 | &nbsp; &nbsp; | &nbsp;
boost159-date-time-1.59.0-2.el7 | &nbsp; &nbsp; | &nbsp;
boost159-devel-1.59.0-2.el7 | &nbsp; &nbsp; | &nbsp;
boost159-doc-1.59.0-2.el7 | &nbsp; &nbsp; | &nbsp;
boost159-doctools-1.59.0-2.el7 | &nbsp; &nbsp; | &nbsp;
boost159-examples-1.59.0-2.el7 | &nbsp; &nbsp; | &nbsp;
boost159-filesystem-1.59.0-2.el7 | &nbsp; &nbsp; | &nbsp;
boost159-graph-1.59.0-2.el7 | &nbsp; &nbsp; | &nbsp;
boost159-graph-mpich-1.59.0-2.el7 | &nbsp; &nbsp; | &nbsp;
boost159-graph-openmpi-1.59.0-2.el7 | &nbsp; &nbsp; | &nbsp;
boost159-iostreams-1.59.0-2.el7 | &nbsp; &nbsp; | &nbsp;
boost159-jam-1.59.0-2.el7 | &nbsp; &nbsp; | &nbsp;
boost159-locale-1.59.0-2.el7 | &nbsp; &nbsp; | &nbsp;
boost159-log-1.59.0-2.el7 | &nbsp; &nbsp; | &nbsp;
boost159-math-1.59.0-2.el7 | &nbsp; &nbsp; | &nbsp;
boost159-mpich-1.59.0-2.el7 | &nbsp; &nbsp; | &nbsp;
boost159-mpich-devel-1.59.0-2.el7 | &nbsp; &nbsp; | &nbsp;
boost159-mpich-python-1.59.0-2.el7 | &nbsp; &nbsp; | &nbsp;
boost159-openmpi-1.59.0-2.el7 | &nbsp; &nbsp; | &nbsp;
boost159-openmpi-devel-1.59.0-2.el7 | &nbsp; &nbsp; | &nbsp;
boost159-openmpi-python-1.59.0-2.el7 | &nbsp; &nbsp; | &nbsp;
boost159-program-options-1.59.0-2.el7 | &nbsp; &nbsp; | &nbsp;
boost159-python-1.59.0-2.el7 | &nbsp; &nbsp; | &nbsp;
boost159-random-1.59.0-2.el7 | &nbsp; &nbsp; | &nbsp;
boost159-regex-1.59.0-2.el7 | &nbsp; &nbsp; | &nbsp;
boost159-serialization-1.59.0-2.el7 | &nbsp; &nbsp; | &nbsp;
boost159-signals-1.59.0-2.el7 | &nbsp; &nbsp; | &nbsp;
boost159-static-1.59.0-2.el7 | &nbsp; &nbsp; | &nbsp;
boost159-system-1.59.0-2.el7 | &nbsp; &nbsp; | &nbsp;
boost159-test-1.59.0-2.el7 | &nbsp; &nbsp; | &nbsp;
boost159-thread-1.59.0-2.el7 | &nbsp; &nbsp; | &nbsp;
boost159-timer-1.59.0-2.el7 | &nbsp; &nbsp; | &nbsp;
boost159-wave-1.59.0-2.el7 | &nbsp; &nbsp; | &nbsp;
coin-or-Cbc-2.9.8-1.el7 | &nbsp; &nbsp; | &nbsp;
coin-or-Cbc-devel-2.9.8-1.el7 | &nbsp; &nbsp; | &nbsp;
coin-or-Cbc-doc-2.9.8-1.el7 | &nbsp; &nbsp; | &nbsp;
coin-or-Cgl-0.59.9-1.el7 | &nbsp; &nbsp; | &nbsp;
coin-or-Cgl-devel-0.59.9-1.el7 | &nbsp; &nbsp; | &nbsp;
coin-or-Cgl-doc-0.59.9-1.el7 | &nbsp; &nbsp; | &nbsp;
coin-or-Clp-1.16.10-1.el7 | &nbsp; &nbsp; | &nbsp;
coin-or-Clp-devel-1.16.10-1.el7 | &nbsp; &nbsp; | &nbsp;
coin-or-Clp-doc-1.16.10-1.el7 | &nbsp; &nbsp; | &nbsp;
coin-or-CoinUtils-2.10.13-1.el7 | &nbsp; &nbsp; | &nbsp;
coin-or-CoinUtils-devel-2.10.13-1.el7 | &nbsp; &nbsp; | &nbsp;
coin-or-CoinUtils-doc-2.10.13-1.el7 | &nbsp; &nbsp; | &nbsp;
coin-or-Osi-0.107.8-1.el7 | &nbsp; &nbsp; | &nbsp;
coin-or-Osi-devel-0.107.8-1.el7 | &nbsp; &nbsp; | &nbsp;
coin-or-Osi-doc-0.107.8-1.el7 | &nbsp; &nbsp; | &nbsp;
coin-or-Sample-1.2.10-5.el7 | &nbsp; &nbsp; | &nbsp;
diskimage-builder-1.14.1-1.el7 | &nbsp; &nbsp; | &nbsp;
dulwich-core-0.16.1-1.el7 | &nbsp; &nbsp; | &nbsp;
facter-2.4.4-4.el7 | &nbsp; &nbsp; | &nbsp;
generatorrunner-0.6.16-10.el7 | &nbsp; &nbsp; | &nbsp;
generatorrunner-devel-0.6.16-10.el7 | &nbsp; &nbsp; | &nbsp;
keycloak-httpd-client-install-0.5-1.el7 | &nbsp; &nbsp; | &nbsp;
liberasurecode-1.4.0-1.el7 | &nbsp; &nbsp; | &nbsp;
liberasurecode-devel-1.4.0-1.el7 | &nbsp; &nbsp; | &nbsp;
liberasurecode-doc-1.4.0-1.el7 | &nbsp; &nbsp; | &nbsp;
libimagequant-2.8.2-2.el7 | &nbsp; &nbsp; | &nbsp;
libimagequant-devel-2.8.2-2.el7 | &nbsp; &nbsp; | &nbsp;
librdkafka-0.9.2-2.el7 | &nbsp; &nbsp; | &nbsp;
librdkafka-devel-0.9.2-2.el7 | &nbsp; &nbsp; | &nbsp;
mariadb-10.1.20-1.el7 | &nbsp; &nbsp; | &nbsp;
mariadb-bench-10.1.20-1.el7 | &nbsp; &nbsp; | &nbsp;
mariadb-common-10.1.20-1.el7 | &nbsp; &nbsp; | &nbsp;
mariadb-config-10.1.20-1.el7 | &nbsp; &nbsp; | &nbsp;
mariadb-connect-engine-10.1.20-1.el7 | &nbsp; &nbsp; | &nbsp;
mariadb-devel-10.1.20-1.el7 | &nbsp; &nbsp; | &nbsp;
mariadb-embedded-10.1.20-1.el7 | &nbsp; &nbsp; | &nbsp;
mariadb-embedded-devel-10.1.20-1.el7 | &nbsp; &nbsp; | &nbsp;
mariadb-errmsg-10.1.20-1.el7 | &nbsp; &nbsp; | &nbsp;
mariadb-libs-10.1.20-1.el7 | &nbsp; &nbsp; | &nbsp;
mariadb-server-10.1.20-1.el7 | &nbsp; &nbsp; | &nbsp;
mariadb-server-galera-10.1.20-1.el7 | &nbsp; &nbsp; | &nbsp;
mariadb-test-10.1.20-1.el7 | &nbsp; &nbsp; | &nbsp;
openjpeg2-2.1.2-1.el7 | &nbsp; &nbsp; | &nbsp;
openjpeg2-devel-2.1.2-1.el7 | &nbsp; &nbsp; | &nbsp;
openjpeg2-devel-docs-2.1.2-1.el7 | &nbsp; &nbsp; | &nbsp;
openjpeg2-tools-2.1.2-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-heat-api-6.1.0-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-heat-api-cfn-6.1.0-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-heat-api-cloudwatch-6.1.0-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-heat-common-6.1.0-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-heat-engine-6.1.0-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-macros-2017.1.5-0 | &nbsp; &nbsp; | &nbsp;
perl-qpid-messaging-1.35.0-2.el7 | &nbsp; &nbsp; | &nbsp;
pyparsing-2.0.7-1.el7 | &nbsp; &nbsp; | &nbsp;
pyparsing-doc-2.0.7-1.el7 | &nbsp; &nbsp; | &nbsp;
PyQt4-4.12-1.el7 | &nbsp; &nbsp; | &nbsp;
PyQt4-devel-4.12-1.el7 | &nbsp; &nbsp; | &nbsp;
PyQt4-doc-4.12-1.el7 | &nbsp; &nbsp; | &nbsp;
PyQt4-qsci-api-4.12-1.el7 | &nbsp; &nbsp; | &nbsp;
python2-backports_abc-0.5-2.el7 | &nbsp; &nbsp; | &nbsp;
python2-bitmath-1.3.1-1.el7 | &nbsp; &nbsp; | &nbsp;
python2-bottle-0.12.9-1.el7 | &nbsp; &nbsp; | &nbsp;
python2-cachez-0.1.0-2.el7 | &nbsp; &nbsp; | &nbsp;
python2-cassandra-driver-3.7.1-4.el7 | &nbsp; &nbsp; | &nbsp;
python2-cycler-0.10.0-2.el7 | &nbsp; &nbsp; | &nbsp;
python2-Cython-0.25.2-3.el7 | &nbsp; &nbsp; | &nbsp;
python2-docopt-0.6.2-4.el7 | &nbsp; &nbsp; | &nbsp;
python2-dulwich-0.16.1-1.el7 | &nbsp; &nbsp; | &nbsp;
python2-fasteners-0.14.1-6.el7 | &nbsp; &nbsp; | &nbsp;
python2-functools32-3.2.3.2-1.el7 | &nbsp; &nbsp; | &nbsp;
python2-gevent-1.1.2-2.el7 | &nbsp; &nbsp; | &nbsp;
python2-influxdb-4.0.0-1.el7.1 | &nbsp; &nbsp; | &nbsp;
python2-keycloak-httpd-client-install-0.5-1.el7 | &nbsp; &nbsp; | &nbsp;
python2-matplotlib-2.0.0-1.el7 | &nbsp; &nbsp; | &nbsp;
python2-matplotlib-doc-2.0.0-1.el7 | &nbsp; &nbsp; | &nbsp;
python2-matplotlib-gtk-2.0.0-1.el7 | &nbsp; &nbsp; | &nbsp;
python2-matplotlib-gtk3-2.0.0-1.el7 | &nbsp; &nbsp; | &nbsp;
python2-matplotlib-qt4-2.0.0-1.el7 | &nbsp; &nbsp; | &nbsp;
python2-matplotlib-tk-2.0.0-1.el7 | &nbsp; &nbsp; | &nbsp;
python2-meld3-1.0.2-4.el7 | &nbsp; &nbsp; | &nbsp;
python2-numexpr-2.6.1-3.el7 | &nbsp; &nbsp; | &nbsp;
python2-numpy-1.11.2-2.el7 | &nbsp; &nbsp; | &nbsp;
python2-numpy-f2py-1.11.2-2.el7 | &nbsp; &nbsp; | &nbsp;
python2-olefile-0.44-1.el7 | &nbsp; &nbsp; | &nbsp;
python2-openstack-doc-tools-1.3.0-2.el7 | &nbsp; &nbsp; | &nbsp;
python2-packaging-16.8-2.el7 | &nbsp; &nbsp; | &nbsp;
python2-pandas-0.19.1-2.el7.2 | &nbsp; &nbsp; | &nbsp;
python2-passlib-1.7.0-1.el7 | &nbsp; &nbsp; | &nbsp;
python2-persist-queue-0.1.4-1.el7 | &nbsp; &nbsp; | &nbsp;
python2-pillow-4.0.0-1.el7 | &nbsp; &nbsp; | &nbsp;
python2-pillow-devel-4.0.0-1.el7 | &nbsp; &nbsp; | &nbsp;
python2-pillow-doc-4.0.0-1.el7 | &nbsp; &nbsp; | &nbsp;
python2-pillow-qt-4.0.0-1.el7 | &nbsp; &nbsp; | &nbsp;
python2-pillow-tk-4.0.0-1.el7 | &nbsp; &nbsp; | &nbsp;
python2-psutil-5.0.1-2.el7 | &nbsp; &nbsp; | &nbsp;
python2-PuLP-1.6.1-5.el7 | &nbsp; &nbsp; | &nbsp;
python2-py-1.4.31-3.el7 | &nbsp; &nbsp; | &nbsp;
python2-pysnmp-4.3.2-3.el7 | &nbsp; &nbsp; | &nbsp;
python2-pytest-2.9.2-1.el7 | &nbsp; &nbsp; | &nbsp;
python2-python-etcd-0.4.3-5.el7 | &nbsp; &nbsp; | &nbsp;
python2-queuelib-1.4.2-4.el7 | &nbsp; &nbsp; | &nbsp;
python2-requests-2.11.1-1.el7 | &nbsp; &nbsp; | &nbsp;
python2-retryz-0.1.8-1.el7 | &nbsp; &nbsp; | &nbsp;
python2-statsd-3.2.1-5.el7 | &nbsp; &nbsp; | &nbsp;
python2-sure-1.4.0-1.el7 | &nbsp; &nbsp; | &nbsp;
python2-tinyrpc-0.5-2.el7 | &nbsp; &nbsp; | &nbsp;
python2-tinyrpc-tests-0.5-2.el7 | &nbsp; &nbsp; | &nbsp;
python2-tornado-4.4.2-2.el7 | &nbsp; &nbsp; | &nbsp;
python2-ujson-1.35-1.el7 | &nbsp; &nbsp; | &nbsp;
python2-urllib3-1.16-1.el7 | &nbsp; &nbsp; | &nbsp;
python2-weakrefmethod-1.0.2-3.el7 | &nbsp; &nbsp; | &nbsp;
python2-wsme-0.8.0-1.el7 | &nbsp; &nbsp; | &nbsp;
python2-XStatic-1.0.1-8.el7 | &nbsp; &nbsp; | &nbsp;
python-blist-1.3.6-9.el7 | &nbsp; &nbsp; | &nbsp;
python-cairocffi-0.7.2-7.el7 | &nbsp; &nbsp; | &nbsp;
python-cairosvg-1.0.20-3.el7 | &nbsp; &nbsp; | &nbsp;
python-cassandra-driver-doc-3.7.1-4.el7 | &nbsp; &nbsp; | &nbsp;
python-construct-2.8.10-1.el7 | &nbsp; &nbsp; | &nbsp;
python-daemon-1.6-4.el7 | &nbsp; &nbsp; | &nbsp;
python-fastcache-1.0.2-3.el7 | &nbsp; &nbsp; | &nbsp;
python-freezegun-0.3.8-2.el7 | &nbsp; &nbsp; | &nbsp;
python-gmpy2-2.0.5-1.el7 | &nbsp; &nbsp; | &nbsp;
python-heat-tests-6.1.0-1.el7 | &nbsp; &nbsp; | &nbsp;
python-matplotlib-data-2.0.0-1.el7 | &nbsp; &nbsp; | &nbsp;
python-matplotlib-data-fonts-2.0.0-1.el7 | &nbsp; &nbsp; | &nbsp;
python-meld3-1.0.2-4.el7 | &nbsp; &nbsp; | &nbsp;
python-mpmath-0.19-2.el7 | &nbsp; &nbsp; | &nbsp;
python-mpmath-doc-0.19-2.el7 | &nbsp; &nbsp; | &nbsp;
python-openstack-doc-tools-doc-1.3.0-2.el7 | &nbsp; &nbsp; | &nbsp;
python-packaging-doc-16.8-2.el7 | &nbsp; &nbsp; | &nbsp;
python-pycxx-devel-6.2.8-3.el7 | &nbsp; &nbsp; | &nbsp;
python-pyeclib-1.4.0-1.el7 | &nbsp; &nbsp; | &nbsp;
python-pygal-2.3.0-2.el7 | &nbsp; &nbsp; | &nbsp;
python-pyglet-1.2.1-2.el7 | &nbsp; &nbsp; | &nbsp;
python-pyside-1.2.2-6.el7.1 | &nbsp; &nbsp; | &nbsp;
python-pyside-devel-1.2.2-6.el7.1 | &nbsp; &nbsp; | &nbsp;
python-qpid-messaging-1.35.0-2.el7 | &nbsp; &nbsp; | &nbsp;
python-qpid-proton-0.14.0-2.el7 | &nbsp; &nbsp; | &nbsp;
python-qpid-proton-docs-0.14.0-2.el7 | &nbsp; &nbsp; | &nbsp;
python-scales-1.0.5-8.el7 | &nbsp; &nbsp; | &nbsp;
python-statsd-doc-3.2.1-5.el7 | &nbsp; &nbsp; | &nbsp;
python-subprocess32-3.2.6-4.el7 | &nbsp; &nbsp; | &nbsp;
python-tinyrpc-doc-0.5-2.el7 | &nbsp; &nbsp; | &nbsp;
python-tornado-doc-4.4.2-2.el7 | &nbsp; &nbsp; | &nbsp;
python-xcffib-0.4.2-2.el7 | &nbsp; &nbsp; | &nbsp;
qpid-cpp-client-1.35.0-2.el7 | &nbsp; &nbsp; | &nbsp;
qpid-cpp-client-devel-1.35.0-2.el7 | &nbsp; &nbsp; | &nbsp;
qpid-cpp-client-devel-docs-1.35.0-2.el7 | &nbsp; &nbsp; | &nbsp;
qpid-cpp-client-rdma-1.35.0-2.el7 | &nbsp; &nbsp; | &nbsp;
qpid-cpp-server-1.35.0-2.el7 | &nbsp; &nbsp; | &nbsp;
qpid-cpp-server-devel-1.35.0-2.el7 | &nbsp; &nbsp; | &nbsp;
qpid-cpp-server-ha-1.35.0-2.el7 | &nbsp; &nbsp; | &nbsp;
qpid-cpp-server-linearstore-1.35.0-2.el7 | &nbsp; &nbsp; | &nbsp;
qpid-cpp-server-rdma-1.35.0-2.el7 | &nbsp; &nbsp; | &nbsp;
qpid-cpp-server-xml-1.35.0-2.el7 | &nbsp; &nbsp; | &nbsp;
qpid-dispatch-docs-0.6.1-4.el7 | &nbsp; &nbsp; | &nbsp;
qpid-dispatch-router-0.6.1-4.el7 | &nbsp; &nbsp; | &nbsp;
qpid-dispatch-tools-0.6.1-4.el7 | &nbsp; &nbsp; | &nbsp;
qpid-proton-c-0.14.0-2.el7 | &nbsp; &nbsp; | &nbsp;
qpid-proton-c-devel-0.14.0-2.el7 | &nbsp; &nbsp; | &nbsp;
qpid-proton-c-docs-0.14.0-2.el7 | &nbsp; &nbsp; | &nbsp;
qpid-proton-cpp-0.14.0-2.el7 | &nbsp; &nbsp; | &nbsp;
qpid-proton-cpp-devel-0.14.0-2.el7 | &nbsp; &nbsp; | &nbsp;
qpid-proton-cpp-docs-0.14.0-2.el7 | &nbsp; &nbsp; | &nbsp;
redis-3.2.8-1.el7 | &nbsp; &nbsp; | &nbsp;
shiboken-1.2.4-2.el7 | &nbsp; &nbsp; | &nbsp;
shiboken-devel-1.2.4-2.el7 | &nbsp; &nbsp; | &nbsp;
shiboken-libs-1.2.4-2.el7 | &nbsp; &nbsp; | &nbsp;
sip-4.19-1.el7 | &nbsp; &nbsp; | &nbsp;
sip-devel-4.19-1.el7 | &nbsp; &nbsp; | &nbsp;
sip-macros-4.19-1.el7 | &nbsp; &nbsp; | &nbsp;
sparsehash-devel-2.0.2-4.el7 | &nbsp; &nbsp; | &nbsp;
supervisor-3.3.1-2.el7 | &nbsp; &nbsp; | &nbsp;
sympy-0.7.6-2.el7 | &nbsp; &nbsp; | &nbsp;
sympy-doc-0.7.6-2.el7 | &nbsp; &nbsp; | &nbsp;
sympy-examples-0.7.6-2.el7 | &nbsp; &nbsp; | &nbsp;
sympy-texmacs-0.7.6-2.el7 | &nbsp; &nbsp; | &nbsp;
firefox-45.7.0-2.el7.centos | &nbsp; &nbsp; | &nbsp;
kernel-3.10.0-514.6.2.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2017:0294" target="secadv">RHSA-2017:0294</a> | &nbsp;
kernel-abi-whitelists-3.10.0-514.6.2.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2017:0294" target="secadv">RHSA-2017:0294</a> | &nbsp;
kernel-debug-3.10.0-514.6.2.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2017:0294" target="secadv">RHSA-2017:0294</a> | &nbsp;
kernel-debug-devel-3.10.0-514.6.2.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2017:0294" target="secadv">RHSA-2017:0294</a> | &nbsp;
kernel-devel-3.10.0-514.6.2.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2017:0294" target="secadv">RHSA-2017:0294</a> | &nbsp;
kernel-doc-3.10.0-514.6.2.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2017:0294" target="secadv">RHSA-2017:0294</a> | &nbsp;
kernel-headers-3.10.0-514.6.2.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2017:0294" target="secadv">RHSA-2017:0294</a> | &nbsp;
kernel-tools-3.10.0-514.6.2.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2017:0294" target="secadv">RHSA-2017:0294</a> | &nbsp;
kernel-tools-libs-3.10.0-514.6.2.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2017:0294" target="secadv">RHSA-2017:0294</a> | &nbsp;
kernel-tools-libs-devel-3.10.0-514.6.2.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2017:0294" target="secadv">RHSA-2017:0294</a> | &nbsp;
perf-3.10.0-514.6.2.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2017:0294" target="secadv">RHSA-2017:0294</a> | &nbsp;
python-perf-3.10.0-514.6.2.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2017:0294" target="secadv">RHSA-2017:0294</a> | &nbsp;
libcacard-2.5.2-2.1.el7 | &nbsp; &nbsp; | &nbsp;
libcacard-devel-2.5.2-2.1.el7 | &nbsp; &nbsp; | &nbsp;
libcacard-tools-2.5.2-2.1.el7 | &nbsp; &nbsp; | &nbsp;
