## 2016-03-09

Package | Advisory | Notes
------- | -------- | -----
aims2-client-2.13.2-1.el7.cern | &nbsp; &nbsp; | &nbsp;
aims2-server-2.13.2-1.el7.cern | &nbsp; &nbsp; | &nbsp;
firefox-38.7.0-1.el7.centos | &nbsp; &nbsp; | &nbsp;
nss-util-3.19.1-9.el7_2 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2016:0370" target="secadv">RHSA-2016:0370</a> | &nbsp;
nss-util-devel-3.19.1-9.el7_2 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2016:0370" target="secadv">RHSA-2016:0370</a> | &nbsp;
openssl098e-0.9.8e-29.el7.centos.3 | &nbsp; &nbsp; | &nbsp;
