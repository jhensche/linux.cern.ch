## 2019-11-26

Package | Advisory | Notes
------- | -------- | -----
cern-get-sso-cookie-0.6-2.el7.cern | &nbsp; &nbsp; | &nbsp;
perl-Authen-Krb5-1.9-3.el7.cern | &nbsp; &nbsp; | &nbsp;
perl-WWW-CERNSSO-Auth-0.6-2.el7.cern | &nbsp; &nbsp; | &nbsp;
openstack-cinder-12.0.10-2.el7 | &nbsp; &nbsp; | &nbsp;
openstack-cinder-13.0.7-2.el7 | &nbsp; &nbsp; | &nbsp;
openstack-cinder-15.0.0-2.el7 | &nbsp; &nbsp; | &nbsp;
openstack-cinder-doc-12.0.10-2.el7 | &nbsp; &nbsp; | &nbsp;
openstack-cinder-doc-13.0.7-2.el7 | &nbsp; &nbsp; | &nbsp;
puppet-stdlib-4.25.1-1.2f85336git.el7 | &nbsp; &nbsp; | &nbsp;
python2-cinder-15.0.0-2.el7 | &nbsp; &nbsp; | &nbsp;
python2-cinder-tests-15.0.0-2.el7 | &nbsp; &nbsp; | &nbsp;
python2-mistral-lib-1.0.1-1.el7 | &nbsp; &nbsp; | &nbsp;
python2-mistral-lib-1.1.1-1.el7 | &nbsp; &nbsp; | &nbsp;
python2-mistral-lib-1.2.1-1.el7 | &nbsp; &nbsp; | &nbsp;
python2-mistral-lib-tests-1.0.1-1.el7 | &nbsp; &nbsp; | &nbsp;
python2-mistral-lib-tests-1.1.1-1.el7 | &nbsp; &nbsp; | &nbsp;
python2-mistral-lib-tests-1.2.1-1.el7 | &nbsp; &nbsp; | &nbsp;
python2-novaclient-11.0.1-1.el7 | &nbsp; &nbsp; | &nbsp;
python2-novaclient-13.0.2-1.el7 | &nbsp; &nbsp; | &nbsp;
python2-oslo-utils-3.41.3-1.el7 | &nbsp; &nbsp; | &nbsp;
python2-oslo-utils-tests-3.41.3-1.el7 | &nbsp; &nbsp; | &nbsp;
python2-os-vif-1.11.2-1.el7 | &nbsp; &nbsp; | &nbsp;
python2-os-vif-tests-1.11.2-1.el7 | &nbsp; &nbsp; | &nbsp;
python-cinder-12.0.10-2.el7 | &nbsp; &nbsp; | &nbsp;
python-cinder-13.0.7-2.el7 | &nbsp; &nbsp; | &nbsp;
python-cinder-tests-12.0.10-2.el7 | &nbsp; &nbsp; | &nbsp;
python-cinder-tests-13.0.7-2.el7 | &nbsp; &nbsp; | &nbsp;
python-mistral-lib-doc-1.0.1-1.el7 | &nbsp; &nbsp; | &nbsp;
python-mistral-lib-doc-1.1.1-1.el7 | &nbsp; &nbsp; | &nbsp;
python-mistral-lib-doc-1.2.1-1.el7 | &nbsp; &nbsp; | &nbsp;
python-novaclient-doc-11.0.1-1.el7 | &nbsp; &nbsp; | &nbsp;
python-novaclient-doc-13.0.2-1.el7 | &nbsp; &nbsp; | &nbsp;
python-oslo-utils-doc-3.41.3-1.el7 | &nbsp; &nbsp; | &nbsp;
python-oslo-utils-lang-3.41.3-1.el7 | &nbsp; &nbsp; | &nbsp;
python-os-vif-doc-1.11.2-1.el7 | &nbsp; &nbsp; | &nbsp;
ceph-ansible-4.0.5-1.el7 | &nbsp; &nbsp; | &nbsp;
