## 2020-07-23


Package | Advisory | Notes
------- | -------- | -----
openstack-cinder-14.2.1-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-cinder-doc-14.2.1-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-dashboard-15.3.1-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-dashboard-theme-15.3.1-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-ironic-api-12.1.5-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-ironic-common-12.1.5-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-ironic-conductor-12.1.5-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-kolla-9.2.0-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-manila-8.1.3-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-manila-doc-8.1.3-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-manila-share-8.1.3-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-neutron-lbaas-ui-6.0.1-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-neutron-lbaas-ui-doc-6.0.1-1.el7 | &nbsp; &nbsp; | &nbsp;
python2-cinder-14.2.1-1.el7 | &nbsp; &nbsp; | &nbsp;
python2-cinder-tests-14.2.1-1.el7 | &nbsp; &nbsp; | &nbsp;
python2-django-horizon-15.3.1-1.el7 | &nbsp; &nbsp; | &nbsp;
python2-ironic-tests-12.1.5-1.el7 | &nbsp; &nbsp; | &nbsp;
python2-manila-8.1.3-1.el7 | &nbsp; &nbsp; | &nbsp;
python2-manila-tests-8.1.3-1.el7 | &nbsp; &nbsp; | &nbsp;
python2-openstacksdk-0.36.4-1.el7 | &nbsp; &nbsp; | &nbsp;
python2-openstacksdk-tests-0.36.4-1.el7 | &nbsp; &nbsp; | &nbsp;
python2-sahara-plugin-mapr-1.0.2-1.el7 | &nbsp; &nbsp; | &nbsp;
python2-sahara-plugin-mapr-tests-unit-1.0.2-1.el7 | &nbsp; &nbsp; | &nbsp;
python-django-horizon-doc-15.3.1-1.el7 | &nbsp; &nbsp; | &nbsp;
python-sahara-plugin-mapr-doc-1.0.2-1.el7 | &nbsp; &nbsp; | &nbsp;
sclo-php72-php-pecl-mongodb-1.7.5-1.el7 | &nbsp; &nbsp; | &nbsp;
sclo-php72-php-pecl-redis5-5.3.1-1.el7 | &nbsp; &nbsp; | &nbsp;
sclo-php73-php-pecl-mongodb-1.7.5-1.el7 | &nbsp; &nbsp; | &nbsp;
sclo-php73-php-pecl-redis5-5.3.1-1.el7 | &nbsp; &nbsp; | &nbsp;

