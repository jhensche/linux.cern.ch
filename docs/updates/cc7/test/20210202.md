## 2021-02-02


Package | Advisory | Notes
------- | -------- | -----
thunderbird-78.7.0-1.el7.centos.plus | &nbsp; &nbsp; | &nbsp;
openstack-neutron-15.3.1-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-neutron-common-15.3.1-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-neutron-linuxbridge-15.3.1-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-neutron-macvtap-agent-15.3.1-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-neutron-metering-agent-15.3.1-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-neutron-ml2-15.3.1-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-neutron-openvswitch-15.3.1-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-neutron-rpc-server-15.3.1-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-neutron-sriov-nic-agent-15.3.1-1.el7 | &nbsp; &nbsp; | &nbsp;
python-tempestconf-doc-3.1.0-1.el7 | &nbsp; &nbsp; | &nbsp;
python2-neutron-15.3.1-1.el7 | &nbsp; &nbsp; | &nbsp;
python2-neutron-tests-15.3.1-1.el7 | &nbsp; &nbsp; | &nbsp;
python2-tempestconf-3.1.0-1.el7 | &nbsp; &nbsp; | &nbsp;
python2-tempestconf-tests-3.1.0-1.el7 | &nbsp; &nbsp; | &nbsp;
firefox-78.7.0-2.el7.centos | &nbsp; &nbsp; | &nbsp;
firefox-78.7.0-2.el7.centos.aarch64.rpm | &nbsp; &nbsp; | &nbsp;
thunderbird-78.7.0-1.el7.centos | &nbsp; &nbsp; | &nbsp;

