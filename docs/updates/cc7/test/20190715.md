## 2019-07-15

Package | Advisory | Notes
------- | -------- | -----
libgudev1-219-62.el7.7.0.1 | &nbsp; &nbsp; | &nbsp;
libgudev1-devel-219-62.el7.7.0.1 | &nbsp; &nbsp; | &nbsp;
systemd-219-62.el7.7.0.1 | &nbsp; &nbsp; | &nbsp;
systemd-devel-219-62.el7.7.0.1 | &nbsp; &nbsp; | &nbsp;
systemd-journal-gateway-219-62.el7.7.0.1 | &nbsp; &nbsp; | &nbsp;
systemd-libs-219-62.el7.7.0.1 | &nbsp; &nbsp; | &nbsp;
systemd-networkd-219-62.el7.7.0.1 | &nbsp; &nbsp; | &nbsp;
systemd-python-219-62.el7.7.0.1 | &nbsp; &nbsp; | &nbsp;
systemd-resolved-219-62.el7.7.0.1 | &nbsp; &nbsp; | &nbsp;
systemd-sysv-219-62.el7.7.0.1 | &nbsp; &nbsp; | &nbsp;
firefox-60.8.0-1.el7.centos | &nbsp; &nbsp; | &nbsp;
