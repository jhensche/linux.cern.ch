## 2016-11-16

Package | Advisory | Notes
------- | -------- | -----
cern-linuxsupport-access-0.7-1.el7.cern | &nbsp; &nbsp; | &nbsp;
flash-plugin-beta-24.0.0.145-0.beta.el7.cern | &nbsp; &nbsp; | &nbsp;
devtoolset-6-6.0-6.el7 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2016:2724" target="secadv">RHBA-2016:2724</a> | &nbsp;
devtoolset-6-build-6.0-6.el7 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2016:2724" target="secadv">RHBA-2016:2724</a> | &nbsp;
devtoolset-6-dockerfiles-6.0-6.el7 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2016:2724" target="secadv">RHBA-2016:2724</a> | &nbsp;
devtoolset-6-perftools-6.0-6.el7 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2016:2724" target="secadv">RHBA-2016:2724</a> | &nbsp;
devtoolset-6-runtime-6.0-6.el7 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2016:2724" target="secadv">RHBA-2016:2724</a> | &nbsp;
devtoolset-6-toolchain-6.0-6.el7 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2016:2724" target="secadv">RHBA-2016:2724</a> | &nbsp;
rh-git29-2.3-4.el7 | <div class="adv_e">[E]</div> <a href="https://access.redhat.com/errata/RHEA-2016:2728" target="secadv">RHEA-2016:2728</a> | &nbsp;
rh-git29-build-2.3-4.el7 | &nbsp; &nbsp; | &nbsp;
rh-git29-runtime-2.3-4.el7 | <div class="adv_e">[E]</div> <a href="https://access.redhat.com/errata/RHEA-2016:2728" target="secadv">RHEA-2016:2728</a> | &nbsp;
rh-git29-scldevel-2.3-4.el7 | <div class="adv_e">[E]</div> <a href="https://access.redhat.com/errata/RHEA-2016:2728" target="secadv">RHEA-2016:2728</a> | &nbsp;
rh-mysql57-2.3-2.el7 | &nbsp; &nbsp; | &nbsp;
rh-mysql57-build-2.3-2.el7 | &nbsp; &nbsp; | &nbsp;
rh-mysql57-runtime-2.3-2.el7 | &nbsp; &nbsp; | &nbsp;
rh-mysql57-scldevel-2.3-2.el7 | &nbsp; &nbsp; | &nbsp;
rh-perl524-2.3-2.el7 | <div class="adv_e">[E]</div> <a href="https://access.redhat.com/errata/RHEA-2016:2729" target="secadv">RHEA-2016:2729</a> | &nbsp;
rh-perl524-build-2.3-2.el7 | &nbsp; &nbsp; | &nbsp;
rh-perl524-runtime-2.3-2.el7 | <div class="adv_e">[E]</div> <a href="https://access.redhat.com/errata/RHEA-2016:2729" target="secadv">RHEA-2016:2729</a> | &nbsp;
rh-perl524-scldevel-2.3-2.el7 | <div class="adv_e">[E]</div> <a href="https://access.redhat.com/errata/RHEA-2016:2729" target="secadv">RHEA-2016:2729</a> | &nbsp;
rh-php70-2.3-1.el7 | <div class="adv_e">[E]</div> <a href="https://access.redhat.com/errata/RHEA-2016:2730" target="secadv">RHEA-2016:2730</a> | &nbsp;
rh-php70-build-2.3-1.el7 | &nbsp; &nbsp; | &nbsp;
rh-php70-runtime-2.3-1.el7 | <div class="adv_e">[E]</div> <a href="https://access.redhat.com/errata/RHEA-2016:2730" target="secadv">RHEA-2016:2730</a> | &nbsp;
rh-php70-scldevel-2.3-1.el7 | <div class="adv_e">[E]</div> <a href="https://access.redhat.com/errata/RHEA-2016:2730" target="secadv">RHEA-2016:2730</a> | &nbsp;
rh-redis32-2.3-1.el7 | <div class="adv_e">[E]</div> <a href="https://access.redhat.com/errata/RHEA-2016:2745" target="secadv">RHEA-2016:2745</a> | &nbsp;
rh-redis32-build-2.3-1.el7 | &nbsp; &nbsp; | &nbsp;
rh-redis32-runtime-2.3-1.el7 | <div class="adv_e">[E]</div> <a href="https://access.redhat.com/errata/RHEA-2016:2745" target="secadv">RHEA-2016:2745</a> | &nbsp;
rh-redis32-scldevel-2.3-1.el7 | <div class="adv_e">[E]</div> <a href="https://access.redhat.com/errata/RHEA-2016:2745" target="secadv">RHEA-2016:2745</a> | &nbsp;
imgbased-0.8.6-1.el7 | &nbsp; &nbsp; | &nbsp;
mom-0.5.8-1.el7 | &nbsp; &nbsp; | &nbsp;
ovirt-host-deploy-1.5.3-1.el7 | &nbsp; &nbsp; | &nbsp;
ovirt-host-deploy-java-1.5.3-1.el7 | &nbsp; &nbsp; | &nbsp;
ovirt-host-deploy-javadoc-1.5.3-1.el7 | &nbsp; &nbsp; | &nbsp;
ovirt-hosted-engine-setup-2.0.3-1.el7 | &nbsp; &nbsp; | &nbsp;
python-ovirt-engine-sdk4-4.0.2-1.el7 | &nbsp; &nbsp; | &nbsp;
vdsm-4.18.15.2-1.el7 | &nbsp; &nbsp; | &nbsp;
vdsm-api-4.18.15.2-1.el7 | &nbsp; &nbsp; | &nbsp;
vdsm-cli-4.18.15.2-1.el7 | &nbsp; &nbsp; | &nbsp;
vdsm-gluster-4.18.15.2-1.el7 | &nbsp; &nbsp; | &nbsp;
vdsm-hook-allocate_net-4.18.15.2-1.el7 | &nbsp; &nbsp; | &nbsp;
vdsm-hook-checkimages-4.18.15.2-1.el7 | &nbsp; &nbsp; | &nbsp;
vdsm-hook-checkips-4.18.15.2-1.el7 | &nbsp; &nbsp; | &nbsp;
vdsm-hook-diskunmap-4.18.15.2-1.el7 | &nbsp; &nbsp; | &nbsp;
vdsm-hook-ethtool-options-4.18.15.2-1.el7 | &nbsp; &nbsp; | &nbsp;
vdsm-hook-extnet-4.18.15.2-1.el7 | &nbsp; &nbsp; | &nbsp;
vdsm-hook-extra-ipv4-addrs-4.18.15.2-1.el7 | &nbsp; &nbsp; | &nbsp;
vdsm-hook-fakesriov-4.18.15.2-1.el7 | &nbsp; &nbsp; | &nbsp;
vdsm-hook-fakevmstats-4.18.15.2-1.el7 | &nbsp; &nbsp; | &nbsp;
vdsm-hook-faqemu-4.18.15.2-1.el7 | &nbsp; &nbsp; | &nbsp;
vdsm-hook-fcoe-4.18.15.2-1.el7 | &nbsp; &nbsp; | &nbsp;
vdsm-hook-fileinject-4.18.15.2-1.el7 | &nbsp; &nbsp; | &nbsp;
vdsm-hook-floppy-4.18.15.2-1.el7 | &nbsp; &nbsp; | &nbsp;
vdsm-hook-hostusb-4.18.15.2-1.el7 | &nbsp; &nbsp; | &nbsp;
vdsm-hook-httpsisoboot-4.18.15.2-1.el7 | &nbsp; &nbsp; | &nbsp;
vdsm-hook-hugepages-4.18.15.2-1.el7 | &nbsp; &nbsp; | &nbsp;
vdsm-hook-ipv6-4.18.15.2-1.el7 | &nbsp; &nbsp; | &nbsp;
vdsm-hook-isolatedprivatevlan-4.18.15.2-1.el7 | &nbsp; &nbsp; | &nbsp;
vdsm-hook-macbind-4.18.15.2-1.el7 | &nbsp; &nbsp; | &nbsp;
vdsm-hook-macspoof-4.18.15.2-1.el7 | &nbsp; &nbsp; | &nbsp;
vdsm-hook-nestedvt-4.18.15.2-1.el7 | &nbsp; &nbsp; | &nbsp;
vdsm-hook-noipspoof-4.18.15.2-1.el7 | &nbsp; &nbsp; | &nbsp;
vdsm-hook-numa-4.18.15.2-1.el7 | &nbsp; &nbsp; | &nbsp;
vdsm-hook-openstacknet-4.18.15.2-1.el7 | &nbsp; &nbsp; | &nbsp;
vdsm-hook-ovs-4.18.15.2-1.el7 | &nbsp; &nbsp; | &nbsp;
vdsm-hook-pincpu-4.18.15.2-1.el7 | &nbsp; &nbsp; | &nbsp;
vdsm-hook-promisc-4.18.15.2-1.el7 | &nbsp; &nbsp; | &nbsp;
vdsm-hook-qemucmdline-4.18.15.2-1.el7 | &nbsp; &nbsp; | &nbsp;
vdsm-hook-qos-4.18.15.2-1.el7 | &nbsp; &nbsp; | &nbsp;
vdsm-hook-scratchpad-4.18.15.2-1.el7 | &nbsp; &nbsp; | &nbsp;
vdsm-hook-smbios-4.18.15.2-1.el7 | &nbsp; &nbsp; | &nbsp;
vdsm-hook-spiceoptions-4.18.15.2-1.el7 | &nbsp; &nbsp; | &nbsp;
vdsm-hook-vhostmd-4.18.15.2-1.el7 | &nbsp; &nbsp; | &nbsp;
vdsm-hook-vmdisk-4.18.15.2-1.el7 | &nbsp; &nbsp; | &nbsp;
vdsm-hook-vmfex-4.18.15.2-1.el7 | &nbsp; &nbsp; | &nbsp;
vdsm-hook-vmfex-dev-4.18.15.2-1.el7 | &nbsp; &nbsp; | &nbsp;
vdsm-infra-4.18.15.2-1.el7 | &nbsp; &nbsp; | &nbsp;
vdsm-jsonrpc-4.18.15.2-1.el7 | &nbsp; &nbsp; | &nbsp;
vdsm-jsonrpc-java-1.2.7-1.el7 | &nbsp; &nbsp; | &nbsp;
vdsm-jsonrpc-java-javadoc-1.2.7-1.el7 | &nbsp; &nbsp; | &nbsp;
vdsm-python-4.18.15.2-1.el7 | &nbsp; &nbsp; | &nbsp;
vdsm-tests-4.18.15.2-1.el7 | &nbsp; &nbsp; | &nbsp;
vdsm-xmlrpc-4.18.15.2-1.el7 | &nbsp; &nbsp; | &nbsp;
vdsm-yajsonrpc-4.18.15.2-1.el7 | &nbsp; &nbsp; | &nbsp;
