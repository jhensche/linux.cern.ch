## 2018-07-24

Package | Advisory | Notes
------- | -------- | -----
sclo-php70-php-pecl-apcu-5.1.12-1.el7 | &nbsp; &nbsp; | &nbsp;
sclo-php70-php-pecl-apcu-devel-5.1.12-1.el7 | &nbsp; &nbsp; | &nbsp;
sclo-php70-php-pecl-mongodb-1.5.1-1.el7 | &nbsp; &nbsp; | &nbsp;
sclo-php70-php-pecl-redis4-4.1.0-1.el7 | &nbsp; &nbsp; | &nbsp;
sclo-php71-php-pecl-mongodb-1.5.1-1.el7 | &nbsp; &nbsp; | &nbsp;
sclo-php71-php-pecl-redis4-4.1.0-1.el7 | &nbsp; &nbsp; | &nbsp;
