## 2015-07-15

Package | Advisory | Notes
------- | -------- | -----
java-1.8.0-oracle-1.8.0.51-1.0.el7.cern | &nbsp; &nbsp; | &nbsp;
java-1.8.0-oracle-devel-1.8.0.51-1.0.el7.cern | &nbsp; &nbsp; | &nbsp;
java-1.8.0-oracle-headless-1.8.0.51-1.0.el7.cern | &nbsp; &nbsp; | &nbsp;
java-1.8.0-oracle-javafx-1.8.0.51-1.0.el7.cern | &nbsp; &nbsp; | &nbsp;
java-1.8.0-oracle-plugin-1.8.0.51-1.0.el7.cern | &nbsp; &nbsp; | &nbsp;
java-1.8.0-oracle-src-1.8.0.51-1.0.el7.cern | &nbsp; &nbsp; | &nbsp;
