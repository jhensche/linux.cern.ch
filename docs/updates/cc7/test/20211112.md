## 2021-11-12


Package | Advisory | Notes
------- | -------- | -----
freerdp-2.1.1-5.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2021:4619" target="secadv">RHSA-2021:4619</a> | &nbsp;
freerdp-devel-2.1.1-5.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2021:4619" target="secadv">RHSA-2021:4619</a> | &nbsp;
freerdp-libs-2.1.1-5.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2021:4619" target="secadv">RHSA-2021:4619</a> | &nbsp;
httpd-2.4.6-97.el7.centos.2 | &nbsp; &nbsp; | &nbsp;
httpd-devel-2.4.6-97.el7.centos.2 | &nbsp; &nbsp; | &nbsp;
httpd-manual-2.4.6-97.el7.centos.2 | &nbsp; &nbsp; | &nbsp;
httpd-tools-2.4.6-97.el7.centos.2 | &nbsp; &nbsp; | &nbsp;
libwinpr-2.1.1-5.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2021:4619" target="secadv">RHSA-2021:4619</a> | &nbsp;
libwinpr-devel-2.1.1-5.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2021:4619" target="secadv">RHSA-2021:4619</a> | &nbsp;
mod_ldap-2.4.6-97.el7.centos.2 | &nbsp; &nbsp; | &nbsp;
mod_proxy_html-2.4.6-97.el7.centos.2 | &nbsp; &nbsp; | &nbsp;
mod_session-2.4.6-97.el7.centos.2 | &nbsp; &nbsp; | &nbsp;
mod_ssl-2.4.6-97.el7.centos.2 | &nbsp; &nbsp; | &nbsp;

