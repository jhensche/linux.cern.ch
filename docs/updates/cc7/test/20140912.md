## 2014-09-12

Package | Advisory | Notes
------- | -------- | -----
fsplit-5.5-3.el7.cern | &nbsp; &nbsp; | &nbsp;
cx_Oracle-5.1.3-2.el7.cern | &nbsp; &nbsp; | &nbsp;
flash-plugin-11.2.202.400-1.el7.cern | &nbsp; &nbsp; | &nbsp;
hpglview-543-1.el7.cern | &nbsp; &nbsp; | &nbsp;
oracle-instantclient-basic-12.1-2.el7.cern | &nbsp; &nbsp; | &nbsp;
oracle-instantclient-devel-12.1-2.el7.cern | &nbsp; &nbsp; | &nbsp;
oracle-instantclient-tnsnames.ora-12.1-2.el7.cern | &nbsp; &nbsp; | &nbsp;
perl-DBD-Oracle-1.74-2.el7.cern | &nbsp; &nbsp; | &nbsp;
php-oci8-2.0.8-2.el7.cern | &nbsp; &nbsp; | &nbsp;
php-oracle-1.0-3.el7.cern | &nbsp; &nbsp; | &nbsp;
tora-2.1.3-2.el7.cern | &nbsp; &nbsp; | &nbsp;
tora-2.1.3-3.el7.cern | &nbsp; &nbsp; | &nbsp;
