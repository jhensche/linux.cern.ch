## 2021-07-05


Package | Advisory | Notes
------- | -------- | -----
ansible-collections-openstack-1.5.0-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-packstack-15.1.0-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-packstack-doc-15.1.0-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-packstack-puppet-15.1.0-1.el7 | &nbsp; &nbsp; | &nbsp;
puppet-murano-15.5.1-1.el7 | &nbsp; &nbsp; | &nbsp;
puppet-nova-15.8.1-1.el7 | &nbsp; &nbsp; | &nbsp;
tripleo-operator-ansible-0.5.1-1.el7 | &nbsp; &nbsp; | &nbsp;

