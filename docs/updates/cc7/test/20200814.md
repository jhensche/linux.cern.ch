## 2020-08-14


Package | Advisory | Notes
------- | -------- | -----
cern-get-keytab-1.1.5-11.el7.cern | &nbsp; &nbsp; | &nbsp;
ansible-2.9.11-1.el7 | &nbsp; &nbsp; | &nbsp;
ansible-test-2.9.11-1.el7 | &nbsp; &nbsp; | &nbsp;

