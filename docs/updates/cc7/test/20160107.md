## 2016-01-07

Package | Advisory | Notes
------- | -------- | -----
kmod-openafs-1.6.9-2.3.10.0_327.4.4.el7.centos.plus | &nbsp; &nbsp; | &nbsp;
kmod-openafs-1.6.9-2.3.10.0_327.4.4.el7 | &nbsp; &nbsp; | &nbsp;
kmod-spl-0.6.5.3-2.el7.cern | &nbsp; &nbsp; | &nbsp;
kmod-spl-devel-0.6.5.3-2.el7.cern | &nbsp; &nbsp; | &nbsp;
kmod-zfs-0.6.5.3-2.el7.cern | &nbsp; &nbsp; | &nbsp;
kmod-zfs-devel-0.6.5.3-2.el7.cern | &nbsp; &nbsp; | &nbsp;
