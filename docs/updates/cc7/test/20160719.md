## 2016-07-19

Package | Advisory | Notes
------- | -------- | -----
httpd-2.4.6-40.el7.centos.4 | &nbsp; &nbsp; | &nbsp;
httpd-devel-2.4.6-40.el7.centos.4 | &nbsp; &nbsp; | &nbsp;
httpd-manual-2.4.6-40.el7.centos.4 | &nbsp; &nbsp; | &nbsp;
httpd-tools-2.4.6-40.el7.centos.4 | &nbsp; &nbsp; | &nbsp;
mod_ldap-2.4.6-40.el7.centos.4 | &nbsp; &nbsp; | &nbsp;
mod_proxy_html-2.4.6-40.el7.centos.4 | &nbsp; &nbsp; | &nbsp;
mod_session-2.4.6-40.el7.centos.4 | &nbsp; &nbsp; | &nbsp;
mod_ssl-2.4.6-40.el7.centos.4 | &nbsp; &nbsp; | &nbsp;
