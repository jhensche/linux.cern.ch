## 2018-10-19

Package | Advisory | Notes
------- | -------- | -----
openstack-ceilometer-central-11.0.1-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-ceilometer-common-11.0.1-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-ceilometer-compute-11.0.1-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-ceilometer-ipmi-11.0.1-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-ceilometer-notification-11.0.1-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-ceilometer-polling-11.0.1-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-neutron-13.0.1-2.el7 | &nbsp; &nbsp; | &nbsp;
openstack-neutron-bgp-dragent-13.0.0-2.el7 | &nbsp; &nbsp; | &nbsp;
openstack-neutron-common-13.0.1-2.el7 | &nbsp; &nbsp; | &nbsp;
openstack-neutron-dynamic-routing-common-13.0.0-2.el7 | &nbsp; &nbsp; | &nbsp;
openstack-neutron-linuxbridge-13.0.1-2.el7 | &nbsp; &nbsp; | &nbsp;
openstack-neutron-macvtap-agent-13.0.1-2.el7 | &nbsp; &nbsp; | &nbsp;
openstack-neutron-metering-agent-13.0.1-2.el7 | &nbsp; &nbsp; | &nbsp;
openstack-neutron-ml2-13.0.1-2.el7 | &nbsp; &nbsp; | &nbsp;
openstack-neutron-openvswitch-13.0.1-2.el7 | &nbsp; &nbsp; | &nbsp;
openstack-neutron-rpc-server-13.0.1-2.el7 | &nbsp; &nbsp; | &nbsp;
openstack-neutron-sriov-nic-agent-13.0.1-2.el7 | &nbsp; &nbsp; | &nbsp;
puppet-aodh-13.3.1-1.el7 | &nbsp; &nbsp; | &nbsp;
puppet-barbican-13.3.1-1.el7 | &nbsp; &nbsp; | &nbsp;
puppet-ceilometer-13.3.1-1.el7 | &nbsp; &nbsp; | &nbsp;
puppet-cinder-13.3.1-1.el7 | &nbsp; &nbsp; | &nbsp;
puppet-congress-13.3.1-1.el7 | &nbsp; &nbsp; | &nbsp;
puppet-designate-13.3.1-1.el7 | &nbsp; &nbsp; | &nbsp;
puppet-ec2api-13.3.1-1.el7 | &nbsp; &nbsp; | &nbsp;
puppet-glance-13.3.1-1.el7 | &nbsp; &nbsp; | &nbsp;
puppet-gnocchi-13.3.1-1.el7 | &nbsp; &nbsp; | &nbsp;
puppet-heat-13.3.1-1.el7 | &nbsp; &nbsp; | &nbsp;
puppet-horizon-13.3.1-1.el7 | &nbsp; &nbsp; | &nbsp;
puppet-ironic-13.3.1-1.el7 | &nbsp; &nbsp; | &nbsp;
puppet-keystone-13.3.1-1.el7 | &nbsp; &nbsp; | &nbsp;
puppet-magnum-13.3.1-1.el7 | &nbsp; &nbsp; | &nbsp;
puppet-manila-13.3.1-1.el7 | &nbsp; &nbsp; | &nbsp;
puppet-mistral-13.3.1-1.el7 | &nbsp; &nbsp; | &nbsp;
puppet-murano-13.3.1-1.el7 | &nbsp; &nbsp; | &nbsp;
puppet-neutron-13.3.1-1.el7 | &nbsp; &nbsp; | &nbsp;
puppet-nova-13.3.1-1.el7 | &nbsp; &nbsp; | &nbsp;
puppet-octavia-13.3.1-1.el7 | &nbsp; &nbsp; | &nbsp;
puppet-openstack_extras-13.3.1-1.el7 | &nbsp; &nbsp; | &nbsp;
puppet-openstacklib-13.3.1-1.el7 | &nbsp; &nbsp; | &nbsp;
puppet-oslo-13.3.1-1.el7 | &nbsp; &nbsp; | &nbsp;
puppet-ovn-13.3.1-1.el7 | &nbsp; &nbsp; | &nbsp;
puppet-panko-13.3.1-1.el7 | &nbsp; &nbsp; | &nbsp;
puppet-qdr-2.3.1-1.el7 | &nbsp; &nbsp; | &nbsp;
puppet-sahara-13.3.1-1.el7 | &nbsp; &nbsp; | &nbsp;
puppet-swift-13.3.1-1.el7 | &nbsp; &nbsp; | &nbsp;
puppet-tacker-13.3.1-1.el7 | &nbsp; &nbsp; | &nbsp;
puppet-tempest-13.3.1-1.el7 | &nbsp; &nbsp; | &nbsp;
puppet-trove-13.3.1-1.el7 | &nbsp; &nbsp; | &nbsp;
puppet-vitrage-3.3.1-1.el7 | &nbsp; &nbsp; | &nbsp;
puppet-vswitch-9.3.1-1.el7 | &nbsp; &nbsp; | &nbsp;
puppet-zaqar-13.3.1-1.el7 | &nbsp; &nbsp; | &nbsp;
python2-neutron-dynamic-routing-13.0.0-2.el7 | &nbsp; &nbsp; | &nbsp;
python2-neutron-dynamic-routing-tests-13.0.0-2.el7 | &nbsp; &nbsp; | &nbsp;
python2-os-brick-1.15.6-1.el7 | &nbsp; &nbsp; | &nbsp;
python2-os-brick-2.5.4-1.el7 | &nbsp; &nbsp; | &nbsp;
python2-oslo-messaging-5.17.4-1.el7 | &nbsp; &nbsp; | &nbsp;
python2-oslo-messaging-tests-5.17.4-1.el7 | &nbsp; &nbsp; | &nbsp;
python-ceilometer-11.0.1-1.el7 | &nbsp; &nbsp; | &nbsp;
python-ceilometer-tests-11.0.1-1.el7 | &nbsp; &nbsp; | &nbsp;
python-neutron-13.0.1-2.el7 | &nbsp; &nbsp; | &nbsp;
python-neutron-tests-13.0.1-2.el7 | &nbsp; &nbsp; | &nbsp;
python-oslo-messaging-doc-5.17.4-1.el7 | &nbsp; &nbsp; | &nbsp;
