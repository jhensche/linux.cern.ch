## 2016-07-28

Package | Advisory | Notes
------- | -------- | -----
java-1.7.0-openjdk-1.7.0.111-2.6.7.2.el7_2 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2016:1504" target="secadv">RHSA-2016:1504</a> | &nbsp;
java-1.7.0-openjdk-accessibility-1.7.0.111-2.6.7.2.el7_2 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2016:1504" target="secadv">RHSA-2016:1504</a> | &nbsp;
java-1.7.0-openjdk-demo-1.7.0.111-2.6.7.2.el7_2 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2016:1504" target="secadv">RHSA-2016:1504</a> | &nbsp;
java-1.7.0-openjdk-devel-1.7.0.111-2.6.7.2.el7_2 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2016:1504" target="secadv">RHSA-2016:1504</a> | &nbsp;
java-1.7.0-openjdk-headless-1.7.0.111-2.6.7.2.el7_2 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2016:1504" target="secadv">RHSA-2016:1504</a> | &nbsp;
java-1.7.0-openjdk-javadoc-1.7.0.111-2.6.7.2.el7_2 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2016:1504" target="secadv">RHSA-2016:1504</a> | &nbsp;
java-1.7.0-openjdk-src-1.7.0.111-2.6.7.2.el7_2 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2016:1504" target="secadv">RHSA-2016:1504</a> | &nbsp;
