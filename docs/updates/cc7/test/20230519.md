## 2023-05-19


Package | Advisory | Notes
------- | -------- | -----
cern-get-certificate-1.0.0-1.el7.cern | &nbsp; &nbsp; | &nbsp;
cronie-1.4.11-25.el7_9 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2023:1989" target="secadv">RHBA-2023:1989</a> | &nbsp;
cronie-anacron-1.4.11-25.el7_9 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2023:1989" target="secadv">RHBA-2023:1989</a> | &nbsp;
cronie-noanacron-1.4.11-25.el7_9 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2023:1989" target="secadv">RHBA-2023:1989</a> | &nbsp;
ipa-client-4.6.8-5.el7.centos.14 | &nbsp; &nbsp; | &nbsp;
ipa-client-common-4.6.8-5.el7.centos.14 | &nbsp; &nbsp; | &nbsp;
ipa-common-4.6.8-5.el7.centos.14 | &nbsp; &nbsp; | &nbsp;
ipa-python-compat-4.6.8-5.el7.centos.14 | &nbsp; &nbsp; | &nbsp;
ipa-server-4.6.8-5.el7.centos.14 | &nbsp; &nbsp; | &nbsp;
ipa-server-common-4.6.8-5.el7.centos.14 | &nbsp; &nbsp; | &nbsp;
ipa-server-dns-4.6.8-5.el7.centos.14 | &nbsp; &nbsp; | &nbsp;
ipa-server-trust-ad-4.6.8-5.el7.centos.14 | &nbsp; &nbsp; | &nbsp;
java-1.8.0-openjdk-1.8.0.372.b07-1.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2023:1904" target="secadv">RHSA-2023:1904</a> | &nbsp;
java-1.8.0-openjdk-accessibility-1.8.0.372.b07-1.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2023:1904" target="secadv">RHSA-2023:1904</a> | &nbsp;
java-1.8.0-openjdk-demo-1.8.0.372.b07-1.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2023:1904" target="secadv">RHSA-2023:1904</a> | &nbsp;
java-1.8.0-openjdk-devel-1.8.0.372.b07-1.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2023:1904" target="secadv">RHSA-2023:1904</a> | &nbsp;
java-1.8.0-openjdk-headless-1.8.0.372.b07-1.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2023:1904" target="secadv">RHSA-2023:1904</a> | &nbsp;
java-1.8.0-openjdk-javadoc-1.8.0.372.b07-1.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2023:1904" target="secadv">RHSA-2023:1904</a> | &nbsp;
java-1.8.0-openjdk-javadoc-zip-1.8.0.372.b07-1.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2023:1904" target="secadv">RHSA-2023:1904</a> | &nbsp;
java-1.8.0-openjdk-src-1.8.0.372.b07-1.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2023:1904" target="secadv">RHSA-2023:1904</a> | &nbsp;
python2-ipaclient-4.6.8-5.el7.centos.14 | &nbsp; &nbsp; | &nbsp;
python2-ipalib-4.6.8-5.el7.centos.14 | &nbsp; &nbsp; | &nbsp;
python2-ipaserver-4.6.8-5.el7.centos.14 | &nbsp; &nbsp; | &nbsp;

