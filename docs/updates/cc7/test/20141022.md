## 2014-10-22

Package | Advisory | Notes
------- | -------- | -----
libcacard-1.5.3-60.el7_0.10 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2014:1669" target="secadv">RHSA-2014:1669</a> | &nbsp;
libcacard-devel-1.5.3-60.el7_0.10 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2014:1669" target="secadv">RHSA-2014:1669</a> | &nbsp;
libcacard-tools-1.5.3-60.el7_0.10 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2014:1669" target="secadv">RHSA-2014:1669</a> | &nbsp;
libxml2-2.9.1-5.el7_0.1 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2014:1655" target="secadv">RHSA-2014:1655</a> | &nbsp;
libxml2-devel-2.9.1-5.el7_0.1 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2014:1655" target="secadv">RHSA-2014:1655</a> | &nbsp;
libxml2-python-2.9.1-5.el7_0.1 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2014:1655" target="secadv">RHSA-2014:1655</a> | &nbsp;
libxml2-static-2.9.1-5.el7_0.1 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2014:1655" target="secadv">RHSA-2014:1655</a> | &nbsp;
qemu-guest-agent-1.5.3-60.el7_0.10 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2014:1669" target="secadv">RHSA-2014:1669</a> | &nbsp;
qemu-img-1.5.3-60.el7_0.10 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2014:1669" target="secadv">RHSA-2014:1669</a> | &nbsp;
qemu-kvm-1.5.3-60.el7_0.10 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2014:1669" target="secadv">RHSA-2014:1669</a> | &nbsp;
qemu-kvm-common-1.5.3-60.el7_0.10 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2014:1669" target="secadv">RHSA-2014:1669</a> | &nbsp;
qemu-kvm-tools-1.5.3-60.el7_0.10 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2014:1669" target="secadv">RHSA-2014:1669</a> | &nbsp;
wireshark-1.10.3-12.el7_0 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2014:1676" target="secadv">RHSA-2014:1676</a> | &nbsp;
wireshark-devel-1.10.3-12.el7_0 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2014:1676" target="secadv">RHSA-2014:1676</a> | &nbsp;
wireshark-gnome-1.10.3-12.el7_0 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2014:1676" target="secadv">RHSA-2014:1676</a> | &nbsp;
