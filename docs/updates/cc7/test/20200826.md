## 2020-08-26


Package | Advisory | Notes
------- | -------- | -----
kmod-openafs-1.6.22.3-1.3.10.0_1127.19.1.el7 | &nbsp; &nbsp; | &nbsp;
ceph-ansible-3.2.49-1.el7 | &nbsp; &nbsp; | &nbsp;
bpftool-3.10.0-1127.19.1.el7 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2020:3528" target="secadv">RHBA-2020:3528</a> | &nbsp;
kernel-3.10.0-1127.19.1.el7 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2020:3528" target="secadv">RHBA-2020:3528</a> | &nbsp;
kernel-abi-whitelists-3.10.0-1127.19.1.el7 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2020:3528" target="secadv">RHBA-2020:3528</a> | &nbsp;
kernel-debug-3.10.0-1127.19.1.el7 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2020:3528" target="secadv">RHBA-2020:3528</a> | &nbsp;
kernel-debug-devel-3.10.0-1127.19.1.el7 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2020:3528" target="secadv">RHBA-2020:3528</a> | &nbsp;
kernel-devel-3.10.0-1127.19.1.el7 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2020:3528" target="secadv">RHBA-2020:3528</a> | &nbsp;
kernel-doc-3.10.0-1127.19.1.el7 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2020:3528" target="secadv">RHBA-2020:3528</a> | &nbsp;
kernel-headers-3.10.0-1127.19.1.el7 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2020:3528" target="secadv">RHBA-2020:3528</a> | &nbsp;
kernel-tools-3.10.0-1127.19.1.el7 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2020:3528" target="secadv">RHBA-2020:3528</a> | &nbsp;
kernel-tools-libs-3.10.0-1127.19.1.el7 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2020:3528" target="secadv">RHBA-2020:3528</a> | &nbsp;
kernel-tools-libs-devel-3.10.0-1127.19.1.el7 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2020:3528" target="secadv">RHBA-2020:3528</a> | &nbsp;
perf-3.10.0-1127.19.1.el7 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2020:3528" target="secadv">RHBA-2020:3528</a> | &nbsp;
python-perf-3.10.0-1127.19.1.el7 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2020:3528" target="secadv">RHBA-2020:3528</a> | &nbsp;

