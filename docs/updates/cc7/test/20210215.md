## 2021-02-15


Package | Advisory | Notes
------- | -------- | -----
puppet-eosclient-2.7-1.el7.cern | &nbsp; &nbsp; | &nbsp;
puppet-kerberos-2.7-1.el7.cern | &nbsp; &nbsp; | &nbsp;
puppet-nscd-2.1-1.el7.cern | &nbsp; &nbsp; | &nbsp;
puppet-ntp-2.1-1.el7.cern | &nbsp; &nbsp; | &nbsp;
puppet-sendmail-2.2-1.el7.cern | &nbsp; &nbsp; | &nbsp;
puppet-ssh-2.2-1.el7.cern | &nbsp; &nbsp; | &nbsp;
puppet-sudo-2.1-1.el7.cern | &nbsp; &nbsp; | &nbsp;

