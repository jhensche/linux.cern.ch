## 2016-08-12

Package | Advisory | Notes
------- | -------- | -----
java-1.8.0-oracle-1.8.0.102-1.el7.cern | &nbsp; &nbsp; | &nbsp;
java-1.8.0-oracle-devel-1.8.0.102-1.el7.cern | &nbsp; &nbsp; | &nbsp;
java-1.8.0-oracle-headless-1.8.0.102-1.el7.cern | &nbsp; &nbsp; | &nbsp;
java-1.8.0-oracle-javafx-1.8.0.102-1.el7.cern | &nbsp; &nbsp; | &nbsp;
java-1.8.0-oracle-plugin-1.8.0.102-1.el7.cern | &nbsp; &nbsp; | &nbsp;
java-1.8.0-oracle-src-1.8.0.102-1.el7.cern | &nbsp; &nbsp; | &nbsp;
python-networking-odl-2.0.0-1.el7 | &nbsp; &nbsp; | &nbsp;
