## 2023-04-19

### CERN x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
useraddcern | 1.1-1.rh8.cern | |

### baseos x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
kpatch-patch-4_18_0-425_10_1 | 1-4.el8_7 | |
kpatch-patch-4_18_0-425_10_1-debuginfo | 1-4.el8_7 | |
kpatch-patch-4_18_0-425_10_1-debugsource | 1-4.el8_7 | |
kpatch-patch-4_18_0-425_13_1 | 1-2.el8_7 | |
kpatch-patch-4_18_0-425_13_1-debuginfo | 1-2.el8_7 | |
kpatch-patch-4_18_0-425_13_1-debugsource | 1-2.el8_7 | |
kpatch-patch-4_18_0-425_3_1 | 1-6.el8 | |
kpatch-patch-4_18_0-425_3_1-debuginfo | 1-6.el8 | |
kpatch-patch-4_18_0-425_3_1-debugsource | 1-6.el8 | |

### appstream x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
httpd | 2.4.37-51.module+el8.7.0+18499+2e106f0b.5 | |
httpd-debuginfo | 2.4.37-51.module+el8.7.0+18499+2e106f0b.5 | |
httpd-debugsource | 2.4.37-51.module+el8.7.0+18499+2e106f0b.5 | |
httpd-devel | 2.4.37-51.module+el8.7.0+18499+2e106f0b.5 | |
httpd-filesystem | 2.4.37-51.module+el8.7.0+18499+2e106f0b.5 | |
httpd-manual | 2.4.37-51.module+el8.7.0+18499+2e106f0b.5 | |
httpd-tools | 2.4.37-51.module+el8.7.0+18499+2e106f0b.5 | |
httpd-tools-debuginfo | 2.4.37-51.module+el8.7.0+18499+2e106f0b.5 | |
mod_http2 | 1.15.7-5.module+el8.7.0+18499+2e106f0b.4 | |
mod_http2-debuginfo | 1.15.7-5.module+el8.7.0+18499+2e106f0b.4 | |
mod_http2-debugsource | 1.15.7-5.module+el8.7.0+18499+2e106f0b.4 | |
mod_ldap | 2.4.37-51.module+el8.7.0+18499+2e106f0b.5 | |
mod_ldap-debuginfo | 2.4.37-51.module+el8.7.0+18499+2e106f0b.5 | |
mod_proxy_html | 2.4.37-51.module+el8.7.0+18499+2e106f0b.5 | |
mod_proxy_html-debuginfo | 2.4.37-51.module+el8.7.0+18499+2e106f0b.5 | |
mod_session | 2.4.37-51.module+el8.7.0+18499+2e106f0b.5 | |
mod_session-debuginfo | 2.4.37-51.module+el8.7.0+18499+2e106f0b.5 | |
mod_ssl | 2.4.37-51.module+el8.7.0+18499+2e106f0b.5 | |
mod_ssl-debuginfo | 2.4.37-51.module+el8.7.0+18499+2e106f0b.5 | |

### CERN aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
useraddcern | 1.1-1.rh8.cern | |

### appstream aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
httpd | 2.4.37-51.module+el8.7.0+18499+2e106f0b.5 | |
httpd-debuginfo | 2.4.37-51.module+el8.7.0+18499+2e106f0b.5 | |
httpd-debugsource | 2.4.37-51.module+el8.7.0+18499+2e106f0b.5 | |
httpd-devel | 2.4.37-51.module+el8.7.0+18499+2e106f0b.5 | |
httpd-filesystem | 2.4.37-51.module+el8.7.0+18499+2e106f0b.5 | |
httpd-manual | 2.4.37-51.module+el8.7.0+18499+2e106f0b.5 | |
httpd-tools | 2.4.37-51.module+el8.7.0+18499+2e106f0b.5 | |
httpd-tools-debuginfo | 2.4.37-51.module+el8.7.0+18499+2e106f0b.5 | |
mod_http2 | 1.15.7-5.module+el8.7.0+18499+2e106f0b.4 | |
mod_http2-debuginfo | 1.15.7-5.module+el8.7.0+18499+2e106f0b.4 | |
mod_http2-debugsource | 1.15.7-5.module+el8.7.0+18499+2e106f0b.4 | |
mod_ldap | 2.4.37-51.module+el8.7.0+18499+2e106f0b.5 | |
mod_ldap-debuginfo | 2.4.37-51.module+el8.7.0+18499+2e106f0b.5 | |
mod_proxy_html | 2.4.37-51.module+el8.7.0+18499+2e106f0b.5 | |
mod_proxy_html-debuginfo | 2.4.37-51.module+el8.7.0+18499+2e106f0b.5 | |
mod_session | 2.4.37-51.module+el8.7.0+18499+2e106f0b.5 | |
mod_session-debuginfo | 2.4.37-51.module+el8.7.0+18499+2e106f0b.5 | |
mod_ssl | 2.4.37-51.module+el8.7.0+18499+2e106f0b.5 | |
mod_ssl-debuginfo | 2.4.37-51.module+el8.7.0+18499+2e106f0b.5 | |

