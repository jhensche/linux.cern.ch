## 2024-02-20

### baseos x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
kpatch-patch-4_18_0-513_11_1 | 1-1.el8_9 | |
kpatch-patch-4_18_0-513_11_1-debuginfo | 1-1.el8_9 | |
kpatch-patch-4_18_0-513_11_1-debugsource | 1-1.el8_9 | |
kpatch-patch-4_18_0-513_18_1 | 0-0.el8_9 | |
kpatch-patch-4_18_0-513_5_1 | 1-3.el8_9 | |
kpatch-patch-4_18_0-513_5_1-debuginfo | 1-3.el8_9 | |
kpatch-patch-4_18_0-513_5_1-debugsource | 1-3.el8_9 | |
kpatch-patch-4_18_0-513_9_1 | 1-2.el8_9 | |
kpatch-patch-4_18_0-513_9_1-debuginfo | 1-2.el8_9 | |
kpatch-patch-4_18_0-513_9_1-debugsource | 1-2.el8_9 | |

### appstream x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
gimp | 2.8.22-25.module+el8.9.0+21239+e25d5ab1 | [RHSA-2024:0861](https://access.redhat.com/errata/RHSA-2024:0861) | <div class="adv_s">Security Advisory</div> ([CVE-2023-44442](https://access.redhat.com/security/cve/CVE-2023-44442), [CVE-2023-44444](https://access.redhat.com/security/cve/CVE-2023-44444))
gimp-debuginfo | 2.8.22-25.module+el8.9.0+21239+e25d5ab1 | |
gimp-debugsource | 2.8.22-25.module+el8.9.0+21239+e25d5ab1 | |
gimp-devel | 2.8.22-25.module+el8.9.0+21239+e25d5ab1 | [RHSA-2024:0861](https://access.redhat.com/errata/RHSA-2024:0861) | <div class="adv_s">Security Advisory</div> ([CVE-2023-44442](https://access.redhat.com/security/cve/CVE-2023-44442), [CVE-2023-44444](https://access.redhat.com/security/cve/CVE-2023-44444))
gimp-devel-tools | 2.8.22-25.module+el8.9.0+21239+e25d5ab1 | [RHSA-2024:0861](https://access.redhat.com/errata/RHSA-2024:0861) | <div class="adv_s">Security Advisory</div> ([CVE-2023-44442](https://access.redhat.com/security/cve/CVE-2023-44442), [CVE-2023-44444](https://access.redhat.com/security/cve/CVE-2023-44444))
gimp-devel-tools-debuginfo | 2.8.22-25.module+el8.9.0+21239+e25d5ab1 | |
gimp-libs | 2.8.22-25.module+el8.9.0+21239+e25d5ab1 | [RHSA-2024:0861](https://access.redhat.com/errata/RHSA-2024:0861) | <div class="adv_s">Security Advisory</div> ([CVE-2023-44442](https://access.redhat.com/security/cve/CVE-2023-44442), [CVE-2023-44444](https://access.redhat.com/security/cve/CVE-2023-44444))
gimp-libs-debuginfo | 2.8.22-25.module+el8.9.0+21239+e25d5ab1 | |
pygobject2 | 2.28.7-4.module+el8.9.0+21228+8e80d31d | [RHSA-2024:0861](https://access.redhat.com/errata/RHSA-2024:0861) | <div class="adv_s">Security Advisory</div> ([CVE-2023-44442](https://access.redhat.com/security/cve/CVE-2023-44442), [CVE-2023-44444](https://access.redhat.com/security/cve/CVE-2023-44444))
pygobject2-codegen | 2.28.7-4.module+el8.9.0+21228+8e80d31d | [RHSA-2024:0861](https://access.redhat.com/errata/RHSA-2024:0861) | <div class="adv_s">Security Advisory</div> ([CVE-2023-44442](https://access.redhat.com/security/cve/CVE-2023-44442), [CVE-2023-44444](https://access.redhat.com/security/cve/CVE-2023-44444))
pygobject2-debuginfo | 2.28.7-4.module+el8.9.0+21228+8e80d31d | |
pygobject2-debugsource | 2.28.7-4.module+el8.9.0+21228+8e80d31d | |
pygobject2-devel | 2.28.7-4.module+el8.9.0+21228+8e80d31d | [RHSA-2024:0861](https://access.redhat.com/errata/RHSA-2024:0861) | <div class="adv_s">Security Advisory</div> ([CVE-2023-44442](https://access.redhat.com/security/cve/CVE-2023-44442), [CVE-2023-44444](https://access.redhat.com/security/cve/CVE-2023-44444))
pygobject2-doc | 2.28.7-4.module+el8.9.0+21228+8e80d31d | [RHSA-2024:0861](https://access.redhat.com/errata/RHSA-2024:0861) | <div class="adv_s">Security Advisory</div> ([CVE-2023-44442](https://access.redhat.com/security/cve/CVE-2023-44442), [CVE-2023-44444](https://access.redhat.com/security/cve/CVE-2023-44444))
pygtk2 | 2.24.0-25.module+el8.9.0+21228+8e80d31d | [RHSA-2024:0861](https://access.redhat.com/errata/RHSA-2024:0861) | <div class="adv_s">Security Advisory</div> ([CVE-2023-44442](https://access.redhat.com/security/cve/CVE-2023-44442), [CVE-2023-44444](https://access.redhat.com/security/cve/CVE-2023-44444))
pygtk2-codegen | 2.24.0-25.module+el8.9.0+21228+8e80d31d | [RHSA-2024:0861](https://access.redhat.com/errata/RHSA-2024:0861) | <div class="adv_s">Security Advisory</div> ([CVE-2023-44442](https://access.redhat.com/security/cve/CVE-2023-44442), [CVE-2023-44444](https://access.redhat.com/security/cve/CVE-2023-44444))
pygtk2-debuginfo | 2.24.0-25.module+el8.9.0+21228+8e80d31d | |
pygtk2-debugsource | 2.24.0-25.module+el8.9.0+21228+8e80d31d | |
pygtk2-devel | 2.24.0-25.module+el8.9.0+21228+8e80d31d | [RHSA-2024:0861](https://access.redhat.com/errata/RHSA-2024:0861) | <div class="adv_s">Security Advisory</div> ([CVE-2023-44442](https://access.redhat.com/security/cve/CVE-2023-44442), [CVE-2023-44444](https://access.redhat.com/security/cve/CVE-2023-44444))
pygtk2-doc | 2.24.0-25.module+el8.9.0+21228+8e80d31d | [RHSA-2024:0861](https://access.redhat.com/errata/RHSA-2024:0861) | <div class="adv_s">Security Advisory</div> ([CVE-2023-44442](https://access.redhat.com/security/cve/CVE-2023-44442), [CVE-2023-44444](https://access.redhat.com/security/cve/CVE-2023-44444))
python2-cairo | 1.16.3-6.module+el8.9.0+21228+8e80d31d | [RHSA-2024:0861](https://access.redhat.com/errata/RHSA-2024:0861) | <div class="adv_s">Security Advisory</div> ([CVE-2023-44442](https://access.redhat.com/security/cve/CVE-2023-44442), [CVE-2023-44444](https://access.redhat.com/security/cve/CVE-2023-44444))
python2-cairo-debuginfo | 1.16.3-6.module+el8.9.0+21228+8e80d31d | |
python2-cairo-devel | 1.16.3-6.module+el8.9.0+21228+8e80d31d | [RHSA-2024:0861](https://access.redhat.com/errata/RHSA-2024:0861) | <div class="adv_s">Security Advisory</div> ([CVE-2023-44442](https://access.redhat.com/security/cve/CVE-2023-44442), [CVE-2023-44444](https://access.redhat.com/security/cve/CVE-2023-44444))
python2-pycairo-debugsource | 1.16.3-6.module+el8.9.0+21228+8e80d31d | |

### appstream aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
gimp | 2.8.22-25.module+el8.9.0+21239+e25d5ab1 | [RHSA-2024:0861](https://access.redhat.com/errata/RHSA-2024:0861) | <div class="adv_s">Security Advisory</div> ([CVE-2023-44442](https://access.redhat.com/security/cve/CVE-2023-44442), [CVE-2023-44444](https://access.redhat.com/security/cve/CVE-2023-44444))
gimp-debuginfo | 2.8.22-25.module+el8.9.0+21239+e25d5ab1 | |
gimp-debugsource | 2.8.22-25.module+el8.9.0+21239+e25d5ab1 | |
gimp-devel | 2.8.22-25.module+el8.9.0+21239+e25d5ab1 | [RHSA-2024:0861](https://access.redhat.com/errata/RHSA-2024:0861) | <div class="adv_s">Security Advisory</div> ([CVE-2023-44442](https://access.redhat.com/security/cve/CVE-2023-44442), [CVE-2023-44444](https://access.redhat.com/security/cve/CVE-2023-44444))
gimp-devel-tools | 2.8.22-25.module+el8.9.0+21239+e25d5ab1 | [RHSA-2024:0861](https://access.redhat.com/errata/RHSA-2024:0861) | <div class="adv_s">Security Advisory</div> ([CVE-2023-44442](https://access.redhat.com/security/cve/CVE-2023-44442), [CVE-2023-44444](https://access.redhat.com/security/cve/CVE-2023-44444))
gimp-devel-tools-debuginfo | 2.8.22-25.module+el8.9.0+21239+e25d5ab1 | |
gimp-libs | 2.8.22-25.module+el8.9.0+21239+e25d5ab1 | [RHSA-2024:0861](https://access.redhat.com/errata/RHSA-2024:0861) | <div class="adv_s">Security Advisory</div> ([CVE-2023-44442](https://access.redhat.com/security/cve/CVE-2023-44442), [CVE-2023-44444](https://access.redhat.com/security/cve/CVE-2023-44444))
gimp-libs-debuginfo | 2.8.22-25.module+el8.9.0+21239+e25d5ab1 | |
pygobject2 | 2.28.7-4.module+el8.9.0+21228+8e80d31d | [RHSA-2024:0861](https://access.redhat.com/errata/RHSA-2024:0861) | <div class="adv_s">Security Advisory</div> ([CVE-2023-44442](https://access.redhat.com/security/cve/CVE-2023-44442), [CVE-2023-44444](https://access.redhat.com/security/cve/CVE-2023-44444))
pygobject2-codegen | 2.28.7-4.module+el8.9.0+21228+8e80d31d | [RHSA-2024:0861](https://access.redhat.com/errata/RHSA-2024:0861) | <div class="adv_s">Security Advisory</div> ([CVE-2023-44442](https://access.redhat.com/security/cve/CVE-2023-44442), [CVE-2023-44444](https://access.redhat.com/security/cve/CVE-2023-44444))
pygobject2-debuginfo | 2.28.7-4.module+el8.9.0+21228+8e80d31d | |
pygobject2-debugsource | 2.28.7-4.module+el8.9.0+21228+8e80d31d | |
pygobject2-devel | 2.28.7-4.module+el8.9.0+21228+8e80d31d | [RHSA-2024:0861](https://access.redhat.com/errata/RHSA-2024:0861) | <div class="adv_s">Security Advisory</div> ([CVE-2023-44442](https://access.redhat.com/security/cve/CVE-2023-44442), [CVE-2023-44444](https://access.redhat.com/security/cve/CVE-2023-44444))
pygobject2-doc | 2.28.7-4.module+el8.9.0+21228+8e80d31d | [RHSA-2024:0861](https://access.redhat.com/errata/RHSA-2024:0861) | <div class="adv_s">Security Advisory</div> ([CVE-2023-44442](https://access.redhat.com/security/cve/CVE-2023-44442), [CVE-2023-44444](https://access.redhat.com/security/cve/CVE-2023-44444))
pygtk2 | 2.24.0-25.module+el8.9.0+21228+8e80d31d | [RHSA-2024:0861](https://access.redhat.com/errata/RHSA-2024:0861) | <div class="adv_s">Security Advisory</div> ([CVE-2023-44442](https://access.redhat.com/security/cve/CVE-2023-44442), [CVE-2023-44444](https://access.redhat.com/security/cve/CVE-2023-44444))
pygtk2-codegen | 2.24.0-25.module+el8.9.0+21228+8e80d31d | [RHSA-2024:0861](https://access.redhat.com/errata/RHSA-2024:0861) | <div class="adv_s">Security Advisory</div> ([CVE-2023-44442](https://access.redhat.com/security/cve/CVE-2023-44442), [CVE-2023-44444](https://access.redhat.com/security/cve/CVE-2023-44444))
pygtk2-debuginfo | 2.24.0-25.module+el8.9.0+21228+8e80d31d | |
pygtk2-debugsource | 2.24.0-25.module+el8.9.0+21228+8e80d31d | |
pygtk2-devel | 2.24.0-25.module+el8.9.0+21228+8e80d31d | [RHSA-2024:0861](https://access.redhat.com/errata/RHSA-2024:0861) | <div class="adv_s">Security Advisory</div> ([CVE-2023-44442](https://access.redhat.com/security/cve/CVE-2023-44442), [CVE-2023-44444](https://access.redhat.com/security/cve/CVE-2023-44444))
pygtk2-doc | 2.24.0-25.module+el8.9.0+21228+8e80d31d | [RHSA-2024:0861](https://access.redhat.com/errata/RHSA-2024:0861) | <div class="adv_s">Security Advisory</div> ([CVE-2023-44442](https://access.redhat.com/security/cve/CVE-2023-44442), [CVE-2023-44444](https://access.redhat.com/security/cve/CVE-2023-44444))
python2-cairo | 1.16.3-6.module+el8.9.0+21228+8e80d31d | [RHSA-2024:0861](https://access.redhat.com/errata/RHSA-2024:0861) | <div class="adv_s">Security Advisory</div> ([CVE-2023-44442](https://access.redhat.com/security/cve/CVE-2023-44442), [CVE-2023-44444](https://access.redhat.com/security/cve/CVE-2023-44444))
python2-cairo-debuginfo | 1.16.3-6.module+el8.9.0+21228+8e80d31d | |
python2-cairo-devel | 1.16.3-6.module+el8.9.0+21228+8e80d31d | [RHSA-2024:0861](https://access.redhat.com/errata/RHSA-2024:0861) | <div class="adv_s">Security Advisory</div> ([CVE-2023-44442](https://access.redhat.com/security/cve/CVE-2023-44442), [CVE-2023-44444](https://access.redhat.com/security/cve/CVE-2023-44444))
python2-pycairo-debugsource | 1.16.3-6.module+el8.9.0+21228+8e80d31d | |

