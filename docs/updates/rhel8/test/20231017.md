## 2023-10-17

### appstream x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
aspnetcore-runtime-6.0 | 6.0.23-1.el8_8 | [RHSA-2023:5710](https://access.redhat.com/errata/RHSA-2023:5710) | <div class="adv_s">Security Advisory</div> ([CVE-2023-44487](https://access.redhat.com/security/cve/CVE-2023-44487))
aspnetcore-runtime-7.0 | 7.0.12-1.el8_8 | [RHSA-2023:5709](https://access.redhat.com/errata/RHSA-2023:5709) | <div class="adv_s">Security Advisory</div> ([CVE-2023-44487](https://access.redhat.com/security/cve/CVE-2023-44487))
aspnetcore-targeting-pack-6.0 | 6.0.23-1.el8_8 | [RHSA-2023:5710](https://access.redhat.com/errata/RHSA-2023:5710) | <div class="adv_s">Security Advisory</div> ([CVE-2023-44487](https://access.redhat.com/security/cve/CVE-2023-44487))
aspnetcore-targeting-pack-7.0 | 7.0.12-1.el8_8 | [RHSA-2023:5709](https://access.redhat.com/errata/RHSA-2023:5709) | <div class="adv_s">Security Advisory</div> ([CVE-2023-44487](https://access.redhat.com/security/cve/CVE-2023-44487))
dotnet | 7.0.112-1.el8_8 | [RHSA-2023:5709](https://access.redhat.com/errata/RHSA-2023:5709) | <div class="adv_s">Security Advisory</div> ([CVE-2023-44487](https://access.redhat.com/security/cve/CVE-2023-44487))
dotnet-apphost-pack-6.0 | 6.0.23-1.el8_8 | [RHSA-2023:5710](https://access.redhat.com/errata/RHSA-2023:5710) | <div class="adv_s">Security Advisory</div> ([CVE-2023-44487](https://access.redhat.com/security/cve/CVE-2023-44487))
dotnet-apphost-pack-6.0-debuginfo | 6.0.23-1.el8_8 | |
dotnet-apphost-pack-7.0 | 7.0.12-1.el8_8 | [RHSA-2023:5709](https://access.redhat.com/errata/RHSA-2023:5709) | <div class="adv_s">Security Advisory</div> ([CVE-2023-44487](https://access.redhat.com/security/cve/CVE-2023-44487))
dotnet-apphost-pack-7.0-debuginfo | 7.0.12-1.el8_8 | |
dotnet-host | 7.0.12-1.el8_8 | [RHSA-2023:5709](https://access.redhat.com/errata/RHSA-2023:5709) | <div class="adv_s">Security Advisory</div> ([CVE-2023-44487](https://access.redhat.com/security/cve/CVE-2023-44487))
dotnet-host-debuginfo | 7.0.12-1.el8_8 | |
dotnet-hostfxr-6.0 | 6.0.23-1.el8_8 | [RHSA-2023:5710](https://access.redhat.com/errata/RHSA-2023:5710) | <div class="adv_s">Security Advisory</div> ([CVE-2023-44487](https://access.redhat.com/security/cve/CVE-2023-44487))
dotnet-hostfxr-6.0-debuginfo | 6.0.23-1.el8_8 | |
dotnet-hostfxr-7.0 | 7.0.12-1.el8_8 | [RHSA-2023:5709](https://access.redhat.com/errata/RHSA-2023:5709) | <div class="adv_s">Security Advisory</div> ([CVE-2023-44487](https://access.redhat.com/security/cve/CVE-2023-44487))
dotnet-hostfxr-7.0-debuginfo | 7.0.12-1.el8_8 | |
dotnet-runtime-6.0 | 6.0.23-1.el8_8 | [RHSA-2023:5710](https://access.redhat.com/errata/RHSA-2023:5710) | <div class="adv_s">Security Advisory</div> ([CVE-2023-44487](https://access.redhat.com/security/cve/CVE-2023-44487))
dotnet-runtime-6.0-debuginfo | 6.0.23-1.el8_8 | |
dotnet-runtime-7.0 | 7.0.12-1.el8_8 | [RHSA-2023:5709](https://access.redhat.com/errata/RHSA-2023:5709) | <div class="adv_s">Security Advisory</div> ([CVE-2023-44487](https://access.redhat.com/security/cve/CVE-2023-44487))
dotnet-runtime-7.0-debuginfo | 7.0.12-1.el8_8 | |
dotnet-sdk-6.0 | 6.0.123-1.el8_8 | [RHSA-2023:5710](https://access.redhat.com/errata/RHSA-2023:5710) | <div class="adv_s">Security Advisory</div> ([CVE-2023-44487](https://access.redhat.com/security/cve/CVE-2023-44487))
dotnet-sdk-6.0-debuginfo | 6.0.123-1.el8_8 | |
dotnet-sdk-7.0 | 7.0.112-1.el8_8 | [RHSA-2023:5709](https://access.redhat.com/errata/RHSA-2023:5709) | <div class="adv_s">Security Advisory</div> ([CVE-2023-44487](https://access.redhat.com/security/cve/CVE-2023-44487))
dotnet-sdk-7.0-debuginfo | 7.0.112-1.el8_8 | |
dotnet-targeting-pack-6.0 | 6.0.23-1.el8_8 | [RHSA-2023:5710](https://access.redhat.com/errata/RHSA-2023:5710) | <div class="adv_s">Security Advisory</div> ([CVE-2023-44487](https://access.redhat.com/security/cve/CVE-2023-44487))
dotnet-targeting-pack-7.0 | 7.0.12-1.el8_8 | [RHSA-2023:5709](https://access.redhat.com/errata/RHSA-2023:5709) | <div class="adv_s">Security Advisory</div> ([CVE-2023-44487](https://access.redhat.com/security/cve/CVE-2023-44487))
dotnet-templates-6.0 | 6.0.123-1.el8_8 | [RHSA-2023:5710](https://access.redhat.com/errata/RHSA-2023:5710) | <div class="adv_s">Security Advisory</div> ([CVE-2023-44487](https://access.redhat.com/security/cve/CVE-2023-44487))
dotnet-templates-7.0 | 7.0.112-1.el8_8 | [RHSA-2023:5709](https://access.redhat.com/errata/RHSA-2023:5709) | <div class="adv_s">Security Advisory</div> ([CVE-2023-44487](https://access.redhat.com/security/cve/CVE-2023-44487))
dotnet6.0-debuginfo | 6.0.123-1.el8_8 | |
dotnet6.0-debugsource | 6.0.123-1.el8_8 | |
dotnet7.0-debuginfo | 7.0.112-1.el8_8 | |
dotnet7.0-debugsource | 7.0.112-1.el8_8 | |
go-toolset | 1.19.13-1.module+el8.8.0+20380+7171fefb | [RHSA-2023:5721](https://access.redhat.com/errata/RHSA-2023:5721) | <div class="adv_s">Security Advisory</div> ([CVE-2023-39325](https://access.redhat.com/security/cve/CVE-2023-39325), [CVE-2023-44487](https://access.redhat.com/security/cve/CVE-2023-44487))
golang | 1.19.13-1.module+el8.8.0+20373+d9cd605c | [RHSA-2023:5721](https://access.redhat.com/errata/RHSA-2023:5721) | <div class="adv_s">Security Advisory</div> ([CVE-2023-39325](https://access.redhat.com/security/cve/CVE-2023-39325), [CVE-2023-44487](https://access.redhat.com/security/cve/CVE-2023-44487))
golang-bin | 1.19.13-1.module+el8.8.0+20373+d9cd605c | [RHSA-2023:5721](https://access.redhat.com/errata/RHSA-2023:5721) | <div class="adv_s">Security Advisory</div> ([CVE-2023-39325](https://access.redhat.com/security/cve/CVE-2023-39325), [CVE-2023-44487](https://access.redhat.com/security/cve/CVE-2023-44487))
golang-docs | 1.19.13-1.module+el8.8.0+20373+d9cd605c | [RHSA-2023:5721](https://access.redhat.com/errata/RHSA-2023:5721) | <div class="adv_s">Security Advisory</div> ([CVE-2023-39325](https://access.redhat.com/security/cve/CVE-2023-39325), [CVE-2023-44487](https://access.redhat.com/security/cve/CVE-2023-44487))
golang-misc | 1.19.13-1.module+el8.8.0+20373+d9cd605c | [RHSA-2023:5721](https://access.redhat.com/errata/RHSA-2023:5721) | <div class="adv_s">Security Advisory</div> ([CVE-2023-39325](https://access.redhat.com/security/cve/CVE-2023-39325), [CVE-2023-44487](https://access.redhat.com/security/cve/CVE-2023-44487))
golang-race | 1.19.13-1.module+el8.8.0+20373+d9cd605c | [RHSA-2023:5721](https://access.redhat.com/errata/RHSA-2023:5721) | <div class="adv_s">Security Advisory</div> ([CVE-2023-39325](https://access.redhat.com/security/cve/CVE-2023-39325), [CVE-2023-44487](https://access.redhat.com/security/cve/CVE-2023-44487))
golang-src | 1.19.13-1.module+el8.8.0+20373+d9cd605c | [RHSA-2023:5721](https://access.redhat.com/errata/RHSA-2023:5721) | <div class="adv_s">Security Advisory</div> ([CVE-2023-39325](https://access.redhat.com/security/cve/CVE-2023-39325), [CVE-2023-44487](https://access.redhat.com/security/cve/CVE-2023-44487))
golang-tests | 1.19.13-1.module+el8.8.0+20373+d9cd605c | [RHSA-2023:5721](https://access.redhat.com/errata/RHSA-2023:5721) | <div class="adv_s">Security Advisory</div> ([CVE-2023-39325](https://access.redhat.com/security/cve/CVE-2023-39325), [CVE-2023-44487](https://access.redhat.com/security/cve/CVE-2023-44487))
netstandard-targeting-pack-2.1 | 7.0.112-1.el8_8 | [RHSA-2023:5709](https://access.redhat.com/errata/RHSA-2023:5709) | <div class="adv_s">Security Advisory</div> ([CVE-2023-44487](https://access.redhat.com/security/cve/CVE-2023-44487))
nginx | 1.20.1-1.module+el8.8.0+20359+9bd89172.1 | [RHSA-2023:5712](https://access.redhat.com/errata/RHSA-2023:5712) | <div class="adv_s">Security Advisory</div> ([CVE-2023-44487](https://access.redhat.com/security/cve/CVE-2023-44487))
nginx | 1.22.1-1.module+el8.8.0+20355+6d9c8a63.1 | [RHSA-2023:5713](https://access.redhat.com/errata/RHSA-2023:5713) | <div class="adv_s">Security Advisory</div> ([CVE-2023-44487](https://access.redhat.com/security/cve/CVE-2023-44487))
nginx-all-modules | 1.20.1-1.module+el8.8.0+20359+9bd89172.1 | [RHSA-2023:5712](https://access.redhat.com/errata/RHSA-2023:5712) | <div class="adv_s">Security Advisory</div> ([CVE-2023-44487](https://access.redhat.com/security/cve/CVE-2023-44487))
nginx-all-modules | 1.22.1-1.module+el8.8.0+20355+6d9c8a63.1 | [RHSA-2023:5713](https://access.redhat.com/errata/RHSA-2023:5713) | <div class="adv_s">Security Advisory</div> ([CVE-2023-44487](https://access.redhat.com/security/cve/CVE-2023-44487))
nginx-debuginfo | 1.20.1-1.module+el8.8.0+20359+9bd89172.1 | |
nginx-debuginfo | 1.22.1-1.module+el8.8.0+20355+6d9c8a63.1 | |
nginx-debugsource | 1.20.1-1.module+el8.8.0+20359+9bd89172.1 | |
nginx-debugsource | 1.22.1-1.module+el8.8.0+20355+6d9c8a63.1 | |
nginx-filesystem | 1.20.1-1.module+el8.8.0+20359+9bd89172.1 | [RHSA-2023:5712](https://access.redhat.com/errata/RHSA-2023:5712) | <div class="adv_s">Security Advisory</div> ([CVE-2023-44487](https://access.redhat.com/security/cve/CVE-2023-44487))
nginx-filesystem | 1.22.1-1.module+el8.8.0+20355+6d9c8a63.1 | [RHSA-2023:5713](https://access.redhat.com/errata/RHSA-2023:5713) | <div class="adv_s">Security Advisory</div> ([CVE-2023-44487](https://access.redhat.com/security/cve/CVE-2023-44487))
nginx-mod-devel | 1.20.1-1.module+el8.8.0+20359+9bd89172.1 | [RHSA-2023:5712](https://access.redhat.com/errata/RHSA-2023:5712) | <div class="adv_s">Security Advisory</div> ([CVE-2023-44487](https://access.redhat.com/security/cve/CVE-2023-44487))
nginx-mod-devel | 1.22.1-1.module+el8.8.0+20355+6d9c8a63.1 | [RHSA-2023:5713](https://access.redhat.com/errata/RHSA-2023:5713) | <div class="adv_s">Security Advisory</div> ([CVE-2023-44487](https://access.redhat.com/security/cve/CVE-2023-44487))
nginx-mod-http-image-filter | 1.20.1-1.module+el8.8.0+20359+9bd89172.1 | [RHSA-2023:5712](https://access.redhat.com/errata/RHSA-2023:5712) | <div class="adv_s">Security Advisory</div> ([CVE-2023-44487](https://access.redhat.com/security/cve/CVE-2023-44487))
nginx-mod-http-image-filter | 1.22.1-1.module+el8.8.0+20355+6d9c8a63.1 | [RHSA-2023:5713](https://access.redhat.com/errata/RHSA-2023:5713) | <div class="adv_s">Security Advisory</div> ([CVE-2023-44487](https://access.redhat.com/security/cve/CVE-2023-44487))
nginx-mod-http-image-filter-debuginfo | 1.20.1-1.module+el8.8.0+20359+9bd89172.1 | |
nginx-mod-http-image-filter-debuginfo | 1.22.1-1.module+el8.8.0+20355+6d9c8a63.1 | |
nginx-mod-http-perl | 1.20.1-1.module+el8.8.0+20359+9bd89172.1 | [RHSA-2023:5712](https://access.redhat.com/errata/RHSA-2023:5712) | <div class="adv_s">Security Advisory</div> ([CVE-2023-44487](https://access.redhat.com/security/cve/CVE-2023-44487))
nginx-mod-http-perl | 1.22.1-1.module+el8.8.0+20355+6d9c8a63.1 | [RHSA-2023:5713](https://access.redhat.com/errata/RHSA-2023:5713) | <div class="adv_s">Security Advisory</div> ([CVE-2023-44487](https://access.redhat.com/security/cve/CVE-2023-44487))
nginx-mod-http-perl-debuginfo | 1.20.1-1.module+el8.8.0+20359+9bd89172.1 | |
nginx-mod-http-perl-debuginfo | 1.22.1-1.module+el8.8.0+20355+6d9c8a63.1 | |
nginx-mod-http-xslt-filter | 1.20.1-1.module+el8.8.0+20359+9bd89172.1 | [RHSA-2023:5712](https://access.redhat.com/errata/RHSA-2023:5712) | <div class="adv_s">Security Advisory</div> ([CVE-2023-44487](https://access.redhat.com/security/cve/CVE-2023-44487))
nginx-mod-http-xslt-filter | 1.22.1-1.module+el8.8.0+20355+6d9c8a63.1 | [RHSA-2023:5713](https://access.redhat.com/errata/RHSA-2023:5713) | <div class="adv_s">Security Advisory</div> ([CVE-2023-44487](https://access.redhat.com/security/cve/CVE-2023-44487))
nginx-mod-http-xslt-filter-debuginfo | 1.20.1-1.module+el8.8.0+20359+9bd89172.1 | |
nginx-mod-http-xslt-filter-debuginfo | 1.22.1-1.module+el8.8.0+20355+6d9c8a63.1 | |
nginx-mod-mail | 1.20.1-1.module+el8.8.0+20359+9bd89172.1 | [RHSA-2023:5712](https://access.redhat.com/errata/RHSA-2023:5712) | <div class="adv_s">Security Advisory</div> ([CVE-2023-44487](https://access.redhat.com/security/cve/CVE-2023-44487))
nginx-mod-mail | 1.22.1-1.module+el8.8.0+20355+6d9c8a63.1 | [RHSA-2023:5713](https://access.redhat.com/errata/RHSA-2023:5713) | <div class="adv_s">Security Advisory</div> ([CVE-2023-44487](https://access.redhat.com/security/cve/CVE-2023-44487))
nginx-mod-mail-debuginfo | 1.20.1-1.module+el8.8.0+20359+9bd89172.1 | |
nginx-mod-mail-debuginfo | 1.22.1-1.module+el8.8.0+20355+6d9c8a63.1 | |
nginx-mod-stream | 1.20.1-1.module+el8.8.0+20359+9bd89172.1 | [RHSA-2023:5712](https://access.redhat.com/errata/RHSA-2023:5712) | <div class="adv_s">Security Advisory</div> ([CVE-2023-44487](https://access.redhat.com/security/cve/CVE-2023-44487))
nginx-mod-stream | 1.22.1-1.module+el8.8.0+20355+6d9c8a63.1 | [RHSA-2023:5713](https://access.redhat.com/errata/RHSA-2023:5713) | <div class="adv_s">Security Advisory</div> ([CVE-2023-44487](https://access.redhat.com/security/cve/CVE-2023-44487))
nginx-mod-stream-debuginfo | 1.20.1-1.module+el8.8.0+20359+9bd89172.1 | |
nginx-mod-stream-debuginfo | 1.22.1-1.module+el8.8.0+20355+6d9c8a63.1 | |

### codeready-builder x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
dotnet-apphost-pack-6.0-debuginfo | 6.0.23-1.el8_8 | |
dotnet-apphost-pack-7.0-debuginfo | 7.0.12-1.el8_8 | |
dotnet-host-debuginfo | 7.0.12-1.el8_8 | |
dotnet-hostfxr-6.0-debuginfo | 6.0.23-1.el8_8 | |
dotnet-hostfxr-7.0-debuginfo | 7.0.12-1.el8_8 | |
dotnet-runtime-6.0-debuginfo | 6.0.23-1.el8_8 | |
dotnet-runtime-7.0-debuginfo | 7.0.12-1.el8_8 | |
dotnet-sdk-6.0-debuginfo | 6.0.123-1.el8_8 | |
dotnet-sdk-6.0-source-built-artifacts | 6.0.123-1.el8_8 | [RHSA-2023:5710](https://access.redhat.com/errata/RHSA-2023:5710) | <div class="adv_s">Security Advisory</div> ([CVE-2023-44487](https://access.redhat.com/security/cve/CVE-2023-44487))
dotnet-sdk-7.0-debuginfo | 7.0.112-1.el8_8 | |
dotnet-sdk-7.0-source-built-artifacts | 7.0.112-1.el8_8 | [RHSA-2023:5709](https://access.redhat.com/errata/RHSA-2023:5709) | <div class="adv_s">Security Advisory</div> ([CVE-2023-44487](https://access.redhat.com/security/cve/CVE-2023-44487))
dotnet6.0-debuginfo | 6.0.123-1.el8_8 | |
dotnet6.0-debugsource | 6.0.123-1.el8_8 | |
dotnet7.0-debuginfo | 7.0.112-1.el8_8 | |
dotnet7.0-debugsource | 7.0.112-1.el8_8 | |

### appstream aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
aspnetcore-runtime-6.0 | 6.0.23-1.el8_8 | [RHSA-2023:5710](https://access.redhat.com/errata/RHSA-2023:5710) | <div class="adv_s">Security Advisory</div> ([CVE-2023-44487](https://access.redhat.com/security/cve/CVE-2023-44487))
aspnetcore-runtime-7.0 | 7.0.12-1.el8_8 | [RHSA-2023:5709](https://access.redhat.com/errata/RHSA-2023:5709) | <div class="adv_s">Security Advisory</div> ([CVE-2023-44487](https://access.redhat.com/security/cve/CVE-2023-44487))
aspnetcore-targeting-pack-6.0 | 6.0.23-1.el8_8 | [RHSA-2023:5710](https://access.redhat.com/errata/RHSA-2023:5710) | <div class="adv_s">Security Advisory</div> ([CVE-2023-44487](https://access.redhat.com/security/cve/CVE-2023-44487))
aspnetcore-targeting-pack-7.0 | 7.0.12-1.el8_8 | [RHSA-2023:5709](https://access.redhat.com/errata/RHSA-2023:5709) | <div class="adv_s">Security Advisory</div> ([CVE-2023-44487](https://access.redhat.com/security/cve/CVE-2023-44487))
dotnet | 7.0.112-1.el8_8 | [RHSA-2023:5709](https://access.redhat.com/errata/RHSA-2023:5709) | <div class="adv_s">Security Advisory</div> ([CVE-2023-44487](https://access.redhat.com/security/cve/CVE-2023-44487))
dotnet-apphost-pack-6.0 | 6.0.23-1.el8_8 | [RHSA-2023:5710](https://access.redhat.com/errata/RHSA-2023:5710) | <div class="adv_s">Security Advisory</div> ([CVE-2023-44487](https://access.redhat.com/security/cve/CVE-2023-44487))
dotnet-apphost-pack-6.0-debuginfo | 6.0.23-1.el8_8 | |
dotnet-apphost-pack-7.0 | 7.0.12-1.el8_8 | [RHSA-2023:5709](https://access.redhat.com/errata/RHSA-2023:5709) | <div class="adv_s">Security Advisory</div> ([CVE-2023-44487](https://access.redhat.com/security/cve/CVE-2023-44487))
dotnet-apphost-pack-7.0-debuginfo | 7.0.12-1.el8_8 | |
dotnet-host | 7.0.12-1.el8_8 | [RHSA-2023:5709](https://access.redhat.com/errata/RHSA-2023:5709) | <div class="adv_s">Security Advisory</div> ([CVE-2023-44487](https://access.redhat.com/security/cve/CVE-2023-44487))
dotnet-host-debuginfo | 7.0.12-1.el8_8 | |
dotnet-hostfxr-6.0 | 6.0.23-1.el8_8 | [RHSA-2023:5710](https://access.redhat.com/errata/RHSA-2023:5710) | <div class="adv_s">Security Advisory</div> ([CVE-2023-44487](https://access.redhat.com/security/cve/CVE-2023-44487))
dotnet-hostfxr-6.0-debuginfo | 6.0.23-1.el8_8 | |
dotnet-hostfxr-7.0 | 7.0.12-1.el8_8 | [RHSA-2023:5709](https://access.redhat.com/errata/RHSA-2023:5709) | <div class="adv_s">Security Advisory</div> ([CVE-2023-44487](https://access.redhat.com/security/cve/CVE-2023-44487))
dotnet-hostfxr-7.0-debuginfo | 7.0.12-1.el8_8 | |
dotnet-runtime-6.0 | 6.0.23-1.el8_8 | [RHSA-2023:5710](https://access.redhat.com/errata/RHSA-2023:5710) | <div class="adv_s">Security Advisory</div> ([CVE-2023-44487](https://access.redhat.com/security/cve/CVE-2023-44487))
dotnet-runtime-6.0-debuginfo | 6.0.23-1.el8_8 | |
dotnet-runtime-7.0 | 7.0.12-1.el8_8 | [RHSA-2023:5709](https://access.redhat.com/errata/RHSA-2023:5709) | <div class="adv_s">Security Advisory</div> ([CVE-2023-44487](https://access.redhat.com/security/cve/CVE-2023-44487))
dotnet-runtime-7.0-debuginfo | 7.0.12-1.el8_8 | |
dotnet-sdk-6.0 | 6.0.123-1.el8_8 | [RHSA-2023:5710](https://access.redhat.com/errata/RHSA-2023:5710) | <div class="adv_s">Security Advisory</div> ([CVE-2023-44487](https://access.redhat.com/security/cve/CVE-2023-44487))
dotnet-sdk-6.0-debuginfo | 6.0.123-1.el8_8 | |
dotnet-sdk-7.0 | 7.0.112-1.el8_8 | [RHSA-2023:5709](https://access.redhat.com/errata/RHSA-2023:5709) | <div class="adv_s">Security Advisory</div> ([CVE-2023-44487](https://access.redhat.com/security/cve/CVE-2023-44487))
dotnet-sdk-7.0-debuginfo | 7.0.112-1.el8_8 | |
dotnet-targeting-pack-6.0 | 6.0.23-1.el8_8 | [RHSA-2023:5710](https://access.redhat.com/errata/RHSA-2023:5710) | <div class="adv_s">Security Advisory</div> ([CVE-2023-44487](https://access.redhat.com/security/cve/CVE-2023-44487))
dotnet-targeting-pack-7.0 | 7.0.12-1.el8_8 | [RHSA-2023:5709](https://access.redhat.com/errata/RHSA-2023:5709) | <div class="adv_s">Security Advisory</div> ([CVE-2023-44487](https://access.redhat.com/security/cve/CVE-2023-44487))
dotnet-templates-6.0 | 6.0.123-1.el8_8 | [RHSA-2023:5710](https://access.redhat.com/errata/RHSA-2023:5710) | <div class="adv_s">Security Advisory</div> ([CVE-2023-44487](https://access.redhat.com/security/cve/CVE-2023-44487))
dotnet-templates-7.0 | 7.0.112-1.el8_8 | [RHSA-2023:5709](https://access.redhat.com/errata/RHSA-2023:5709) | <div class="adv_s">Security Advisory</div> ([CVE-2023-44487](https://access.redhat.com/security/cve/CVE-2023-44487))
dotnet6.0-debuginfo | 6.0.123-1.el8_8 | |
dotnet6.0-debugsource | 6.0.123-1.el8_8 | |
dotnet7.0-debuginfo | 7.0.112-1.el8_8 | |
dotnet7.0-debugsource | 7.0.112-1.el8_8 | |
go-toolset | 1.19.13-1.module+el8.8.0+20380+7171fefb | [RHSA-2023:5721](https://access.redhat.com/errata/RHSA-2023:5721) | <div class="adv_s">Security Advisory</div> ([CVE-2023-39325](https://access.redhat.com/security/cve/CVE-2023-39325), [CVE-2023-44487](https://access.redhat.com/security/cve/CVE-2023-44487))
golang | 1.19.13-1.module+el8.8.0+20373+d9cd605c | [RHSA-2023:5721](https://access.redhat.com/errata/RHSA-2023:5721) | <div class="adv_s">Security Advisory</div> ([CVE-2023-39325](https://access.redhat.com/security/cve/CVE-2023-39325), [CVE-2023-44487](https://access.redhat.com/security/cve/CVE-2023-44487))
golang-bin | 1.19.13-1.module+el8.8.0+20373+d9cd605c | [RHSA-2023:5721](https://access.redhat.com/errata/RHSA-2023:5721) | <div class="adv_s">Security Advisory</div> ([CVE-2023-39325](https://access.redhat.com/security/cve/CVE-2023-39325), [CVE-2023-44487](https://access.redhat.com/security/cve/CVE-2023-44487))
golang-docs | 1.19.13-1.module+el8.8.0+20373+d9cd605c | [RHSA-2023:5721](https://access.redhat.com/errata/RHSA-2023:5721) | <div class="adv_s">Security Advisory</div> ([CVE-2023-39325](https://access.redhat.com/security/cve/CVE-2023-39325), [CVE-2023-44487](https://access.redhat.com/security/cve/CVE-2023-44487))
golang-misc | 1.19.13-1.module+el8.8.0+20373+d9cd605c | [RHSA-2023:5721](https://access.redhat.com/errata/RHSA-2023:5721) | <div class="adv_s">Security Advisory</div> ([CVE-2023-39325](https://access.redhat.com/security/cve/CVE-2023-39325), [CVE-2023-44487](https://access.redhat.com/security/cve/CVE-2023-44487))
golang-src | 1.19.13-1.module+el8.8.0+20373+d9cd605c | [RHSA-2023:5721](https://access.redhat.com/errata/RHSA-2023:5721) | <div class="adv_s">Security Advisory</div> ([CVE-2023-39325](https://access.redhat.com/security/cve/CVE-2023-39325), [CVE-2023-44487](https://access.redhat.com/security/cve/CVE-2023-44487))
golang-tests | 1.19.13-1.module+el8.8.0+20373+d9cd605c | [RHSA-2023:5721](https://access.redhat.com/errata/RHSA-2023:5721) | <div class="adv_s">Security Advisory</div> ([CVE-2023-39325](https://access.redhat.com/security/cve/CVE-2023-39325), [CVE-2023-44487](https://access.redhat.com/security/cve/CVE-2023-44487))
netstandard-targeting-pack-2.1 | 7.0.112-1.el8_8 | [RHSA-2023:5709](https://access.redhat.com/errata/RHSA-2023:5709) | <div class="adv_s">Security Advisory</div> ([CVE-2023-44487](https://access.redhat.com/security/cve/CVE-2023-44487))
nginx | 1.20.1-1.module+el8.8.0+20359+9bd89172.1 | [RHSA-2023:5712](https://access.redhat.com/errata/RHSA-2023:5712) | <div class="adv_s">Security Advisory</div> ([CVE-2023-44487](https://access.redhat.com/security/cve/CVE-2023-44487))
nginx | 1.22.1-1.module+el8.8.0+20355+6d9c8a63.1 | [RHSA-2023:5713](https://access.redhat.com/errata/RHSA-2023:5713) | <div class="adv_s">Security Advisory</div> ([CVE-2023-44487](https://access.redhat.com/security/cve/CVE-2023-44487))
nginx-all-modules | 1.20.1-1.module+el8.8.0+20359+9bd89172.1 | [RHSA-2023:5712](https://access.redhat.com/errata/RHSA-2023:5712) | <div class="adv_s">Security Advisory</div> ([CVE-2023-44487](https://access.redhat.com/security/cve/CVE-2023-44487))
nginx-all-modules | 1.22.1-1.module+el8.8.0+20355+6d9c8a63.1 | [RHSA-2023:5713](https://access.redhat.com/errata/RHSA-2023:5713) | <div class="adv_s">Security Advisory</div> ([CVE-2023-44487](https://access.redhat.com/security/cve/CVE-2023-44487))
nginx-debuginfo | 1.20.1-1.module+el8.8.0+20359+9bd89172.1 | |
nginx-debuginfo | 1.22.1-1.module+el8.8.0+20355+6d9c8a63.1 | |
nginx-debugsource | 1.20.1-1.module+el8.8.0+20359+9bd89172.1 | |
nginx-debugsource | 1.22.1-1.module+el8.8.0+20355+6d9c8a63.1 | |
nginx-filesystem | 1.20.1-1.module+el8.8.0+20359+9bd89172.1 | [RHSA-2023:5712](https://access.redhat.com/errata/RHSA-2023:5712) | <div class="adv_s">Security Advisory</div> ([CVE-2023-44487](https://access.redhat.com/security/cve/CVE-2023-44487))
nginx-filesystem | 1.22.1-1.module+el8.8.0+20355+6d9c8a63.1 | [RHSA-2023:5713](https://access.redhat.com/errata/RHSA-2023:5713) | <div class="adv_s">Security Advisory</div> ([CVE-2023-44487](https://access.redhat.com/security/cve/CVE-2023-44487))
nginx-mod-devel | 1.20.1-1.module+el8.8.0+20359+9bd89172.1 | [RHSA-2023:5712](https://access.redhat.com/errata/RHSA-2023:5712) | <div class="adv_s">Security Advisory</div> ([CVE-2023-44487](https://access.redhat.com/security/cve/CVE-2023-44487))
nginx-mod-devel | 1.22.1-1.module+el8.8.0+20355+6d9c8a63.1 | [RHSA-2023:5713](https://access.redhat.com/errata/RHSA-2023:5713) | <div class="adv_s">Security Advisory</div> ([CVE-2023-44487](https://access.redhat.com/security/cve/CVE-2023-44487))
nginx-mod-http-image-filter | 1.20.1-1.module+el8.8.0+20359+9bd89172.1 | [RHSA-2023:5712](https://access.redhat.com/errata/RHSA-2023:5712) | <div class="adv_s">Security Advisory</div> ([CVE-2023-44487](https://access.redhat.com/security/cve/CVE-2023-44487))
nginx-mod-http-image-filter | 1.22.1-1.module+el8.8.0+20355+6d9c8a63.1 | [RHSA-2023:5713](https://access.redhat.com/errata/RHSA-2023:5713) | <div class="adv_s">Security Advisory</div> ([CVE-2023-44487](https://access.redhat.com/security/cve/CVE-2023-44487))
nginx-mod-http-image-filter-debuginfo | 1.20.1-1.module+el8.8.0+20359+9bd89172.1 | |
nginx-mod-http-image-filter-debuginfo | 1.22.1-1.module+el8.8.0+20355+6d9c8a63.1 | |
nginx-mod-http-perl | 1.20.1-1.module+el8.8.0+20359+9bd89172.1 | [RHSA-2023:5712](https://access.redhat.com/errata/RHSA-2023:5712) | <div class="adv_s">Security Advisory</div> ([CVE-2023-44487](https://access.redhat.com/security/cve/CVE-2023-44487))
nginx-mod-http-perl | 1.22.1-1.module+el8.8.0+20355+6d9c8a63.1 | [RHSA-2023:5713](https://access.redhat.com/errata/RHSA-2023:5713) | <div class="adv_s">Security Advisory</div> ([CVE-2023-44487](https://access.redhat.com/security/cve/CVE-2023-44487))
nginx-mod-http-perl-debuginfo | 1.20.1-1.module+el8.8.0+20359+9bd89172.1 | |
nginx-mod-http-perl-debuginfo | 1.22.1-1.module+el8.8.0+20355+6d9c8a63.1 | |
nginx-mod-http-xslt-filter | 1.20.1-1.module+el8.8.0+20359+9bd89172.1 | [RHSA-2023:5712](https://access.redhat.com/errata/RHSA-2023:5712) | <div class="adv_s">Security Advisory</div> ([CVE-2023-44487](https://access.redhat.com/security/cve/CVE-2023-44487))
nginx-mod-http-xslt-filter | 1.22.1-1.module+el8.8.0+20355+6d9c8a63.1 | [RHSA-2023:5713](https://access.redhat.com/errata/RHSA-2023:5713) | <div class="adv_s">Security Advisory</div> ([CVE-2023-44487](https://access.redhat.com/security/cve/CVE-2023-44487))
nginx-mod-http-xslt-filter-debuginfo | 1.20.1-1.module+el8.8.0+20359+9bd89172.1 | |
nginx-mod-http-xslt-filter-debuginfo | 1.22.1-1.module+el8.8.0+20355+6d9c8a63.1 | |
nginx-mod-mail | 1.20.1-1.module+el8.8.0+20359+9bd89172.1 | [RHSA-2023:5712](https://access.redhat.com/errata/RHSA-2023:5712) | <div class="adv_s">Security Advisory</div> ([CVE-2023-44487](https://access.redhat.com/security/cve/CVE-2023-44487))
nginx-mod-mail | 1.22.1-1.module+el8.8.0+20355+6d9c8a63.1 | [RHSA-2023:5713](https://access.redhat.com/errata/RHSA-2023:5713) | <div class="adv_s">Security Advisory</div> ([CVE-2023-44487](https://access.redhat.com/security/cve/CVE-2023-44487))
nginx-mod-mail-debuginfo | 1.20.1-1.module+el8.8.0+20359+9bd89172.1 | |
nginx-mod-mail-debuginfo | 1.22.1-1.module+el8.8.0+20355+6d9c8a63.1 | |
nginx-mod-stream | 1.20.1-1.module+el8.8.0+20359+9bd89172.1 | [RHSA-2023:5712](https://access.redhat.com/errata/RHSA-2023:5712) | <div class="adv_s">Security Advisory</div> ([CVE-2023-44487](https://access.redhat.com/security/cve/CVE-2023-44487))
nginx-mod-stream | 1.22.1-1.module+el8.8.0+20355+6d9c8a63.1 | [RHSA-2023:5713](https://access.redhat.com/errata/RHSA-2023:5713) | <div class="adv_s">Security Advisory</div> ([CVE-2023-44487](https://access.redhat.com/security/cve/CVE-2023-44487))
nginx-mod-stream-debuginfo | 1.20.1-1.module+el8.8.0+20359+9bd89172.1 | |
nginx-mod-stream-debuginfo | 1.22.1-1.module+el8.8.0+20355+6d9c8a63.1 | |

### codeready-builder aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
dotnet-apphost-pack-6.0-debuginfo | 6.0.23-1.el8_8 | |
dotnet-apphost-pack-7.0-debuginfo | 7.0.12-1.el8_8 | |
dotnet-host-debuginfo | 7.0.12-1.el8_8 | |
dotnet-hostfxr-6.0-debuginfo | 6.0.23-1.el8_8 | |
dotnet-hostfxr-7.0-debuginfo | 7.0.12-1.el8_8 | |
dotnet-runtime-6.0-debuginfo | 6.0.23-1.el8_8 | |
dotnet-runtime-7.0-debuginfo | 7.0.12-1.el8_8 | |
dotnet-sdk-6.0-debuginfo | 6.0.123-1.el8_8 | |
dotnet-sdk-6.0-source-built-artifacts | 6.0.123-1.el8_8 | [RHSA-2023:5710](https://access.redhat.com/errata/RHSA-2023:5710) | <div class="adv_s">Security Advisory</div> ([CVE-2023-44487](https://access.redhat.com/security/cve/CVE-2023-44487))
dotnet-sdk-7.0-debuginfo | 7.0.112-1.el8_8 | |
dotnet-sdk-7.0-source-built-artifacts | 7.0.112-1.el8_8 | [RHSA-2023:5709](https://access.redhat.com/errata/RHSA-2023:5709) | <div class="adv_s">Security Advisory</div> ([CVE-2023-44487](https://access.redhat.com/security/cve/CVE-2023-44487))
dotnet6.0-debuginfo | 6.0.123-1.el8_8 | |
dotnet6.0-debugsource | 6.0.123-1.el8_8 | |
dotnet7.0-debuginfo | 7.0.112-1.el8_8 | |
dotnet7.0-debugsource | 7.0.112-1.el8_8 | |

