## 2022-12-08

### appstream x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
rhc | 0.2.1-10.el8_7 | |

### appstream aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
rhc | 0.2.1-10.el8_7 | |

