## 2024-02-13

### appstream x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
nss | 3.90.0-6.el8_9 | [RHSA-2024:0786](https://access.redhat.com/errata/RHSA-2024:0786) | <div class="adv_s">Security Advisory</div> ([CVE-2023-6135](https://access.redhat.com/security/cve/CVE-2023-6135))
nss-debuginfo | 3.90.0-6.el8_9 | |
nss-debugsource | 3.90.0-6.el8_9 | |
nss-devel | 3.90.0-6.el8_9 | [RHSA-2024:0786](https://access.redhat.com/errata/RHSA-2024:0786) | <div class="adv_s">Security Advisory</div> ([CVE-2023-6135](https://access.redhat.com/security/cve/CVE-2023-6135))
nss-softokn | 3.90.0-6.el8_9 | [RHSA-2024:0786](https://access.redhat.com/errata/RHSA-2024:0786) | <div class="adv_s">Security Advisory</div> ([CVE-2023-6135](https://access.redhat.com/security/cve/CVE-2023-6135))
nss-softokn-debuginfo | 3.90.0-6.el8_9 | |
nss-softokn-devel | 3.90.0-6.el8_9 | [RHSA-2024:0786](https://access.redhat.com/errata/RHSA-2024:0786) | <div class="adv_s">Security Advisory</div> ([CVE-2023-6135](https://access.redhat.com/security/cve/CVE-2023-6135))
nss-softokn-freebl | 3.90.0-6.el8_9 | [RHSA-2024:0786](https://access.redhat.com/errata/RHSA-2024:0786) | <div class="adv_s">Security Advisory</div> ([CVE-2023-6135](https://access.redhat.com/security/cve/CVE-2023-6135))
nss-softokn-freebl-debuginfo | 3.90.0-6.el8_9 | |
nss-softokn-freebl-devel | 3.90.0-6.el8_9 | [RHSA-2024:0786](https://access.redhat.com/errata/RHSA-2024:0786) | <div class="adv_s">Security Advisory</div> ([CVE-2023-6135](https://access.redhat.com/security/cve/CVE-2023-6135))
nss-sysinit | 3.90.0-6.el8_9 | [RHSA-2024:0786](https://access.redhat.com/errata/RHSA-2024:0786) | <div class="adv_s">Security Advisory</div> ([CVE-2023-6135](https://access.redhat.com/security/cve/CVE-2023-6135))
nss-sysinit-debuginfo | 3.90.0-6.el8_9 | |
nss-tools | 3.90.0-6.el8_9 | [RHSA-2024:0786](https://access.redhat.com/errata/RHSA-2024:0786) | <div class="adv_s">Security Advisory</div> ([CVE-2023-6135](https://access.redhat.com/security/cve/CVE-2023-6135))
nss-tools-debuginfo | 3.90.0-6.el8_9 | |
nss-util | 3.90.0-6.el8_9 | [RHSA-2024:0786](https://access.redhat.com/errata/RHSA-2024:0786) | <div class="adv_s">Security Advisory</div> ([CVE-2023-6135](https://access.redhat.com/security/cve/CVE-2023-6135))
nss-util-debuginfo | 3.90.0-6.el8_9 | |
nss-util-devel | 3.90.0-6.el8_9 | [RHSA-2024:0786](https://access.redhat.com/errata/RHSA-2024:0786) | <div class="adv_s">Security Advisory</div> ([CVE-2023-6135](https://access.redhat.com/security/cve/CVE-2023-6135))
osbuild | 93-1.el8_9.1 | [RHBA-2024:0788](https://access.redhat.com/errata/RHBA-2024:0788) | <div class="adv_b">Bug Fix Advisory</div>
osbuild-luks2 | 93-1.el8_9.1 | [RHBA-2024:0788](https://access.redhat.com/errata/RHBA-2024:0788) | <div class="adv_b">Bug Fix Advisory</div>
osbuild-lvm2 | 93-1.el8_9.1 | [RHBA-2024:0788](https://access.redhat.com/errata/RHBA-2024:0788) | <div class="adv_b">Bug Fix Advisory</div>
osbuild-ostree | 93-1.el8_9.1 | [RHBA-2024:0788](https://access.redhat.com/errata/RHBA-2024:0788) | <div class="adv_b">Bug Fix Advisory</div>
osbuild-selinux | 93-1.el8_9.1 | [RHBA-2024:0788](https://access.redhat.com/errata/RHBA-2024:0788) | <div class="adv_b">Bug Fix Advisory</div>
python3-osbuild | 93-1.el8_9.1 | [RHBA-2024:0788](https://access.redhat.com/errata/RHBA-2024:0788) | <div class="adv_b">Bug Fix Advisory</div>

### appstream aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
nss | 3.90.0-6.el8_9 | [RHSA-2024:0786](https://access.redhat.com/errata/RHSA-2024:0786) | <div class="adv_s">Security Advisory</div> ([CVE-2023-6135](https://access.redhat.com/security/cve/CVE-2023-6135))
nss-debuginfo | 3.90.0-6.el8_9 | |
nss-debugsource | 3.90.0-6.el8_9 | |
nss-devel | 3.90.0-6.el8_9 | [RHSA-2024:0786](https://access.redhat.com/errata/RHSA-2024:0786) | <div class="adv_s">Security Advisory</div> ([CVE-2023-6135](https://access.redhat.com/security/cve/CVE-2023-6135))
nss-softokn | 3.90.0-6.el8_9 | [RHSA-2024:0786](https://access.redhat.com/errata/RHSA-2024:0786) | <div class="adv_s">Security Advisory</div> ([CVE-2023-6135](https://access.redhat.com/security/cve/CVE-2023-6135))
nss-softokn-debuginfo | 3.90.0-6.el8_9 | |
nss-softokn-devel | 3.90.0-6.el8_9 | [RHSA-2024:0786](https://access.redhat.com/errata/RHSA-2024:0786) | <div class="adv_s">Security Advisory</div> ([CVE-2023-6135](https://access.redhat.com/security/cve/CVE-2023-6135))
nss-softokn-freebl | 3.90.0-6.el8_9 | [RHSA-2024:0786](https://access.redhat.com/errata/RHSA-2024:0786) | <div class="adv_s">Security Advisory</div> ([CVE-2023-6135](https://access.redhat.com/security/cve/CVE-2023-6135))
nss-softokn-freebl-debuginfo | 3.90.0-6.el8_9 | |
nss-softokn-freebl-devel | 3.90.0-6.el8_9 | [RHSA-2024:0786](https://access.redhat.com/errata/RHSA-2024:0786) | <div class="adv_s">Security Advisory</div> ([CVE-2023-6135](https://access.redhat.com/security/cve/CVE-2023-6135))
nss-sysinit | 3.90.0-6.el8_9 | [RHSA-2024:0786](https://access.redhat.com/errata/RHSA-2024:0786) | <div class="adv_s">Security Advisory</div> ([CVE-2023-6135](https://access.redhat.com/security/cve/CVE-2023-6135))
nss-sysinit-debuginfo | 3.90.0-6.el8_9 | |
nss-tools | 3.90.0-6.el8_9 | [RHSA-2024:0786](https://access.redhat.com/errata/RHSA-2024:0786) | <div class="adv_s">Security Advisory</div> ([CVE-2023-6135](https://access.redhat.com/security/cve/CVE-2023-6135))
nss-tools-debuginfo | 3.90.0-6.el8_9 | |
nss-util | 3.90.0-6.el8_9 | [RHSA-2024:0786](https://access.redhat.com/errata/RHSA-2024:0786) | <div class="adv_s">Security Advisory</div> ([CVE-2023-6135](https://access.redhat.com/security/cve/CVE-2023-6135))
nss-util-debuginfo | 3.90.0-6.el8_9 | |
nss-util-devel | 3.90.0-6.el8_9 | [RHSA-2024:0786](https://access.redhat.com/errata/RHSA-2024:0786) | <div class="adv_s">Security Advisory</div> ([CVE-2023-6135](https://access.redhat.com/security/cve/CVE-2023-6135))
osbuild | 93-1.el8_9.1 | [RHBA-2024:0788](https://access.redhat.com/errata/RHBA-2024:0788) | <div class="adv_b">Bug Fix Advisory</div>
osbuild-luks2 | 93-1.el8_9.1 | [RHBA-2024:0788](https://access.redhat.com/errata/RHBA-2024:0788) | <div class="adv_b">Bug Fix Advisory</div>
osbuild-lvm2 | 93-1.el8_9.1 | [RHBA-2024:0788](https://access.redhat.com/errata/RHBA-2024:0788) | <div class="adv_b">Bug Fix Advisory</div>
osbuild-ostree | 93-1.el8_9.1 | [RHBA-2024:0788](https://access.redhat.com/errata/RHBA-2024:0788) | <div class="adv_b">Bug Fix Advisory</div>
osbuild-selinux | 93-1.el8_9.1 | [RHBA-2024:0788](https://access.redhat.com/errata/RHBA-2024:0788) | <div class="adv_b">Bug Fix Advisory</div>
python3-osbuild | 93-1.el8_9.1 | [RHBA-2024:0788](https://access.redhat.com/errata/RHBA-2024:0788) | <div class="adv_b">Bug Fix Advisory</div>

