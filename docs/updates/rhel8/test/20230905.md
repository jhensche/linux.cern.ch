## 2023-09-05

### appstream x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
firefox | 102.15.0-1.el8_8 | [RHSA-2023:4952](https://access.redhat.com/errata/RHSA-2023:4952) | <div class="adv_s">Security Advisory</div> ([CVE-2023-4051](https://access.redhat.com/security/cve/CVE-2023-4051), [CVE-2023-4053](https://access.redhat.com/security/cve/CVE-2023-4053), [CVE-2023-4573](https://access.redhat.com/security/cve/CVE-2023-4573), [CVE-2023-4574](https://access.redhat.com/security/cve/CVE-2023-4574), [CVE-2023-4575](https://access.redhat.com/security/cve/CVE-2023-4575), [CVE-2023-4577](https://access.redhat.com/security/cve/CVE-2023-4577), [CVE-2023-4578](https://access.redhat.com/security/cve/CVE-2023-4578), [CVE-2023-4580](https://access.redhat.com/security/cve/CVE-2023-4580), [CVE-2023-4581](https://access.redhat.com/security/cve/CVE-2023-4581), [CVE-2023-4583](https://access.redhat.com/security/cve/CVE-2023-4583), [CVE-2023-4584](https://access.redhat.com/security/cve/CVE-2023-4584), [CVE-2023-4585](https://access.redhat.com/security/cve/CVE-2023-4585))
firefox-debuginfo | 102.15.0-1.el8_8 | |
firefox-debugsource | 102.15.0-1.el8_8 | |
thunderbird | 102.15.0-1.el8_8 | [RHSA-2023:4954](https://access.redhat.com/errata/RHSA-2023:4954) | <div class="adv_s">Security Advisory</div> ([CVE-2023-4051](https://access.redhat.com/security/cve/CVE-2023-4051), [CVE-2023-4053](https://access.redhat.com/security/cve/CVE-2023-4053), [CVE-2023-4573](https://access.redhat.com/security/cve/CVE-2023-4573), [CVE-2023-4574](https://access.redhat.com/security/cve/CVE-2023-4574), [CVE-2023-4575](https://access.redhat.com/security/cve/CVE-2023-4575), [CVE-2023-4577](https://access.redhat.com/security/cve/CVE-2023-4577), [CVE-2023-4578](https://access.redhat.com/security/cve/CVE-2023-4578), [CVE-2023-4580](https://access.redhat.com/security/cve/CVE-2023-4580), [CVE-2023-4581](https://access.redhat.com/security/cve/CVE-2023-4581), [CVE-2023-4583](https://access.redhat.com/security/cve/CVE-2023-4583), [CVE-2023-4584](https://access.redhat.com/security/cve/CVE-2023-4584), [CVE-2023-4585](https://access.redhat.com/security/cve/CVE-2023-4585))
thunderbird-debuginfo | 102.15.0-1.el8_8 | |
thunderbird-debugsource | 102.15.0-1.el8_8 | |

### appstream aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
firefox | 102.15.0-1.el8_8 | [RHSA-2023:4952](https://access.redhat.com/errata/RHSA-2023:4952) | <div class="adv_s">Security Advisory</div> ([CVE-2023-4051](https://access.redhat.com/security/cve/CVE-2023-4051), [CVE-2023-4053](https://access.redhat.com/security/cve/CVE-2023-4053), [CVE-2023-4573](https://access.redhat.com/security/cve/CVE-2023-4573), [CVE-2023-4574](https://access.redhat.com/security/cve/CVE-2023-4574), [CVE-2023-4575](https://access.redhat.com/security/cve/CVE-2023-4575), [CVE-2023-4577](https://access.redhat.com/security/cve/CVE-2023-4577), [CVE-2023-4578](https://access.redhat.com/security/cve/CVE-2023-4578), [CVE-2023-4580](https://access.redhat.com/security/cve/CVE-2023-4580), [CVE-2023-4581](https://access.redhat.com/security/cve/CVE-2023-4581), [CVE-2023-4583](https://access.redhat.com/security/cve/CVE-2023-4583), [CVE-2023-4584](https://access.redhat.com/security/cve/CVE-2023-4584), [CVE-2023-4585](https://access.redhat.com/security/cve/CVE-2023-4585))
firefox-debuginfo | 102.15.0-1.el8_8 | |
firefox-debugsource | 102.15.0-1.el8_8 | |
thunderbird | 102.15.0-1.el8_8 | [RHSA-2023:4954](https://access.redhat.com/errata/RHSA-2023:4954) | <div class="adv_s">Security Advisory</div> ([CVE-2023-4051](https://access.redhat.com/security/cve/CVE-2023-4051), [CVE-2023-4053](https://access.redhat.com/security/cve/CVE-2023-4053), [CVE-2023-4573](https://access.redhat.com/security/cve/CVE-2023-4573), [CVE-2023-4574](https://access.redhat.com/security/cve/CVE-2023-4574), [CVE-2023-4575](https://access.redhat.com/security/cve/CVE-2023-4575), [CVE-2023-4577](https://access.redhat.com/security/cve/CVE-2023-4577), [CVE-2023-4578](https://access.redhat.com/security/cve/CVE-2023-4578), [CVE-2023-4580](https://access.redhat.com/security/cve/CVE-2023-4580), [CVE-2023-4581](https://access.redhat.com/security/cve/CVE-2023-4581), [CVE-2023-4583](https://access.redhat.com/security/cve/CVE-2023-4583), [CVE-2023-4584](https://access.redhat.com/security/cve/CVE-2023-4584), [CVE-2023-4585](https://access.redhat.com/security/cve/CVE-2023-4585))
thunderbird-debuginfo | 102.15.0-1.el8_8 | |
thunderbird-debugsource | 102.15.0-1.el8_8 | |

