## 2023-08-01

### baseos x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
ctdb | 4.17.5-3.el8_8 | [RHSA-2023:4328](https://access.redhat.com/errata/RHSA-2023:4328) | <div class="adv_s">Security Advisory</div> ([CVE-2023-3347](https://access.redhat.com/security/cve/CVE-2023-3347))
ctdb-debuginfo | 4.17.5-3.el8_8 | |
libnetapi | 4.17.5-3.el8_8 | [RHSA-2023:4328](https://access.redhat.com/errata/RHSA-2023:4328) | <div class="adv_s">Security Advisory</div> ([CVE-2023-3347](https://access.redhat.com/security/cve/CVE-2023-3347))
libnetapi-debuginfo | 4.17.5-3.el8_8 | |
libsmbclient | 4.17.5-3.el8_8 | [RHSA-2023:4328](https://access.redhat.com/errata/RHSA-2023:4328) | <div class="adv_s">Security Advisory</div> ([CVE-2023-3347](https://access.redhat.com/security/cve/CVE-2023-3347))
libsmbclient-debuginfo | 4.17.5-3.el8_8 | |
libwbclient | 4.17.5-3.el8_8 | [RHSA-2023:4328](https://access.redhat.com/errata/RHSA-2023:4328) | <div class="adv_s">Security Advisory</div> ([CVE-2023-3347](https://access.redhat.com/security/cve/CVE-2023-3347))
libwbclient-debuginfo | 4.17.5-3.el8_8 | |
python3-samba | 4.17.5-3.el8_8 | [RHSA-2023:4328](https://access.redhat.com/errata/RHSA-2023:4328) | <div class="adv_s">Security Advisory</div> ([CVE-2023-3347](https://access.redhat.com/security/cve/CVE-2023-3347))
python3-samba-dc | 4.17.5-3.el8_8 | [RHSA-2023:4328](https://access.redhat.com/errata/RHSA-2023:4328) | <div class="adv_s">Security Advisory</div> ([CVE-2023-3347](https://access.redhat.com/security/cve/CVE-2023-3347))
python3-samba-dc-debuginfo | 4.17.5-3.el8_8 | |
python3-samba-debuginfo | 4.17.5-3.el8_8 | |
python3-samba-test | 4.17.5-3.el8_8 | [RHSA-2023:4328](https://access.redhat.com/errata/RHSA-2023:4328) | <div class="adv_s">Security Advisory</div> ([CVE-2023-3347](https://access.redhat.com/security/cve/CVE-2023-3347))
samba | 4.17.5-3.el8_8 | [RHSA-2023:4328](https://access.redhat.com/errata/RHSA-2023:4328) | <div class="adv_s">Security Advisory</div> ([CVE-2023-3347](https://access.redhat.com/security/cve/CVE-2023-3347))
samba-client | 4.17.5-3.el8_8 | [RHSA-2023:4328](https://access.redhat.com/errata/RHSA-2023:4328) | <div class="adv_s">Security Advisory</div> ([CVE-2023-3347](https://access.redhat.com/security/cve/CVE-2023-3347))
samba-client-debuginfo | 4.17.5-3.el8_8 | |
samba-client-libs | 4.17.5-3.el8_8 | [RHSA-2023:4328](https://access.redhat.com/errata/RHSA-2023:4328) | <div class="adv_s">Security Advisory</div> ([CVE-2023-3347](https://access.redhat.com/security/cve/CVE-2023-3347))
samba-client-libs-debuginfo | 4.17.5-3.el8_8 | |
samba-common | 4.17.5-3.el8_8 | [RHSA-2023:4328](https://access.redhat.com/errata/RHSA-2023:4328) | <div class="adv_s">Security Advisory</div> ([CVE-2023-3347](https://access.redhat.com/security/cve/CVE-2023-3347))
samba-common-libs | 4.17.5-3.el8_8 | [RHSA-2023:4328](https://access.redhat.com/errata/RHSA-2023:4328) | <div class="adv_s">Security Advisory</div> ([CVE-2023-3347](https://access.redhat.com/security/cve/CVE-2023-3347))
samba-common-libs-debuginfo | 4.17.5-3.el8_8 | |
samba-common-tools | 4.17.5-3.el8_8 | [RHSA-2023:4328](https://access.redhat.com/errata/RHSA-2023:4328) | <div class="adv_s">Security Advisory</div> ([CVE-2023-3347](https://access.redhat.com/security/cve/CVE-2023-3347))
samba-common-tools-debuginfo | 4.17.5-3.el8_8 | |
samba-dc-libs | 4.17.5-3.el8_8 | [RHSA-2023:4328](https://access.redhat.com/errata/RHSA-2023:4328) | <div class="adv_s">Security Advisory</div> ([CVE-2023-3347](https://access.redhat.com/security/cve/CVE-2023-3347))
samba-dc-libs-debuginfo | 4.17.5-3.el8_8 | |
samba-dcerpc | 4.17.5-3.el8_8 | [RHSA-2023:4328](https://access.redhat.com/errata/RHSA-2023:4328) | <div class="adv_s">Security Advisory</div> ([CVE-2023-3347](https://access.redhat.com/security/cve/CVE-2023-3347))
samba-dcerpc-debuginfo | 4.17.5-3.el8_8 | |
samba-debuginfo | 4.17.5-3.el8_8 | |
samba-debugsource | 4.17.5-3.el8_8 | |
samba-krb5-printing | 4.17.5-3.el8_8 | [RHSA-2023:4328](https://access.redhat.com/errata/RHSA-2023:4328) | <div class="adv_s">Security Advisory</div> ([CVE-2023-3347](https://access.redhat.com/security/cve/CVE-2023-3347))
samba-krb5-printing-debuginfo | 4.17.5-3.el8_8 | |
samba-ldb-ldap-modules | 4.17.5-3.el8_8 | [RHSA-2023:4328](https://access.redhat.com/errata/RHSA-2023:4328) | <div class="adv_s">Security Advisory</div> ([CVE-2023-3347](https://access.redhat.com/security/cve/CVE-2023-3347))
samba-ldb-ldap-modules-debuginfo | 4.17.5-3.el8_8 | |
samba-libs | 4.17.5-3.el8_8 | [RHSA-2023:4328](https://access.redhat.com/errata/RHSA-2023:4328) | <div class="adv_s">Security Advisory</div> ([CVE-2023-3347](https://access.redhat.com/security/cve/CVE-2023-3347))
samba-libs-debuginfo | 4.17.5-3.el8_8 | |
samba-pidl | 4.17.5-3.el8_8 | [RHSA-2023:4328](https://access.redhat.com/errata/RHSA-2023:4328) | <div class="adv_s">Security Advisory</div> ([CVE-2023-3347](https://access.redhat.com/security/cve/CVE-2023-3347))
samba-test | 4.17.5-3.el8_8 | [RHSA-2023:4328](https://access.redhat.com/errata/RHSA-2023:4328) | <div class="adv_s">Security Advisory</div> ([CVE-2023-3347](https://access.redhat.com/security/cve/CVE-2023-3347))
samba-test-debuginfo | 4.17.5-3.el8_8 | |
samba-test-libs | 4.17.5-3.el8_8 | [RHSA-2023:4328](https://access.redhat.com/errata/RHSA-2023:4328) | <div class="adv_s">Security Advisory</div> ([CVE-2023-3347](https://access.redhat.com/security/cve/CVE-2023-3347))
samba-test-libs-debuginfo | 4.17.5-3.el8_8 | |
samba-tools | 4.17.5-3.el8_8 | [RHSA-2023:4328](https://access.redhat.com/errata/RHSA-2023:4328) | <div class="adv_s">Security Advisory</div> ([CVE-2023-3347](https://access.redhat.com/security/cve/CVE-2023-3347))
samba-usershares | 4.17.5-3.el8_8 | [RHSA-2023:4328](https://access.redhat.com/errata/RHSA-2023:4328) | <div class="adv_s">Security Advisory</div> ([CVE-2023-3347](https://access.redhat.com/security/cve/CVE-2023-3347))
samba-vfs-iouring-debuginfo | 4.17.5-3.el8_8 | |
samba-winbind | 4.17.5-3.el8_8 | [RHSA-2023:4328](https://access.redhat.com/errata/RHSA-2023:4328) | <div class="adv_s">Security Advisory</div> ([CVE-2023-3347](https://access.redhat.com/security/cve/CVE-2023-3347))
samba-winbind-clients | 4.17.5-3.el8_8 | [RHSA-2023:4328](https://access.redhat.com/errata/RHSA-2023:4328) | <div class="adv_s">Security Advisory</div> ([CVE-2023-3347](https://access.redhat.com/security/cve/CVE-2023-3347))
samba-winbind-clients-debuginfo | 4.17.5-3.el8_8 | |
samba-winbind-debuginfo | 4.17.5-3.el8_8 | |
samba-winbind-krb5-locator | 4.17.5-3.el8_8 | [RHSA-2023:4328](https://access.redhat.com/errata/RHSA-2023:4328) | <div class="adv_s">Security Advisory</div> ([CVE-2023-3347](https://access.redhat.com/security/cve/CVE-2023-3347))
samba-winbind-krb5-locator-debuginfo | 4.17.5-3.el8_8 | |
samba-winbind-modules | 4.17.5-3.el8_8 | [RHSA-2023:4328](https://access.redhat.com/errata/RHSA-2023:4328) | <div class="adv_s">Security Advisory</div> ([CVE-2023-3347](https://access.redhat.com/security/cve/CVE-2023-3347))
samba-winbind-modules-debuginfo | 4.17.5-3.el8_8 | |
samba-winexe | 4.17.5-3.el8_8 | [RHSA-2023:4328](https://access.redhat.com/errata/RHSA-2023:4328) | <div class="adv_s">Security Advisory</div> ([CVE-2023-3347](https://access.redhat.com/security/cve/CVE-2023-3347))
samba-winexe-debuginfo | 4.17.5-3.el8_8 | |

### appstream x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
ctdb-debuginfo | 4.17.5-3.el8_8 | |
libnetapi-debuginfo | 4.17.5-3.el8_8 | |
libsmbclient-debuginfo | 4.17.5-3.el8_8 | |
libwbclient-debuginfo | 4.17.5-3.el8_8 | |
python3-samba-dc-debuginfo | 4.17.5-3.el8_8 | |
python3-samba-debuginfo | 4.17.5-3.el8_8 | |
samba-client-debuginfo | 4.17.5-3.el8_8 | |
samba-client-libs-debuginfo | 4.17.5-3.el8_8 | |
samba-common-libs-debuginfo | 4.17.5-3.el8_8 | |
samba-common-tools-debuginfo | 4.17.5-3.el8_8 | |
samba-dc-libs-debuginfo | 4.17.5-3.el8_8 | |
samba-dcerpc-debuginfo | 4.17.5-3.el8_8 | |
samba-debuginfo | 4.17.5-3.el8_8 | |
samba-debugsource | 4.17.5-3.el8_8 | |
samba-krb5-printing-debuginfo | 4.17.5-3.el8_8 | |
samba-ldb-ldap-modules-debuginfo | 4.17.5-3.el8_8 | |
samba-libs-debuginfo | 4.17.5-3.el8_8 | |
samba-test-debuginfo | 4.17.5-3.el8_8 | |
samba-test-libs-debuginfo | 4.17.5-3.el8_8 | |
samba-vfs-iouring | 4.17.5-3.el8_8 | [RHSA-2023:4328](https://access.redhat.com/errata/RHSA-2023:4328) | <div class="adv_s">Security Advisory</div> ([CVE-2023-3347](https://access.redhat.com/security/cve/CVE-2023-3347))
samba-vfs-iouring-debuginfo | 4.17.5-3.el8_8 | |
samba-winbind-clients-debuginfo | 4.17.5-3.el8_8 | |
samba-winbind-debuginfo | 4.17.5-3.el8_8 | |
samba-winbind-krb5-locator-debuginfo | 4.17.5-3.el8_8 | |
samba-winbind-modules-debuginfo | 4.17.5-3.el8_8 | |
samba-winexe-debuginfo | 4.17.5-3.el8_8 | |

### codeready-builder x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
ctdb-debuginfo | 4.17.5-3.el8_8 | |
libnetapi-debuginfo | 4.17.5-3.el8_8 | |
libnetapi-devel | 4.17.5-3.el8_8 | [RHSA-2023:4328](https://access.redhat.com/errata/RHSA-2023:4328) | <div class="adv_s">Security Advisory</div> ([CVE-2023-3347](https://access.redhat.com/security/cve/CVE-2023-3347))
libsmbclient-debuginfo | 4.17.5-3.el8_8 | |
libsmbclient-devel | 4.17.5-3.el8_8 | [RHSA-2023:4328](https://access.redhat.com/errata/RHSA-2023:4328) | <div class="adv_s">Security Advisory</div> ([CVE-2023-3347](https://access.redhat.com/security/cve/CVE-2023-3347))
libwbclient-debuginfo | 4.17.5-3.el8_8 | |
libwbclient-devel | 4.17.5-3.el8_8 | [RHSA-2023:4328](https://access.redhat.com/errata/RHSA-2023:4328) | <div class="adv_s">Security Advisory</div> ([CVE-2023-3347](https://access.redhat.com/security/cve/CVE-2023-3347))
python3-samba-dc-debuginfo | 4.17.5-3.el8_8 | |
python3-samba-debuginfo | 4.17.5-3.el8_8 | |
python3-samba-devel | 4.17.5-3.el8_8 | [RHSA-2023:4328](https://access.redhat.com/errata/RHSA-2023:4328) | <div class="adv_s">Security Advisory</div> ([CVE-2023-3347](https://access.redhat.com/security/cve/CVE-2023-3347))
samba-client-debuginfo | 4.17.5-3.el8_8 | |
samba-client-libs-debuginfo | 4.17.5-3.el8_8 | |
samba-common-libs-debuginfo | 4.17.5-3.el8_8 | |
samba-common-tools-debuginfo | 4.17.5-3.el8_8 | |
samba-dc-libs-debuginfo | 4.17.5-3.el8_8 | |
samba-dcerpc-debuginfo | 4.17.5-3.el8_8 | |
samba-debuginfo | 4.17.5-3.el8_8 | |
samba-debugsource | 4.17.5-3.el8_8 | |
samba-devel | 4.17.5-3.el8_8 | [RHSA-2023:4328](https://access.redhat.com/errata/RHSA-2023:4328) | <div class="adv_s">Security Advisory</div> ([CVE-2023-3347](https://access.redhat.com/security/cve/CVE-2023-3347))
samba-krb5-printing-debuginfo | 4.17.5-3.el8_8 | |
samba-ldb-ldap-modules-debuginfo | 4.17.5-3.el8_8 | |
samba-libs-debuginfo | 4.17.5-3.el8_8 | |
samba-test-debuginfo | 4.17.5-3.el8_8 | |
samba-test-libs-debuginfo | 4.17.5-3.el8_8 | |
samba-vfs-iouring-debuginfo | 4.17.5-3.el8_8 | |
samba-winbind-clients-debuginfo | 4.17.5-3.el8_8 | |
samba-winbind-debuginfo | 4.17.5-3.el8_8 | |
samba-winbind-krb5-locator-debuginfo | 4.17.5-3.el8_8 | |
samba-winbind-modules-debuginfo | 4.17.5-3.el8_8 | |
samba-winexe-debuginfo | 4.17.5-3.el8_8 | |

### baseos aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
ctdb | 4.17.5-3.el8_8 | [RHSA-2023:4328](https://access.redhat.com/errata/RHSA-2023:4328) | <div class="adv_s">Security Advisory</div> ([CVE-2023-3347](https://access.redhat.com/security/cve/CVE-2023-3347))
ctdb-debuginfo | 4.17.5-3.el8_8 | |
libnetapi | 4.17.5-3.el8_8 | [RHSA-2023:4328](https://access.redhat.com/errata/RHSA-2023:4328) | <div class="adv_s">Security Advisory</div> ([CVE-2023-3347](https://access.redhat.com/security/cve/CVE-2023-3347))
libnetapi-debuginfo | 4.17.5-3.el8_8 | |
libsmbclient | 4.17.5-3.el8_8 | [RHSA-2023:4328](https://access.redhat.com/errata/RHSA-2023:4328) | <div class="adv_s">Security Advisory</div> ([CVE-2023-3347](https://access.redhat.com/security/cve/CVE-2023-3347))
libsmbclient-debuginfo | 4.17.5-3.el8_8 | |
libwbclient | 4.17.5-3.el8_8 | [RHSA-2023:4328](https://access.redhat.com/errata/RHSA-2023:4328) | <div class="adv_s">Security Advisory</div> ([CVE-2023-3347](https://access.redhat.com/security/cve/CVE-2023-3347))
libwbclient-debuginfo | 4.17.5-3.el8_8 | |
python3-samba | 4.17.5-3.el8_8 | [RHSA-2023:4328](https://access.redhat.com/errata/RHSA-2023:4328) | <div class="adv_s">Security Advisory</div> ([CVE-2023-3347](https://access.redhat.com/security/cve/CVE-2023-3347))
python3-samba-dc | 4.17.5-3.el8_8 | [RHSA-2023:4328](https://access.redhat.com/errata/RHSA-2023:4328) | <div class="adv_s">Security Advisory</div> ([CVE-2023-3347](https://access.redhat.com/security/cve/CVE-2023-3347))
python3-samba-dc-debuginfo | 4.17.5-3.el8_8 | |
python3-samba-debuginfo | 4.17.5-3.el8_8 | |
python3-samba-test | 4.17.5-3.el8_8 | [RHSA-2023:4328](https://access.redhat.com/errata/RHSA-2023:4328) | <div class="adv_s">Security Advisory</div> ([CVE-2023-3347](https://access.redhat.com/security/cve/CVE-2023-3347))
samba | 4.17.5-3.el8_8 | [RHSA-2023:4328](https://access.redhat.com/errata/RHSA-2023:4328) | <div class="adv_s">Security Advisory</div> ([CVE-2023-3347](https://access.redhat.com/security/cve/CVE-2023-3347))
samba-client | 4.17.5-3.el8_8 | [RHSA-2023:4328](https://access.redhat.com/errata/RHSA-2023:4328) | <div class="adv_s">Security Advisory</div> ([CVE-2023-3347](https://access.redhat.com/security/cve/CVE-2023-3347))
samba-client-debuginfo | 4.17.5-3.el8_8 | |
samba-client-libs | 4.17.5-3.el8_8 | [RHSA-2023:4328](https://access.redhat.com/errata/RHSA-2023:4328) | <div class="adv_s">Security Advisory</div> ([CVE-2023-3347](https://access.redhat.com/security/cve/CVE-2023-3347))
samba-client-libs-debuginfo | 4.17.5-3.el8_8 | |
samba-common | 4.17.5-3.el8_8 | [RHSA-2023:4328](https://access.redhat.com/errata/RHSA-2023:4328) | <div class="adv_s">Security Advisory</div> ([CVE-2023-3347](https://access.redhat.com/security/cve/CVE-2023-3347))
samba-common-libs | 4.17.5-3.el8_8 | [RHSA-2023:4328](https://access.redhat.com/errata/RHSA-2023:4328) | <div class="adv_s">Security Advisory</div> ([CVE-2023-3347](https://access.redhat.com/security/cve/CVE-2023-3347))
samba-common-libs-debuginfo | 4.17.5-3.el8_8 | |
samba-common-tools | 4.17.5-3.el8_8 | [RHSA-2023:4328](https://access.redhat.com/errata/RHSA-2023:4328) | <div class="adv_s">Security Advisory</div> ([CVE-2023-3347](https://access.redhat.com/security/cve/CVE-2023-3347))
samba-common-tools-debuginfo | 4.17.5-3.el8_8 | |
samba-dc-libs | 4.17.5-3.el8_8 | [RHSA-2023:4328](https://access.redhat.com/errata/RHSA-2023:4328) | <div class="adv_s">Security Advisory</div> ([CVE-2023-3347](https://access.redhat.com/security/cve/CVE-2023-3347))
samba-dc-libs-debuginfo | 4.17.5-3.el8_8 | |
samba-dcerpc | 4.17.5-3.el8_8 | [RHSA-2023:4328](https://access.redhat.com/errata/RHSA-2023:4328) | <div class="adv_s">Security Advisory</div> ([CVE-2023-3347](https://access.redhat.com/security/cve/CVE-2023-3347))
samba-dcerpc-debuginfo | 4.17.5-3.el8_8 | |
samba-debuginfo | 4.17.5-3.el8_8 | |
samba-debugsource | 4.17.5-3.el8_8 | |
samba-krb5-printing | 4.17.5-3.el8_8 | [RHSA-2023:4328](https://access.redhat.com/errata/RHSA-2023:4328) | <div class="adv_s">Security Advisory</div> ([CVE-2023-3347](https://access.redhat.com/security/cve/CVE-2023-3347))
samba-krb5-printing-debuginfo | 4.17.5-3.el8_8 | |
samba-ldb-ldap-modules | 4.17.5-3.el8_8 | [RHSA-2023:4328](https://access.redhat.com/errata/RHSA-2023:4328) | <div class="adv_s">Security Advisory</div> ([CVE-2023-3347](https://access.redhat.com/security/cve/CVE-2023-3347))
samba-ldb-ldap-modules-debuginfo | 4.17.5-3.el8_8 | |
samba-libs | 4.17.5-3.el8_8 | [RHSA-2023:4328](https://access.redhat.com/errata/RHSA-2023:4328) | <div class="adv_s">Security Advisory</div> ([CVE-2023-3347](https://access.redhat.com/security/cve/CVE-2023-3347))
samba-libs-debuginfo | 4.17.5-3.el8_8 | |
samba-pidl | 4.17.5-3.el8_8 | [RHSA-2023:4328](https://access.redhat.com/errata/RHSA-2023:4328) | <div class="adv_s">Security Advisory</div> ([CVE-2023-3347](https://access.redhat.com/security/cve/CVE-2023-3347))
samba-test | 4.17.5-3.el8_8 | [RHSA-2023:4328](https://access.redhat.com/errata/RHSA-2023:4328) | <div class="adv_s">Security Advisory</div> ([CVE-2023-3347](https://access.redhat.com/security/cve/CVE-2023-3347))
samba-test-debuginfo | 4.17.5-3.el8_8 | |
samba-test-libs | 4.17.5-3.el8_8 | [RHSA-2023:4328](https://access.redhat.com/errata/RHSA-2023:4328) | <div class="adv_s">Security Advisory</div> ([CVE-2023-3347](https://access.redhat.com/security/cve/CVE-2023-3347))
samba-test-libs-debuginfo | 4.17.5-3.el8_8 | |
samba-tools | 4.17.5-3.el8_8 | [RHSA-2023:4328](https://access.redhat.com/errata/RHSA-2023:4328) | <div class="adv_s">Security Advisory</div> ([CVE-2023-3347](https://access.redhat.com/security/cve/CVE-2023-3347))
samba-usershares | 4.17.5-3.el8_8 | [RHSA-2023:4328](https://access.redhat.com/errata/RHSA-2023:4328) | <div class="adv_s">Security Advisory</div> ([CVE-2023-3347](https://access.redhat.com/security/cve/CVE-2023-3347))
samba-vfs-iouring-debuginfo | 4.17.5-3.el8_8 | |
samba-winbind | 4.17.5-3.el8_8 | [RHSA-2023:4328](https://access.redhat.com/errata/RHSA-2023:4328) | <div class="adv_s">Security Advisory</div> ([CVE-2023-3347](https://access.redhat.com/security/cve/CVE-2023-3347))
samba-winbind-clients | 4.17.5-3.el8_8 | [RHSA-2023:4328](https://access.redhat.com/errata/RHSA-2023:4328) | <div class="adv_s">Security Advisory</div> ([CVE-2023-3347](https://access.redhat.com/security/cve/CVE-2023-3347))
samba-winbind-clients-debuginfo | 4.17.5-3.el8_8 | |
samba-winbind-debuginfo | 4.17.5-3.el8_8 | |
samba-winbind-krb5-locator | 4.17.5-3.el8_8 | [RHSA-2023:4328](https://access.redhat.com/errata/RHSA-2023:4328) | <div class="adv_s">Security Advisory</div> ([CVE-2023-3347](https://access.redhat.com/security/cve/CVE-2023-3347))
samba-winbind-krb5-locator-debuginfo | 4.17.5-3.el8_8 | |
samba-winbind-modules | 4.17.5-3.el8_8 | [RHSA-2023:4328](https://access.redhat.com/errata/RHSA-2023:4328) | <div class="adv_s">Security Advisory</div> ([CVE-2023-3347](https://access.redhat.com/security/cve/CVE-2023-3347))
samba-winbind-modules-debuginfo | 4.17.5-3.el8_8 | |

### appstream aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
ctdb-debuginfo | 4.17.5-3.el8_8 | |
libnetapi-debuginfo | 4.17.5-3.el8_8 | |
libsmbclient-debuginfo | 4.17.5-3.el8_8 | |
libwbclient-debuginfo | 4.17.5-3.el8_8 | |
python3-samba-dc-debuginfo | 4.17.5-3.el8_8 | |
python3-samba-debuginfo | 4.17.5-3.el8_8 | |
samba-client-debuginfo | 4.17.5-3.el8_8 | |
samba-client-libs-debuginfo | 4.17.5-3.el8_8 | |
samba-common-libs-debuginfo | 4.17.5-3.el8_8 | |
samba-common-tools-debuginfo | 4.17.5-3.el8_8 | |
samba-dc-libs-debuginfo | 4.17.5-3.el8_8 | |
samba-dcerpc-debuginfo | 4.17.5-3.el8_8 | |
samba-debuginfo | 4.17.5-3.el8_8 | |
samba-debugsource | 4.17.5-3.el8_8 | |
samba-krb5-printing-debuginfo | 4.17.5-3.el8_8 | |
samba-ldb-ldap-modules-debuginfo | 4.17.5-3.el8_8 | |
samba-libs-debuginfo | 4.17.5-3.el8_8 | |
samba-test-debuginfo | 4.17.5-3.el8_8 | |
samba-test-libs-debuginfo | 4.17.5-3.el8_8 | |
samba-vfs-iouring | 4.17.5-3.el8_8 | [RHSA-2023:4328](https://access.redhat.com/errata/RHSA-2023:4328) | <div class="adv_s">Security Advisory</div> ([CVE-2023-3347](https://access.redhat.com/security/cve/CVE-2023-3347))
samba-vfs-iouring-debuginfo | 4.17.5-3.el8_8 | |
samba-winbind-clients-debuginfo | 4.17.5-3.el8_8 | |
samba-winbind-debuginfo | 4.17.5-3.el8_8 | |
samba-winbind-krb5-locator-debuginfo | 4.17.5-3.el8_8 | |
samba-winbind-modules-debuginfo | 4.17.5-3.el8_8 | |

### codeready-builder aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
ctdb-debuginfo | 4.17.5-3.el8_8 | |
libnetapi-debuginfo | 4.17.5-3.el8_8 | |
libnetapi-devel | 4.17.5-3.el8_8 | [RHSA-2023:4328](https://access.redhat.com/errata/RHSA-2023:4328) | <div class="adv_s">Security Advisory</div> ([CVE-2023-3347](https://access.redhat.com/security/cve/CVE-2023-3347))
libsmbclient-debuginfo | 4.17.5-3.el8_8 | |
libsmbclient-devel | 4.17.5-3.el8_8 | [RHSA-2023:4328](https://access.redhat.com/errata/RHSA-2023:4328) | <div class="adv_s">Security Advisory</div> ([CVE-2023-3347](https://access.redhat.com/security/cve/CVE-2023-3347))
libwbclient-debuginfo | 4.17.5-3.el8_8 | |
libwbclient-devel | 4.17.5-3.el8_8 | [RHSA-2023:4328](https://access.redhat.com/errata/RHSA-2023:4328) | <div class="adv_s">Security Advisory</div> ([CVE-2023-3347](https://access.redhat.com/security/cve/CVE-2023-3347))
python3-samba-dc-debuginfo | 4.17.5-3.el8_8 | |
python3-samba-debuginfo | 4.17.5-3.el8_8 | |
python3-samba-devel | 4.17.5-3.el8_8 | [RHSA-2023:4328](https://access.redhat.com/errata/RHSA-2023:4328) | <div class="adv_s">Security Advisory</div> ([CVE-2023-3347](https://access.redhat.com/security/cve/CVE-2023-3347))
samba-client-debuginfo | 4.17.5-3.el8_8 | |
samba-client-libs-debuginfo | 4.17.5-3.el8_8 | |
samba-common-libs-debuginfo | 4.17.5-3.el8_8 | |
samba-common-tools-debuginfo | 4.17.5-3.el8_8 | |
samba-dc-libs-debuginfo | 4.17.5-3.el8_8 | |
samba-dcerpc-debuginfo | 4.17.5-3.el8_8 | |
samba-debuginfo | 4.17.5-3.el8_8 | |
samba-debugsource | 4.17.5-3.el8_8 | |
samba-devel | 4.17.5-3.el8_8 | [RHSA-2023:4328](https://access.redhat.com/errata/RHSA-2023:4328) | <div class="adv_s">Security Advisory</div> ([CVE-2023-3347](https://access.redhat.com/security/cve/CVE-2023-3347))
samba-krb5-printing-debuginfo | 4.17.5-3.el8_8 | |
samba-ldb-ldap-modules-debuginfo | 4.17.5-3.el8_8 | |
samba-libs-debuginfo | 4.17.5-3.el8_8 | |
samba-test-debuginfo | 4.17.5-3.el8_8 | |
samba-test-libs-debuginfo | 4.17.5-3.el8_8 | |
samba-vfs-iouring-debuginfo | 4.17.5-3.el8_8 | |
samba-winbind-clients-debuginfo | 4.17.5-3.el8_8 | |
samba-winbind-debuginfo | 4.17.5-3.el8_8 | |
samba-winbind-krb5-locator-debuginfo | 4.17.5-3.el8_8 | |
samba-winbind-modules-debuginfo | 4.17.5-3.el8_8 | |

