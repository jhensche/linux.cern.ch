## 2023-08-15

### appstream x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
aspnetcore-runtime-6.0 | 6.0.21-1.el9_2 | [RHSA-2023:4644](https://access.redhat.com/errata/RHSA-2023:4644) | <div class="adv_s">Security Advisory</div> ([CVE-2023-35390](https://access.redhat.com/security/cve/CVE-2023-35390), [CVE-2023-38180](https://access.redhat.com/security/cve/CVE-2023-38180))
aspnetcore-runtime-7.0 | 7.0.10-1.el9_2 | [RHSA-2023:4642](https://access.redhat.com/errata/RHSA-2023:4642) | <div class="adv_s">Security Advisory</div> ([CVE-2023-35390](https://access.redhat.com/security/cve/CVE-2023-35390), [CVE-2023-38180](https://access.redhat.com/security/cve/CVE-2023-38180))
aspnetcore-targeting-pack-6.0 | 6.0.21-1.el9_2 | [RHSA-2023:4644](https://access.redhat.com/errata/RHSA-2023:4644) | <div class="adv_s">Security Advisory</div> ([CVE-2023-35390](https://access.redhat.com/security/cve/CVE-2023-35390), [CVE-2023-38180](https://access.redhat.com/security/cve/CVE-2023-38180))
aspnetcore-targeting-pack-7.0 | 7.0.10-1.el9_2 | [RHSA-2023:4642](https://access.redhat.com/errata/RHSA-2023:4642) | <div class="adv_s">Security Advisory</div> ([CVE-2023-35390](https://access.redhat.com/security/cve/CVE-2023-35390), [CVE-2023-38180](https://access.redhat.com/security/cve/CVE-2023-38180))
cargo | 1.66.1-2.el9_2 | [RHSA-2023:4634](https://access.redhat.com/errata/RHSA-2023:4634) | <div class="adv_s">Security Advisory</div> ([CVE-2023-38497](https://access.redhat.com/security/cve/CVE-2023-38497))
cargo-debuginfo | 1.66.1-2.el9_2 | |
clippy | 1.66.1-2.el9_2 | [RHSA-2023:4634](https://access.redhat.com/errata/RHSA-2023:4634) | <div class="adv_s">Security Advisory</div> ([CVE-2023-38497](https://access.redhat.com/security/cve/CVE-2023-38497))
clippy-debuginfo | 1.66.1-2.el9_2 | |
dotnet-apphost-pack-6.0 | 6.0.21-1.el9_2 | [RHSA-2023:4644](https://access.redhat.com/errata/RHSA-2023:4644) | <div class="adv_s">Security Advisory</div> ([CVE-2023-35390](https://access.redhat.com/security/cve/CVE-2023-35390), [CVE-2023-38180](https://access.redhat.com/security/cve/CVE-2023-38180))
dotnet-apphost-pack-6.0-debuginfo | 6.0.21-1.el9_2 | |
dotnet-apphost-pack-7.0 | 7.0.10-1.el9_2 | [RHSA-2023:4642](https://access.redhat.com/errata/RHSA-2023:4642) | <div class="adv_s">Security Advisory</div> ([CVE-2023-35390](https://access.redhat.com/security/cve/CVE-2023-35390), [CVE-2023-38180](https://access.redhat.com/security/cve/CVE-2023-38180))
dotnet-apphost-pack-7.0-debuginfo | 7.0.10-1.el9_2 | |
dotnet-host | 7.0.10-1.el9_2 | [RHSA-2023:4642](https://access.redhat.com/errata/RHSA-2023:4642) | <div class="adv_s">Security Advisory</div> ([CVE-2023-35390](https://access.redhat.com/security/cve/CVE-2023-35390), [CVE-2023-38180](https://access.redhat.com/security/cve/CVE-2023-38180))
dotnet-host-debuginfo | 7.0.10-1.el9_2 | |
dotnet-hostfxr-6.0 | 6.0.21-1.el9_2 | [RHSA-2023:4644](https://access.redhat.com/errata/RHSA-2023:4644) | <div class="adv_s">Security Advisory</div> ([CVE-2023-35390](https://access.redhat.com/security/cve/CVE-2023-35390), [CVE-2023-38180](https://access.redhat.com/security/cve/CVE-2023-38180))
dotnet-hostfxr-6.0-debuginfo | 6.0.21-1.el9_2 | |
dotnet-hostfxr-7.0 | 7.0.10-1.el9_2 | [RHSA-2023:4642](https://access.redhat.com/errata/RHSA-2023:4642) | <div class="adv_s">Security Advisory</div> ([CVE-2023-35390](https://access.redhat.com/security/cve/CVE-2023-35390), [CVE-2023-38180](https://access.redhat.com/security/cve/CVE-2023-38180))
dotnet-hostfxr-7.0-debuginfo | 7.0.10-1.el9_2 | |
dotnet-runtime-6.0 | 6.0.21-1.el9_2 | [RHSA-2023:4644](https://access.redhat.com/errata/RHSA-2023:4644) | <div class="adv_s">Security Advisory</div> ([CVE-2023-35390](https://access.redhat.com/security/cve/CVE-2023-35390), [CVE-2023-38180](https://access.redhat.com/security/cve/CVE-2023-38180))
dotnet-runtime-6.0-debuginfo | 6.0.21-1.el9_2 | |
dotnet-runtime-7.0 | 7.0.10-1.el9_2 | [RHSA-2023:4642](https://access.redhat.com/errata/RHSA-2023:4642) | <div class="adv_s">Security Advisory</div> ([CVE-2023-35390](https://access.redhat.com/security/cve/CVE-2023-35390), [CVE-2023-38180](https://access.redhat.com/security/cve/CVE-2023-38180))
dotnet-runtime-7.0-debuginfo | 7.0.10-1.el9_2 | |
dotnet-sdk-6.0 | 6.0.121-1.el9_2 | [RHSA-2023:4644](https://access.redhat.com/errata/RHSA-2023:4644) | <div class="adv_s">Security Advisory</div> ([CVE-2023-35390](https://access.redhat.com/security/cve/CVE-2023-35390), [CVE-2023-38180](https://access.redhat.com/security/cve/CVE-2023-38180))
dotnet-sdk-6.0-debuginfo | 6.0.121-1.el9_2 | |
dotnet-sdk-7.0 | 7.0.110-1.el9_2 | [RHSA-2023:4642](https://access.redhat.com/errata/RHSA-2023:4642) | <div class="adv_s">Security Advisory</div> ([CVE-2023-35390](https://access.redhat.com/security/cve/CVE-2023-35390), [CVE-2023-38180](https://access.redhat.com/security/cve/CVE-2023-38180))
dotnet-sdk-7.0-debuginfo | 7.0.110-1.el9_2 | |
dotnet-targeting-pack-6.0 | 6.0.21-1.el9_2 | [RHSA-2023:4644](https://access.redhat.com/errata/RHSA-2023:4644) | <div class="adv_s">Security Advisory</div> ([CVE-2023-35390](https://access.redhat.com/security/cve/CVE-2023-35390), [CVE-2023-38180](https://access.redhat.com/security/cve/CVE-2023-38180))
dotnet-targeting-pack-7.0 | 7.0.10-1.el9_2 | [RHSA-2023:4642](https://access.redhat.com/errata/RHSA-2023:4642) | <div class="adv_s">Security Advisory</div> ([CVE-2023-35390](https://access.redhat.com/security/cve/CVE-2023-35390), [CVE-2023-38180](https://access.redhat.com/security/cve/CVE-2023-38180))
dotnet-templates-6.0 | 6.0.121-1.el9_2 | [RHSA-2023:4644](https://access.redhat.com/errata/RHSA-2023:4644) | <div class="adv_s">Security Advisory</div> ([CVE-2023-35390](https://access.redhat.com/security/cve/CVE-2023-35390), [CVE-2023-38180](https://access.redhat.com/security/cve/CVE-2023-38180))
dotnet-templates-7.0 | 7.0.110-1.el9_2 | [RHSA-2023:4642](https://access.redhat.com/errata/RHSA-2023:4642) | <div class="adv_s">Security Advisory</div> ([CVE-2023-35390](https://access.redhat.com/security/cve/CVE-2023-35390), [CVE-2023-38180](https://access.redhat.com/security/cve/CVE-2023-38180))
dotnet6.0-debuginfo | 6.0.121-1.el9_2 | |
dotnet6.0-debugsource | 6.0.121-1.el9_2 | |
dotnet7.0-debuginfo | 7.0.110-1.el9_2 | |
dotnet7.0-debugsource | 7.0.110-1.el9_2 | |
netstandard-targeting-pack-2.1 | 7.0.110-1.el9_2 | [RHSA-2023:4642](https://access.redhat.com/errata/RHSA-2023:4642) | <div class="adv_s">Security Advisory</div> ([CVE-2023-35390](https://access.redhat.com/security/cve/CVE-2023-35390), [CVE-2023-38180](https://access.redhat.com/security/cve/CVE-2023-38180))
rust | 1.66.1-2.el9_2 | [RHSA-2023:4634](https://access.redhat.com/errata/RHSA-2023:4634) | <div class="adv_s">Security Advisory</div> ([CVE-2023-38497](https://access.redhat.com/security/cve/CVE-2023-38497))
rust-analysis | 1.66.1-2.el9_2 | [RHSA-2023:4634](https://access.redhat.com/errata/RHSA-2023:4634) | <div class="adv_s">Security Advisory</div> ([CVE-2023-38497](https://access.redhat.com/security/cve/CVE-2023-38497))
rust-analyzer | 1.66.1-2.el9_2 | [RHSA-2023:4634](https://access.redhat.com/errata/RHSA-2023:4634) | <div class="adv_s">Security Advisory</div> ([CVE-2023-38497](https://access.redhat.com/security/cve/CVE-2023-38497))
rust-analyzer-debuginfo | 1.66.1-2.el9_2 | |
rust-debugger-common | 1.66.1-2.el9_2 | [RHSA-2023:4634](https://access.redhat.com/errata/RHSA-2023:4634) | <div class="adv_s">Security Advisory</div> ([CVE-2023-38497](https://access.redhat.com/security/cve/CVE-2023-38497))
rust-debuginfo | 1.66.1-2.el9_2 | |
rust-debugsource | 1.66.1-2.el9_2 | |
rust-doc | 1.66.1-2.el9_2 | [RHSA-2023:4634](https://access.redhat.com/errata/RHSA-2023:4634) | <div class="adv_s">Security Advisory</div> ([CVE-2023-38497](https://access.redhat.com/security/cve/CVE-2023-38497))
rust-gdb | 1.66.1-2.el9_2 | [RHSA-2023:4634](https://access.redhat.com/errata/RHSA-2023:4634) | <div class="adv_s">Security Advisory</div> ([CVE-2023-38497](https://access.redhat.com/security/cve/CVE-2023-38497))
rust-lldb | 1.66.1-2.el9_2 | [RHSA-2023:4634](https://access.redhat.com/errata/RHSA-2023:4634) | <div class="adv_s">Security Advisory</div> ([CVE-2023-38497](https://access.redhat.com/security/cve/CVE-2023-38497))
rust-src | 1.66.1-2.el9_2 | [RHSA-2023:4634](https://access.redhat.com/errata/RHSA-2023:4634) | <div class="adv_s">Security Advisory</div> ([CVE-2023-38497](https://access.redhat.com/security/cve/CVE-2023-38497))
rust-std-static | 1.66.1-2.el9_2 | [RHSA-2023:4634](https://access.redhat.com/errata/RHSA-2023:4634) | <div class="adv_s">Security Advisory</div> ([CVE-2023-38497](https://access.redhat.com/security/cve/CVE-2023-38497))
rust-std-static-wasm32-unknown-unknown | 1.66.1-2.el9_2 | [RHSA-2023:4634](https://access.redhat.com/errata/RHSA-2023:4634) | <div class="adv_s">Security Advisory</div> ([CVE-2023-38497](https://access.redhat.com/security/cve/CVE-2023-38497))
rust-std-static-wasm32-wasi | 1.66.1-2.el9_2 | [RHSA-2023:4634](https://access.redhat.com/errata/RHSA-2023:4634) | <div class="adv_s">Security Advisory</div> ([CVE-2023-38497](https://access.redhat.com/security/cve/CVE-2023-38497))
rust-toolset | 1.66.1-2.el9_2 | [RHSA-2023:4634](https://access.redhat.com/errata/RHSA-2023:4634) | <div class="adv_s">Security Advisory</div> ([CVE-2023-38497](https://access.redhat.com/security/cve/CVE-2023-38497))
rustfmt | 1.66.1-2.el9_2 | [RHSA-2023:4634](https://access.redhat.com/errata/RHSA-2023:4634) | <div class="adv_s">Security Advisory</div> ([CVE-2023-38497](https://access.redhat.com/security/cve/CVE-2023-38497))
rustfmt-debuginfo | 1.66.1-2.el9_2 | |

### codeready-builder x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
dotnet-apphost-pack-6.0-debuginfo | 6.0.21-1.el9_2 | |
dotnet-apphost-pack-7.0-debuginfo | 7.0.10-1.el9_2 | |
dotnet-host-debuginfo | 7.0.10-1.el9_2 | |
dotnet-hostfxr-6.0-debuginfo | 6.0.21-1.el9_2 | |
dotnet-hostfxr-7.0-debuginfo | 7.0.10-1.el9_2 | |
dotnet-runtime-6.0-debuginfo | 6.0.21-1.el9_2 | |
dotnet-runtime-7.0-debuginfo | 7.0.10-1.el9_2 | |
dotnet-sdk-6.0-debuginfo | 6.0.121-1.el9_2 | |
dotnet-sdk-6.0-source-built-artifacts | 6.0.121-1.el9_2 | [RHSA-2023:4644](https://access.redhat.com/errata/RHSA-2023:4644) | <div class="adv_s">Security Advisory</div> ([CVE-2023-35390](https://access.redhat.com/security/cve/CVE-2023-35390), [CVE-2023-38180](https://access.redhat.com/security/cve/CVE-2023-38180))
dotnet-sdk-7.0-debuginfo | 7.0.110-1.el9_2 | |
dotnet-sdk-7.0-source-built-artifacts | 7.0.110-1.el9_2 | [RHSA-2023:4642](https://access.redhat.com/errata/RHSA-2023:4642) | <div class="adv_s">Security Advisory</div> ([CVE-2023-35390](https://access.redhat.com/security/cve/CVE-2023-35390), [CVE-2023-38180](https://access.redhat.com/security/cve/CVE-2023-38180))
dotnet6.0-debuginfo | 6.0.121-1.el9_2 | |
dotnet6.0-debugsource | 6.0.121-1.el9_2 | |
dotnet7.0-debuginfo | 7.0.110-1.el9_2 | |
dotnet7.0-debugsource | 7.0.110-1.el9_2 | |

### appstream aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
aspnetcore-runtime-6.0 | 6.0.21-1.el9_2 | [RHSA-2023:4644](https://access.redhat.com/errata/RHSA-2023:4644) | <div class="adv_s">Security Advisory</div> ([CVE-2023-35390](https://access.redhat.com/security/cve/CVE-2023-35390), [CVE-2023-38180](https://access.redhat.com/security/cve/CVE-2023-38180))
aspnetcore-runtime-7.0 | 7.0.10-1.el9_2 | [RHSA-2023:4642](https://access.redhat.com/errata/RHSA-2023:4642) | <div class="adv_s">Security Advisory</div> ([CVE-2023-35390](https://access.redhat.com/security/cve/CVE-2023-35390), [CVE-2023-38180](https://access.redhat.com/security/cve/CVE-2023-38180))
aspnetcore-targeting-pack-6.0 | 6.0.21-1.el9_2 | [RHSA-2023:4644](https://access.redhat.com/errata/RHSA-2023:4644) | <div class="adv_s">Security Advisory</div> ([CVE-2023-35390](https://access.redhat.com/security/cve/CVE-2023-35390), [CVE-2023-38180](https://access.redhat.com/security/cve/CVE-2023-38180))
aspnetcore-targeting-pack-7.0 | 7.0.10-1.el9_2 | [RHSA-2023:4642](https://access.redhat.com/errata/RHSA-2023:4642) | <div class="adv_s">Security Advisory</div> ([CVE-2023-35390](https://access.redhat.com/security/cve/CVE-2023-35390), [CVE-2023-38180](https://access.redhat.com/security/cve/CVE-2023-38180))
cargo | 1.66.1-2.el9_2 | [RHSA-2023:4634](https://access.redhat.com/errata/RHSA-2023:4634) | <div class="adv_s">Security Advisory</div> ([CVE-2023-38497](https://access.redhat.com/security/cve/CVE-2023-38497))
cargo-debuginfo | 1.66.1-2.el9_2 | |
clippy | 1.66.1-2.el9_2 | [RHSA-2023:4634](https://access.redhat.com/errata/RHSA-2023:4634) | <div class="adv_s">Security Advisory</div> ([CVE-2023-38497](https://access.redhat.com/security/cve/CVE-2023-38497))
clippy-debuginfo | 1.66.1-2.el9_2 | |
dotnet-apphost-pack-6.0 | 6.0.21-1.el9_2 | [RHSA-2023:4644](https://access.redhat.com/errata/RHSA-2023:4644) | <div class="adv_s">Security Advisory</div> ([CVE-2023-35390](https://access.redhat.com/security/cve/CVE-2023-35390), [CVE-2023-38180](https://access.redhat.com/security/cve/CVE-2023-38180))
dotnet-apphost-pack-6.0-debuginfo | 6.0.21-1.el9_2 | |
dotnet-apphost-pack-7.0 | 7.0.10-1.el9_2 | [RHSA-2023:4642](https://access.redhat.com/errata/RHSA-2023:4642) | <div class="adv_s">Security Advisory</div> ([CVE-2023-35390](https://access.redhat.com/security/cve/CVE-2023-35390), [CVE-2023-38180](https://access.redhat.com/security/cve/CVE-2023-38180))
dotnet-apphost-pack-7.0-debuginfo | 7.0.10-1.el9_2 | |
dotnet-host | 7.0.10-1.el9_2 | [RHSA-2023:4642](https://access.redhat.com/errata/RHSA-2023:4642) | <div class="adv_s">Security Advisory</div> ([CVE-2023-35390](https://access.redhat.com/security/cve/CVE-2023-35390), [CVE-2023-38180](https://access.redhat.com/security/cve/CVE-2023-38180))
dotnet-host-debuginfo | 7.0.10-1.el9_2 | |
dotnet-hostfxr-6.0 | 6.0.21-1.el9_2 | [RHSA-2023:4644](https://access.redhat.com/errata/RHSA-2023:4644) | <div class="adv_s">Security Advisory</div> ([CVE-2023-35390](https://access.redhat.com/security/cve/CVE-2023-35390), [CVE-2023-38180](https://access.redhat.com/security/cve/CVE-2023-38180))
dotnet-hostfxr-6.0-debuginfo | 6.0.21-1.el9_2 | |
dotnet-hostfxr-7.0 | 7.0.10-1.el9_2 | [RHSA-2023:4642](https://access.redhat.com/errata/RHSA-2023:4642) | <div class="adv_s">Security Advisory</div> ([CVE-2023-35390](https://access.redhat.com/security/cve/CVE-2023-35390), [CVE-2023-38180](https://access.redhat.com/security/cve/CVE-2023-38180))
dotnet-hostfxr-7.0-debuginfo | 7.0.10-1.el9_2 | |
dotnet-runtime-6.0 | 6.0.21-1.el9_2 | [RHSA-2023:4644](https://access.redhat.com/errata/RHSA-2023:4644) | <div class="adv_s">Security Advisory</div> ([CVE-2023-35390](https://access.redhat.com/security/cve/CVE-2023-35390), [CVE-2023-38180](https://access.redhat.com/security/cve/CVE-2023-38180))
dotnet-runtime-6.0-debuginfo | 6.0.21-1.el9_2 | |
dotnet-runtime-7.0 | 7.0.10-1.el9_2 | [RHSA-2023:4642](https://access.redhat.com/errata/RHSA-2023:4642) | <div class="adv_s">Security Advisory</div> ([CVE-2023-35390](https://access.redhat.com/security/cve/CVE-2023-35390), [CVE-2023-38180](https://access.redhat.com/security/cve/CVE-2023-38180))
dotnet-runtime-7.0-debuginfo | 7.0.10-1.el9_2 | |
dotnet-sdk-6.0 | 6.0.121-1.el9_2 | [RHSA-2023:4644](https://access.redhat.com/errata/RHSA-2023:4644) | <div class="adv_s">Security Advisory</div> ([CVE-2023-35390](https://access.redhat.com/security/cve/CVE-2023-35390), [CVE-2023-38180](https://access.redhat.com/security/cve/CVE-2023-38180))
dotnet-sdk-6.0-debuginfo | 6.0.121-1.el9_2 | |
dotnet-sdk-7.0 | 7.0.110-1.el9_2 | [RHSA-2023:4642](https://access.redhat.com/errata/RHSA-2023:4642) | <div class="adv_s">Security Advisory</div> ([CVE-2023-35390](https://access.redhat.com/security/cve/CVE-2023-35390), [CVE-2023-38180](https://access.redhat.com/security/cve/CVE-2023-38180))
dotnet-sdk-7.0-debuginfo | 7.0.110-1.el9_2 | |
dotnet-targeting-pack-6.0 | 6.0.21-1.el9_2 | [RHSA-2023:4644](https://access.redhat.com/errata/RHSA-2023:4644) | <div class="adv_s">Security Advisory</div> ([CVE-2023-35390](https://access.redhat.com/security/cve/CVE-2023-35390), [CVE-2023-38180](https://access.redhat.com/security/cve/CVE-2023-38180))
dotnet-targeting-pack-7.0 | 7.0.10-1.el9_2 | [RHSA-2023:4642](https://access.redhat.com/errata/RHSA-2023:4642) | <div class="adv_s">Security Advisory</div> ([CVE-2023-35390](https://access.redhat.com/security/cve/CVE-2023-35390), [CVE-2023-38180](https://access.redhat.com/security/cve/CVE-2023-38180))
dotnet-templates-6.0 | 6.0.121-1.el9_2 | [RHSA-2023:4644](https://access.redhat.com/errata/RHSA-2023:4644) | <div class="adv_s">Security Advisory</div> ([CVE-2023-35390](https://access.redhat.com/security/cve/CVE-2023-35390), [CVE-2023-38180](https://access.redhat.com/security/cve/CVE-2023-38180))
dotnet-templates-7.0 | 7.0.110-1.el9_2 | [RHSA-2023:4642](https://access.redhat.com/errata/RHSA-2023:4642) | <div class="adv_s">Security Advisory</div> ([CVE-2023-35390](https://access.redhat.com/security/cve/CVE-2023-35390), [CVE-2023-38180](https://access.redhat.com/security/cve/CVE-2023-38180))
dotnet6.0-debuginfo | 6.0.121-1.el9_2 | |
dotnet6.0-debugsource | 6.0.121-1.el9_2 | |
dotnet7.0-debuginfo | 7.0.110-1.el9_2 | |
dotnet7.0-debugsource | 7.0.110-1.el9_2 | |
netstandard-targeting-pack-2.1 | 7.0.110-1.el9_2 | [RHSA-2023:4642](https://access.redhat.com/errata/RHSA-2023:4642) | <div class="adv_s">Security Advisory</div> ([CVE-2023-35390](https://access.redhat.com/security/cve/CVE-2023-35390), [CVE-2023-38180](https://access.redhat.com/security/cve/CVE-2023-38180))
rust | 1.66.1-2.el9_2 | [RHSA-2023:4634](https://access.redhat.com/errata/RHSA-2023:4634) | <div class="adv_s">Security Advisory</div> ([CVE-2023-38497](https://access.redhat.com/security/cve/CVE-2023-38497))
rust-analysis | 1.66.1-2.el9_2 | [RHSA-2023:4634](https://access.redhat.com/errata/RHSA-2023:4634) | <div class="adv_s">Security Advisory</div> ([CVE-2023-38497](https://access.redhat.com/security/cve/CVE-2023-38497))
rust-analyzer | 1.66.1-2.el9_2 | [RHSA-2023:4634](https://access.redhat.com/errata/RHSA-2023:4634) | <div class="adv_s">Security Advisory</div> ([CVE-2023-38497](https://access.redhat.com/security/cve/CVE-2023-38497))
rust-analyzer-debuginfo | 1.66.1-2.el9_2 | |
rust-debugger-common | 1.66.1-2.el9_2 | [RHSA-2023:4634](https://access.redhat.com/errata/RHSA-2023:4634) | <div class="adv_s">Security Advisory</div> ([CVE-2023-38497](https://access.redhat.com/security/cve/CVE-2023-38497))
rust-debuginfo | 1.66.1-2.el9_2 | |
rust-debugsource | 1.66.1-2.el9_2 | |
rust-doc | 1.66.1-2.el9_2 | [RHSA-2023:4634](https://access.redhat.com/errata/RHSA-2023:4634) | <div class="adv_s">Security Advisory</div> ([CVE-2023-38497](https://access.redhat.com/security/cve/CVE-2023-38497))
rust-gdb | 1.66.1-2.el9_2 | [RHSA-2023:4634](https://access.redhat.com/errata/RHSA-2023:4634) | <div class="adv_s">Security Advisory</div> ([CVE-2023-38497](https://access.redhat.com/security/cve/CVE-2023-38497))
rust-lldb | 1.66.1-2.el9_2 | [RHSA-2023:4634](https://access.redhat.com/errata/RHSA-2023:4634) | <div class="adv_s">Security Advisory</div> ([CVE-2023-38497](https://access.redhat.com/security/cve/CVE-2023-38497))
rust-src | 1.66.1-2.el9_2 | [RHSA-2023:4634](https://access.redhat.com/errata/RHSA-2023:4634) | <div class="adv_s">Security Advisory</div> ([CVE-2023-38497](https://access.redhat.com/security/cve/CVE-2023-38497))
rust-std-static | 1.66.1-2.el9_2 | [RHSA-2023:4634](https://access.redhat.com/errata/RHSA-2023:4634) | <div class="adv_s">Security Advisory</div> ([CVE-2023-38497](https://access.redhat.com/security/cve/CVE-2023-38497))
rust-std-static-wasm32-unknown-unknown | 1.66.1-2.el9_2 | [RHSA-2023:4634](https://access.redhat.com/errata/RHSA-2023:4634) | <div class="adv_s">Security Advisory</div> ([CVE-2023-38497](https://access.redhat.com/security/cve/CVE-2023-38497))
rust-std-static-wasm32-wasi | 1.66.1-2.el9_2 | [RHSA-2023:4634](https://access.redhat.com/errata/RHSA-2023:4634) | <div class="adv_s">Security Advisory</div> ([CVE-2023-38497](https://access.redhat.com/security/cve/CVE-2023-38497))
rust-toolset | 1.66.1-2.el9_2 | [RHSA-2023:4634](https://access.redhat.com/errata/RHSA-2023:4634) | <div class="adv_s">Security Advisory</div> ([CVE-2023-38497](https://access.redhat.com/security/cve/CVE-2023-38497))
rustfmt | 1.66.1-2.el9_2 | [RHSA-2023:4634](https://access.redhat.com/errata/RHSA-2023:4634) | <div class="adv_s">Security Advisory</div> ([CVE-2023-38497](https://access.redhat.com/security/cve/CVE-2023-38497))
rustfmt-debuginfo | 1.66.1-2.el9_2 | |

### codeready-builder aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
dotnet-apphost-pack-6.0-debuginfo | 6.0.21-1.el9_2 | |
dotnet-apphost-pack-7.0-debuginfo | 7.0.10-1.el9_2 | |
dotnet-host-debuginfo | 7.0.10-1.el9_2 | |
dotnet-hostfxr-6.0-debuginfo | 6.0.21-1.el9_2 | |
dotnet-hostfxr-7.0-debuginfo | 7.0.10-1.el9_2 | |
dotnet-runtime-6.0-debuginfo | 6.0.21-1.el9_2 | |
dotnet-runtime-7.0-debuginfo | 7.0.10-1.el9_2 | |
dotnet-sdk-6.0-debuginfo | 6.0.121-1.el9_2 | |
dotnet-sdk-6.0-source-built-artifacts | 6.0.121-1.el9_2 | [RHSA-2023:4644](https://access.redhat.com/errata/RHSA-2023:4644) | <div class="adv_s">Security Advisory</div> ([CVE-2023-35390](https://access.redhat.com/security/cve/CVE-2023-35390), [CVE-2023-38180](https://access.redhat.com/security/cve/CVE-2023-38180))
dotnet-sdk-7.0-debuginfo | 7.0.110-1.el9_2 | |
dotnet-sdk-7.0-source-built-artifacts | 7.0.110-1.el9_2 | [RHSA-2023:4642](https://access.redhat.com/errata/RHSA-2023:4642) | <div class="adv_s">Security Advisory</div> ([CVE-2023-35390](https://access.redhat.com/security/cve/CVE-2023-35390), [CVE-2023-38180](https://access.redhat.com/security/cve/CVE-2023-38180))
dotnet6.0-debuginfo | 6.0.121-1.el9_2 | |
dotnet6.0-debugsource | 6.0.121-1.el9_2 | |
dotnet7.0-debuginfo | 7.0.110-1.el9_2 | |
dotnet7.0-debugsource | 7.0.110-1.el9_2 | |

