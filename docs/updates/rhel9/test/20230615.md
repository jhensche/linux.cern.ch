## 2023-06-15

### baseos x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
python3 | 3.9.16-1.el9_2.1 | |
python3-libs | 3.9.16-1.el9_2.1 | |
python3.9-debuginfo | 3.9.16-1.el9_2.1 | |
python3.9-debugsource | 3.9.16-1.el9_2.1 | |

### appstream x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
aspnetcore-runtime-6.0 | 6.0.18-1.el9_2 | [RHSA-2023:3581](https://access.redhat.com/errata/RHSA-2023:3581) | <div class="adv_s">Security Advisory</div> ([CVE-2023-24936](https://access.redhat.com/security/cve/CVE-2023-24936), [CVE-2023-29331](https://access.redhat.com/security/cve/CVE-2023-29331), [CVE-2023-29337](https://access.redhat.com/security/cve/CVE-2023-29337), [CVE-2023-33128](https://access.redhat.com/security/cve/CVE-2023-33128))
aspnetcore-runtime-7.0 | 7.0.7-1.el9_2 | [RHSA-2023:3592](https://access.redhat.com/errata/RHSA-2023:3592) | <div class="adv_s">Security Advisory</div> ([CVE-2023-24936](https://access.redhat.com/security/cve/CVE-2023-24936), [CVE-2023-29331](https://access.redhat.com/security/cve/CVE-2023-29331), [CVE-2023-29337](https://access.redhat.com/security/cve/CVE-2023-29337), [CVE-2023-32032](https://access.redhat.com/security/cve/CVE-2023-32032), [CVE-2023-33128](https://access.redhat.com/security/cve/CVE-2023-33128))
aspnetcore-targeting-pack-6.0 | 6.0.18-1.el9_2 | [RHSA-2023:3581](https://access.redhat.com/errata/RHSA-2023:3581) | <div class="adv_s">Security Advisory</div> ([CVE-2023-24936](https://access.redhat.com/security/cve/CVE-2023-24936), [CVE-2023-29331](https://access.redhat.com/security/cve/CVE-2023-29331), [CVE-2023-29337](https://access.redhat.com/security/cve/CVE-2023-29337), [CVE-2023-33128](https://access.redhat.com/security/cve/CVE-2023-33128))
aspnetcore-targeting-pack-7.0 | 7.0.7-1.el9_2 | [RHSA-2023:3592](https://access.redhat.com/errata/RHSA-2023:3592) | <div class="adv_s">Security Advisory</div> ([CVE-2023-24936](https://access.redhat.com/security/cve/CVE-2023-24936), [CVE-2023-29331](https://access.redhat.com/security/cve/CVE-2023-29331), [CVE-2023-29337](https://access.redhat.com/security/cve/CVE-2023-29337), [CVE-2023-32032](https://access.redhat.com/security/cve/CVE-2023-32032), [CVE-2023-33128](https://access.redhat.com/security/cve/CVE-2023-33128))
dotnet-apphost-pack-6.0 | 6.0.18-1.el9_2 | [RHSA-2023:3581](https://access.redhat.com/errata/RHSA-2023:3581) | <div class="adv_s">Security Advisory</div> ([CVE-2023-24936](https://access.redhat.com/security/cve/CVE-2023-24936), [CVE-2023-29331](https://access.redhat.com/security/cve/CVE-2023-29331), [CVE-2023-29337](https://access.redhat.com/security/cve/CVE-2023-29337), [CVE-2023-33128](https://access.redhat.com/security/cve/CVE-2023-33128))
dotnet-apphost-pack-6.0-debuginfo | 6.0.18-1.el9_2 | |
dotnet-apphost-pack-7.0 | 7.0.7-1.el9_2 | [RHSA-2023:3592](https://access.redhat.com/errata/RHSA-2023:3592) | <div class="adv_s">Security Advisory</div> ([CVE-2023-24936](https://access.redhat.com/security/cve/CVE-2023-24936), [CVE-2023-29331](https://access.redhat.com/security/cve/CVE-2023-29331), [CVE-2023-29337](https://access.redhat.com/security/cve/CVE-2023-29337), [CVE-2023-32032](https://access.redhat.com/security/cve/CVE-2023-32032), [CVE-2023-33128](https://access.redhat.com/security/cve/CVE-2023-33128))
dotnet-apphost-pack-7.0-debuginfo | 7.0.7-1.el9_2 | |
dotnet-host | 7.0.7-1.el9_2 | [RHSA-2023:3592](https://access.redhat.com/errata/RHSA-2023:3592) | <div class="adv_s">Security Advisory</div> ([CVE-2023-24936](https://access.redhat.com/security/cve/CVE-2023-24936), [CVE-2023-29331](https://access.redhat.com/security/cve/CVE-2023-29331), [CVE-2023-29337](https://access.redhat.com/security/cve/CVE-2023-29337), [CVE-2023-32032](https://access.redhat.com/security/cve/CVE-2023-32032), [CVE-2023-33128](https://access.redhat.com/security/cve/CVE-2023-33128))
dotnet-host-debuginfo | 7.0.7-1.el9_2 | |
dotnet-hostfxr-6.0 | 6.0.18-1.el9_2 | [RHSA-2023:3581](https://access.redhat.com/errata/RHSA-2023:3581) | <div class="adv_s">Security Advisory</div> ([CVE-2023-24936](https://access.redhat.com/security/cve/CVE-2023-24936), [CVE-2023-29331](https://access.redhat.com/security/cve/CVE-2023-29331), [CVE-2023-29337](https://access.redhat.com/security/cve/CVE-2023-29337), [CVE-2023-33128](https://access.redhat.com/security/cve/CVE-2023-33128))
dotnet-hostfxr-6.0-debuginfo | 6.0.18-1.el9_2 | |
dotnet-hostfxr-7.0 | 7.0.7-1.el9_2 | [RHSA-2023:3592](https://access.redhat.com/errata/RHSA-2023:3592) | <div class="adv_s">Security Advisory</div> ([CVE-2023-24936](https://access.redhat.com/security/cve/CVE-2023-24936), [CVE-2023-29331](https://access.redhat.com/security/cve/CVE-2023-29331), [CVE-2023-29337](https://access.redhat.com/security/cve/CVE-2023-29337), [CVE-2023-32032](https://access.redhat.com/security/cve/CVE-2023-32032), [CVE-2023-33128](https://access.redhat.com/security/cve/CVE-2023-33128))
dotnet-hostfxr-7.0-debuginfo | 7.0.7-1.el9_2 | |
dotnet-runtime-6.0 | 6.0.18-1.el9_2 | [RHSA-2023:3581](https://access.redhat.com/errata/RHSA-2023:3581) | <div class="adv_s">Security Advisory</div> ([CVE-2023-24936](https://access.redhat.com/security/cve/CVE-2023-24936), [CVE-2023-29331](https://access.redhat.com/security/cve/CVE-2023-29331), [CVE-2023-29337](https://access.redhat.com/security/cve/CVE-2023-29337), [CVE-2023-33128](https://access.redhat.com/security/cve/CVE-2023-33128))
dotnet-runtime-6.0-debuginfo | 6.0.18-1.el9_2 | |
dotnet-runtime-7.0 | 7.0.7-1.el9_2 | [RHSA-2023:3592](https://access.redhat.com/errata/RHSA-2023:3592) | <div class="adv_s">Security Advisory</div> ([CVE-2023-24936](https://access.redhat.com/security/cve/CVE-2023-24936), [CVE-2023-29331](https://access.redhat.com/security/cve/CVE-2023-29331), [CVE-2023-29337](https://access.redhat.com/security/cve/CVE-2023-29337), [CVE-2023-32032](https://access.redhat.com/security/cve/CVE-2023-32032), [CVE-2023-33128](https://access.redhat.com/security/cve/CVE-2023-33128))
dotnet-runtime-7.0-debuginfo | 7.0.7-1.el9_2 | |
dotnet-sdk-6.0 | 6.0.118-1.el9_2 | [RHSA-2023:3581](https://access.redhat.com/errata/RHSA-2023:3581) | <div class="adv_s">Security Advisory</div> ([CVE-2023-24936](https://access.redhat.com/security/cve/CVE-2023-24936), [CVE-2023-29331](https://access.redhat.com/security/cve/CVE-2023-29331), [CVE-2023-29337](https://access.redhat.com/security/cve/CVE-2023-29337), [CVE-2023-33128](https://access.redhat.com/security/cve/CVE-2023-33128))
dotnet-sdk-6.0-debuginfo | 6.0.118-1.el9_2 | |
dotnet-sdk-7.0 | 7.0.107-1.el9_2 | [RHSA-2023:3592](https://access.redhat.com/errata/RHSA-2023:3592) | <div class="adv_s">Security Advisory</div> ([CVE-2023-24936](https://access.redhat.com/security/cve/CVE-2023-24936), [CVE-2023-29331](https://access.redhat.com/security/cve/CVE-2023-29331), [CVE-2023-29337](https://access.redhat.com/security/cve/CVE-2023-29337), [CVE-2023-32032](https://access.redhat.com/security/cve/CVE-2023-32032), [CVE-2023-33128](https://access.redhat.com/security/cve/CVE-2023-33128))
dotnet-sdk-7.0-debuginfo | 7.0.107-1.el9_2 | |
dotnet-targeting-pack-6.0 | 6.0.18-1.el9_2 | [RHSA-2023:3581](https://access.redhat.com/errata/RHSA-2023:3581) | <div class="adv_s">Security Advisory</div> ([CVE-2023-24936](https://access.redhat.com/security/cve/CVE-2023-24936), [CVE-2023-29331](https://access.redhat.com/security/cve/CVE-2023-29331), [CVE-2023-29337](https://access.redhat.com/security/cve/CVE-2023-29337), [CVE-2023-33128](https://access.redhat.com/security/cve/CVE-2023-33128))
dotnet-targeting-pack-7.0 | 7.0.7-1.el9_2 | [RHSA-2023:3592](https://access.redhat.com/errata/RHSA-2023:3592) | <div class="adv_s">Security Advisory</div> ([CVE-2023-24936](https://access.redhat.com/security/cve/CVE-2023-24936), [CVE-2023-29331](https://access.redhat.com/security/cve/CVE-2023-29331), [CVE-2023-29337](https://access.redhat.com/security/cve/CVE-2023-29337), [CVE-2023-32032](https://access.redhat.com/security/cve/CVE-2023-32032), [CVE-2023-33128](https://access.redhat.com/security/cve/CVE-2023-33128))
dotnet-templates-6.0 | 6.0.118-1.el9_2 | [RHSA-2023:3581](https://access.redhat.com/errata/RHSA-2023:3581) | <div class="adv_s">Security Advisory</div> ([CVE-2023-24936](https://access.redhat.com/security/cve/CVE-2023-24936), [CVE-2023-29331](https://access.redhat.com/security/cve/CVE-2023-29331), [CVE-2023-29337](https://access.redhat.com/security/cve/CVE-2023-29337), [CVE-2023-33128](https://access.redhat.com/security/cve/CVE-2023-33128))
dotnet-templates-7.0 | 7.0.107-1.el9_2 | [RHSA-2023:3592](https://access.redhat.com/errata/RHSA-2023:3592) | <div class="adv_s">Security Advisory</div> ([CVE-2023-24936](https://access.redhat.com/security/cve/CVE-2023-24936), [CVE-2023-29331](https://access.redhat.com/security/cve/CVE-2023-29331), [CVE-2023-29337](https://access.redhat.com/security/cve/CVE-2023-29337), [CVE-2023-32032](https://access.redhat.com/security/cve/CVE-2023-32032), [CVE-2023-33128](https://access.redhat.com/security/cve/CVE-2023-33128))
dotnet6.0-debuginfo | 6.0.118-1.el9_2 | |
dotnet6.0-debugsource | 6.0.118-1.el9_2 | |
dotnet7.0-debuginfo | 7.0.107-1.el9_2 | |
dotnet7.0-debugsource | 7.0.107-1.el9_2 | |
firefox | 102.12.0-1.el9_2 | [RHSA-2023:3589](https://access.redhat.com/errata/RHSA-2023:3589) | <div class="adv_s">Security Advisory</div> ([CVE-2023-34414](https://access.redhat.com/security/cve/CVE-2023-34414), [CVE-2023-34416](https://access.redhat.com/security/cve/CVE-2023-34416))
firefox-debuginfo | 102.12.0-1.el9_2 | |
firefox-debugsource | 102.12.0-1.el9_2 | |
firefox-x11 | 102.12.0-1.el9_2 | [RHSA-2023:3589](https://access.redhat.com/errata/RHSA-2023:3589) | <div class="adv_s">Security Advisory</div> ([CVE-2023-34414](https://access.redhat.com/security/cve/CVE-2023-34414), [CVE-2023-34416](https://access.redhat.com/security/cve/CVE-2023-34416))
netstandard-targeting-pack-2.1 | 7.0.107-1.el9_2 | [RHSA-2023:3592](https://access.redhat.com/errata/RHSA-2023:3592) | <div class="adv_s">Security Advisory</div> ([CVE-2023-24936](https://access.redhat.com/security/cve/CVE-2023-24936), [CVE-2023-29331](https://access.redhat.com/security/cve/CVE-2023-29331), [CVE-2023-29337](https://access.redhat.com/security/cve/CVE-2023-29337), [CVE-2023-32032](https://access.redhat.com/security/cve/CVE-2023-32032), [CVE-2023-33128](https://access.redhat.com/security/cve/CVE-2023-33128))
nodejs | 16.19.1-2.el9_2 | [RHSA-2023:3586](https://access.redhat.com/errata/RHSA-2023:3586) | <div class="adv_s">Security Advisory</div> ([CVE-2023-31124](https://access.redhat.com/security/cve/CVE-2023-31124), [CVE-2023-31130](https://access.redhat.com/security/cve/CVE-2023-31130), [CVE-2023-31147](https://access.redhat.com/security/cve/CVE-2023-31147), [CVE-2023-32067](https://access.redhat.com/security/cve/CVE-2023-32067))
nodejs | 18.14.2-3.module+el9.2.0.z+18964+42696395 | [RHSA-2023:3577](https://access.redhat.com/errata/RHSA-2023:3577) | <div class="adv_s">Security Advisory</div> ([CVE-2023-31124](https://access.redhat.com/security/cve/CVE-2023-31124), [CVE-2023-31130](https://access.redhat.com/security/cve/CVE-2023-31130), [CVE-2023-31147](https://access.redhat.com/security/cve/CVE-2023-31147), [CVE-2023-32067](https://access.redhat.com/security/cve/CVE-2023-32067))
nodejs-debuginfo | 16.19.1-2.el9_2 | |
nodejs-debuginfo | 18.14.2-3.module+el9.2.0.z+18964+42696395 | |
nodejs-debugsource | 16.19.1-2.el9_2 | |
nodejs-debugsource | 18.14.2-3.module+el9.2.0.z+18964+42696395 | |
nodejs-devel | 18.14.2-3.module+el9.2.0.z+18964+42696395 | [RHSA-2023:3577](https://access.redhat.com/errata/RHSA-2023:3577) | <div class="adv_s">Security Advisory</div> ([CVE-2023-31124](https://access.redhat.com/security/cve/CVE-2023-31124), [CVE-2023-31130](https://access.redhat.com/security/cve/CVE-2023-31130), [CVE-2023-31147](https://access.redhat.com/security/cve/CVE-2023-31147), [CVE-2023-32067](https://access.redhat.com/security/cve/CVE-2023-32067))
nodejs-docs | 16.19.1-2.el9_2 | [RHSA-2023:3586](https://access.redhat.com/errata/RHSA-2023:3586) | <div class="adv_s">Security Advisory</div> ([CVE-2023-31124](https://access.redhat.com/security/cve/CVE-2023-31124), [CVE-2023-31130](https://access.redhat.com/security/cve/CVE-2023-31130), [CVE-2023-31147](https://access.redhat.com/security/cve/CVE-2023-31147), [CVE-2023-32067](https://access.redhat.com/security/cve/CVE-2023-32067))
nodejs-docs | 18.14.2-3.module+el9.2.0.z+18964+42696395 | [RHSA-2023:3577](https://access.redhat.com/errata/RHSA-2023:3577) | <div class="adv_s">Security Advisory</div> ([CVE-2023-31124](https://access.redhat.com/security/cve/CVE-2023-31124), [CVE-2023-31130](https://access.redhat.com/security/cve/CVE-2023-31130), [CVE-2023-31147](https://access.redhat.com/security/cve/CVE-2023-31147), [CVE-2023-32067](https://access.redhat.com/security/cve/CVE-2023-32067))
nodejs-full-i18n | 16.19.1-2.el9_2 | [RHSA-2023:3586](https://access.redhat.com/errata/RHSA-2023:3586) | <div class="adv_s">Security Advisory</div> ([CVE-2023-31124](https://access.redhat.com/security/cve/CVE-2023-31124), [CVE-2023-31130](https://access.redhat.com/security/cve/CVE-2023-31130), [CVE-2023-31147](https://access.redhat.com/security/cve/CVE-2023-31147), [CVE-2023-32067](https://access.redhat.com/security/cve/CVE-2023-32067))
nodejs-full-i18n | 18.14.2-3.module+el9.2.0.z+18964+42696395 | [RHSA-2023:3577](https://access.redhat.com/errata/RHSA-2023:3577) | <div class="adv_s">Security Advisory</div> ([CVE-2023-31124](https://access.redhat.com/security/cve/CVE-2023-31124), [CVE-2023-31130](https://access.redhat.com/security/cve/CVE-2023-31130), [CVE-2023-31147](https://access.redhat.com/security/cve/CVE-2023-31147), [CVE-2023-32067](https://access.redhat.com/security/cve/CVE-2023-32067))
nodejs-libs | 16.19.1-2.el9_2 | [RHSA-2023:3586](https://access.redhat.com/errata/RHSA-2023:3586) | <div class="adv_s">Security Advisory</div> ([CVE-2023-31124](https://access.redhat.com/security/cve/CVE-2023-31124), [CVE-2023-31130](https://access.redhat.com/security/cve/CVE-2023-31130), [CVE-2023-31147](https://access.redhat.com/security/cve/CVE-2023-31147), [CVE-2023-32067](https://access.redhat.com/security/cve/CVE-2023-32067))
nodejs-libs-debuginfo | 16.19.1-2.el9_2 | |
npm | 8.19.3-1.16.19.1.2.el9_2 | [RHSA-2023:3586](https://access.redhat.com/errata/RHSA-2023:3586) | <div class="adv_s">Security Advisory</div> ([CVE-2023-31124](https://access.redhat.com/security/cve/CVE-2023-31124), [CVE-2023-31130](https://access.redhat.com/security/cve/CVE-2023-31130), [CVE-2023-31147](https://access.redhat.com/security/cve/CVE-2023-31147), [CVE-2023-32067](https://access.redhat.com/security/cve/CVE-2023-32067))
npm | 9.5.0-1.18.14.2.3.module+el9.2.0.z+18964+42696395 | [RHSA-2023:3577](https://access.redhat.com/errata/RHSA-2023:3577) | <div class="adv_s">Security Advisory</div> ([CVE-2023-31124](https://access.redhat.com/security/cve/CVE-2023-31124), [CVE-2023-31130](https://access.redhat.com/security/cve/CVE-2023-31130), [CVE-2023-31147](https://access.redhat.com/security/cve/CVE-2023-31147), [CVE-2023-32067](https://access.redhat.com/security/cve/CVE-2023-32067))
python-unversioned-command | 3.9.16-1.el9_2.1 | |
python3-devel | 3.9.16-1.el9_2.1 | |
python3-tkinter | 3.9.16-1.el9_2.1 | |
python3.11 | 3.11.2-2.el9_2.1 | |
python3.11-debuginfo | 3.11.2-2.el9_2.1 | |
python3.11-debugsource | 3.11.2-2.el9_2.1 | |
python3.11-devel | 3.11.2-2.el9_2.1 | |
python3.11-libs | 3.11.2-2.el9_2.1 | |
python3.11-tkinter | 3.11.2-2.el9_2.1 | |
python3.9-debuginfo | 3.9.16-1.el9_2.1 | |
python3.9-debugsource | 3.9.16-1.el9_2.1 | |
thunderbird | 102.12.0-1.el9_2 | [RHSA-2023:3587](https://access.redhat.com/errata/RHSA-2023:3587) | <div class="adv_s">Security Advisory</div> ([CVE-2023-34414](https://access.redhat.com/security/cve/CVE-2023-34414), [CVE-2023-34416](https://access.redhat.com/security/cve/CVE-2023-34416))
thunderbird-debuginfo | 102.12.0-1.el9_2 | |
thunderbird-debugsource | 102.12.0-1.el9_2 | |

### codeready-builder x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
dotnet-apphost-pack-6.0-debuginfo | 6.0.18-1.el9_2 | |
dotnet-apphost-pack-7.0-debuginfo | 7.0.7-1.el9_2 | |
dotnet-host-debuginfo | 7.0.7-1.el9_2 | |
dotnet-hostfxr-6.0-debuginfo | 6.0.18-1.el9_2 | |
dotnet-hostfxr-7.0-debuginfo | 7.0.7-1.el9_2 | |
dotnet-runtime-6.0-debuginfo | 6.0.18-1.el9_2 | |
dotnet-runtime-7.0-debuginfo | 7.0.7-1.el9_2 | |
dotnet-sdk-6.0-debuginfo | 6.0.118-1.el9_2 | |
dotnet-sdk-6.0-source-built-artifacts | 6.0.118-1.el9_2 | [RHSA-2023:3581](https://access.redhat.com/errata/RHSA-2023:3581) | <div class="adv_s">Security Advisory</div> ([CVE-2023-24936](https://access.redhat.com/security/cve/CVE-2023-24936), [CVE-2023-29331](https://access.redhat.com/security/cve/CVE-2023-29331), [CVE-2023-29337](https://access.redhat.com/security/cve/CVE-2023-29337), [CVE-2023-33128](https://access.redhat.com/security/cve/CVE-2023-33128))
dotnet-sdk-7.0-debuginfo | 7.0.107-1.el9_2 | |
dotnet-sdk-7.0-source-built-artifacts | 7.0.107-1.el9_2 | [RHSA-2023:3592](https://access.redhat.com/errata/RHSA-2023:3592) | <div class="adv_s">Security Advisory</div> ([CVE-2023-24936](https://access.redhat.com/security/cve/CVE-2023-24936), [CVE-2023-29331](https://access.redhat.com/security/cve/CVE-2023-29331), [CVE-2023-29337](https://access.redhat.com/security/cve/CVE-2023-29337), [CVE-2023-32032](https://access.redhat.com/security/cve/CVE-2023-32032), [CVE-2023-33128](https://access.redhat.com/security/cve/CVE-2023-33128))
dotnet6.0-debuginfo | 6.0.118-1.el9_2 | |
dotnet6.0-debugsource | 6.0.118-1.el9_2 | |
dotnet7.0-debuginfo | 7.0.107-1.el9_2 | |
dotnet7.0-debugsource | 7.0.107-1.el9_2 | |
python3-debug | 3.9.16-1.el9_2.1 | |
python3-idle | 3.9.16-1.el9_2.1 | |
python3-test | 3.9.16-1.el9_2.1 | |
python3.11-debug | 3.11.2-2.el9_2.1 | |
python3.11-debuginfo | 3.11.2-2.el9_2.1 | |
python3.11-debugsource | 3.11.2-2.el9_2.1 | |
python3.11-idle | 3.11.2-2.el9_2.1 | |
python3.11-test | 3.11.2-2.el9_2.1 | |
python3.9-debuginfo | 3.9.16-1.el9_2.1 | |
python3.9-debugsource | 3.9.16-1.el9_2.1 | |

### baseos aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
python3 | 3.9.16-1.el9_2.1 | |
python3-libs | 3.9.16-1.el9_2.1 | |
python3.9-debuginfo | 3.9.16-1.el9_2.1 | |
python3.9-debugsource | 3.9.16-1.el9_2.1 | |

### appstream aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
aspnetcore-runtime-6.0 | 6.0.18-1.el9_2 | [RHSA-2023:3581](https://access.redhat.com/errata/RHSA-2023:3581) | <div class="adv_s">Security Advisory</div> ([CVE-2023-24936](https://access.redhat.com/security/cve/CVE-2023-24936), [CVE-2023-29331](https://access.redhat.com/security/cve/CVE-2023-29331), [CVE-2023-29337](https://access.redhat.com/security/cve/CVE-2023-29337), [CVE-2023-33128](https://access.redhat.com/security/cve/CVE-2023-33128))
aspnetcore-runtime-7.0 | 7.0.7-1.el9_2 | [RHSA-2023:3592](https://access.redhat.com/errata/RHSA-2023:3592) | <div class="adv_s">Security Advisory</div> ([CVE-2023-24936](https://access.redhat.com/security/cve/CVE-2023-24936), [CVE-2023-29331](https://access.redhat.com/security/cve/CVE-2023-29331), [CVE-2023-29337](https://access.redhat.com/security/cve/CVE-2023-29337), [CVE-2023-32032](https://access.redhat.com/security/cve/CVE-2023-32032), [CVE-2023-33128](https://access.redhat.com/security/cve/CVE-2023-33128))
aspnetcore-targeting-pack-6.0 | 6.0.18-1.el9_2 | [RHSA-2023:3581](https://access.redhat.com/errata/RHSA-2023:3581) | <div class="adv_s">Security Advisory</div> ([CVE-2023-24936](https://access.redhat.com/security/cve/CVE-2023-24936), [CVE-2023-29331](https://access.redhat.com/security/cve/CVE-2023-29331), [CVE-2023-29337](https://access.redhat.com/security/cve/CVE-2023-29337), [CVE-2023-33128](https://access.redhat.com/security/cve/CVE-2023-33128))
aspnetcore-targeting-pack-7.0 | 7.0.7-1.el9_2 | [RHSA-2023:3592](https://access.redhat.com/errata/RHSA-2023:3592) | <div class="adv_s">Security Advisory</div> ([CVE-2023-24936](https://access.redhat.com/security/cve/CVE-2023-24936), [CVE-2023-29331](https://access.redhat.com/security/cve/CVE-2023-29331), [CVE-2023-29337](https://access.redhat.com/security/cve/CVE-2023-29337), [CVE-2023-32032](https://access.redhat.com/security/cve/CVE-2023-32032), [CVE-2023-33128](https://access.redhat.com/security/cve/CVE-2023-33128))
dotnet-apphost-pack-6.0 | 6.0.18-1.el9_2 | [RHSA-2023:3581](https://access.redhat.com/errata/RHSA-2023:3581) | <div class="adv_s">Security Advisory</div> ([CVE-2023-24936](https://access.redhat.com/security/cve/CVE-2023-24936), [CVE-2023-29331](https://access.redhat.com/security/cve/CVE-2023-29331), [CVE-2023-29337](https://access.redhat.com/security/cve/CVE-2023-29337), [CVE-2023-33128](https://access.redhat.com/security/cve/CVE-2023-33128))
dotnet-apphost-pack-6.0-debuginfo | 6.0.18-1.el9_2 | |
dotnet-apphost-pack-7.0 | 7.0.7-1.el9_2 | [RHSA-2023:3592](https://access.redhat.com/errata/RHSA-2023:3592) | <div class="adv_s">Security Advisory</div> ([CVE-2023-24936](https://access.redhat.com/security/cve/CVE-2023-24936), [CVE-2023-29331](https://access.redhat.com/security/cve/CVE-2023-29331), [CVE-2023-29337](https://access.redhat.com/security/cve/CVE-2023-29337), [CVE-2023-32032](https://access.redhat.com/security/cve/CVE-2023-32032), [CVE-2023-33128](https://access.redhat.com/security/cve/CVE-2023-33128))
dotnet-apphost-pack-7.0-debuginfo | 7.0.7-1.el9_2 | |
dotnet-host | 7.0.7-1.el9_2 | [RHSA-2023:3592](https://access.redhat.com/errata/RHSA-2023:3592) | <div class="adv_s">Security Advisory</div> ([CVE-2023-24936](https://access.redhat.com/security/cve/CVE-2023-24936), [CVE-2023-29331](https://access.redhat.com/security/cve/CVE-2023-29331), [CVE-2023-29337](https://access.redhat.com/security/cve/CVE-2023-29337), [CVE-2023-32032](https://access.redhat.com/security/cve/CVE-2023-32032), [CVE-2023-33128](https://access.redhat.com/security/cve/CVE-2023-33128))
dotnet-host-debuginfo | 7.0.7-1.el9_2 | |
dotnet-hostfxr-6.0 | 6.0.18-1.el9_2 | [RHSA-2023:3581](https://access.redhat.com/errata/RHSA-2023:3581) | <div class="adv_s">Security Advisory</div> ([CVE-2023-24936](https://access.redhat.com/security/cve/CVE-2023-24936), [CVE-2023-29331](https://access.redhat.com/security/cve/CVE-2023-29331), [CVE-2023-29337](https://access.redhat.com/security/cve/CVE-2023-29337), [CVE-2023-33128](https://access.redhat.com/security/cve/CVE-2023-33128))
dotnet-hostfxr-6.0-debuginfo | 6.0.18-1.el9_2 | |
dotnet-hostfxr-7.0 | 7.0.7-1.el9_2 | [RHSA-2023:3592](https://access.redhat.com/errata/RHSA-2023:3592) | <div class="adv_s">Security Advisory</div> ([CVE-2023-24936](https://access.redhat.com/security/cve/CVE-2023-24936), [CVE-2023-29331](https://access.redhat.com/security/cve/CVE-2023-29331), [CVE-2023-29337](https://access.redhat.com/security/cve/CVE-2023-29337), [CVE-2023-32032](https://access.redhat.com/security/cve/CVE-2023-32032), [CVE-2023-33128](https://access.redhat.com/security/cve/CVE-2023-33128))
dotnet-hostfxr-7.0-debuginfo | 7.0.7-1.el9_2 | |
dotnet-runtime-6.0 | 6.0.18-1.el9_2 | [RHSA-2023:3581](https://access.redhat.com/errata/RHSA-2023:3581) | <div class="adv_s">Security Advisory</div> ([CVE-2023-24936](https://access.redhat.com/security/cve/CVE-2023-24936), [CVE-2023-29331](https://access.redhat.com/security/cve/CVE-2023-29331), [CVE-2023-29337](https://access.redhat.com/security/cve/CVE-2023-29337), [CVE-2023-33128](https://access.redhat.com/security/cve/CVE-2023-33128))
dotnet-runtime-6.0-debuginfo | 6.0.18-1.el9_2 | |
dotnet-runtime-7.0 | 7.0.7-1.el9_2 | [RHSA-2023:3592](https://access.redhat.com/errata/RHSA-2023:3592) | <div class="adv_s">Security Advisory</div> ([CVE-2023-24936](https://access.redhat.com/security/cve/CVE-2023-24936), [CVE-2023-29331](https://access.redhat.com/security/cve/CVE-2023-29331), [CVE-2023-29337](https://access.redhat.com/security/cve/CVE-2023-29337), [CVE-2023-32032](https://access.redhat.com/security/cve/CVE-2023-32032), [CVE-2023-33128](https://access.redhat.com/security/cve/CVE-2023-33128))
dotnet-runtime-7.0-debuginfo | 7.0.7-1.el9_2 | |
dotnet-sdk-6.0 | 6.0.118-1.el9_2 | [RHSA-2023:3581](https://access.redhat.com/errata/RHSA-2023:3581) | <div class="adv_s">Security Advisory</div> ([CVE-2023-24936](https://access.redhat.com/security/cve/CVE-2023-24936), [CVE-2023-29331](https://access.redhat.com/security/cve/CVE-2023-29331), [CVE-2023-29337](https://access.redhat.com/security/cve/CVE-2023-29337), [CVE-2023-33128](https://access.redhat.com/security/cve/CVE-2023-33128))
dotnet-sdk-6.0-debuginfo | 6.0.118-1.el9_2 | |
dotnet-sdk-7.0 | 7.0.107-1.el9_2 | [RHSA-2023:3592](https://access.redhat.com/errata/RHSA-2023:3592) | <div class="adv_s">Security Advisory</div> ([CVE-2023-24936](https://access.redhat.com/security/cve/CVE-2023-24936), [CVE-2023-29331](https://access.redhat.com/security/cve/CVE-2023-29331), [CVE-2023-29337](https://access.redhat.com/security/cve/CVE-2023-29337), [CVE-2023-32032](https://access.redhat.com/security/cve/CVE-2023-32032), [CVE-2023-33128](https://access.redhat.com/security/cve/CVE-2023-33128))
dotnet-sdk-7.0-debuginfo | 7.0.107-1.el9_2 | |
dotnet-targeting-pack-6.0 | 6.0.18-1.el9_2 | [RHSA-2023:3581](https://access.redhat.com/errata/RHSA-2023:3581) | <div class="adv_s">Security Advisory</div> ([CVE-2023-24936](https://access.redhat.com/security/cve/CVE-2023-24936), [CVE-2023-29331](https://access.redhat.com/security/cve/CVE-2023-29331), [CVE-2023-29337](https://access.redhat.com/security/cve/CVE-2023-29337), [CVE-2023-33128](https://access.redhat.com/security/cve/CVE-2023-33128))
dotnet-targeting-pack-7.0 | 7.0.7-1.el9_2 | [RHSA-2023:3592](https://access.redhat.com/errata/RHSA-2023:3592) | <div class="adv_s">Security Advisory</div> ([CVE-2023-24936](https://access.redhat.com/security/cve/CVE-2023-24936), [CVE-2023-29331](https://access.redhat.com/security/cve/CVE-2023-29331), [CVE-2023-29337](https://access.redhat.com/security/cve/CVE-2023-29337), [CVE-2023-32032](https://access.redhat.com/security/cve/CVE-2023-32032), [CVE-2023-33128](https://access.redhat.com/security/cve/CVE-2023-33128))
dotnet-templates-6.0 | 6.0.118-1.el9_2 | [RHSA-2023:3581](https://access.redhat.com/errata/RHSA-2023:3581) | <div class="adv_s">Security Advisory</div> ([CVE-2023-24936](https://access.redhat.com/security/cve/CVE-2023-24936), [CVE-2023-29331](https://access.redhat.com/security/cve/CVE-2023-29331), [CVE-2023-29337](https://access.redhat.com/security/cve/CVE-2023-29337), [CVE-2023-33128](https://access.redhat.com/security/cve/CVE-2023-33128))
dotnet-templates-7.0 | 7.0.107-1.el9_2 | [RHSA-2023:3592](https://access.redhat.com/errata/RHSA-2023:3592) | <div class="adv_s">Security Advisory</div> ([CVE-2023-24936](https://access.redhat.com/security/cve/CVE-2023-24936), [CVE-2023-29331](https://access.redhat.com/security/cve/CVE-2023-29331), [CVE-2023-29337](https://access.redhat.com/security/cve/CVE-2023-29337), [CVE-2023-32032](https://access.redhat.com/security/cve/CVE-2023-32032), [CVE-2023-33128](https://access.redhat.com/security/cve/CVE-2023-33128))
dotnet6.0-debuginfo | 6.0.118-1.el9_2 | |
dotnet6.0-debugsource | 6.0.118-1.el9_2 | |
dotnet7.0-debuginfo | 7.0.107-1.el9_2 | |
dotnet7.0-debugsource | 7.0.107-1.el9_2 | |
firefox | 102.12.0-1.el9_2 | [RHSA-2023:3589](https://access.redhat.com/errata/RHSA-2023:3589) | <div class="adv_s">Security Advisory</div> ([CVE-2023-34414](https://access.redhat.com/security/cve/CVE-2023-34414), [CVE-2023-34416](https://access.redhat.com/security/cve/CVE-2023-34416))
firefox-debuginfo | 102.12.0-1.el9_2 | |
firefox-debugsource | 102.12.0-1.el9_2 | |
firefox-x11 | 102.12.0-1.el9_2 | [RHSA-2023:3589](https://access.redhat.com/errata/RHSA-2023:3589) | <div class="adv_s">Security Advisory</div> ([CVE-2023-34414](https://access.redhat.com/security/cve/CVE-2023-34414), [CVE-2023-34416](https://access.redhat.com/security/cve/CVE-2023-34416))
netstandard-targeting-pack-2.1 | 7.0.107-1.el9_2 | [RHSA-2023:3592](https://access.redhat.com/errata/RHSA-2023:3592) | <div class="adv_s">Security Advisory</div> ([CVE-2023-24936](https://access.redhat.com/security/cve/CVE-2023-24936), [CVE-2023-29331](https://access.redhat.com/security/cve/CVE-2023-29331), [CVE-2023-29337](https://access.redhat.com/security/cve/CVE-2023-29337), [CVE-2023-32032](https://access.redhat.com/security/cve/CVE-2023-32032), [CVE-2023-33128](https://access.redhat.com/security/cve/CVE-2023-33128))
nodejs | 16.19.1-2.el9_2 | [RHSA-2023:3586](https://access.redhat.com/errata/RHSA-2023:3586) | <div class="adv_s">Security Advisory</div> ([CVE-2023-31124](https://access.redhat.com/security/cve/CVE-2023-31124), [CVE-2023-31130](https://access.redhat.com/security/cve/CVE-2023-31130), [CVE-2023-31147](https://access.redhat.com/security/cve/CVE-2023-31147), [CVE-2023-32067](https://access.redhat.com/security/cve/CVE-2023-32067))
nodejs | 18.14.2-3.module+el9.2.0.z+18964+42696395 | [RHSA-2023:3577](https://access.redhat.com/errata/RHSA-2023:3577) | <div class="adv_s">Security Advisory</div> ([CVE-2023-31124](https://access.redhat.com/security/cve/CVE-2023-31124), [CVE-2023-31130](https://access.redhat.com/security/cve/CVE-2023-31130), [CVE-2023-31147](https://access.redhat.com/security/cve/CVE-2023-31147), [CVE-2023-32067](https://access.redhat.com/security/cve/CVE-2023-32067))
nodejs-debuginfo | 16.19.1-2.el9_2 | |
nodejs-debuginfo | 18.14.2-3.module+el9.2.0.z+18964+42696395 | |
nodejs-debugsource | 16.19.1-2.el9_2 | |
nodejs-debugsource | 18.14.2-3.module+el9.2.0.z+18964+42696395 | |
nodejs-devel | 18.14.2-3.module+el9.2.0.z+18964+42696395 | [RHSA-2023:3577](https://access.redhat.com/errata/RHSA-2023:3577) | <div class="adv_s">Security Advisory</div> ([CVE-2023-31124](https://access.redhat.com/security/cve/CVE-2023-31124), [CVE-2023-31130](https://access.redhat.com/security/cve/CVE-2023-31130), [CVE-2023-31147](https://access.redhat.com/security/cve/CVE-2023-31147), [CVE-2023-32067](https://access.redhat.com/security/cve/CVE-2023-32067))
nodejs-docs | 16.19.1-2.el9_2 | [RHSA-2023:3586](https://access.redhat.com/errata/RHSA-2023:3586) | <div class="adv_s">Security Advisory</div> ([CVE-2023-31124](https://access.redhat.com/security/cve/CVE-2023-31124), [CVE-2023-31130](https://access.redhat.com/security/cve/CVE-2023-31130), [CVE-2023-31147](https://access.redhat.com/security/cve/CVE-2023-31147), [CVE-2023-32067](https://access.redhat.com/security/cve/CVE-2023-32067))
nodejs-docs | 18.14.2-3.module+el9.2.0.z+18964+42696395 | [RHSA-2023:3577](https://access.redhat.com/errata/RHSA-2023:3577) | <div class="adv_s">Security Advisory</div> ([CVE-2023-31124](https://access.redhat.com/security/cve/CVE-2023-31124), [CVE-2023-31130](https://access.redhat.com/security/cve/CVE-2023-31130), [CVE-2023-31147](https://access.redhat.com/security/cve/CVE-2023-31147), [CVE-2023-32067](https://access.redhat.com/security/cve/CVE-2023-32067))
nodejs-full-i18n | 16.19.1-2.el9_2 | [RHSA-2023:3586](https://access.redhat.com/errata/RHSA-2023:3586) | <div class="adv_s">Security Advisory</div> ([CVE-2023-31124](https://access.redhat.com/security/cve/CVE-2023-31124), [CVE-2023-31130](https://access.redhat.com/security/cve/CVE-2023-31130), [CVE-2023-31147](https://access.redhat.com/security/cve/CVE-2023-31147), [CVE-2023-32067](https://access.redhat.com/security/cve/CVE-2023-32067))
nodejs-full-i18n | 18.14.2-3.module+el9.2.0.z+18964+42696395 | [RHSA-2023:3577](https://access.redhat.com/errata/RHSA-2023:3577) | <div class="adv_s">Security Advisory</div> ([CVE-2023-31124](https://access.redhat.com/security/cve/CVE-2023-31124), [CVE-2023-31130](https://access.redhat.com/security/cve/CVE-2023-31130), [CVE-2023-31147](https://access.redhat.com/security/cve/CVE-2023-31147), [CVE-2023-32067](https://access.redhat.com/security/cve/CVE-2023-32067))
nodejs-libs | 16.19.1-2.el9_2 | [RHSA-2023:3586](https://access.redhat.com/errata/RHSA-2023:3586) | <div class="adv_s">Security Advisory</div> ([CVE-2023-31124](https://access.redhat.com/security/cve/CVE-2023-31124), [CVE-2023-31130](https://access.redhat.com/security/cve/CVE-2023-31130), [CVE-2023-31147](https://access.redhat.com/security/cve/CVE-2023-31147), [CVE-2023-32067](https://access.redhat.com/security/cve/CVE-2023-32067))
nodejs-libs-debuginfo | 16.19.1-2.el9_2 | |
npm | 8.19.3-1.16.19.1.2.el9_2 | [RHSA-2023:3586](https://access.redhat.com/errata/RHSA-2023:3586) | <div class="adv_s">Security Advisory</div> ([CVE-2023-31124](https://access.redhat.com/security/cve/CVE-2023-31124), [CVE-2023-31130](https://access.redhat.com/security/cve/CVE-2023-31130), [CVE-2023-31147](https://access.redhat.com/security/cve/CVE-2023-31147), [CVE-2023-32067](https://access.redhat.com/security/cve/CVE-2023-32067))
npm | 9.5.0-1.18.14.2.3.module+el9.2.0.z+18964+42696395 | [RHSA-2023:3577](https://access.redhat.com/errata/RHSA-2023:3577) | <div class="adv_s">Security Advisory</div> ([CVE-2023-31124](https://access.redhat.com/security/cve/CVE-2023-31124), [CVE-2023-31130](https://access.redhat.com/security/cve/CVE-2023-31130), [CVE-2023-31147](https://access.redhat.com/security/cve/CVE-2023-31147), [CVE-2023-32067](https://access.redhat.com/security/cve/CVE-2023-32067))
python-unversioned-command | 3.9.16-1.el9_2.1 | |
python3-devel | 3.9.16-1.el9_2.1 | |
python3-tkinter | 3.9.16-1.el9_2.1 | |
python3.11 | 3.11.2-2.el9_2.1 | |
python3.11-debuginfo | 3.11.2-2.el9_2.1 | |
python3.11-debugsource | 3.11.2-2.el9_2.1 | |
python3.11-devel | 3.11.2-2.el9_2.1 | |
python3.11-libs | 3.11.2-2.el9_2.1 | |
python3.11-tkinter | 3.11.2-2.el9_2.1 | |
python3.9-debuginfo | 3.9.16-1.el9_2.1 | |
python3.9-debugsource | 3.9.16-1.el9_2.1 | |
thunderbird | 102.12.0-1.el9_2 | [RHSA-2023:3587](https://access.redhat.com/errata/RHSA-2023:3587) | <div class="adv_s">Security Advisory</div> ([CVE-2023-34414](https://access.redhat.com/security/cve/CVE-2023-34414), [CVE-2023-34416](https://access.redhat.com/security/cve/CVE-2023-34416))
thunderbird-debuginfo | 102.12.0-1.el9_2 | |
thunderbird-debugsource | 102.12.0-1.el9_2 | |

### codeready-builder aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
dotnet-apphost-pack-6.0-debuginfo | 6.0.18-1.el9_2 | |
dotnet-apphost-pack-7.0-debuginfo | 7.0.7-1.el9_2 | |
dotnet-host-debuginfo | 7.0.7-1.el9_2 | |
dotnet-hostfxr-6.0-debuginfo | 6.0.18-1.el9_2 | |
dotnet-hostfxr-7.0-debuginfo | 7.0.7-1.el9_2 | |
dotnet-runtime-6.0-debuginfo | 6.0.18-1.el9_2 | |
dotnet-runtime-7.0-debuginfo | 7.0.7-1.el9_2 | |
dotnet-sdk-6.0-debuginfo | 6.0.118-1.el9_2 | |
dotnet-sdk-6.0-source-built-artifacts | 6.0.118-1.el9_2 | [RHSA-2023:3581](https://access.redhat.com/errata/RHSA-2023:3581) | <div class="adv_s">Security Advisory</div> ([CVE-2023-24936](https://access.redhat.com/security/cve/CVE-2023-24936), [CVE-2023-29331](https://access.redhat.com/security/cve/CVE-2023-29331), [CVE-2023-29337](https://access.redhat.com/security/cve/CVE-2023-29337), [CVE-2023-33128](https://access.redhat.com/security/cve/CVE-2023-33128))
dotnet-sdk-7.0-debuginfo | 7.0.107-1.el9_2 | |
dotnet-sdk-7.0-source-built-artifacts | 7.0.107-1.el9_2 | [RHSA-2023:3592](https://access.redhat.com/errata/RHSA-2023:3592) | <div class="adv_s">Security Advisory</div> ([CVE-2023-24936](https://access.redhat.com/security/cve/CVE-2023-24936), [CVE-2023-29331](https://access.redhat.com/security/cve/CVE-2023-29331), [CVE-2023-29337](https://access.redhat.com/security/cve/CVE-2023-29337), [CVE-2023-32032](https://access.redhat.com/security/cve/CVE-2023-32032), [CVE-2023-33128](https://access.redhat.com/security/cve/CVE-2023-33128))
dotnet6.0-debuginfo | 6.0.118-1.el9_2 | |
dotnet6.0-debugsource | 6.0.118-1.el9_2 | |
dotnet7.0-debuginfo | 7.0.107-1.el9_2 | |
dotnet7.0-debugsource | 7.0.107-1.el9_2 | |
python3-debug | 3.9.16-1.el9_2.1 | |
python3-idle | 3.9.16-1.el9_2.1 | |
python3-test | 3.9.16-1.el9_2.1 | |
python3.11-debug | 3.11.2-2.el9_2.1 | |
python3.11-debuginfo | 3.11.2-2.el9_2.1 | |
python3.11-debugsource | 3.11.2-2.el9_2.1 | |
python3.11-idle | 3.11.2-2.el9_2.1 | |
python3.11-test | 3.11.2-2.el9_2.1 | |
python3.9-debuginfo | 3.9.16-1.el9_2.1 | |
python3.9-debugsource | 3.9.16-1.el9_2.1 | |

