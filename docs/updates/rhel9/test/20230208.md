## 2023-02-08

### baseos x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
libksba | 1.5.1-6.el9_1 | |
libksba-debuginfo | 1.5.1-6.el9_1 | |
libksba-debugsource | 1.5.1-6.el9_1 | |

### appstream x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
tigervnc | 1.12.0-5.el9_1.1 | |
tigervnc-debuginfo | 1.12.0-5.el9_1.1 | |
tigervnc-debugsource | 1.12.0-5.el9_1.1 | |
tigervnc-icons | 1.12.0-5.el9_1.1 | |
tigervnc-license | 1.12.0-5.el9_1.1 | |
tigervnc-selinux | 1.12.0-5.el9_1.1 | |
tigervnc-server | 1.12.0-5.el9_1.1 | |
tigervnc-server-debuginfo | 1.12.0-5.el9_1.1 | |
tigervnc-server-minimal | 1.12.0-5.el9_1.1 | |
tigervnc-server-minimal-debuginfo | 1.12.0-5.el9_1.1 | |
tigervnc-server-module | 1.12.0-5.el9_1.1 | |
tigervnc-server-module-debuginfo | 1.12.0-5.el9_1.1 | |

### codeready-builder x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
libksba-debuginfo | 1.5.1-6.el9_1 | |
libksba-debugsource | 1.5.1-6.el9_1 | |
libksba-devel | 1.5.1-6.el9_1 | |

### baseos aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
libksba | 1.5.1-6.el9_1 | |
libksba-debuginfo | 1.5.1-6.el9_1 | |
libksba-debugsource | 1.5.1-6.el9_1 | |

### appstream aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
tigervnc | 1.12.0-5.el9_1.1 | |
tigervnc-debuginfo | 1.12.0-5.el9_1.1 | |
tigervnc-debugsource | 1.12.0-5.el9_1.1 | |
tigervnc-icons | 1.12.0-5.el9_1.1 | |
tigervnc-license | 1.12.0-5.el9_1.1 | |
tigervnc-selinux | 1.12.0-5.el9_1.1 | |
tigervnc-server | 1.12.0-5.el9_1.1 | |
tigervnc-server-debuginfo | 1.12.0-5.el9_1.1 | |
tigervnc-server-minimal | 1.12.0-5.el9_1.1 | |
tigervnc-server-minimal-debuginfo | 1.12.0-5.el9_1.1 | |
tigervnc-server-module | 1.12.0-5.el9_1.1 | |
tigervnc-server-module-debuginfo | 1.12.0-5.el9_1.1 | |

### codeready-builder aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
libksba-debuginfo | 1.5.1-6.el9_1 | |
libksba-debugsource | 1.5.1-6.el9_1 | |
libksba-devel | 1.5.1-6.el9_1 | |

