## 2023-09-20

### appstream x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
fdo-admin-cli | 0.4.12-4.el9_2 | |
fdo-admin-cli-debuginfo | 0.4.12-4.el9_2 | |
fdo-client | 0.4.12-4.el9_2 | |
fdo-client-debuginfo | 0.4.12-4.el9_2 | |
fdo-init | 0.4.12-4.el9_2 | |
fdo-init-debuginfo | 0.4.12-4.el9_2 | |
fdo-manufacturing-server | 0.4.12-4.el9_2 | |
fdo-manufacturing-server-debuginfo | 0.4.12-4.el9_2 | |
fdo-owner-cli | 0.4.12-4.el9_2 | |
fdo-owner-cli-debuginfo | 0.4.12-4.el9_2 | |
fdo-owner-onboarding-server | 0.4.12-4.el9_2 | |
fdo-owner-onboarding-server-debuginfo | 0.4.12-4.el9_2 | |
fdo-rendezvous-server | 0.4.12-4.el9_2 | |
fdo-rendezvous-server-debuginfo | 0.4.12-4.el9_2 | |
fido-device-onboard-debuginfo | 0.4.12-4.el9_2 | |
fido-device-onboard-debugsource | 0.4.12-4.el9_2 | |
libwebp | 1.2.0-7.el9_2 | [RHSA-2023:5214](https://access.redhat.com/errata/RHSA-2023:5214) | <div class="adv_s">Security Advisory</div> ([CVE-2023-4863](https://access.redhat.com/security/cve/CVE-2023-4863))
libwebp-debuginfo | 1.2.0-7.el9_2 | |
libwebp-debugsource | 1.2.0-7.el9_2 | |
libwebp-devel | 1.2.0-7.el9_2 | [RHSA-2023:5214](https://access.redhat.com/errata/RHSA-2023:5214) | <div class="adv_s">Security Advisory</div> ([CVE-2023-4863](https://access.redhat.com/security/cve/CVE-2023-4863))
libwebp-java-debuginfo | 1.2.0-7.el9_2 | |
libwebp-tools-debuginfo | 1.2.0-7.el9_2 | |
nmstate | 2.2.15-2.el9_2 | [RHBA-2023:5215](https://access.redhat.com/errata/RHBA-2023:5215) | <div class="adv_b">Bug Fix Advisory</div>
nmstate-debuginfo | 2.2.15-2.el9_2 | |
nmstate-debugsource | 2.2.15-2.el9_2 | |
nmstate-libs | 2.2.15-2.el9_2 | [RHBA-2023:5215](https://access.redhat.com/errata/RHBA-2023:5215) | <div class="adv_b">Bug Fix Advisory</div>
nmstate-libs-debuginfo | 2.2.15-2.el9_2 | |
python3-libnmstate | 2.2.15-2.el9_2 | [RHBA-2023:5215](https://access.redhat.com/errata/RHBA-2023:5215) | <div class="adv_b">Bug Fix Advisory</div>
thunderbird | 102.15.1-1.el9_2 | [RHSA-2023:5224](https://access.redhat.com/errata/RHSA-2023:5224) | <div class="adv_s">Security Advisory</div> ([CVE-2023-4863](https://access.redhat.com/security/cve/CVE-2023-4863))
thunderbird-debuginfo | 102.15.1-1.el9_2 | |
thunderbird-debugsource | 102.15.1-1.el9_2 | |

### codeready-builder x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
libwebp-debuginfo | 1.2.0-7.el9_2 | |
libwebp-debugsource | 1.2.0-7.el9_2 | |
libwebp-java-debuginfo | 1.2.0-7.el9_2 | |
libwebp-tools | 1.2.0-7.el9_2 | [RHSA-2023:5214](https://access.redhat.com/errata/RHSA-2023:5214) | <div class="adv_s">Security Advisory</div> ([CVE-2023-4863](https://access.redhat.com/security/cve/CVE-2023-4863))
libwebp-tools-debuginfo | 1.2.0-7.el9_2 | |
nmstate-debuginfo | 2.2.15-2.el9_2 | |
nmstate-debugsource | 2.2.15-2.el9_2 | |
nmstate-devel | 2.2.15-2.el9_2 | [RHBA-2023:5215](https://access.redhat.com/errata/RHBA-2023:5215) | <div class="adv_b">Bug Fix Advisory</div>
nmstate-libs-debuginfo | 2.2.15-2.el9_2 | |
nmstate-static | 2.2.15-2.el9_2 | [RHBA-2023:5215](https://access.redhat.com/errata/RHBA-2023:5215) | <div class="adv_b">Bug Fix Advisory</div>

### appstream aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
fdo-admin-cli | 0.4.12-4.el9_2 | |
fdo-admin-cli-debuginfo | 0.4.12-4.el9_2 | |
fdo-client | 0.4.12-4.el9_2 | |
fdo-client-debuginfo | 0.4.12-4.el9_2 | |
fdo-init | 0.4.12-4.el9_2 | |
fdo-init-debuginfo | 0.4.12-4.el9_2 | |
fdo-manufacturing-server | 0.4.12-4.el9_2 | |
fdo-manufacturing-server-debuginfo | 0.4.12-4.el9_2 | |
fdo-owner-cli | 0.4.12-4.el9_2 | |
fdo-owner-cli-debuginfo | 0.4.12-4.el9_2 | |
fdo-owner-onboarding-server | 0.4.12-4.el9_2 | |
fdo-owner-onboarding-server-debuginfo | 0.4.12-4.el9_2 | |
fdo-rendezvous-server | 0.4.12-4.el9_2 | |
fdo-rendezvous-server-debuginfo | 0.4.12-4.el9_2 | |
fido-device-onboard-debuginfo | 0.4.12-4.el9_2 | |
fido-device-onboard-debugsource | 0.4.12-4.el9_2 | |
libwebp | 1.2.0-7.el9_2 | [RHSA-2023:5214](https://access.redhat.com/errata/RHSA-2023:5214) | <div class="adv_s">Security Advisory</div> ([CVE-2023-4863](https://access.redhat.com/security/cve/CVE-2023-4863))
libwebp-debuginfo | 1.2.0-7.el9_2 | |
libwebp-debugsource | 1.2.0-7.el9_2 | |
libwebp-devel | 1.2.0-7.el9_2 | [RHSA-2023:5214](https://access.redhat.com/errata/RHSA-2023:5214) | <div class="adv_s">Security Advisory</div> ([CVE-2023-4863](https://access.redhat.com/security/cve/CVE-2023-4863))
libwebp-java-debuginfo | 1.2.0-7.el9_2 | |
libwebp-tools-debuginfo | 1.2.0-7.el9_2 | |
nmstate | 2.2.15-2.el9_2 | [RHBA-2023:5215](https://access.redhat.com/errata/RHBA-2023:5215) | <div class="adv_b">Bug Fix Advisory</div>
nmstate-debuginfo | 2.2.15-2.el9_2 | |
nmstate-debugsource | 2.2.15-2.el9_2 | |
nmstate-libs | 2.2.15-2.el9_2 | [RHBA-2023:5215](https://access.redhat.com/errata/RHBA-2023:5215) | <div class="adv_b">Bug Fix Advisory</div>
nmstate-libs-debuginfo | 2.2.15-2.el9_2 | |
python3-libnmstate | 2.2.15-2.el9_2 | [RHBA-2023:5215](https://access.redhat.com/errata/RHBA-2023:5215) | <div class="adv_b">Bug Fix Advisory</div>
thunderbird | 102.15.1-1.el9_2 | [RHSA-2023:5224](https://access.redhat.com/errata/RHSA-2023:5224) | <div class="adv_s">Security Advisory</div> ([CVE-2023-4863](https://access.redhat.com/security/cve/CVE-2023-4863))
thunderbird-debuginfo | 102.15.1-1.el9_2 | |
thunderbird-debugsource | 102.15.1-1.el9_2 | |

### codeready-builder aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
libwebp-debuginfo | 1.2.0-7.el9_2 | |
libwebp-debugsource | 1.2.0-7.el9_2 | |
libwebp-java-debuginfo | 1.2.0-7.el9_2 | |
libwebp-tools | 1.2.0-7.el9_2 | [RHSA-2023:5214](https://access.redhat.com/errata/RHSA-2023:5214) | <div class="adv_s">Security Advisory</div> ([CVE-2023-4863](https://access.redhat.com/security/cve/CVE-2023-4863))
libwebp-tools-debuginfo | 1.2.0-7.el9_2 | |
nmstate-debuginfo | 2.2.15-2.el9_2 | |
nmstate-debugsource | 2.2.15-2.el9_2 | |
nmstate-devel | 2.2.15-2.el9_2 | [RHBA-2023:5215](https://access.redhat.com/errata/RHBA-2023:5215) | <div class="adv_b">Bug Fix Advisory</div>
nmstate-libs-debuginfo | 2.2.15-2.el9_2 | |
nmstate-static | 2.2.15-2.el9_2 | [RHBA-2023:5215](https://access.redhat.com/errata/RHBA-2023:5215) | <div class="adv_b">Bug Fix Advisory</div>

