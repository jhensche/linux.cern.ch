## 2023-01-09

### CERN x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
cern-get-certificate | 0.9.4-4.rh9.cern | |
cern-get-certificate | 0.9.4-5.rh9.cern | |

### appstream x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
bcel | 6.4.1-9.el9_1 | |
webkit2gtk3 | 2.36.7-1.el9_1.1 | |
webkit2gtk3-debuginfo | 2.36.7-1.el9_1.1 | |
webkit2gtk3-debugsource | 2.36.7-1.el9_1.1 | |
webkit2gtk3-devel | 2.36.7-1.el9_1.1 | |
webkit2gtk3-devel-debuginfo | 2.36.7-1.el9_1.1 | |
webkit2gtk3-jsc | 2.36.7-1.el9_1.1 | |
webkit2gtk3-jsc-debuginfo | 2.36.7-1.el9_1.1 | |
webkit2gtk3-jsc-devel | 2.36.7-1.el9_1.1 | |
webkit2gtk3-jsc-devel-debuginfo | 2.36.7-1.el9_1.1 | |

### CERN aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
cern-get-certificate | 0.9.4-4.rh9.cern | |
cern-get-certificate | 0.9.4-5.rh9.cern | |

### appstream aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
bcel | 6.4.1-9.el9_1 | |
webkit2gtk3 | 2.36.7-1.el9_1.1 | |
webkit2gtk3-debuginfo | 2.36.7-1.el9_1.1 | |
webkit2gtk3-debugsource | 2.36.7-1.el9_1.1 | |
webkit2gtk3-devel | 2.36.7-1.el9_1.1 | |
webkit2gtk3-devel-debuginfo | 2.36.7-1.el9_1.1 | |
webkit2gtk3-jsc | 2.36.7-1.el9_1.1 | |
webkit2gtk3-jsc-debuginfo | 2.36.7-1.el9_1.1 | |
webkit2gtk3-jsc-devel | 2.36.7-1.el9_1.1 | |
webkit2gtk3-jsc-devel-debuginfo | 2.36.7-1.el9_1.1 | |

