## 2023-01-25

### appstream x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
aspnetcore-runtime-6.0 | 6.0.13-1.el9_1 | |
aspnetcore-runtime-7.0 | 7.0.2-1.el9_1 | |
aspnetcore-targeting-pack-6.0 | 6.0.13-1.el9_1 | |
aspnetcore-targeting-pack-7.0 | 7.0.2-1.el9_1 | |
dotnet-apphost-pack-6.0 | 6.0.13-1.el9_1 | |
dotnet-apphost-pack-6.0-debuginfo | 6.0.13-1.el9_1 | |
dotnet-apphost-pack-7.0 | 7.0.2-1.el9_1 | |
dotnet-apphost-pack-7.0-debuginfo | 7.0.2-1.el9_1 | |
dotnet-host | 7.0.2-1.el9_1 | |
dotnet-host-debuginfo | 7.0.2-1.el9_1 | |
dotnet-hostfxr-6.0 | 6.0.13-1.el9_1 | |
dotnet-hostfxr-6.0-debuginfo | 6.0.13-1.el9_1 | |
dotnet-hostfxr-7.0 | 7.0.2-1.el9_1 | |
dotnet-hostfxr-7.0-debuginfo | 7.0.2-1.el9_1 | |
dotnet-runtime-6.0 | 6.0.13-1.el9_1 | |
dotnet-runtime-6.0-debuginfo | 6.0.13-1.el9_1 | |
dotnet-runtime-7.0 | 7.0.2-1.el9_1 | |
dotnet-runtime-7.0-debuginfo | 7.0.2-1.el9_1 | |
dotnet-sdk-6.0 | 6.0.113-1.el9_1 | |
dotnet-sdk-6.0-debuginfo | 6.0.113-1.el9_1 | |
dotnet-sdk-7.0 | 7.0.102-1.el9_1 | |
dotnet-sdk-7.0-debuginfo | 7.0.102-1.el9_1 | |
dotnet-targeting-pack-6.0 | 6.0.13-1.el9_1 | |
dotnet-targeting-pack-7.0 | 7.0.2-1.el9_1 | |
dotnet-templates-6.0 | 6.0.113-1.el9_1 | |
dotnet-templates-7.0 | 7.0.102-1.el9_1 | |
dotnet6.0-debuginfo | 6.0.113-1.el9_1 | |
dotnet6.0-debugsource | 6.0.113-1.el9_1 | |
dotnet7.0-debuginfo | 7.0.102-1.el9_1 | |
dotnet7.0-debugsource | 7.0.102-1.el9_1 | |
netstandard-targeting-pack-2.1 | 7.0.102-1.el9_1 | |

### codeready-builder x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
dotnet-apphost-pack-6.0-debuginfo | 6.0.13-1.el9_1 | |
dotnet-apphost-pack-7.0-debuginfo | 7.0.2-1.el9_1 | |
dotnet-host-debuginfo | 7.0.2-1.el9_1 | |
dotnet-hostfxr-6.0-debuginfo | 6.0.13-1.el9_1 | |
dotnet-hostfxr-7.0-debuginfo | 7.0.2-1.el9_1 | |
dotnet-runtime-6.0-debuginfo | 6.0.13-1.el9_1 | |
dotnet-runtime-7.0-debuginfo | 7.0.2-1.el9_1 | |
dotnet-sdk-6.0-debuginfo | 6.0.113-1.el9_1 | |
dotnet-sdk-6.0-source-built-artifacts | 6.0.113-1.el9_1 | |
dotnet-sdk-7.0-debuginfo | 7.0.102-1.el9_1 | |
dotnet-sdk-7.0-source-built-artifacts | 7.0.102-1.el9_1 | |
dotnet6.0-debuginfo | 6.0.113-1.el9_1 | |
dotnet6.0-debugsource | 6.0.113-1.el9_1 | |
dotnet7.0-debuginfo | 7.0.102-1.el9_1 | |
dotnet7.0-debugsource | 7.0.102-1.el9_1 | |

### appstream aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
aspnetcore-runtime-6.0 | 6.0.13-1.el9_1 | |
aspnetcore-runtime-7.0 | 7.0.2-1.el9_1 | |
aspnetcore-targeting-pack-6.0 | 6.0.13-1.el9_1 | |
aspnetcore-targeting-pack-7.0 | 7.0.2-1.el9_1 | |
dotnet-apphost-pack-6.0 | 6.0.13-1.el9_1 | |
dotnet-apphost-pack-6.0-debuginfo | 6.0.13-1.el9_1 | |
dotnet-apphost-pack-7.0 | 7.0.2-1.el9_1 | |
dotnet-apphost-pack-7.0-debuginfo | 7.0.2-1.el9_1 | |
dotnet-host | 7.0.2-1.el9_1 | |
dotnet-host-debuginfo | 7.0.2-1.el9_1 | |
dotnet-hostfxr-6.0 | 6.0.13-1.el9_1 | |
dotnet-hostfxr-6.0-debuginfo | 6.0.13-1.el9_1 | |
dotnet-hostfxr-7.0 | 7.0.2-1.el9_1 | |
dotnet-hostfxr-7.0-debuginfo | 7.0.2-1.el9_1 | |
dotnet-runtime-6.0 | 6.0.13-1.el9_1 | |
dotnet-runtime-6.0-debuginfo | 6.0.13-1.el9_1 | |
dotnet-runtime-7.0 | 7.0.2-1.el9_1 | |
dotnet-runtime-7.0-debuginfo | 7.0.2-1.el9_1 | |
dotnet-sdk-6.0 | 6.0.113-1.el9_1 | |
dotnet-sdk-6.0-debuginfo | 6.0.113-1.el9_1 | |
dotnet-sdk-7.0 | 7.0.102-1.el9_1 | |
dotnet-sdk-7.0-debuginfo | 7.0.102-1.el9_1 | |
dotnet-targeting-pack-6.0 | 6.0.13-1.el9_1 | |
dotnet-targeting-pack-7.0 | 7.0.2-1.el9_1 | |
dotnet-templates-6.0 | 6.0.113-1.el9_1 | |
dotnet-templates-7.0 | 7.0.102-1.el9_1 | |
dotnet6.0-debuginfo | 6.0.113-1.el9_1 | |
dotnet6.0-debugsource | 6.0.113-1.el9_1 | |
dotnet7.0-debuginfo | 7.0.102-1.el9_1 | |
dotnet7.0-debugsource | 7.0.102-1.el9_1 | |
netstandard-targeting-pack-2.1 | 7.0.102-1.el9_1 | |

### codeready-builder aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
dotnet-apphost-pack-6.0-debuginfo | 6.0.13-1.el9_1 | |
dotnet-apphost-pack-7.0-debuginfo | 7.0.2-1.el9_1 | |
dotnet-host-debuginfo | 7.0.2-1.el9_1 | |
dotnet-hostfxr-6.0-debuginfo | 6.0.13-1.el9_1 | |
dotnet-hostfxr-7.0-debuginfo | 7.0.2-1.el9_1 | |
dotnet-runtime-6.0-debuginfo | 6.0.13-1.el9_1 | |
dotnet-runtime-7.0-debuginfo | 7.0.2-1.el9_1 | |
dotnet-sdk-6.0-debuginfo | 6.0.113-1.el9_1 | |
dotnet-sdk-6.0-source-built-artifacts | 6.0.113-1.el9_1 | |
dotnet-sdk-7.0-debuginfo | 7.0.102-1.el9_1 | |
dotnet-sdk-7.0-source-built-artifacts | 7.0.102-1.el9_1 | |
dotnet6.0-debuginfo | 6.0.113-1.el9_1 | |
dotnet6.0-debugsource | 6.0.113-1.el9_1 | |
dotnet7.0-debuginfo | 7.0.102-1.el9_1 | |
dotnet7.0-debugsource | 7.0.102-1.el9_1 | |

