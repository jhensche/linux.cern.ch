## 2023-06-21

### baseos x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
c-ares | 1.17.1-5.el9_2.1 | |
c-ares-debuginfo | 1.17.1-5.el9_2.1 | |
c-ares-debugsource | 1.17.1-5.el9_2.1 | |

### appstream x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
c-ares-debuginfo | 1.17.1-5.el9_2.1 | |
c-ares-debugsource | 1.17.1-5.el9_2.1 | |
c-ares-devel | 1.17.1-5.el9_2.1 | |
ignition | 2.15.0-2.el9_2 | [RHBA-2023:3551](https://access.redhat.com/errata/RHBA-2023:3551) | <div class="adv_b">Bug Fix Advisory</div>
ignition-debuginfo | 2.15.0-2.el9_2 | |
ignition-debugsource | 2.15.0-2.el9_2 | |
ignition-edge | 2.15.0-2.el9_2 | [RHBA-2023:3551](https://access.redhat.com/errata/RHBA-2023:3551) | <div class="adv_b">Bug Fix Advisory</div>
ignition-validate | 2.15.0-2.el9_2 | [RHBA-2023:3551](https://access.redhat.com/errata/RHBA-2023:3551) | <div class="adv_b">Bug Fix Advisory</div>
ignition-validate-debuginfo | 2.15.0-2.el9_2 | |

### baseos aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
c-ares | 1.17.1-5.el9_2.1 | |
c-ares-debuginfo | 1.17.1-5.el9_2.1 | |
c-ares-debugsource | 1.17.1-5.el9_2.1 | |

### appstream aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
c-ares-debuginfo | 1.17.1-5.el9_2.1 | |
c-ares-debugsource | 1.17.1-5.el9_2.1 | |
c-ares-devel | 1.17.1-5.el9_2.1 | |
ignition | 2.15.0-2.el9_2 | [RHBA-2023:3551](https://access.redhat.com/errata/RHBA-2023:3551) | <div class="adv_b">Bug Fix Advisory</div>
ignition-debuginfo | 2.15.0-2.el9_2 | |
ignition-debugsource | 2.15.0-2.el9_2 | |
ignition-edge | 2.15.0-2.el9_2 | [RHBA-2023:3551](https://access.redhat.com/errata/RHBA-2023:3551) | <div class="adv_b">Bug Fix Advisory</div>
ignition-validate | 2.15.0-2.el9_2 | [RHBA-2023:3551](https://access.redhat.com/errata/RHBA-2023:3551) | <div class="adv_b">Bug Fix Advisory</div>
ignition-validate-debuginfo | 2.15.0-2.el9_2 | |

