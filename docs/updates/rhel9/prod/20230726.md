## 2023-07-26

### CERN x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
locmap-release | 1.0-9.rh9.cern | |

### appstream x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
aspnetcore-runtime-6.0 | 6.0.20-1.el9_2 | [RHSA-2023:4060](https://access.redhat.com/errata/RHSA-2023:4060) | <div class="adv_s">Security Advisory</div> ([CVE-2023-33170](https://access.redhat.com/security/cve/CVE-2023-33170))
aspnetcore-runtime-7.0 | 7.0.9-1.el9_2 | [RHSA-2023:4057](https://access.redhat.com/errata/RHSA-2023:4057) | <div class="adv_s">Security Advisory</div> ([CVE-2023-33170](https://access.redhat.com/security/cve/CVE-2023-33170))
aspnetcore-targeting-pack-6.0 | 6.0.20-1.el9_2 | [RHSA-2023:4060](https://access.redhat.com/errata/RHSA-2023:4060) | <div class="adv_s">Security Advisory</div> ([CVE-2023-33170](https://access.redhat.com/security/cve/CVE-2023-33170))
aspnetcore-targeting-pack-7.0 | 7.0.9-1.el9_2 | [RHSA-2023:4057](https://access.redhat.com/errata/RHSA-2023:4057) | <div class="adv_s">Security Advisory</div> ([CVE-2023-33170](https://access.redhat.com/security/cve/CVE-2023-33170))
bind | 9.16.23-11.el9_2.1 | [RHSA-2023:4099](https://access.redhat.com/errata/RHSA-2023:4099) | <div class="adv_s">Security Advisory</div> ([CVE-2023-2828](https://access.redhat.com/security/cve/CVE-2023-2828))
bind-chroot | 9.16.23-11.el9_2.1 | [RHSA-2023:4099](https://access.redhat.com/errata/RHSA-2023:4099) | <div class="adv_s">Security Advisory</div> ([CVE-2023-2828](https://access.redhat.com/security/cve/CVE-2023-2828))
bind-debuginfo | 9.16.23-11.el9_2.1 | |
bind-debugsource | 9.16.23-11.el9_2.1 | |
bind-dnssec-doc | 9.16.23-11.el9_2.1 | [RHSA-2023:4099](https://access.redhat.com/errata/RHSA-2023:4099) | <div class="adv_s">Security Advisory</div> ([CVE-2023-2828](https://access.redhat.com/security/cve/CVE-2023-2828))
bind-dnssec-utils | 9.16.23-11.el9_2.1 | [RHSA-2023:4099](https://access.redhat.com/errata/RHSA-2023:4099) | <div class="adv_s">Security Advisory</div> ([CVE-2023-2828](https://access.redhat.com/security/cve/CVE-2023-2828))
bind-dnssec-utils-debuginfo | 9.16.23-11.el9_2.1 | |
bind-libs | 9.16.23-11.el9_2.1 | [RHSA-2023:4099](https://access.redhat.com/errata/RHSA-2023:4099) | <div class="adv_s">Security Advisory</div> ([CVE-2023-2828](https://access.redhat.com/security/cve/CVE-2023-2828))
bind-libs-debuginfo | 9.16.23-11.el9_2.1 | |
bind-license | 9.16.23-11.el9_2.1 | [RHSA-2023:4099](https://access.redhat.com/errata/RHSA-2023:4099) | <div class="adv_s">Security Advisory</div> ([CVE-2023-2828](https://access.redhat.com/security/cve/CVE-2023-2828))
bind-utils | 9.16.23-11.el9_2.1 | [RHSA-2023:4099](https://access.redhat.com/errata/RHSA-2023:4099) | <div class="adv_s">Security Advisory</div> ([CVE-2023-2828](https://access.redhat.com/security/cve/CVE-2023-2828))
bind-utils-debuginfo | 9.16.23-11.el9_2.1 | |
dotnet-apphost-pack-6.0 | 6.0.20-1.el9_2 | [RHSA-2023:4060](https://access.redhat.com/errata/RHSA-2023:4060) | <div class="adv_s">Security Advisory</div> ([CVE-2023-33170](https://access.redhat.com/security/cve/CVE-2023-33170))
dotnet-apphost-pack-6.0-debuginfo | 6.0.20-1.el9_2 | |
dotnet-apphost-pack-7.0 | 7.0.9-1.el9_2 | [RHSA-2023:4057](https://access.redhat.com/errata/RHSA-2023:4057) | <div class="adv_s">Security Advisory</div> ([CVE-2023-33170](https://access.redhat.com/security/cve/CVE-2023-33170))
dotnet-apphost-pack-7.0-debuginfo | 7.0.9-1.el9_2 | |
dotnet-host | 7.0.9-1.el9_2 | [RHSA-2023:4057](https://access.redhat.com/errata/RHSA-2023:4057) | <div class="adv_s">Security Advisory</div> ([CVE-2023-33170](https://access.redhat.com/security/cve/CVE-2023-33170))
dotnet-host-debuginfo | 7.0.9-1.el9_2 | |
dotnet-hostfxr-6.0 | 6.0.20-1.el9_2 | [RHSA-2023:4060](https://access.redhat.com/errata/RHSA-2023:4060) | <div class="adv_s">Security Advisory</div> ([CVE-2023-33170](https://access.redhat.com/security/cve/CVE-2023-33170))
dotnet-hostfxr-6.0-debuginfo | 6.0.20-1.el9_2 | |
dotnet-hostfxr-7.0 | 7.0.9-1.el9_2 | [RHSA-2023:4057](https://access.redhat.com/errata/RHSA-2023:4057) | <div class="adv_s">Security Advisory</div> ([CVE-2023-33170](https://access.redhat.com/security/cve/CVE-2023-33170))
dotnet-hostfxr-7.0-debuginfo | 7.0.9-1.el9_2 | |
dotnet-runtime-6.0 | 6.0.20-1.el9_2 | [RHSA-2023:4060](https://access.redhat.com/errata/RHSA-2023:4060) | <div class="adv_s">Security Advisory</div> ([CVE-2023-33170](https://access.redhat.com/security/cve/CVE-2023-33170))
dotnet-runtime-6.0-debuginfo | 6.0.20-1.el9_2 | |
dotnet-runtime-7.0 | 7.0.9-1.el9_2 | [RHSA-2023:4057](https://access.redhat.com/errata/RHSA-2023:4057) | <div class="adv_s">Security Advisory</div> ([CVE-2023-33170](https://access.redhat.com/security/cve/CVE-2023-33170))
dotnet-runtime-7.0-debuginfo | 7.0.9-1.el9_2 | |
dotnet-sdk-6.0 | 6.0.120-1.el9_2 | [RHSA-2023:4060](https://access.redhat.com/errata/RHSA-2023:4060) | <div class="adv_s">Security Advisory</div> ([CVE-2023-33170](https://access.redhat.com/security/cve/CVE-2023-33170))
dotnet-sdk-6.0-debuginfo | 6.0.120-1.el9_2 | |
dotnet-sdk-7.0 | 7.0.109-1.el9_2 | [RHSA-2023:4057](https://access.redhat.com/errata/RHSA-2023:4057) | <div class="adv_s">Security Advisory</div> ([CVE-2023-33170](https://access.redhat.com/security/cve/CVE-2023-33170))
dotnet-sdk-7.0-debuginfo | 7.0.109-1.el9_2 | |
dotnet-targeting-pack-6.0 | 6.0.20-1.el9_2 | [RHSA-2023:4060](https://access.redhat.com/errata/RHSA-2023:4060) | <div class="adv_s">Security Advisory</div> ([CVE-2023-33170](https://access.redhat.com/security/cve/CVE-2023-33170))
dotnet-targeting-pack-7.0 | 7.0.9-1.el9_2 | [RHSA-2023:4057](https://access.redhat.com/errata/RHSA-2023:4057) | <div class="adv_s">Security Advisory</div> ([CVE-2023-33170](https://access.redhat.com/security/cve/CVE-2023-33170))
dotnet-templates-6.0 | 6.0.120-1.el9_2 | [RHSA-2023:4060](https://access.redhat.com/errata/RHSA-2023:4060) | <div class="adv_s">Security Advisory</div> ([CVE-2023-33170](https://access.redhat.com/security/cve/CVE-2023-33170))
dotnet-templates-7.0 | 7.0.109-1.el9_2 | [RHSA-2023:4057](https://access.redhat.com/errata/RHSA-2023:4057) | <div class="adv_s">Security Advisory</div> ([CVE-2023-33170](https://access.redhat.com/security/cve/CVE-2023-33170))
dotnet6.0-debuginfo | 6.0.120-1.el9_2 | |
dotnet6.0-debugsource | 6.0.120-1.el9_2 | |
dotnet7.0-debuginfo | 7.0.109-1.el9_2 | |
dotnet7.0-debugsource | 7.0.109-1.el9_2 | |
firefox | 102.13.0-2.el9_2 | [RHSA-2023:4071](https://access.redhat.com/errata/RHSA-2023:4071) | <div class="adv_s">Security Advisory</div> ([CVE-2023-37201](https://access.redhat.com/security/cve/CVE-2023-37201), [CVE-2023-37202](https://access.redhat.com/security/cve/CVE-2023-37202), [CVE-2023-37207](https://access.redhat.com/security/cve/CVE-2023-37207), [CVE-2023-37208](https://access.redhat.com/security/cve/CVE-2023-37208), [CVE-2023-37211](https://access.redhat.com/security/cve/CVE-2023-37211))
firefox-debuginfo | 102.13.0-2.el9_2 | |
firefox-debugsource | 102.13.0-2.el9_2 | |
firefox-x11 | 102.13.0-2.el9_2 | [RHSA-2023:4071](https://access.redhat.com/errata/RHSA-2023:4071) | <div class="adv_s">Security Advisory</div> ([CVE-2023-37201](https://access.redhat.com/security/cve/CVE-2023-37201), [CVE-2023-37202](https://access.redhat.com/security/cve/CVE-2023-37202), [CVE-2023-37207](https://access.redhat.com/security/cve/CVE-2023-37207), [CVE-2023-37208](https://access.redhat.com/security/cve/CVE-2023-37208), [CVE-2023-37211](https://access.redhat.com/security/cve/CVE-2023-37211))
netstandard-targeting-pack-2.1 | 7.0.109-1.el9_2 | [RHSA-2023:4057](https://access.redhat.com/errata/RHSA-2023:4057) | <div class="adv_s">Security Advisory</div> ([CVE-2023-33170](https://access.redhat.com/security/cve/CVE-2023-33170))
python3-bind | 9.16.23-11.el9_2.1 | [RHSA-2023:4099](https://access.redhat.com/errata/RHSA-2023:4099) | <div class="adv_s">Security Advisory</div> ([CVE-2023-2828](https://access.redhat.com/security/cve/CVE-2023-2828))
thunderbird | 102.13.0-2.el9_2 | [RHSA-2023:4064](https://access.redhat.com/errata/RHSA-2023:4064) | <div class="adv_s">Security Advisory</div> ([CVE-2023-37201](https://access.redhat.com/security/cve/CVE-2023-37201), [CVE-2023-37202](https://access.redhat.com/security/cve/CVE-2023-37202), [CVE-2023-37207](https://access.redhat.com/security/cve/CVE-2023-37207), [CVE-2023-37208](https://access.redhat.com/security/cve/CVE-2023-37208), [CVE-2023-37211](https://access.redhat.com/security/cve/CVE-2023-37211))
thunderbird-debuginfo | 102.13.0-2.el9_2 | |
thunderbird-debugsource | 102.13.0-2.el9_2 | |
webkit2gtk3 | 2.38.5-1.el9_2.3 | [RHSA-2023:4201](https://access.redhat.com/errata/RHSA-2023:4201) | <div class="adv_s">Security Advisory</div> ([CVE-2023-32435](https://access.redhat.com/security/cve/CVE-2023-32435), [CVE-2023-32439](https://access.redhat.com/security/cve/CVE-2023-32439), [CVE-2023-37450](https://access.redhat.com/security/cve/CVE-2023-37450))
webkit2gtk3-debuginfo | 2.38.5-1.el9_2.3 | |
webkit2gtk3-debugsource | 2.38.5-1.el9_2.3 | |
webkit2gtk3-devel | 2.38.5-1.el9_2.3 | [RHSA-2023:4201](https://access.redhat.com/errata/RHSA-2023:4201) | <div class="adv_s">Security Advisory</div> ([CVE-2023-32435](https://access.redhat.com/security/cve/CVE-2023-32435), [CVE-2023-32439](https://access.redhat.com/security/cve/CVE-2023-32439), [CVE-2023-37450](https://access.redhat.com/security/cve/CVE-2023-37450))
webkit2gtk3-devel-debuginfo | 2.38.5-1.el9_2.3 | |
webkit2gtk3-jsc | 2.38.5-1.el9_2.3 | [RHSA-2023:4201](https://access.redhat.com/errata/RHSA-2023:4201) | <div class="adv_s">Security Advisory</div> ([CVE-2023-32435](https://access.redhat.com/security/cve/CVE-2023-32435), [CVE-2023-32439](https://access.redhat.com/security/cve/CVE-2023-32439), [CVE-2023-37450](https://access.redhat.com/security/cve/CVE-2023-37450))
webkit2gtk3-jsc-debuginfo | 2.38.5-1.el9_2.3 | |
webkit2gtk3-jsc-devel | 2.38.5-1.el9_2.3 | [RHSA-2023:4201](https://access.redhat.com/errata/RHSA-2023:4201) | <div class="adv_s">Security Advisory</div> ([CVE-2023-32435](https://access.redhat.com/security/cve/CVE-2023-32435), [CVE-2023-32439](https://access.redhat.com/security/cve/CVE-2023-32439), [CVE-2023-37450](https://access.redhat.com/security/cve/CVE-2023-37450))
webkit2gtk3-jsc-devel-debuginfo | 2.38.5-1.el9_2.3 | |

### codeready-builder x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
bind-debuginfo | 9.16.23-11.el9_2.1 | |
bind-debugsource | 9.16.23-11.el9_2.1 | |
bind-devel | 9.16.23-11.el9_2.1 | [RHSA-2023:4099](https://access.redhat.com/errata/RHSA-2023:4099) | <div class="adv_s">Security Advisory</div> ([CVE-2023-2828](https://access.redhat.com/security/cve/CVE-2023-2828))
bind-dnssec-utils-debuginfo | 9.16.23-11.el9_2.1 | |
bind-doc | 9.16.23-11.el9_2.1 | [RHSA-2023:4099](https://access.redhat.com/errata/RHSA-2023:4099) | <div class="adv_s">Security Advisory</div> ([CVE-2023-2828](https://access.redhat.com/security/cve/CVE-2023-2828))
bind-libs-debuginfo | 9.16.23-11.el9_2.1 | |
bind-utils-debuginfo | 9.16.23-11.el9_2.1 | |
dotnet-apphost-pack-6.0-debuginfo | 6.0.20-1.el9_2 | |
dotnet-apphost-pack-7.0-debuginfo | 7.0.9-1.el9_2 | |
dotnet-host-debuginfo | 7.0.9-1.el9_2 | |
dotnet-hostfxr-6.0-debuginfo | 6.0.20-1.el9_2 | |
dotnet-hostfxr-7.0-debuginfo | 7.0.9-1.el9_2 | |
dotnet-runtime-6.0-debuginfo | 6.0.20-1.el9_2 | |
dotnet-runtime-7.0-debuginfo | 7.0.9-1.el9_2 | |
dotnet-sdk-6.0-debuginfo | 6.0.120-1.el9_2 | |
dotnet-sdk-6.0-source-built-artifacts | 6.0.120-1.el9_2 | [RHSA-2023:4060](https://access.redhat.com/errata/RHSA-2023:4060) | <div class="adv_s">Security Advisory</div> ([CVE-2023-33170](https://access.redhat.com/security/cve/CVE-2023-33170))
dotnet-sdk-7.0-debuginfo | 7.0.109-1.el9_2 | |
dotnet-sdk-7.0-source-built-artifacts | 7.0.109-1.el9_2 | [RHSA-2023:4057](https://access.redhat.com/errata/RHSA-2023:4057) | <div class="adv_s">Security Advisory</div> ([CVE-2023-33170](https://access.redhat.com/security/cve/CVE-2023-33170))
dotnet6.0-debuginfo | 6.0.120-1.el9_2 | |
dotnet6.0-debugsource | 6.0.120-1.el9_2 | |
dotnet7.0-debuginfo | 7.0.109-1.el9_2 | |
dotnet7.0-debugsource | 7.0.109-1.el9_2 | |

### CERN aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
locmap-release | 1.0-9.rh9.cern | |

### appstream aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
aspnetcore-runtime-6.0 | 6.0.20-1.el9_2 | [RHSA-2023:4060](https://access.redhat.com/errata/RHSA-2023:4060) | <div class="adv_s">Security Advisory</div> ([CVE-2023-33170](https://access.redhat.com/security/cve/CVE-2023-33170))
aspnetcore-runtime-7.0 | 7.0.9-1.el9_2 | [RHSA-2023:4057](https://access.redhat.com/errata/RHSA-2023:4057) | <div class="adv_s">Security Advisory</div> ([CVE-2023-33170](https://access.redhat.com/security/cve/CVE-2023-33170))
aspnetcore-targeting-pack-6.0 | 6.0.20-1.el9_2 | [RHSA-2023:4060](https://access.redhat.com/errata/RHSA-2023:4060) | <div class="adv_s">Security Advisory</div> ([CVE-2023-33170](https://access.redhat.com/security/cve/CVE-2023-33170))
aspnetcore-targeting-pack-7.0 | 7.0.9-1.el9_2 | [RHSA-2023:4057](https://access.redhat.com/errata/RHSA-2023:4057) | <div class="adv_s">Security Advisory</div> ([CVE-2023-33170](https://access.redhat.com/security/cve/CVE-2023-33170))
bind | 9.16.23-11.el9_2.1 | [RHSA-2023:4099](https://access.redhat.com/errata/RHSA-2023:4099) | <div class="adv_s">Security Advisory</div> ([CVE-2023-2828](https://access.redhat.com/security/cve/CVE-2023-2828))
bind-chroot | 9.16.23-11.el9_2.1 | [RHSA-2023:4099](https://access.redhat.com/errata/RHSA-2023:4099) | <div class="adv_s">Security Advisory</div> ([CVE-2023-2828](https://access.redhat.com/security/cve/CVE-2023-2828))
bind-debuginfo | 9.16.23-11.el9_2.1 | |
bind-debugsource | 9.16.23-11.el9_2.1 | |
bind-dnssec-doc | 9.16.23-11.el9_2.1 | [RHSA-2023:4099](https://access.redhat.com/errata/RHSA-2023:4099) | <div class="adv_s">Security Advisory</div> ([CVE-2023-2828](https://access.redhat.com/security/cve/CVE-2023-2828))
bind-dnssec-utils | 9.16.23-11.el9_2.1 | [RHSA-2023:4099](https://access.redhat.com/errata/RHSA-2023:4099) | <div class="adv_s">Security Advisory</div> ([CVE-2023-2828](https://access.redhat.com/security/cve/CVE-2023-2828))
bind-dnssec-utils-debuginfo | 9.16.23-11.el9_2.1 | |
bind-libs | 9.16.23-11.el9_2.1 | [RHSA-2023:4099](https://access.redhat.com/errata/RHSA-2023:4099) | <div class="adv_s">Security Advisory</div> ([CVE-2023-2828](https://access.redhat.com/security/cve/CVE-2023-2828))
bind-libs-debuginfo | 9.16.23-11.el9_2.1 | |
bind-license | 9.16.23-11.el9_2.1 | [RHSA-2023:4099](https://access.redhat.com/errata/RHSA-2023:4099) | <div class="adv_s">Security Advisory</div> ([CVE-2023-2828](https://access.redhat.com/security/cve/CVE-2023-2828))
bind-utils | 9.16.23-11.el9_2.1 | [RHSA-2023:4099](https://access.redhat.com/errata/RHSA-2023:4099) | <div class="adv_s">Security Advisory</div> ([CVE-2023-2828](https://access.redhat.com/security/cve/CVE-2023-2828))
bind-utils-debuginfo | 9.16.23-11.el9_2.1 | |
dotnet-apphost-pack-6.0 | 6.0.20-1.el9_2 | [RHSA-2023:4060](https://access.redhat.com/errata/RHSA-2023:4060) | <div class="adv_s">Security Advisory</div> ([CVE-2023-33170](https://access.redhat.com/security/cve/CVE-2023-33170))
dotnet-apphost-pack-6.0-debuginfo | 6.0.20-1.el9_2 | |
dotnet-apphost-pack-7.0 | 7.0.9-1.el9_2 | [RHSA-2023:4057](https://access.redhat.com/errata/RHSA-2023:4057) | <div class="adv_s">Security Advisory</div> ([CVE-2023-33170](https://access.redhat.com/security/cve/CVE-2023-33170))
dotnet-apphost-pack-7.0-debuginfo | 7.0.9-1.el9_2 | |
dotnet-host | 7.0.9-1.el9_2 | [RHSA-2023:4057](https://access.redhat.com/errata/RHSA-2023:4057) | <div class="adv_s">Security Advisory</div> ([CVE-2023-33170](https://access.redhat.com/security/cve/CVE-2023-33170))
dotnet-host-debuginfo | 7.0.9-1.el9_2 | |
dotnet-hostfxr-6.0 | 6.0.20-1.el9_2 | [RHSA-2023:4060](https://access.redhat.com/errata/RHSA-2023:4060) | <div class="adv_s">Security Advisory</div> ([CVE-2023-33170](https://access.redhat.com/security/cve/CVE-2023-33170))
dotnet-hostfxr-6.0-debuginfo | 6.0.20-1.el9_2 | |
dotnet-hostfxr-7.0 | 7.0.9-1.el9_2 | [RHSA-2023:4057](https://access.redhat.com/errata/RHSA-2023:4057) | <div class="adv_s">Security Advisory</div> ([CVE-2023-33170](https://access.redhat.com/security/cve/CVE-2023-33170))
dotnet-hostfxr-7.0-debuginfo | 7.0.9-1.el9_2 | |
dotnet-runtime-6.0 | 6.0.20-1.el9_2 | [RHSA-2023:4060](https://access.redhat.com/errata/RHSA-2023:4060) | <div class="adv_s">Security Advisory</div> ([CVE-2023-33170](https://access.redhat.com/security/cve/CVE-2023-33170))
dotnet-runtime-6.0-debuginfo | 6.0.20-1.el9_2 | |
dotnet-runtime-7.0 | 7.0.9-1.el9_2 | [RHSA-2023:4057](https://access.redhat.com/errata/RHSA-2023:4057) | <div class="adv_s">Security Advisory</div> ([CVE-2023-33170](https://access.redhat.com/security/cve/CVE-2023-33170))
dotnet-runtime-7.0-debuginfo | 7.0.9-1.el9_2 | |
dotnet-sdk-6.0 | 6.0.120-1.el9_2 | [RHSA-2023:4060](https://access.redhat.com/errata/RHSA-2023:4060) | <div class="adv_s">Security Advisory</div> ([CVE-2023-33170](https://access.redhat.com/security/cve/CVE-2023-33170))
dotnet-sdk-6.0-debuginfo | 6.0.120-1.el9_2 | |
dotnet-sdk-7.0 | 7.0.109-1.el9_2 | [RHSA-2023:4057](https://access.redhat.com/errata/RHSA-2023:4057) | <div class="adv_s">Security Advisory</div> ([CVE-2023-33170](https://access.redhat.com/security/cve/CVE-2023-33170))
dotnet-sdk-7.0-debuginfo | 7.0.109-1.el9_2 | |
dotnet-targeting-pack-6.0 | 6.0.20-1.el9_2 | [RHSA-2023:4060](https://access.redhat.com/errata/RHSA-2023:4060) | <div class="adv_s">Security Advisory</div> ([CVE-2023-33170](https://access.redhat.com/security/cve/CVE-2023-33170))
dotnet-targeting-pack-7.0 | 7.0.9-1.el9_2 | [RHSA-2023:4057](https://access.redhat.com/errata/RHSA-2023:4057) | <div class="adv_s">Security Advisory</div> ([CVE-2023-33170](https://access.redhat.com/security/cve/CVE-2023-33170))
dotnet-templates-6.0 | 6.0.120-1.el9_2 | [RHSA-2023:4060](https://access.redhat.com/errata/RHSA-2023:4060) | <div class="adv_s">Security Advisory</div> ([CVE-2023-33170](https://access.redhat.com/security/cve/CVE-2023-33170))
dotnet-templates-7.0 | 7.0.109-1.el9_2 | [RHSA-2023:4057](https://access.redhat.com/errata/RHSA-2023:4057) | <div class="adv_s">Security Advisory</div> ([CVE-2023-33170](https://access.redhat.com/security/cve/CVE-2023-33170))
dotnet6.0-debuginfo | 6.0.120-1.el9_2 | |
dotnet6.0-debugsource | 6.0.120-1.el9_2 | |
dotnet7.0-debuginfo | 7.0.109-1.el9_2 | |
dotnet7.0-debugsource | 7.0.109-1.el9_2 | |
firefox | 102.13.0-2.el9_2 | [RHSA-2023:4071](https://access.redhat.com/errata/RHSA-2023:4071) | <div class="adv_s">Security Advisory</div> ([CVE-2023-37201](https://access.redhat.com/security/cve/CVE-2023-37201), [CVE-2023-37202](https://access.redhat.com/security/cve/CVE-2023-37202), [CVE-2023-37207](https://access.redhat.com/security/cve/CVE-2023-37207), [CVE-2023-37208](https://access.redhat.com/security/cve/CVE-2023-37208), [CVE-2023-37211](https://access.redhat.com/security/cve/CVE-2023-37211))
firefox-debuginfo | 102.13.0-2.el9_2 | |
firefox-debugsource | 102.13.0-2.el9_2 | |
firefox-x11 | 102.13.0-2.el9_2 | [RHSA-2023:4071](https://access.redhat.com/errata/RHSA-2023:4071) | <div class="adv_s">Security Advisory</div> ([CVE-2023-37201](https://access.redhat.com/security/cve/CVE-2023-37201), [CVE-2023-37202](https://access.redhat.com/security/cve/CVE-2023-37202), [CVE-2023-37207](https://access.redhat.com/security/cve/CVE-2023-37207), [CVE-2023-37208](https://access.redhat.com/security/cve/CVE-2023-37208), [CVE-2023-37211](https://access.redhat.com/security/cve/CVE-2023-37211))
netstandard-targeting-pack-2.1 | 7.0.109-1.el9_2 | [RHSA-2023:4057](https://access.redhat.com/errata/RHSA-2023:4057) | <div class="adv_s">Security Advisory</div> ([CVE-2023-33170](https://access.redhat.com/security/cve/CVE-2023-33170))
python3-bind | 9.16.23-11.el9_2.1 | [RHSA-2023:4099](https://access.redhat.com/errata/RHSA-2023:4099) | <div class="adv_s">Security Advisory</div> ([CVE-2023-2828](https://access.redhat.com/security/cve/CVE-2023-2828))
thunderbird | 102.13.0-2.el9_2 | [RHSA-2023:4064](https://access.redhat.com/errata/RHSA-2023:4064) | <div class="adv_s">Security Advisory</div> ([CVE-2023-37201](https://access.redhat.com/security/cve/CVE-2023-37201), [CVE-2023-37202](https://access.redhat.com/security/cve/CVE-2023-37202), [CVE-2023-37207](https://access.redhat.com/security/cve/CVE-2023-37207), [CVE-2023-37208](https://access.redhat.com/security/cve/CVE-2023-37208), [CVE-2023-37211](https://access.redhat.com/security/cve/CVE-2023-37211))
thunderbird-debuginfo | 102.13.0-2.el9_2 | |
thunderbird-debugsource | 102.13.0-2.el9_2 | |
webkit2gtk3 | 2.38.5-1.el9_2.3 | [RHSA-2023:4201](https://access.redhat.com/errata/RHSA-2023:4201) | <div class="adv_s">Security Advisory</div> ([CVE-2023-32435](https://access.redhat.com/security/cve/CVE-2023-32435), [CVE-2023-32439](https://access.redhat.com/security/cve/CVE-2023-32439), [CVE-2023-37450](https://access.redhat.com/security/cve/CVE-2023-37450))
webkit2gtk3-debuginfo | 2.38.5-1.el9_2.3 | |
webkit2gtk3-debugsource | 2.38.5-1.el9_2.3 | |
webkit2gtk3-devel | 2.38.5-1.el9_2.3 | [RHSA-2023:4201](https://access.redhat.com/errata/RHSA-2023:4201) | <div class="adv_s">Security Advisory</div> ([CVE-2023-32435](https://access.redhat.com/security/cve/CVE-2023-32435), [CVE-2023-32439](https://access.redhat.com/security/cve/CVE-2023-32439), [CVE-2023-37450](https://access.redhat.com/security/cve/CVE-2023-37450))
webkit2gtk3-devel-debuginfo | 2.38.5-1.el9_2.3 | |
webkit2gtk3-jsc | 2.38.5-1.el9_2.3 | [RHSA-2023:4201](https://access.redhat.com/errata/RHSA-2023:4201) | <div class="adv_s">Security Advisory</div> ([CVE-2023-32435](https://access.redhat.com/security/cve/CVE-2023-32435), [CVE-2023-32439](https://access.redhat.com/security/cve/CVE-2023-32439), [CVE-2023-37450](https://access.redhat.com/security/cve/CVE-2023-37450))
webkit2gtk3-jsc-debuginfo | 2.38.5-1.el9_2.3 | |
webkit2gtk3-jsc-devel | 2.38.5-1.el9_2.3 | [RHSA-2023:4201](https://access.redhat.com/errata/RHSA-2023:4201) | <div class="adv_s">Security Advisory</div> ([CVE-2023-32435](https://access.redhat.com/security/cve/CVE-2023-32435), [CVE-2023-32439](https://access.redhat.com/security/cve/CVE-2023-32439), [CVE-2023-37450](https://access.redhat.com/security/cve/CVE-2023-37450))
webkit2gtk3-jsc-devel-debuginfo | 2.38.5-1.el9_2.3 | |

### codeready-builder aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
bind-debuginfo | 9.16.23-11.el9_2.1 | |
bind-debugsource | 9.16.23-11.el9_2.1 | |
bind-devel | 9.16.23-11.el9_2.1 | [RHSA-2023:4099](https://access.redhat.com/errata/RHSA-2023:4099) | <div class="adv_s">Security Advisory</div> ([CVE-2023-2828](https://access.redhat.com/security/cve/CVE-2023-2828))
bind-dnssec-utils-debuginfo | 9.16.23-11.el9_2.1 | |
bind-doc | 9.16.23-11.el9_2.1 | [RHSA-2023:4099](https://access.redhat.com/errata/RHSA-2023:4099) | <div class="adv_s">Security Advisory</div> ([CVE-2023-2828](https://access.redhat.com/security/cve/CVE-2023-2828))
bind-libs-debuginfo | 9.16.23-11.el9_2.1 | |
bind-utils-debuginfo | 9.16.23-11.el9_2.1 | |
dotnet-apphost-pack-6.0-debuginfo | 6.0.20-1.el9_2 | |
dotnet-apphost-pack-7.0-debuginfo | 7.0.9-1.el9_2 | |
dotnet-host-debuginfo | 7.0.9-1.el9_2 | |
dotnet-hostfxr-6.0-debuginfo | 6.0.20-1.el9_2 | |
dotnet-hostfxr-7.0-debuginfo | 7.0.9-1.el9_2 | |
dotnet-runtime-6.0-debuginfo | 6.0.20-1.el9_2 | |
dotnet-runtime-7.0-debuginfo | 7.0.9-1.el9_2 | |
dotnet-sdk-6.0-debuginfo | 6.0.120-1.el9_2 | |
dotnet-sdk-6.0-source-built-artifacts | 6.0.120-1.el9_2 | [RHSA-2023:4060](https://access.redhat.com/errata/RHSA-2023:4060) | <div class="adv_s">Security Advisory</div> ([CVE-2023-33170](https://access.redhat.com/security/cve/CVE-2023-33170))
dotnet-sdk-7.0-debuginfo | 7.0.109-1.el9_2 | |
dotnet-sdk-7.0-source-built-artifacts | 7.0.109-1.el9_2 | [RHSA-2023:4057](https://access.redhat.com/errata/RHSA-2023:4057) | <div class="adv_s">Security Advisory</div> ([CVE-2023-33170](https://access.redhat.com/security/cve/CVE-2023-33170))
dotnet6.0-debuginfo | 6.0.120-1.el9_2 | |
dotnet6.0-debugsource | 6.0.120-1.el9_2 | |
dotnet7.0-debuginfo | 7.0.109-1.el9_2 | |
dotnet7.0-debugsource | 7.0.109-1.el9_2 | |

