## 2023-08-17

### extras x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
centos-release-cloud | 1-1.el9 | |
centos-release-nfv-common | 1-5.el9 | |
centos-release-nfv-openvswitch | 1-5.el9 | |
centos-release-openstack-yoga | 1-4.el9 | |
centos-release-ovirt45 | 9.2-1.el9 | |
centos-release-ovirt45-testing | 9.2-1.el9 | |

### extras aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
centos-release-cloud | 1-1.el9 | |
centos-release-nfv-common | 1-5.el9 | |
centos-release-nfv-openvswitch | 1-5.el9 | |
centos-release-openstack-yoga | 1-4.el9 | |
centos-release-ovirt45 | 9.2-1.el9 | |
centos-release-ovirt45-testing | 9.2-1.el9 | |

