## 2023-11-06

### AppStream x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
aspnetcore-runtime-6.0 | 6.0.24-1.el9_2 | [ALSA-2023:6242](https://errata.almalinux.org/9/ALSA-2023-6242.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-36799](https://access.redhat.com/security/cve/CVE-2023-36799))
aspnetcore-runtime-7.0 | 7.0.13-1.el9_2 | [ALSA-2023:6246](https://errata.almalinux.org/9/ALSA-2023-6246.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-36799](https://access.redhat.com/security/cve/CVE-2023-36799))
aspnetcore-targeting-pack-6.0 | 6.0.24-1.el9_2 | [ALSA-2023:6242](https://errata.almalinux.org/9/ALSA-2023-6242.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-36799](https://access.redhat.com/security/cve/CVE-2023-36799))
aspnetcore-targeting-pack-7.0 | 7.0.13-1.el9_2 | [ALSA-2023:6246](https://errata.almalinux.org/9/ALSA-2023-6246.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-36799](https://access.redhat.com/security/cve/CVE-2023-36799))
dotnet-apphost-pack-6.0 | 6.0.24-1.el9_2 | [ALSA-2023:6242](https://errata.almalinux.org/9/ALSA-2023-6242.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-36799](https://access.redhat.com/security/cve/CVE-2023-36799))
dotnet-apphost-pack-6.0-debuginfo | 6.0.24-1.el9_2 | |
dotnet-apphost-pack-7.0 | 7.0.13-1.el9_2 | [ALSA-2023:6246](https://errata.almalinux.org/9/ALSA-2023-6246.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-36799](https://access.redhat.com/security/cve/CVE-2023-36799))
dotnet-apphost-pack-7.0-debuginfo | 7.0.13-1.el9_2 | |
dotnet-host | 7.0.13-1.el9_2 | [ALSA-2023:6246](https://errata.almalinux.org/9/ALSA-2023-6246.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-36799](https://access.redhat.com/security/cve/CVE-2023-36799))
dotnet-host-debuginfo | 7.0.13-1.el9_2 | |
dotnet-hostfxr-6.0 | 6.0.24-1.el9_2 | [ALSA-2023:6242](https://errata.almalinux.org/9/ALSA-2023-6242.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-36799](https://access.redhat.com/security/cve/CVE-2023-36799))
dotnet-hostfxr-6.0-debuginfo | 6.0.24-1.el9_2 | |
dotnet-hostfxr-7.0 | 7.0.13-1.el9_2 | [ALSA-2023:6246](https://errata.almalinux.org/9/ALSA-2023-6246.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-36799](https://access.redhat.com/security/cve/CVE-2023-36799))
dotnet-hostfxr-7.0-debuginfo | 7.0.13-1.el9_2 | |
dotnet-runtime-6.0 | 6.0.24-1.el9_2 | [ALSA-2023:6242](https://errata.almalinux.org/9/ALSA-2023-6242.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-36799](https://access.redhat.com/security/cve/CVE-2023-36799))
dotnet-runtime-6.0-debuginfo | 6.0.24-1.el9_2 | |
dotnet-runtime-7.0 | 7.0.13-1.el9_2 | [ALSA-2023:6246](https://errata.almalinux.org/9/ALSA-2023-6246.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-36799](https://access.redhat.com/security/cve/CVE-2023-36799))
dotnet-runtime-7.0-debuginfo | 7.0.13-1.el9_2 | |
dotnet-sdk-6.0 | 6.0.124-1.el9_2 | [ALSA-2023:6242](https://errata.almalinux.org/9/ALSA-2023-6242.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-36799](https://access.redhat.com/security/cve/CVE-2023-36799))
dotnet-sdk-6.0-debuginfo | 6.0.124-1.el9_2 | |
dotnet-sdk-7.0 | 7.0.113-1.el9_2 | [ALSA-2023:6246](https://errata.almalinux.org/9/ALSA-2023-6246.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-36799](https://access.redhat.com/security/cve/CVE-2023-36799))
dotnet-sdk-7.0-debuginfo | 7.0.113-1.el9_2 | |
dotnet-targeting-pack-6.0 | 6.0.24-1.el9_2 | [ALSA-2023:6242](https://errata.almalinux.org/9/ALSA-2023-6242.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-36799](https://access.redhat.com/security/cve/CVE-2023-36799))
dotnet-targeting-pack-7.0 | 7.0.13-1.el9_2 | [ALSA-2023:6246](https://errata.almalinux.org/9/ALSA-2023-6246.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-36799](https://access.redhat.com/security/cve/CVE-2023-36799))
dotnet-templates-6.0 | 6.0.124-1.el9_2 | [ALSA-2023:6242](https://errata.almalinux.org/9/ALSA-2023-6242.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-36799](https://access.redhat.com/security/cve/CVE-2023-36799))
dotnet-templates-7.0 | 7.0.113-1.el9_2 | [ALSA-2023:6246](https://errata.almalinux.org/9/ALSA-2023-6246.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-36799](https://access.redhat.com/security/cve/CVE-2023-36799))
dotnet6.0-debuginfo | 6.0.124-1.el9_2 | |
dotnet6.0-debugsource | 6.0.124-1.el9_2 | |
dotnet7.0-debuginfo | 7.0.113-1.el9_2 | |
dotnet7.0-debugsource | 7.0.113-1.el9_2 | |
firefox | 115.4.0-1.el9_2.alma.1 | [ALSA-2023:6188](https://errata.almalinux.org/9/ALSA-2023-6188.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-44488](https://access.redhat.com/security/cve/CVE-2023-44488), [CVE-2023-5721](https://access.redhat.com/security/cve/CVE-2023-5721), [CVE-2023-5724](https://access.redhat.com/security/cve/CVE-2023-5724), [CVE-2023-5725](https://access.redhat.com/security/cve/CVE-2023-5725), [CVE-2023-5728](https://access.redhat.com/security/cve/CVE-2023-5728), [CVE-2023-5730](https://access.redhat.com/security/cve/CVE-2023-5730), [CVE-2023-5732](https://access.redhat.com/security/cve/CVE-2023-5732))
firefox-debuginfo | 115.4.0-1.el9_2.alma.1 | |
firefox-debugsource | 115.4.0-1.el9_2.alma.1 | |
firefox-x11 | 115.4.0-1.el9_2.alma.1 | [ALSA-2023:6188](https://errata.almalinux.org/9/ALSA-2023-6188.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-44488](https://access.redhat.com/security/cve/CVE-2023-44488), [CVE-2023-5721](https://access.redhat.com/security/cve/CVE-2023-5721), [CVE-2023-5724](https://access.redhat.com/security/cve/CVE-2023-5724), [CVE-2023-5725](https://access.redhat.com/security/cve/CVE-2023-5725), [CVE-2023-5728](https://access.redhat.com/security/cve/CVE-2023-5728), [CVE-2023-5730](https://access.redhat.com/security/cve/CVE-2023-5730), [CVE-2023-5732](https://access.redhat.com/security/cve/CVE-2023-5732))
ghostscript | 9.54.0-11.el9_2 | [ALSA-2023:6265](https://errata.almalinux.org/9/ALSA-2023-6265.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-43115](https://access.redhat.com/security/cve/CVE-2023-43115))
ghostscript-debuginfo | 9.54.0-11.el9_2 | |
ghostscript-debugsource | 9.54.0-11.el9_2 | |
ghostscript-doc | 9.54.0-11.el9_2 | [ALSA-2023:6265](https://errata.almalinux.org/9/ALSA-2023-6265.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-43115](https://access.redhat.com/security/cve/CVE-2023-43115))
ghostscript-gtk-debuginfo | 9.54.0-11.el9_2 | |
ghostscript-tools-dvipdf | 9.54.0-11.el9_2 | [ALSA-2023:6265](https://errata.almalinux.org/9/ALSA-2023-6265.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-43115](https://access.redhat.com/security/cve/CVE-2023-43115))
ghostscript-tools-fonts | 9.54.0-11.el9_2 | [ALSA-2023:6265](https://errata.almalinux.org/9/ALSA-2023-6265.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-43115](https://access.redhat.com/security/cve/CVE-2023-43115))
ghostscript-tools-printing | 9.54.0-11.el9_2 | [ALSA-2023:6265](https://errata.almalinux.org/9/ALSA-2023-6265.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-43115](https://access.redhat.com/security/cve/CVE-2023-43115))
ghostscript-x11 | 9.54.0-11.el9_2 | [ALSA-2023:6265](https://errata.almalinux.org/9/ALSA-2023-6265.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-43115](https://access.redhat.com/security/cve/CVE-2023-43115))
ghostscript-x11-debuginfo | 9.54.0-11.el9_2 | |
libgs | 9.54.0-11.el9_2 | [ALSA-2023:6265](https://errata.almalinux.org/9/ALSA-2023-6265.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-43115](https://access.redhat.com/security/cve/CVE-2023-43115))
libgs-debuginfo | 9.54.0-11.el9_2 | |
netstandard-targeting-pack-2.1 | 7.0.113-1.el9_2 | [ALSA-2023:6246](https://errata.almalinux.org/9/ALSA-2023-6246.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-36799](https://access.redhat.com/security/cve/CVE-2023-36799))
squid | 5.5-5.el9_2.1 | [ALSA-2023:6266](https://errata.almalinux.org/9/ALSA-2023-6266.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-46846](https://access.redhat.com/security/cve/CVE-2023-46846), [CVE-2023-46847](https://access.redhat.com/security/cve/CVE-2023-46847), [CVE-2023-46848](https://access.redhat.com/security/cve/CVE-2023-46848))
squid-debuginfo | 5.5-5.el9_2.1 | |
squid-debugsource | 5.5-5.el9_2.1 | |
thunderbird | 115.4.1-1.el9_2.alma | [ALSA-2023:6191](https://errata.almalinux.org/9/ALSA-2023-6191.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-44488](https://access.redhat.com/security/cve/CVE-2023-44488), [CVE-2023-5721](https://access.redhat.com/security/cve/CVE-2023-5721), [CVE-2023-5724](https://access.redhat.com/security/cve/CVE-2023-5724), [CVE-2023-5725](https://access.redhat.com/security/cve/CVE-2023-5725), [CVE-2023-5728](https://access.redhat.com/security/cve/CVE-2023-5728), [CVE-2023-5730](https://access.redhat.com/security/cve/CVE-2023-5730), [CVE-2023-5732](https://access.redhat.com/security/cve/CVE-2023-5732))
thunderbird-debuginfo | 115.4.1-1.el9_2.alma | |
thunderbird-debugsource | 115.4.1-1.el9_2.alma | |

### CRB x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
dotnet-sdk-6.0-source-built-artifacts | 6.0.124-1.el9_2 | [ALSA-2023:6242](https://errata.almalinux.org/9/ALSA-2023-6242.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-36799](https://access.redhat.com/security/cve/CVE-2023-36799))
dotnet-sdk-7.0-source-built-artifacts | 7.0.113-1.el9_2 | [ALSA-2023:6246](https://errata.almalinux.org/9/ALSA-2023-6246.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-36799](https://access.redhat.com/security/cve/CVE-2023-36799))
libgs-devel | 9.54.0-11.el9_2 | [ALSA-2023:6265](https://errata.almalinux.org/9/ALSA-2023-6265.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-43115](https://access.redhat.com/security/cve/CVE-2023-43115))

### devel x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
ghostscript-gtk | 9.54.0-11.el9_2 | |

### plus x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
thunderbird | 115.4.1-1.el9_2.alma.plus | [ALSA-2023:6191](https://errata.almalinux.org/9/ALSA-2023-6191.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-44488](https://access.redhat.com/security/cve/CVE-2023-44488), [CVE-2023-5721](https://access.redhat.com/security/cve/CVE-2023-5721), [CVE-2023-5724](https://access.redhat.com/security/cve/CVE-2023-5724), [CVE-2023-5725](https://access.redhat.com/security/cve/CVE-2023-5725), [CVE-2023-5728](https://access.redhat.com/security/cve/CVE-2023-5728), [CVE-2023-5730](https://access.redhat.com/security/cve/CVE-2023-5730), [CVE-2023-5732](https://access.redhat.com/security/cve/CVE-2023-5732))
thunderbird-debuginfo | 115.4.1-1.el9_2.alma.plus | |
thunderbird-debugsource | 115.4.1-1.el9_2.alma.plus | |
thunderbird-librnp-rnp | 115.4.1-1.el9_2.alma.plus | |
thunderbird-librnp-rnp-debuginfo | 115.4.1-1.el9_2.alma.plus | |

### AppStream aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
aspnetcore-runtime-6.0 | 6.0.24-1.el9_2 | [ALSA-2023:6242](https://errata.almalinux.org/9/ALSA-2023-6242.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-36799](https://access.redhat.com/security/cve/CVE-2023-36799))
aspnetcore-runtime-7.0 | 7.0.13-1.el9_2 | [ALSA-2023:6246](https://errata.almalinux.org/9/ALSA-2023-6246.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-36799](https://access.redhat.com/security/cve/CVE-2023-36799))
aspnetcore-targeting-pack-6.0 | 6.0.24-1.el9_2 | [ALSA-2023:6242](https://errata.almalinux.org/9/ALSA-2023-6242.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-36799](https://access.redhat.com/security/cve/CVE-2023-36799))
aspnetcore-targeting-pack-7.0 | 7.0.13-1.el9_2 | [ALSA-2023:6246](https://errata.almalinux.org/9/ALSA-2023-6246.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-36799](https://access.redhat.com/security/cve/CVE-2023-36799))
dotnet-apphost-pack-6.0 | 6.0.24-1.el9_2 | [ALSA-2023:6242](https://errata.almalinux.org/9/ALSA-2023-6242.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-36799](https://access.redhat.com/security/cve/CVE-2023-36799))
dotnet-apphost-pack-6.0-debuginfo | 6.0.24-1.el9_2 | |
dotnet-apphost-pack-7.0 | 7.0.13-1.el9_2 | [ALSA-2023:6246](https://errata.almalinux.org/9/ALSA-2023-6246.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-36799](https://access.redhat.com/security/cve/CVE-2023-36799))
dotnet-apphost-pack-7.0-debuginfo | 7.0.13-1.el9_2 | |
dotnet-host | 7.0.13-1.el9_2 | [ALSA-2023:6246](https://errata.almalinux.org/9/ALSA-2023-6246.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-36799](https://access.redhat.com/security/cve/CVE-2023-36799))
dotnet-host-debuginfo | 7.0.13-1.el9_2 | |
dotnet-hostfxr-6.0 | 6.0.24-1.el9_2 | [ALSA-2023:6242](https://errata.almalinux.org/9/ALSA-2023-6242.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-36799](https://access.redhat.com/security/cve/CVE-2023-36799))
dotnet-hostfxr-6.0-debuginfo | 6.0.24-1.el9_2 | |
dotnet-hostfxr-7.0 | 7.0.13-1.el9_2 | [ALSA-2023:6246](https://errata.almalinux.org/9/ALSA-2023-6246.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-36799](https://access.redhat.com/security/cve/CVE-2023-36799))
dotnet-hostfxr-7.0-debuginfo | 7.0.13-1.el9_2 | |
dotnet-runtime-6.0 | 6.0.24-1.el9_2 | [ALSA-2023:6242](https://errata.almalinux.org/9/ALSA-2023-6242.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-36799](https://access.redhat.com/security/cve/CVE-2023-36799))
dotnet-runtime-6.0-debuginfo | 6.0.24-1.el9_2 | |
dotnet-runtime-7.0 | 7.0.13-1.el9_2 | [ALSA-2023:6246](https://errata.almalinux.org/9/ALSA-2023-6246.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-36799](https://access.redhat.com/security/cve/CVE-2023-36799))
dotnet-runtime-7.0-debuginfo | 7.0.13-1.el9_2 | |
dotnet-sdk-6.0 | 6.0.124-1.el9_2 | [ALSA-2023:6242](https://errata.almalinux.org/9/ALSA-2023-6242.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-36799](https://access.redhat.com/security/cve/CVE-2023-36799))
dotnet-sdk-6.0-debuginfo | 6.0.124-1.el9_2 | |
dotnet-sdk-7.0 | 7.0.113-1.el9_2 | [ALSA-2023:6246](https://errata.almalinux.org/9/ALSA-2023-6246.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-36799](https://access.redhat.com/security/cve/CVE-2023-36799))
dotnet-sdk-7.0-debuginfo | 7.0.113-1.el9_2 | |
dotnet-targeting-pack-6.0 | 6.0.24-1.el9_2 | [ALSA-2023:6242](https://errata.almalinux.org/9/ALSA-2023-6242.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-36799](https://access.redhat.com/security/cve/CVE-2023-36799))
dotnet-targeting-pack-7.0 | 7.0.13-1.el9_2 | [ALSA-2023:6246](https://errata.almalinux.org/9/ALSA-2023-6246.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-36799](https://access.redhat.com/security/cve/CVE-2023-36799))
dotnet-templates-6.0 | 6.0.124-1.el9_2 | [ALSA-2023:6242](https://errata.almalinux.org/9/ALSA-2023-6242.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-36799](https://access.redhat.com/security/cve/CVE-2023-36799))
dotnet-templates-7.0 | 7.0.113-1.el9_2 | [ALSA-2023:6246](https://errata.almalinux.org/9/ALSA-2023-6246.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-36799](https://access.redhat.com/security/cve/CVE-2023-36799))
dotnet6.0-debuginfo | 6.0.124-1.el9_2 | |
dotnet6.0-debugsource | 6.0.124-1.el9_2 | |
dotnet7.0-debuginfo | 7.0.113-1.el9_2 | |
dotnet7.0-debugsource | 7.0.113-1.el9_2 | |
firefox | 115.4.0-1.el9_2.alma.1 | [ALSA-2023:6188](https://errata.almalinux.org/9/ALSA-2023-6188.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-44488](https://access.redhat.com/security/cve/CVE-2023-44488), [CVE-2023-5721](https://access.redhat.com/security/cve/CVE-2023-5721), [CVE-2023-5724](https://access.redhat.com/security/cve/CVE-2023-5724), [CVE-2023-5725](https://access.redhat.com/security/cve/CVE-2023-5725), [CVE-2023-5728](https://access.redhat.com/security/cve/CVE-2023-5728), [CVE-2023-5730](https://access.redhat.com/security/cve/CVE-2023-5730), [CVE-2023-5732](https://access.redhat.com/security/cve/CVE-2023-5732))
firefox-debuginfo | 115.4.0-1.el9_2.alma.1 | |
firefox-debugsource | 115.4.0-1.el9_2.alma.1 | |
firefox-x11 | 115.4.0-1.el9_2.alma.1 | [ALSA-2023:6188](https://errata.almalinux.org/9/ALSA-2023-6188.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-44488](https://access.redhat.com/security/cve/CVE-2023-44488), [CVE-2023-5721](https://access.redhat.com/security/cve/CVE-2023-5721), [CVE-2023-5724](https://access.redhat.com/security/cve/CVE-2023-5724), [CVE-2023-5725](https://access.redhat.com/security/cve/CVE-2023-5725), [CVE-2023-5728](https://access.redhat.com/security/cve/CVE-2023-5728), [CVE-2023-5730](https://access.redhat.com/security/cve/CVE-2023-5730), [CVE-2023-5732](https://access.redhat.com/security/cve/CVE-2023-5732))
ghostscript | 9.54.0-11.el9_2 | [ALSA-2023:6265](https://errata.almalinux.org/9/ALSA-2023-6265.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-43115](https://access.redhat.com/security/cve/CVE-2023-43115))
ghostscript-debuginfo | 9.54.0-11.el9_2 | |
ghostscript-debugsource | 9.54.0-11.el9_2 | |
ghostscript-doc | 9.54.0-11.el9_2 | [ALSA-2023:6265](https://errata.almalinux.org/9/ALSA-2023-6265.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-43115](https://access.redhat.com/security/cve/CVE-2023-43115))
ghostscript-gtk-debuginfo | 9.54.0-11.el9_2 | |
ghostscript-tools-dvipdf | 9.54.0-11.el9_2 | [ALSA-2023:6265](https://errata.almalinux.org/9/ALSA-2023-6265.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-43115](https://access.redhat.com/security/cve/CVE-2023-43115))
ghostscript-tools-fonts | 9.54.0-11.el9_2 | [ALSA-2023:6265](https://errata.almalinux.org/9/ALSA-2023-6265.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-43115](https://access.redhat.com/security/cve/CVE-2023-43115))
ghostscript-tools-printing | 9.54.0-11.el9_2 | [ALSA-2023:6265](https://errata.almalinux.org/9/ALSA-2023-6265.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-43115](https://access.redhat.com/security/cve/CVE-2023-43115))
ghostscript-x11 | 9.54.0-11.el9_2 | [ALSA-2023:6265](https://errata.almalinux.org/9/ALSA-2023-6265.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-43115](https://access.redhat.com/security/cve/CVE-2023-43115))
ghostscript-x11-debuginfo | 9.54.0-11.el9_2 | |
libgs | 9.54.0-11.el9_2 | [ALSA-2023:6265](https://errata.almalinux.org/9/ALSA-2023-6265.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-43115](https://access.redhat.com/security/cve/CVE-2023-43115))
libgs-debuginfo | 9.54.0-11.el9_2 | |
netstandard-targeting-pack-2.1 | 7.0.113-1.el9_2 | [ALSA-2023:6246](https://errata.almalinux.org/9/ALSA-2023-6246.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-36799](https://access.redhat.com/security/cve/CVE-2023-36799))
squid | 5.5-5.el9_2.1 | [ALSA-2023:6266](https://errata.almalinux.org/9/ALSA-2023-6266.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-46846](https://access.redhat.com/security/cve/CVE-2023-46846), [CVE-2023-46847](https://access.redhat.com/security/cve/CVE-2023-46847), [CVE-2023-46848](https://access.redhat.com/security/cve/CVE-2023-46848))
squid-debuginfo | 5.5-5.el9_2.1 | |
squid-debugsource | 5.5-5.el9_2.1 | |
thunderbird | 115.4.1-1.el9_2.alma | [ALSA-2023:6191](https://errata.almalinux.org/9/ALSA-2023-6191.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-44488](https://access.redhat.com/security/cve/CVE-2023-44488), [CVE-2023-5721](https://access.redhat.com/security/cve/CVE-2023-5721), [CVE-2023-5724](https://access.redhat.com/security/cve/CVE-2023-5724), [CVE-2023-5725](https://access.redhat.com/security/cve/CVE-2023-5725), [CVE-2023-5728](https://access.redhat.com/security/cve/CVE-2023-5728), [CVE-2023-5730](https://access.redhat.com/security/cve/CVE-2023-5730), [CVE-2023-5732](https://access.redhat.com/security/cve/CVE-2023-5732))
thunderbird-debuginfo | 115.4.1-1.el9_2.alma | |
thunderbird-debugsource | 115.4.1-1.el9_2.alma | |

### CRB aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
dotnet-sdk-6.0-source-built-artifacts | 6.0.124-1.el9_2 | [ALSA-2023:6242](https://errata.almalinux.org/9/ALSA-2023-6242.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-36799](https://access.redhat.com/security/cve/CVE-2023-36799))
dotnet-sdk-7.0-source-built-artifacts | 7.0.113-1.el9_2 | [ALSA-2023:6246](https://errata.almalinux.org/9/ALSA-2023-6246.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-36799](https://access.redhat.com/security/cve/CVE-2023-36799))
libgs-devel | 9.54.0-11.el9_2 | [ALSA-2023:6265](https://errata.almalinux.org/9/ALSA-2023-6265.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-43115](https://access.redhat.com/security/cve/CVE-2023-43115))

### devel aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
ghostscript-gtk | 9.54.0-11.el9_2 | |

### plus aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
thunderbird | 115.4.1-1.el9_2.alma.plus | [ALSA-2023:6191](https://errata.almalinux.org/9/ALSA-2023-6191.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-44488](https://access.redhat.com/security/cve/CVE-2023-44488), [CVE-2023-5721](https://access.redhat.com/security/cve/CVE-2023-5721), [CVE-2023-5724](https://access.redhat.com/security/cve/CVE-2023-5724), [CVE-2023-5725](https://access.redhat.com/security/cve/CVE-2023-5725), [CVE-2023-5728](https://access.redhat.com/security/cve/CVE-2023-5728), [CVE-2023-5730](https://access.redhat.com/security/cve/CVE-2023-5730), [CVE-2023-5732](https://access.redhat.com/security/cve/CVE-2023-5732))
thunderbird-debuginfo | 115.4.1-1.el9_2.alma.plus | |
thunderbird-debugsource | 115.4.1-1.el9_2.alma.plus | |
thunderbird-librnp-rnp | 115.4.1-1.el9_2.alma.plus | |
thunderbird-librnp-rnp-debuginfo | 115.4.1-1.el9_2.alma.plus | |

