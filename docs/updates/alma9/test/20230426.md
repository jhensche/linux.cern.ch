## 2023-04-26

### BaseOS x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
gcab-debugsource | 1.4-6.el9 | |
python-ethtool-debugsource | 0.15-2.el9 | |
python-systemd-debugsource | 234-18.el9 | |
python-urwid-debugsource | 2.1.2-4.el9 | |
star-debugsource | 1.6-6.el9 | |
subscription-manager-rhsm-certificates | 20220623-1.el9 | [RHBA-2022:8356](https://access.redhat.com/errata/RHBA-2022:8356) | <div class="adv_b">Bug Fix Advisory</div>
tree-pkg-debugsource | 1.8.0-10.el9 | |

### AppStream x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
alsa-tools-debugsource | 1.2.2-6.el9 | |
bacula-debugsource | 11.0.1-5.el9 | |
ceph-debugsource | 16.2.4-5.el9 | |
clucene-debugsource | 2.3.3.4-42.20130812.e8e3d20git.el9 | |
gtk-vnc-debugsource | 1.3.0-1.el9 | |
java-1.8.0-openjdk | 1.8.0.372.b07-1.el9_1 | [RHSA-2023:1909](https://access.redhat.com/errata/RHSA-2023:1909) | <div class="adv_s">Security Advisory</div> ([CVE-2023-21930](https://access.redhat.com/security/cve/CVE-2023-21930), [CVE-2023-21937](https://access.redhat.com/security/cve/CVE-2023-21937), [CVE-2023-21938](https://access.redhat.com/security/cve/CVE-2023-21938), [CVE-2023-21939](https://access.redhat.com/security/cve/CVE-2023-21939), [CVE-2023-21954](https://access.redhat.com/security/cve/CVE-2023-21954), [CVE-2023-21967](https://access.redhat.com/security/cve/CVE-2023-21967), [CVE-2023-21968](https://access.redhat.com/security/cve/CVE-2023-21968))
java-1.8.0-openjdk-debuginfo | 1.8.0.372.b07-1.el9_1 | |
java-1.8.0-openjdk-debugsource | 1.8.0.372.b07-1.el9_1 | |
java-1.8.0-openjdk-demo | 1.8.0.372.b07-1.el9_1 | [RHSA-2023:1909](https://access.redhat.com/errata/RHSA-2023:1909) | <div class="adv_s">Security Advisory</div> ([CVE-2023-21930](https://access.redhat.com/security/cve/CVE-2023-21930), [CVE-2023-21937](https://access.redhat.com/security/cve/CVE-2023-21937), [CVE-2023-21938](https://access.redhat.com/security/cve/CVE-2023-21938), [CVE-2023-21939](https://access.redhat.com/security/cve/CVE-2023-21939), [CVE-2023-21954](https://access.redhat.com/security/cve/CVE-2023-21954), [CVE-2023-21967](https://access.redhat.com/security/cve/CVE-2023-21967), [CVE-2023-21968](https://access.redhat.com/security/cve/CVE-2023-21968))
java-1.8.0-openjdk-demo-debuginfo | 1.8.0.372.b07-1.el9_1 | |
java-1.8.0-openjdk-devel | 1.8.0.372.b07-1.el9_1 | [RHSA-2023:1909](https://access.redhat.com/errata/RHSA-2023:1909) | <div class="adv_s">Security Advisory</div> ([CVE-2023-21930](https://access.redhat.com/security/cve/CVE-2023-21930), [CVE-2023-21937](https://access.redhat.com/security/cve/CVE-2023-21937), [CVE-2023-21938](https://access.redhat.com/security/cve/CVE-2023-21938), [CVE-2023-21939](https://access.redhat.com/security/cve/CVE-2023-21939), [CVE-2023-21954](https://access.redhat.com/security/cve/CVE-2023-21954), [CVE-2023-21967](https://access.redhat.com/security/cve/CVE-2023-21967), [CVE-2023-21968](https://access.redhat.com/security/cve/CVE-2023-21968))
java-1.8.0-openjdk-devel-debuginfo | 1.8.0.372.b07-1.el9_1 | |
java-1.8.0-openjdk-headless | 1.8.0.372.b07-1.el9_1 | [RHSA-2023:1909](https://access.redhat.com/errata/RHSA-2023:1909) | <div class="adv_s">Security Advisory</div> ([CVE-2023-21930](https://access.redhat.com/security/cve/CVE-2023-21930), [CVE-2023-21937](https://access.redhat.com/security/cve/CVE-2023-21937), [CVE-2023-21938](https://access.redhat.com/security/cve/CVE-2023-21938), [CVE-2023-21939](https://access.redhat.com/security/cve/CVE-2023-21939), [CVE-2023-21954](https://access.redhat.com/security/cve/CVE-2023-21954), [CVE-2023-21967](https://access.redhat.com/security/cve/CVE-2023-21967), [CVE-2023-21968](https://access.redhat.com/security/cve/CVE-2023-21968))
java-1.8.0-openjdk-headless-debuginfo | 1.8.0.372.b07-1.el9_1 | |
java-1.8.0-openjdk-javadoc | 1.8.0.372.b07-1.el9_1 | [RHSA-2023:1909](https://access.redhat.com/errata/RHSA-2023:1909) | <div class="adv_s">Security Advisory</div> ([CVE-2023-21930](https://access.redhat.com/security/cve/CVE-2023-21930), [CVE-2023-21937](https://access.redhat.com/security/cve/CVE-2023-21937), [CVE-2023-21938](https://access.redhat.com/security/cve/CVE-2023-21938), [CVE-2023-21939](https://access.redhat.com/security/cve/CVE-2023-21939), [CVE-2023-21954](https://access.redhat.com/security/cve/CVE-2023-21954), [CVE-2023-21967](https://access.redhat.com/security/cve/CVE-2023-21967), [CVE-2023-21968](https://access.redhat.com/security/cve/CVE-2023-21968))
java-1.8.0-openjdk-javadoc-zip | 1.8.0.372.b07-1.el9_1 | [RHSA-2023:1909](https://access.redhat.com/errata/RHSA-2023:1909) | <div class="adv_s">Security Advisory</div> ([CVE-2023-21930](https://access.redhat.com/security/cve/CVE-2023-21930), [CVE-2023-21937](https://access.redhat.com/security/cve/CVE-2023-21937), [CVE-2023-21938](https://access.redhat.com/security/cve/CVE-2023-21938), [CVE-2023-21939](https://access.redhat.com/security/cve/CVE-2023-21939), [CVE-2023-21954](https://access.redhat.com/security/cve/CVE-2023-21954), [CVE-2023-21967](https://access.redhat.com/security/cve/CVE-2023-21967), [CVE-2023-21968](https://access.redhat.com/security/cve/CVE-2023-21968))
java-1.8.0-openjdk-src | 1.8.0.372.b07-1.el9_1 | [RHSA-2023:1909](https://access.redhat.com/errata/RHSA-2023:1909) | <div class="adv_s">Security Advisory</div> ([CVE-2023-21930](https://access.redhat.com/security/cve/CVE-2023-21930), [CVE-2023-21937](https://access.redhat.com/security/cve/CVE-2023-21937), [CVE-2023-21938](https://access.redhat.com/security/cve/CVE-2023-21938), [CVE-2023-21939](https://access.redhat.com/security/cve/CVE-2023-21939), [CVE-2023-21954](https://access.redhat.com/security/cve/CVE-2023-21954), [CVE-2023-21967](https://access.redhat.com/security/cve/CVE-2023-21967), [CVE-2023-21968](https://access.redhat.com/security/cve/CVE-2023-21968))
libpst-debugsource | 0.6.75-12.el9 | |
mpitests-debugsource | 5.8-1.el9 | |
numpy-debugsource | 1.20.1-5.el9 | |
pyodbc-debugsource | 4.0.30-4.el9 | |
python-cups-debugsource | 2.0.1-10.el9 | |
python-gssapi-debugsource | 1.6.9-5.el9 | |
python-markupsafe-debugsource | 1.1.1-12.el9 | |
python-netifaces-debugsource | 0.10.6-15.el9 | |
python-psutil-debugsource | 5.8.0-12.el9 | |
python-psycopg2-debugsource | 2.8.6-6.el9 | |
python-pycurl-debugsource | 7.43.0.6-8.el9 | |
python3-libfdt-debuginfo | 1.6.0-7.el9 | |
rpcsvc-proto-debugsource | 1.4-9.el9 | |
rust-zram-generator-debugsource | 0.3.2-7.el9 | |
scipy-debugsource | 1.6.2-8.el9 | |
speech-tools-debugsource | 2.5-18.el9 | |
v4l-utils-debugsource | 1.20.0-5.el9 | |

### HighAvailability x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----

### ResilientStorage x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----

### CRB x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
ceph-debugsource | 16.2.4-5.el9 | |
clucene-debugsource | 2.3.3.4-42.20130812.e8e3d20git.el9 | |
Cython-debugsource | 0.29.22-7.el9 | |
gcab-debugsource | 1.4-6.el9 | |
java-1.8.0-openjdk-debuginfo | 1.8.0.372.b07-1.el9_1 | |
java-1.8.0-openjdk-debugsource | 1.8.0.372.b07-1.el9_1 | |
java-1.8.0-openjdk-demo-debuginfo | 1.8.0.372.b07-1.el9_1 | |
java-1.8.0-openjdk-demo-fastdebug | 1.8.0.372.b07-1.el9_1 | [RHSA-2023:1909](https://access.redhat.com/errata/RHSA-2023:1909) | <div class="adv_s">Security Advisory</div> ([CVE-2023-21930](https://access.redhat.com/security/cve/CVE-2023-21930), [CVE-2023-21937](https://access.redhat.com/security/cve/CVE-2023-21937), [CVE-2023-21938](https://access.redhat.com/security/cve/CVE-2023-21938), [CVE-2023-21939](https://access.redhat.com/security/cve/CVE-2023-21939), [CVE-2023-21954](https://access.redhat.com/security/cve/CVE-2023-21954), [CVE-2023-21967](https://access.redhat.com/security/cve/CVE-2023-21967), [CVE-2023-21968](https://access.redhat.com/security/cve/CVE-2023-21968))
java-1.8.0-openjdk-demo-fastdebug-debuginfo | 1.8.0.372.b07-1.el9_1 | |
java-1.8.0-openjdk-demo-slowdebug | 1.8.0.372.b07-1.el9_1 | [RHSA-2023:1909](https://access.redhat.com/errata/RHSA-2023:1909) | <div class="adv_s">Security Advisory</div> ([CVE-2023-21930](https://access.redhat.com/security/cve/CVE-2023-21930), [CVE-2023-21937](https://access.redhat.com/security/cve/CVE-2023-21937), [CVE-2023-21938](https://access.redhat.com/security/cve/CVE-2023-21938), [CVE-2023-21939](https://access.redhat.com/security/cve/CVE-2023-21939), [CVE-2023-21954](https://access.redhat.com/security/cve/CVE-2023-21954), [CVE-2023-21967](https://access.redhat.com/security/cve/CVE-2023-21967), [CVE-2023-21968](https://access.redhat.com/security/cve/CVE-2023-21968))
java-1.8.0-openjdk-demo-slowdebug-debuginfo | 1.8.0.372.b07-1.el9_1 | |
java-1.8.0-openjdk-devel-debuginfo | 1.8.0.372.b07-1.el9_1 | |
java-1.8.0-openjdk-devel-fastdebug | 1.8.0.372.b07-1.el9_1 | [RHSA-2023:1909](https://access.redhat.com/errata/RHSA-2023:1909) | <div class="adv_s">Security Advisory</div> ([CVE-2023-21930](https://access.redhat.com/security/cve/CVE-2023-21930), [CVE-2023-21937](https://access.redhat.com/security/cve/CVE-2023-21937), [CVE-2023-21938](https://access.redhat.com/security/cve/CVE-2023-21938), [CVE-2023-21939](https://access.redhat.com/security/cve/CVE-2023-21939), [CVE-2023-21954](https://access.redhat.com/security/cve/CVE-2023-21954), [CVE-2023-21967](https://access.redhat.com/security/cve/CVE-2023-21967), [CVE-2023-21968](https://access.redhat.com/security/cve/CVE-2023-21968))
java-1.8.0-openjdk-devel-fastdebug-debuginfo | 1.8.0.372.b07-1.el9_1 | |
java-1.8.0-openjdk-devel-slowdebug | 1.8.0.372.b07-1.el9_1 | [RHSA-2023:1909](https://access.redhat.com/errata/RHSA-2023:1909) | <div class="adv_s">Security Advisory</div> ([CVE-2023-21930](https://access.redhat.com/security/cve/CVE-2023-21930), [CVE-2023-21937](https://access.redhat.com/security/cve/CVE-2023-21937), [CVE-2023-21938](https://access.redhat.com/security/cve/CVE-2023-21938), [CVE-2023-21939](https://access.redhat.com/security/cve/CVE-2023-21939), [CVE-2023-21954](https://access.redhat.com/security/cve/CVE-2023-21954), [CVE-2023-21967](https://access.redhat.com/security/cve/CVE-2023-21967), [CVE-2023-21968](https://access.redhat.com/security/cve/CVE-2023-21968))
java-1.8.0-openjdk-devel-slowdebug-debuginfo | 1.8.0.372.b07-1.el9_1 | |
java-1.8.0-openjdk-fastdebug | 1.8.0.372.b07-1.el9_1 | [RHSA-2023:1909](https://access.redhat.com/errata/RHSA-2023:1909) | <div class="adv_s">Security Advisory</div> ([CVE-2023-21930](https://access.redhat.com/security/cve/CVE-2023-21930), [CVE-2023-21937](https://access.redhat.com/security/cve/CVE-2023-21937), [CVE-2023-21938](https://access.redhat.com/security/cve/CVE-2023-21938), [CVE-2023-21939](https://access.redhat.com/security/cve/CVE-2023-21939), [CVE-2023-21954](https://access.redhat.com/security/cve/CVE-2023-21954), [CVE-2023-21967](https://access.redhat.com/security/cve/CVE-2023-21967), [CVE-2023-21968](https://access.redhat.com/security/cve/CVE-2023-21968))
java-1.8.0-openjdk-fastdebug-debuginfo | 1.8.0.372.b07-1.el9_1 | |
java-1.8.0-openjdk-headless-debuginfo | 1.8.0.372.b07-1.el9_1 | |
java-1.8.0-openjdk-headless-fastdebug | 1.8.0.372.b07-1.el9_1 | [RHSA-2023:1909](https://access.redhat.com/errata/RHSA-2023:1909) | <div class="adv_s">Security Advisory</div> ([CVE-2023-21930](https://access.redhat.com/security/cve/CVE-2023-21930), [CVE-2023-21937](https://access.redhat.com/security/cve/CVE-2023-21937), [CVE-2023-21938](https://access.redhat.com/security/cve/CVE-2023-21938), [CVE-2023-21939](https://access.redhat.com/security/cve/CVE-2023-21939), [CVE-2023-21954](https://access.redhat.com/security/cve/CVE-2023-21954), [CVE-2023-21967](https://access.redhat.com/security/cve/CVE-2023-21967), [CVE-2023-21968](https://access.redhat.com/security/cve/CVE-2023-21968))
java-1.8.0-openjdk-headless-fastdebug-debuginfo | 1.8.0.372.b07-1.el9_1 | |
java-1.8.0-openjdk-headless-slowdebug | 1.8.0.372.b07-1.el9_1 | [RHSA-2023:1909](https://access.redhat.com/errata/RHSA-2023:1909) | <div class="adv_s">Security Advisory</div> ([CVE-2023-21930](https://access.redhat.com/security/cve/CVE-2023-21930), [CVE-2023-21937](https://access.redhat.com/security/cve/CVE-2023-21937), [CVE-2023-21938](https://access.redhat.com/security/cve/CVE-2023-21938), [CVE-2023-21939](https://access.redhat.com/security/cve/CVE-2023-21939), [CVE-2023-21954](https://access.redhat.com/security/cve/CVE-2023-21954), [CVE-2023-21967](https://access.redhat.com/security/cve/CVE-2023-21967), [CVE-2023-21968](https://access.redhat.com/security/cve/CVE-2023-21968))
java-1.8.0-openjdk-headless-slowdebug-debuginfo | 1.8.0.372.b07-1.el9_1 | |
java-1.8.0-openjdk-slowdebug | 1.8.0.372.b07-1.el9_1 | [RHSA-2023:1909](https://access.redhat.com/errata/RHSA-2023:1909) | <div class="adv_s">Security Advisory</div> ([CVE-2023-21930](https://access.redhat.com/security/cve/CVE-2023-21930), [CVE-2023-21937](https://access.redhat.com/security/cve/CVE-2023-21937), [CVE-2023-21938](https://access.redhat.com/security/cve/CVE-2023-21938), [CVE-2023-21939](https://access.redhat.com/security/cve/CVE-2023-21939), [CVE-2023-21954](https://access.redhat.com/security/cve/CVE-2023-21954), [CVE-2023-21967](https://access.redhat.com/security/cve/CVE-2023-21967), [CVE-2023-21968](https://access.redhat.com/security/cve/CVE-2023-21968))
java-1.8.0-openjdk-slowdebug-debuginfo | 1.8.0.372.b07-1.el9_1 | |
java-1.8.0-openjdk-src-fastdebug | 1.8.0.372.b07-1.el9_1 | [RHSA-2023:1909](https://access.redhat.com/errata/RHSA-2023:1909) | <div class="adv_s">Security Advisory</div> ([CVE-2023-21930](https://access.redhat.com/security/cve/CVE-2023-21930), [CVE-2023-21937](https://access.redhat.com/security/cve/CVE-2023-21937), [CVE-2023-21938](https://access.redhat.com/security/cve/CVE-2023-21938), [CVE-2023-21939](https://access.redhat.com/security/cve/CVE-2023-21939), [CVE-2023-21954](https://access.redhat.com/security/cve/CVE-2023-21954), [CVE-2023-21967](https://access.redhat.com/security/cve/CVE-2023-21967), [CVE-2023-21968](https://access.redhat.com/security/cve/CVE-2023-21968))
java-1.8.0-openjdk-src-slowdebug | 1.8.0.372.b07-1.el9_1 | [RHSA-2023:1909](https://access.redhat.com/errata/RHSA-2023:1909) | <div class="adv_s">Security Advisory</div> ([CVE-2023-21930](https://access.redhat.com/security/cve/CVE-2023-21930), [CVE-2023-21937](https://access.redhat.com/security/cve/CVE-2023-21937), [CVE-2023-21938](https://access.redhat.com/security/cve/CVE-2023-21938), [CVE-2023-21939](https://access.redhat.com/security/cve/CVE-2023-21939), [CVE-2023-21954](https://access.redhat.com/security/cve/CVE-2023-21954), [CVE-2023-21967](https://access.redhat.com/security/cve/CVE-2023-21967), [CVE-2023-21968](https://access.redhat.com/security/cve/CVE-2023-21968))
perl-gettext-debugsource | 1.07-21.el9 | |
python-psutil-debugsource | 5.8.0-12.el9 | |
python-ruamel-yaml-clib-debugsource | 0.1.2-8.el9.1 | |
python3-libfdt-debuginfo | 1.6.0-7.el9 | |
pyxattr-debugsource | 0.7.2-4.el9 | |
qhull-debugsource | 7.2.1-9.el9 | |
rpcsvc-proto-debugsource | 1.4-9.el9 | |
v4l-utils-debugsource | 1.20.0-5.el9 | |

### devel x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
qhull | 7.2.1-9.el9 | |
speech-tools | 2.5-18.el9 | |
star | 1.6-6.el9 | |
v4l-utils | 1.20.0-5.el9 | |

### BaseOS aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
gcab-debugsource | 1.4-6.el9 | |
python-ethtool-debugsource | 0.15-2.el9 | |
python-systemd-debugsource | 234-18.el9 | |
python-urwid-debugsource | 2.1.2-4.el9 | |
star-debugsource | 1.6-6.el9 | |
subscription-manager-rhsm-certificates | 20220623-1.el9 | [RHBA-2022:8356](https://access.redhat.com/errata/RHBA-2022:8356) | <div class="adv_b">Bug Fix Advisory</div>
tree-pkg-debugsource | 1.8.0-10.el9 | |

### AppStream aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
alsa-tools-debugsource | 1.2.2-6.el9 | |
bacula-debugsource | 11.0.1-5.el9 | |
ceph-debugsource | 16.2.4-5.el9 | |
cheese-debugsource | 3.38.0-6.el9 | |
clucene-debugsource | 2.3.3.4-42.20130812.e8e3d20git.el9 | |
gtk-vnc-debugsource | 1.3.0-1.el9 | |
java-1.8.0-openjdk | 1.8.0.372.b07-1.el9_1 | [RHSA-2023:1909](https://access.redhat.com/errata/RHSA-2023:1909) | <div class="adv_s">Security Advisory</div> ([CVE-2023-21930](https://access.redhat.com/security/cve/CVE-2023-21930), [CVE-2023-21937](https://access.redhat.com/security/cve/CVE-2023-21937), [CVE-2023-21938](https://access.redhat.com/security/cve/CVE-2023-21938), [CVE-2023-21939](https://access.redhat.com/security/cve/CVE-2023-21939), [CVE-2023-21954](https://access.redhat.com/security/cve/CVE-2023-21954), [CVE-2023-21967](https://access.redhat.com/security/cve/CVE-2023-21967), [CVE-2023-21968](https://access.redhat.com/security/cve/CVE-2023-21968))
java-1.8.0-openjdk-debuginfo | 1.8.0.372.b07-1.el9_1 | |
java-1.8.0-openjdk-debugsource | 1.8.0.372.b07-1.el9_1 | |
java-1.8.0-openjdk-demo | 1.8.0.372.b07-1.el9_1 | [RHSA-2023:1909](https://access.redhat.com/errata/RHSA-2023:1909) | <div class="adv_s">Security Advisory</div> ([CVE-2023-21930](https://access.redhat.com/security/cve/CVE-2023-21930), [CVE-2023-21937](https://access.redhat.com/security/cve/CVE-2023-21937), [CVE-2023-21938](https://access.redhat.com/security/cve/CVE-2023-21938), [CVE-2023-21939](https://access.redhat.com/security/cve/CVE-2023-21939), [CVE-2023-21954](https://access.redhat.com/security/cve/CVE-2023-21954), [CVE-2023-21967](https://access.redhat.com/security/cve/CVE-2023-21967), [CVE-2023-21968](https://access.redhat.com/security/cve/CVE-2023-21968))
java-1.8.0-openjdk-demo-debuginfo | 1.8.0.372.b07-1.el9_1 | |
java-1.8.0-openjdk-devel | 1.8.0.372.b07-1.el9_1 | [RHSA-2023:1909](https://access.redhat.com/errata/RHSA-2023:1909) | <div class="adv_s">Security Advisory</div> ([CVE-2023-21930](https://access.redhat.com/security/cve/CVE-2023-21930), [CVE-2023-21937](https://access.redhat.com/security/cve/CVE-2023-21937), [CVE-2023-21938](https://access.redhat.com/security/cve/CVE-2023-21938), [CVE-2023-21939](https://access.redhat.com/security/cve/CVE-2023-21939), [CVE-2023-21954](https://access.redhat.com/security/cve/CVE-2023-21954), [CVE-2023-21967](https://access.redhat.com/security/cve/CVE-2023-21967), [CVE-2023-21968](https://access.redhat.com/security/cve/CVE-2023-21968))
java-1.8.0-openjdk-devel-debuginfo | 1.8.0.372.b07-1.el9_1 | |
java-1.8.0-openjdk-headless | 1.8.0.372.b07-1.el9_1 | [RHSA-2023:1909](https://access.redhat.com/errata/RHSA-2023:1909) | <div class="adv_s">Security Advisory</div> ([CVE-2023-21930](https://access.redhat.com/security/cve/CVE-2023-21930), [CVE-2023-21937](https://access.redhat.com/security/cve/CVE-2023-21937), [CVE-2023-21938](https://access.redhat.com/security/cve/CVE-2023-21938), [CVE-2023-21939](https://access.redhat.com/security/cve/CVE-2023-21939), [CVE-2023-21954](https://access.redhat.com/security/cve/CVE-2023-21954), [CVE-2023-21967](https://access.redhat.com/security/cve/CVE-2023-21967), [CVE-2023-21968](https://access.redhat.com/security/cve/CVE-2023-21968))
java-1.8.0-openjdk-headless-debuginfo | 1.8.0.372.b07-1.el9_1 | |
java-1.8.0-openjdk-javadoc | 1.8.0.372.b07-1.el9_1 | [RHSA-2023:1909](https://access.redhat.com/errata/RHSA-2023:1909) | <div class="adv_s">Security Advisory</div> ([CVE-2023-21930](https://access.redhat.com/security/cve/CVE-2023-21930), [CVE-2023-21937](https://access.redhat.com/security/cve/CVE-2023-21937), [CVE-2023-21938](https://access.redhat.com/security/cve/CVE-2023-21938), [CVE-2023-21939](https://access.redhat.com/security/cve/CVE-2023-21939), [CVE-2023-21954](https://access.redhat.com/security/cve/CVE-2023-21954), [CVE-2023-21967](https://access.redhat.com/security/cve/CVE-2023-21967), [CVE-2023-21968](https://access.redhat.com/security/cve/CVE-2023-21968))
java-1.8.0-openjdk-javadoc-zip | 1.8.0.372.b07-1.el9_1 | [RHSA-2023:1909](https://access.redhat.com/errata/RHSA-2023:1909) | <div class="adv_s">Security Advisory</div> ([CVE-2023-21930](https://access.redhat.com/security/cve/CVE-2023-21930), [CVE-2023-21937](https://access.redhat.com/security/cve/CVE-2023-21937), [CVE-2023-21938](https://access.redhat.com/security/cve/CVE-2023-21938), [CVE-2023-21939](https://access.redhat.com/security/cve/CVE-2023-21939), [CVE-2023-21954](https://access.redhat.com/security/cve/CVE-2023-21954), [CVE-2023-21967](https://access.redhat.com/security/cve/CVE-2023-21967), [CVE-2023-21968](https://access.redhat.com/security/cve/CVE-2023-21968))
java-1.8.0-openjdk-src | 1.8.0.372.b07-1.el9_1 | [RHSA-2023:1909](https://access.redhat.com/errata/RHSA-2023:1909) | <div class="adv_s">Security Advisory</div> ([CVE-2023-21930](https://access.redhat.com/security/cve/CVE-2023-21930), [CVE-2023-21937](https://access.redhat.com/security/cve/CVE-2023-21937), [CVE-2023-21938](https://access.redhat.com/security/cve/CVE-2023-21938), [CVE-2023-21939](https://access.redhat.com/security/cve/CVE-2023-21939), [CVE-2023-21954](https://access.redhat.com/security/cve/CVE-2023-21954), [CVE-2023-21967](https://access.redhat.com/security/cve/CVE-2023-21967), [CVE-2023-21968](https://access.redhat.com/security/cve/CVE-2023-21968))
libpst-debugsource | 0.6.75-12.el9 | |
mpitests-debugsource | 5.8-1.el9 | |
numpy-debugsource | 1.20.1-5.el9 | |
pyodbc-debugsource | 4.0.30-4.el9 | |
python-cups-debugsource | 2.0.1-10.el9 | |
python-gssapi-debugsource | 1.6.9-5.el9 | |
python-markupsafe-debugsource | 1.1.1-12.el9 | |
python-netifaces-debugsource | 0.10.6-15.el9 | |
python-psutil-debugsource | 5.8.0-12.el9 | |
python-psycopg2-debugsource | 2.8.6-6.el9 | |
python-pycurl-debugsource | 7.43.0.6-8.el9 | |
python3-libfdt-debuginfo | 1.6.0-7.el9 | |
rpcsvc-proto-debugsource | 1.4-9.el9 | |
rust-zram-generator-debugsource | 0.3.2-7.el9 | |
scipy-debugsource | 1.6.2-8.el9 | |
speech-tools-debugsource | 2.5-18.el9 | |
v4l-utils-debugsource | 1.20.0-5.el9 | |

### CRB aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
brasero-debugsource | 3.12.2-19.el9 | |
brasero-nautilus-debuginfo | 3.12.2-19.el9 | |
ceph-debugsource | 16.2.4-5.el9 | |
clucene-debugsource | 2.3.3.4-42.20130812.e8e3d20git.el9 | |
Cython-debugsource | 0.29.22-7.el9 | |
gcab-debugsource | 1.4-6.el9 | |
java-1.8.0-openjdk-debuginfo | 1.8.0.372.b07-1.el9_1 | |
java-1.8.0-openjdk-debugsource | 1.8.0.372.b07-1.el9_1 | |
java-1.8.0-openjdk-demo-debuginfo | 1.8.0.372.b07-1.el9_1 | |
java-1.8.0-openjdk-demo-fastdebug | 1.8.0.372.b07-1.el9_1 | [RHSA-2023:1909](https://access.redhat.com/errata/RHSA-2023:1909) | <div class="adv_s">Security Advisory</div> ([CVE-2023-21930](https://access.redhat.com/security/cve/CVE-2023-21930), [CVE-2023-21937](https://access.redhat.com/security/cve/CVE-2023-21937), [CVE-2023-21938](https://access.redhat.com/security/cve/CVE-2023-21938), [CVE-2023-21939](https://access.redhat.com/security/cve/CVE-2023-21939), [CVE-2023-21954](https://access.redhat.com/security/cve/CVE-2023-21954), [CVE-2023-21967](https://access.redhat.com/security/cve/CVE-2023-21967), [CVE-2023-21968](https://access.redhat.com/security/cve/CVE-2023-21968))
java-1.8.0-openjdk-demo-fastdebug-debuginfo | 1.8.0.372.b07-1.el9_1 | |
java-1.8.0-openjdk-demo-slowdebug | 1.8.0.372.b07-1.el9_1 | [RHSA-2023:1909](https://access.redhat.com/errata/RHSA-2023:1909) | <div class="adv_s">Security Advisory</div> ([CVE-2023-21930](https://access.redhat.com/security/cve/CVE-2023-21930), [CVE-2023-21937](https://access.redhat.com/security/cve/CVE-2023-21937), [CVE-2023-21938](https://access.redhat.com/security/cve/CVE-2023-21938), [CVE-2023-21939](https://access.redhat.com/security/cve/CVE-2023-21939), [CVE-2023-21954](https://access.redhat.com/security/cve/CVE-2023-21954), [CVE-2023-21967](https://access.redhat.com/security/cve/CVE-2023-21967), [CVE-2023-21968](https://access.redhat.com/security/cve/CVE-2023-21968))
java-1.8.0-openjdk-demo-slowdebug-debuginfo | 1.8.0.372.b07-1.el9_1 | |
java-1.8.0-openjdk-devel-debuginfo | 1.8.0.372.b07-1.el9_1 | |
java-1.8.0-openjdk-devel-fastdebug | 1.8.0.372.b07-1.el9_1 | [RHSA-2023:1909](https://access.redhat.com/errata/RHSA-2023:1909) | <div class="adv_s">Security Advisory</div> ([CVE-2023-21930](https://access.redhat.com/security/cve/CVE-2023-21930), [CVE-2023-21937](https://access.redhat.com/security/cve/CVE-2023-21937), [CVE-2023-21938](https://access.redhat.com/security/cve/CVE-2023-21938), [CVE-2023-21939](https://access.redhat.com/security/cve/CVE-2023-21939), [CVE-2023-21954](https://access.redhat.com/security/cve/CVE-2023-21954), [CVE-2023-21967](https://access.redhat.com/security/cve/CVE-2023-21967), [CVE-2023-21968](https://access.redhat.com/security/cve/CVE-2023-21968))
java-1.8.0-openjdk-devel-fastdebug-debuginfo | 1.8.0.372.b07-1.el9_1 | |
java-1.8.0-openjdk-devel-slowdebug | 1.8.0.372.b07-1.el9_1 | [RHSA-2023:1909](https://access.redhat.com/errata/RHSA-2023:1909) | <div class="adv_s">Security Advisory</div> ([CVE-2023-21930](https://access.redhat.com/security/cve/CVE-2023-21930), [CVE-2023-21937](https://access.redhat.com/security/cve/CVE-2023-21937), [CVE-2023-21938](https://access.redhat.com/security/cve/CVE-2023-21938), [CVE-2023-21939](https://access.redhat.com/security/cve/CVE-2023-21939), [CVE-2023-21954](https://access.redhat.com/security/cve/CVE-2023-21954), [CVE-2023-21967](https://access.redhat.com/security/cve/CVE-2023-21967), [CVE-2023-21968](https://access.redhat.com/security/cve/CVE-2023-21968))
java-1.8.0-openjdk-devel-slowdebug-debuginfo | 1.8.0.372.b07-1.el9_1 | |
java-1.8.0-openjdk-fastdebug | 1.8.0.372.b07-1.el9_1 | [RHSA-2023:1909](https://access.redhat.com/errata/RHSA-2023:1909) | <div class="adv_s">Security Advisory</div> ([CVE-2023-21930](https://access.redhat.com/security/cve/CVE-2023-21930), [CVE-2023-21937](https://access.redhat.com/security/cve/CVE-2023-21937), [CVE-2023-21938](https://access.redhat.com/security/cve/CVE-2023-21938), [CVE-2023-21939](https://access.redhat.com/security/cve/CVE-2023-21939), [CVE-2023-21954](https://access.redhat.com/security/cve/CVE-2023-21954), [CVE-2023-21967](https://access.redhat.com/security/cve/CVE-2023-21967), [CVE-2023-21968](https://access.redhat.com/security/cve/CVE-2023-21968))
java-1.8.0-openjdk-fastdebug-debuginfo | 1.8.0.372.b07-1.el9_1 | |
java-1.8.0-openjdk-headless-debuginfo | 1.8.0.372.b07-1.el9_1 | |
java-1.8.0-openjdk-headless-fastdebug | 1.8.0.372.b07-1.el9_1 | [RHSA-2023:1909](https://access.redhat.com/errata/RHSA-2023:1909) | <div class="adv_s">Security Advisory</div> ([CVE-2023-21930](https://access.redhat.com/security/cve/CVE-2023-21930), [CVE-2023-21937](https://access.redhat.com/security/cve/CVE-2023-21937), [CVE-2023-21938](https://access.redhat.com/security/cve/CVE-2023-21938), [CVE-2023-21939](https://access.redhat.com/security/cve/CVE-2023-21939), [CVE-2023-21954](https://access.redhat.com/security/cve/CVE-2023-21954), [CVE-2023-21967](https://access.redhat.com/security/cve/CVE-2023-21967), [CVE-2023-21968](https://access.redhat.com/security/cve/CVE-2023-21968))
java-1.8.0-openjdk-headless-fastdebug-debuginfo | 1.8.0.372.b07-1.el9_1 | |
java-1.8.0-openjdk-headless-slowdebug | 1.8.0.372.b07-1.el9_1 | [RHSA-2023:1909](https://access.redhat.com/errata/RHSA-2023:1909) | <div class="adv_s">Security Advisory</div> ([CVE-2023-21930](https://access.redhat.com/security/cve/CVE-2023-21930), [CVE-2023-21937](https://access.redhat.com/security/cve/CVE-2023-21937), [CVE-2023-21938](https://access.redhat.com/security/cve/CVE-2023-21938), [CVE-2023-21939](https://access.redhat.com/security/cve/CVE-2023-21939), [CVE-2023-21954](https://access.redhat.com/security/cve/CVE-2023-21954), [CVE-2023-21967](https://access.redhat.com/security/cve/CVE-2023-21967), [CVE-2023-21968](https://access.redhat.com/security/cve/CVE-2023-21968))
java-1.8.0-openjdk-headless-slowdebug-debuginfo | 1.8.0.372.b07-1.el9_1 | |
java-1.8.0-openjdk-slowdebug | 1.8.0.372.b07-1.el9_1 | [RHSA-2023:1909](https://access.redhat.com/errata/RHSA-2023:1909) | <div class="adv_s">Security Advisory</div> ([CVE-2023-21930](https://access.redhat.com/security/cve/CVE-2023-21930), [CVE-2023-21937](https://access.redhat.com/security/cve/CVE-2023-21937), [CVE-2023-21938](https://access.redhat.com/security/cve/CVE-2023-21938), [CVE-2023-21939](https://access.redhat.com/security/cve/CVE-2023-21939), [CVE-2023-21954](https://access.redhat.com/security/cve/CVE-2023-21954), [CVE-2023-21967](https://access.redhat.com/security/cve/CVE-2023-21967), [CVE-2023-21968](https://access.redhat.com/security/cve/CVE-2023-21968))
java-1.8.0-openjdk-slowdebug-debuginfo | 1.8.0.372.b07-1.el9_1 | |
java-1.8.0-openjdk-src-fastdebug | 1.8.0.372.b07-1.el9_1 | [RHSA-2023:1909](https://access.redhat.com/errata/RHSA-2023:1909) | <div class="adv_s">Security Advisory</div> ([CVE-2023-21930](https://access.redhat.com/security/cve/CVE-2023-21930), [CVE-2023-21937](https://access.redhat.com/security/cve/CVE-2023-21937), [CVE-2023-21938](https://access.redhat.com/security/cve/CVE-2023-21938), [CVE-2023-21939](https://access.redhat.com/security/cve/CVE-2023-21939), [CVE-2023-21954](https://access.redhat.com/security/cve/CVE-2023-21954), [CVE-2023-21967](https://access.redhat.com/security/cve/CVE-2023-21967), [CVE-2023-21968](https://access.redhat.com/security/cve/CVE-2023-21968))
java-1.8.0-openjdk-src-slowdebug | 1.8.0.372.b07-1.el9_1 | [RHSA-2023:1909](https://access.redhat.com/errata/RHSA-2023:1909) | <div class="adv_s">Security Advisory</div> ([CVE-2023-21930](https://access.redhat.com/security/cve/CVE-2023-21930), [CVE-2023-21937](https://access.redhat.com/security/cve/CVE-2023-21937), [CVE-2023-21938](https://access.redhat.com/security/cve/CVE-2023-21938), [CVE-2023-21939](https://access.redhat.com/security/cve/CVE-2023-21939), [CVE-2023-21954](https://access.redhat.com/security/cve/CVE-2023-21954), [CVE-2023-21967](https://access.redhat.com/security/cve/CVE-2023-21967), [CVE-2023-21968](https://access.redhat.com/security/cve/CVE-2023-21968))
perl-gettext-debugsource | 1.07-21.el9 | |
python-psutil-debugsource | 5.8.0-12.el9 | |
python-ruamel-yaml-clib-debugsource | 0.1.2-8.el9.1 | |
python3-libfdt-debuginfo | 1.6.0-7.el9 | |
pyxattr-debugsource | 0.7.2-4.el9 | |
qhull-debugsource | 7.2.1-9.el9 | |
rpcsvc-proto-debugsource | 1.4-9.el9 | |
v4l-utils-debugsource | 1.20.0-5.el9 | |

### devel aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
brasero | 3.12.2-19.el9 | [RHBA-2022:2516](https://access.redhat.com/errata/RHBA-2022:2516) | <div class="adv_b">Bug Fix Advisory</div>
brasero-nautilus | 3.12.2-19.el9 | [RHBA-2022:2516](https://access.redhat.com/errata/RHBA-2022:2516) | <div class="adv_b">Bug Fix Advisory</div>
cheese | 3.38.0-6.el9 | [RHBA-2022:2763](https://access.redhat.com/errata/RHBA-2022:2763) | <div class="adv_b">Bug Fix Advisory</div>
qhull | 7.2.1-9.el9 | |
speech-tools | 2.5-18.el9 | |
star | 1.6-6.el9 | |
v4l-utils | 1.20.0-5.el9 | |

