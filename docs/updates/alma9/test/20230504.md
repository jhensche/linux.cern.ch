## 2023-05-04

### AppStream x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
emacs | 27.2-6.el9_1.1 | |
emacs-common | 27.2-6.el9_1.1 | |
emacs-common-debuginfo | 27.2-6.el9_1.1 | |
emacs-debuginfo | 27.2-6.el9_1.1 | |
emacs-debugsource | 27.2-6.el9_1.1 | |
emacs-filesystem | 27.2-6.el9_1.1 | |
emacs-lucid | 27.2-6.el9_1.1 | |
emacs-lucid-debuginfo | 27.2-6.el9_1.1 | |
emacs-nox | 27.2-6.el9_1.1 | |
emacs-nox-debuginfo | 27.2-6.el9_1.1 | |
libwebp | 1.2.0-6.el9_1 | [RHSA-2023:2078](https://access.redhat.com/errata/RHSA-2023:2078) | <div class="adv_s">Security Advisory</div> ([CVE-2023-1999](https://access.redhat.com/security/cve/CVE-2023-1999))
libwebp-debuginfo | 1.2.0-6.el9_1 | |
libwebp-debugsource | 1.2.0-6.el9_1 | |
libwebp-devel | 1.2.0-6.el9_1 | [RHSA-2023:2078](https://access.redhat.com/errata/RHSA-2023:2078) | <div class="adv_s">Security Advisory</div> ([CVE-2023-1999](https://access.redhat.com/security/cve/CVE-2023-1999))
libwebp-java-debuginfo | 1.2.0-6.el9_1 | |
libwebp-tools-debuginfo | 1.2.0-6.el9_1 | |

### devel x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
emacs-devel | 27.2-6.el9_1.1 | |
emacs-terminal | 27.2-6.el9_1.1 | |
libwebp-java | 1.2.0-6.el9_1 | |
libwebp-tools | 1.2.0-6.el9_1 | |
redhat-lsb | 4.1-56.el9 | |
redhat-lsb-core | 4.1-56.el9 | |
redhat-lsb-cxx | 4.1-56.el9 | |
redhat-lsb-desktop | 4.1-56.el9 | |
redhat-lsb-languages | 4.1-56.el9 | |
redhat-lsb-printing | 4.1-56.el9 | |
redhat-lsb-submod-multimedia | 4.1-56.el9 | |
redhat-lsb-submod-security | 4.1-56.el9 | |
redhat-lsb-supplemental | 4.1-56.el9 | |
redhat-lsb-trialuse | 4.1-56.el9 | |

### AppStream aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
emacs | 27.2-6.el9_1.1 | |
emacs-common | 27.2-6.el9_1.1 | |
emacs-common-debuginfo | 27.2-6.el9_1.1 | |
emacs-debuginfo | 27.2-6.el9_1.1 | |
emacs-debugsource | 27.2-6.el9_1.1 | |
emacs-filesystem | 27.2-6.el9_1.1 | |
emacs-lucid | 27.2-6.el9_1.1 | |
emacs-lucid-debuginfo | 27.2-6.el9_1.1 | |
emacs-nox | 27.2-6.el9_1.1 | |
emacs-nox-debuginfo | 27.2-6.el9_1.1 | |
libwebp | 1.2.0-6.el9_1 | [RHSA-2023:2078](https://access.redhat.com/errata/RHSA-2023:2078) | <div class="adv_s">Security Advisory</div> ([CVE-2023-1999](https://access.redhat.com/security/cve/CVE-2023-1999))
libwebp-debuginfo | 1.2.0-6.el9_1 | |
libwebp-debugsource | 1.2.0-6.el9_1 | |
libwebp-devel | 1.2.0-6.el9_1 | [RHSA-2023:2078](https://access.redhat.com/errata/RHSA-2023:2078) | <div class="adv_s">Security Advisory</div> ([CVE-2023-1999](https://access.redhat.com/security/cve/CVE-2023-1999))
libwebp-java-debuginfo | 1.2.0-6.el9_1 | |
libwebp-tools-debuginfo | 1.2.0-6.el9_1 | |

### devel aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
emacs-devel | 27.2-6.el9_1.1 | |
emacs-terminal | 27.2-6.el9_1.1 | |
libwebp-java | 1.2.0-6.el9_1 | |
libwebp-tools | 1.2.0-6.el9_1 | |
redhat-lsb | 4.1-56.el9 | |
redhat-lsb-core | 4.1-56.el9 | |
redhat-lsb-cxx | 4.1-56.el9 | |
redhat-lsb-desktop | 4.1-56.el9 | |
redhat-lsb-languages | 4.1-56.el9 | |
redhat-lsb-printing | 4.1-56.el9 | |
redhat-lsb-submod-multimedia | 4.1-56.el9 | |
redhat-lsb-submod-security | 4.1-56.el9 | |
redhat-lsb-supplemental | 4.1-56.el9 | |
redhat-lsb-trialuse | 4.1-56.el9 | |

