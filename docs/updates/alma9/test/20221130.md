## 2022-11-30

### CERN x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
msktutil | 1.2-1.al9.cern | |
msktutil-debugsource | 1.2-1.al9.cern | |

### CERN aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
msktutil | 1.2-1.al9.cern | |
msktutil-debugsource | 1.2-1.al9.cern | |

