## 2023-03-23

### AppStream x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
thunderbird | 102.9.0-1.el9_1.alma | |
thunderbird-debuginfo | 102.9.0-1.el9_1.alma | |
thunderbird-debugsource | 102.9.0-1.el9_1.alma | |

### plus x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
thunderbird | 102.9.0-1.el9_1.alma.plus | |
thunderbird-debuginfo | 102.9.0-1.el9_1.alma.plus | |
thunderbird-debugsource | 102.9.0-1.el9_1.alma.plus | |
thunderbird-librnp-rnp | 102.9.0-1.el9_1.alma.plus | |
thunderbird-librnp-rnp-debuginfo | 102.9.0-1.el9_1.alma.plus | |

### AppStream aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
thunderbird | 102.9.0-1.el9_1.alma | |
thunderbird-debuginfo | 102.9.0-1.el9_1.alma | |
thunderbird-debugsource | 102.9.0-1.el9_1.alma | |

### plus aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
thunderbird | 102.9.0-1.el9_1.alma.plus | |
thunderbird-debuginfo | 102.9.0-1.el9_1.alma.plus | |
thunderbird-debugsource | 102.9.0-1.el9_1.alma.plus | |
thunderbird-librnp-rnp | 102.9.0-1.el9_1.alma.plus | |
thunderbird-librnp-rnp-debuginfo | 102.9.0-1.el9_1.alma.plus | |

