## 2023-11-15

### openafs aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
kmod-openafs | 1.8.10-0.5.14.0_362.8.1.el9_3.al9.cern | |

