## 2023-11-16

### BaseOS x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
curl | 7.76.1-26.el9 | |
curl-debuginfo | 7.76.1-26.el9 | |
curl-debugsource | 7.76.1-26.el9 | |
curl-minimal | 7.76.1-26.el9 | |
curl-minimal-debuginfo | 7.76.1-26.el9 | |
libcurl | 7.76.1-26.el9 | |
libcurl-debuginfo | 7.76.1-26.el9 | |
libcurl-minimal | 7.76.1-26.el9 | |
libcurl-minimal-debuginfo | 7.76.1-26.el9 | |
libnetapi | 4.18.6-100.el9 | |
libnetapi-debuginfo | 4.18.6-100.el9 | |
libsmbclient | 4.18.6-100.el9 | |
libsmbclient-debuginfo | 4.18.6-100.el9 | |
libwbclient | 4.18.6-100.el9 | |
libwbclient-debuginfo | 4.18.6-100.el9 | |
python3-samba | 4.18.6-100.el9 | |
python3-samba-dc | 4.18.6-100.el9 | |
python3-samba-dc-debuginfo | 4.18.6-100.el9 | |
python3-samba-debuginfo | 4.18.6-100.el9 | |
samba | 4.18.6-100.el9 | |
samba-client-libs | 4.18.6-100.el9 | |
samba-client-libs-debuginfo | 4.18.6-100.el9 | |
samba-common | 4.18.6-100.el9 | |
samba-common-libs | 4.18.6-100.el9 | |
samba-common-libs-debuginfo | 4.18.6-100.el9 | |
samba-common-tools | 4.18.6-100.el9 | |
samba-common-tools-debuginfo | 4.18.6-100.el9 | |
samba-dc-libs | 4.18.6-100.el9 | |
samba-dc-libs-debuginfo | 4.18.6-100.el9 | |
samba-dcerpc | 4.18.6-100.el9 | |
samba-dcerpc-debuginfo | 4.18.6-100.el9 | |
samba-debuginfo | 4.18.6-100.el9 | |
samba-debugsource | 4.18.6-100.el9 | |
samba-ldb-ldap-modules | 4.18.6-100.el9 | |
samba-ldb-ldap-modules-debuginfo | 4.18.6-100.el9 | |
samba-libs | 4.18.6-100.el9 | |
samba-libs-debuginfo | 4.18.6-100.el9 | |
samba-tools | 4.18.6-100.el9 | |
samba-usershares | 4.18.6-100.el9 | |
samba-winbind | 4.18.6-100.el9 | |
samba-winbind-debuginfo | 4.18.6-100.el9 | |
samba-winbind-modules | 4.18.6-100.el9 | |
samba-winbind-modules-debuginfo | 4.18.6-100.el9 | |

### AppStream x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
edk2-aarch64 | 20230524-3.el9 | |
edk2-ovmf | 20230524-3.el9 | |
ghostscript | 9.54.0-13.el9 | |
ghostscript-debuginfo | 9.54.0-13.el9 | |
ghostscript-debugsource | 9.54.0-13.el9 | |
ghostscript-doc | 9.54.0-13.el9 | |
ghostscript-gtk-debuginfo | 9.54.0-13.el9 | |
ghostscript-tools-dvipdf | 9.54.0-13.el9 | |
ghostscript-tools-fonts | 9.54.0-13.el9 | |
ghostscript-tools-printing | 9.54.0-13.el9 | |
ghostscript-x11 | 9.54.0-13.el9 | |
ghostscript-x11-debuginfo | 9.54.0-13.el9 | |
libcurl-devel | 7.76.1-26.el9 | |
libgs | 9.54.0-13.el9 | |
libgs-debuginfo | 9.54.0-13.el9 | |
qemu-guest-agent | 8.0.0-16.el9_3.alma.1 | |
qemu-guest-agent-debuginfo | 8.0.0-16.el9_3.alma.1 | |
qemu-img | 8.0.0-16.el9_3.alma.1 | |
qemu-img-debuginfo | 8.0.0-16.el9_3.alma.1 | |
qemu-kvm | 8.0.0-16.el9_3.alma.1 | |
qemu-kvm-audio-dbus-debuginfo | 8.0.0-16.el9_3.alma.1 | |
qemu-kvm-audio-pa | 8.0.0-16.el9_3.alma.1 | |
qemu-kvm-audio-pa-debuginfo | 8.0.0-16.el9_3.alma.1 | |
qemu-kvm-block-blkio | 8.0.0-16.el9_3.alma.1 | |
qemu-kvm-block-blkio-debuginfo | 8.0.0-16.el9_3.alma.1 | |
qemu-kvm-block-curl | 8.0.0-16.el9_3.alma.1 | |
qemu-kvm-block-curl-debuginfo | 8.0.0-16.el9_3.alma.1 | |
qemu-kvm-block-rbd | 8.0.0-16.el9_3.alma.1 | |
qemu-kvm-block-rbd-debuginfo | 8.0.0-16.el9_3.alma.1 | |
qemu-kvm-common | 8.0.0-16.el9_3.alma.1 | |
qemu-kvm-common-debuginfo | 8.0.0-16.el9_3.alma.1 | |
qemu-kvm-core | 8.0.0-16.el9_3.alma.1 | |
qemu-kvm-core-debuginfo | 8.0.0-16.el9_3.alma.1 | |
qemu-kvm-debuginfo | 8.0.0-16.el9_3.alma.1 | |
qemu-kvm-debugsource | 8.0.0-16.el9_3.alma.1 | |
qemu-kvm-device-display-virtio-gpu | 8.0.0-16.el9_3.alma.1 | |
qemu-kvm-device-display-virtio-gpu-debuginfo | 8.0.0-16.el9_3.alma.1 | |
qemu-kvm-device-display-virtio-gpu-pci | 8.0.0-16.el9_3.alma.1 | |
qemu-kvm-device-display-virtio-gpu-pci-debuginfo | 8.0.0-16.el9_3.alma.1 | |
qemu-kvm-device-display-virtio-vga | 8.0.0-16.el9_3.alma.1 | |
qemu-kvm-device-display-virtio-vga-debuginfo | 8.0.0-16.el9_3.alma.1 | |
qemu-kvm-device-usb-host | 8.0.0-16.el9_3.alma.1 | |
qemu-kvm-device-usb-host-debuginfo | 8.0.0-16.el9_3.alma.1 | |
qemu-kvm-device-usb-redirect | 8.0.0-16.el9_3.alma.1 | |
qemu-kvm-device-usb-redirect-debuginfo | 8.0.0-16.el9_3.alma.1 | |
qemu-kvm-docs | 8.0.0-16.el9_3.alma.1 | |
qemu-kvm-tests-debuginfo | 8.0.0-16.el9_3.alma.1 | |
qemu-kvm-tools | 8.0.0-16.el9_3.alma.1 | |
qemu-kvm-tools-debuginfo | 8.0.0-16.el9_3.alma.1 | |
qemu-kvm-ui-dbus-debuginfo | 8.0.0-16.el9_3.alma.1 | |
qemu-kvm-ui-egl-headless | 8.0.0-16.el9_3.alma.1 | |
qemu-kvm-ui-egl-headless-debuginfo | 8.0.0-16.el9_3.alma.1 | |
qemu-kvm-ui-opengl | 8.0.0-16.el9_3.alma.1 | |
qemu-kvm-ui-opengl-debuginfo | 8.0.0-16.el9_3.alma.1 | |
qemu-pr-helper | 8.0.0-16.el9_3.alma.1 | |
qemu-pr-helper-debuginfo | 8.0.0-16.el9_3.alma.1 | |
samba-client | 4.18.6-100.el9 | |
samba-client-debuginfo | 4.18.6-100.el9 | |
samba-krb5-printing | 4.18.6-100.el9 | |
samba-krb5-printing-debuginfo | 4.18.6-100.el9 | |
samba-vfs-iouring | 4.18.6-100.el9 | |
samba-vfs-iouring-debuginfo | 4.18.6-100.el9 | |
samba-winbind-clients | 4.18.6-100.el9 | |
samba-winbind-clients-debuginfo | 4.18.6-100.el9 | |
samba-winbind-krb5-locator | 4.18.6-100.el9 | |
samba-winbind-krb5-locator-debuginfo | 4.18.6-100.el9 | |
samba-winexe | 4.18.6-100.el9 | |
samba-winexe-debuginfo | 4.18.6-100.el9 | |

### ResilientStorage x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
ctdb | 4.18.6-100.el9 | |
ctdb-debuginfo | 4.18.6-100.el9 | |

### CRB x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
edk2-aarch64 | 20230524-3.el9 | |
edk2-debugsource | 20230524-3.el9 | |
edk2-ovmf | 20230524-3.el9 | |
edk2-tools | 20230524-3.el9 | |
edk2-tools-debuginfo | 20230524-3.el9 | |
edk2-tools-doc | 20230524-3.el9 | |
libgs-devel | 9.54.0-13.el9 | |
libnetapi-devel | 4.18.6-100.el9 | |
libsmbclient-devel | 4.18.6-100.el9 | |
libwbclient-devel | 4.18.6-100.el9 | |
python3-samba-devel | 4.18.6-100.el9 | |
python3-samba-test | 4.18.6-100.el9 | |
samba-devel | 4.18.6-100.el9 | |
samba-pidl | 4.18.6-100.el9 | |
samba-test | 4.18.6-100.el9 | |
samba-test-debuginfo | 4.18.6-100.el9 | |
samba-test-libs | 4.18.6-100.el9 | |
samba-test-libs-debuginfo | 4.18.6-100.el9 | |

### devel x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
ghostscript-gtk | 9.54.0-13.el9 | |
qemu-kvm-audio-dbus | 8.0.0-16.el9_3.alma.1 | |
qemu-kvm-tests | 8.0.0-16.el9_3.alma.1 | |
qemu-kvm-ui-dbus | 8.0.0-16.el9_3.alma.1 | |

### BaseOS aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
ctdb-debuginfo | 4.18.6-100.el9 | |
curl | 7.76.1-26.el9 | |
curl-debuginfo | 7.76.1-26.el9 | |
curl-debugsource | 7.76.1-26.el9 | |
curl-minimal | 7.76.1-26.el9 | |
curl-minimal-debuginfo | 7.76.1-26.el9 | |
libcurl | 7.76.1-26.el9 | |
libcurl-debuginfo | 7.76.1-26.el9 | |
libcurl-minimal | 7.76.1-26.el9 | |
libcurl-minimal-debuginfo | 7.76.1-26.el9 | |
libnetapi | 4.18.6-100.el9 | |
libnetapi-debuginfo | 4.18.6-100.el9 | |
libsmbclient | 4.18.6-100.el9 | |
libsmbclient-debuginfo | 4.18.6-100.el9 | |
libwbclient | 4.18.6-100.el9 | |
libwbclient-debuginfo | 4.18.6-100.el9 | |
python3-samba | 4.18.6-100.el9 | |
python3-samba-dc | 4.18.6-100.el9 | |
python3-samba-dc-debuginfo | 4.18.6-100.el9 | |
python3-samba-debuginfo | 4.18.6-100.el9 | |
samba | 4.18.6-100.el9 | |
samba-client-libs | 4.18.6-100.el9 | |
samba-client-libs-debuginfo | 4.18.6-100.el9 | |
samba-common | 4.18.6-100.el9 | |
samba-common-libs | 4.18.6-100.el9 | |
samba-common-libs-debuginfo | 4.18.6-100.el9 | |
samba-common-tools | 4.18.6-100.el9 | |
samba-common-tools-debuginfo | 4.18.6-100.el9 | |
samba-dc-libs | 4.18.6-100.el9 | |
samba-dc-libs-debuginfo | 4.18.6-100.el9 | |
samba-dcerpc | 4.18.6-100.el9 | |
samba-dcerpc-debuginfo | 4.18.6-100.el9 | |
samba-debuginfo | 4.18.6-100.el9 | |
samba-debugsource | 4.18.6-100.el9 | |
samba-ldb-ldap-modules | 4.18.6-100.el9 | |
samba-ldb-ldap-modules-debuginfo | 4.18.6-100.el9 | |
samba-libs | 4.18.6-100.el9 | |
samba-libs-debuginfo | 4.18.6-100.el9 | |
samba-tools | 4.18.6-100.el9 | |
samba-usershares | 4.18.6-100.el9 | |
samba-winbind | 4.18.6-100.el9 | |
samba-winbind-debuginfo | 4.18.6-100.el9 | |
samba-winbind-modules | 4.18.6-100.el9 | |
samba-winbind-modules-debuginfo | 4.18.6-100.el9 | |

### AppStream aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
edk2-aarch64 | 20230524-3.el9 | |
edk2-ovmf | 20230524-3.el9 | |
ghostscript | 9.54.0-13.el9 | |
ghostscript-debuginfo | 9.54.0-13.el9 | |
ghostscript-debugsource | 9.54.0-13.el9 | |
ghostscript-doc | 9.54.0-13.el9 | |
ghostscript-gtk-debuginfo | 9.54.0-13.el9 | |
ghostscript-tools-dvipdf | 9.54.0-13.el9 | |
ghostscript-tools-fonts | 9.54.0-13.el9 | |
ghostscript-tools-printing | 9.54.0-13.el9 | |
ghostscript-x11 | 9.54.0-13.el9 | |
ghostscript-x11-debuginfo | 9.54.0-13.el9 | |
libcurl-devel | 7.76.1-26.el9 | |
libgs | 9.54.0-13.el9 | |
libgs-debuginfo | 9.54.0-13.el9 | |
qemu-guest-agent | 8.0.0-16.el9_3.alma.1 | |
qemu-guest-agent-debuginfo | 8.0.0-16.el9_3.alma.1 | |
qemu-img | 8.0.0-16.el9_3.alma.1 | |
qemu-img-debuginfo | 8.0.0-16.el9_3.alma.1 | |
qemu-kvm | 8.0.0-16.el9_3.alma.1 | |
qemu-kvm-audio-dbus-debuginfo | 8.0.0-16.el9_3.alma.1 | |
qemu-kvm-audio-pa | 8.0.0-16.el9_3.alma.1 | |
qemu-kvm-audio-pa-debuginfo | 8.0.0-16.el9_3.alma.1 | |
qemu-kvm-block-blkio | 8.0.0-16.el9_3.alma.1 | |
qemu-kvm-block-blkio-debuginfo | 8.0.0-16.el9_3.alma.1 | |
qemu-kvm-block-curl | 8.0.0-16.el9_3.alma.1 | |
qemu-kvm-block-curl-debuginfo | 8.0.0-16.el9_3.alma.1 | |
qemu-kvm-block-rbd | 8.0.0-16.el9_3.alma.1 | |
qemu-kvm-block-rbd-debuginfo | 8.0.0-16.el9_3.alma.1 | |
qemu-kvm-common | 8.0.0-16.el9_3.alma.1 | |
qemu-kvm-common-debuginfo | 8.0.0-16.el9_3.alma.1 | |
qemu-kvm-core | 8.0.0-16.el9_3.alma.1 | |
qemu-kvm-core-debuginfo | 8.0.0-16.el9_3.alma.1 | |
qemu-kvm-debuginfo | 8.0.0-16.el9_3.alma.1 | |
qemu-kvm-debugsource | 8.0.0-16.el9_3.alma.1 | |
qemu-kvm-device-display-virtio-gpu | 8.0.0-16.el9_3.alma.1 | |
qemu-kvm-device-display-virtio-gpu-debuginfo | 8.0.0-16.el9_3.alma.1 | |
qemu-kvm-device-display-virtio-gpu-pci | 8.0.0-16.el9_3.alma.1 | |
qemu-kvm-device-display-virtio-gpu-pci-debuginfo | 8.0.0-16.el9_3.alma.1 | |
qemu-kvm-device-usb-host | 8.0.0-16.el9_3.alma.1 | |
qemu-kvm-device-usb-host-debuginfo | 8.0.0-16.el9_3.alma.1 | |
qemu-kvm-docs | 8.0.0-16.el9_3.alma.1 | |
qemu-kvm-tests-debuginfo | 8.0.0-16.el9_3.alma.1 | |
qemu-kvm-tools | 8.0.0-16.el9_3.alma.1 | |
qemu-kvm-tools-debuginfo | 8.0.0-16.el9_3.alma.1 | |
qemu-kvm-ui-dbus-debuginfo | 8.0.0-16.el9_3.alma.1 | |
qemu-pr-helper | 8.0.0-16.el9_3.alma.1 | |
qemu-pr-helper-debuginfo | 8.0.0-16.el9_3.alma.1 | |
samba-client | 4.18.6-100.el9 | |
samba-client-debuginfo | 4.18.6-100.el9 | |
samba-krb5-printing | 4.18.6-100.el9 | |
samba-krb5-printing-debuginfo | 4.18.6-100.el9 | |
samba-vfs-iouring | 4.18.6-100.el9 | |
samba-vfs-iouring-debuginfo | 4.18.6-100.el9 | |
samba-winbind-clients | 4.18.6-100.el9 | |
samba-winbind-clients-debuginfo | 4.18.6-100.el9 | |
samba-winbind-krb5-locator | 4.18.6-100.el9 | |
samba-winbind-krb5-locator-debuginfo | 4.18.6-100.el9 | |

### ResilientStorage aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
ctdb | 4.18.6-100.el9 | |

### CRB aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
edk2-aarch64 | 20230524-3.el9 | |
edk2-debugsource | 20230524-3.el9 | |
edk2-ovmf | 20230524-3.el9 | |
edk2-tools | 20230524-3.el9 | |
edk2-tools-debuginfo | 20230524-3.el9 | |
edk2-tools-doc | 20230524-3.el9 | |
libgs-devel | 9.54.0-13.el9 | |
libnetapi-devel | 4.18.6-100.el9 | |
libsmbclient-devel | 4.18.6-100.el9 | |
libwbclient-devel | 4.18.6-100.el9 | |
python3-samba-devel | 4.18.6-100.el9 | |
python3-samba-test | 4.18.6-100.el9 | |
samba-devel | 4.18.6-100.el9 | |
samba-pidl | 4.18.6-100.el9 | |
samba-test | 4.18.6-100.el9 | |
samba-test-debuginfo | 4.18.6-100.el9 | |
samba-test-libs | 4.18.6-100.el9 | |
samba-test-libs-debuginfo | 4.18.6-100.el9 | |

### devel aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
ghostscript-gtk | 9.54.0-13.el9 | |
qemu-kvm-audio-dbus | 8.0.0-16.el9_3.alma.1 | |
qemu-kvm-tests | 8.0.0-16.el9_3.alma.1 | |
qemu-kvm-ui-dbus | 8.0.0-16.el9_3.alma.1 | |

