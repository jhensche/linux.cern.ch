## 2023-01-09

### CERN x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
cern-get-certificate | 0.9.4-4.al9.cern | |
cern-get-certificate | 0.9.4-5.al9.cern | |

### AppStream x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
bcel | 6.4.1-9.el9_1 | [RHSA-2023:0005](https://access.redhat.com/errata/RHSA-2023:0005) | <div class="adv_s">Security Advisory</div> ([CVE-2022-42920](https://access.redhat.com/security/cve/CVE-2022-42920))
firefox | 102.6.0-1.el9_1.alma | |
firefox-debuginfo | 102.6.0-1.el9_1.alma | |
firefox-debugsource | 102.6.0-1.el9_1.alma | |
thunderbird | 102.6.0-2.el9_1.alma | |
thunderbird-debuginfo | 102.6.0-2.el9_1.alma | |
thunderbird-debugsource | 102.6.0-2.el9_1.alma | |

### devel x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
apache-commons-parent | 52-6.el9 | |
bcel-javadoc | 6.4.1-9.el9_1 | |

### plus x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
thunderbird | 102.6.0-2.el9_1.alma.plus | |
thunderbird-debuginfo | 102.6.0-2.el9_1.alma.plus | |
thunderbird-debugsource | 102.6.0-2.el9_1.alma.plus | |
thunderbird-librnp-rnp | 102.6.0-2.el9_1.alma.plus | |
thunderbird-librnp-rnp-debuginfo | 102.6.0-2.el9_1.alma.plus | |

### CERN aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
cern-get-certificate | 0.9.4-4.al9.cern | |
cern-get-certificate | 0.9.4-5.al9.cern | |

### AppStream aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
bcel | 6.4.1-9.el9_1 | [RHSA-2023:0005](https://access.redhat.com/errata/RHSA-2023:0005) | <div class="adv_s">Security Advisory</div> ([CVE-2022-42920](https://access.redhat.com/security/cve/CVE-2022-42920))
firefox | 102.6.0-1.el9_1.alma | |
firefox-debuginfo | 102.6.0-1.el9_1.alma | |
firefox-debugsource | 102.6.0-1.el9_1.alma | |
thunderbird | 102.6.0-2.el9_1.alma | |
thunderbird-debuginfo | 102.6.0-2.el9_1.alma | |
thunderbird-debugsource | 102.6.0-2.el9_1.alma | |

### devel aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
apache-commons-parent | 52-6.el9 | |
bcel-javadoc | 6.4.1-9.el9_1 | |

### plus aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
thunderbird | 102.6.0-2.el9_1.alma.plus | |
thunderbird-debuginfo | 102.6.0-2.el9_1.alma.plus | |
thunderbird-debugsource | 102.6.0-2.el9_1.alma.plus | |
thunderbird-librnp-rnp | 102.6.0-2.el9_1.alma.plus | |
thunderbird-librnp-rnp-debuginfo | 102.6.0-2.el9_1.alma.plus | |

