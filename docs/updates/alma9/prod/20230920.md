## 2023-09-20

### BaseOS x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
microcode_ctl | 20220809-2.20230808.2.el9_2 | |

### AppStream x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
firefox | 102.15.0-1.el9_2.alma | [ALSA-2023:4958](https://errata.almalinux.org/9/ALSA-2023-4958.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-4051](https://access.redhat.com/security/cve/CVE-2023-4051), [CVE-2023-4053](https://access.redhat.com/security/cve/CVE-2023-4053), [CVE-2023-4573](https://access.redhat.com/security/cve/CVE-2023-4573), [CVE-2023-4574](https://access.redhat.com/security/cve/CVE-2023-4574), [CVE-2023-4575](https://access.redhat.com/security/cve/CVE-2023-4575), [CVE-2023-4577](https://access.redhat.com/security/cve/CVE-2023-4577), [CVE-2023-4578](https://access.redhat.com/security/cve/CVE-2023-4578), [CVE-2023-4580](https://access.redhat.com/security/cve/CVE-2023-4580), [CVE-2023-4581](https://access.redhat.com/security/cve/CVE-2023-4581), [CVE-2023-4583](https://access.redhat.com/security/cve/CVE-2023-4583), [CVE-2023-4584](https://access.redhat.com/security/cve/CVE-2023-4584), [CVE-2023-4585](https://access.redhat.com/security/cve/CVE-2023-4585))
firefox-debuginfo | 102.15.0-1.el9_2.alma | |
firefox-debugsource | 102.15.0-1.el9_2.alma | |
firefox-x11 | 102.15.0-1.el9_2.alma | [ALSA-2023:4958](https://errata.almalinux.org/9/ALSA-2023-4958.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-4051](https://access.redhat.com/security/cve/CVE-2023-4051), [CVE-2023-4053](https://access.redhat.com/security/cve/CVE-2023-4053), [CVE-2023-4573](https://access.redhat.com/security/cve/CVE-2023-4573), [CVE-2023-4574](https://access.redhat.com/security/cve/CVE-2023-4574), [CVE-2023-4575](https://access.redhat.com/security/cve/CVE-2023-4575), [CVE-2023-4577](https://access.redhat.com/security/cve/CVE-2023-4577), [CVE-2023-4578](https://access.redhat.com/security/cve/CVE-2023-4578), [CVE-2023-4580](https://access.redhat.com/security/cve/CVE-2023-4580), [CVE-2023-4581](https://access.redhat.com/security/cve/CVE-2023-4581), [CVE-2023-4583](https://access.redhat.com/security/cve/CVE-2023-4583), [CVE-2023-4584](https://access.redhat.com/security/cve/CVE-2023-4584), [CVE-2023-4585](https://access.redhat.com/security/cve/CVE-2023-4585))
flac-debuginfo | 1.3.3-10.el9_2.1 | |
flac-debugsource | 1.3.3-10.el9_2.1 | |
flac-libs | 1.3.3-10.el9_2.1 | [ALSA-2023:5048](https://errata.almalinux.org/9/ALSA-2023-5048.html) | <div class="adv_s">Security Advisory</div> ([CVE-2020-22219](https://access.redhat.com/security/cve/CVE-2020-22219))
flac-libs-debuginfo | 1.3.3-10.el9_2.1 | |
thunderbird | 102.15.0-1.el9_2.alma | [ALSA-2023:4955](https://errata.almalinux.org/9/ALSA-2023-4955.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-4051](https://access.redhat.com/security/cve/CVE-2023-4051), [CVE-2023-4053](https://access.redhat.com/security/cve/CVE-2023-4053), [CVE-2023-4573](https://access.redhat.com/security/cve/CVE-2023-4573), [CVE-2023-4574](https://access.redhat.com/security/cve/CVE-2023-4574), [CVE-2023-4575](https://access.redhat.com/security/cve/CVE-2023-4575), [CVE-2023-4577](https://access.redhat.com/security/cve/CVE-2023-4577), [CVE-2023-4578](https://access.redhat.com/security/cve/CVE-2023-4578), [CVE-2023-4580](https://access.redhat.com/security/cve/CVE-2023-4580), [CVE-2023-4581](https://access.redhat.com/security/cve/CVE-2023-4581), [CVE-2023-4583](https://access.redhat.com/security/cve/CVE-2023-4583), [CVE-2023-4584](https://access.redhat.com/security/cve/CVE-2023-4584), [CVE-2023-4585](https://access.redhat.com/security/cve/CVE-2023-4585))
thunderbird-debuginfo | 102.15.0-1.el9_2.alma | |
thunderbird-debugsource | 102.15.0-1.el9_2.alma | |

### CRB x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
flac | 1.3.3-10.el9_2.1 | [ALSA-2023:5048](https://errata.almalinux.org/9/ALSA-2023-5048.html) | <div class="adv_s">Security Advisory</div> ([CVE-2020-22219](https://access.redhat.com/security/cve/CVE-2020-22219))
flac-devel | 1.3.3-10.el9_2.1 | [ALSA-2023:5048](https://errata.almalinux.org/9/ALSA-2023-5048.html) | <div class="adv_s">Security Advisory</div> ([CVE-2020-22219](https://access.redhat.com/security/cve/CVE-2020-22219))

### plus x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
thunderbird | 102.15.0-1.el9_2.alma.plus | [ALSA-2023:4955](https://errata.almalinux.org/9/ALSA-2023-4955.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-4051](https://access.redhat.com/security/cve/CVE-2023-4051), [CVE-2023-4053](https://access.redhat.com/security/cve/CVE-2023-4053), [CVE-2023-4573](https://access.redhat.com/security/cve/CVE-2023-4573), [CVE-2023-4574](https://access.redhat.com/security/cve/CVE-2023-4574), [CVE-2023-4575](https://access.redhat.com/security/cve/CVE-2023-4575), [CVE-2023-4577](https://access.redhat.com/security/cve/CVE-2023-4577), [CVE-2023-4578](https://access.redhat.com/security/cve/CVE-2023-4578), [CVE-2023-4580](https://access.redhat.com/security/cve/CVE-2023-4580), [CVE-2023-4581](https://access.redhat.com/security/cve/CVE-2023-4581), [CVE-2023-4583](https://access.redhat.com/security/cve/CVE-2023-4583), [CVE-2023-4584](https://access.redhat.com/security/cve/CVE-2023-4584), [CVE-2023-4585](https://access.redhat.com/security/cve/CVE-2023-4585))
thunderbird-debuginfo | 102.15.0-1.el9_2.alma.plus | |
thunderbird-debugsource | 102.15.0-1.el9_2.alma.plus | |
thunderbird-librnp-rnp | 102.15.0-1.el9_2.alma.plus | |
thunderbird-librnp-rnp-debuginfo | 102.15.0-1.el9_2.alma.plus | |

### BaseOS aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
microcode_ctl | 20220809-2.20230808.2.el9_2 | |

### AppStream aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
firefox | 102.15.0-1.el9_2.alma | [ALSA-2023:4958](https://errata.almalinux.org/9/ALSA-2023-4958.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-4051](https://access.redhat.com/security/cve/CVE-2023-4051), [CVE-2023-4053](https://access.redhat.com/security/cve/CVE-2023-4053), [CVE-2023-4573](https://access.redhat.com/security/cve/CVE-2023-4573), [CVE-2023-4574](https://access.redhat.com/security/cve/CVE-2023-4574), [CVE-2023-4575](https://access.redhat.com/security/cve/CVE-2023-4575), [CVE-2023-4577](https://access.redhat.com/security/cve/CVE-2023-4577), [CVE-2023-4578](https://access.redhat.com/security/cve/CVE-2023-4578), [CVE-2023-4580](https://access.redhat.com/security/cve/CVE-2023-4580), [CVE-2023-4581](https://access.redhat.com/security/cve/CVE-2023-4581), [CVE-2023-4583](https://access.redhat.com/security/cve/CVE-2023-4583), [CVE-2023-4584](https://access.redhat.com/security/cve/CVE-2023-4584), [CVE-2023-4585](https://access.redhat.com/security/cve/CVE-2023-4585))
firefox-debuginfo | 102.15.0-1.el9_2.alma | |
firefox-debugsource | 102.15.0-1.el9_2.alma | |
firefox-x11 | 102.15.0-1.el9_2.alma | [ALSA-2023:4958](https://errata.almalinux.org/9/ALSA-2023-4958.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-4051](https://access.redhat.com/security/cve/CVE-2023-4051), [CVE-2023-4053](https://access.redhat.com/security/cve/CVE-2023-4053), [CVE-2023-4573](https://access.redhat.com/security/cve/CVE-2023-4573), [CVE-2023-4574](https://access.redhat.com/security/cve/CVE-2023-4574), [CVE-2023-4575](https://access.redhat.com/security/cve/CVE-2023-4575), [CVE-2023-4577](https://access.redhat.com/security/cve/CVE-2023-4577), [CVE-2023-4578](https://access.redhat.com/security/cve/CVE-2023-4578), [CVE-2023-4580](https://access.redhat.com/security/cve/CVE-2023-4580), [CVE-2023-4581](https://access.redhat.com/security/cve/CVE-2023-4581), [CVE-2023-4583](https://access.redhat.com/security/cve/CVE-2023-4583), [CVE-2023-4584](https://access.redhat.com/security/cve/CVE-2023-4584), [CVE-2023-4585](https://access.redhat.com/security/cve/CVE-2023-4585))
flac-debuginfo | 1.3.3-10.el9_2.1 | |
flac-debugsource | 1.3.3-10.el9_2.1 | |
flac-libs | 1.3.3-10.el9_2.1 | [ALSA-2023:5048](https://errata.almalinux.org/9/ALSA-2023-5048.html) | <div class="adv_s">Security Advisory</div> ([CVE-2020-22219](https://access.redhat.com/security/cve/CVE-2020-22219))
flac-libs-debuginfo | 1.3.3-10.el9_2.1 | |
thunderbird | 102.15.0-1.el9_2.alma | [ALSA-2023:4955](https://errata.almalinux.org/9/ALSA-2023-4955.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-4051](https://access.redhat.com/security/cve/CVE-2023-4051), [CVE-2023-4053](https://access.redhat.com/security/cve/CVE-2023-4053), [CVE-2023-4573](https://access.redhat.com/security/cve/CVE-2023-4573), [CVE-2023-4574](https://access.redhat.com/security/cve/CVE-2023-4574), [CVE-2023-4575](https://access.redhat.com/security/cve/CVE-2023-4575), [CVE-2023-4577](https://access.redhat.com/security/cve/CVE-2023-4577), [CVE-2023-4578](https://access.redhat.com/security/cve/CVE-2023-4578), [CVE-2023-4580](https://access.redhat.com/security/cve/CVE-2023-4580), [CVE-2023-4581](https://access.redhat.com/security/cve/CVE-2023-4581), [CVE-2023-4583](https://access.redhat.com/security/cve/CVE-2023-4583), [CVE-2023-4584](https://access.redhat.com/security/cve/CVE-2023-4584), [CVE-2023-4585](https://access.redhat.com/security/cve/CVE-2023-4585))
thunderbird-debuginfo | 102.15.0-1.el9_2.alma | |
thunderbird-debugsource | 102.15.0-1.el9_2.alma | |

### CRB aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
flac | 1.3.3-10.el9_2.1 | [ALSA-2023:5048](https://errata.almalinux.org/9/ALSA-2023-5048.html) | <div class="adv_s">Security Advisory</div> ([CVE-2020-22219](https://access.redhat.com/security/cve/CVE-2020-22219))
flac-devel | 1.3.3-10.el9_2.1 | [ALSA-2023:5048](https://errata.almalinux.org/9/ALSA-2023-5048.html) | <div class="adv_s">Security Advisory</div> ([CVE-2020-22219](https://access.redhat.com/security/cve/CVE-2020-22219))

### plus aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
thunderbird | 102.15.0-1.el9_2.alma.plus | [ALSA-2023:4955](https://errata.almalinux.org/9/ALSA-2023-4955.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-4051](https://access.redhat.com/security/cve/CVE-2023-4051), [CVE-2023-4053](https://access.redhat.com/security/cve/CVE-2023-4053), [CVE-2023-4573](https://access.redhat.com/security/cve/CVE-2023-4573), [CVE-2023-4574](https://access.redhat.com/security/cve/CVE-2023-4574), [CVE-2023-4575](https://access.redhat.com/security/cve/CVE-2023-4575), [CVE-2023-4577](https://access.redhat.com/security/cve/CVE-2023-4577), [CVE-2023-4578](https://access.redhat.com/security/cve/CVE-2023-4578), [CVE-2023-4580](https://access.redhat.com/security/cve/CVE-2023-4580), [CVE-2023-4581](https://access.redhat.com/security/cve/CVE-2023-4581), [CVE-2023-4583](https://access.redhat.com/security/cve/CVE-2023-4583), [CVE-2023-4584](https://access.redhat.com/security/cve/CVE-2023-4584), [CVE-2023-4585](https://access.redhat.com/security/cve/CVE-2023-4585))
thunderbird-debuginfo | 102.15.0-1.el9_2.alma.plus | |
thunderbird-debugsource | 102.15.0-1.el9_2.alma.plus | |
thunderbird-librnp-rnp | 102.15.0-1.el9_2.alma.plus | |
thunderbird-librnp-rnp-debuginfo | 102.15.0-1.el9_2.alma.plus | |

