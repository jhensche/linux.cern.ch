## 2023-02-01

### CERN x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
cern-get-keytab | 1.2.1-1.al9.cern | |
oracle-release | 1.5-2.al9.cern | |

### openafs x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
dkms-openafs | 1.8.9.0-1.al9.cern | |
kmod-openafs | 1.8.9.0-1.5.14.0_162.6.1.el9_1.al9.cern | |
openafs | 1.8.9.0-1.al9.cern | |
openafs-authlibs | 1.8.9.0-1.al9.cern | |
openafs-authlibs-devel | 1.8.9.0-1.al9.cern | |
openafs-client | 1.8.9.0-1.al9.cern | |
openafs-compat | 1.8.9.0-1.al9.cern | |
openafs-debugsource | 1.8.9.0-1.al9.cern | |
openafs-debugsource | 1.8.9.0_5.14.0_162.6.1.el9_1-1.al9.cern | |
openafs-devel | 1.8.9.0-1.al9.cern | |
openafs-docs | 1.8.9.0-1.al9.cern | |
openafs-kernel-source | 1.8.9.0-1.al9.cern | |
openafs-krb5 | 1.8.9.0-1.al9.cern | |
openafs-server | 1.8.9.0-1.al9.cern | |

### AppStream x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
java-11-openjdk | 11.0.18.0.10-2.el9_1 | [RHSA-2023:0202](https://access.redhat.com/errata/RHSA-2023:0202) | <div class="adv_s">Security Advisory</div> ([CVE-2023-21835](https://access.redhat.com/security/cve/CVE-2023-21835), [CVE-2023-21843](https://access.redhat.com/security/cve/CVE-2023-21843))
java-11-openjdk-debuginfo | 11.0.18.0.10-2.el9_1 | |
java-11-openjdk-debugsource | 11.0.18.0.10-2.el9_1 | |
java-11-openjdk-demo | 11.0.18.0.10-2.el9_1 | [RHSA-2023:0202](https://access.redhat.com/errata/RHSA-2023:0202) | <div class="adv_s">Security Advisory</div> ([CVE-2023-21835](https://access.redhat.com/security/cve/CVE-2023-21835), [CVE-2023-21843](https://access.redhat.com/security/cve/CVE-2023-21843))
java-11-openjdk-devel | 11.0.18.0.10-2.el9_1 | [RHSA-2023:0202](https://access.redhat.com/errata/RHSA-2023:0202) | <div class="adv_s">Security Advisory</div> ([CVE-2023-21835](https://access.redhat.com/security/cve/CVE-2023-21835), [CVE-2023-21843](https://access.redhat.com/security/cve/CVE-2023-21843))
java-11-openjdk-devel-debuginfo | 11.0.18.0.10-2.el9_1 | |
java-11-openjdk-headless | 11.0.18.0.10-2.el9_1 | [RHSA-2023:0202](https://access.redhat.com/errata/RHSA-2023:0202) | <div class="adv_s">Security Advisory</div> ([CVE-2023-21835](https://access.redhat.com/security/cve/CVE-2023-21835), [CVE-2023-21843](https://access.redhat.com/security/cve/CVE-2023-21843))
java-11-openjdk-headless-debuginfo | 11.0.18.0.10-2.el9_1 | |
java-11-openjdk-javadoc | 11.0.18.0.10-2.el9_1 | [RHSA-2023:0202](https://access.redhat.com/errata/RHSA-2023:0202) | <div class="adv_s">Security Advisory</div> ([CVE-2023-21835](https://access.redhat.com/security/cve/CVE-2023-21835), [CVE-2023-21843](https://access.redhat.com/security/cve/CVE-2023-21843))
java-11-openjdk-javadoc-zip | 11.0.18.0.10-2.el9_1 | [RHSA-2023:0202](https://access.redhat.com/errata/RHSA-2023:0202) | <div class="adv_s">Security Advisory</div> ([CVE-2023-21835](https://access.redhat.com/security/cve/CVE-2023-21835), [CVE-2023-21843](https://access.redhat.com/security/cve/CVE-2023-21843))
java-11-openjdk-jmods | 11.0.18.0.10-2.el9_1 | [RHSA-2023:0202](https://access.redhat.com/errata/RHSA-2023:0202) | <div class="adv_s">Security Advisory</div> ([CVE-2023-21835](https://access.redhat.com/security/cve/CVE-2023-21835), [CVE-2023-21843](https://access.redhat.com/security/cve/CVE-2023-21843))
java-11-openjdk-src | 11.0.18.0.10-2.el9_1 | [RHSA-2023:0202](https://access.redhat.com/errata/RHSA-2023:0202) | <div class="adv_s">Security Advisory</div> ([CVE-2023-21835](https://access.redhat.com/security/cve/CVE-2023-21835), [CVE-2023-21843](https://access.redhat.com/security/cve/CVE-2023-21843))
java-11-openjdk-static-libs | 11.0.18.0.10-2.el9_1 | [RHSA-2023:0202](https://access.redhat.com/errata/RHSA-2023:0202) | <div class="adv_s">Security Advisory</div> ([CVE-2023-21835](https://access.redhat.com/security/cve/CVE-2023-21835), [CVE-2023-21843](https://access.redhat.com/security/cve/CVE-2023-21843))

### CRB x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
java-11-openjdk-debuginfo | 11.0.18.0.10-2.el9_1 | |
java-11-openjdk-debugsource | 11.0.18.0.10-2.el9_1 | |
java-11-openjdk-demo-fastdebug | 11.0.18.0.10-2.el9_1 | [RHSA-2023:0202](https://access.redhat.com/errata/RHSA-2023:0202) | <div class="adv_s">Security Advisory</div> ([CVE-2023-21835](https://access.redhat.com/security/cve/CVE-2023-21835), [CVE-2023-21843](https://access.redhat.com/security/cve/CVE-2023-21843))
java-11-openjdk-demo-slowdebug | 11.0.18.0.10-2.el9_1 | [RHSA-2023:0202](https://access.redhat.com/errata/RHSA-2023:0202) | <div class="adv_s">Security Advisory</div> ([CVE-2023-21835](https://access.redhat.com/security/cve/CVE-2023-21835), [CVE-2023-21843](https://access.redhat.com/security/cve/CVE-2023-21843))
java-11-openjdk-devel-debuginfo | 11.0.18.0.10-2.el9_1 | |
java-11-openjdk-devel-fastdebug | 11.0.18.0.10-2.el9_1 | [RHSA-2023:0202](https://access.redhat.com/errata/RHSA-2023:0202) | <div class="adv_s">Security Advisory</div> ([CVE-2023-21835](https://access.redhat.com/security/cve/CVE-2023-21835), [CVE-2023-21843](https://access.redhat.com/security/cve/CVE-2023-21843))
java-11-openjdk-devel-fastdebug-debuginfo | 11.0.18.0.10-2.el9_1 | |
java-11-openjdk-devel-slowdebug | 11.0.18.0.10-2.el9_1 | [RHSA-2023:0202](https://access.redhat.com/errata/RHSA-2023:0202) | <div class="adv_s">Security Advisory</div> ([CVE-2023-21835](https://access.redhat.com/security/cve/CVE-2023-21835), [CVE-2023-21843](https://access.redhat.com/security/cve/CVE-2023-21843))
java-11-openjdk-devel-slowdebug-debuginfo | 11.0.18.0.10-2.el9_1 | |
java-11-openjdk-fastdebug | 11.0.18.0.10-2.el9_1 | [RHSA-2023:0202](https://access.redhat.com/errata/RHSA-2023:0202) | <div class="adv_s">Security Advisory</div> ([CVE-2023-21835](https://access.redhat.com/security/cve/CVE-2023-21835), [CVE-2023-21843](https://access.redhat.com/security/cve/CVE-2023-21843))
java-11-openjdk-fastdebug-debuginfo | 11.0.18.0.10-2.el9_1 | |
java-11-openjdk-headless-debuginfo | 11.0.18.0.10-2.el9_1 | |
java-11-openjdk-headless-fastdebug | 11.0.18.0.10-2.el9_1 | [RHSA-2023:0202](https://access.redhat.com/errata/RHSA-2023:0202) | <div class="adv_s">Security Advisory</div> ([CVE-2023-21835](https://access.redhat.com/security/cve/CVE-2023-21835), [CVE-2023-21843](https://access.redhat.com/security/cve/CVE-2023-21843))
java-11-openjdk-headless-fastdebug-debuginfo | 11.0.18.0.10-2.el9_1 | |
java-11-openjdk-headless-slowdebug | 11.0.18.0.10-2.el9_1 | [RHSA-2023:0202](https://access.redhat.com/errata/RHSA-2023:0202) | <div class="adv_s">Security Advisory</div> ([CVE-2023-21835](https://access.redhat.com/security/cve/CVE-2023-21835), [CVE-2023-21843](https://access.redhat.com/security/cve/CVE-2023-21843))
java-11-openjdk-headless-slowdebug-debuginfo | 11.0.18.0.10-2.el9_1 | |
java-11-openjdk-jmods-fastdebug | 11.0.18.0.10-2.el9_1 | [RHSA-2023:0202](https://access.redhat.com/errata/RHSA-2023:0202) | <div class="adv_s">Security Advisory</div> ([CVE-2023-21835](https://access.redhat.com/security/cve/CVE-2023-21835), [CVE-2023-21843](https://access.redhat.com/security/cve/CVE-2023-21843))
java-11-openjdk-jmods-slowdebug | 11.0.18.0.10-2.el9_1 | [RHSA-2023:0202](https://access.redhat.com/errata/RHSA-2023:0202) | <div class="adv_s">Security Advisory</div> ([CVE-2023-21835](https://access.redhat.com/security/cve/CVE-2023-21835), [CVE-2023-21843](https://access.redhat.com/security/cve/CVE-2023-21843))
java-11-openjdk-slowdebug | 11.0.18.0.10-2.el9_1 | [RHSA-2023:0202](https://access.redhat.com/errata/RHSA-2023:0202) | <div class="adv_s">Security Advisory</div> ([CVE-2023-21835](https://access.redhat.com/security/cve/CVE-2023-21835), [CVE-2023-21843](https://access.redhat.com/security/cve/CVE-2023-21843))
java-11-openjdk-slowdebug-debuginfo | 11.0.18.0.10-2.el9_1 | |
java-11-openjdk-src-fastdebug | 11.0.18.0.10-2.el9_1 | [RHSA-2023:0202](https://access.redhat.com/errata/RHSA-2023:0202) | <div class="adv_s">Security Advisory</div> ([CVE-2023-21835](https://access.redhat.com/security/cve/CVE-2023-21835), [CVE-2023-21843](https://access.redhat.com/security/cve/CVE-2023-21843))
java-11-openjdk-src-slowdebug | 11.0.18.0.10-2.el9_1 | [RHSA-2023:0202](https://access.redhat.com/errata/RHSA-2023:0202) | <div class="adv_s">Security Advisory</div> ([CVE-2023-21835](https://access.redhat.com/security/cve/CVE-2023-21835), [CVE-2023-21843](https://access.redhat.com/security/cve/CVE-2023-21843))
java-11-openjdk-static-libs-fastdebug | 11.0.18.0.10-2.el9_1 | [RHSA-2023:0202](https://access.redhat.com/errata/RHSA-2023:0202) | <div class="adv_s">Security Advisory</div> ([CVE-2023-21835](https://access.redhat.com/security/cve/CVE-2023-21835), [CVE-2023-21843](https://access.redhat.com/security/cve/CVE-2023-21843))
java-11-openjdk-static-libs-slowdebug | 11.0.18.0.10-2.el9_1 | [RHSA-2023:0202](https://access.redhat.com/errata/RHSA-2023:0202) | <div class="adv_s">Security Advisory</div> ([CVE-2023-21835](https://access.redhat.com/security/cve/CVE-2023-21835), [CVE-2023-21843](https://access.redhat.com/security/cve/CVE-2023-21843))

### CERN aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
cern-get-keytab | 1.2.1-1.al9.cern | |
oracle-release | 1.5-2.al9.cern | |

### openafs aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
dkms-openafs | 1.8.9.0-1.al9.cern | |
kmod-openafs | 1.8.9.0-1.5.14.0_162.6.1.el9_1.al9.cern | |
openafs | 1.8.9.0-1.al9.cern | |
openafs-authlibs | 1.8.9.0-1.al9.cern | |
openafs-authlibs-devel | 1.8.9.0-1.al9.cern | |
openafs-client | 1.8.9.0-1.al9.cern | |
openafs-compat | 1.8.9.0-1.al9.cern | |
openafs-debugsource | 1.8.9.0-1.al9.cern | |
openafs-debugsource | 1.8.9.0_5.14.0_162.6.1.el9_1-1.al9.cern | |
openafs-devel | 1.8.9.0-1.al9.cern | |
openafs-docs | 1.8.9.0-1.al9.cern | |
openafs-kernel-source | 1.8.9.0-1.al9.cern | |
openafs-krb5 | 1.8.9.0-1.al9.cern | |
openafs-server | 1.8.9.0-1.al9.cern | |

### AppStream aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
java-11-openjdk | 11.0.18.0.10-2.el9_1 | [RHSA-2023:0202](https://access.redhat.com/errata/RHSA-2023:0202) | <div class="adv_s">Security Advisory</div> ([CVE-2023-21835](https://access.redhat.com/security/cve/CVE-2023-21835), [CVE-2023-21843](https://access.redhat.com/security/cve/CVE-2023-21843))
java-11-openjdk-debuginfo | 11.0.18.0.10-2.el9_1 | |
java-11-openjdk-debugsource | 11.0.18.0.10-2.el9_1 | |
java-11-openjdk-demo | 11.0.18.0.10-2.el9_1 | [RHSA-2023:0202](https://access.redhat.com/errata/RHSA-2023:0202) | <div class="adv_s">Security Advisory</div> ([CVE-2023-21835](https://access.redhat.com/security/cve/CVE-2023-21835), [CVE-2023-21843](https://access.redhat.com/security/cve/CVE-2023-21843))
java-11-openjdk-devel | 11.0.18.0.10-2.el9_1 | [RHSA-2023:0202](https://access.redhat.com/errata/RHSA-2023:0202) | <div class="adv_s">Security Advisory</div> ([CVE-2023-21835](https://access.redhat.com/security/cve/CVE-2023-21835), [CVE-2023-21843](https://access.redhat.com/security/cve/CVE-2023-21843))
java-11-openjdk-devel-debuginfo | 11.0.18.0.10-2.el9_1 | |
java-11-openjdk-headless | 11.0.18.0.10-2.el9_1 | [RHSA-2023:0202](https://access.redhat.com/errata/RHSA-2023:0202) | <div class="adv_s">Security Advisory</div> ([CVE-2023-21835](https://access.redhat.com/security/cve/CVE-2023-21835), [CVE-2023-21843](https://access.redhat.com/security/cve/CVE-2023-21843))
java-11-openjdk-headless-debuginfo | 11.0.18.0.10-2.el9_1 | |
java-11-openjdk-javadoc | 11.0.18.0.10-2.el9_1 | [RHSA-2023:0202](https://access.redhat.com/errata/RHSA-2023:0202) | <div class="adv_s">Security Advisory</div> ([CVE-2023-21835](https://access.redhat.com/security/cve/CVE-2023-21835), [CVE-2023-21843](https://access.redhat.com/security/cve/CVE-2023-21843))
java-11-openjdk-javadoc-zip | 11.0.18.0.10-2.el9_1 | [RHSA-2023:0202](https://access.redhat.com/errata/RHSA-2023:0202) | <div class="adv_s">Security Advisory</div> ([CVE-2023-21835](https://access.redhat.com/security/cve/CVE-2023-21835), [CVE-2023-21843](https://access.redhat.com/security/cve/CVE-2023-21843))
java-11-openjdk-jmods | 11.0.18.0.10-2.el9_1 | [RHSA-2023:0202](https://access.redhat.com/errata/RHSA-2023:0202) | <div class="adv_s">Security Advisory</div> ([CVE-2023-21835](https://access.redhat.com/security/cve/CVE-2023-21835), [CVE-2023-21843](https://access.redhat.com/security/cve/CVE-2023-21843))
java-11-openjdk-src | 11.0.18.0.10-2.el9_1 | [RHSA-2023:0202](https://access.redhat.com/errata/RHSA-2023:0202) | <div class="adv_s">Security Advisory</div> ([CVE-2023-21835](https://access.redhat.com/security/cve/CVE-2023-21835), [CVE-2023-21843](https://access.redhat.com/security/cve/CVE-2023-21843))
java-11-openjdk-static-libs | 11.0.18.0.10-2.el9_1 | [RHSA-2023:0202](https://access.redhat.com/errata/RHSA-2023:0202) | <div class="adv_s">Security Advisory</div> ([CVE-2023-21835](https://access.redhat.com/security/cve/CVE-2023-21835), [CVE-2023-21843](https://access.redhat.com/security/cve/CVE-2023-21843))

### CRB aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
java-11-openjdk-debuginfo | 11.0.18.0.10-2.el9_1 | |
java-11-openjdk-debugsource | 11.0.18.0.10-2.el9_1 | |
java-11-openjdk-demo-fastdebug | 11.0.18.0.10-2.el9_1 | [RHSA-2023:0202](https://access.redhat.com/errata/RHSA-2023:0202) | <div class="adv_s">Security Advisory</div> ([CVE-2023-21835](https://access.redhat.com/security/cve/CVE-2023-21835), [CVE-2023-21843](https://access.redhat.com/security/cve/CVE-2023-21843))
java-11-openjdk-demo-slowdebug | 11.0.18.0.10-2.el9_1 | [RHSA-2023:0202](https://access.redhat.com/errata/RHSA-2023:0202) | <div class="adv_s">Security Advisory</div> ([CVE-2023-21835](https://access.redhat.com/security/cve/CVE-2023-21835), [CVE-2023-21843](https://access.redhat.com/security/cve/CVE-2023-21843))
java-11-openjdk-devel-debuginfo | 11.0.18.0.10-2.el9_1 | |
java-11-openjdk-devel-fastdebug | 11.0.18.0.10-2.el9_1 | [RHSA-2023:0202](https://access.redhat.com/errata/RHSA-2023:0202) | <div class="adv_s">Security Advisory</div> ([CVE-2023-21835](https://access.redhat.com/security/cve/CVE-2023-21835), [CVE-2023-21843](https://access.redhat.com/security/cve/CVE-2023-21843))
java-11-openjdk-devel-fastdebug-debuginfo | 11.0.18.0.10-2.el9_1 | |
java-11-openjdk-devel-slowdebug | 11.0.18.0.10-2.el9_1 | [RHSA-2023:0202](https://access.redhat.com/errata/RHSA-2023:0202) | <div class="adv_s">Security Advisory</div> ([CVE-2023-21835](https://access.redhat.com/security/cve/CVE-2023-21835), [CVE-2023-21843](https://access.redhat.com/security/cve/CVE-2023-21843))
java-11-openjdk-devel-slowdebug-debuginfo | 11.0.18.0.10-2.el9_1 | |
java-11-openjdk-fastdebug | 11.0.18.0.10-2.el9_1 | [RHSA-2023:0202](https://access.redhat.com/errata/RHSA-2023:0202) | <div class="adv_s">Security Advisory</div> ([CVE-2023-21835](https://access.redhat.com/security/cve/CVE-2023-21835), [CVE-2023-21843](https://access.redhat.com/security/cve/CVE-2023-21843))
java-11-openjdk-fastdebug-debuginfo | 11.0.18.0.10-2.el9_1 | |
java-11-openjdk-headless-debuginfo | 11.0.18.0.10-2.el9_1 | |
java-11-openjdk-headless-fastdebug | 11.0.18.0.10-2.el9_1 | [RHSA-2023:0202](https://access.redhat.com/errata/RHSA-2023:0202) | <div class="adv_s">Security Advisory</div> ([CVE-2023-21835](https://access.redhat.com/security/cve/CVE-2023-21835), [CVE-2023-21843](https://access.redhat.com/security/cve/CVE-2023-21843))
java-11-openjdk-headless-fastdebug-debuginfo | 11.0.18.0.10-2.el9_1 | |
java-11-openjdk-headless-slowdebug | 11.0.18.0.10-2.el9_1 | [RHSA-2023:0202](https://access.redhat.com/errata/RHSA-2023:0202) | <div class="adv_s">Security Advisory</div> ([CVE-2023-21835](https://access.redhat.com/security/cve/CVE-2023-21835), [CVE-2023-21843](https://access.redhat.com/security/cve/CVE-2023-21843))
java-11-openjdk-headless-slowdebug-debuginfo | 11.0.18.0.10-2.el9_1 | |
java-11-openjdk-jmods-fastdebug | 11.0.18.0.10-2.el9_1 | [RHSA-2023:0202](https://access.redhat.com/errata/RHSA-2023:0202) | <div class="adv_s">Security Advisory</div> ([CVE-2023-21835](https://access.redhat.com/security/cve/CVE-2023-21835), [CVE-2023-21843](https://access.redhat.com/security/cve/CVE-2023-21843))
java-11-openjdk-jmods-slowdebug | 11.0.18.0.10-2.el9_1 | [RHSA-2023:0202](https://access.redhat.com/errata/RHSA-2023:0202) | <div class="adv_s">Security Advisory</div> ([CVE-2023-21835](https://access.redhat.com/security/cve/CVE-2023-21835), [CVE-2023-21843](https://access.redhat.com/security/cve/CVE-2023-21843))
java-11-openjdk-slowdebug | 11.0.18.0.10-2.el9_1 | [RHSA-2023:0202](https://access.redhat.com/errata/RHSA-2023:0202) | <div class="adv_s">Security Advisory</div> ([CVE-2023-21835](https://access.redhat.com/security/cve/CVE-2023-21835), [CVE-2023-21843](https://access.redhat.com/security/cve/CVE-2023-21843))
java-11-openjdk-slowdebug-debuginfo | 11.0.18.0.10-2.el9_1 | |
java-11-openjdk-src-fastdebug | 11.0.18.0.10-2.el9_1 | [RHSA-2023:0202](https://access.redhat.com/errata/RHSA-2023:0202) | <div class="adv_s">Security Advisory</div> ([CVE-2023-21835](https://access.redhat.com/security/cve/CVE-2023-21835), [CVE-2023-21843](https://access.redhat.com/security/cve/CVE-2023-21843))
java-11-openjdk-src-slowdebug | 11.0.18.0.10-2.el9_1 | [RHSA-2023:0202](https://access.redhat.com/errata/RHSA-2023:0202) | <div class="adv_s">Security Advisory</div> ([CVE-2023-21835](https://access.redhat.com/security/cve/CVE-2023-21835), [CVE-2023-21843](https://access.redhat.com/security/cve/CVE-2023-21843))
java-11-openjdk-static-libs-fastdebug | 11.0.18.0.10-2.el9_1 | [RHSA-2023:0202](https://access.redhat.com/errata/RHSA-2023:0202) | <div class="adv_s">Security Advisory</div> ([CVE-2023-21835](https://access.redhat.com/security/cve/CVE-2023-21835), [CVE-2023-21843](https://access.redhat.com/security/cve/CVE-2023-21843))
java-11-openjdk-static-libs-slowdebug | 11.0.18.0.10-2.el9_1 | [RHSA-2023:0202](https://access.redhat.com/errata/RHSA-2023:0202) | <div class="adv_s">Security Advisory</div> ([CVE-2023-21835](https://access.redhat.com/security/cve/CVE-2023-21835), [CVE-2023-21843](https://access.redhat.com/security/cve/CVE-2023-21843))

