## 2023-08-02

### RT x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
kernel-rt-core | 4.18.0-477.15.1.rt7.278.el8_8 | [ALSA-2023:3819](https://errata.almalinux.org/8/ALSA-2023-3819.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-28466](https://access.redhat.com/security/cve/CVE-2023-28466))

### NFV x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
kernel-rt-core | 4.18.0-477.15.1.rt7.278.el8_8 | [ALSA-2023:3819](https://errata.almalinux.org/8/ALSA-2023-3819.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-28466](https://access.redhat.com/security/cve/CVE-2023-28466))

