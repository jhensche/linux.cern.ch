## 2023-06-05

### CERN x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
CERN-CA-certs | 20230604-1.al8.cern | |

### AppStream x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
cups-filters | 1.20.0-29.el8_8.2 | |
cups-filters-debuginfo | 1.20.0-29.el8_8.2 | |
cups-filters-debugsource | 1.20.0-29.el8_8.2 | |
cups-filters-libs | 1.20.0-29.el8_8.2 | |
cups-filters-libs-debuginfo | 1.20.0-29.el8_8.2 | |

### PowerTools x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
cups-filters-debuginfo | 1.20.0-29.el8_8.2 | |
cups-filters-debugsource | 1.20.0-29.el8_8.2 | |
cups-filters-devel | 1.20.0-29.el8_8.2 | |
cups-filters-libs-debuginfo | 1.20.0-29.el8_8.2 | |

### CERN aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
CERN-CA-certs | 20230604-1.al8.cern | |

### AppStream aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
cups-filters | 1.20.0-29.el8_8.2 | |
cups-filters-debuginfo | 1.20.0-29.el8_8.2 | |
cups-filters-debugsource | 1.20.0-29.el8_8.2 | |
cups-filters-libs | 1.20.0-29.el8_8.2 | |
cups-filters-libs-debuginfo | 1.20.0-29.el8_8.2 | |

### PowerTools aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
cups-filters-debuginfo | 1.20.0-29.el8_8.2 | |
cups-filters-debugsource | 1.20.0-29.el8_8.2 | |
cups-filters-devel | 1.20.0-29.el8_8.2 | |
cups-filters-libs-debuginfo | 1.20.0-29.el8_8.2 | |

