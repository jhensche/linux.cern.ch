## 2022-12-01

### CERN x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
cern-anaconda-addon | 1.9-2.al8.cern | |
cern-yum-tool | 1.9-1.al8.cern | |
locmap-firstboot | 2.1-5.al8.cern | |
openafs-release | 1.4-1.al8.cern | |

### CERN aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
cern-anaconda-addon | 1.9-2.al8.cern | |
cern-yum-tool | 1.9-1.al8.cern | |
locmap-firstboot | 2.1-5.al8.cern | |
openafs-release | 1.4-1.al8.cern | |

