## 2023-10-20

### CERN x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
cern-get-keytab | 1.5.10-1.al8.cern | |

### BaseOS x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
bpftool-debuginfo | 4.18.0-477.27.1.el8_8 | |
cups-client-debuginfo | 2.2.6-51.el8_8.2 | |
cups-debuginfo | 2.2.6-51.el8_8.2 | |
cups-debugsource | 2.2.6-51.el8_8.2 | |
cups-ipptool-debuginfo | 2.2.6-51.el8_8.2 | |
cups-libs-debuginfo | 2.2.6-51.el8_8.2 | |
cups-lpd-debuginfo | 2.2.6-51.el8_8.2 | |
dmidecode-debuginfo | 3.3-4.el8_8.1 | |
dmidecode-debugsource | 3.3-4.el8_8.1 | |
findutils-debuginfo | 4.6.0-20.el8_8.1 | |
findutils-debugsource | 4.6.0-20.el8_8.1 | |
iptables-debuginfo | 1.8.4-24.el8_8.2 | |
iptables-debugsource | 1.8.4-24.el8_8.2 | |
iptables-libs-debuginfo | 1.8.4-24.el8_8.2 | |
iptables-utils-debuginfo | 1.8.4-24.el8_8.2 | |
kernel-debug-debuginfo | 4.18.0-477.27.1.el8_8 | |
kernel-debuginfo | 4.18.0-477.27.1.el8_8 | |
kernel-debuginfo-common-x86_64 | 4.18.0-477.27.1.el8_8 | |
kernel-tools-debuginfo | 4.18.0-477.27.1.el8_8 | |
libnghttp2 | 1.33.0-5.el8_8 | |
libnghttp2-debuginfo | 1.33.0-5.el8_8 | |
ncurses-c++-libs-debuginfo | 6.1-9.20180224.el8_8.1 | |
ncurses-compat-libs-debuginfo | 6.1-9.20180224.el8_8.1 | |
ncurses-debuginfo | 6.1-9.20180224.el8_8.1 | |
ncurses-debugsource | 6.1-9.20180224.el8_8.1 | |
ncurses-libs-debuginfo | 6.1-9.20180224.el8_8.1 | |
nghttp2-debuginfo | 1.33.0-5.el8_8 | |
nghttp2-debugsource | 1.33.0-5.el8_8 | |
perf-debuginfo | 4.18.0-477.27.1.el8_8 | |
python3-perf-debuginfo | 4.18.0-477.27.1.el8_8 | |
systemd-container-debuginfo | 239-74.el8_8.5 | |
systemd-debuginfo | 239-74.el8_8.5 | |
systemd-debugsource | 239-74.el8_8.5 | |
systemd-journal-remote-debuginfo | 239-74.el8_8.5 | |
systemd-libs-debuginfo | 239-74.el8_8.5 | |
systemd-pam-debuginfo | 239-74.el8_8.5 | |
systemd-tests-debuginfo | 239-74.el8_8.5 | |
systemd-udev-debuginfo | 239-74.el8_8.5 | |

### AppStream x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
389-ds-base-debuginfo | 1.4.3.35-2.module_el8.8.0+3606+2967cd72.alma.1 | |
389-ds-base-debugsource | 1.4.3.35-2.module_el8.8.0+3606+2967cd72.alma.1 | |
389-ds-base-legacy-tools-debuginfo | 1.4.3.35-2.module_el8.8.0+3606+2967cd72.alma.1 | |
389-ds-base-libs-debuginfo | 1.4.3.35-2.module_el8.8.0+3606+2967cd72.alma.1 | |
389-ds-base-snmp-debuginfo | 1.4.3.35-2.module_el8.8.0+3606+2967cd72.alma.1 | |
firefox-debuginfo | 102.15.1-1.el8_8.alma | |
firefox-debugsource | 102.15.1-1.el8_8.alma | |
frr-debuginfo | 7.5.1-7.el8_8.2.alma.1 | |
frr-debugsource | 7.5.1-7.el8_8.2.alma.1 | |
galera-debuginfo | 25.3.37-1.module_el8.8.0+3609+204d4ab0 | |
galera-debugsource | 25.3.37-1.module_el8.8.0+3609+204d4ab0 | |
grafana | 7.5.15-5.el8_8.alma.1 | |
grafana-debuginfo | 7.5.15-5.el8_8.alma.1 | |
ipa-client-debuginfo | 4.9.11-7.module_el8.8.0+3611+265d7112.alma.1 | |
ipa-debuginfo | 4.9.11-7.module_el8.8.0+3611+265d7112.alma.1 | |
ipa-debugsource | 4.9.11-7.module_el8.8.0+3611+265d7112.alma.1 | |
ipa-server-debuginfo | 4.9.11-7.module_el8.8.0+3611+265d7112.alma.1 | |
ipa-server-trust-ad-debuginfo | 4.9.11-7.module_el8.8.0+3611+265d7112.alma.1 | |
libwebp-debuginfo | 1.0.0-8.el8_8.1 | |
libwebp-debugsource | 1.0.0-8.el8_8.1 | |
libwebp-java-debuginfo | 1.0.0-8.el8_8.1 | |
libwebp-tools-debuginfo | 1.0.0-8.el8_8.1 | |
linuxptp-debuginfo | 3.1.1-3.el8_8.2.alma.1 | |
linuxptp-debugsource | 3.1.1-3.el8_8.2.alma.1 | |
mariadb-backup-debuginfo | 10.3.39-1.module_el8.8.0+3609+204d4ab0 | |
mariadb-debuginfo | 10.3.39-1.module_el8.8.0+3609+204d4ab0 | |
mariadb-debugsource | 10.3.39-1.module_el8.8.0+3609+204d4ab0 | |
mariadb-embedded-debuginfo | 10.3.39-1.module_el8.8.0+3609+204d4ab0 | |
mariadb-gssapi-server-debuginfo | 10.3.39-1.module_el8.8.0+3609+204d4ab0 | |
mariadb-oqgraph-engine-debuginfo | 10.3.39-1.module_el8.8.0+3609+204d4ab0 | |
mariadb-server-debuginfo | 10.3.39-1.module_el8.8.0+3609+204d4ab0 | |
mariadb-server-utils-debuginfo | 10.3.39-1.module_el8.8.0+3609+204d4ab0 | |
mariadb-test-debuginfo | 10.3.39-1.module_el8.8.0+3609+204d4ab0 | |
nodejs | 16.20.2-3.module_el8.8.0+3653+c77a731f | |
nodejs | 18.18.2-1.module_el8.8.0+3652+0e111ba0 | |
nodejs-debuginfo | 16.20.2-3.module_el8.8.0+3653+c77a731f | |
nodejs-debuginfo | 18.18.2-1.module_el8.8.0+3652+0e111ba0 | |
nodejs-debugsource | 16.20.2-3.module_el8.8.0+3653+c77a731f | |
nodejs-debugsource | 18.18.2-1.module_el8.8.0+3652+0e111ba0 | |
nodejs-devel | 16.20.2-3.module_el8.8.0+3653+c77a731f | |
nodejs-devel | 18.18.2-1.module_el8.8.0+3652+0e111ba0 | |
nodejs-docs | 16.20.2-3.module_el8.8.0+3653+c77a731f | |
nodejs-docs | 18.18.2-1.module_el8.8.0+3652+0e111ba0 | |
nodejs-full-i18n | 16.20.2-3.module_el8.8.0+3653+c77a731f | |
nodejs-full-i18n | 18.18.2-1.module_el8.8.0+3652+0e111ba0 | |
npm | 8.19.4-1.16.20.2.3.module_el8.8.0+3653+c77a731f | |
npm | 9.8.1-1.18.18.2.1.module_el8.8.0+3652+0e111ba0 | |
nspr-debuginfo | 4.35.0-1.el8_8 | |
nspr-debugsource | 4.35.0-1.el8_8 | |
nss-debuginfo | 3.90.0-3.el8_8 | |
nss-debugsource | 3.90.0-3.el8_8 | |
nss-softokn-debuginfo | 3.90.0-3.el8_8 | |
nss-softokn-freebl-debuginfo | 3.90.0-3.el8_8 | |
nss-sysinit-debuginfo | 3.90.0-3.el8_8 | |
nss-tools-debuginfo | 3.90.0-3.el8_8 | |
nss-util-debuginfo | 3.90.0-3.el8_8 | |
open-vm-tools-debuginfo | 12.1.5-2.el8_8.2.alma.1 | |
open-vm-tools-debuginfo | 12.1.5-2.el8_8.3.alma.1 | |
open-vm-tools-debugsource | 12.1.5-2.el8_8.2.alma.1 | |
open-vm-tools-debugsource | 12.1.5-2.el8_8.3.alma.1 | |
open-vm-tools-desktop-debuginfo | 12.1.5-2.el8_8.2.alma.1 | |
open-vm-tools-desktop-debuginfo | 12.1.5-2.el8_8.3.alma.1 | |
open-vm-tools-sdmp-debuginfo | 12.1.5-2.el8_8.2.alma.1 | |
open-vm-tools-sdmp-debuginfo | 12.1.5-2.el8_8.3.alma.1 | |
openscap-debuginfo | 1.3.8-1.el8_8 | |
openscap-debugsource | 1.3.8-1.el8_8 | |
openscap-engine-sce-debuginfo | 1.3.8-1.el8_8 | |
openscap-python3-debuginfo | 1.3.8-1.el8_8 | |
openscap-scanner-debuginfo | 1.3.8-1.el8_8 | |
ostree-debuginfo | 2022.2-7.el8_8 | |
ostree-debugsource | 2022.2-7.el8_8 | |
ostree-libs-debuginfo | 2022.2-7.el8_8 | |
pacemaker-cli-debuginfo | 2.1.5-9.3.el8_8.alma.1 | |
pacemaker-cluster-libs-debuginfo | 2.1.5-9.3.el8_8.alma.1 | |
pacemaker-debuginfo | 2.1.5-9.3.el8_8.alma.1 | |
pacemaker-debugsource | 2.1.5-9.3.el8_8.alma.1 | |
pacemaker-libs-debuginfo | 2.1.5-9.3.el8_8.alma.1 | |
pacemaker-remote-debuginfo | 2.1.5-9.3.el8_8.alma.1 | |
podman-catatonit-debuginfo | 4.4.1-15.module_el8.8.0+3607+3f1b8f9a | |
podman-debuginfo | 4.4.1-15.module_el8.8.0+3607+3f1b8f9a | |
podman-debugsource | 4.4.1-15.module_el8.8.0+3607+3f1b8f9a | |
podman-gvproxy-debuginfo | 4.4.1-15.module_el8.8.0+3607+3f1b8f9a | |
podman-plugins-debuginfo | 4.4.1-15.module_el8.8.0+3607+3f1b8f9a | |
podman-remote-debuginfo | 4.4.1-15.module_el8.8.0+3607+3f1b8f9a | |
postgresql-contrib-debuginfo | 15.3-1.module_el8.8.0+3610+f1fe5820 | |
postgresql-debuginfo | 15.3-1.module_el8.8.0+3610+f1fe5820 | |
postgresql-debugsource | 15.3-1.module_el8.8.0+3610+f1fe5820 | |
postgresql-docs-debuginfo | 15.3-1.module_el8.8.0+3610+f1fe5820 | |
postgresql-plperl-debuginfo | 15.3-1.module_el8.8.0+3610+f1fe5820 | |
postgresql-plpython3-debuginfo | 15.3-1.module_el8.8.0+3610+f1fe5820 | |
postgresql-pltcl-debuginfo | 15.3-1.module_el8.8.0+3610+f1fe5820 | |
postgresql-private-libs-debuginfo | 15.3-1.module_el8.8.0+3610+f1fe5820 | |
postgresql-server-debuginfo | 15.3-1.module_el8.8.0+3610+f1fe5820 | |
postgresql-server-devel-debuginfo | 15.3-1.module_el8.8.0+3610+f1fe5820 | |
postgresql-test-debuginfo | 15.3-1.module_el8.8.0+3610+f1fe5820 | |
postgresql-upgrade-debuginfo | 15.3-1.module_el8.8.0+3610+f1fe5820 | |
postgresql-upgrade-devel-debuginfo | 15.3-1.module_el8.8.0+3610+f1fe5820 | |
python-reportlab-debugsource | 3.4.0-8.el8_8.1.alma.1 | |
python3-reportlab | 3.4.0-8.el8_8.1.alma.1 | |
python3-reportlab-debuginfo | 3.4.0-8.el8_8.1.alma.1 | |
qemu-guest-agent-debuginfo | 6.2.0-33.module_el8.8.0+3612+f18d2b89.alma.1 | |
qemu-img-debuginfo | 6.2.0-33.module_el8.8.0+3612+f18d2b89.alma.1 | |
qemu-kvm-block-curl-debuginfo | 6.2.0-33.module_el8.8.0+3612+f18d2b89.alma.1 | |
qemu-kvm-block-gluster-debuginfo | 6.2.0-33.module_el8.8.0+3612+f18d2b89.alma.1 | |
qemu-kvm-block-iscsi-debuginfo | 6.2.0-33.module_el8.8.0+3612+f18d2b89.alma.1 | |
qemu-kvm-block-rbd-debuginfo | 6.2.0-33.module_el8.8.0+3612+f18d2b89.alma.1 | |
qemu-kvm-block-ssh-debuginfo | 6.2.0-33.module_el8.8.0+3612+f18d2b89.alma.1 | |
qemu-kvm-common-debuginfo | 6.2.0-33.module_el8.8.0+3612+f18d2b89.alma.1 | |
qemu-kvm-core-debuginfo | 6.2.0-33.module_el8.8.0+3612+f18d2b89.alma.1 | |
qemu-kvm-debuginfo | 6.2.0-33.module_el8.8.0+3612+f18d2b89.alma.1 | |
qemu-kvm-debugsource | 6.2.0-33.module_el8.8.0+3612+f18d2b89.alma.1 | |
qemu-kvm-hw-usbredir-debuginfo | 6.2.0-33.module_el8.8.0+3612+f18d2b89.alma.1 | |
qemu-kvm-ui-opengl-debuginfo | 6.2.0-33.module_el8.8.0+3612+f18d2b89.alma.1 | |
qemu-kvm-ui-spice-debuginfo | 6.2.0-33.module_el8.8.0+3612+f18d2b89.alma.1 | |
thunderbird-debuginfo | 102.15.1-1.el8_8.alma | |
thunderbird-debugsource | 102.15.1-1.el8_8.alma | |

### PowerTools x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
libnghttp2-devel | 1.33.0-5.el8_8 | |
nghttp2 | 1.33.0-5.el8_8 | |

### NFV x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
kernel-rt-debug-debuginfo | 4.18.0-477.27.1.rt7.290.el8_8 | |

### devel x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
python-reportlab-doc | 3.4.0-8.el8_8.1.alma.1 | |

### CERN aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
cern-get-keytab | 1.5.10-1.al8.cern | |

### BaseOS aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
bpftool-debuginfo | 4.18.0-477.27.1.el8_8 | |
cups-client-debuginfo | 2.2.6-51.el8_8.2 | |
cups-debuginfo | 2.2.6-51.el8_8.2 | |
cups-debugsource | 2.2.6-51.el8_8.2 | |
cups-ipptool-debuginfo | 2.2.6-51.el8_8.2 | |
cups-libs-debuginfo | 2.2.6-51.el8_8.2 | |
cups-lpd-debuginfo | 2.2.6-51.el8_8.2 | |
dmidecode-debuginfo | 3.3-4.el8_8.1 | |
dmidecode-debugsource | 3.3-4.el8_8.1 | |
findutils-debuginfo | 4.6.0-20.el8_8.1 | |
findutils-debugsource | 4.6.0-20.el8_8.1 | |
iptables-debuginfo | 1.8.4-24.el8_8.2 | |
iptables-debugsource | 1.8.4-24.el8_8.2 | |
iptables-libs-debuginfo | 1.8.4-24.el8_8.2 | |
iptables-utils-debuginfo | 1.8.4-24.el8_8.2 | |
kernel-debug-debuginfo | 4.18.0-477.27.1.el8_8 | |
kernel-debuginfo | 4.18.0-477.27.1.el8_8 | |
kernel-debuginfo-common-aarch64 | 4.18.0-477.27.1.el8_8 | |
kernel-tools-debuginfo | 4.18.0-477.27.1.el8_8 | |
libnghttp2 | 1.33.0-5.el8_8 | |
libnghttp2-debuginfo | 1.33.0-5.el8_8 | |
ncurses-c++-libs-debuginfo | 6.1-9.20180224.el8_8.1 | |
ncurses-compat-libs-debuginfo | 6.1-9.20180224.el8_8.1 | |
ncurses-debuginfo | 6.1-9.20180224.el8_8.1 | |
ncurses-debugsource | 6.1-9.20180224.el8_8.1 | |
ncurses-libs-debuginfo | 6.1-9.20180224.el8_8.1 | |
nghttp2-debuginfo | 1.33.0-5.el8_8 | |
nghttp2-debugsource | 1.33.0-5.el8_8 | |
perf-debuginfo | 4.18.0-477.27.1.el8_8 | |
python3-perf-debuginfo | 4.18.0-477.27.1.el8_8 | |
systemd-container-debuginfo | 239-74.el8_8.5 | |
systemd-debuginfo | 239-74.el8_8.5 | |
systemd-debugsource | 239-74.el8_8.5 | |
systemd-journal-remote-debuginfo | 239-74.el8_8.5 | |
systemd-libs-debuginfo | 239-74.el8_8.5 | |
systemd-pam-debuginfo | 239-74.el8_8.5 | |
systemd-tests-debuginfo | 239-74.el8_8.5 | |
systemd-udev-debuginfo | 239-74.el8_8.5 | |

### AppStream aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
389-ds-base-debuginfo | 1.4.3.35-2.module_el8.8.0+3606+2967cd72.alma.1 | |
389-ds-base-debugsource | 1.4.3.35-2.module_el8.8.0+3606+2967cd72.alma.1 | |
389-ds-base-legacy-tools-debuginfo | 1.4.3.35-2.module_el8.8.0+3606+2967cd72.alma.1 | |
389-ds-base-libs-debuginfo | 1.4.3.35-2.module_el8.8.0+3606+2967cd72.alma.1 | |
389-ds-base-snmp-debuginfo | 1.4.3.35-2.module_el8.8.0+3606+2967cd72.alma.1 | |
firefox-debuginfo | 102.15.1-1.el8_8.alma | |
firefox-debugsource | 102.15.1-1.el8_8.alma | |
frr-debuginfo | 7.5.1-7.el8_8.2.alma.1 | |
frr-debugsource | 7.5.1-7.el8_8.2.alma.1 | |
galera-debuginfo | 25.3.37-1.module_el8.8.0+3609+204d4ab0 | |
galera-debugsource | 25.3.37-1.module_el8.8.0+3609+204d4ab0 | |
grafana | 7.5.15-5.el8_8.alma.1 | |
grafana-debuginfo | 7.5.15-5.el8_8.alma.1 | |
ipa-client-debuginfo | 4.9.11-7.module_el8.8.0+3611+265d7112.alma.1 | |
ipa-debuginfo | 4.9.11-7.module_el8.8.0+3611+265d7112.alma.1 | |
ipa-debugsource | 4.9.11-7.module_el8.8.0+3611+265d7112.alma.1 | |
ipa-server-debuginfo | 4.9.11-7.module_el8.8.0+3611+265d7112.alma.1 | |
ipa-server-trust-ad-debuginfo | 4.9.11-7.module_el8.8.0+3611+265d7112.alma.1 | |
libwebp-debuginfo | 1.0.0-8.el8_8.1 | |
libwebp-debugsource | 1.0.0-8.el8_8.1 | |
libwebp-java-debuginfo | 1.0.0-8.el8_8.1 | |
libwebp-tools-debuginfo | 1.0.0-8.el8_8.1 | |
linuxptp-debuginfo | 3.1.1-3.el8_8.2.alma.1 | |
linuxptp-debugsource | 3.1.1-3.el8_8.2.alma.1 | |
mariadb-backup-debuginfo | 10.3.39-1.module_el8.8.0+3609+204d4ab0 | |
mariadb-debuginfo | 10.3.39-1.module_el8.8.0+3609+204d4ab0 | |
mariadb-debugsource | 10.3.39-1.module_el8.8.0+3609+204d4ab0 | |
mariadb-embedded-debuginfo | 10.3.39-1.module_el8.8.0+3609+204d4ab0 | |
mariadb-gssapi-server-debuginfo | 10.3.39-1.module_el8.8.0+3609+204d4ab0 | |
mariadb-oqgraph-engine-debuginfo | 10.3.39-1.module_el8.8.0+3609+204d4ab0 | |
mariadb-server-debuginfo | 10.3.39-1.module_el8.8.0+3609+204d4ab0 | |
mariadb-server-utils-debuginfo | 10.3.39-1.module_el8.8.0+3609+204d4ab0 | |
mariadb-test-debuginfo | 10.3.39-1.module_el8.8.0+3609+204d4ab0 | |
nodejs | 16.20.2-3.module_el8.8.0+3653+c77a731f | |
nodejs | 18.18.2-1.module_el8.8.0+3652+0e111ba0 | |
nodejs-debuginfo | 16.20.2-3.module_el8.8.0+3653+c77a731f | |
nodejs-debuginfo | 18.18.2-1.module_el8.8.0+3652+0e111ba0 | |
nodejs-debugsource | 16.20.2-3.module_el8.8.0+3653+c77a731f | |
nodejs-debugsource | 18.18.2-1.module_el8.8.0+3652+0e111ba0 | |
nodejs-devel | 16.20.2-3.module_el8.8.0+3653+c77a731f | |
nodejs-devel | 18.18.2-1.module_el8.8.0+3652+0e111ba0 | |
nodejs-docs | 16.20.2-3.module_el8.8.0+3653+c77a731f | |
nodejs-docs | 18.18.2-1.module_el8.8.0+3652+0e111ba0 | |
nodejs-full-i18n | 16.20.2-3.module_el8.8.0+3653+c77a731f | |
nodejs-full-i18n | 18.18.2-1.module_el8.8.0+3652+0e111ba0 | |
npm | 8.19.4-1.16.20.2.3.module_el8.8.0+3653+c77a731f | |
npm | 9.8.1-1.18.18.2.1.module_el8.8.0+3652+0e111ba0 | |
nspr-debuginfo | 4.35.0-1.el8_8 | |
nspr-debugsource | 4.35.0-1.el8_8 | |
nss-debuginfo | 3.90.0-3.el8_8 | |
nss-debugsource | 3.90.0-3.el8_8 | |
nss-softokn-debuginfo | 3.90.0-3.el8_8 | |
nss-softokn-freebl-debuginfo | 3.90.0-3.el8_8 | |
nss-sysinit-debuginfo | 3.90.0-3.el8_8 | |
nss-tools-debuginfo | 3.90.0-3.el8_8 | |
nss-util-debuginfo | 3.90.0-3.el8_8 | |
openscap-debuginfo | 1.3.8-1.el8_8 | |
openscap-debugsource | 1.3.8-1.el8_8 | |
openscap-engine-sce-debuginfo | 1.3.8-1.el8_8 | |
openscap-python3-debuginfo | 1.3.8-1.el8_8 | |
openscap-scanner-debuginfo | 1.3.8-1.el8_8 | |
ostree-debuginfo | 2022.2-7.el8_8 | |
ostree-debugsource | 2022.2-7.el8_8 | |
ostree-libs-debuginfo | 2022.2-7.el8_8 | |
pacemaker-cli-debuginfo | 2.1.5-9.3.el8_8.alma.1 | |
pacemaker-cluster-libs-debuginfo | 2.1.5-9.3.el8_8.alma.1 | |
pacemaker-debuginfo | 2.1.5-9.3.el8_8.alma.1 | |
pacemaker-debugsource | 2.1.5-9.3.el8_8.alma.1 | |
pacemaker-libs-debuginfo | 2.1.5-9.3.el8_8.alma.1 | |
pacemaker-remote-debuginfo | 2.1.5-9.3.el8_8.alma.1 | |
podman-catatonit-debuginfo | 4.4.1-15.module_el8.8.0+3607+3f1b8f9a | |
podman-debuginfo | 4.4.1-15.module_el8.8.0+3607+3f1b8f9a | |
podman-debugsource | 4.4.1-15.module_el8.8.0+3607+3f1b8f9a | |
podman-gvproxy-debuginfo | 4.4.1-15.module_el8.8.0+3607+3f1b8f9a | |
podman-plugins-debuginfo | 4.4.1-15.module_el8.8.0+3607+3f1b8f9a | |
podman-remote-debuginfo | 4.4.1-15.module_el8.8.0+3607+3f1b8f9a | |
postgresql-contrib-debuginfo | 15.3-1.module_el8.8.0+3610+f1fe5820 | |
postgresql-debuginfo | 15.3-1.module_el8.8.0+3610+f1fe5820 | |
postgresql-debugsource | 15.3-1.module_el8.8.0+3610+f1fe5820 | |
postgresql-docs-debuginfo | 15.3-1.module_el8.8.0+3610+f1fe5820 | |
postgresql-plperl-debuginfo | 15.3-1.module_el8.8.0+3610+f1fe5820 | |
postgresql-plpython3-debuginfo | 15.3-1.module_el8.8.0+3610+f1fe5820 | |
postgresql-pltcl-debuginfo | 15.3-1.module_el8.8.0+3610+f1fe5820 | |
postgresql-private-libs-debuginfo | 15.3-1.module_el8.8.0+3610+f1fe5820 | |
postgresql-server-debuginfo | 15.3-1.module_el8.8.0+3610+f1fe5820 | |
postgresql-server-devel-debuginfo | 15.3-1.module_el8.8.0+3610+f1fe5820 | |
postgresql-test-debuginfo | 15.3-1.module_el8.8.0+3610+f1fe5820 | |
postgresql-upgrade-debuginfo | 15.3-1.module_el8.8.0+3610+f1fe5820 | |
postgresql-upgrade-devel-debuginfo | 15.3-1.module_el8.8.0+3610+f1fe5820 | |
python-reportlab-debugsource | 3.4.0-8.el8_8.1.alma.1 | |
python3-reportlab | 3.4.0-8.el8_8.1.alma.1 | |
python3-reportlab-debuginfo | 3.4.0-8.el8_8.1.alma.1 | |
qemu-guest-agent-debuginfo | 6.2.0-33.module_el8.8.0+3612+f18d2b89.alma.1 | |
qemu-img-debuginfo | 6.2.0-33.module_el8.8.0+3612+f18d2b89.alma.1 | |
qemu-kvm-block-curl-debuginfo | 6.2.0-33.module_el8.8.0+3612+f18d2b89.alma.1 | |
qemu-kvm-block-iscsi-debuginfo | 6.2.0-33.module_el8.8.0+3612+f18d2b89.alma.1 | |
qemu-kvm-block-rbd-debuginfo | 6.2.0-33.module_el8.8.0+3612+f18d2b89.alma.1 | |
qemu-kvm-block-ssh-debuginfo | 6.2.0-33.module_el8.8.0+3612+f18d2b89.alma.1 | |
qemu-kvm-common-debuginfo | 6.2.0-33.module_el8.8.0+3612+f18d2b89.alma.1 | |
qemu-kvm-core-debuginfo | 6.2.0-33.module_el8.8.0+3612+f18d2b89.alma.1 | |
qemu-kvm-debuginfo | 6.2.0-33.module_el8.8.0+3612+f18d2b89.alma.1 | |
qemu-kvm-debugsource | 6.2.0-33.module_el8.8.0+3612+f18d2b89.alma.1 | |
thunderbird-debuginfo | 102.15.1-1.el8_8.alma | |
thunderbird-debugsource | 102.15.1-1.el8_8.alma | |

### PowerTools aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
libnghttp2-devel | 1.33.0-5.el8_8 | |
nghttp2 | 1.33.0-5.el8_8 | |

### devel aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
python-reportlab-doc | 3.4.0-8.el8_8.1.alma.1 | |

