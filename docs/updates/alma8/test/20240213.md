## 2024-02-13

### AppStream x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
aardvark-dns | 1.7.0-1.module_el8.9.0+3717+81096349 | |
buildah | 1.31.3-3.module_el8.9.0+3717+81096349 | [ALSA-2024:0752](https://errata.almalinux.org/8/ALSA-2024-0752.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-21626](https://access.redhat.com/security/cve/CVE-2024-21626))
buildah-debuginfo | 1.31.3-3.module_el8.9.0+3717+81096349 | |
buildah-debugsource | 1.31.3-3.module_el8.9.0+3717+81096349 | |
buildah-tests | 1.31.3-3.module_el8.9.0+3717+81096349 | [ALSA-2024:0752](https://errata.almalinux.org/8/ALSA-2024-0752.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-21626](https://access.redhat.com/security/cve/CVE-2024-21626))
buildah-tests-debuginfo | 1.31.3-3.module_el8.9.0+3717+81096349 | |
container-selinux | 2.221.0-1.module_el8.9.0+3717+81096349 | |
containernetworking-plugins | 1.3.0-8.module_el8.9.0+3717+81096349 | [ALSA-2024:0752](https://errata.almalinux.org/8/ALSA-2024-0752.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-21626](https://access.redhat.com/security/cve/CVE-2024-21626))
containernetworking-plugins-debuginfo | 1.3.0-8.module_el8.9.0+3717+81096349 | |
containernetworking-plugins-debugsource | 1.3.0-8.module_el8.9.0+3717+81096349 | |
containers-common | 1-71.module_el8.9.0+3717+81096349 | [ALSA-2024:0752](https://errata.almalinux.org/8/ALSA-2024-0752.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-21626](https://access.redhat.com/security/cve/CVE-2024-21626))
crit | 3.18-4.module_el8.9.0+3717+81096349 | |
criu | 3.18-4.module_el8.9.0+3717+81096349 | |
criu-debuginfo | 3.18-4.module_el8.9.0+3717+81096349 | |
criu-debugsource | 3.18-4.module_el8.9.0+3717+81096349 | |
criu-devel | 3.18-4.module_el8.9.0+3717+81096349 | |
criu-libs | 3.18-4.module_el8.9.0+3717+81096349 | |
criu-libs-debuginfo | 3.18-4.module_el8.9.0+3717+81096349 | |
libmaxminddb | 1.2.0-10.el8_9.1 | [ALSA-2024:0768](https://errata.almalinux.org/8/ALSA-2024-0768.html) | <div class="adv_s">Security Advisory</div> ([CVE-2020-28241](https://access.redhat.com/security/cve/CVE-2020-28241))
libmaxminddb-debuginfo | 1.2.0-10.el8_9.1 | |
libmaxminddb-debugsource | 1.2.0-10.el8_9.1 | |
libmaxminddb-devel | 1.2.0-10.el8_9.1 | [ALSA-2024:0768](https://errata.almalinux.org/8/ALSA-2024-0768.html) | <div class="adv_s">Security Advisory</div> ([CVE-2020-28241](https://access.redhat.com/security/cve/CVE-2020-28241))
libslirp | 4.4.0-1.module_el8.9.0+3717+81096349 | |
libslirp-debuginfo | 4.4.0-1.module_el8.9.0+3717+81096349 | |
libslirp-debugsource | 4.4.0-1.module_el8.9.0+3717+81096349 | |
libslirp-devel | 4.4.0-1.module_el8.9.0+3717+81096349 | |
netavark | 1.7.0-2.module_el8.9.0+3717+81096349 | [ALSA-2024:0752](https://errata.almalinux.org/8/ALSA-2024-0752.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-21626](https://access.redhat.com/security/cve/CVE-2024-21626))
podman | 4.0.2-26.module_el8.9.0+3722+7fd8ab2b.alma.1 | [ALSA-2024:0748](https://errata.almalinux.org/8/ALSA-2024-0748.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-39326](https://access.redhat.com/security/cve/CVE-2023-39326), [CVE-2023-45287](https://access.redhat.com/security/cve/CVE-2023-45287), [CVE-2024-21626](https://access.redhat.com/security/cve/CVE-2024-21626))
podman | 4.6.1-8.module_el8.9.0+3717+81096349 | [ALSA-2024:0752](https://errata.almalinux.org/8/ALSA-2024-0752.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-21626](https://access.redhat.com/security/cve/CVE-2024-21626))
podman-catatonit | 4.0.2-26.module_el8.9.0+3722+7fd8ab2b.alma.1 | [ALSA-2024:0748](https://errata.almalinux.org/8/ALSA-2024-0748.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-39326](https://access.redhat.com/security/cve/CVE-2023-39326), [CVE-2023-45287](https://access.redhat.com/security/cve/CVE-2023-45287), [CVE-2024-21626](https://access.redhat.com/security/cve/CVE-2024-21626))
podman-catatonit | 4.6.1-8.module_el8.9.0+3717+81096349 | [ALSA-2024:0752](https://errata.almalinux.org/8/ALSA-2024-0752.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-21626](https://access.redhat.com/security/cve/CVE-2024-21626))
podman-catatonit-debuginfo | 4.0.2-26.module_el8.9.0+3722+7fd8ab2b.alma.1 | |
podman-catatonit-debuginfo | 4.6.1-8.module_el8.9.0+3717+81096349 | |
podman-debuginfo | 4.0.2-26.module_el8.9.0+3722+7fd8ab2b.alma.1 | |
podman-debuginfo | 4.6.1-8.module_el8.9.0+3717+81096349 | |
podman-debugsource | 4.0.2-26.module_el8.9.0+3722+7fd8ab2b.alma.1 | |
podman-debugsource | 4.6.1-8.module_el8.9.0+3717+81096349 | |
podman-docker | 4.0.2-26.module_el8.9.0+3722+7fd8ab2b.alma.1 | [ALSA-2024:0748](https://errata.almalinux.org/8/ALSA-2024-0748.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-39326](https://access.redhat.com/security/cve/CVE-2023-39326), [CVE-2023-45287](https://access.redhat.com/security/cve/CVE-2023-45287), [CVE-2024-21626](https://access.redhat.com/security/cve/CVE-2024-21626))
podman-docker | 4.6.1-8.module_el8.9.0+3717+81096349 | [ALSA-2024:0752](https://errata.almalinux.org/8/ALSA-2024-0752.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-21626](https://access.redhat.com/security/cve/CVE-2024-21626))
podman-gvproxy | 4.0.2-26.module_el8.9.0+3722+7fd8ab2b.alma.1 | [ALSA-2024:0748](https://errata.almalinux.org/8/ALSA-2024-0748.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-39326](https://access.redhat.com/security/cve/CVE-2023-39326), [CVE-2023-45287](https://access.redhat.com/security/cve/CVE-2023-45287), [CVE-2024-21626](https://access.redhat.com/security/cve/CVE-2024-21626))
podman-gvproxy | 4.6.1-8.module_el8.9.0+3717+81096349 | [ALSA-2024:0752](https://errata.almalinux.org/8/ALSA-2024-0752.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-21626](https://access.redhat.com/security/cve/CVE-2024-21626))
podman-gvproxy-debuginfo | 4.0.2-26.module_el8.9.0+3722+7fd8ab2b.alma.1 | |
podman-gvproxy-debuginfo | 4.6.1-8.module_el8.9.0+3717+81096349 | |
podman-plugins | 4.0.2-26.module_el8.9.0+3722+7fd8ab2b.alma.1 | [ALSA-2024:0748](https://errata.almalinux.org/8/ALSA-2024-0748.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-39326](https://access.redhat.com/security/cve/CVE-2023-39326), [CVE-2023-45287](https://access.redhat.com/security/cve/CVE-2023-45287), [CVE-2024-21626](https://access.redhat.com/security/cve/CVE-2024-21626))
podman-plugins | 4.6.1-8.module_el8.9.0+3717+81096349 | [ALSA-2024:0752](https://errata.almalinux.org/8/ALSA-2024-0752.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-21626](https://access.redhat.com/security/cve/CVE-2024-21626))
podman-plugins-debuginfo | 4.0.2-26.module_el8.9.0+3722+7fd8ab2b.alma.1 | |
podman-plugins-debuginfo | 4.6.1-8.module_el8.9.0+3717+81096349 | |
podman-remote | 4.0.2-26.module_el8.9.0+3722+7fd8ab2b.alma.1 | [ALSA-2024:0748](https://errata.almalinux.org/8/ALSA-2024-0748.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-39326](https://access.redhat.com/security/cve/CVE-2023-39326), [CVE-2023-45287](https://access.redhat.com/security/cve/CVE-2023-45287), [CVE-2024-21626](https://access.redhat.com/security/cve/CVE-2024-21626))
podman-remote | 4.6.1-8.module_el8.9.0+3717+81096349 | [ALSA-2024:0752](https://errata.almalinux.org/8/ALSA-2024-0752.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-21626](https://access.redhat.com/security/cve/CVE-2024-21626))
podman-remote-debuginfo | 4.0.2-26.module_el8.9.0+3722+7fd8ab2b.alma.1 | |
podman-remote-debuginfo | 4.6.1-8.module_el8.9.0+3717+81096349 | |
podman-tests | 4.0.2-26.module_el8.9.0+3722+7fd8ab2b.alma.1 | [ALSA-2024:0748](https://errata.almalinux.org/8/ALSA-2024-0748.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-39326](https://access.redhat.com/security/cve/CVE-2023-39326), [CVE-2023-45287](https://access.redhat.com/security/cve/CVE-2023-45287), [CVE-2024-21626](https://access.redhat.com/security/cve/CVE-2024-21626))
podman-tests | 4.6.1-8.module_el8.9.0+3717+81096349 | [ALSA-2024:0752](https://errata.almalinux.org/8/ALSA-2024-0752.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-21626](https://access.redhat.com/security/cve/CVE-2024-21626))
python3-criu | 3.18-4.module_el8.9.0+3717+81096349 | |
python3-podman | 4.0.0-2.module_el8.9.0+3722+7fd8ab2b | |
python3-podman | 4.6.0-2.module_el8.9.0+3717+81096349 | [ALSA-2024:0752](https://errata.almalinux.org/8/ALSA-2024-0752.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-21626](https://access.redhat.com/security/cve/CVE-2024-21626))
runc | 1.1.12-1.module_el8.9.0+3717+81096349 | [ALSA-2024:0752](https://errata.almalinux.org/8/ALSA-2024-0752.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-21626](https://access.redhat.com/security/cve/CVE-2024-21626))
runc | 1.1.12-1.module_el8.9.0+3722+7fd8ab2b | [ALSA-2024:0748](https://errata.almalinux.org/8/ALSA-2024-0748.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-39326](https://access.redhat.com/security/cve/CVE-2023-39326), [CVE-2023-45287](https://access.redhat.com/security/cve/CVE-2023-45287), [CVE-2024-21626](https://access.redhat.com/security/cve/CVE-2024-21626))
runc-debuginfo | 1.1.12-1.module_el8.9.0+3717+81096349 | |
runc-debuginfo | 1.1.12-1.module_el8.9.0+3722+7fd8ab2b | |
runc-debugsource | 1.1.12-1.module_el8.9.0+3717+81096349 | |
runc-debugsource | 1.1.12-1.module_el8.9.0+3722+7fd8ab2b | |
skopeo | 1.13.3-3.module_el8.9.0+3717+81096349 | [ALSA-2024:0752](https://errata.almalinux.org/8/ALSA-2024-0752.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-21626](https://access.redhat.com/security/cve/CVE-2024-21626))
skopeo | 1.6.2-9.module_el8.9.0+3722+7fd8ab2b.alma.1 | |
skopeo-debuginfo | 1.6.2-9.module_el8.9.0+3722+7fd8ab2b.alma.1 | |
skopeo-debugsource | 1.6.2-9.module_el8.9.0+3722+7fd8ab2b.alma.1 | |
skopeo-tests | 1.13.3-3.module_el8.9.0+3717+81096349 | [ALSA-2024:0752](https://errata.almalinux.org/8/ALSA-2024-0752.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-21626](https://access.redhat.com/security/cve/CVE-2024-21626))
skopeo-tests | 1.6.2-9.module_el8.9.0+3722+7fd8ab2b.alma.1 | |
tcpdump | 4.9.3-3.el8_9.1.alma.1 | [ALSA-2024:0769](https://errata.almalinux.org/8/ALSA-2024-0769.html) | <div class="adv_s">Security Advisory</div> ([CVE-2021-41043](https://access.redhat.com/security/cve/CVE-2021-41043))
tcpdump-debuginfo | 4.9.3-3.el8_9.1.alma.1 | |
tcpdump-debugsource | 4.9.3-3.el8_9.1.alma.1 | |
toolbox | 0.0.99.4-5.module_el8.9.0+3722+7fd8ab2b | |
toolbox-debuginfo | 0.0.99.4-5.module_el8.9.0+3722+7fd8ab2b | |
toolbox-debugsource | 0.0.99.4-5.module_el8.9.0+3722+7fd8ab2b | |
toolbox-tests | 0.0.99.4-5.module_el8.9.0+3722+7fd8ab2b | |
udica | 0.2.6-4.module_el8.9.0+3722+7fd8ab2b | |

### AppStream aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
aardvark-dns | 1.7.0-1.module_el8.9.0+3717+81096349 | |
buildah | 1.31.3-3.module_el8.9.0+3717+81096349 | [ALSA-2024:0752](https://errata.almalinux.org/8/ALSA-2024-0752.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-21626](https://access.redhat.com/security/cve/CVE-2024-21626))
buildah-debuginfo | 1.31.3-3.module_el8.9.0+3717+81096349 | |
buildah-debugsource | 1.31.3-3.module_el8.9.0+3717+81096349 | |
buildah-tests | 1.31.3-3.module_el8.9.0+3717+81096349 | [ALSA-2024:0752](https://errata.almalinux.org/8/ALSA-2024-0752.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-21626](https://access.redhat.com/security/cve/CVE-2024-21626))
buildah-tests-debuginfo | 1.31.3-3.module_el8.9.0+3717+81096349 | |
container-selinux | 2.221.0-1.module_el8.9.0+3717+81096349 | |
containernetworking-plugins | 1.3.0-8.module_el8.9.0+3717+81096349 | [ALSA-2024:0752](https://errata.almalinux.org/8/ALSA-2024-0752.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-21626](https://access.redhat.com/security/cve/CVE-2024-21626))
containernetworking-plugins-debuginfo | 1.3.0-8.module_el8.9.0+3717+81096349 | |
containernetworking-plugins-debugsource | 1.3.0-8.module_el8.9.0+3717+81096349 | |
containers-common | 1-71.module_el8.9.0+3717+81096349 | [ALSA-2024:0752](https://errata.almalinux.org/8/ALSA-2024-0752.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-21626](https://access.redhat.com/security/cve/CVE-2024-21626))
crit | 3.18-4.module_el8.9.0+3717+81096349 | |
criu | 3.18-4.module_el8.9.0+3717+81096349 | |
criu-debuginfo | 3.18-4.module_el8.9.0+3717+81096349 | |
criu-debugsource | 3.18-4.module_el8.9.0+3717+81096349 | |
criu-devel | 3.18-4.module_el8.9.0+3717+81096349 | |
criu-libs | 3.18-4.module_el8.9.0+3717+81096349 | |
criu-libs-debuginfo | 3.18-4.module_el8.9.0+3717+81096349 | |
libmaxminddb | 1.2.0-10.el8_9.1 | [ALSA-2024:0768](https://errata.almalinux.org/8/ALSA-2024-0768.html) | <div class="adv_s">Security Advisory</div> ([CVE-2020-28241](https://access.redhat.com/security/cve/CVE-2020-28241))
libmaxminddb-debuginfo | 1.2.0-10.el8_9.1 | |
libmaxminddb-debugsource | 1.2.0-10.el8_9.1 | |
libmaxminddb-devel | 1.2.0-10.el8_9.1 | [ALSA-2024:0768](https://errata.almalinux.org/8/ALSA-2024-0768.html) | <div class="adv_s">Security Advisory</div> ([CVE-2020-28241](https://access.redhat.com/security/cve/CVE-2020-28241))
libslirp | 4.4.0-1.module_el8.9.0+3717+81096349 | |
libslirp-debuginfo | 4.4.0-1.module_el8.9.0+3717+81096349 | |
libslirp-debugsource | 4.4.0-1.module_el8.9.0+3717+81096349 | |
libslirp-devel | 4.4.0-1.module_el8.9.0+3717+81096349 | |
netavark | 1.7.0-2.module_el8.9.0+3717+81096349 | [ALSA-2024:0752](https://errata.almalinux.org/8/ALSA-2024-0752.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-21626](https://access.redhat.com/security/cve/CVE-2024-21626))
podman | 4.0.2-26.module_el8.9.0+3722+7fd8ab2b.alma.1 | [ALSA-2024:0748](https://errata.almalinux.org/8/ALSA-2024-0748.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-39326](https://access.redhat.com/security/cve/CVE-2023-39326), [CVE-2023-45287](https://access.redhat.com/security/cve/CVE-2023-45287), [CVE-2024-21626](https://access.redhat.com/security/cve/CVE-2024-21626))
podman | 4.6.1-8.module_el8.9.0+3717+81096349 | [ALSA-2024:0752](https://errata.almalinux.org/8/ALSA-2024-0752.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-21626](https://access.redhat.com/security/cve/CVE-2024-21626))
podman-catatonit | 4.0.2-26.module_el8.9.0+3722+7fd8ab2b.alma.1 | [ALSA-2024:0748](https://errata.almalinux.org/8/ALSA-2024-0748.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-39326](https://access.redhat.com/security/cve/CVE-2023-39326), [CVE-2023-45287](https://access.redhat.com/security/cve/CVE-2023-45287), [CVE-2024-21626](https://access.redhat.com/security/cve/CVE-2024-21626))
podman-catatonit | 4.6.1-8.module_el8.9.0+3717+81096349 | [ALSA-2024:0752](https://errata.almalinux.org/8/ALSA-2024-0752.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-21626](https://access.redhat.com/security/cve/CVE-2024-21626))
podman-catatonit-debuginfo | 4.0.2-26.module_el8.9.0+3722+7fd8ab2b.alma.1 | |
podman-catatonit-debuginfo | 4.6.1-8.module_el8.9.0+3717+81096349 | |
podman-debuginfo | 4.0.2-26.module_el8.9.0+3722+7fd8ab2b.alma.1 | |
podman-debuginfo | 4.6.1-8.module_el8.9.0+3717+81096349 | |
podman-debugsource | 4.0.2-26.module_el8.9.0+3722+7fd8ab2b.alma.1 | |
podman-debugsource | 4.6.1-8.module_el8.9.0+3717+81096349 | |
podman-docker | 4.0.2-26.module_el8.9.0+3722+7fd8ab2b.alma.1 | [ALSA-2024:0748](https://errata.almalinux.org/8/ALSA-2024-0748.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-39326](https://access.redhat.com/security/cve/CVE-2023-39326), [CVE-2023-45287](https://access.redhat.com/security/cve/CVE-2023-45287), [CVE-2024-21626](https://access.redhat.com/security/cve/CVE-2024-21626))
podman-docker | 4.6.1-8.module_el8.9.0+3717+81096349 | [ALSA-2024:0752](https://errata.almalinux.org/8/ALSA-2024-0752.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-21626](https://access.redhat.com/security/cve/CVE-2024-21626))
podman-gvproxy | 4.0.2-26.module_el8.9.0+3722+7fd8ab2b.alma.1 | [ALSA-2024:0748](https://errata.almalinux.org/8/ALSA-2024-0748.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-39326](https://access.redhat.com/security/cve/CVE-2023-39326), [CVE-2023-45287](https://access.redhat.com/security/cve/CVE-2023-45287), [CVE-2024-21626](https://access.redhat.com/security/cve/CVE-2024-21626))
podman-gvproxy | 4.6.1-8.module_el8.9.0+3717+81096349 | [ALSA-2024:0752](https://errata.almalinux.org/8/ALSA-2024-0752.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-21626](https://access.redhat.com/security/cve/CVE-2024-21626))
podman-gvproxy-debuginfo | 4.0.2-26.module_el8.9.0+3722+7fd8ab2b.alma.1 | |
podman-gvproxy-debuginfo | 4.6.1-8.module_el8.9.0+3717+81096349 | |
podman-plugins | 4.0.2-26.module_el8.9.0+3722+7fd8ab2b.alma.1 | [ALSA-2024:0748](https://errata.almalinux.org/8/ALSA-2024-0748.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-39326](https://access.redhat.com/security/cve/CVE-2023-39326), [CVE-2023-45287](https://access.redhat.com/security/cve/CVE-2023-45287), [CVE-2024-21626](https://access.redhat.com/security/cve/CVE-2024-21626))
podman-plugins | 4.6.1-8.module_el8.9.0+3717+81096349 | [ALSA-2024:0752](https://errata.almalinux.org/8/ALSA-2024-0752.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-21626](https://access.redhat.com/security/cve/CVE-2024-21626))
podman-plugins-debuginfo | 4.0.2-26.module_el8.9.0+3722+7fd8ab2b.alma.1 | |
podman-plugins-debuginfo | 4.6.1-8.module_el8.9.0+3717+81096349 | |
podman-remote | 4.0.2-26.module_el8.9.0+3722+7fd8ab2b.alma.1 | [ALSA-2024:0748](https://errata.almalinux.org/8/ALSA-2024-0748.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-39326](https://access.redhat.com/security/cve/CVE-2023-39326), [CVE-2023-45287](https://access.redhat.com/security/cve/CVE-2023-45287), [CVE-2024-21626](https://access.redhat.com/security/cve/CVE-2024-21626))
podman-remote | 4.6.1-8.module_el8.9.0+3717+81096349 | [ALSA-2024:0752](https://errata.almalinux.org/8/ALSA-2024-0752.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-21626](https://access.redhat.com/security/cve/CVE-2024-21626))
podman-remote-debuginfo | 4.0.2-26.module_el8.9.0+3722+7fd8ab2b.alma.1 | |
podman-remote-debuginfo | 4.6.1-8.module_el8.9.0+3717+81096349 | |
podman-tests | 4.0.2-26.module_el8.9.0+3722+7fd8ab2b.alma.1 | [ALSA-2024:0748](https://errata.almalinux.org/8/ALSA-2024-0748.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-39326](https://access.redhat.com/security/cve/CVE-2023-39326), [CVE-2023-45287](https://access.redhat.com/security/cve/CVE-2023-45287), [CVE-2024-21626](https://access.redhat.com/security/cve/CVE-2024-21626))
podman-tests | 4.6.1-8.module_el8.9.0+3717+81096349 | [ALSA-2024:0752](https://errata.almalinux.org/8/ALSA-2024-0752.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-21626](https://access.redhat.com/security/cve/CVE-2024-21626))
python3-criu | 3.18-4.module_el8.9.0+3717+81096349 | |
python3-podman | 4.0.0-2.module_el8.9.0+3722+7fd8ab2b | |
python3-podman | 4.6.0-2.module_el8.9.0+3717+81096349 | [ALSA-2024:0752](https://errata.almalinux.org/8/ALSA-2024-0752.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-21626](https://access.redhat.com/security/cve/CVE-2024-21626))
runc | 1.1.12-1.module_el8.9.0+3717+81096349 | [ALSA-2024:0752](https://errata.almalinux.org/8/ALSA-2024-0752.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-21626](https://access.redhat.com/security/cve/CVE-2024-21626))
runc | 1.1.12-1.module_el8.9.0+3722+7fd8ab2b | [ALSA-2024:0748](https://errata.almalinux.org/8/ALSA-2024-0748.html) | <div class="adv_s">Security Advisory</div> ([CVE-2023-39326](https://access.redhat.com/security/cve/CVE-2023-39326), [CVE-2023-45287](https://access.redhat.com/security/cve/CVE-2023-45287), [CVE-2024-21626](https://access.redhat.com/security/cve/CVE-2024-21626))
runc-debuginfo | 1.1.12-1.module_el8.9.0+3717+81096349 | |
runc-debuginfo | 1.1.12-1.module_el8.9.0+3722+7fd8ab2b | |
runc-debugsource | 1.1.12-1.module_el8.9.0+3717+81096349 | |
runc-debugsource | 1.1.12-1.module_el8.9.0+3722+7fd8ab2b | |
skopeo | 1.13.3-3.module_el8.9.0+3717+81096349 | [ALSA-2024:0752](https://errata.almalinux.org/8/ALSA-2024-0752.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-21626](https://access.redhat.com/security/cve/CVE-2024-21626))
skopeo | 1.6.2-9.module_el8.9.0+3722+7fd8ab2b.alma.1 | |
skopeo-debuginfo | 1.6.2-9.module_el8.9.0+3722+7fd8ab2b.alma.1 | |
skopeo-debugsource | 1.6.2-9.module_el8.9.0+3722+7fd8ab2b.alma.1 | |
skopeo-tests | 1.13.3-3.module_el8.9.0+3717+81096349 | [ALSA-2024:0752](https://errata.almalinux.org/8/ALSA-2024-0752.html) | <div class="adv_s">Security Advisory</div> ([CVE-2024-21626](https://access.redhat.com/security/cve/CVE-2024-21626))
skopeo-tests | 1.6.2-9.module_el8.9.0+3722+7fd8ab2b.alma.1 | |
tcpdump | 4.9.3-3.el8_9.1.alma.1 | [ALSA-2024:0769](https://errata.almalinux.org/8/ALSA-2024-0769.html) | <div class="adv_s">Security Advisory</div> ([CVE-2021-41043](https://access.redhat.com/security/cve/CVE-2021-41043))
tcpdump-debuginfo | 4.9.3-3.el8_9.1.alma.1 | |
tcpdump-debugsource | 4.9.3-3.el8_9.1.alma.1 | |
toolbox | 0.0.99.4-5.module_el8.9.0+3722+7fd8ab2b | |
toolbox-debuginfo | 0.0.99.4-5.module_el8.9.0+3722+7fd8ab2b | |
toolbox-debugsource | 0.0.99.4-5.module_el8.9.0+3722+7fd8ab2b | |
toolbox-tests | 0.0.99.4-5.module_el8.9.0+3722+7fd8ab2b | |
udica | 0.2.6-4.module_el8.9.0+3722+7fd8ab2b | |

