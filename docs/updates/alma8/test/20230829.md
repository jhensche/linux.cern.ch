## 2023-08-29

### BaseOS x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
iwl100-firmware | 39.31.5.1-114.el8_8.1.alma.1 | |
iwl1000-firmware | 39.31.5.1-114.el8_8.1.alma.1 | |
iwl105-firmware | 18.168.6.1-114.el8_8.1.alma.1 | |
iwl135-firmware | 18.168.6.1-114.el8_8.1.alma.1 | |
iwl2000-firmware | 18.168.6.1-114.el8_8.1.alma.1 | |
iwl2030-firmware | 18.168.6.1-114.el8_8.1.alma.1 | |
iwl3160-firmware | 25.30.13.0-114.el8_8.1.alma.1 | |
iwl3945-firmware | 15.32.2.9-114.el8_8.1.alma.1 | |
iwl4965-firmware | 228.61.2.24-114.el8_8.1.alma.1 | |
iwl5000-firmware | 8.83.5.1_1-114.el8_8.1.alma.1 | |
iwl5150-firmware | 8.24.2.2-114.el8_8.1.alma.1 | |
iwl6000-firmware | 9.221.4.1-114.el8_8.1.alma.1 | |
iwl6000g2a-firmware | 18.168.6.1-114.el8_8.1.alma.1 | |
iwl6000g2b-firmware | 18.168.6.1-114.el8_8.1.alma.1 | |
iwl6050-firmware | 41.28.5.1-114.el8_8.1.alma.1 | |
iwl7260-firmware | 25.30.13.0-114.el8_8.1.alma.1 | |
libertas-sd8686-firmware | 20230404-114.git2e92a49f.el8_8.alma.1 | |
libertas-sd8787-firmware | 20230404-114.git2e92a49f.el8_8.alma.1 | |
libertas-usb8388-firmware | 20230404-114.git2e92a49f.el8_8.alma.1 | |
libertas-usb8388-olpc-firmware | 20230404-114.git2e92a49f.el8_8.alma.1 | |
linux-firmware | 20230404-114.git2e92a49f.el8_8.alma.1 | |
microcode_ctl | 20220809-2.20230808.1.el8_8.alma | |

### BaseOS aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
iwl100-firmware | 39.31.5.1-114.el8_8.1.alma.1 | |
iwl1000-firmware | 39.31.5.1-114.el8_8.1.alma.1 | |
iwl105-firmware | 18.168.6.1-114.el8_8.1.alma.1 | |
iwl135-firmware | 18.168.6.1-114.el8_8.1.alma.1 | |
iwl2000-firmware | 18.168.6.1-114.el8_8.1.alma.1 | |
iwl2030-firmware | 18.168.6.1-114.el8_8.1.alma.1 | |
iwl3160-firmware | 25.30.13.0-114.el8_8.1.alma.1 | |
iwl3945-firmware | 15.32.2.9-114.el8_8.1.alma.1 | |
iwl4965-firmware | 228.61.2.24-114.el8_8.1.alma.1 | |
iwl5000-firmware | 8.83.5.1_1-114.el8_8.1.alma.1 | |
iwl5150-firmware | 8.24.2.2-114.el8_8.1.alma.1 | |
iwl6000-firmware | 9.221.4.1-114.el8_8.1.alma.1 | |
iwl6000g2a-firmware | 18.168.6.1-114.el8_8.1.alma.1 | |
iwl6000g2b-firmware | 18.168.6.1-114.el8_8.1.alma.1 | |
iwl6050-firmware | 41.28.5.1-114.el8_8.1.alma.1 | |
iwl7260-firmware | 25.30.13.0-114.el8_8.1.alma.1 | |
libertas-sd8686-firmware | 20230404-114.git2e92a49f.el8_8.alma.1 | |
libertas-sd8787-firmware | 20230404-114.git2e92a49f.el8_8.alma.1 | |
libertas-usb8388-firmware | 20230404-114.git2e92a49f.el8_8.alma.1 | |
libertas-usb8388-olpc-firmware | 20230404-114.git2e92a49f.el8_8.alma.1 | |
linux-firmware | 20230404-114.git2e92a49f.el8_8.alma.1 | |

