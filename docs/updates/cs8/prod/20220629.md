## 2022-06-29

### extras x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
centos-release-ovirt45 | 8.7-2.el8s | |
centos-release-ovirt45-testing | 8.7-2.el8s | |

### extras aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
centos-release-ovirt45 | 8.7-2.el8s | |
centos-release-ovirt45-testing | 8.7-2.el8s | |

