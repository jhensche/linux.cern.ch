## 2022-01-19

### cloud x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
openstack-kolla | 11.2.0-1.el8 | |
openstack-kolla | 12.1.0-1.el8 | |
openstack-kolla | 13.0.1-1.el8 | |
openstack-neutron-bgp-dragent | 18.1.0-1.el8 | |
openstack-neutron-bgp-dragent | 19.1.0-1.el8 | |
openstack-neutron-dynamic-routing-common | 18.1.0-1.el8 | |
openstack-neutron-dynamic-routing-common | 19.1.0-1.el8 | |
openstack-octavia-amphora-agent | 9.0.1-1.el8 | |
openstack-octavia-api | 9.0.1-1.el8 | |
openstack-octavia-common | 9.0.1-1.el8 | |
openstack-octavia-diskimage-create | 9.0.1-1.el8 | |
openstack-octavia-health-manager | 9.0.1-1.el8 | |
openstack-octavia-housekeeping | 9.0.1-1.el8 | |
openstack-octavia-worker | 9.0.1-1.el8 | |
puppet-stdlib | 6.3.0-3.7c1ae25git.el8 | |
puppet-stdlib | 8.1.0-1.el8 | |
python-aodhclient-doc | 2.3.1-1.el8 | |
python3-aodhclient | 2.3.1-1.el8 | |
python3-aodhclient-tests | 2.3.1-1.el8 | |
python3-dogpile-cache | 1.1.2-1.el8 | |
python3-neutron-dynamic-routing | 18.1.0-1.el8 | |
python3-neutron-dynamic-routing | 19.1.0-1.el8 | |
python3-neutron-dynamic-routing-tests | 18.1.0-1.el8 | |
python3-neutron-dynamic-routing-tests | 19.1.0-1.el8 | |
python3-octavia | 9.0.1-1.el8 | |
python3-octavia-tests | 9.0.1-1.el8 | |
python3-pymemcache | 3.4.0-1.el8 | |
python3-wsme | 0.10.1-1.el8 | |

### cloud aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
openstack-kolla | 11.2.0-1.el8 | |
openstack-kolla | 12.1.0-1.el8 | |
openstack-kolla | 13.0.1-1.el8 | |
openstack-neutron-bgp-dragent | 18.1.0-1.el8 | |
openstack-neutron-bgp-dragent | 19.1.0-1.el8 | |
openstack-neutron-dynamic-routing-common | 18.1.0-1.el8 | |
openstack-neutron-dynamic-routing-common | 19.1.0-1.el8 | |
openstack-octavia-amphora-agent | 9.0.1-1.el8 | |
openstack-octavia-api | 9.0.1-1.el8 | |
openstack-octavia-common | 9.0.1-1.el8 | |
openstack-octavia-diskimage-create | 9.0.1-1.el8 | |
openstack-octavia-health-manager | 9.0.1-1.el8 | |
openstack-octavia-housekeeping | 9.0.1-1.el8 | |
openstack-octavia-worker | 9.0.1-1.el8 | |
puppet-stdlib | 6.3.0-3.7c1ae25git.el8 | |
puppet-stdlib | 8.1.0-1.el8 | |
python-aodhclient-doc | 2.3.1-1.el8 | |
python3-aodhclient | 2.3.1-1.el8 | |
python3-aodhclient-tests | 2.3.1-1.el8 | |
python3-dogpile-cache | 1.1.2-1.el8 | |
python3-neutron-dynamic-routing | 18.1.0-1.el8 | |
python3-neutron-dynamic-routing | 19.1.0-1.el8 | |
python3-neutron-dynamic-routing-tests | 18.1.0-1.el8 | |
python3-neutron-dynamic-routing-tests | 19.1.0-1.el8 | |
python3-octavia | 9.0.1-1.el8 | |
python3-octavia-tests | 9.0.1-1.el8 | |
python3-pymemcache | 3.4.0-1.el8 | |
python3-wsme | 0.10.1-1.el8 | |

