## 2021-07-07

### CERN x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
CERN-CA-certs | 20200530-2.el8s.cern | |
cern-krb5-conf | 1.3-7.el8s.cern | |
cern-krb5-conf-atlas | 1.3-7.el8s.cern | |
cern-krb5-conf-cms | 1.3-7.el8s.cern | |
cern-krb5-conf-defaults-cernch | 1.3-7.el8s.cern | |
cern-krb5-conf-defaults-ipadev | 1.3-7.el8s.cern | |
cern-krb5-conf-ipadev | 1.3-7.el8s.cern | |
cern-krb5-conf-realm-cernch | 1.3-7.el8s.cern | |
cern-krb5-conf-realm-cernch-atlas | 1.3-7.el8s.cern | |
cern-krb5-conf-realm-cernch-cms | 1.3-7.el8s.cern | |
cern-krb5-conf-realm-cernch-tn | 1.3-7.el8s.cern | |
cern-krb5-conf-realm-ipadev | 1.3-7.el8s.cern | |
cern-krb5-conf-tn | 1.3-7.el8s.cern | |
perl-Convert-BinHex | 1.125-13.el8s.cern | |
perl-SOAP-Lite | 1.27-7.el8s.cern | |

### cloud x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
openstack-cinder | 16.3.0-1.el8 | |
python3-cinder | 16.3.0-1.el8 | |
python3-cinder-tests | 16.3.0-1.el8 | |

### virt x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
ansible | 2.9.23-2.el8 | |
ansible-doc | 2.9.23-2.el8 | |
ansible-test | 2.9.23-2.el8 | |

### CERN aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
CERN-CA-certs | 20200530-2.el8s.cern | |
cern-krb5-conf | 1.3-7.el8s.cern | |
cern-krb5-conf-atlas | 1.3-7.el8s.cern | |
cern-krb5-conf-cms | 1.3-7.el8s.cern | |
cern-krb5-conf-defaults-cernch | 1.3-7.el8s.cern | |
cern-krb5-conf-defaults-ipadev | 1.3-7.el8s.cern | |
cern-krb5-conf-ipadev | 1.3-7.el8s.cern | |
cern-krb5-conf-realm-cernch | 1.3-7.el8s.cern | |
cern-krb5-conf-realm-cernch-atlas | 1.3-7.el8s.cern | |
cern-krb5-conf-realm-cernch-cms | 1.3-7.el8s.cern | |
cern-krb5-conf-realm-cernch-tn | 1.3-7.el8s.cern | |
cern-krb5-conf-realm-ipadev | 1.3-7.el8s.cern | |
cern-krb5-conf-tn | 1.3-7.el8s.cern | |
perl-Convert-BinHex | 1.125-13.el8s.cern | |
perl-SOAP-Lite | 1.27-7.el8s.cern | |

### cloud aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
openstack-cinder | 16.3.0-1.el8 | |
python3-cinder | 16.3.0-1.el8 | |
python3-cinder-tests | 16.3.0-1.el8 | |

### virt aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
ansible | 2.9.23-2.el8 | |
ansible-doc | 2.9.23-2.el8 | |
ansible-test | 2.9.23-2.el8 | |

