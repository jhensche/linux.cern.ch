## 2023-04-19

### CERN x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
useraddcern | 1.1-1.el8s.cern | |

### openafs x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
kmod-openafs | 1.8.9.0-2.4.18.0_483.el8.el8s.cern | |
kmod-openafs | 1.8.9.0-2.4.18.0_485.el8.el8s.cern | |
openafs-debugsource | 1.8.9.0_4.18.0_483.el8-2.el8s.cern | |
openafs-debugsource | 1.8.9.0_4.18.0_485.el8-2.el8s.cern | |

### BaseOS x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
bpftool | 4.18.0-483.el8 | |
bpftool | 4.18.0-485.el8 | |
kernel | 4.18.0-483.el8 | |
kernel | 4.18.0-485.el8 | |
kernel-abi-stablelists | 4.18.0-483.el8 | |
kernel-abi-stablelists | 4.18.0-485.el8 | |
kernel-core | 4.18.0-483.el8 | |
kernel-core | 4.18.0-485.el8 | |
kernel-cross-headers | 4.18.0-483.el8 | |
kernel-cross-headers | 4.18.0-485.el8 | |
kernel-debug | 4.18.0-483.el8 | |
kernel-debug | 4.18.0-485.el8 | |
kernel-debug-core | 4.18.0-483.el8 | |
kernel-debug-core | 4.18.0-485.el8 | |
kernel-debug-devel | 4.18.0-483.el8 | |
kernel-debug-devel | 4.18.0-485.el8 | |
kernel-debug-modules | 4.18.0-483.el8 | |
kernel-debug-modules | 4.18.0-485.el8 | |
kernel-debug-modules-extra | 4.18.0-483.el8 | |
kernel-debug-modules-extra | 4.18.0-485.el8 | |
kernel-devel | 4.18.0-483.el8 | |
kernel-devel | 4.18.0-485.el8 | |
kernel-doc | 4.18.0-483.el8 | |
kernel-doc | 4.18.0-485.el8 | |
kernel-headers | 4.18.0-483.el8 | |
kernel-headers | 4.18.0-485.el8 | |
kernel-modules | 4.18.0-483.el8 | |
kernel-modules | 4.18.0-485.el8 | |
kernel-modules-extra | 4.18.0-483.el8 | |
kernel-modules-extra | 4.18.0-485.el8 | |
kernel-tools | 4.18.0-483.el8 | |
kernel-tools | 4.18.0-485.el8 | |
kernel-tools-libs | 4.18.0-483.el8 | |
kernel-tools-libs | 4.18.0-485.el8 | |
perf | 4.18.0-483.el8 | |
perf | 4.18.0-485.el8 | |
python-rpm-macros | 3-45.el8 | |
python-srpm-macros | 3-45.el8 | |
python3-perf | 4.18.0-483.el8 | |
python3-perf | 4.18.0-485.el8 | |
python3-rpm-macros | 3-45.el8 | |
which | 2.21-20.el8 | |

### AppStream x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
cockpit-machines | 286.1-1.el8 | |
cockpit-packagekit | 286.1-1.el8 | |
cockpit-pcp | 286.1-1.el8 | |
cockpit-storaged | 286.1-1.el8 | |
edk2-ovmf | 20220126gitbb1bba3d77-5.el8 | |
libblockdev | 2.28-3.el8 | |
libblockdev-crypto | 2.28-3.el8 | |
libblockdev-dm | 2.28-3.el8 | |
libblockdev-fs | 2.28-3.el8 | |
libblockdev-kbd | 2.28-3.el8 | |
libblockdev-loop | 2.28-3.el8 | |
libblockdev-lvm | 2.28-3.el8 | |
libblockdev-lvm-dbus | 2.28-3.el8 | |
libblockdev-mdraid | 2.28-3.el8 | |
libblockdev-mpath | 2.28-3.el8 | |
libblockdev-nvdimm | 2.28-3.el8 | |
libblockdev-part | 2.28-3.el8 | |
libblockdev-plugins-all | 2.28-3.el8 | |
libblockdev-swap | 2.28-3.el8 | |
libblockdev-utils | 2.28-3.el8 | |
libblockdev-vdo | 2.28-3.el8 | |
mpich | 3.4.2-2.el8 | |
mpich-devel | 3.4.2-2.el8 | |
mpich-doc | 3.4.2-2.el8 | |
nmstate | 1.4.3-1.el8 | |
nmstate-libs | 1.4.3-1.el8 | |
nmstate-plugin-ovsdb | 1.4.3-1.el8 | |
osbuild-composer | 79-1.el8 | |
osbuild-composer-core | 79-1.el8 | |
osbuild-composer-dnf-json | 79-1.el8 | |
osbuild-composer-worker | 79-1.el8 | |
python3-blockdev | 2.28-3.el8 | |
python3-libnmstate | 1.4.3-1.el8 | |
qatengine | 1.0.0-1.el8 | |
qatzip | 1.1.2-1.el8 | |
qatzip-libs | 1.1.2-1.el8 | |
xorg-x11-server-common | 1.20.11-16.el8 | |
xorg-x11-server-Xdmx | 1.20.11-16.el8 | |
xorg-x11-server-Xephyr | 1.20.11-16.el8 | |
xorg-x11-server-Xnest | 1.20.11-16.el8 | |
xorg-x11-server-Xorg | 1.20.11-16.el8 | |
xorg-x11-server-Xvfb | 1.20.11-16.el8 | |
xorg-x11-server-Xwayland | 21.1.3-11.el8 | |

### PowerTools x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
kernel-tools-libs-devel | 4.18.0-483.el8 | |
kernel-tools-libs-devel | 4.18.0-485.el8 | |
libblockdev-crypto-devel | 2.28-3.el8 | |
libblockdev-devel | 2.28-3.el8 | |
libblockdev-fs-devel | 2.28-3.el8 | |
libblockdev-loop-devel | 2.28-3.el8 | |
libblockdev-lvm-devel | 2.28-3.el8 | |
libblockdev-mdraid-devel | 2.28-3.el8 | |
libblockdev-part-devel | 2.28-3.el8 | |
libblockdev-swap-devel | 2.28-3.el8 | |
libblockdev-utils-devel | 2.28-3.el8 | |
libblockdev-vdo-devel | 2.28-3.el8 | |
nmstate-devel | 1.4.3-1.el8 | |
python3-mpich | 3.4.2-2.el8 | |
python3.11-iniconfig | 1.1.1-2.el8 | |
python3.11-pybind11 | 2.10.3-2.el8 | |
python3.11-pybind11-devel | 2.10.3-2.el8 | |
qatzip-devel | 1.1.2-1.el8 | |
xorg-x11-server-devel | 1.20.11-16.el8 | |
xorg-x11-server-source | 1.20.11-16.el8 | |

### RT x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
kernel-rt | 4.18.0-479.rt7.268.el8 | |
kernel-rt | 4.18.0-484.rt7.273.el8 | |
kernel-rt-core | 4.18.0-479.rt7.268.el8 | |
kernel-rt-core | 4.18.0-484.rt7.273.el8 | |
kernel-rt-debug | 4.18.0-479.rt7.268.el8 | |
kernel-rt-debug | 4.18.0-484.rt7.273.el8 | |
kernel-rt-debug-core | 4.18.0-479.rt7.268.el8 | |
kernel-rt-debug-core | 4.18.0-484.rt7.273.el8 | |
kernel-rt-debug-devel | 4.18.0-479.rt7.268.el8 | |
kernel-rt-debug-devel | 4.18.0-484.rt7.273.el8 | |
kernel-rt-debug-modules | 4.18.0-479.rt7.268.el8 | |
kernel-rt-debug-modules | 4.18.0-484.rt7.273.el8 | |
kernel-rt-debug-modules-extra | 4.18.0-479.rt7.268.el8 | |
kernel-rt-debug-modules-extra | 4.18.0-484.rt7.273.el8 | |
kernel-rt-devel | 4.18.0-479.rt7.268.el8 | |
kernel-rt-devel | 4.18.0-484.rt7.273.el8 | |
kernel-rt-modules | 4.18.0-479.rt7.268.el8 | |
kernel-rt-modules | 4.18.0-484.rt7.273.el8 | |
kernel-rt-modules-extra | 4.18.0-479.rt7.268.el8 | |
kernel-rt-modules-extra | 4.18.0-484.rt7.273.el8 | |

### extras x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
centos-release-samba418 | 1.0-2.el8s | |

### CERN aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
useraddcern | 1.1-1.el8s.cern | |

### openafs aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
kmod-openafs | 1.8.9.0-2.4.18.0_483.el8.el8s.cern | |
kmod-openafs | 1.8.9.0-2.4.18.0_485.el8.el8s.cern | |
openafs-debugsource | 1.8.9.0_4.18.0_483.el8-2.el8s.cern | |
openafs-debugsource | 1.8.9.0_4.18.0_485.el8-2.el8s.cern | |

### BaseOS aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
bpftool | 4.18.0-483.el8 | |
bpftool | 4.18.0-485.el8 | |
kernel | 4.18.0-483.el8 | |
kernel | 4.18.0-485.el8 | |
kernel-abi-stablelists | 4.18.0-483.el8 | |
kernel-abi-stablelists | 4.18.0-485.el8 | |
kernel-core | 4.18.0-483.el8 | |
kernel-core | 4.18.0-485.el8 | |
kernel-cross-headers | 4.18.0-483.el8 | |
kernel-cross-headers | 4.18.0-485.el8 | |
kernel-debug | 4.18.0-483.el8 | |
kernel-debug | 4.18.0-485.el8 | |
kernel-debug-core | 4.18.0-483.el8 | |
kernel-debug-core | 4.18.0-485.el8 | |
kernel-debug-devel | 4.18.0-483.el8 | |
kernel-debug-devel | 4.18.0-485.el8 | |
kernel-debug-modules | 4.18.0-483.el8 | |
kernel-debug-modules | 4.18.0-485.el8 | |
kernel-debug-modules-extra | 4.18.0-483.el8 | |
kernel-debug-modules-extra | 4.18.0-485.el8 | |
kernel-devel | 4.18.0-483.el8 | |
kernel-devel | 4.18.0-485.el8 | |
kernel-doc | 4.18.0-483.el8 | |
kernel-doc | 4.18.0-485.el8 | |
kernel-headers | 4.18.0-483.el8 | |
kernel-headers | 4.18.0-485.el8 | |
kernel-modules | 4.18.0-483.el8 | |
kernel-modules | 4.18.0-485.el8 | |
kernel-modules-extra | 4.18.0-483.el8 | |
kernel-modules-extra | 4.18.0-485.el8 | |
kernel-tools | 4.18.0-483.el8 | |
kernel-tools | 4.18.0-485.el8 | |
kernel-tools-libs | 4.18.0-483.el8 | |
kernel-tools-libs | 4.18.0-485.el8 | |
perf | 4.18.0-483.el8 | |
perf | 4.18.0-485.el8 | |
python3-perf | 4.18.0-483.el8 | |
python3-perf | 4.18.0-485.el8 | |
which | 2.21-20.el8 | |

### AppStream aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
cockpit-machines | 286.1-1.el8 | |
cockpit-packagekit | 286.1-1.el8 | |
cockpit-pcp | 286.1-1.el8 | |
cockpit-storaged | 286.1-1.el8 | |
edk2-aarch64 | 20220126gitbb1bba3d77-5.el8 | |
libblockdev | 2.28-3.el8 | |
libblockdev-crypto | 2.28-3.el8 | |
libblockdev-dm | 2.28-3.el8 | |
libblockdev-fs | 2.28-3.el8 | |
libblockdev-kbd | 2.28-3.el8 | |
libblockdev-loop | 2.28-3.el8 | |
libblockdev-lvm | 2.28-3.el8 | |
libblockdev-lvm-dbus | 2.28-3.el8 | |
libblockdev-mdraid | 2.28-3.el8 | |
libblockdev-mpath | 2.28-3.el8 | |
libblockdev-nvdimm | 2.28-3.el8 | |
libblockdev-part | 2.28-3.el8 | |
libblockdev-plugins-all | 2.28-3.el8 | |
libblockdev-swap | 2.28-3.el8 | |
libblockdev-utils | 2.28-3.el8 | |
libblockdev-vdo | 2.28-3.el8 | |
mpich | 3.4.2-2.el8 | |
mpich-devel | 3.4.2-2.el8 | |
mpich-doc | 3.4.2-2.el8 | |
nmstate | 1.4.3-1.el8 | |
nmstate-libs | 1.4.3-1.el8 | |
nmstate-plugin-ovsdb | 1.4.3-1.el8 | |
osbuild-composer | 79-1.el8 | |
osbuild-composer-core | 79-1.el8 | |
osbuild-composer-dnf-json | 79-1.el8 | |
osbuild-composer-worker | 79-1.el8 | |
python-rpm-macros | 3-45.el8 | |
python-srpm-macros | 3-45.el8 | |
python3-blockdev | 2.28-3.el8 | |
python3-libnmstate | 1.4.3-1.el8 | |
python3-rpm-macros | 3-45.el8 | |
xorg-x11-server-common | 1.20.11-16.el8 | |
xorg-x11-server-Xdmx | 1.20.11-16.el8 | |
xorg-x11-server-Xephyr | 1.20.11-16.el8 | |
xorg-x11-server-Xnest | 1.20.11-16.el8 | |
xorg-x11-server-Xorg | 1.20.11-16.el8 | |
xorg-x11-server-Xvfb | 1.20.11-16.el8 | |
xorg-x11-server-Xwayland | 21.1.3-11.el8 | |

### PowerTools aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
kernel-tools-libs-devel | 4.18.0-483.el8 | |
kernel-tools-libs-devel | 4.18.0-485.el8 | |
libblockdev-crypto-devel | 2.28-3.el8 | |
libblockdev-devel | 2.28-3.el8 | |
libblockdev-fs-devel | 2.28-3.el8 | |
libblockdev-loop-devel | 2.28-3.el8 | |
libblockdev-lvm-devel | 2.28-3.el8 | |
libblockdev-mdraid-devel | 2.28-3.el8 | |
libblockdev-part-devel | 2.28-3.el8 | |
libblockdev-swap-devel | 2.28-3.el8 | |
libblockdev-utils-devel | 2.28-3.el8 | |
libblockdev-vdo-devel | 2.28-3.el8 | |
nmstate-devel | 1.4.3-1.el8 | |
python3-mpich | 3.4.2-2.el8 | |
python3.11-iniconfig | 1.1.1-2.el8 | |
python3.11-pybind11 | 2.10.3-2.el8 | |
python3.11-pybind11-devel | 2.10.3-2.el8 | |
xorg-x11-server-devel | 1.20.11-16.el8 | |
xorg-x11-server-source | 1.20.11-16.el8 | |

### extras aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
centos-release-samba418 | 1.0-2.el8s | |

