## 2024-03-06

### openafs x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
kmod-openafs | 1.8.10-0.4.18.0_540.el8.el8s.cern | |

### BaseOS x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
bpftool | 4.18.0-540.el8 | |
gmp | 6.1.2-12.el8 | |
gmp-c++ | 6.1.2-12.el8 | |
gmp-devel | 6.1.2-12.el8 | |
gpgme | 1.13.1-12.el8 | |
gpgmepp | 1.13.1-12.el8 | |
kernel | 4.18.0-540.el8 | |
kernel-abi-stablelists | 4.18.0-540.el8 | |
kernel-core | 4.18.0-540.el8 | |
kernel-cross-headers | 4.18.0-540.el8 | |
kernel-debug | 4.18.0-540.el8 | |
kernel-debug-core | 4.18.0-540.el8 | |
kernel-debug-devel | 4.18.0-540.el8 | |
kernel-debug-modules | 4.18.0-540.el8 | |
kernel-debug-modules-extra | 4.18.0-540.el8 | |
kernel-devel | 4.18.0-540.el8 | |
kernel-doc | 4.18.0-540.el8 | |
kernel-headers | 4.18.0-540.el8 | |
kernel-modules | 4.18.0-540.el8 | |
kernel-modules-extra | 4.18.0-540.el8 | |
kernel-tools | 4.18.0-540.el8 | |
kernel-tools-libs | 4.18.0-540.el8 | |
opencryptoki | 3.22.0-2.el8 | |
opencryptoki-icsftok | 3.22.0-2.el8 | |
opencryptoki-libs | 3.22.0-2.el8 | |
opencryptoki-swtok | 3.22.0-2.el8 | |
opencryptoki-tpmtok | 3.22.0-2.el8 | |
opensc | 0.20.0-8.el8 | |
perf | 4.18.0-540.el8 | |
platform-python-pip | 9.0.3-24.el8 | |
python3-gpg | 1.13.1-12.el8 | |
python3-perf | 4.18.0-540.el8 | |
python3-pip-wheel | 9.0.3-24.el8 | |
selinux-policy | 3.14.3-136.el8 | |
selinux-policy-devel | 3.14.3-136.el8 | |
selinux-policy-doc | 3.14.3-136.el8 | |
selinux-policy-minimum | 3.14.3-136.el8 | |
selinux-policy-mls | 3.14.3-136.el8 | |
selinux-policy-sandbox | 3.14.3-136.el8 | |
selinux-policy-targeted | 3.14.3-136.el8 | |
setup | 2.12.2-11.el8 | |
sudo | 1.9.5p2-1.el8 | [RHSA-2024:0811](https://access.redhat.com/errata/RHSA-2024:0811) | <div class="adv_s">Security Advisory</div> ([CVE-2023-28486](https://access.redhat.com/security/cve/CVE-2023-28486), [CVE-2023-28487](https://access.redhat.com/security/cve/CVE-2023-28487), [CVE-2023-42465](https://access.redhat.com/security/cve/CVE-2023-42465))
tuned | 2.22.0-0.1.rc1.el8 | |
tuned-profiles-atomic | 2.22.0-0.1.rc1.el8 | |
tuned-profiles-compat | 2.22.0-0.1.rc1.el8 | |
tuned-profiles-cpu-partitioning | 2.22.0-0.1.rc1.el8 | |
tuned-profiles-mssql | 2.22.0-0.1.rc1.el8 | |
tuned-profiles-oracle | 2.22.0-0.1.rc1.el8 | |
tzdata | 2024a-1.el8 | [RHBA-2024:0762](https://access.redhat.com/errata/RHBA-2024:0762) | <div class="adv_b">Bug Fix Advisory</div>

### AppStream x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
389-ds-base | 1.4.3.39-2.module_el8+909+03085a27 | |
389-ds-base-devel | 1.4.3.39-2.module_el8+909+03085a27 | |
389-ds-base-legacy-tools | 1.4.3.39-2.module_el8+909+03085a27 | |
389-ds-base-libs | 1.4.3.39-2.module_el8+909+03085a27 | |
389-ds-base-snmp | 1.4.3.39-2.module_el8+909+03085a27 | |
ansible-freeipa | 1.12.1-1.el8 | |
ansible-freeipa-tests | 1.12.1-1.el8 | |
cockpit-session-recording | 16-1.el8 | |
containers-common | 1-80.module_el8+954+1d378e5e | |
crun | 1.14.1-1.module_el8+951+32019cde | |
delve | 1.21.2-3.module_el8+960+4060efbe | |
freeglut | 3.0.0-9.el8 | |
freeglut-devel | 3.0.0-9.el8 | |
galera | 26.4.16-1.module_el8+852+a3891817 | |
gcc-toolset-13-binutils | 2.40-19.el8 | |
gcc-toolset-13-binutils-devel | 2.40-19.el8 | |
gcc-toolset-13-binutils-gold | 2.40-19.el8 | |
go-toolset | 1.21.7-1.module_el8+960+4060efbe | |
golang | 1.21.7-1.module_el8+960+4060efbe | |
golang-bin | 1.21.7-1.module_el8+960+4060efbe | |
golang-docs | 1.21.7-1.module_el8+960+4060efbe | |
golang-misc | 1.21.7-1.module_el8+960+4060efbe | |
golang-src | 1.21.7-1.module_el8+960+4060efbe | |
golang-tests | 1.21.7-1.module_el8+960+4060efbe | |
Judy | 1.0.5-18.module_el8+852+a3891817 | |
leapp-deps | 0.17.0-1.el8 | |
mariadb | 10.11.6-1.module_el8+956+53fd50ff | |
mariadb-backup | 10.11.6-1.module_el8+956+53fd50ff | |
mariadb-common | 10.11.6-1.module_el8+956+53fd50ff | |
mariadb-devel | 10.11.6-1.module_el8+956+53fd50ff | |
mariadb-embedded | 10.11.6-1.module_el8+956+53fd50ff | |
mariadb-embedded-devel | 10.11.6-1.module_el8+956+53fd50ff | |
mariadb-errmsg | 10.11.6-1.module_el8+956+53fd50ff | |
mariadb-gssapi-server | 10.11.6-1.module_el8+956+53fd50ff | |
mariadb-oqgraph-engine | 10.11.6-1.module_el8+956+53fd50ff | |
mariadb-pam | 10.11.6-1.module_el8+956+53fd50ff | |
mariadb-server | 10.11.6-1.module_el8+956+53fd50ff | |
mariadb-server-galera | 10.11.6-1.module_el8+956+53fd50ff | |
mariadb-server-utils | 10.11.6-1.module_el8+956+53fd50ff | |
mariadb-test | 10.11.6-1.module_el8+956+53fd50ff | |
netavark | 1.10.3-1.module_el8+951+32019cde | |
python3-leapp | 0.17.0-1.el8 | |
python3-lib389 | 1.4.3.39-2.module_el8+909+03085a27 | |
python3-pip | 9.0.3-24.el8 | |
python3.11-pip | 22.3.1-5.el8 | |
python3.11-pip-wheel | 22.3.1-5.el8 | |
python39-pip | 20.2.4-9.module_el8+963+a9c12c76 | |
python39-pip-wheel | 20.2.4-9.module_el8+963+a9c12c76 | |
qgpgme | 1.13.1-12.el8 | |
rhc | 0.2.4-5.el8 | |
rhel-system-roles | 1.23.0-2.17.el8 | |
slirp4netns | 1.2.3-1.module_el8+951+32019cde | |
snactor | 0.17.0-1.el8 | |
squid | 4.15-9.module_el8+880+b94e8375 | |
stalld | 1.19.1-4.el8 | |
tftp | 5.2-27.el8 | |
tftp-server | 5.2-27.el8 | |
tlog | 14-1.el8 | |
tuned-gtk | 2.22.0-0.1.rc1.el8 | |
tuned-utils | 2.22.0-0.1.rc1.el8 | |
tuned-utils-systemtap | 2.22.0-0.1.rc1.el8 | |
tzdata-java | 2024a-1.el8 | [RHBA-2024:0762](https://access.redhat.com/errata/RHBA-2024:0762) | <div class="adv_b">Bug Fix Advisory</div>

### PowerTools x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
gpgme-devel | 1.13.1-12.el8 | |
gpgmepp-devel | 1.13.1-12.el8 | |
kernel-tools-libs-devel | 4.18.0-540.el8 | |
opencryptoki-devel | 3.22.0-2.el8 | |
qgpgme-devel | 1.13.1-12.el8 | |

### RT x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
rteval | 3.7-4.el8 | |
tuned-profiles-realtime | 2.22.0-0.1.rc1.el8 | |

### openafs aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
kmod-openafs | 1.8.10-0.4.18.0_539.el8.el8s.cern | |
kmod-openafs | 1.8.10-0.4.18.0_540.el8.el8s.cern | |

### BaseOS aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
bpftool | 4.18.0-540.el8 | |
gmp | 6.1.2-12.el8 | |
gmp-c++ | 6.1.2-12.el8 | |
gmp-devel | 6.1.2-12.el8 | |
gpgme | 1.13.1-12.el8 | |
gpgmepp | 1.13.1-12.el8 | |
kernel | 4.18.0-540.el8 | |
kernel-abi-stablelists | 4.18.0-540.el8 | |
kernel-core | 4.18.0-540.el8 | |
kernel-cross-headers | 4.18.0-540.el8 | |
kernel-debug | 4.18.0-540.el8 | |
kernel-debug-core | 4.18.0-540.el8 | |
kernel-debug-devel | 4.18.0-540.el8 | |
kernel-debug-modules | 4.18.0-540.el8 | |
kernel-debug-modules-extra | 4.18.0-540.el8 | |
kernel-devel | 4.18.0-540.el8 | |
kernel-doc | 4.18.0-540.el8 | |
kernel-headers | 4.18.0-540.el8 | |
kernel-modules | 4.18.0-540.el8 | |
kernel-modules-extra | 4.18.0-540.el8 | |
kernel-tools | 4.18.0-540.el8 | |
kernel-tools-libs | 4.18.0-540.el8 | |
opencryptoki | 3.22.0-2.el8 | |
opencryptoki-icsftok | 3.22.0-2.el8 | |
opencryptoki-libs | 3.22.0-2.el8 | |
opencryptoki-swtok | 3.22.0-2.el8 | |
opencryptoki-tpmtok | 3.22.0-2.el8 | |
opensc | 0.20.0-8.el8 | |
perf | 4.18.0-540.el8 | |
platform-python-pip | 9.0.3-24.el8 | |
python3-gpg | 1.13.1-12.el8 | |
python3-perf | 4.18.0-540.el8 | |
python3-pip-wheel | 9.0.3-24.el8 | |
selinux-policy | 3.14.3-136.el8 | |
selinux-policy-devel | 3.14.3-136.el8 | |
selinux-policy-doc | 3.14.3-136.el8 | |
selinux-policy-minimum | 3.14.3-136.el8 | |
selinux-policy-mls | 3.14.3-136.el8 | |
selinux-policy-sandbox | 3.14.3-136.el8 | |
selinux-policy-targeted | 3.14.3-136.el8 | |
setup | 2.12.2-11.el8 | |
sudo | 1.9.5p2-1.el8 | [RHSA-2024:0811](https://access.redhat.com/errata/RHSA-2024:0811) | <div class="adv_s">Security Advisory</div> ([CVE-2023-28486](https://access.redhat.com/security/cve/CVE-2023-28486), [CVE-2023-28487](https://access.redhat.com/security/cve/CVE-2023-28487), [CVE-2023-42465](https://access.redhat.com/security/cve/CVE-2023-42465))
tuned | 2.22.0-0.1.rc1.el8 | |
tuned-profiles-atomic | 2.22.0-0.1.rc1.el8 | |
tuned-profiles-compat | 2.22.0-0.1.rc1.el8 | |
tuned-profiles-cpu-partitioning | 2.22.0-0.1.rc1.el8 | |
tuned-profiles-mssql | 2.22.0-0.1.rc1.el8 | |
tuned-profiles-oracle | 2.22.0-0.1.rc1.el8 | |
tzdata | 2024a-1.el8 | [RHBA-2024:0762](https://access.redhat.com/errata/RHBA-2024:0762) | <div class="adv_b">Bug Fix Advisory</div>

### AppStream aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
389-ds-base | 1.4.3.39-2.module_el8+909+03085a27 | |
389-ds-base-devel | 1.4.3.39-2.module_el8+909+03085a27 | |
389-ds-base-legacy-tools | 1.4.3.39-2.module_el8+909+03085a27 | |
389-ds-base-libs | 1.4.3.39-2.module_el8+909+03085a27 | |
389-ds-base-snmp | 1.4.3.39-2.module_el8+909+03085a27 | |
ansible-freeipa | 1.12.1-1.el8 | |
ansible-freeipa-tests | 1.12.1-1.el8 | |
cockpit-session-recording | 16-1.el8 | |
containers-common | 1-80.module_el8+954+1d378e5e | |
crun | 1.14.1-1.module_el8+951+32019cde | |
delve | 1.21.2-3.module_el8+960+4060efbe | |
freeglut | 3.0.0-9.el8 | |
freeglut-devel | 3.0.0-9.el8 | |
galera | 26.4.16-1.module_el8+852+a3891817 | |
gcc-toolset-13-binutils | 2.40-19.el8 | |
gcc-toolset-13-binutils-devel | 2.40-19.el8 | |
gcc-toolset-13-binutils-gold | 2.40-19.el8 | |
go-toolset | 1.21.7-1.module_el8+960+4060efbe | |
golang | 1.21.7-1.module_el8+960+4060efbe | |
golang-bin | 1.21.7-1.module_el8+960+4060efbe | |
golang-docs | 1.21.7-1.module_el8+960+4060efbe | |
golang-misc | 1.21.7-1.module_el8+960+4060efbe | |
golang-src | 1.21.7-1.module_el8+960+4060efbe | |
golang-tests | 1.21.7-1.module_el8+960+4060efbe | |
Judy | 1.0.5-18.module_el8+852+a3891817 | |
leapp-deps | 0.17.0-1.el8 | |
mariadb | 10.11.6-1.module_el8+956+53fd50ff | |
mariadb-backup | 10.11.6-1.module_el8+956+53fd50ff | |
mariadb-common | 10.11.6-1.module_el8+956+53fd50ff | |
mariadb-devel | 10.11.6-1.module_el8+956+53fd50ff | |
mariadb-embedded | 10.11.6-1.module_el8+956+53fd50ff | |
mariadb-embedded-devel | 10.11.6-1.module_el8+956+53fd50ff | |
mariadb-errmsg | 10.11.6-1.module_el8+956+53fd50ff | |
mariadb-gssapi-server | 10.11.6-1.module_el8+956+53fd50ff | |
mariadb-oqgraph-engine | 10.11.6-1.module_el8+956+53fd50ff | |
mariadb-pam | 10.11.6-1.module_el8+956+53fd50ff | |
mariadb-server | 10.11.6-1.module_el8+956+53fd50ff | |
mariadb-server-galera | 10.11.6-1.module_el8+956+53fd50ff | |
mariadb-server-utils | 10.11.6-1.module_el8+956+53fd50ff | |
mariadb-test | 10.11.6-1.module_el8+956+53fd50ff | |
netavark | 1.10.3-1.module_el8+951+32019cde | |
python3-leapp | 0.17.0-1.el8 | |
python3-lib389 | 1.4.3.39-2.module_el8+909+03085a27 | |
python3-pip | 9.0.3-24.el8 | |
python3.11-pip | 22.3.1-5.el8 | |
python3.11-pip-wheel | 22.3.1-5.el8 | |
python39-pip | 20.2.4-9.module_el8+963+a9c12c76 | |
python39-pip-wheel | 20.2.4-9.module_el8+963+a9c12c76 | |
qgpgme | 1.13.1-12.el8 | |
rhc | 0.2.4-5.el8 | |
rhel-system-roles | 1.23.0-2.17.el8 | |
slirp4netns | 1.2.3-1.module_el8+951+32019cde | |
snactor | 0.17.0-1.el8 | |
squid | 4.15-9.module_el8+880+b94e8375 | |
stalld | 1.19.1-4.el8 | |
tftp | 5.2-27.el8 | |
tftp-server | 5.2-27.el8 | |
tlog | 14-1.el8 | |
tuned-gtk | 2.22.0-0.1.rc1.el8 | |
tuned-profiles-postgresql | 2.22.0-0.1.rc1.el8 | |
tuned-utils | 2.22.0-0.1.rc1.el8 | |
tuned-utils-systemtap | 2.22.0-0.1.rc1.el8 | |
tzdata-java | 2024a-1.el8 | [RHBA-2024:0762](https://access.redhat.com/errata/RHBA-2024:0762) | <div class="adv_b">Bug Fix Advisory</div>

### PowerTools aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
gpgme-devel | 1.13.1-12.el8 | |
gpgmepp-devel | 1.13.1-12.el8 | |
kernel-tools-libs-devel | 4.18.0-540.el8 | |
opencryptoki-devel | 3.22.0-2.el8 | |
qgpgme-devel | 1.13.1-12.el8 | |

