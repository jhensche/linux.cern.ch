## 2022-03-18

### BaseOS x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
dnf-plugin-subscription-manager | 1.28.28-1.el8 | |
libblkid | 2.32.1-35.el8 | |
libblkid-devel | 2.32.1-35.el8 | |
libfdisk | 2.32.1-35.el8 | |
libfdisk-devel | 2.32.1-35.el8 | |
libipa_hbac | 2.6.2-3.el8 | |
libmount | 2.32.1-35.el8 | |
libsmartcols | 2.32.1-35.el8 | |
libsmartcols-devel | 2.32.1-35.el8 | |
libsss_autofs | 2.6.2-3.el8 | |
libsss_certmap | 2.6.2-3.el8 | |
libsss_idmap | 2.6.2-3.el8 | |
libsss_nss_idmap | 2.6.2-3.el8 | |
libsss_simpleifp | 2.6.2-3.el8 | |
libsss_sudo | 2.6.2-3.el8 | |
libuuid | 2.32.1-35.el8 | |
libuuid-devel | 2.32.1-35.el8 | |
mcelog | 180-0.el8 | |
NetworkManager | 1.37.2-1.el8 | |
NetworkManager-adsl | 1.37.2-1.el8 | |
NetworkManager-bluetooth | 1.37.2-1.el8 | |
NetworkManager-config-connectivity-redhat | 1.37.2-1.el8 | |
NetworkManager-config-server | 1.37.2-1.el8 | |
NetworkManager-dispatcher-routing-rules | 1.37.2-1.el8 | |
NetworkManager-initscripts-updown | 1.37.2-1.el8 | |
NetworkManager-libnm | 1.37.2-1.el8 | |
NetworkManager-ovs | 1.37.2-1.el8 | |
NetworkManager-ppp | 1.37.2-1.el8 | |
NetworkManager-team | 1.37.2-1.el8 | |
NetworkManager-tui | 1.37.2-1.el8 | |
NetworkManager-wifi | 1.37.2-1.el8 | |
NetworkManager-wwan | 1.37.2-1.el8 | |
python3-cloud-what | 1.28.28-1.el8 | |
python3-libipa_hbac | 2.6.2-3.el8 | |
python3-libsss_nss_idmap | 2.6.2-3.el8 | |
python3-sss | 2.6.2-3.el8 | |
python3-sss-murmur | 2.6.2-3.el8 | |
python3-sssdconfig | 2.6.2-3.el8 | |
python3-subscription-manager-rhsm | 1.28.28-1.el8 | |
python3-syspurpose | 1.28.28-1.el8 | |
rhsm-icons | 1.28.28-1.el8 | |
selinux-policy | 3.14.3-94.el8 | |
selinux-policy-devel | 3.14.3-94.el8 | |
selinux-policy-doc | 3.14.3-94.el8 | |
selinux-policy-minimum | 3.14.3-94.el8 | |
selinux-policy-mls | 3.14.3-94.el8 | |
selinux-policy-sandbox | 3.14.3-94.el8 | |
selinux-policy-targeted | 3.14.3-94.el8 | |
sssd | 2.6.2-3.el8 | |
sssd-ad | 2.6.2-3.el8 | |
sssd-client | 2.6.2-3.el8 | |
sssd-common | 2.6.2-3.el8 | |
sssd-common-pac | 2.6.2-3.el8 | |
sssd-dbus | 2.6.2-3.el8 | |
sssd-ipa | 2.6.2-3.el8 | |
sssd-kcm | 2.6.2-3.el8 | |
sssd-krb5 | 2.6.2-3.el8 | |
sssd-krb5-common | 2.6.2-3.el8 | |
sssd-ldap | 2.6.2-3.el8 | |
sssd-nfs-idmap | 2.6.2-3.el8 | |
sssd-polkit-rules | 2.6.2-3.el8 | |
sssd-proxy | 2.6.2-3.el8 | |
sssd-tools | 2.6.2-3.el8 | |
sssd-winbind-idmap | 2.6.2-3.el8 | |
subscription-manager | 1.28.28-1.el8 | |
subscription-manager-cockpit | 1.28.28-1.el8 | |
subscription-manager-plugin-ostree | 1.28.28-1.el8 | |
subscription-manager-rhsm-certificates | 1.28.28-1.el8 | |
util-linux | 2.32.1-35.el8 | |
util-linux-user | 2.32.1-35.el8 | |
uuidd | 2.32.1-35.el8 | |

### AppStream x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
389-ds-base | 1.4.3.28-6.module_el8.6.0+1102+fe5d910f | |
389-ds-base-devel | 1.4.3.28-6.module_el8.6.0+1102+fe5d910f | |
389-ds-base-legacy-tools | 1.4.3.28-6.module_el8.6.0+1102+fe5d910f | |
389-ds-base-libs | 1.4.3.28-6.module_el8.6.0+1102+fe5d910f | |
389-ds-base-snmp | 1.4.3.28-6.module_el8.6.0+1102+fe5d910f | |
aardvark-dns | 1.0.0-18.module_el8.6.0+1108+b13568aa | |
aardvark-dns | 1.0.1-23.module_el8.7.0+1106+45480ee0 | |
anaconda | 33.16.6.7-1.el8 | |
anaconda-core | 33.16.6.7-1.el8 | |
anaconda-dracut | 33.16.6.7-1.el8 | |
anaconda-gui | 33.16.6.7-1.el8 | |
anaconda-install-env-deps | 33.16.6.7-1.el8 | |
anaconda-tui | 33.16.6.7-1.el8 | |
anaconda-widgets | 33.16.6.7-1.el8 | |
ansible-core | 2.12.2-3.el8 | |
ansible-test | 2.12.2-3.el8 | |
bind-dyndb-ldap | 11.6-3.module_el8.6.0+1103+a004f6a8 | |
buildah | 1.19.9-1.module_el8.6.0+1107+d59a301b | |
buildah | 1.24.2-2.module_el8.6.0+1108+b13568aa | |
buildah | 1.24.2-2.module_el8.7.0+1106+45480ee0 | |
buildah-tests | 1.19.9-1.module_el8.6.0+1107+d59a301b | |
buildah-tests | 1.24.2-2.module_el8.6.0+1108+b13568aa | |
buildah-tests | 1.24.2-2.module_el8.7.0+1106+45480ee0 | |
cockpit-podman | 29-2.module_el8.6.0+1107+d59a301b | |
cockpit-podman | 42-1.module_el8.6.0+1108+b13568aa | |
cockpit-podman | 43-1.module_el8.7.0+1106+45480ee0 | |
conmon | 2.0.26-1.module_el8.6.0+1107+d59a301b | |
conmon | 2.1.0-1.module_el8.6.0+1108+b13568aa | |
conmon | 2.1.0-1.module_el8.7.0+1106+45480ee0 | |
container-selinux | 2.167.0-1.module_el8.6.0+1107+d59a301b | |
container-selinux | 2.178.0-1.module_el8.6.0+1108+b13568aa | |
container-selinux | 2.180.0-1.module_el8.7.0+1106+45480ee0 | |
containernetworking-plugins | 0.9.1-1.module_el8.6.0+1107+d59a301b | |
containernetworking-plugins | 1.0.1-1.module_el8.6.0+1108+b13568aa | |
containernetworking-plugins | 1.1.1-1.module_el8.7.0+1106+45480ee0 | |
containers-common | 1-19.module_el8.6.0+1108+b13568aa | |
containers-common | 1-23.module_el8.7.0+1106+45480ee0 | |
containers-common | 1.2.4-1.module_el8.6.0+1107+d59a301b | |
crit | 3.15-1.module_el8.4.0+786+4668b267 | |
crit | 3.15-3.module_el8.6.0+1108+b13568aa | |
criu | 3.15-1.module_el8.4.0+786+4668b267 | |
criu | 3.15-3.module_el8.6.0+1108+b13568aa | |
criu-devel | 3.15-3.module_el8.6.0+1108+b13568aa | |
criu-libs | 3.15-3.module_el8.6.0+1108+b13568aa | |
crun | 0.18-2.module_el8.6.0+1107+d59a301b | |
crun | 1.4.2-1.module_el8.6.0+1108+b13568aa | |
crun | 1.4.3-1.module_el8.7.0+1106+45480ee0 | |
fuse-overlayfs | 1.4.0-2.module_el8.6.0+1107+d59a301b | |
fuse-overlayfs | 1.8.2-1.module_el8.6.0+1108+b13568aa | |
fuse-overlayfs | 1.8.2-1.module_el8.7.0+1106+45480ee0 | |
ipa-client | 4.9.8-6.module_el8.6.0+1104+ba556574 | |
ipa-client | 4.9.8-7.module_el8.6.0+1103+a004f6a8 | |
ipa-client-common | 4.9.8-6.module_el8.6.0+1104+ba556574 | |
ipa-client-common | 4.9.8-7.module_el8.6.0+1103+a004f6a8 | |
ipa-client-epn | 4.9.8-6.module_el8.6.0+1104+ba556574 | |
ipa-client-epn | 4.9.8-7.module_el8.6.0+1103+a004f6a8 | |
ipa-client-samba | 4.9.8-6.module_el8.6.0+1104+ba556574 | |
ipa-client-samba | 4.9.8-7.module_el8.6.0+1103+a004f6a8 | |
ipa-common | 4.9.8-6.module_el8.6.0+1104+ba556574 | |
ipa-common | 4.9.8-7.module_el8.6.0+1103+a004f6a8 | |
ipa-healthcheck | 0.7-10.module_el8.6.0+1103+a004f6a8 | |
ipa-healthcheck-core | 0.7-10.module_el8.6.0+1103+a004f6a8 | |
ipa-healthcheck-core | 0.7-10.module_el8.6.0+1104+ba556574 | |
ipa-python-compat | 4.9.8-6.module_el8.6.0+1104+ba556574 | |
ipa-python-compat | 4.9.8-7.module_el8.6.0+1103+a004f6a8 | |
ipa-selinux | 4.9.8-6.module_el8.6.0+1104+ba556574 | |
ipa-selinux | 4.9.8-7.module_el8.6.0+1103+a004f6a8 | |
ipa-server | 4.9.8-7.module_el8.6.0+1103+a004f6a8 | |
ipa-server-common | 4.9.8-7.module_el8.6.0+1103+a004f6a8 | |
ipa-server-dns | 4.9.8-7.module_el8.6.0+1103+a004f6a8 | |
ipa-server-trust-ad | 4.9.8-7.module_el8.6.0+1103+a004f6a8 | |
java-11-openjdk | 11.0.14.1.1-2.el8 | [RHBA-2022:0887](https://access.redhat.com/errata/RHBA-2022:0887) | <div class="adv_b">Bug Fix Advisory</div>
java-11-openjdk-demo | 11.0.14.1.1-2.el8 | [RHBA-2022:0887](https://access.redhat.com/errata/RHBA-2022:0887) | <div class="adv_b">Bug Fix Advisory</div>
java-11-openjdk-devel | 11.0.14.1.1-2.el8 | [RHBA-2022:0887](https://access.redhat.com/errata/RHBA-2022:0887) | <div class="adv_b">Bug Fix Advisory</div>
java-11-openjdk-headless | 11.0.14.1.1-2.el8 | [RHBA-2022:0887](https://access.redhat.com/errata/RHBA-2022:0887) | <div class="adv_b">Bug Fix Advisory</div>
java-11-openjdk-javadoc | 11.0.14.1.1-2.el8 | [RHBA-2022:0887](https://access.redhat.com/errata/RHBA-2022:0887) | <div class="adv_b">Bug Fix Advisory</div>
java-11-openjdk-javadoc-zip | 11.0.14.1.1-2.el8 | [RHBA-2022:0887](https://access.redhat.com/errata/RHBA-2022:0887) | <div class="adv_b">Bug Fix Advisory</div>
java-11-openjdk-jmods | 11.0.14.1.1-2.el8 | [RHBA-2022:0887](https://access.redhat.com/errata/RHBA-2022:0887) | <div class="adv_b">Bug Fix Advisory</div>
java-11-openjdk-src | 11.0.14.1.1-2.el8 | [RHBA-2022:0887](https://access.redhat.com/errata/RHBA-2022:0887) | <div class="adv_b">Bug Fix Advisory</div>
java-11-openjdk-static-libs | 11.0.14.1.1-2.el8 | [RHBA-2022:0887](https://access.redhat.com/errata/RHBA-2022:0887) | <div class="adv_b">Bug Fix Advisory</div>
jigawatts | 1.21.0.0.0-3.el8 | |
libslirp | 4.3.1-1.module_el8.4.0+786+4668b267 | |
libslirp | 4.4.0-1.module_el8.6.0+1108+b13568aa | |
libslirp-devel | 4.3.1-1.module_el8.4.0+786+4668b267 | |
libslirp-devel | 4.4.0-1.module_el8.6.0+1108+b13568aa | |
netavark | 1.0.0-18.module_el8.6.0+1108+b13568aa | |
netavark | 1.0.1-23.module_el8.7.0+1106+45480ee0 | |
NetworkManager-cloud-setup | 1.37.2-1.el8 | |
oci-seccomp-bpf-hook | 1.2.0-3.module_el8.6.0+1107+d59a301b | |
oci-seccomp-bpf-hook | 1.2.3-3.module_el8.6.0+1108+b13568aa | |
plymouth | 0.9.4-10.20200615git1e36e30.el8.1 | |
plymouth-core-libs | 0.9.4-10.20200615git1e36e30.el8.1 | |
plymouth-graphics-libs | 0.9.4-10.20200615git1e36e30.el8.1 | |
plymouth-plugin-fade-throbber | 0.9.4-10.20200615git1e36e30.el8.1 | |
plymouth-plugin-label | 0.9.4-10.20200615git1e36e30.el8.1 | |
plymouth-plugin-script | 0.9.4-10.20200615git1e36e30.el8.1 | |
plymouth-plugin-space-flares | 0.9.4-10.20200615git1e36e30.el8.1 | |
plymouth-plugin-throbgress | 0.9.4-10.20200615git1e36e30.el8.1 | |
plymouth-plugin-two-step | 0.9.4-10.20200615git1e36e30.el8.1 | |
plymouth-scripts | 0.9.4-10.20200615git1e36e30.el8.1 | |
plymouth-system-theme | 0.9.4-10.20200615git1e36e30.el8.1 | |
plymouth-theme-charge | 0.9.4-10.20200615git1e36e30.el8.1 | |
plymouth-theme-fade-in | 0.9.4-10.20200615git1e36e30.el8.1 | |
plymouth-theme-script | 0.9.4-10.20200615git1e36e30.el8.1 | |
plymouth-theme-solar | 0.9.4-10.20200615git1e36e30.el8.1 | |
plymouth-theme-spinfinity | 0.9.4-10.20200615git1e36e30.el8.1 | |
plymouth-theme-spinner | 0.9.4-10.20200615git1e36e30.el8.1 | |
podman | 3.0.1-7.module_el8.6.0+1107+d59a301b | |
podman | 4.0.0-1.module_el8.6.0+1108+b13568aa | |
podman | 4.0.2-1.module_el8.7.0+1106+45480ee0 | |
podman-catatonit | 3.0.1-7.module_el8.6.0+1107+d59a301b | |
podman-catatonit | 4.0.0-1.module_el8.6.0+1108+b13568aa | |
podman-catatonit | 4.0.2-1.module_el8.7.0+1106+45480ee0 | |
podman-docker | 3.0.1-7.module_el8.6.0+1107+d59a301b | |
podman-docker | 4.0.0-1.module_el8.6.0+1108+b13568aa | |
podman-docker | 4.0.2-1.module_el8.7.0+1106+45480ee0 | |
podman-gvproxy | 4.0.0-1.module_el8.6.0+1108+b13568aa | |
podman-gvproxy | 4.0.2-1.module_el8.7.0+1106+45480ee0 | |
podman-plugins | 3.0.1-7.module_el8.6.0+1107+d59a301b | |
podman-plugins | 4.0.0-1.module_el8.6.0+1108+b13568aa | |
podman-plugins | 4.0.2-1.module_el8.7.0+1106+45480ee0 | |
podman-remote | 3.0.1-7.module_el8.6.0+1107+d59a301b | |
podman-remote | 4.0.0-1.module_el8.6.0+1108+b13568aa | |
podman-remote | 4.0.2-1.module_el8.7.0+1106+45480ee0 | |
podman-tests | 3.0.1-7.module_el8.6.0+1107+d59a301b | |
podman-tests | 4.0.0-1.module_el8.6.0+1108+b13568aa | |
podman-tests | 4.0.2-1.module_el8.7.0+1106+45480ee0 | |
python3-criu | 3.15-1.module_el8.4.0+786+4668b267 | |
python3-criu | 3.15-3.module_el8.6.0+1108+b13568aa | |
python3-ipaclient | 4.9.8-6.module_el8.6.0+1104+ba556574 | |
python3-ipaclient | 4.9.8-7.module_el8.6.0+1103+a004f6a8 | |
python3-ipalib | 4.9.8-6.module_el8.6.0+1104+ba556574 | |
python3-ipalib | 4.9.8-7.module_el8.6.0+1103+a004f6a8 | |
python3-ipaserver | 4.9.8-7.module_el8.6.0+1103+a004f6a8 | |
python3-ipatests | 4.9.8-7.module_el8.6.0+1103+a004f6a8 | |
python3-lib389 | 1.4.3.28-6.module_el8.6.0+1102+fe5d910f | |
python3-libmount | 2.32.1-35.el8 | |
python3-podman | 3.2.1-4.module_el8.6.0+1108+b13568aa | |
python3-podman | 4.0.0-1.module_el8.7.0+1106+45480ee0 | |
ruby | 2.5.9-109.module_el8.5.0+1097+b05a0601 | |
ruby-devel | 2.5.9-109.module_el8.5.0+1097+b05a0601 | |
ruby-doc | 2.5.9-109.module_el8.5.0+1097+b05a0601 | |
ruby-irb | 2.5.9-109.module_el8.5.0+1097+b05a0601 | |
ruby-libs | 2.5.9-109.module_el8.5.0+1097+b05a0601 | |
rubygem-bigdecimal | 1.3.4-109.module_el8.5.0+1097+b05a0601 | |
rubygem-bundler | 1.16.1-4.module_el8.5.0+1097+b05a0601 | |
rubygem-bundler-doc | 1.16.1-4.module_el8.5.0+1097+b05a0601 | |
rubygem-did_you_mean | 1.2.0-109.module_el8.5.0+1097+b05a0601 | |
rubygem-io-console | 0.4.6-109.module_el8.5.0+1097+b05a0601 | |
rubygem-json | 2.1.0-109.module_el8.5.0+1097+b05a0601 | |
rubygem-minitest | 5.10.3-109.module_el8.5.0+1097+b05a0601 | |
rubygem-net-telnet | 0.1.1-109.module_el8.5.0+1097+b05a0601 | |
rubygem-openssl | 2.1.2-109.module_el8.5.0+1097+b05a0601 | |
rubygem-power_assert | 1.1.1-109.module_el8.5.0+1097+b05a0601 | |
rubygem-psych | 3.0.2-109.module_el8.5.0+1097+b05a0601 | |
rubygem-rake | 12.3.3-109.module_el8.5.0+1097+b05a0601 | |
rubygem-rdoc | 6.0.1.1-109.module_el8.5.0+1097+b05a0601 | |
rubygem-test-unit | 3.2.7-109.module_el8.5.0+1097+b05a0601 | |
rubygem-xmlrpc | 0.3.0-109.module_el8.5.0+1097+b05a0601 | |
rubygems | 2.7.6.3-109.module_el8.5.0+1097+b05a0601 | |
rubygems-devel | 2.7.6.3-109.module_el8.5.0+1097+b05a0601 | |
runc | 1.0.0-73.rc95.module_el8.6.0+1107+d59a301b | |
runc | 1.0.3-1.module_el8.6.0+1108+b13568aa | |
runc | 1.0.3-3.module_el8.7.0+1106+45480ee0 | |
skopeo | 1.2.4-1.module_el8.6.0+1107+d59a301b | |
skopeo | 1.6.1-1.module_el8.6.0+1108+b13568aa | |
skopeo | 1.6.1-1.module_el8.7.0+1106+45480ee0 | |
skopeo-tests | 1.2.4-1.module_el8.6.0+1107+d59a301b | |
skopeo-tests | 1.6.1-1.module_el8.6.0+1108+b13568aa | |
skopeo-tests | 1.6.1-1.module_el8.7.0+1106+45480ee0 | |
slirp4netns | 1.1.8-1.module_el8.4.0+786+4668b267 | |
slirp4netns | 1.1.8-2.module_el8.6.0+1108+b13568aa | |
slirp4netns | 1.1.8-2.module_el8.7.0+1106+45480ee0 | |
subscription-manager-migration | 1.28.28-1.el8 | |
thunderbird | 91.7.0-2.el8 | [RHSA-2022:0845](https://access.redhat.com/errata/RHSA-2022:0845) | <div class="adv_s">Security Advisory</div>
toolbox | 0.0.99.3-0.4.module_el8.6.0+1108+b13568aa | |
toolbox | 0.0.99.3-1.module_el8.6.0+1107+d59a301b | |
toolbox-tests | 0.0.99.3-0.4.module_el8.6.0+1108+b13568aa | |
toolbox-tests | 0.0.99.3-1.module_el8.6.0+1107+d59a301b | |
udica | 0.2.4-1.module_el8.4.0+786+4668b267 | |
udica | 0.2.6-2.module_el8.6.0+1108+b13568aa | |

### PowerTools x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
java-11-openjdk-demo-fastdebug | 11.0.14.1.1-2.el8 | [RHBA-2022:0887](https://access.redhat.com/errata/RHBA-2022:0887) | <div class="adv_b">Bug Fix Advisory</div>
java-11-openjdk-demo-slowdebug | 11.0.14.1.1-2.el8 | [RHBA-2022:0887](https://access.redhat.com/errata/RHBA-2022:0887) | <div class="adv_b">Bug Fix Advisory</div>
java-11-openjdk-devel-fastdebug | 11.0.14.1.1-2.el8 | [RHBA-2022:0887](https://access.redhat.com/errata/RHBA-2022:0887) | <div class="adv_b">Bug Fix Advisory</div>
java-11-openjdk-devel-slowdebug | 11.0.14.1.1-2.el8 | [RHBA-2022:0887](https://access.redhat.com/errata/RHBA-2022:0887) | <div class="adv_b">Bug Fix Advisory</div>
java-11-openjdk-fastdebug | 11.0.14.1.1-2.el8 | [RHBA-2022:0887](https://access.redhat.com/errata/RHBA-2022:0887) | <div class="adv_b">Bug Fix Advisory</div>
java-11-openjdk-headless-fastdebug | 11.0.14.1.1-2.el8 | [RHBA-2022:0887](https://access.redhat.com/errata/RHBA-2022:0887) | <div class="adv_b">Bug Fix Advisory</div>
java-11-openjdk-headless-slowdebug | 11.0.14.1.1-2.el8 | [RHBA-2022:0887](https://access.redhat.com/errata/RHBA-2022:0887) | <div class="adv_b">Bug Fix Advisory</div>
java-11-openjdk-jmods-fastdebug | 11.0.14.1.1-2.el8 | [RHBA-2022:0887](https://access.redhat.com/errata/RHBA-2022:0887) | <div class="adv_b">Bug Fix Advisory</div>
java-11-openjdk-jmods-slowdebug | 11.0.14.1.1-2.el8 | [RHBA-2022:0887](https://access.redhat.com/errata/RHBA-2022:0887) | <div class="adv_b">Bug Fix Advisory</div>
java-11-openjdk-slowdebug | 11.0.14.1.1-2.el8 | [RHBA-2022:0887](https://access.redhat.com/errata/RHBA-2022:0887) | <div class="adv_b">Bug Fix Advisory</div>
java-11-openjdk-src-fastdebug | 11.0.14.1.1-2.el8 | [RHBA-2022:0887](https://access.redhat.com/errata/RHBA-2022:0887) | <div class="adv_b">Bug Fix Advisory</div>
java-11-openjdk-src-slowdebug | 11.0.14.1.1-2.el8 | [RHBA-2022:0887](https://access.redhat.com/errata/RHBA-2022:0887) | <div class="adv_b">Bug Fix Advisory</div>
java-11-openjdk-static-libs-fastdebug | 11.0.14.1.1-2.el8 | [RHBA-2022:0887](https://access.redhat.com/errata/RHBA-2022:0887) | <div class="adv_b">Bug Fix Advisory</div>
java-11-openjdk-static-libs-slowdebug | 11.0.14.1.1-2.el8 | [RHBA-2022:0887](https://access.redhat.com/errata/RHBA-2022:0887) | <div class="adv_b">Bug Fix Advisory</div>
libmount-devel | 2.32.1-35.el8 | |
libsss_nss_idmap-devel | 2.6.2-3.el8 | |
NetworkManager-libnm-devel | 1.37.2-1.el8 | |

### BaseOS aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
dnf-plugin-subscription-manager | 1.28.28-1.el8 | |
libblkid | 2.32.1-35.el8 | |
libblkid-devel | 2.32.1-35.el8 | |
libfdisk | 2.32.1-35.el8 | |
libfdisk-devel | 2.32.1-35.el8 | |
libipa_hbac | 2.6.2-3.el8 | |
libmount | 2.32.1-35.el8 | |
libsmartcols | 2.32.1-35.el8 | |
libsmartcols-devel | 2.32.1-35.el8 | |
libsss_autofs | 2.6.2-3.el8 | |
libsss_certmap | 2.6.2-3.el8 | |
libsss_idmap | 2.6.2-3.el8 | |
libsss_nss_idmap | 2.6.2-3.el8 | |
libsss_simpleifp | 2.6.2-3.el8 | |
libsss_sudo | 2.6.2-3.el8 | |
libuuid | 2.32.1-35.el8 | |
libuuid-devel | 2.32.1-35.el8 | |
NetworkManager | 1.37.2-1.el8 | |
NetworkManager-adsl | 1.37.2-1.el8 | |
NetworkManager-bluetooth | 1.37.2-1.el8 | |
NetworkManager-config-connectivity-redhat | 1.37.2-1.el8 | |
NetworkManager-config-server | 1.37.2-1.el8 | |
NetworkManager-dispatcher-routing-rules | 1.37.2-1.el8 | |
NetworkManager-initscripts-updown | 1.37.2-1.el8 | |
NetworkManager-libnm | 1.37.2-1.el8 | |
NetworkManager-ovs | 1.37.2-1.el8 | |
NetworkManager-ppp | 1.37.2-1.el8 | |
NetworkManager-team | 1.37.2-1.el8 | |
NetworkManager-tui | 1.37.2-1.el8 | |
NetworkManager-wifi | 1.37.2-1.el8 | |
NetworkManager-wwan | 1.37.2-1.el8 | |
python3-cloud-what | 1.28.28-1.el8 | |
python3-libipa_hbac | 2.6.2-3.el8 | |
python3-libsss_nss_idmap | 2.6.2-3.el8 | |
python3-sss | 2.6.2-3.el8 | |
python3-sss-murmur | 2.6.2-3.el8 | |
python3-sssdconfig | 2.6.2-3.el8 | |
python3-subscription-manager-rhsm | 1.28.28-1.el8 | |
python3-syspurpose | 1.28.28-1.el8 | |
rhsm-icons | 1.28.28-1.el8 | |
selinux-policy | 3.14.3-94.el8 | |
selinux-policy-devel | 3.14.3-94.el8 | |
selinux-policy-doc | 3.14.3-94.el8 | |
selinux-policy-minimum | 3.14.3-94.el8 | |
selinux-policy-mls | 3.14.3-94.el8 | |
selinux-policy-sandbox | 3.14.3-94.el8 | |
selinux-policy-targeted | 3.14.3-94.el8 | |
sssd | 2.6.2-3.el8 | |
sssd-ad | 2.6.2-3.el8 | |
sssd-client | 2.6.2-3.el8 | |
sssd-common | 2.6.2-3.el8 | |
sssd-common-pac | 2.6.2-3.el8 | |
sssd-dbus | 2.6.2-3.el8 | |
sssd-ipa | 2.6.2-3.el8 | |
sssd-kcm | 2.6.2-3.el8 | |
sssd-krb5 | 2.6.2-3.el8 | |
sssd-krb5-common | 2.6.2-3.el8 | |
sssd-ldap | 2.6.2-3.el8 | |
sssd-nfs-idmap | 2.6.2-3.el8 | |
sssd-polkit-rules | 2.6.2-3.el8 | |
sssd-proxy | 2.6.2-3.el8 | |
sssd-tools | 2.6.2-3.el8 | |
sssd-winbind-idmap | 2.6.2-3.el8 | |
subscription-manager | 1.28.28-1.el8 | |
subscription-manager-cockpit | 1.28.28-1.el8 | |
subscription-manager-plugin-ostree | 1.28.28-1.el8 | |
subscription-manager-rhsm-certificates | 1.28.28-1.el8 | |
util-linux | 2.32.1-35.el8 | |
util-linux-user | 2.32.1-35.el8 | |
uuidd | 2.32.1-35.el8 | |

### AppStream aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
389-ds-base | 1.4.3.28-6.module_el8.6.0+1102+fe5d910f | |
389-ds-base-devel | 1.4.3.28-6.module_el8.6.0+1102+fe5d910f | |
389-ds-base-legacy-tools | 1.4.3.28-6.module_el8.6.0+1102+fe5d910f | |
389-ds-base-libs | 1.4.3.28-6.module_el8.6.0+1102+fe5d910f | |
389-ds-base-snmp | 1.4.3.28-6.module_el8.6.0+1102+fe5d910f | |
aardvark-dns | 1.0.0-18.module_el8.6.0+1108+b13568aa | |
aardvark-dns | 1.0.1-23.module_el8.7.0+1106+45480ee0 | |
anaconda | 33.16.6.7-1.el8 | |
anaconda-core | 33.16.6.7-1.el8 | |
anaconda-dracut | 33.16.6.7-1.el8 | |
anaconda-gui | 33.16.6.7-1.el8 | |
anaconda-install-env-deps | 33.16.6.7-1.el8 | |
anaconda-tui | 33.16.6.7-1.el8 | |
anaconda-widgets | 33.16.6.7-1.el8 | |
ansible-core | 2.12.2-3.el8 | |
ansible-test | 2.12.2-3.el8 | |
bind-dyndb-ldap | 11.6-3.module_el8.6.0+1103+a004f6a8 | |
buildah | 1.19.9-1.module_el8.6.0+1107+d59a301b | |
buildah | 1.24.2-2.module_el8.6.0+1108+b13568aa | |
buildah | 1.24.2-2.module_el8.7.0+1106+45480ee0 | |
buildah-tests | 1.19.9-1.module_el8.6.0+1107+d59a301b | |
buildah-tests | 1.24.2-2.module_el8.6.0+1108+b13568aa | |
buildah-tests | 1.24.2-2.module_el8.7.0+1106+45480ee0 | |
cockpit-podman | 29-2.module_el8.6.0+1107+d59a301b | |
cockpit-podman | 42-1.module_el8.6.0+1108+b13568aa | |
cockpit-podman | 43-1.module_el8.7.0+1106+45480ee0 | |
conmon | 2.0.26-1.module_el8.6.0+1107+d59a301b | |
conmon | 2.1.0-1.module_el8.6.0+1108+b13568aa | |
conmon | 2.1.0-1.module_el8.7.0+1106+45480ee0 | |
container-selinux | 2.167.0-1.module_el8.6.0+1107+d59a301b | |
container-selinux | 2.178.0-1.module_el8.6.0+1108+b13568aa | |
container-selinux | 2.180.0-1.module_el8.7.0+1106+45480ee0 | |
containernetworking-plugins | 0.9.1-1.module_el8.6.0+1107+d59a301b | |
containernetworking-plugins | 1.0.1-1.module_el8.6.0+1108+b13568aa | |
containernetworking-plugins | 1.1.1-1.module_el8.7.0+1106+45480ee0 | |
containers-common | 1-19.module_el8.6.0+1108+b13568aa | |
containers-common | 1-23.module_el8.7.0+1106+45480ee0 | |
containers-common | 1.2.4-1.module_el8.6.0+1107+d59a301b | |
crit | 3.15-1.module_el8.4.0+786+4668b267 | |
crit | 3.15-3.module_el8.6.0+1108+b13568aa | |
criu | 3.15-1.module_el8.4.0+786+4668b267 | |
criu | 3.15-3.module_el8.6.0+1108+b13568aa | |
criu-devel | 3.15-3.module_el8.6.0+1108+b13568aa | |
criu-libs | 3.15-3.module_el8.6.0+1108+b13568aa | |
crun | 0.18-2.module_el8.6.0+1107+d59a301b | |
crun | 1.4.2-1.module_el8.6.0+1108+b13568aa | |
crun | 1.4.3-1.module_el8.7.0+1106+45480ee0 | |
fuse-overlayfs | 1.4.0-2.module_el8.6.0+1107+d59a301b | |
fuse-overlayfs | 1.8.2-1.module_el8.6.0+1108+b13568aa | |
fuse-overlayfs | 1.8.2-1.module_el8.7.0+1106+45480ee0 | |
ipa-client | 4.9.8-6.module_el8.6.0+1104+ba556574 | |
ipa-client | 4.9.8-7.module_el8.6.0+1103+a004f6a8 | |
ipa-client-common | 4.9.8-6.module_el8.6.0+1104+ba556574 | |
ipa-client-common | 4.9.8-7.module_el8.6.0+1103+a004f6a8 | |
ipa-client-epn | 4.9.8-6.module_el8.6.0+1104+ba556574 | |
ipa-client-epn | 4.9.8-7.module_el8.6.0+1103+a004f6a8 | |
ipa-client-samba | 4.9.8-6.module_el8.6.0+1104+ba556574 | |
ipa-client-samba | 4.9.8-7.module_el8.6.0+1103+a004f6a8 | |
ipa-common | 4.9.8-6.module_el8.6.0+1104+ba556574 | |
ipa-common | 4.9.8-7.module_el8.6.0+1103+a004f6a8 | |
ipa-healthcheck | 0.7-10.module_el8.6.0+1103+a004f6a8 | |
ipa-healthcheck-core | 0.7-10.module_el8.6.0+1103+a004f6a8 | |
ipa-healthcheck-core | 0.7-10.module_el8.6.0+1104+ba556574 | |
ipa-python-compat | 4.9.8-6.module_el8.6.0+1104+ba556574 | |
ipa-python-compat | 4.9.8-7.module_el8.6.0+1103+a004f6a8 | |
ipa-selinux | 4.9.8-6.module_el8.6.0+1104+ba556574 | |
ipa-selinux | 4.9.8-7.module_el8.6.0+1103+a004f6a8 | |
ipa-server | 4.9.8-7.module_el8.6.0+1103+a004f6a8 | |
ipa-server-common | 4.9.8-7.module_el8.6.0+1103+a004f6a8 | |
ipa-server-dns | 4.9.8-7.module_el8.6.0+1103+a004f6a8 | |
ipa-server-trust-ad | 4.9.8-7.module_el8.6.0+1103+a004f6a8 | |
java-11-openjdk | 11.0.14.1.1-2.el8 | [RHBA-2022:0887](https://access.redhat.com/errata/RHBA-2022:0887) | <div class="adv_b">Bug Fix Advisory</div>
java-11-openjdk-demo | 11.0.14.1.1-2.el8 | [RHBA-2022:0887](https://access.redhat.com/errata/RHBA-2022:0887) | <div class="adv_b">Bug Fix Advisory</div>
java-11-openjdk-devel | 11.0.14.1.1-2.el8 | [RHBA-2022:0887](https://access.redhat.com/errata/RHBA-2022:0887) | <div class="adv_b">Bug Fix Advisory</div>
java-11-openjdk-headless | 11.0.14.1.1-2.el8 | [RHBA-2022:0887](https://access.redhat.com/errata/RHBA-2022:0887) | <div class="adv_b">Bug Fix Advisory</div>
java-11-openjdk-javadoc | 11.0.14.1.1-2.el8 | [RHBA-2022:0887](https://access.redhat.com/errata/RHBA-2022:0887) | <div class="adv_b">Bug Fix Advisory</div>
java-11-openjdk-javadoc-zip | 11.0.14.1.1-2.el8 | [RHBA-2022:0887](https://access.redhat.com/errata/RHBA-2022:0887) | <div class="adv_b">Bug Fix Advisory</div>
java-11-openjdk-jmods | 11.0.14.1.1-2.el8 | [RHBA-2022:0887](https://access.redhat.com/errata/RHBA-2022:0887) | <div class="adv_b">Bug Fix Advisory</div>
java-11-openjdk-src | 11.0.14.1.1-2.el8 | [RHBA-2022:0887](https://access.redhat.com/errata/RHBA-2022:0887) | <div class="adv_b">Bug Fix Advisory</div>
java-11-openjdk-static-libs | 11.0.14.1.1-2.el8 | [RHBA-2022:0887](https://access.redhat.com/errata/RHBA-2022:0887) | <div class="adv_b">Bug Fix Advisory</div>
jigawatts | 1.21.0.0.0-3.el8 | |
libslirp | 4.3.1-1.module_el8.4.0+786+4668b267 | |
libslirp | 4.4.0-1.module_el8.6.0+1108+b13568aa | |
libslirp-devel | 4.3.1-1.module_el8.4.0+786+4668b267 | |
libslirp-devel | 4.4.0-1.module_el8.6.0+1108+b13568aa | |
netavark | 1.0.0-18.module_el8.6.0+1108+b13568aa | |
netavark | 1.0.1-23.module_el8.7.0+1106+45480ee0 | |
NetworkManager-cloud-setup | 1.37.2-1.el8 | |
oci-seccomp-bpf-hook | 1.2.0-3.module_el8.6.0+1107+d59a301b | |
oci-seccomp-bpf-hook | 1.2.3-3.module_el8.6.0+1108+b13568aa | |
plymouth | 0.9.4-10.20200615git1e36e30.el8.1 | |
plymouth-core-libs | 0.9.4-10.20200615git1e36e30.el8.1 | |
plymouth-graphics-libs | 0.9.4-10.20200615git1e36e30.el8.1 | |
plymouth-plugin-fade-throbber | 0.9.4-10.20200615git1e36e30.el8.1 | |
plymouth-plugin-label | 0.9.4-10.20200615git1e36e30.el8.1 | |
plymouth-plugin-script | 0.9.4-10.20200615git1e36e30.el8.1 | |
plymouth-plugin-space-flares | 0.9.4-10.20200615git1e36e30.el8.1 | |
plymouth-plugin-throbgress | 0.9.4-10.20200615git1e36e30.el8.1 | |
plymouth-plugin-two-step | 0.9.4-10.20200615git1e36e30.el8.1 | |
plymouth-scripts | 0.9.4-10.20200615git1e36e30.el8.1 | |
plymouth-system-theme | 0.9.4-10.20200615git1e36e30.el8.1 | |
plymouth-theme-charge | 0.9.4-10.20200615git1e36e30.el8.1 | |
plymouth-theme-fade-in | 0.9.4-10.20200615git1e36e30.el8.1 | |
plymouth-theme-script | 0.9.4-10.20200615git1e36e30.el8.1 | |
plymouth-theme-solar | 0.9.4-10.20200615git1e36e30.el8.1 | |
plymouth-theme-spinfinity | 0.9.4-10.20200615git1e36e30.el8.1 | |
plymouth-theme-spinner | 0.9.4-10.20200615git1e36e30.el8.1 | |
podman | 3.0.1-7.module_el8.6.0+1107+d59a301b | |
podman | 4.0.0-1.module_el8.6.0+1108+b13568aa | |
podman | 4.0.2-1.module_el8.7.0+1106+45480ee0 | |
podman-catatonit | 3.0.1-7.module_el8.6.0+1107+d59a301b | |
podman-catatonit | 4.0.0-1.module_el8.6.0+1108+b13568aa | |
podman-catatonit | 4.0.2-1.module_el8.7.0+1106+45480ee0 | |
podman-docker | 3.0.1-7.module_el8.6.0+1107+d59a301b | |
podman-docker | 4.0.0-1.module_el8.6.0+1108+b13568aa | |
podman-docker | 4.0.2-1.module_el8.7.0+1106+45480ee0 | |
podman-gvproxy | 4.0.0-1.module_el8.6.0+1108+b13568aa | |
podman-gvproxy | 4.0.2-1.module_el8.7.0+1106+45480ee0 | |
podman-plugins | 3.0.1-7.module_el8.6.0+1107+d59a301b | |
podman-plugins | 4.0.0-1.module_el8.6.0+1108+b13568aa | |
podman-plugins | 4.0.2-1.module_el8.7.0+1106+45480ee0 | |
podman-remote | 3.0.1-7.module_el8.6.0+1107+d59a301b | |
podman-remote | 4.0.0-1.module_el8.6.0+1108+b13568aa | |
podman-remote | 4.0.2-1.module_el8.7.0+1106+45480ee0 | |
podman-tests | 3.0.1-7.module_el8.6.0+1107+d59a301b | |
podman-tests | 4.0.0-1.module_el8.6.0+1108+b13568aa | |
podman-tests | 4.0.2-1.module_el8.7.0+1106+45480ee0 | |
python3-criu | 3.15-1.module_el8.4.0+786+4668b267 | |
python3-criu | 3.15-3.module_el8.6.0+1108+b13568aa | |
python3-ipaclient | 4.9.8-6.module_el8.6.0+1104+ba556574 | |
python3-ipaclient | 4.9.8-7.module_el8.6.0+1103+a004f6a8 | |
python3-ipalib | 4.9.8-6.module_el8.6.0+1104+ba556574 | |
python3-ipalib | 4.9.8-7.module_el8.6.0+1103+a004f6a8 | |
python3-ipaserver | 4.9.8-7.module_el8.6.0+1103+a004f6a8 | |
python3-ipatests | 4.9.8-7.module_el8.6.0+1103+a004f6a8 | |
python3-lib389 | 1.4.3.28-6.module_el8.6.0+1102+fe5d910f | |
python3-libmount | 2.32.1-35.el8 | |
python3-podman | 3.2.1-4.module_el8.6.0+1108+b13568aa | |
python3-podman | 4.0.0-1.module_el8.7.0+1106+45480ee0 | |
ruby | 2.5.9-109.module_el8.5.0+1097+b05a0601 | |
ruby-devel | 2.5.9-109.module_el8.5.0+1097+b05a0601 | |
ruby-doc | 2.5.9-109.module_el8.5.0+1097+b05a0601 | |
ruby-irb | 2.5.9-109.module_el8.5.0+1097+b05a0601 | |
ruby-libs | 2.5.9-109.module_el8.5.0+1097+b05a0601 | |
rubygem-bigdecimal | 1.3.4-109.module_el8.5.0+1097+b05a0601 | |
rubygem-bundler | 1.16.1-4.module_el8.5.0+1097+b05a0601 | |
rubygem-bundler-doc | 1.16.1-4.module_el8.5.0+1097+b05a0601 | |
rubygem-did_you_mean | 1.2.0-109.module_el8.5.0+1097+b05a0601 | |
rubygem-io-console | 0.4.6-109.module_el8.5.0+1097+b05a0601 | |
rubygem-json | 2.1.0-109.module_el8.5.0+1097+b05a0601 | |
rubygem-minitest | 5.10.3-109.module_el8.5.0+1097+b05a0601 | |
rubygem-net-telnet | 0.1.1-109.module_el8.5.0+1097+b05a0601 | |
rubygem-openssl | 2.1.2-109.module_el8.5.0+1097+b05a0601 | |
rubygem-power_assert | 1.1.1-109.module_el8.5.0+1097+b05a0601 | |
rubygem-psych | 3.0.2-109.module_el8.5.0+1097+b05a0601 | |
rubygem-rake | 12.3.3-109.module_el8.5.0+1097+b05a0601 | |
rubygem-rdoc | 6.0.1.1-109.module_el8.5.0+1097+b05a0601 | |
rubygem-test-unit | 3.2.7-109.module_el8.5.0+1097+b05a0601 | |
rubygem-xmlrpc | 0.3.0-109.module_el8.5.0+1097+b05a0601 | |
rubygems | 2.7.6.3-109.module_el8.5.0+1097+b05a0601 | |
rubygems-devel | 2.7.6.3-109.module_el8.5.0+1097+b05a0601 | |
runc | 1.0.0-73.rc95.module_el8.6.0+1107+d59a301b | |
runc | 1.0.3-1.module_el8.6.0+1108+b13568aa | |
runc | 1.0.3-3.module_el8.7.0+1106+45480ee0 | |
skopeo | 1.2.4-1.module_el8.6.0+1107+d59a301b | |
skopeo | 1.6.1-1.module_el8.6.0+1108+b13568aa | |
skopeo | 1.6.1-1.module_el8.7.0+1106+45480ee0 | |
skopeo-tests | 1.2.4-1.module_el8.6.0+1107+d59a301b | |
skopeo-tests | 1.6.1-1.module_el8.6.0+1108+b13568aa | |
skopeo-tests | 1.6.1-1.module_el8.7.0+1106+45480ee0 | |
slirp4netns | 1.1.8-1.module_el8.4.0+786+4668b267 | |
slirp4netns | 1.1.8-2.module_el8.6.0+1108+b13568aa | |
slirp4netns | 1.1.8-2.module_el8.7.0+1106+45480ee0 | |
subscription-manager-migration | 1.28.28-1.el8 | |
thunderbird | 91.7.0-2.el8 | [RHSA-2022:0845](https://access.redhat.com/errata/RHSA-2022:0845) | <div class="adv_s">Security Advisory</div>
toolbox | 0.0.99.3-0.4.module_el8.6.0+1108+b13568aa | |
toolbox | 0.0.99.3-1.module_el8.6.0+1107+d59a301b | |
toolbox-tests | 0.0.99.3-0.4.module_el8.6.0+1108+b13568aa | |
toolbox-tests | 0.0.99.3-1.module_el8.6.0+1107+d59a301b | |
udica | 0.2.4-1.module_el8.4.0+786+4668b267 | |
udica | 0.2.6-2.module_el8.6.0+1108+b13568aa | |

### PowerTools aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
java-11-openjdk-demo-fastdebug | 11.0.14.1.1-2.el8 | [RHBA-2022:0887](https://access.redhat.com/errata/RHBA-2022:0887) | <div class="adv_b">Bug Fix Advisory</div>
java-11-openjdk-demo-slowdebug | 11.0.14.1.1-2.el8 | [RHBA-2022:0887](https://access.redhat.com/errata/RHBA-2022:0887) | <div class="adv_b">Bug Fix Advisory</div>
java-11-openjdk-devel-fastdebug | 11.0.14.1.1-2.el8 | [RHBA-2022:0887](https://access.redhat.com/errata/RHBA-2022:0887) | <div class="adv_b">Bug Fix Advisory</div>
java-11-openjdk-devel-slowdebug | 11.0.14.1.1-2.el8 | [RHBA-2022:0887](https://access.redhat.com/errata/RHBA-2022:0887) | <div class="adv_b">Bug Fix Advisory</div>
java-11-openjdk-fastdebug | 11.0.14.1.1-2.el8 | [RHBA-2022:0887](https://access.redhat.com/errata/RHBA-2022:0887) | <div class="adv_b">Bug Fix Advisory</div>
java-11-openjdk-headless-fastdebug | 11.0.14.1.1-2.el8 | [RHBA-2022:0887](https://access.redhat.com/errata/RHBA-2022:0887) | <div class="adv_b">Bug Fix Advisory</div>
java-11-openjdk-headless-slowdebug | 11.0.14.1.1-2.el8 | [RHBA-2022:0887](https://access.redhat.com/errata/RHBA-2022:0887) | <div class="adv_b">Bug Fix Advisory</div>
java-11-openjdk-jmods-fastdebug | 11.0.14.1.1-2.el8 | [RHBA-2022:0887](https://access.redhat.com/errata/RHBA-2022:0887) | <div class="adv_b">Bug Fix Advisory</div>
java-11-openjdk-jmods-slowdebug | 11.0.14.1.1-2.el8 | [RHBA-2022:0887](https://access.redhat.com/errata/RHBA-2022:0887) | <div class="adv_b">Bug Fix Advisory</div>
java-11-openjdk-slowdebug | 11.0.14.1.1-2.el8 | [RHBA-2022:0887](https://access.redhat.com/errata/RHBA-2022:0887) | <div class="adv_b">Bug Fix Advisory</div>
java-11-openjdk-src-fastdebug | 11.0.14.1.1-2.el8 | [RHBA-2022:0887](https://access.redhat.com/errata/RHBA-2022:0887) | <div class="adv_b">Bug Fix Advisory</div>
java-11-openjdk-src-slowdebug | 11.0.14.1.1-2.el8 | [RHBA-2022:0887](https://access.redhat.com/errata/RHBA-2022:0887) | <div class="adv_b">Bug Fix Advisory</div>
java-11-openjdk-static-libs-fastdebug | 11.0.14.1.1-2.el8 | [RHBA-2022:0887](https://access.redhat.com/errata/RHBA-2022:0887) | <div class="adv_b">Bug Fix Advisory</div>
java-11-openjdk-static-libs-slowdebug | 11.0.14.1.1-2.el8 | [RHBA-2022:0887](https://access.redhat.com/errata/RHBA-2022:0887) | <div class="adv_b">Bug Fix Advisory</div>
libmount-devel | 2.32.1-35.el8 | |
libsss_nss_idmap-devel | 2.6.2-3.el8 | |
NetworkManager-libnm-devel | 1.37.2-1.el8 | |

