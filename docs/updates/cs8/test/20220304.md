## 2022-03-04

### CERN x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
cern-sssd-conf | 1.4-1.el8s.cern | |
cern-sssd-conf-domain-cernch | 1.4-1.el8s.cern | |
cern-sssd-conf-global | 1.4-1.el8s.cern | |
cern-sssd-conf-global-cernch | 1.4-1.el8s.cern | |
cern-sssd-conf-servers-cernch-gpn | 1.4-1.el8s.cern | |

### cloud x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
python-automaton-doc | 2.3.1-1.el8 | |
python-oslo-cache-doc | 2.7.1-1.el8 | |
python-oslo-cache-lang | 2.7.1-1.el8 | |
python-oslo-messaging-doc | 12.7.2-1.el8 | |
python-oslo-metrics-doc | 0.2.2-1.el8 | |
python-oslo-middleware-doc | 4.2.1-1.el8 | |
python-oslo-middleware-lang | 4.2.1-1.el8 | |
python-oslo-privsep-doc | 2.5.1-1.el8 | |
python-oslo-privsep-lang | 2.5.1-1.el8 | |
python-oslo-rootwrap-doc | 6.3.1-1.el8 | |
python-oslo-serialization-doc | 4.1.1-1.el8 | |
python-oslo-upgradecheck-doc | 1.3.1-1.el8 | |
python-oslo-versionedobjects-doc | 2.4.1-1.el8 | |
python-oslo-versionedobjects-lang | 2.4.1-1.el8 | |
python-oslo-vmware-doc | 3.8.1-1.el8 | |
python-oslo-vmware-lang | 3.8.1-1.el8 | |
python3-automaton | 2.3.1-1.el8 | |
python3-barbican-tests-tempest | 1.5.0-1.el8 | |
python3-cinder-tests-tempest | 1.5.0-1.el8 | |
python3-oslo-cache | 2.7.1-1.el8 | |
python3-oslo-cache-tests | 2.7.1-1.el8 | |
python3-oslo-messaging | 12.7.2-1.el8 | |
python3-oslo-messaging-tests | 12.7.2-1.el8 | |
python3-oslo-metrics | 0.2.2-1.el8 | |
python3-oslo-metrics-tests | 0.2.2-1.el8 | |
python3-oslo-middleware | 4.2.1-1.el8 | |
python3-oslo-middleware-tests | 4.2.1-1.el8 | |
python3-oslo-privsep | 2.5.1-1.el8 | |
python3-oslo-privsep-tests | 2.5.1-1.el8 | |
python3-oslo-rootwrap | 6.3.1-1.el8 | |
python3-oslo-rootwrap-tests | 6.3.1-1.el8 | |
python3-oslo-serialization | 4.1.1-1.el8 | |
python3-oslo-serialization-tests | 4.1.1-1.el8 | |
python3-oslo-upgradecheck | 1.3.1-1.el8 | |
python3-oslo-versionedobjects | 2.4.1-1.el8 | |
python3-oslo-versionedobjects-tests | 2.4.1-1.el8 | |
python3-oslo-vmware | 3.8.1-1.el8 | |
python3-oslo-vmware-tests | 3.8.1-1.el8 | |
python3-pymemcache | 3.5.0-1.el8 | |
python3-stevedore | 3.3.1-1.el8 | |

### CERN aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
cern-sssd-conf | 1.4-1.el8s.cern | |
cern-sssd-conf-domain-cernch | 1.4-1.el8s.cern | |
cern-sssd-conf-global | 1.4-1.el8s.cern | |
cern-sssd-conf-global-cernch | 1.4-1.el8s.cern | |
cern-sssd-conf-servers-cernch-gpn | 1.4-1.el8s.cern | |

### cloud aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
python-automaton-doc | 2.3.1-1.el8 | |
python-oslo-cache-doc | 2.7.1-1.el8 | |
python-oslo-cache-lang | 2.7.1-1.el8 | |
python-oslo-messaging-doc | 12.7.2-1.el8 | |
python-oslo-metrics-doc | 0.2.2-1.el8 | |
python-oslo-middleware-doc | 4.2.1-1.el8 | |
python-oslo-middleware-lang | 4.2.1-1.el8 | |
python-oslo-privsep-doc | 2.5.1-1.el8 | |
python-oslo-privsep-lang | 2.5.1-1.el8 | |
python-oslo-rootwrap-doc | 6.3.1-1.el8 | |
python-oslo-serialization-doc | 4.1.1-1.el8 | |
python-oslo-upgradecheck-doc | 1.3.1-1.el8 | |
python-oslo-versionedobjects-doc | 2.4.1-1.el8 | |
python-oslo-versionedobjects-lang | 2.4.1-1.el8 | |
python-oslo-vmware-doc | 3.8.1-1.el8 | |
python-oslo-vmware-lang | 3.8.1-1.el8 | |
python3-automaton | 2.3.1-1.el8 | |
python3-barbican-tests-tempest | 1.5.0-1.el8 | |
python3-cinder-tests-tempest | 1.5.0-1.el8 | |
python3-oslo-cache | 2.7.1-1.el8 | |
python3-oslo-cache-tests | 2.7.1-1.el8 | |
python3-oslo-messaging | 12.7.2-1.el8 | |
python3-oslo-messaging-tests | 12.7.2-1.el8 | |
python3-oslo-metrics | 0.2.2-1.el8 | |
python3-oslo-metrics-tests | 0.2.2-1.el8 | |
python3-oslo-middleware | 4.2.1-1.el8 | |
python3-oslo-middleware-tests | 4.2.1-1.el8 | |
python3-oslo-privsep | 2.5.1-1.el8 | |
python3-oslo-privsep-tests | 2.5.1-1.el8 | |
python3-oslo-rootwrap | 6.3.1-1.el8 | |
python3-oslo-rootwrap-tests | 6.3.1-1.el8 | |
python3-oslo-serialization | 4.1.1-1.el8 | |
python3-oslo-serialization-tests | 4.1.1-1.el8 | |
python3-oslo-upgradecheck | 1.3.1-1.el8 | |
python3-oslo-versionedobjects | 2.4.1-1.el8 | |
python3-oslo-versionedobjects-tests | 2.4.1-1.el8 | |
python3-oslo-vmware | 3.8.1-1.el8 | |
python3-oslo-vmware-tests | 3.8.1-1.el8 | |
python3-pymemcache | 3.5.0-1.el8 | |
python3-stevedore | 3.3.1-1.el8 | |

