## 2022-03-14

### AppStream x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
cargo | 1.58.1-1.module_el8.6.0+1082+15e7b372 | |
cargo-doc | 1.58.1-1.module_el8.6.0+1082+15e7b372 | |
clippy | 1.58.1-1.module_el8.6.0+1082+15e7b372 | |
edk2-ovmf | 20220126gitbb1bba3d77-2.el8 | |
fence-agents-ibm-vpc | 4.2.1-89.el8 | |
firefox | 91.7.0-3.el8 | [RHSA-2022:0818](https://access.redhat.com/errata/RHSA-2022:0818) | <div class="adv_s">Security Advisory</div>
python3-suds | 0.7-0.11.94664ddd46a6.el8 | |
rls | 1.58.1-1.module_el8.6.0+1082+15e7b372 | |
rust | 1.58.1-1.module_el8.6.0+1082+15e7b372 | |
rust-analysis | 1.58.1-1.module_el8.6.0+1082+15e7b372 | |
rust-debugger-common | 1.58.1-1.module_el8.6.0+1082+15e7b372 | |
rust-doc | 1.58.1-1.module_el8.6.0+1082+15e7b372 | |
rust-gdb | 1.58.1-1.module_el8.6.0+1082+15e7b372 | |
rust-lldb | 1.58.1-1.module_el8.6.0+1082+15e7b372 | |
rust-src | 1.58.1-1.module_el8.6.0+1082+15e7b372 | |
rust-std-static | 1.58.1-1.module_el8.6.0+1082+15e7b372 | |
rust-std-static-wasm32-unknown-unknown | 1.58.1-1.module_el8.6.0+1082+15e7b372 | |
rust-std-static-wasm32-wasi | 1.58.1-1.module_el8.6.0+1082+15e7b372 | |
rust-toolset | 1.58.1-1.module_el8.6.0+1082+15e7b372 | |
rustfmt | 1.58.1-1.module_el8.6.0+1082+15e7b372 | |

### extras x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
centos-release-ovirt45 | 8.6-1.el8 | |

### AppStream aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
cargo | 1.58.1-1.module_el8.6.0+1082+15e7b372 | |
cargo-doc | 1.58.1-1.module_el8.6.0+1082+15e7b372 | |
clippy | 1.58.1-1.module_el8.6.0+1082+15e7b372 | |
edk2-aarch64 | 20220126gitbb1bba3d77-2.el8 | |
fence-agents-ibm-vpc | 4.2.1-89.el8 | |
firefox | 91.7.0-3.el8 | [RHSA-2022:0818](https://access.redhat.com/errata/RHSA-2022:0818) | <div class="adv_s">Security Advisory</div>
python3-suds | 0.7-0.11.94664ddd46a6.el8 | |
rls | 1.58.1-1.module_el8.6.0+1082+15e7b372 | |
rust | 1.58.1-1.module_el8.6.0+1082+15e7b372 | |
rust-analysis | 1.58.1-1.module_el8.6.0+1082+15e7b372 | |
rust-debugger-common | 1.58.1-1.module_el8.6.0+1082+15e7b372 | |
rust-doc | 1.58.1-1.module_el8.6.0+1082+15e7b372 | |
rust-gdb | 1.58.1-1.module_el8.6.0+1082+15e7b372 | |
rust-lldb | 1.58.1-1.module_el8.6.0+1082+15e7b372 | |
rust-src | 1.58.1-1.module_el8.6.0+1082+15e7b372 | |
rust-std-static | 1.58.1-1.module_el8.6.0+1082+15e7b372 | |
rust-std-static-wasm32-unknown-unknown | 1.58.1-1.module_el8.6.0+1082+15e7b372 | |
rust-std-static-wasm32-wasi | 1.58.1-1.module_el8.6.0+1082+15e7b372 | |
rust-toolset | 1.58.1-1.module_el8.6.0+1082+15e7b372 | |
rustfmt | 1.58.1-1.module_el8.6.0+1082+15e7b372 | |

### extras aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
centos-release-ovirt45 | 8.6-1.el8 | |

