## 2022-11-24

### CERN x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
cern-sssd-conf | 1.5-1.el8s.cern | |
cern-sssd-conf-domain-cernch | 1.5-1.el8s.cern | |
cern-sssd-conf-global | 1.5-1.el8s.cern | |
cern-sssd-conf-global-cernch | 1.5-1.el8s.cern | |
cern-sssd-conf-servers-cernch-gpn | 1.5-1.el8s.cern | |
hepix | 4.10.3-0.el8s.cern | |

### CERN aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
cern-sssd-conf | 1.5-1.el8s.cern | |
cern-sssd-conf-domain-cernch | 1.5-1.el8s.cern | |
cern-sssd-conf-global | 1.5-1.el8s.cern | |
cern-sssd-conf-global-cernch | 1.5-1.el8s.cern | |
cern-sssd-conf-servers-cernch-gpn | 1.5-1.el8s.cern | |
hepix | 4.10.3-0.el8s.cern | |

