## 2023-01-16

### BaseOS x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
autofs | 5.1.4-91.el8 | |
cockpit | 282.1-1.el8 | |
cockpit-bridge | 282.1-1.el8 | |
cockpit-doc | 282.1-1.el8 | |
cockpit-system | 282.1-1.el8 | |
cockpit-ws | 282.1-1.el8 | |
cpp | 8.5.0-18.el8 | |
ctags | 5.8-23.el8 | |
gcc | 8.5.0-18.el8 | |
gcc-plugin-annobin | 8.5.0-18.el8 | |
glib2 | 2.56.4-161.el8 | |
glib2-devel | 2.56.4-161.el8 | |
glib2-fam | 2.56.4-161.el8 | |
glib2-tests | 2.56.4-161.el8 | |
hwdata | 0.314-8.15.el8 | |
libasan | 8.5.0-18.el8 | |
libatomic | 8.5.0-18.el8 | |
libatomic-static | 8.5.0-18.el8 | |
libgcc | 8.5.0-18.el8 | |
libgfortran | 8.5.0-18.el8 | |
libgomp | 8.5.0-18.el8 | |
libgomp-offload-nvptx | 8.5.0-18.el8 | |
libipa_hbac | 2.8.2-1.el8 | |
libitm | 8.5.0-18.el8 | |
liblsan | 8.5.0-18.el8 | |
libquadmath | 8.5.0-18.el8 | |
libsss_autofs | 2.8.2-1.el8 | |
libsss_certmap | 2.8.2-1.el8 | |
libsss_idmap | 2.8.2-1.el8 | |
libsss_nss_idmap | 2.8.2-1.el8 | |
libsss_simpleifp | 2.8.2-1.el8 | |
libsss_sudo | 2.8.2-1.el8 | |
libstdc++ | 8.5.0-18.el8 | |
libtsan | 8.5.0-18.el8 | |
libubsan | 8.5.0-18.el8 | |
libxml2 | 2.9.7-16.el8 | |
NetworkManager | 1.40.8-2.el8 | |
NetworkManager-adsl | 1.40.8-2.el8 | |
NetworkManager-bluetooth | 1.40.8-2.el8 | |
NetworkManager-config-connectivity-redhat | 1.40.8-2.el8 | |
NetworkManager-config-server | 1.40.8-2.el8 | |
NetworkManager-dispatcher-routing-rules | 1.40.8-2.el8 | |
NetworkManager-initscripts-updown | 1.40.8-2.el8 | |
NetworkManager-libnm | 1.40.8-2.el8 | |
NetworkManager-ovs | 1.40.8-2.el8 | |
NetworkManager-ppp | 1.40.8-2.el8 | |
NetworkManager-team | 1.40.8-2.el8 | |
NetworkManager-tui | 1.40.8-2.el8 | |
NetworkManager-wifi | 1.40.8-2.el8 | |
NetworkManager-wwan | 1.40.8-2.el8 | |
platform-python | 3.6.8-50.el8 | |
python3-libipa_hbac | 2.8.2-1.el8 | |
python3-libs | 3.6.8-50.el8 | |
python3-libsss_nss_idmap | 2.8.2-1.el8 | |
python3-libxml2 | 2.9.7-16.el8 | |
python3-rpm | 4.14.3-26.el8 | |
python3-sss | 2.8.2-1.el8 | |
python3-sss-murmur | 2.8.2-1.el8 | |
python3-sssdconfig | 2.8.2-1.el8 | |
python3-test | 3.6.8-50.el8 | |
rpm | 4.14.3-26.el8 | |
rpm-apidocs | 4.14.3-26.el8 | |
rpm-build-libs | 4.14.3-26.el8 | |
rpm-cron | 4.14.3-26.el8 | |
rpm-devel | 4.14.3-26.el8 | |
rpm-libs | 4.14.3-26.el8 | |
rpm-plugin-ima | 4.14.3-26.el8 | |
rpm-plugin-prioreset | 4.14.3-26.el8 | |
rpm-plugin-selinux | 4.14.3-26.el8 | |
rpm-plugin-syslog | 4.14.3-26.el8 | |
rpm-plugin-systemd-inhibit | 4.14.3-26.el8 | |
rpm-sign | 4.14.3-26.el8 | |
sssd | 2.8.2-1.el8 | |
sssd-ad | 2.8.2-1.el8 | |
sssd-client | 2.8.2-1.el8 | |
sssd-common | 2.8.2-1.el8 | |
sssd-common-pac | 2.8.2-1.el8 | |
sssd-dbus | 2.8.2-1.el8 | |
sssd-ipa | 2.8.2-1.el8 | |
sssd-kcm | 2.8.2-1.el8 | |
sssd-krb5 | 2.8.2-1.el8 | |
sssd-krb5-common | 2.8.2-1.el8 | |
sssd-ldap | 2.8.2-1.el8 | |
sssd-nfs-idmap | 2.8.2-1.el8 | |
sssd-polkit-rules | 2.8.2-1.el8 | |
sssd-proxy | 2.8.2-1.el8 | |
sssd-tools | 2.8.2-1.el8 | |
sssd-winbind-idmap | 2.8.2-1.el8 | |

### AppStream x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
alsa-lib | 1.2.8-2.el8 | |
alsa-lib-devel | 1.2.8-2.el8 | |
alsa-ucm | 1.2.8-2.el8 | |
anaconda | 33.16.8.5-1.el8 | |
anaconda-core | 33.16.8.5-1.el8 | |
anaconda-dracut | 33.16.8.5-1.el8 | |
anaconda-gui | 33.16.8.5-1.el8 | |
anaconda-install-env-deps | 33.16.8.5-1.el8 | |
anaconda-tui | 33.16.8.5-1.el8 | |
anaconda-widgets | 33.16.8.5-1.el8 | |
cloud-init | 22.1-7.el8 | |
cockpit-machines | 282.1-1.el8 | |
cockpit-packagekit | 282.1-1.el8 | |
cockpit-pcp | 282.1-1.el8 | |
cockpit-storaged | 282.1-1.el8 | |
evolution | 3.28.5-22.el8 | |
evolution-bogofilter | 3.28.5-22.el8 | |
evolution-help | 3.28.5-22.el8 | |
evolution-langpacks | 3.28.5-22.el8 | |
evolution-pst | 3.28.5-22.el8 | |
evolution-spamassassin | 3.28.5-22.el8 | |
gcc-c++ | 8.5.0-18.el8 | |
gcc-gdb-plugin | 8.5.0-18.el8 | |
gcc-gfortran | 8.5.0-18.el8 | |
gcc-offload-nvptx | 8.5.0-18.el8 | |
git | 2.39.0-1.el8 | |
git-all | 2.39.0-1.el8 | |
git-core | 2.39.0-1.el8 | |
git-core-doc | 2.39.0-1.el8 | |
git-credential-libsecret | 2.39.0-1.el8 | |
git-daemon | 2.39.0-1.el8 | |
git-email | 2.39.0-1.el8 | |
git-gui | 2.39.0-1.el8 | |
git-instaweb | 2.39.0-1.el8 | |
git-subtree | 2.39.0-1.el8 | |
git-svn | 2.39.0-1.el8 | |
gitk | 2.39.0-1.el8 | |
gitweb | 2.39.0-1.el8 | |
gnome-classic-session | 3.32.1-31.el8 | |
gnome-shell-extension-apps-menu | 3.32.1-31.el8 | |
gnome-shell-extension-auto-move-windows | 3.32.1-31.el8 | |
gnome-shell-extension-classification-banner | 3.32.1-31.el8 | |
gnome-shell-extension-common | 3.32.1-31.el8 | |
gnome-shell-extension-dash-to-dock | 3.32.1-31.el8 | |
gnome-shell-extension-dash-to-panel | 3.32.1-31.el8 | |
gnome-shell-extension-desktop-icons | 3.32.1-31.el8 | |
gnome-shell-extension-disable-screenshield | 3.32.1-31.el8 | |
gnome-shell-extension-drive-menu | 3.32.1-31.el8 | |
gnome-shell-extension-gesture-inhibitor | 3.32.1-31.el8 | |
gnome-shell-extension-heads-up-display | 3.32.1-31.el8 | |
gnome-shell-extension-horizontal-workspaces | 3.32.1-31.el8 | |
gnome-shell-extension-launch-new-instance | 3.32.1-31.el8 | |
gnome-shell-extension-native-window-placement | 3.32.1-31.el8 | |
gnome-shell-extension-no-hot-corner | 3.32.1-31.el8 | |
gnome-shell-extension-panel-favorites | 3.32.1-31.el8 | |
gnome-shell-extension-places-menu | 3.32.1-31.el8 | |
gnome-shell-extension-screenshot-window-sizer | 3.32.1-31.el8 | |
gnome-shell-extension-systemMonitor | 3.32.1-31.el8 | |
gnome-shell-extension-top-icons | 3.32.1-31.el8 | |
gnome-shell-extension-updates-dialog | 3.32.1-31.el8 | |
gnome-shell-extension-user-theme | 3.32.1-31.el8 | |
gnome-shell-extension-window-grouper | 3.32.1-31.el8 | |
gnome-shell-extension-window-list | 3.32.1-31.el8 | |
gnome-shell-extension-windowsNavigator | 3.32.1-31.el8 | |
gnome-shell-extension-workspace-indicator | 3.32.1-31.el8 | |
libitm-devel | 8.5.0-18.el8 | |
libquadmath-devel | 8.5.0-18.el8 | |
libstdc++-devel | 8.5.0-18.el8 | |
libstdc++-docs | 8.5.0-18.el8 | |
libxml2-devel | 2.9.7-16.el8 | |
mutter | 3.32.2-68.el8 | |
NetworkManager-cloud-setup | 1.40.8-2.el8 | |
osbuild | 75-1.el8 | |
osbuild-composer | 71-1.el8 | |
osbuild-composer-core | 71-1.el8 | |
osbuild-composer-dnf-json | 71-1.el8 | |
osbuild-composer-worker | 71-1.el8 | |
osbuild-luks2 | 75-1.el8 | |
osbuild-lvm2 | 75-1.el8 | |
osbuild-ostree | 75-1.el8 | |
osbuild-selinux | 75-1.el8 | |
perl-Git | 2.39.0-1.el8 | |
perl-Git-SVN | 2.39.0-1.el8 | |
platform-python-debug | 3.6.8-50.el8 | |
platform-python-devel | 3.6.8-50.el8 | |
python3-idle | 3.6.8-50.el8 | |
python3-osbuild | 75-1.el8 | |
python3-tkinter | 3.6.8-50.el8 | |
rpm-build | 4.14.3-26.el8 | |
rpm-plugin-fapolicyd | 4.14.3-26.el8 | |
sssd-idp | 2.8.2-1.el8 | |
sysstat | 11.7.3-9.el8 | |

### PowerTools x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
anaconda-widgets-devel | 33.16.8.5-1.el8 | |
ctags-etags | 5.8-23.el8 | |
evolution-devel | 3.28.5-22.el8 | |
gcc-plugin-devel | 8.5.0-18.el8 | |
glib2-doc | 2.56.4-161.el8 | |
glib2-static | 2.56.4-161.el8 | |
libserf-devel | 1.3.9-9.module_el8.3.0+703+ba2f61b7 | |
libsss_nss_idmap-devel | 2.8.2-1.el8 | |
libstdc++-static | 8.5.0-18.el8 | |
mutter-devel | 3.32.2-68.el8 | |
NetworkManager-libnm-devel | 1.40.8-2.el8 | |
subversion-ruby | 1.10.2-5.module_el8.7.0+1146+633d65ff | |
utf8proc-devel | 2.6.1-3.module_el8.7.0+1186+7834489d | |

### HighAvailability x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
pcs | 0.10.15-2.el8 | |
pcs-snmp | 0.10.15-2.el8 | |
resource-agents | 4.9.0-37.el8 | |
resource-agents-aliyun | 4.9.0-37.el8 | |
resource-agents-gcp | 4.9.0-37.el8 | |
resource-agents-paf | 4.9.0-37.el8 | |

### ResilientStorage x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
pcs | 0.10.15-2.el8 | |
pcs-snmp | 0.10.15-2.el8 | |
resource-agents | 4.9.0-37.el8 | |
resource-agents-aliyun | 4.9.0-37.el8 | |
resource-agents-gcp | 4.9.0-37.el8 | |
resource-agents-paf | 4.9.0-37.el8 | |

### BaseOS aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
autofs | 5.1.4-91.el8 | |
cockpit | 282.1-1.el8 | |
cockpit-bridge | 282.1-1.el8 | |
cockpit-doc | 282.1-1.el8 | |
cockpit-system | 282.1-1.el8 | |
cockpit-ws | 282.1-1.el8 | |
glib2 | 2.56.4-161.el8 | |
glib2-devel | 2.56.4-161.el8 | |
glib2-fam | 2.56.4-161.el8 | |
glib2-tests | 2.56.4-161.el8 | |
hwdata | 0.314-8.15.el8 | |
libasan | 8.5.0-18.el8 | |
libatomic | 8.5.0-18.el8 | |
libatomic-static | 8.5.0-18.el8 | |
libgcc | 8.5.0-18.el8 | |
libgfortran | 8.5.0-18.el8 | |
libgomp | 8.5.0-18.el8 | |
libipa_hbac | 2.8.2-1.el8 | |
libitm | 8.5.0-18.el8 | |
liblsan | 8.5.0-18.el8 | |
libsss_autofs | 2.8.2-1.el8 | |
libsss_certmap | 2.8.2-1.el8 | |
libsss_idmap | 2.8.2-1.el8 | |
libsss_nss_idmap | 2.8.2-1.el8 | |
libsss_simpleifp | 2.8.2-1.el8 | |
libsss_sudo | 2.8.2-1.el8 | |
libstdc++ | 8.5.0-18.el8 | |
libtsan | 8.5.0-18.el8 | |
libubsan | 8.5.0-18.el8 | |
libxml2 | 2.9.7-16.el8 | |
NetworkManager | 1.40.8-2.el8 | |
NetworkManager-adsl | 1.40.8-2.el8 | |
NetworkManager-bluetooth | 1.40.8-2.el8 | |
NetworkManager-config-connectivity-redhat | 1.40.8-2.el8 | |
NetworkManager-config-server | 1.40.8-2.el8 | |
NetworkManager-dispatcher-routing-rules | 1.40.8-2.el8 | |
NetworkManager-libnm | 1.40.8-2.el8 | |
NetworkManager-ovs | 1.40.8-2.el8 | |
NetworkManager-ppp | 1.40.8-2.el8 | |
NetworkManager-team | 1.40.8-2.el8 | |
NetworkManager-tui | 1.40.8-2.el8 | |
NetworkManager-wifi | 1.40.8-2.el8 | |
NetworkManager-wwan | 1.40.8-2.el8 | |
platform-python | 3.6.8-50.el8 | |
python3-libipa_hbac | 2.8.2-1.el8 | |
python3-libs | 3.6.8-50.el8 | |
python3-libsss_nss_idmap | 2.8.2-1.el8 | |
python3-libxml2 | 2.9.7-16.el8 | |
python3-rpm | 4.14.3-26.el8 | |
python3-sss | 2.8.2-1.el8 | |
python3-sss-murmur | 2.8.2-1.el8 | |
python3-sssdconfig | 2.8.2-1.el8 | |
python3-test | 3.6.8-50.el8 | |
rpm | 4.14.3-26.el8 | |
rpm-apidocs | 4.14.3-26.el8 | |
rpm-build-libs | 4.14.3-26.el8 | |
rpm-cron | 4.14.3-26.el8 | |
rpm-devel | 4.14.3-26.el8 | |
rpm-libs | 4.14.3-26.el8 | |
rpm-plugin-ima | 4.14.3-26.el8 | |
rpm-plugin-prioreset | 4.14.3-26.el8 | |
rpm-plugin-selinux | 4.14.3-26.el8 | |
rpm-plugin-syslog | 4.14.3-26.el8 | |
rpm-plugin-systemd-inhibit | 4.14.3-26.el8 | |
rpm-sign | 4.14.3-26.el8 | |
sssd | 2.8.2-1.el8 | |
sssd-ad | 2.8.2-1.el8 | |
sssd-client | 2.8.2-1.el8 | |
sssd-common | 2.8.2-1.el8 | |
sssd-common-pac | 2.8.2-1.el8 | |
sssd-dbus | 2.8.2-1.el8 | |
sssd-ipa | 2.8.2-1.el8 | |
sssd-kcm | 2.8.2-1.el8 | |
sssd-krb5 | 2.8.2-1.el8 | |
sssd-krb5-common | 2.8.2-1.el8 | |
sssd-ldap | 2.8.2-1.el8 | |
sssd-nfs-idmap | 2.8.2-1.el8 | |
sssd-polkit-rules | 2.8.2-1.el8 | |
sssd-proxy | 2.8.2-1.el8 | |
sssd-tools | 2.8.2-1.el8 | |
sssd-winbind-idmap | 2.8.2-1.el8 | |

### AppStream aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
alsa-lib | 1.2.8-2.el8 | |
alsa-lib-devel | 1.2.8-2.el8 | |
alsa-ucm | 1.2.8-2.el8 | |
anaconda | 33.16.8.5-1.el8 | |
anaconda-core | 33.16.8.5-1.el8 | |
anaconda-dracut | 33.16.8.5-1.el8 | |
anaconda-gui | 33.16.8.5-1.el8 | |
anaconda-install-env-deps | 33.16.8.5-1.el8 | |
anaconda-tui | 33.16.8.5-1.el8 | |
anaconda-widgets | 33.16.8.5-1.el8 | |
cloud-init | 22.1-7.el8 | |
cockpit-machines | 282.1-1.el8 | |
cockpit-packagekit | 282.1-1.el8 | |
cockpit-pcp | 282.1-1.el8 | |
cockpit-storaged | 282.1-1.el8 | |
cpp | 8.5.0-18.el8 | |
ctags | 5.8-23.el8 | |
evolution | 3.28.5-22.el8 | |
evolution-bogofilter | 3.28.5-22.el8 | |
evolution-help | 3.28.5-22.el8 | |
evolution-langpacks | 3.28.5-22.el8 | |
evolution-pst | 3.28.5-22.el8 | |
evolution-spamassassin | 3.28.5-22.el8 | |
gcc | 8.5.0-18.el8 | |
gcc-c++ | 8.5.0-18.el8 | |
gcc-gdb-plugin | 8.5.0-18.el8 | |
gcc-gfortran | 8.5.0-18.el8 | |
gcc-plugin-annobin | 8.5.0-18.el8 | |
git | 2.39.0-1.el8 | |
git-all | 2.39.0-1.el8 | |
git-core | 2.39.0-1.el8 | |
git-core-doc | 2.39.0-1.el8 | |
git-credential-libsecret | 2.39.0-1.el8 | |
git-daemon | 2.39.0-1.el8 | |
git-email | 2.39.0-1.el8 | |
git-gui | 2.39.0-1.el8 | |
git-instaweb | 2.39.0-1.el8 | |
git-subtree | 2.39.0-1.el8 | |
git-svn | 2.39.0-1.el8 | |
gitk | 2.39.0-1.el8 | |
gitweb | 2.39.0-1.el8 | |
gnome-classic-session | 3.32.1-31.el8 | |
gnome-shell-extension-apps-menu | 3.32.1-31.el8 | |
gnome-shell-extension-auto-move-windows | 3.32.1-31.el8 | |
gnome-shell-extension-classification-banner | 3.32.1-31.el8 | |
gnome-shell-extension-common | 3.32.1-31.el8 | |
gnome-shell-extension-dash-to-dock | 3.32.1-31.el8 | |
gnome-shell-extension-dash-to-panel | 3.32.1-31.el8 | |
gnome-shell-extension-desktop-icons | 3.32.1-31.el8 | |
gnome-shell-extension-disable-screenshield | 3.32.1-31.el8 | |
gnome-shell-extension-drive-menu | 3.32.1-31.el8 | |
gnome-shell-extension-gesture-inhibitor | 3.32.1-31.el8 | |
gnome-shell-extension-heads-up-display | 3.32.1-31.el8 | |
gnome-shell-extension-horizontal-workspaces | 3.32.1-31.el8 | |
gnome-shell-extension-launch-new-instance | 3.32.1-31.el8 | |
gnome-shell-extension-native-window-placement | 3.32.1-31.el8 | |
gnome-shell-extension-no-hot-corner | 3.32.1-31.el8 | |
gnome-shell-extension-panel-favorites | 3.32.1-31.el8 | |
gnome-shell-extension-places-menu | 3.32.1-31.el8 | |
gnome-shell-extension-screenshot-window-sizer | 3.32.1-31.el8 | |
gnome-shell-extension-systemMonitor | 3.32.1-31.el8 | |
gnome-shell-extension-top-icons | 3.32.1-31.el8 | |
gnome-shell-extension-updates-dialog | 3.32.1-31.el8 | |
gnome-shell-extension-user-theme | 3.32.1-31.el8 | |
gnome-shell-extension-window-grouper | 3.32.1-31.el8 | |
gnome-shell-extension-window-list | 3.32.1-31.el8 | |
gnome-shell-extension-windowsNavigator | 3.32.1-31.el8 | |
gnome-shell-extension-workspace-indicator | 3.32.1-31.el8 | |
libitm-devel | 8.5.0-18.el8 | |
libstdc++-devel | 8.5.0-18.el8 | |
libstdc++-docs | 8.5.0-18.el8 | |
libxml2-devel | 2.9.7-16.el8 | |
mutter | 3.32.2-68.el8 | |
NetworkManager-cloud-setup | 1.40.8-2.el8 | |
osbuild | 75-1.el8 | |
osbuild-composer | 71-1.el8 | |
osbuild-composer-core | 71-1.el8 | |
osbuild-composer-dnf-json | 71-1.el8 | |
osbuild-composer-worker | 71-1.el8 | |
osbuild-luks2 | 75-1.el8 | |
osbuild-lvm2 | 75-1.el8 | |
osbuild-ostree | 75-1.el8 | |
osbuild-selinux | 75-1.el8 | |
perl-Git | 2.39.0-1.el8 | |
perl-Git-SVN | 2.39.0-1.el8 | |
platform-python-debug | 3.6.8-50.el8 | |
platform-python-devel | 3.6.8-50.el8 | |
python3-idle | 3.6.8-50.el8 | |
python3-osbuild | 75-1.el8 | |
python3-tkinter | 3.6.8-50.el8 | |
rpm-build | 4.14.3-26.el8 | |
rpm-plugin-fapolicyd | 4.14.3-26.el8 | |
sssd-idp | 2.8.2-1.el8 | |
sysstat | 11.7.3-9.el8 | |

### PowerTools aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
anaconda-widgets-devel | 33.16.8.5-1.el8 | |
ctags-etags | 5.8-23.el8 | |
evolution-devel | 3.28.5-22.el8 | |
gcc-plugin-devel | 8.5.0-18.el8 | |
glib2-doc | 2.56.4-161.el8 | |
glib2-static | 2.56.4-161.el8 | |
libserf-devel | 1.3.9-9.module_el8.3.0+703+ba2f61b7 | |
libsss_nss_idmap-devel | 2.8.2-1.el8 | |
libstdc++-static | 8.5.0-18.el8 | |
mutter-devel | 3.32.2-68.el8 | |
NetworkManager-libnm-devel | 1.40.8-2.el8 | |
subversion-ruby | 1.10.2-5.module_el8.7.0+1146+633d65ff | |
utf8proc-devel | 2.6.1-3.module_el8.7.0+1186+7834489d | |

### HighAvailability aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
pcs | 0.10.15-2.el8 | |
pcs-snmp | 0.10.15-2.el8 | |
resource-agents | 4.9.0-37.el8 | |
resource-agents-paf | 4.9.0-37.el8 | |

