## 2021-06-30

### CERN x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
perl-Convert-BinHex | 1.125-13.el8s.cern | |
perl-SOAP-Lite | 1.27-7.el8s.cern | |

### virt x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
ansible | 2.9.23-2.el8 | |
ansible-doc | 2.9.23-2.el8 | |
ansible-test | 2.9.23-2.el8 | |

### CERN aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
perl-Convert-BinHex | 1.125-13.el8s.cern | |
perl-SOAP-Lite | 1.27-7.el8s.cern | |

### virt aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
ansible | 2.9.23-2.el8 | |
ansible-doc | 2.9.23-2.el8 | |
ansible-test | 2.9.23-2.el8 | |

