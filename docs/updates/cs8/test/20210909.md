## 2021-09-09

### CERN x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
pyphonebook | 2.1.1-1.el8s.cern | |

### CERN aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
pyphonebook | 2.1.1-1.el8s.cern | |

