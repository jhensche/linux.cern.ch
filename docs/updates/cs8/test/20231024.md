## 2023-10-24

### CERN x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
hepix | 4.10.8-0.el8s.cern | |

### cloud x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
openstack-keystone | 21.0.1-1.el8 | |
openstack-keystone-doc | 21.0.1-1.el8 | |
python-octavia-tests-tempest-doc | 2.4.0-1.el8 | |
python3-keystone | 21.0.1-1.el8 | |
python3-keystone-tests | 21.0.1-1.el8 | |
python3-octavia-tests-tempest | 2.4.0-1.el8 | |
python3-octavia-tests-tempest-golang | 2.4.0-1.el8 | |

### CERN aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
hepix | 4.10.8-0.el8s.cern | |

### cloud aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
openstack-keystone | 21.0.1-1.el8 | |
openstack-keystone-doc | 21.0.1-1.el8 | |
python-octavia-tests-tempest-doc | 2.4.0-1.el8 | |
python3-keystone | 21.0.1-1.el8 | |
python3-keystone-tests | 21.0.1-1.el8 | |
python3-octavia-tests-tempest | 2.4.0-1.el8 | |
python3-octavia-tests-tempest-golang | 2.4.0-1.el8 | |

