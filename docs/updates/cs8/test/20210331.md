## 2021-03-31

### cloud x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
ansible | 2.9.19-1.el8 | |
ansible-test | 2.9.19-1.el8 | |
openstack-nova | 22.2.0-1.el8 | |
openstack-nova-api | 22.2.0-1.el8 | |
openstack-nova-common | 22.2.0-1.el8 | |
openstack-nova-compute | 22.2.0-1.el8 | |
openstack-nova-conductor | 22.2.0-1.el8 | |
openstack-nova-migration | 22.2.0-1.el8 | |
openstack-nova-novncproxy | 22.2.0-1.el8 | |
openstack-nova-scheduler | 22.2.0-1.el8 | |
openstack-nova-serialproxy | 22.2.0-1.el8 | |
openstack-nova-spicehtml5proxy | 22.2.0-1.el8 | |
openstack-packstack | 17.0.0-0.3.0rc2.el8 | |
openstack-packstack-doc | 17.0.0-0.3.0rc2.el8 | |
openstack-packstack-puppet | 17.0.0-0.3.0rc2.el8 | |
openstack-tempest | 26.1.0-2.el8 | |
openstack-tempest-all | 26.1.0-2.el8 | |
openstack-tempest-doc | 26.1.0-2.el8 | |
python3-barbican-tests-tempest | 1.2.1-1.el8 | |
python3-nova | 22.2.0-1.el8 | |
python3-nova-tests | 22.2.0-1.el8 | |
python3-tempest | 26.1.0-2.el8 | |
python3-tempest-tests | 26.1.0-2.el8 | |

### cloud aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
ansible | 2.9.19-1.el8 | |
ansible-test | 2.9.19-1.el8 | |
openstack-nova | 22.2.0-1.el8 | |
openstack-nova-api | 22.2.0-1.el8 | |
openstack-nova-common | 22.2.0-1.el8 | |
openstack-nova-compute | 22.2.0-1.el8 | |
openstack-nova-conductor | 22.2.0-1.el8 | |
openstack-nova-migration | 22.2.0-1.el8 | |
openstack-nova-novncproxy | 22.2.0-1.el8 | |
openstack-nova-scheduler | 22.2.0-1.el8 | |
openstack-nova-serialproxy | 22.2.0-1.el8 | |
openstack-nova-spicehtml5proxy | 22.2.0-1.el8 | |
openstack-packstack | 17.0.0-0.3.0rc2.el8 | |
openstack-packstack-doc | 17.0.0-0.3.0rc2.el8 | |
openstack-packstack-puppet | 17.0.0-0.3.0rc2.el8 | |
openstack-tempest | 26.1.0-2.el8 | |
openstack-tempest-all | 26.1.0-2.el8 | |
openstack-tempest-doc | 26.1.0-2.el8 | |
python3-barbican-tests-tempest | 1.2.1-1.el8 | |
python3-nova | 22.2.0-1.el8 | |
python3-nova-tests | 22.2.0-1.el8 | |
python3-tempest | 26.1.0-2.el8 | |
python3-tempest-tests | 26.1.0-2.el8 | |

