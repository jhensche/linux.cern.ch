## 2023-12-01

### virt x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
ovirt-engine-api-model | 4.6.0-1.el8 | |

### virt aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
ovirt-engine-api-model | 4.6.0-1.el8 | |

