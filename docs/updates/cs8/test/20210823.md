## 2021-08-23

### openafs x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
afs_tools | 2.3-0.el8s.cern | |
afs_tools_standalone | 2.3-0.el8s.cern | |
arc | 47-1.0.el8s | |
arc | 48-1.1.el8s | |
cern-aklog-systemd-user | 1.0-2.el8s.cern | |
cern-aklog-systemd-user | 1.1-1.el8s.cern | |
dkms-openafs | 1.8.7-1.el8s.cern | |
dkms-openafs | 1.8.8-1.el8s.cern | |
kmod-openafs | 1.8.7-1.4.18.0_277.el8.el8s.cern | |
kmod-openafs | 1.8.7-1.4.18.0_294.el8.el8s.cern | |
kmod-openafs | 1.8.7-1.4.18.0_301.1.el8.el8s.cern | |
kmod-openafs | 1.8.7-1.4.18.0_305.10.2.el8_4.el8s.cern | |
kmod-openafs | 1.8.7-1.4.18.0_305.3.1.el8.el8s.cern | |
kmod-openafs | 1.8.7-1.4.18.0_305.el8.el8s.cern | |
kmod-openafs | 1.8.8-1.4.18.0_326.el8.el8s.cern | |
kmod-openafs | 1.8.8-1.4.18.0_331.el8.el8s.cern | |
openafs | 1.8.7-1.el8s.cern | |
openafs | 1.8.8-1.el8s.cern | |
openafs-authlibs | 1.8.7-1.el8s.cern | |
openafs-authlibs | 1.8.8-1.el8s.cern | |
openafs-authlibs-devel | 1.8.7-1.el8s.cern | |
openafs-authlibs-devel | 1.8.8-1.el8s.cern | |
openafs-client | 1.8.7-1.el8s.cern | |
openafs-client | 1.8.8-1.el8s.cern | |
openafs-compat | 1.8.7-1.el8s.cern | |
openafs-compat | 1.8.8-1.el8s.cern | |
openafs-debugsource | 1.8.7-1.el8s.cern | |
openafs-debugsource | 1.8.7_4.18.0_277.el8-1.el8s.cern | |
openafs-debugsource | 1.8.7_4.18.0_294.el8-1.el8s.cern | |
openafs-debugsource | 1.8.7_4.18.0_301.1.el8-1.el8s.cern | |
openafs-debugsource | 1.8.7_4.18.0_305.10.2.el8_4-1.el8s.cern | |
openafs-debugsource | 1.8.7_4.18.0_305.3.1.el8-1.el8s.cern | |
openafs-debugsource | 1.8.7_4.18.0_305.el8-1.el8s.cern | |
openafs-debugsource | 1.8.8-1.el8s.cern | |
openafs-debugsource | 1.8.8_4.18.0_326.el8-1.el8s.cern | |
openafs-debugsource | 1.8.8_4.18.0_331.el8-1.el8s.cern | |
openafs-devel | 1.8.7-1.el8s.cern | |
openafs-devel | 1.8.8-1.el8s.cern | |
openafs-docs | 1.8.7-1.el8s.cern | |
openafs-docs | 1.8.8-1.el8s.cern | |
openafs-kernel-source | 1.8.7-1.el8s.cern | |
openafs-kernel-source | 1.8.8-1.el8s.cern | |
openafs-krb5 | 1.8.7-1.el8s.cern | |
openafs-krb5 | 1.8.8-1.el8s.cern | |
openafs-server | 1.8.7-1.el8s.cern | |
openafs-server | 1.8.8-1.el8s.cern | |
pam_afs_session | 2.6-3.el8s.cern | |
pam_afs_session-debugsource | 2.6-3.el8s.cern | |

### BaseOS x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
bpftool | 4.18.0-305.12.1.el8_4 | [RHSA-2021:3057](https://access.redhat.com/errata/RHSA-2021:3057) | <div class="adv_s">Security Advisory</div>
bpftool | 4.18.0-331.el8 | |
curl | 7.61.1-18.el8_4.1 | |
device-mapper | 1.02.177-6.el8 | |
device-mapper-event | 1.02.177-6.el8 | |
device-mapper-event-libs | 1.02.177-6.el8 | |
device-mapper-libs | 1.02.177-6.el8 | |
kernel | 4.18.0-305.12.1.el8_4 | [RHSA-2021:3057](https://access.redhat.com/errata/RHSA-2021:3057) | <div class="adv_s">Security Advisory</div>
kernel | 4.18.0-331.el8 | |
kernel-abi-stablelists | 4.18.0-305.12.1.el8_4 | [RHSA-2021:3057](https://access.redhat.com/errata/RHSA-2021:3057) | <div class="adv_s">Security Advisory</div>
kernel-abi-stablelists | 4.18.0-331.el8 | |
kernel-core | 4.18.0-305.12.1.el8_4 | [RHSA-2021:3057](https://access.redhat.com/errata/RHSA-2021:3057) | <div class="adv_s">Security Advisory</div>
kernel-core | 4.18.0-331.el8 | |
kernel-cross-headers | 4.18.0-305.12.1.el8_4 | [RHSA-2021:3057](https://access.redhat.com/errata/RHSA-2021:3057) | <div class="adv_s">Security Advisory</div>
kernel-cross-headers | 4.18.0-331.el8 | |
kernel-debug | 4.18.0-305.12.1.el8_4 | [RHSA-2021:3057](https://access.redhat.com/errata/RHSA-2021:3057) | <div class="adv_s">Security Advisory</div>
kernel-debug | 4.18.0-331.el8 | |
kernel-debug-core | 4.18.0-305.12.1.el8_4 | [RHSA-2021:3057](https://access.redhat.com/errata/RHSA-2021:3057) | <div class="adv_s">Security Advisory</div>
kernel-debug-core | 4.18.0-331.el8 | |
kernel-debug-devel | 4.18.0-305.12.1.el8_4 | [RHSA-2021:3057](https://access.redhat.com/errata/RHSA-2021:3057) | <div class="adv_s">Security Advisory</div>
kernel-debug-devel | 4.18.0-331.el8 | |
kernel-debug-modules | 4.18.0-305.12.1.el8_4 | [RHSA-2021:3057](https://access.redhat.com/errata/RHSA-2021:3057) | <div class="adv_s">Security Advisory</div>
kernel-debug-modules | 4.18.0-331.el8 | |
kernel-debug-modules-extra | 4.18.0-305.12.1.el8_4 | [RHSA-2021:3057](https://access.redhat.com/errata/RHSA-2021:3057) | <div class="adv_s">Security Advisory</div>
kernel-debug-modules-extra | 4.18.0-331.el8 | |
kernel-devel | 4.18.0-305.12.1.el8_4 | [RHSA-2021:3057](https://access.redhat.com/errata/RHSA-2021:3057) | <div class="adv_s">Security Advisory</div>
kernel-devel | 4.18.0-331.el8 | |
kernel-doc | 4.18.0-305.12.1.el8_4 | [RHSA-2021:3057](https://access.redhat.com/errata/RHSA-2021:3057) | <div class="adv_s">Security Advisory</div>
kernel-doc | 4.18.0-331.el8 | |
kernel-headers | 4.18.0-305.12.1.el8_4 | [RHSA-2021:3057](https://access.redhat.com/errata/RHSA-2021:3057) | <div class="adv_s">Security Advisory</div>
kernel-headers | 4.18.0-331.el8 | |
kernel-modules | 4.18.0-305.12.1.el8_4 | [RHSA-2021:3057](https://access.redhat.com/errata/RHSA-2021:3057) | <div class="adv_s">Security Advisory</div>
kernel-modules | 4.18.0-331.el8 | |
kernel-modules-extra | 4.18.0-305.12.1.el8_4 | [RHSA-2021:3057](https://access.redhat.com/errata/RHSA-2021:3057) | <div class="adv_s">Security Advisory</div>
kernel-modules-extra | 4.18.0-331.el8 | |
kernel-rt-core | 4.18.0-331.rt7.112.el8 | |
kernel-tools | 4.18.0-305.12.1.el8_4 | [RHSA-2021:3057](https://access.redhat.com/errata/RHSA-2021:3057) | <div class="adv_s">Security Advisory</div>
kernel-tools | 4.18.0-331.el8 | |
kernel-tools-libs | 4.18.0-305.12.1.el8_4 | [RHSA-2021:3057](https://access.redhat.com/errata/RHSA-2021:3057) | <div class="adv_s">Security Advisory</div>
kernel-tools-libs | 4.18.0-331.el8 | |
kmod-kvdo | 6.2.5.72-79.el8 | |
libcurl | 7.61.1-18.el8_4.1 | |
libcurl-devel | 7.61.1-18.el8_4.1 | |
libcurl-minimal | 7.61.1-18.el8_4.1 | |
lvm2 | 2.03.12-6.el8 | |
lvm2-dbusd | 2.03.12-6.el8 | |
lvm2-libs | 2.03.12-6.el8 | |
lvm2-lockd | 2.03.12-6.el8 | |
ModemManager | 1.10.8-4.el8 | |
ModemManager-glib | 1.10.8-4.el8 | |
NetworkManager | 1.32.8-1.el8 | |
NetworkManager-adsl | 1.32.8-1.el8 | |
NetworkManager-bluetooth | 1.32.8-1.el8 | |
NetworkManager-config-connectivity-redhat | 1.32.8-1.el8 | |
NetworkManager-config-server | 1.32.8-1.el8 | |
NetworkManager-dispatcher-routing-rules | 1.32.8-1.el8 | |
NetworkManager-libnm | 1.32.8-1.el8 | |
NetworkManager-ovs | 1.32.8-1.el8 | |
NetworkManager-ppp | 1.32.8-1.el8 | |
NetworkManager-team | 1.32.8-1.el8 | |
NetworkManager-tui | 1.32.8-1.el8 | |
NetworkManager-wifi | 1.32.8-1.el8 | |
NetworkManager-wwan | 1.32.8-1.el8 | |
perf | 4.18.0-305.12.1.el8_4 | [RHSA-2021:3057](https://access.redhat.com/errata/RHSA-2021:3057) | <div class="adv_s">Security Advisory</div>
perf | 4.18.0-331.el8 | |
python3-perf | 4.18.0-305.12.1.el8_4 | [RHSA-2021:3057](https://access.redhat.com/errata/RHSA-2021:3057) | <div class="adv_s">Security Advisory</div>
python3-perf | 4.18.0-331.el8 | |
selinux-policy | 3.14.3-76.el8 | |
selinux-policy-devel | 3.14.3-76.el8 | |
selinux-policy-doc | 3.14.3-76.el8 | |
selinux-policy-minimum | 3.14.3-76.el8 | |
selinux-policy-mls | 3.14.3-76.el8 | |
selinux-policy-sandbox | 3.14.3-76.el8 | |
selinux-policy-targeted | 3.14.3-76.el8 | |
shadow-utils | 4.6-14.el8 | |
sos | 4.1-5.el8 | |
sos-audit | 4.1-5.el8 | |

### AppStream x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
389-ds-base | 1.4.3.23-7.module_el8.5.0+889+90e0384f | |
389-ds-base-devel | 1.4.3.23-7.module_el8.5.0+889+90e0384f | |
389-ds-base-legacy-tools | 1.4.3.23-7.module_el8.5.0+889+90e0384f | |
389-ds-base-libs | 1.4.3.23-7.module_el8.5.0+889+90e0384f | |
389-ds-base-snmp | 1.4.3.23-7.module_el8.5.0+889+90e0384f | |
aspnetcore-runtime-3.1 | 3.1.18-1.el8_4 | [RHSA-2021:3142](https://access.redhat.com/errata/RHSA-2021:3142) | <div class="adv_s">Security Advisory</div>
aspnetcore-runtime-5.0 | 5.0.9-1.el8_4 | [RHSA-2021:3148](https://access.redhat.com/errata/RHSA-2021:3148) | <div class="adv_s">Security Advisory</div>
aspnetcore-targeting-pack-3.1 | 3.1.18-1.el8_4 | [RHSA-2021:3142](https://access.redhat.com/errata/RHSA-2021:3142) | <div class="adv_s">Security Advisory</div>
aspnetcore-targeting-pack-5.0 | 5.0.9-1.el8_4 | [RHSA-2021:3148](https://access.redhat.com/errata/RHSA-2021:3148) | <div class="adv_s">Security Advisory</div>
buildah | 1.22.0-2.module_el8.5.0+890+6b136101 | |
buildah-tests | 1.22.0-2.module_el8.5.0+890+6b136101 | |
cockpit-podman | 33-1.module_el8.5.0+890+6b136101 | |
conmon | 2.0.29-1.module_el8.5.0+890+6b136101 | |
container-selinux | 2.164.2-1.module_el8.5.0+890+6b136101 | |
containernetworking-plugins | 1.0.0-1.module_el8.5.0+890+6b136101 | |
containers-common | 1-2.module_el8.5.0+890+6b136101 | |
crit | 3.15-3.module_el8.5.0+890+6b136101 | |
criu | 3.15-3.module_el8.5.0+890+6b136101 | |
criu-devel | 3.15-3.module_el8.5.0+890+6b136101 | |
criu-libs | 3.15-3.module_el8.5.0+890+6b136101 | |
crun | 0.21-3.module_el8.5.0+890+6b136101 | |
dotnet | 5.0.206-1.el8_4 | [RHSA-2021:3148](https://access.redhat.com/errata/RHSA-2021:3148) | <div class="adv_s">Security Advisory</div>
dotnet-apphost-pack-3.1 | 3.1.18-1.el8_4 | [RHSA-2021:3142](https://access.redhat.com/errata/RHSA-2021:3142) | <div class="adv_s">Security Advisory</div>
dotnet-apphost-pack-5.0 | 5.0.9-1.el8_4 | [RHSA-2021:3148](https://access.redhat.com/errata/RHSA-2021:3148) | <div class="adv_s">Security Advisory</div>
dotnet-host | 5.0.9-1.el8_4 | [RHSA-2021:3148](https://access.redhat.com/errata/RHSA-2021:3148) | <div class="adv_s">Security Advisory</div>
dotnet-host-fxr-2.1 | 2.1.29-1.el8_4 | [RHSA-2021:3145](https://access.redhat.com/errata/RHSA-2021:3145) | <div class="adv_s">Security Advisory</div>
dotnet-hostfxr-3.1 | 3.1.18-1.el8_4 | [RHSA-2021:3142](https://access.redhat.com/errata/RHSA-2021:3142) | <div class="adv_s">Security Advisory</div>
dotnet-hostfxr-5.0 | 5.0.9-1.el8_4 | [RHSA-2021:3148](https://access.redhat.com/errata/RHSA-2021:3148) | <div class="adv_s">Security Advisory</div>
dotnet-runtime-2.1 | 2.1.29-1.el8_4 | [RHSA-2021:3145](https://access.redhat.com/errata/RHSA-2021:3145) | <div class="adv_s">Security Advisory</div>
dotnet-runtime-3.1 | 3.1.18-1.el8_4 | [RHSA-2021:3142](https://access.redhat.com/errata/RHSA-2021:3142) | <div class="adv_s">Security Advisory</div>
dotnet-runtime-5.0 | 5.0.9-1.el8_4 | [RHSA-2021:3148](https://access.redhat.com/errata/RHSA-2021:3148) | <div class="adv_s">Security Advisory</div>
dotnet-sdk-2.1 | 2.1.525-1.el8_4 | [RHSA-2021:3145](https://access.redhat.com/errata/RHSA-2021:3145) | <div class="adv_s">Security Advisory</div>
dotnet-sdk-2.1.5xx | 2.1.525-1.el8_4 | [RHSA-2021:3145](https://access.redhat.com/errata/RHSA-2021:3145) | <div class="adv_s">Security Advisory</div>
dotnet-sdk-3.1 | 3.1.118-1.el8_4 | [RHSA-2021:3142](https://access.redhat.com/errata/RHSA-2021:3142) | <div class="adv_s">Security Advisory</div>
dotnet-sdk-5.0 | 5.0.206-1.el8_4 | [RHSA-2021:3148](https://access.redhat.com/errata/RHSA-2021:3148) | <div class="adv_s">Security Advisory</div>
dotnet-targeting-pack-3.1 | 3.1.18-1.el8_4 | [RHSA-2021:3142](https://access.redhat.com/errata/RHSA-2021:3142) | <div class="adv_s">Security Advisory</div>
dotnet-targeting-pack-5.0 | 5.0.9-1.el8_4 | [RHSA-2021:3148](https://access.redhat.com/errata/RHSA-2021:3148) | <div class="adv_s">Security Advisory</div>
dotnet-templates-3.1 | 3.1.118-1.el8_4 | [RHSA-2021:3142](https://access.redhat.com/errata/RHSA-2021:3142) | <div class="adv_s">Security Advisory</div>
dotnet-templates-5.0 | 5.0.206-1.el8_4 | [RHSA-2021:3148](https://access.redhat.com/errata/RHSA-2021:3148) | <div class="adv_s">Security Advisory</div>
fence-agents-all | 4.2.1-75.el8 | |
fence-agents-amt-ws | 4.2.1-75.el8 | |
fence-agents-apc | 4.2.1-75.el8 | |
fence-agents-apc-snmp | 4.2.1-75.el8 | |
fence-agents-bladecenter | 4.2.1-75.el8 | |
fence-agents-brocade | 4.2.1-75.el8 | |
fence-agents-cisco-mds | 4.2.1-75.el8 | |
fence-agents-cisco-ucs | 4.2.1-75.el8 | |
fence-agents-common | 4.2.1-75.el8 | |
fence-agents-compute | 4.2.1-75.el8 | |
fence-agents-drac5 | 4.2.1-75.el8 | |
fence-agents-eaton-snmp | 4.2.1-75.el8 | |
fence-agents-emerson | 4.2.1-75.el8 | |
fence-agents-eps | 4.2.1-75.el8 | |
fence-agents-heuristics-ping | 4.2.1-75.el8 | |
fence-agents-hpblade | 4.2.1-75.el8 | |
fence-agents-ibmblade | 4.2.1-75.el8 | |
fence-agents-ifmib | 4.2.1-75.el8 | |
fence-agents-ilo-moonshot | 4.2.1-75.el8 | |
fence-agents-ilo-mp | 4.2.1-75.el8 | |
fence-agents-ilo-ssh | 4.2.1-75.el8 | |
fence-agents-ilo2 | 4.2.1-75.el8 | |
fence-agents-intelmodular | 4.2.1-75.el8 | |
fence-agents-ipdu | 4.2.1-75.el8 | |
fence-agents-ipmilan | 4.2.1-75.el8 | |
fence-agents-kdump | 4.2.1-75.el8 | |
fence-agents-lpar | 4.2.1-75.el8 | |
fence-agents-mpath | 4.2.1-75.el8 | |
fence-agents-redfish | 4.2.1-75.el8 | |
fence-agents-rhevm | 4.2.1-75.el8 | |
fence-agents-rsa | 4.2.1-75.el8 | |
fence-agents-rsb | 4.2.1-75.el8 | |
fence-agents-sbd | 4.2.1-75.el8 | |
fence-agents-scsi | 4.2.1-75.el8 | |
fence-agents-virsh | 4.2.1-75.el8 | |
fence-agents-vmware-rest | 4.2.1-75.el8 | |
fence-agents-vmware-soap | 4.2.1-75.el8 | |
fence-agents-wti | 4.2.1-75.el8 | |
fuse-overlayfs | 1.7.1-1.module_el8.5.0+890+6b136101 | |
gcc-toolset-11 | 11.0-1.el8 | |
gcc-toolset-11-build | 11.0-1.el8 | |
gcc-toolset-11-gcc-plugin-devel | 11.1.1-6.el8 | |
gcc-toolset-11-perftools | 11.0-1.el8 | |
gcc-toolset-11-runtime | 11.0-1.el8 | |
gcc-toolset-11-toolchain | 11.0-1.el8 | |
gnome-settings-daemon | 3.32.0-15.el8 | |
libslirp | 4.4.0-1.module_el8.5.0+890+6b136101 | |
libslirp-devel | 4.4.0-1.module_el8.5.0+890+6b136101 | |
netstandard-targeting-pack-2.1 | 5.0.206-1.el8_4 | [RHSA-2021:3148](https://access.redhat.com/errata/RHSA-2021:3148) | <div class="adv_s">Security Advisory</div>
NetworkManager-cloud-setup | 1.32.8-1.el8 | |
oci-seccomp-bpf-hook | 1.2.3-3.module_el8.5.0+890+6b136101 | |
oscap-anaconda-addon | 1.2.1-2.el8 | |
pacemaker-cluster-libs | 2.1.0-6.el8 | |
pacemaker-libs | 2.1.0-6.el8 | |
pacemaker-schemas | 2.1.0-6.el8 | |
podman | 3.3.0-4.module_el8.5.0+890+6b136101 | |
podman-catatonit | 3.3.0-4.module_el8.5.0+890+6b136101 | |
podman-docker | 3.3.0-4.module_el8.5.0+890+6b136101 | |
podman-plugins | 3.3.0-4.module_el8.5.0+890+6b136101 | |
podman-remote | 3.3.0-4.module_el8.5.0+890+6b136101 | |
podman-tests | 3.3.0-4.module_el8.5.0+890+6b136101 | |
python3-criu | 3.15-3.module_el8.5.0+890+6b136101 | |
python3-lib389 | 1.4.3.23-7.module_el8.5.0+889+90e0384f | |
python3-podman | 3.2.0-2.module_el8.5.0+890+6b136101 | |
python3-psutil | 5.4.3-11.el8 | |
runc | 1.0.1-5.module_el8.5.0+890+6b136101 | |
saab-fonts | 0.91-18.el8 | |
skopeo | 1.4.0-6.module_el8.5.0+890+6b136101 | |
skopeo-tests | 1.4.0-6.module_el8.5.0+890+6b136101 | |
slirp4netns | 1.1.8-1.module_el8.5.0+890+6b136101 | |
systemtap | 4.5-3.el8 | |
systemtap-client | 4.5-3.el8 | |
systemtap-devel | 4.5-3.el8 | |
systemtap-exporter | 4.5-3.el8 | |
systemtap-initscript | 4.5-3.el8 | |
systemtap-runtime | 4.5-3.el8 | |
systemtap-runtime-java | 4.5-3.el8 | |
systemtap-runtime-python3 | 4.5-3.el8 | |
systemtap-runtime-virtguest | 4.5-3.el8 | |
systemtap-runtime-virthost | 4.5-3.el8 | |
systemtap-sdt-devel | 4.5-3.el8 | |
systemtap-server | 4.5-3.el8 | |
toolbox | 0.0.99.3-0.1.module_el8.5.0+890+6b136101 | |
toolbox-tests | 0.0.99.3-0.1.module_el8.5.0+890+6b136101 | |
udica | 0.2.4-2.module_el8.5.0+890+6b136101 | |
unicode-ucd | 11.0.0-2.el8 | |
vulkan-validation-layers | 1.2.182.0-2.el8_4 | |

### PowerTools x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
device-mapper-devel | 1.02.177-6.el8 | |
device-mapper-event-devel | 1.02.177-6.el8 | |
kernel-tools-libs-devel | 4.18.0-305.12.1.el8_4 | [RHSA-2021:3057](https://access.redhat.com/errata/RHSA-2021:3057) | <div class="adv_s">Security Advisory</div>
kernel-tools-libs-devel | 4.18.0-331.el8 | |
lvm2-devel | 2.03.12-6.el8 | |
ModemManager-devel | 1.10.8-4.el8 | |
ModemManager-glib-devel | 1.10.8-4.el8 | |
NetworkManager-libnm-devel | 1.32.8-1.el8 | |
unicode-ucd-unihan | 11.0.0-2.el8 | |

### HighAvailability x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
fence-agents-aliyun | 4.2.1-75.el8 | |
fence-agents-aws | 4.2.1-75.el8 | |
fence-agents-azure-arm | 4.2.1-75.el8 | |
fence-agents-gce | 4.2.1-75.el8 | |
pacemaker | 2.1.0-6.el8 | |
pacemaker-cli | 2.1.0-6.el8 | |
pacemaker-cts | 2.1.0-6.el8 | |
pacemaker-doc | 2.1.0-6.el8 | |
pacemaker-libs-devel | 2.1.0-6.el8 | |
pacemaker-nagios-plugins-metadata | 2.1.0-6.el8 | |
pacemaker-remote | 2.1.0-6.el8 | |

### RT x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
kernel-rt | 4.18.0-331.rt7.112.el8 | |
kernel-rt-core | 4.18.0-331.rt7.112.el8 | |
kernel-rt-debug | 4.18.0-331.rt7.112.el8 | |
kernel-rt-debug-core | 4.18.0-331.rt7.112.el8 | |
kernel-rt-debug-devel | 4.18.0-331.rt7.112.el8 | |
kernel-rt-debug-modules | 4.18.0-331.rt7.112.el8 | |
kernel-rt-debug-modules-extra | 4.18.0-331.rt7.112.el8 | |
kernel-rt-devel | 4.18.0-331.rt7.112.el8 | |
kernel-rt-modules | 4.18.0-331.rt7.112.el8 | |
kernel-rt-modules-extra | 4.18.0-331.rt7.112.el8 | |

### cloud x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
openstack-tripleo-common | 12.4.6-2.el8 | |
openstack-tripleo-common-container-base | 12.4.6-2.el8 | |
openstack-tripleo-common-containers | 12.4.6-2.el8 | |
openstack-tripleo-common-devtools | 12.4.6-2.el8 | |
python3-tripleo-common | 12.4.6-2.el8 | |

### openafs aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
afs_tools | 2.3-0.el8s.cern | |
afs_tools_standalone | 2.3-0.el8s.cern | |
cern-aklog-systemd-user | 1.0-2.el8s.cern | |
cern-aklog-systemd-user | 1.1-1.el8s.cern | |
dkms-openafs | 1.8.7-1.el8s.cern | |
dkms-openafs | 1.8.8-1.el8s.cern | |
kmod-openafs | 1.8.7-1.4.18.0_277.el8.el8s.cern | |
kmod-openafs | 1.8.7-1.4.18.0_294.el8.el8s.cern | |
kmod-openafs | 1.8.7-1.4.18.0_301.1.el8.el8s.cern | |
kmod-openafs | 1.8.7-1.4.18.0_305.10.2.el8_4.el8s.cern | |
kmod-openafs | 1.8.7-1.4.18.0_305.3.1.el8.el8s.cern | |
kmod-openafs | 1.8.7-1.4.18.0_305.el8.el8s.cern | |
kmod-openafs | 1.8.8-1.4.18.0_326.el8.el8s.cern | |
kmod-openafs | 1.8.8-1.4.18.0_331.el8.el8s.cern | |
openafs | 1.8.7-1.el8s.cern | |
openafs | 1.8.8-1.el8s.cern | |
openafs-authlibs | 1.8.7-1.el8s.cern | |
openafs-authlibs | 1.8.8-1.el8s.cern | |
openafs-authlibs-devel | 1.8.7-1.el8s.cern | |
openafs-authlibs-devel | 1.8.8-1.el8s.cern | |
openafs-client | 1.8.7-1.el8s.cern | |
openafs-client | 1.8.8-1.el8s.cern | |
openafs-compat | 1.8.7-1.el8s.cern | |
openafs-compat | 1.8.8-1.el8s.cern | |
openafs-debugsource | 1.8.7-1.el8s.cern | |
openafs-debugsource | 1.8.7_4.18.0_277.el8-1.el8s.cern | |
openafs-debugsource | 1.8.7_4.18.0_294.el8-1.el8s.cern | |
openafs-debugsource | 1.8.7_4.18.0_301.1.el8-1.el8s.cern | |
openafs-debugsource | 1.8.7_4.18.0_305.10.2.el8_4-1.el8s.cern | |
openafs-debugsource | 1.8.7_4.18.0_305.3.1.el8-1.el8s.cern | |
openafs-debugsource | 1.8.7_4.18.0_305.el8-1.el8s.cern | |
openafs-debugsource | 1.8.8-1.el8s.cern | |
openafs-debugsource | 1.8.8_4.18.0_326.el8-1.el8s.cern | |
openafs-debugsource | 1.8.8_4.18.0_331.el8-1.el8s.cern | |
openafs-devel | 1.8.7-1.el8s.cern | |
openafs-devel | 1.8.8-1.el8s.cern | |
openafs-docs | 1.8.7-1.el8s.cern | |
openafs-docs | 1.8.8-1.el8s.cern | |
openafs-kernel-source | 1.8.7-1.el8s.cern | |
openafs-kernel-source | 1.8.8-1.el8s.cern | |
openafs-krb5 | 1.8.7-1.el8s.cern | |
openafs-krb5 | 1.8.8-1.el8s.cern | |
openafs-server | 1.8.7-1.el8s.cern | |
openafs-server | 1.8.8-1.el8s.cern | |
pam_afs_session | 2.6-3.el8s.cern | |
pam_afs_session-debugsource | 2.6-3.el8s.cern | |

### BaseOS aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
bpftool | 4.18.0-305.12.1.el8_4 | [RHSA-2021:3057](https://access.redhat.com/errata/RHSA-2021:3057) | <div class="adv_s">Security Advisory</div>
bpftool | 4.18.0-331.el8 | |
curl | 7.61.1-18.el8_4.1 | |
device-mapper | 1.02.177-6.el8 | |
device-mapper-event | 1.02.177-6.el8 | |
device-mapper-event-libs | 1.02.177-6.el8 | |
device-mapper-libs | 1.02.177-6.el8 | |
kernel | 4.18.0-305.12.1.el8_4 | [RHSA-2021:3057](https://access.redhat.com/errata/RHSA-2021:3057) | <div class="adv_s">Security Advisory</div>
kernel | 4.18.0-331.el8 | |
kernel-abi-stablelists | 4.18.0-305.12.1.el8_4 | [RHSA-2021:3057](https://access.redhat.com/errata/RHSA-2021:3057) | <div class="adv_s">Security Advisory</div>
kernel-abi-stablelists | 4.18.0-331.el8 | |
kernel-core | 4.18.0-305.12.1.el8_4 | [RHSA-2021:3057](https://access.redhat.com/errata/RHSA-2021:3057) | <div class="adv_s">Security Advisory</div>
kernel-core | 4.18.0-331.el8 | |
kernel-cross-headers | 4.18.0-305.12.1.el8_4 | [RHSA-2021:3057](https://access.redhat.com/errata/RHSA-2021:3057) | <div class="adv_s">Security Advisory</div>
kernel-cross-headers | 4.18.0-331.el8 | |
kernel-debug | 4.18.0-305.12.1.el8_4 | [RHSA-2021:3057](https://access.redhat.com/errata/RHSA-2021:3057) | <div class="adv_s">Security Advisory</div>
kernel-debug | 4.18.0-331.el8 | |
kernel-debug-core | 4.18.0-305.12.1.el8_4 | [RHSA-2021:3057](https://access.redhat.com/errata/RHSA-2021:3057) | <div class="adv_s">Security Advisory</div>
kernel-debug-core | 4.18.0-331.el8 | |
kernel-debug-devel | 4.18.0-305.12.1.el8_4 | [RHSA-2021:3057](https://access.redhat.com/errata/RHSA-2021:3057) | <div class="adv_s">Security Advisory</div>
kernel-debug-devel | 4.18.0-331.el8 | |
kernel-debug-modules | 4.18.0-305.12.1.el8_4 | [RHSA-2021:3057](https://access.redhat.com/errata/RHSA-2021:3057) | <div class="adv_s">Security Advisory</div>
kernel-debug-modules | 4.18.0-331.el8 | |
kernel-debug-modules-extra | 4.18.0-305.12.1.el8_4 | [RHSA-2021:3057](https://access.redhat.com/errata/RHSA-2021:3057) | <div class="adv_s">Security Advisory</div>
kernel-debug-modules-extra | 4.18.0-331.el8 | |
kernel-devel | 4.18.0-305.12.1.el8_4 | [RHSA-2021:3057](https://access.redhat.com/errata/RHSA-2021:3057) | <div class="adv_s">Security Advisory</div>
kernel-devel | 4.18.0-331.el8 | |
kernel-doc | 4.18.0-305.12.1.el8_4 | [RHSA-2021:3057](https://access.redhat.com/errata/RHSA-2021:3057) | <div class="adv_s">Security Advisory</div>
kernel-doc | 4.18.0-331.el8 | |
kernel-headers | 4.18.0-305.12.1.el8_4 | [RHSA-2021:3057](https://access.redhat.com/errata/RHSA-2021:3057) | <div class="adv_s">Security Advisory</div>
kernel-headers | 4.18.0-331.el8 | |
kernel-modules | 4.18.0-305.12.1.el8_4 | [RHSA-2021:3057](https://access.redhat.com/errata/RHSA-2021:3057) | <div class="adv_s">Security Advisory</div>
kernel-modules | 4.18.0-331.el8 | |
kernel-modules-extra | 4.18.0-305.12.1.el8_4 | [RHSA-2021:3057](https://access.redhat.com/errata/RHSA-2021:3057) | <div class="adv_s">Security Advisory</div>
kernel-modules-extra | 4.18.0-331.el8 | |
kernel-tools | 4.18.0-305.12.1.el8_4 | [RHSA-2021:3057](https://access.redhat.com/errata/RHSA-2021:3057) | <div class="adv_s">Security Advisory</div>
kernel-tools | 4.18.0-331.el8 | |
kernel-tools-libs | 4.18.0-305.12.1.el8_4 | [RHSA-2021:3057](https://access.redhat.com/errata/RHSA-2021:3057) | <div class="adv_s">Security Advisory</div>
kernel-tools-libs | 4.18.0-331.el8 | |
kmod-kvdo | 6.2.5.72-79.el8 | |
libcurl | 7.61.1-18.el8_4.1 | |
libcurl-devel | 7.61.1-18.el8_4.1 | |
libcurl-minimal | 7.61.1-18.el8_4.1 | |
lvm2 | 2.03.12-6.el8 | |
lvm2-dbusd | 2.03.12-6.el8 | |
lvm2-libs | 2.03.12-6.el8 | |
lvm2-lockd | 2.03.12-6.el8 | |
ModemManager | 1.10.8-4.el8 | |
ModemManager-glib | 1.10.8-4.el8 | |
NetworkManager | 1.32.8-1.el8 | |
NetworkManager-adsl | 1.32.8-1.el8 | |
NetworkManager-bluetooth | 1.32.8-1.el8 | |
NetworkManager-config-connectivity-redhat | 1.32.8-1.el8 | |
NetworkManager-config-server | 1.32.8-1.el8 | |
NetworkManager-dispatcher-routing-rules | 1.32.8-1.el8 | |
NetworkManager-libnm | 1.32.8-1.el8 | |
NetworkManager-ovs | 1.32.8-1.el8 | |
NetworkManager-ppp | 1.32.8-1.el8 | |
NetworkManager-team | 1.32.8-1.el8 | |
NetworkManager-tui | 1.32.8-1.el8 | |
NetworkManager-wifi | 1.32.8-1.el8 | |
NetworkManager-wwan | 1.32.8-1.el8 | |
perf | 4.18.0-305.12.1.el8_4 | [RHSA-2021:3057](https://access.redhat.com/errata/RHSA-2021:3057) | <div class="adv_s">Security Advisory</div>
perf | 4.18.0-331.el8 | |
python3-perf | 4.18.0-305.12.1.el8_4 | [RHSA-2021:3057](https://access.redhat.com/errata/RHSA-2021:3057) | <div class="adv_s">Security Advisory</div>
python3-perf | 4.18.0-331.el8 | |
selinux-policy | 3.14.3-76.el8 | |
selinux-policy-devel | 3.14.3-76.el8 | |
selinux-policy-doc | 3.14.3-76.el8 | |
selinux-policy-minimum | 3.14.3-76.el8 | |
selinux-policy-mls | 3.14.3-76.el8 | |
selinux-policy-sandbox | 3.14.3-76.el8 | |
selinux-policy-targeted | 3.14.3-76.el8 | |
shadow-utils | 4.6-14.el8 | |
sos | 4.1-5.el8 | |
sos-audit | 4.1-5.el8 | |

### AppStream aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
389-ds-base | 1.4.3.23-7.module_el8.5.0+889+90e0384f | |
389-ds-base-devel | 1.4.3.23-7.module_el8.5.0+889+90e0384f | |
389-ds-base-legacy-tools | 1.4.3.23-7.module_el8.5.0+889+90e0384f | |
389-ds-base-libs | 1.4.3.23-7.module_el8.5.0+889+90e0384f | |
389-ds-base-snmp | 1.4.3.23-7.module_el8.5.0+889+90e0384f | |
buildah | 1.22.0-2.module_el8.5.0+890+6b136101 | |
buildah-tests | 1.22.0-2.module_el8.5.0+890+6b136101 | |
cockpit-podman | 33-1.module_el8.5.0+890+6b136101 | |
conmon | 2.0.29-1.module_el8.5.0+890+6b136101 | |
container-selinux | 2.164.2-1.module_el8.5.0+890+6b136101 | |
containernetworking-plugins | 1.0.0-1.module_el8.5.0+890+6b136101 | |
containers-common | 1-2.module_el8.5.0+890+6b136101 | |
crit | 3.15-3.module_el8.5.0+890+6b136101 | |
criu | 3.15-3.module_el8.5.0+890+6b136101 | |
criu-devel | 3.15-3.module_el8.5.0+890+6b136101 | |
criu-libs | 3.15-3.module_el8.5.0+890+6b136101 | |
crun | 0.21-3.module_el8.5.0+890+6b136101 | |
fence-agents-all | 4.2.1-75.el8 | |
fence-agents-amt-ws | 4.2.1-75.el8 | |
fence-agents-apc | 4.2.1-75.el8 | |
fence-agents-apc-snmp | 4.2.1-75.el8 | |
fence-agents-bladecenter | 4.2.1-75.el8 | |
fence-agents-brocade | 4.2.1-75.el8 | |
fence-agents-cisco-mds | 4.2.1-75.el8 | |
fence-agents-cisco-ucs | 4.2.1-75.el8 | |
fence-agents-common | 4.2.1-75.el8 | |
fence-agents-compute | 4.2.1-75.el8 | |
fence-agents-drac5 | 4.2.1-75.el8 | |
fence-agents-eaton-snmp | 4.2.1-75.el8 | |
fence-agents-emerson | 4.2.1-75.el8 | |
fence-agents-eps | 4.2.1-75.el8 | |
fence-agents-heuristics-ping | 4.2.1-75.el8 | |
fence-agents-hpblade | 4.2.1-75.el8 | |
fence-agents-ibmblade | 4.2.1-75.el8 | |
fence-agents-ifmib | 4.2.1-75.el8 | |
fence-agents-ilo-moonshot | 4.2.1-75.el8 | |
fence-agents-ilo-mp | 4.2.1-75.el8 | |
fence-agents-ilo-ssh | 4.2.1-75.el8 | |
fence-agents-ilo2 | 4.2.1-75.el8 | |
fence-agents-intelmodular | 4.2.1-75.el8 | |
fence-agents-ipdu | 4.2.1-75.el8 | |
fence-agents-ipmilan | 4.2.1-75.el8 | |
fence-agents-kdump | 4.2.1-75.el8 | |
fence-agents-mpath | 4.2.1-75.el8 | |
fence-agents-redfish | 4.2.1-75.el8 | |
fence-agents-rhevm | 4.2.1-75.el8 | |
fence-agents-rsa | 4.2.1-75.el8 | |
fence-agents-rsb | 4.2.1-75.el8 | |
fence-agents-sbd | 4.2.1-75.el8 | |
fence-agents-scsi | 4.2.1-75.el8 | |
fence-agents-virsh | 4.2.1-75.el8 | |
fence-agents-vmware-rest | 4.2.1-75.el8 | |
fence-agents-vmware-soap | 4.2.1-75.el8 | |
fence-agents-wti | 4.2.1-75.el8 | |
fuse-overlayfs | 1.7.1-1.module_el8.5.0+890+6b136101 | |
gcc-toolset-11 | 11.0-1.el8 | |
gcc-toolset-11-build | 11.0-1.el8 | |
gcc-toolset-11-gcc-plugin-devel | 11.1.1-6.el8 | |
gcc-toolset-11-perftools | 11.0-1.el8 | |
gcc-toolset-11-runtime | 11.0-1.el8 | |
gcc-toolset-11-toolchain | 11.0-1.el8 | |
gnome-settings-daemon | 3.32.0-15.el8 | |
libslirp | 4.4.0-1.module_el8.5.0+890+6b136101 | |
libslirp-devel | 4.4.0-1.module_el8.5.0+890+6b136101 | |
NetworkManager-cloud-setup | 1.32.8-1.el8 | |
oci-seccomp-bpf-hook | 1.2.3-3.module_el8.5.0+890+6b136101 | |
oscap-anaconda-addon | 1.2.1-2.el8 | |
pacemaker-cluster-libs | 2.1.0-6.el8 | |
pacemaker-libs | 2.1.0-6.el8 | |
pacemaker-schemas | 2.1.0-6.el8 | |
podman | 3.3.0-4.module_el8.5.0+890+6b136101 | |
podman-catatonit | 3.3.0-4.module_el8.5.0+890+6b136101 | |
podman-docker | 3.3.0-4.module_el8.5.0+890+6b136101 | |
podman-plugins | 3.3.0-4.module_el8.5.0+890+6b136101 | |
podman-remote | 3.3.0-4.module_el8.5.0+890+6b136101 | |
podman-tests | 3.3.0-4.module_el8.5.0+890+6b136101 | |
python3-criu | 3.15-3.module_el8.5.0+890+6b136101 | |
python3-lib389 | 1.4.3.23-7.module_el8.5.0+889+90e0384f | |
python3-podman | 3.2.0-2.module_el8.5.0+890+6b136101 | |
python3-psutil | 5.4.3-11.el8 | |
runc | 1.0.1-5.module_el8.5.0+890+6b136101 | |
saab-fonts | 0.91-18.el8 | |
skopeo | 1.4.0-6.module_el8.5.0+890+6b136101 | |
skopeo-tests | 1.4.0-6.module_el8.5.0+890+6b136101 | |
slirp4netns | 1.1.8-1.module_el8.5.0+890+6b136101 | |
systemtap | 4.5-3.el8 | |
systemtap-client | 4.5-3.el8 | |
systemtap-devel | 4.5-3.el8 | |
systemtap-exporter | 4.5-3.el8 | |
systemtap-initscript | 4.5-3.el8 | |
systemtap-runtime | 4.5-3.el8 | |
systemtap-runtime-java | 4.5-3.el8 | |
systemtap-runtime-python3 | 4.5-3.el8 | |
systemtap-runtime-virtguest | 4.5-3.el8 | |
systemtap-sdt-devel | 4.5-3.el8 | |
systemtap-server | 4.5-3.el8 | |
toolbox | 0.0.99.3-0.1.module_el8.5.0+890+6b136101 | |
toolbox-tests | 0.0.99.3-0.1.module_el8.5.0+890+6b136101 | |
udica | 0.2.4-2.module_el8.5.0+890+6b136101 | |
unicode-ucd | 11.0.0-2.el8 | |
vulkan-validation-layers | 1.2.182.0-2.el8_4 | |

### PowerTools aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
device-mapper-devel | 1.02.177-6.el8 | |
device-mapper-event-devel | 1.02.177-6.el8 | |
kernel-tools-libs-devel | 4.18.0-305.12.1.el8_4 | [RHSA-2021:3057](https://access.redhat.com/errata/RHSA-2021:3057) | <div class="adv_s">Security Advisory</div>
kernel-tools-libs-devel | 4.18.0-331.el8 | |
lvm2-devel | 2.03.12-6.el8 | |
ModemManager-devel | 1.10.8-4.el8 | |
ModemManager-glib-devel | 1.10.8-4.el8 | |
NetworkManager-libnm-devel | 1.32.8-1.el8 | |
unicode-ucd-unihan | 11.0.0-2.el8 | |

### HighAvailability aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
fence-agents-azure-arm | 4.2.1-75.el8 | |
fence-agents-gce | 4.2.1-75.el8 | |
pacemaker | 2.1.0-6.el8 | |
pacemaker-cli | 2.1.0-6.el8 | |
pacemaker-cts | 2.1.0-6.el8 | |
pacemaker-doc | 2.1.0-6.el8 | |
pacemaker-libs-devel | 2.1.0-6.el8 | |
pacemaker-nagios-plugins-metadata | 2.1.0-6.el8 | |
pacemaker-remote | 2.1.0-6.el8 | |

### cloud aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
openstack-tripleo-common | 12.4.6-2.el8 | |
openstack-tripleo-common-container-base | 12.4.6-2.el8 | |
openstack-tripleo-common-containers | 12.4.6-2.el8 | |
openstack-tripleo-common-devtools | 12.4.6-2.el8 | |
python3-tripleo-common | 12.4.6-2.el8 | |

