## 2023-11-07

### openafs x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
kmod-openafs | 1.8.10-0.4.18.0_521.el8.el8s.cern | |

### BaseOS x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
bpftool | 4.18.0-521.el8 | |
cockpit | 304-1.el8 | |
cockpit-bridge | 304-1.el8 | |
cockpit-doc | 304-1.el8 | |
cockpit-system | 304-1.el8 | |
cockpit-ws | 304-1.el8 | |
kernel | 4.18.0-521.el8 | |
kernel-abi-stablelists | 4.18.0-521.el8 | |
kernel-core | 4.18.0-521.el8 | |
kernel-cross-headers | 4.18.0-521.el8 | |
kernel-debug | 4.18.0-521.el8 | |
kernel-debug-core | 4.18.0-521.el8 | |
kernel-debug-devel | 4.18.0-521.el8 | |
kernel-debug-modules | 4.18.0-521.el8 | |
kernel-debug-modules-extra | 4.18.0-521.el8 | |
kernel-devel | 4.18.0-521.el8 | |
kernel-doc | 4.18.0-521.el8 | |
kernel-headers | 4.18.0-521.el8 | |
kernel-modules | 4.18.0-521.el8 | |
kernel-modules-extra | 4.18.0-521.el8 | |
kernel-tools | 4.18.0-521.el8 | |
kernel-tools-libs | 4.18.0-521.el8 | |
libdnf | 0.63.0-18.el8 | |
mcelog | 195-0.el8 | |
pam | 1.3.1-30.el8 | |
pam-devel | 1.3.1-30.el8 | |
perf | 4.18.0-521.el8 | |
python3-hawkey | 0.63.0-18.el8 | |
python3-libdnf | 0.63.0-18.el8 | |
python3-linux-procfs | 0.7.2-1.el8 | |
python3-perf | 4.18.0-521.el8 | |
shadow-utils | 4.6-21.el8 | |
shadow-utils-subid | 4.6-21.el8 | |
tuna | 0.18-7.el8 | |

### AppStream x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
clang | 17.0.2-1.module_el8+698+172ae44f | |
clang-analyzer | 17.0.2-1.module_el8+698+172ae44f | |
clang-devel | 17.0.2-1.module_el8+698+172ae44f | |
clang-libs | 17.0.2-1.module_el8+698+172ae44f | |
clang-resource-filesystem | 17.0.2-1.module_el8+698+172ae44f | |
clang-tools-extra | 17.0.2-1.module_el8+698+172ae44f | |
clang-tools-extra-devel | 17.0.2-1.module_el8+698+172ae44f | |
cockpit-machines | 304-1.el8 | |
cockpit-packagekit | 304-1.el8 | |
cockpit-pcp | 304-1.el8 | |
cockpit-storaged | 304-1.el8 | |
compiler-rt | 17.0.2-2.module_el8+721+8e6a0389 | |
firefox | 115.4.0-1.el8 | [RHSA-2023:6187](https://access.redhat.com/errata/RHSA-2023:6187) | <div class="adv_s">Security Advisory</div> ([CVE-2023-44488](https://access.redhat.com/security/cve/CVE-2023-44488), [CVE-2023-5721](https://access.redhat.com/security/cve/CVE-2023-5721), [CVE-2023-5724](https://access.redhat.com/security/cve/CVE-2023-5724), [CVE-2023-5725](https://access.redhat.com/security/cve/CVE-2023-5725), [CVE-2023-5728](https://access.redhat.com/security/cve/CVE-2023-5728), [CVE-2023-5730](https://access.redhat.com/security/cve/CVE-2023-5730), [CVE-2023-5732](https://access.redhat.com/security/cve/CVE-2023-5732))
git-clang-format | 17.0.2-1.module_el8+698+172ae44f | |
gnome-shell | 3.32.2-54.el8 | |
jq | 1.6-8.el8 | |
libblockdev | 2.28-5.el8 | |
libblockdev-crypto | 2.28-5.el8 | |
libblockdev-dm | 2.28-5.el8 | |
libblockdev-fs | 2.28-5.el8 | |
libblockdev-kbd | 2.28-5.el8 | |
libblockdev-loop | 2.28-5.el8 | |
libblockdev-lvm | 2.28-5.el8 | |
libblockdev-lvm-dbus | 2.28-5.el8 | |
libblockdev-mdraid | 2.28-5.el8 | |
libblockdev-mpath | 2.28-5.el8 | |
libblockdev-nvdimm | 2.28-5.el8 | |
libblockdev-part | 2.28-5.el8 | |
libblockdev-plugins-all | 2.28-5.el8 | |
libblockdev-swap | 2.28-5.el8 | |
libblockdev-utils | 2.28-5.el8 | |
libblockdev-vdo | 2.28-5.el8 | |
libomp | 17.0.2-1.module_el8+721+8e6a0389 | |
libomp-devel | 17.0.2-1.module_el8+721+8e6a0389 | |
LibRaw | 0.19.5-4.el8 | |
lld | 17.0.2-1.module_el8+698+172ae44f | |
lld-devel | 17.0.2-1.module_el8+698+172ae44f | |
lld-libs | 17.0.2-1.module_el8+698+172ae44f | |
lldb | 17.0.2-1.module_el8+698+172ae44f | |
lldb-devel | 17.0.2-1.module_el8+698+172ae44f | |
llvm | 17.0.2-2.module_el8+738+cd33199b | |
llvm-cmake-utils | 17.0.2-2.module_el8+738+cd33199b | |
llvm-compat | 16.0.6-4.module_el8+729+5bfd1bbd | |
llvm-compat-libs | 16.0.6-4.module_el8+729+5bfd1bbd | |
llvm-devel | 17.0.2-2.module_el8+738+cd33199b | |
llvm-doc | 17.0.2-2.module_el8+738+cd33199b | |
llvm-googletest | 17.0.2-2.module_el8+738+cd33199b | |
llvm-libs | 17.0.2-2.module_el8+738+cd33199b | |
llvm-static | 17.0.2-2.module_el8+738+cd33199b | |
llvm-test | 17.0.2-2.module_el8+738+cd33199b | |
llvm-toolset | 17.0.2-2.module_el8+738+cd33199b | |
nmstate | 1.4.5-1.el8 | |
nmstate-libs | 1.4.5-1.el8 | |
nmstate-plugin-ovsdb | 1.4.5-1.el8 | |
python2 | 2.7.18-16.module_el8+739+75e8b99b | |
python2-debug | 2.7.18-16.module_el8+739+75e8b99b | |
python2-devel | 2.7.18-16.module_el8+739+75e8b99b | |
python2-libs | 2.7.18-16.module_el8+739+75e8b99b | |
python2-test | 2.7.18-16.module_el8+739+75e8b99b | |
python2-tkinter | 2.7.18-16.module_el8+739+75e8b99b | |
python2-tools | 2.7.18-16.module_el8+739+75e8b99b | |
python3-blockdev | 2.28-5.el8 | |
python3-clang | 17.0.2-1.module_el8+698+172ae44f | |
python3-libnmstate | 1.4.5-1.el8 | |
python3-lit | 17.0.2-1.module_el8+698+172ae44f | |
python3-lldb | 17.0.2-1.module_el8+698+172ae44f | |
rt-tests | 2.6-1.el8 | |
thunderbird | 115.4.1-1.el8 | [RHSA-2023:6194](https://access.redhat.com/errata/RHSA-2023:6194) | <div class="adv_s">Security Advisory</div> ([CVE-2023-44488](https://access.redhat.com/security/cve/CVE-2023-44488), [CVE-2023-5721](https://access.redhat.com/security/cve/CVE-2023-5721), [CVE-2023-5724](https://access.redhat.com/security/cve/CVE-2023-5724), [CVE-2023-5725](https://access.redhat.com/security/cve/CVE-2023-5725), [CVE-2023-5728](https://access.redhat.com/security/cve/CVE-2023-5728), [CVE-2023-5730](https://access.redhat.com/security/cve/CVE-2023-5730), [CVE-2023-5732](https://access.redhat.com/security/cve/CVE-2023-5732))
tigervnc | 1.13.1-4.el8 | |
tigervnc-icons | 1.13.1-4.el8 | |
tigervnc-license | 1.13.1-4.el8 | |
tigervnc-selinux | 1.13.1-4.el8 | |
tigervnc-server | 1.13.1-4.el8 | |
tigervnc-server-minimal | 1.13.1-4.el8 | |
tigervnc-server-module | 1.13.1-4.el8 | |
xorg-x11-server-common | 1.20.11-19.el8 | |
xorg-x11-server-Xdmx | 1.20.11-19.el8 | |
xorg-x11-server-Xephyr | 1.20.11-19.el8 | |
xorg-x11-server-Xnest | 1.20.11-19.el8 | |
xorg-x11-server-Xorg | 1.20.11-19.el8 | |
xorg-x11-server-Xvfb | 1.20.11-19.el8 | |
xorg-x11-server-Xwayland | 21.1.3-13.el8 | |

### PowerTools x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
jq-devel | 1.6-8.el8 | |
kernel-tools-libs-devel | 4.18.0-521.el8 | |
libblockdev-crypto-devel | 2.28-5.el8 | |
libblockdev-devel | 2.28-5.el8 | |
libblockdev-fs-devel | 2.28-5.el8 | |
libblockdev-loop-devel | 2.28-5.el8 | |
libblockdev-lvm-devel | 2.28-5.el8 | |
libblockdev-mdraid-devel | 2.28-5.el8 | |
libblockdev-part-devel | 2.28-5.el8 | |
libblockdev-swap-devel | 2.28-5.el8 | |
libblockdev-utils-devel | 2.28-5.el8 | |
libblockdev-vdo-devel | 2.28-5.el8 | |
libdnf-devel | 0.63.0-18.el8 | |
LibRaw-devel | 0.19.5-4.el8 | |
nmstate-devel | 1.4.5-1.el8 | |
shadow-utils-subid-devel | 4.6-21.el8 | |
xorg-x11-server-devel | 1.20.11-19.el8 | |
xorg-x11-server-source | 1.20.11-19.el8 | |

### HighAvailability x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
resource-agents | 4.9.0-49.el8 | |
resource-agents-aliyun | 4.9.0-49.el8 | |
resource-agents-gcp | 4.9.0-49.el8 | |
resource-agents-paf | 4.9.0-49.el8 | |

### RT x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
kernel-rt | 4.18.0-521.rt7.310.el8 | |
kernel-rt-core | 4.18.0-521.rt7.310.el8 | |
kernel-rt-debug | 4.18.0-521.rt7.310.el8 | |
kernel-rt-debug-core | 4.18.0-521.rt7.310.el8 | |
kernel-rt-debug-devel | 4.18.0-521.rt7.310.el8 | |
kernel-rt-debug-modules | 4.18.0-521.rt7.310.el8 | |
kernel-rt-debug-modules-extra | 4.18.0-521.rt7.310.el8 | |
kernel-rt-devel | 4.18.0-521.rt7.310.el8 | |
kernel-rt-modules | 4.18.0-521.rt7.310.el8 | |
kernel-rt-modules-extra | 4.18.0-521.rt7.310.el8 | |

### ResilientStorage x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
resource-agents | 4.9.0-49.el8 | |
resource-agents-aliyun | 4.9.0-49.el8 | |
resource-agents-gcp | 4.9.0-49.el8 | |
resource-agents-paf | 4.9.0-49.el8 | |

### BaseOS aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
bpftool | 4.18.0-521.el8 | |
cockpit | 304-1.el8 | |
cockpit-bridge | 304-1.el8 | |
cockpit-doc | 304-1.el8 | |
cockpit-system | 304-1.el8 | |
cockpit-ws | 304-1.el8 | |
kernel | 4.18.0-521.el8 | |
kernel-abi-stablelists | 4.18.0-521.el8 | |
kernel-core | 4.18.0-521.el8 | |
kernel-cross-headers | 4.18.0-521.el8 | |
kernel-debug | 4.18.0-521.el8 | |
kernel-debug-core | 4.18.0-521.el8 | |
kernel-debug-devel | 4.18.0-521.el8 | |
kernel-debug-modules | 4.18.0-521.el8 | |
kernel-debug-modules-extra | 4.18.0-521.el8 | |
kernel-devel | 4.18.0-521.el8 | |
kernel-doc | 4.18.0-521.el8 | |
kernel-headers | 4.18.0-521.el8 | |
kernel-modules | 4.18.0-521.el8 | |
kernel-modules-extra | 4.18.0-521.el8 | |
kernel-tools | 4.18.0-521.el8 | |
kernel-tools-libs | 4.18.0-521.el8 | |
libdnf | 0.63.0-18.el8 | |
pam | 1.3.1-30.el8 | |
pam-devel | 1.3.1-30.el8 | |
perf | 4.18.0-521.el8 | |
python3-hawkey | 0.63.0-18.el8 | |
python3-libdnf | 0.63.0-18.el8 | |
python3-linux-procfs | 0.7.2-1.el8 | |
python3-perf | 4.18.0-521.el8 | |
shadow-utils | 4.6-21.el8 | |
shadow-utils-subid | 4.6-21.el8 | |
tuna | 0.18-7.el8 | |

### AppStream aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
clang | 17.0.2-1.module_el8+698+172ae44f | |
clang-analyzer | 17.0.2-1.module_el8+698+172ae44f | |
clang-devel | 17.0.2-1.module_el8+698+172ae44f | |
clang-libs | 17.0.2-1.module_el8+698+172ae44f | |
clang-resource-filesystem | 17.0.2-1.module_el8+698+172ae44f | |
clang-tools-extra | 17.0.2-1.module_el8+698+172ae44f | |
clang-tools-extra-devel | 17.0.2-1.module_el8+698+172ae44f | |
cockpit-machines | 304-1.el8 | |
cockpit-packagekit | 304-1.el8 | |
cockpit-pcp | 304-1.el8 | |
cockpit-storaged | 304-1.el8 | |
compiler-rt | 17.0.2-2.module_el8+721+8e6a0389 | |
firefox | 115.4.0-1.el8 | [RHSA-2023:6187](https://access.redhat.com/errata/RHSA-2023:6187) | <div class="adv_s">Security Advisory</div> ([CVE-2023-44488](https://access.redhat.com/security/cve/CVE-2023-44488), [CVE-2023-5721](https://access.redhat.com/security/cve/CVE-2023-5721), [CVE-2023-5724](https://access.redhat.com/security/cve/CVE-2023-5724), [CVE-2023-5725](https://access.redhat.com/security/cve/CVE-2023-5725), [CVE-2023-5728](https://access.redhat.com/security/cve/CVE-2023-5728), [CVE-2023-5730](https://access.redhat.com/security/cve/CVE-2023-5730), [CVE-2023-5732](https://access.redhat.com/security/cve/CVE-2023-5732))
git-clang-format | 17.0.2-1.module_el8+698+172ae44f | |
gnome-shell | 3.32.2-54.el8 | |
jq | 1.6-8.el8 | |
libblockdev | 2.28-5.el8 | |
libblockdev-crypto | 2.28-5.el8 | |
libblockdev-dm | 2.28-5.el8 | |
libblockdev-fs | 2.28-5.el8 | |
libblockdev-kbd | 2.28-5.el8 | |
libblockdev-loop | 2.28-5.el8 | |
libblockdev-lvm | 2.28-5.el8 | |
libblockdev-lvm-dbus | 2.28-5.el8 | |
libblockdev-mdraid | 2.28-5.el8 | |
libblockdev-mpath | 2.28-5.el8 | |
libblockdev-nvdimm | 2.28-5.el8 | |
libblockdev-part | 2.28-5.el8 | |
libblockdev-plugins-all | 2.28-5.el8 | |
libblockdev-swap | 2.28-5.el8 | |
libblockdev-utils | 2.28-5.el8 | |
libblockdev-vdo | 2.28-5.el8 | |
libomp | 17.0.2-1.module_el8+721+8e6a0389 | |
libomp-devel | 17.0.2-1.module_el8+721+8e6a0389 | |
lld | 17.0.2-1.module_el8+698+172ae44f | |
lld-devel | 17.0.2-1.module_el8+698+172ae44f | |
lld-libs | 17.0.2-1.module_el8+698+172ae44f | |
lldb | 17.0.2-1.module_el8+698+172ae44f | |
lldb-devel | 17.0.2-1.module_el8+698+172ae44f | |
llvm | 17.0.2-2.module_el8+738+cd33199b | |
llvm-cmake-utils | 17.0.2-2.module_el8+738+cd33199b | |
llvm-compat | 16.0.6-4.module_el8+729+5bfd1bbd | |
llvm-compat-libs | 16.0.6-4.module_el8+729+5bfd1bbd | |
llvm-devel | 17.0.2-2.module_el8+738+cd33199b | |
llvm-doc | 17.0.2-2.module_el8+738+cd33199b | |
llvm-googletest | 17.0.2-2.module_el8+738+cd33199b | |
llvm-libs | 17.0.2-2.module_el8+738+cd33199b | |
llvm-static | 17.0.2-2.module_el8+738+cd33199b | |
llvm-test | 17.0.2-2.module_el8+738+cd33199b | |
llvm-toolset | 17.0.2-2.module_el8+738+cd33199b | |
nmstate | 1.4.5-1.el8 | |
nmstate-libs | 1.4.5-1.el8 | |
nmstate-plugin-ovsdb | 1.4.5-1.el8 | |
python2 | 2.7.18-16.module_el8+739+75e8b99b | |
python2-debug | 2.7.18-16.module_el8+739+75e8b99b | |
python2-devel | 2.7.18-16.module_el8+739+75e8b99b | |
python2-libs | 2.7.18-16.module_el8+739+75e8b99b | |
python2-test | 2.7.18-16.module_el8+739+75e8b99b | |
python2-tkinter | 2.7.18-16.module_el8+739+75e8b99b | |
python2-tools | 2.7.18-16.module_el8+739+75e8b99b | |
python3-blockdev | 2.28-5.el8 | |
python3-clang | 17.0.2-1.module_el8+698+172ae44f | |
python3-libnmstate | 1.4.5-1.el8 | |
python3-lit | 17.0.2-1.module_el8+698+172ae44f | |
python3-lldb | 17.0.2-1.module_el8+698+172ae44f | |
thunderbird | 115.4.1-1.el8 | [RHSA-2023:6194](https://access.redhat.com/errata/RHSA-2023:6194) | <div class="adv_s">Security Advisory</div> ([CVE-2023-44488](https://access.redhat.com/security/cve/CVE-2023-44488), [CVE-2023-5721](https://access.redhat.com/security/cve/CVE-2023-5721), [CVE-2023-5724](https://access.redhat.com/security/cve/CVE-2023-5724), [CVE-2023-5725](https://access.redhat.com/security/cve/CVE-2023-5725), [CVE-2023-5728](https://access.redhat.com/security/cve/CVE-2023-5728), [CVE-2023-5730](https://access.redhat.com/security/cve/CVE-2023-5730), [CVE-2023-5732](https://access.redhat.com/security/cve/CVE-2023-5732))
tigervnc | 1.13.1-4.el8 | |
tigervnc-icons | 1.13.1-4.el8 | |
tigervnc-license | 1.13.1-4.el8 | |
tigervnc-selinux | 1.13.1-4.el8 | |
tigervnc-server | 1.13.1-4.el8 | |
tigervnc-server-minimal | 1.13.1-4.el8 | |
tigervnc-server-module | 1.13.1-4.el8 | |
xorg-x11-server-common | 1.20.11-19.el8 | |
xorg-x11-server-Xdmx | 1.20.11-19.el8 | |
xorg-x11-server-Xephyr | 1.20.11-19.el8 | |
xorg-x11-server-Xnest | 1.20.11-19.el8 | |
xorg-x11-server-Xorg | 1.20.11-19.el8 | |
xorg-x11-server-Xvfb | 1.20.11-19.el8 | |
xorg-x11-server-Xwayland | 21.1.3-13.el8 | |

### PowerTools aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
jq-devel | 1.6-8.el8 | |
kernel-tools-libs-devel | 4.18.0-521.el8 | |
libblockdev-crypto-devel | 2.28-5.el8 | |
libblockdev-devel | 2.28-5.el8 | |
libblockdev-fs-devel | 2.28-5.el8 | |
libblockdev-loop-devel | 2.28-5.el8 | |
libblockdev-lvm-devel | 2.28-5.el8 | |
libblockdev-mdraid-devel | 2.28-5.el8 | |
libblockdev-part-devel | 2.28-5.el8 | |
libblockdev-swap-devel | 2.28-5.el8 | |
libblockdev-utils-devel | 2.28-5.el8 | |
libblockdev-vdo-devel | 2.28-5.el8 | |
libdnf-devel | 0.63.0-18.el8 | |
nmstate-devel | 1.4.5-1.el8 | |
shadow-utils-subid-devel | 4.6-21.el8 | |
xorg-x11-server-devel | 1.20.11-19.el8 | |
xorg-x11-server-source | 1.20.11-19.el8 | |

### HighAvailability aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
resource-agents | 4.9.0-49.el8 | |
resource-agents-paf | 4.9.0-49.el8 | |

