## 2022-01-25

### messaging x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
python-qpid-proton-docs | 0.35.0-3.el8s | |
python3-qpid-proton | 0.35.0-3.el8s | |
qpid-proton-c | 0.35.0-3.el8s | |
qpid-proton-c-devel | 0.35.0-3.el8s | |
qpid-proton-c-docs | 0.35.0-3.el8s | |
qpid-proton-cpp | 0.35.0-3.el8s | |
qpid-proton-cpp-devel | 0.35.0-3.el8s | |
qpid-proton-cpp-docs | 0.35.0-3.el8s | |
qpid-proton-tests | 0.35.0-3.el8s | |
rubygem-qpid_proton | 0.35.0-3.el8s | |

### messaging aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
python-qpid-proton-docs | 0.35.0-3.el8s | |
python3-qpid-proton | 0.35.0-3.el8s | |
qpid-proton-c | 0.35.0-3.el8s | |
qpid-proton-c-devel | 0.35.0-3.el8s | |
qpid-proton-c-docs | 0.35.0-3.el8s | |
qpid-proton-cpp | 0.35.0-3.el8s | |
qpid-proton-cpp-devel | 0.35.0-3.el8s | |
qpid-proton-cpp-docs | 0.35.0-3.el8s | |
qpid-proton-tests | 0.35.0-3.el8s | |
rubygem-qpid_proton | 0.35.0-3.el8s | |

