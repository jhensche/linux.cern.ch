## 2024-01-30

### cloud x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
openstack-kolla | 14.12.0-1.el8 | |
python-oslo-utils-doc | 4.12.4-1.el8 | |
python-oslo-utils-lang | 4.12.4-1.el8 | |
python3-oslo-utils | 4.12.4-1.el8 | |
python3-oslo-utils-tests | 4.12.4-1.el8 | |

### cloud aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
openstack-kolla | 14.12.0-1.el8 | |
python-oslo-utils-doc | 4.12.4-1.el8 | |
python-oslo-utils-lang | 4.12.4-1.el8 | |
python3-oslo-utils | 4.12.4-1.el8 | |
python3-oslo-utils-tests | 4.12.4-1.el8 | |

