## 2021-08-12

### BaseOS x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
cockpit | 250-1.el8 | |
cockpit-bridge | 250-1.el8 | |
cockpit-doc | 250-1.el8 | |
cockpit-system | 250-1.el8 | |
cockpit-ws | 250-1.el8 | |
device-mapper | 1.02.177-5.el8 | |
device-mapper-event | 1.02.177-5.el8 | |
device-mapper-event-libs | 1.02.177-5.el8 | |
device-mapper-libs | 1.02.177-5.el8 | |
dnf | 4.7.0-2.el8 | |
dnf-automatic | 4.7.0-2.el8 | |
dnf-data | 4.7.0-2.el8 | |
dnf-plugin-subscription-manager | 1.28.20-1.el8 | |
dnf-plugins-core | 4.0.21-2.el8 | |
dracut | 049-188.git20210802.el8 | |
dracut-caps | 049-188.git20210802.el8 | |
dracut-config-generic | 049-188.git20210802.el8 | |
dracut-config-rescue | 049-188.git20210802.el8 | |
dracut-live | 049-188.git20210802.el8 | |
dracut-network | 049-188.git20210802.el8 | |
dracut-squash | 049-188.git20210802.el8 | |
dracut-tools | 049-188.git20210802.el8 | |
emacs-filesystem | 26.1-7.el8 | |
firewalld | 0.9.3-7.el8 | |
firewalld-filesystem | 0.9.3-7.el8 | |
iproute | 5.12.0-2.el8 | |
iproute-tc | 5.12.0-2.el8 | |
iscsi-initiator-utils | 6.2.1.4-4.git095f59c.el8 | |
iscsi-initiator-utils-iscsiuio | 6.2.1.4-4.git095f59c.el8 | |
iwl100-firmware | 39.31.5.1-103.el8.1 | |
iwl1000-firmware | 39.31.5.1-103.el8.1 | |
iwl105-firmware | 18.168.6.1-103.el8.1 | |
iwl135-firmware | 18.168.6.1-103.el8.1 | |
iwl2000-firmware | 18.168.6.1-103.el8.1 | |
iwl2030-firmware | 18.168.6.1-103.el8.1 | |
iwl3160-firmware | 25.30.13.0-103.el8.1 | |
iwl3945-firmware | 15.32.2.9-103.el8.1 | |
iwl4965-firmware | 228.61.2.24-103.el8.1 | |
iwl5000-firmware | 8.83.5.1_1-103.el8.1 | |
iwl5150-firmware | 8.24.2.2-103.el8.1 | |
iwl6000-firmware | 9.221.4.1-103.el8.1 | |
iwl6000g2a-firmware | 18.168.6.1-103.el8.1 | |
iwl6000g2b-firmware | 18.168.6.1-103.el8.1 | |
iwl6050-firmware | 41.28.5.1-103.el8.1 | |
iwl7260-firmware | 25.30.13.0-103.el8.1 | |
kexec-tools | 2.0.20-56.el8 | |
libdnf | 0.63.0-2.el8 | |
libertas-sd8686-firmware | 20210702-103.gitd79c2677.el8 | |
libertas-sd8787-firmware | 20210702-103.gitd79c2677.el8 | |
libertas-usb8388-firmware | 20210702-103.gitd79c2677.el8 | |
libertas-usb8388-olpc-firmware | 20210702-103.gitd79c2677.el8 | |
libipa_hbac | 2.5.2-2.el8 | |
libqmi | 1.24.0-3.el8 | |
libqmi-utils | 1.24.0-3.el8 | |
libsolv | 0.7.19-1.el8 | |
libsss_autofs | 2.5.2-2.el8 | |
libsss_certmap | 2.5.2-2.el8 | |
libsss_idmap | 2.5.2-2.el8 | |
libsss_nss_idmap | 2.5.2-2.el8 | |
libsss_simpleifp | 2.5.2-2.el8 | |
libsss_sudo | 2.5.2-2.el8 | |
linux-firmware | 20210702-103.gitd79c2677.el8 | |
lvm2 | 2.03.12-5.el8 | |
lvm2-dbusd | 2.03.12-5.el8 | |
lvm2-libs | 2.03.12-5.el8 | |
lvm2-lockd | 2.03.12-5.el8 | |
lz4 | 1.8.3-3.el8_4 | [RHSA-2021:2575](https://access.redhat.com/errata/RHSA-2021:2575) | <div class="adv_s">Security Advisory</div>
lz4-devel | 1.8.3-3.el8_4 | [RHSA-2021:2575](https://access.redhat.com/errata/RHSA-2021:2575) | <div class="adv_s">Security Advisory</div>
lz4-libs | 1.8.3-3.el8_4 | [RHSA-2021:2575](https://access.redhat.com/errata/RHSA-2021:2575) | <div class="adv_s">Security Advisory</div>
mcelog | 175-1.el8 | |
mobile-broadband-provider-info | 20210805-1.el8 | |
NetworkManager | 1.32.6-1.el8 | |
NetworkManager-adsl | 1.32.6-1.el8 | |
NetworkManager-bluetooth | 1.32.6-1.el8 | |
NetworkManager-config-connectivity-redhat | 1.32.6-1.el8 | |
NetworkManager-config-server | 1.32.6-1.el8 | |
NetworkManager-dispatcher-routing-rules | 1.32.6-1.el8 | |
NetworkManager-libnm | 1.32.6-1.el8 | |
NetworkManager-ovs | 1.32.6-1.el8 | |
NetworkManager-ppp | 1.32.6-1.el8 | |
NetworkManager-team | 1.32.6-1.el8 | |
NetworkManager-tui | 1.32.6-1.el8 | |
NetworkManager-wifi | 1.32.6-1.el8 | |
NetworkManager-wwan | 1.32.6-1.el8 | |
OpenIPMI | 2.0.31-2.el8 | |
OpenIPMI-lanserv | 2.0.31-2.el8 | |
OpenIPMI-libs | 2.0.31-2.el8 | |
OpenIPMI-perl | 2.0.31-2.el8 | |
openldap | 2.4.46-18.el8 | |
openldap-clients | 2.4.46-18.el8 | |
openldap-devel | 2.4.46-18.el8 | |
postfix | 3.5.8-2.el8 | |
python3-cloud-what | 1.28.20-1.el8 | |
python3-dnf | 4.7.0-2.el8 | |
python3-dnf-plugin-post-transaction-actions | 4.0.21-2.el8 | |
python3-dnf-plugin-versionlock | 4.0.21-2.el8 | |
python3-dnf-plugins-core | 4.0.21-2.el8 | |
python3-firewall | 0.9.3-7.el8 | |
python3-hawkey | 0.63.0-2.el8 | |
python3-iscsi-initiator-utils | 6.2.1.4-4.git095f59c.el8 | |
python3-libdnf | 0.63.0-2.el8 | |
python3-libipa_hbac | 2.5.2-2.el8 | |
python3-libsss_nss_idmap | 2.5.2-2.el8 | |
python3-openipmi | 2.0.31-2.el8 | |
python3-solv | 0.7.19-1.el8 | |
python3-sss | 2.5.2-2.el8 | |
python3-sss-murmur | 2.5.2-2.el8 | |
python3-sssdconfig | 2.5.2-2.el8 | |
python3-subscription-manager-rhsm | 1.28.20-1.el8 | |
python3-syspurpose | 1.28.20-1.el8 | |
rhsm-icons | 1.28.20-1.el8 | |
rng-tools | 6.13-1.git.d207e0b6.el8 | |
sssd | 2.5.2-2.el8 | |
sssd-ad | 2.5.2-2.el8 | |
sssd-client | 2.5.2-2.el8 | |
sssd-common | 2.5.2-2.el8 | |
sssd-common-pac | 2.5.2-2.el8 | |
sssd-dbus | 2.5.2-2.el8 | |
sssd-ipa | 2.5.2-2.el8 | |
sssd-kcm | 2.5.2-2.el8 | |
sssd-krb5 | 2.5.2-2.el8 | |
sssd-krb5-common | 2.5.2-2.el8 | |
sssd-ldap | 2.5.2-2.el8 | |
sssd-nfs-idmap | 2.5.2-2.el8 | |
sssd-polkit-rules | 2.5.2-2.el8 | |
sssd-proxy | 2.5.2-2.el8 | |
sssd-tools | 2.5.2-2.el8 | |
sssd-winbind-idmap | 2.5.2-2.el8 | |
subscription-manager | 1.28.20-1.el8 | |
subscription-manager-cockpit | 1.28.20-1.el8 | |
subscription-manager-plugin-ostree | 1.28.20-1.el8 | |
subscription-manager-rhsm-certificates | 1.28.20-1.el8 | |
tpm2-tools | 4.1.1-5.el8 | |
yum | 4.7.0-2.el8 | |
yum-utils | 4.0.21-2.el8 | |

### AppStream x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
anaconda | 33.16.5.4-1.el8 | |
anaconda-core | 33.16.5.4-1.el8 | |
anaconda-dracut | 33.16.5.4-1.el8 | |
anaconda-gui | 33.16.5.4-1.el8 | |
anaconda-install-env-deps | 33.16.5.4-1.el8 | |
anaconda-tui | 33.16.5.4-1.el8 | |
anaconda-widgets | 33.16.5.4-1.el8 | |
ansible-collection-microsoft-sql | 1.1.0-1.el8 | |
apr | 1.6.3-12.el8 | |
apr-devel | 1.6.3-12.el8 | |
blivet-data | 3.4.0-4.el8 | |
buildah | 1.22.0-2.module_el8.5.0+877+1c30e0c9 | |
buildah-tests | 1.22.0-2.module_el8.5.0+877+1c30e0c9 | |
cockpit-machines | 250-1.el8 | |
cockpit-packagekit | 250-1.el8 | |
cockpit-pcp | 250-1.el8 | |
cockpit-podman | 33-1.module_el8.5.0+877+1c30e0c9 | |
cockpit-storaged | 250-1.el8 | |
compat-exiv2-026 | 0.26-5.el8 | |
conmon | 2.0.29-1.module_el8.5.0+877+1c30e0c9 | |
container-selinux | 2.164.2-1.module_el8.5.0+877+1c30e0c9 | |
containernetworking-plugins | 0.9.1-1.module_el8.5.0+877+1c30e0c9 | |
containers-common | 1.4.0-5.module_el8.5.0+878+851f435b | |
coreos-installer-bootinfra | 0.9.1-17.el8 | |
corosynclib | 3.1.5-1.el8 | |
createrepo_c | 0.17.2-2.el8 | |
createrepo_c-devel | 0.17.2-2.el8 | |
createrepo_c-libs | 0.17.2-2.el8 | |
crit | 3.15-2.module_el8.5.0+877+1c30e0c9 | |
criu | 3.15-2.module_el8.5.0+877+1c30e0c9 | |
criu-devel | 3.15-2.module_el8.5.0+877+1c30e0c9 | |
criu-libs | 3.15-2.module_el8.5.0+877+1c30e0c9 | |
crun | 0.21-3.module_el8.5.0+878+851f435b | |
dnsmasq | 2.79-19.el8 | |
dnsmasq-utils | 2.79-19.el8 | |
dyninst | 11.0.0-3.el8 | |
edk2-ovmf | 20210527gite1999b264f1f-3.el8 | |
emacs | 26.1-7.el8 | |
emacs-common | 26.1-7.el8 | |
emacs-lucid | 26.1-7.el8 | |
emacs-nox | 26.1-7.el8 | |
emacs-terminal | 26.1-7.el8 | |
exiv2 | 0.27.4-2.el8 | |
exiv2-libs | 0.27.4-2.el8 | |
firefox | 78.13.0-2.el8_4 | |
firewall-applet | 0.9.3-7.el8 | |
firewall-config | 0.9.3-7.el8 | |
fuse-overlayfs | 1.7-1.module_el8.5.0+877+1c30e0c9 | |
gcc-toolset-10-elfutils | 0.182-5.el8_4 | [RHBA-2021:3082](https://access.redhat.com/errata/RHBA-2021:3082) | <div class="adv_b">Bug Fix Advisory</div>
gcc-toolset-10-elfutils-debuginfod-client | 0.182-5.el8_4 | [RHSA-2020:1665](https://access.redhat.com/errata/RHSA-2020:1665) | <div class="adv_s">Security Advisory</div>
gcc-toolset-10-elfutils-debuginfod-client-devel | 0.182-5.el8_4 | [RHSA-2020:1665](https://access.redhat.com/errata/RHSA-2020:1665) | <div class="adv_s">Security Advisory</div>
gcc-toolset-10-elfutils-devel | 0.182-5.el8_4 | [RHBA-2021:3082](https://access.redhat.com/errata/RHBA-2021:3082) | <div class="adv_b">Bug Fix Advisory</div>
gcc-toolset-10-elfutils-libelf | 0.182-5.el8_4 | [RHBA-2021:3082](https://access.redhat.com/errata/RHBA-2021:3082) | <div class="adv_b">Bug Fix Advisory</div>
gcc-toolset-10-elfutils-libelf-devel | 0.182-5.el8_4 | [RHBA-2021:3082](https://access.redhat.com/errata/RHBA-2021:3082) | <div class="adv_b">Bug Fix Advisory</div>
gcc-toolset-10-elfutils-libs | 0.182-5.el8_4 | [RHBA-2021:3082](https://access.redhat.com/errata/RHBA-2021:3082) | <div class="adv_b">Bug Fix Advisory</div>
gcc-toolset-10-valgrind | 3.16.0-6.el8_4 | [RHBA-2021:3083](https://access.redhat.com/errata/RHBA-2021:3083) | <div class="adv_b">Bug Fix Advisory</div>
gcc-toolset-10-valgrind-devel | 3.16.0-6.el8_4 | [RHBA-2021:3083](https://access.redhat.com/errata/RHBA-2021:3083) | <div class="adv_b">Bug Fix Advisory</div>
gdb | 8.2-16.el8 | |
gdb-doc | 8.2-16.el8 | |
gdb-gdbserver | 8.2-16.el8 | |
gdb-headless | 8.2-16.el8 | |
gnome-session | 3.28.1-13.el8 | |
gnome-session-kiosk-session | 3.28.1-13.el8 | |
gnome-session-wayland-session | 3.28.1-13.el8 | |
gnome-session-xsession | 3.28.1-13.el8 | |
gnome-shell | 3.32.2-38.el8 | |
java-17-openjdk | 17.0.0.0.26-0.2.ea.el8 | |
java-17-openjdk-headless | 17.0.0.0.26-0.2.ea.el8 | |
jss | 4.9.0-1.module_el8.5.0+876+d4bb8aa6 | |
jss-javadoc | 4.9.0-1.module_el8.5.0+876+d4bb8aa6 | |
ldapjdk | 4.23.0-1.module_el8.5.0+876+d4bb8aa6 | |
ldapjdk-javadoc | 4.23.0-1.module_el8.5.0+876+d4bb8aa6 | |
libserf | 1.3.9-9.module_el8.3.0+703+ba2f61b7 | |
libslirp | 4.4.0-1.module_el8.5.0+877+1c30e0c9 | |
libslirp-devel | 4.4.0-1.module_el8.5.0+877+1c30e0c9 | |
libuv | 1.41.1-1.el8_4 | [RHSA-2021:3075](https://access.redhat.com/errata/RHSA-2021:3075) | <div class="adv_s">Security Advisory</div>
linuxptp | 3.1.1-1.el8 | |
mod_dav_svn | 1.10.2-4.module_el8.3.0+703+ba2f61b7 | |
NetworkManager-cloud-setup | 1.32.6-1.el8 | |
oci-seccomp-bpf-hook | 1.2.3-3.module_el8.5.0+877+1c30e0c9 | |
openscap | 1.3.5-6.el8 | |
openscap-devel | 1.3.5-6.el8 | |
openscap-engine-sce | 1.3.5-6.el8 | |
openscap-python3 | 1.3.5-6.el8 | |
openscap-scanner | 1.3.5-6.el8 | |
openscap-utils | 1.3.5-6.el8 | |
oscap-anaconda-addon | 1.2.1-1.el8 | |
pacemaker-cluster-libs | 2.1.0-5.el8 | |
pacemaker-libs | 2.1.0-5.el8 | |
pacemaker-schemas | 2.1.0-5.el8 | |
pki-acme | 10.11.0-1.module_el8.5.0+876+d4bb8aa6 | |
pki-base | 10.11.0-1.module_el8.5.0+876+d4bb8aa6 | |
pki-base-java | 10.11.0-1.module_el8.5.0+876+d4bb8aa6 | |
pki-ca | 10.11.0-1.module_el8.5.0+876+d4bb8aa6 | |
pki-kra | 10.11.0-1.module_el8.5.0+876+d4bb8aa6 | |
pki-server | 10.11.0-1.module_el8.5.0+876+d4bb8aa6 | |
pki-symkey | 10.11.0-1.module_el8.5.0+876+d4bb8aa6 | |
pki-tools | 10.11.0-1.module_el8.5.0+876+d4bb8aa6 | |
podman | 3.3.0-2.module_el8.5.0+877+1c30e0c9 | |
podman-catatonit | 3.3.0-2.module_el8.5.0+877+1c30e0c9 | |
podman-docker | 3.3.0-2.module_el8.5.0+877+1c30e0c9 | |
podman-plugins | 3.3.0-2.module_el8.5.0+877+1c30e0c9 | |
podman-remote | 3.3.0-2.module_el8.5.0+877+1c30e0c9 | |
podman-tests | 3.3.0-2.module_el8.5.0+877+1c30e0c9 | |
poppler | 20.11.0-3.el8 | |
poppler-glib | 20.11.0-3.el8 | |
poppler-utils | 20.11.0-3.el8 | |
postfix-cdb | 3.5.8-2.el8 | |
postfix-ldap | 3.5.8-2.el8 | |
postfix-mysql | 3.5.8-2.el8 | |
postfix-pcre | 3.5.8-2.el8 | |
postfix-perl-scripts | 3.5.8-2.el8 | |
postfix-pgsql | 3.5.8-2.el8 | |
postfix-sqlite | 3.5.8-2.el8 | |
python3-blivet | 3.4.0-4.el8 | |
python3-createrepo_c | 0.17.2-2.el8 | |
python3-criu | 3.15-2.module_el8.5.0+877+1c30e0c9 | |
python3-pillow | 5.1.1-16.el8 | |
python3-pki | 10.11.0-1.module_el8.5.0+876+d4bb8aa6 | |
python3-podman | 3.2.0-2.module_el8.5.0+877+1c30e0c9 | |
rhel-system-roles | 1.6.6-1.el8 | |
rsyslog | 8.2102.0-5.el8 | |
rsyslog-crypto | 8.2102.0-5.el8 | |
rsyslog-doc | 8.2102.0-5.el8 | |
rsyslog-elasticsearch | 8.2102.0-5.el8 | |
rsyslog-gnutls | 8.2102.0-5.el8 | |
rsyslog-gssapi | 8.2102.0-5.el8 | |
rsyslog-kafka | 8.2102.0-5.el8 | |
rsyslog-mmaudit | 8.2102.0-5.el8 | |
rsyslog-mmjsonparse | 8.2102.0-5.el8 | |
rsyslog-mmkubernetes | 8.2102.0-5.el8 | |
rsyslog-mmnormalize | 8.2102.0-5.el8 | |
rsyslog-mmsnmptrapd | 8.2102.0-5.el8 | |
rsyslog-mysql | 8.2102.0-5.el8 | |
rsyslog-omamqp1 | 8.2102.0-5.el8 | |
rsyslog-pgsql | 8.2102.0-5.el8 | |
rsyslog-relp | 8.2102.0-5.el8 | |
rsyslog-snmp | 8.2102.0-5.el8 | |
rsyslog-udpspoof | 8.2102.0-5.el8 | |
runc | 1.0.1-5.module_el8.5.0+878+851f435b | |
sblim-gather | 2.2.9-23.el8 | |
scap-security-guide | 0.1.57-1.el8 | |
scap-security-guide-doc | 0.1.57-1.el8 | |
skopeo | 1.4.0-5.module_el8.5.0+878+851f435b | |
skopeo-tests | 1.4.0-5.module_el8.5.0+878+851f435b | |
slirp4netns | 1.1.8-1.module_el8.5.0+877+1c30e0c9 | |
spamassassin | 3.4.4-4.el8 | |
subscription-manager-migration | 1.28.20-1.el8 | |
subversion | 1.10.2-4.module_el8.3.0+703+ba2f61b7 | |
subversion-devel | 1.10.2-4.module_el8.3.0+703+ba2f61b7 | |
subversion-gnome | 1.10.2-4.module_el8.3.0+703+ba2f61b7 | |
subversion-javahl | 1.10.2-4.module_el8.3.0+703+ba2f61b7 | |
subversion-libs | 1.10.2-4.module_el8.3.0+703+ba2f61b7 | |
subversion-perl | 1.10.2-4.module_el8.3.0+703+ba2f61b7 | |
subversion-tools | 1.10.2-4.module_el8.3.0+703+ba2f61b7 | |
systemtap | 4.5-2.el8 | |
systemtap-client | 4.5-2.el8 | |
systemtap-devel | 4.5-2.el8 | |
systemtap-exporter | 4.5-2.el8 | |
systemtap-initscript | 4.5-2.el8 | |
systemtap-runtime | 4.5-2.el8 | |
systemtap-runtime-java | 4.5-2.el8 | |
systemtap-runtime-python3 | 4.5-2.el8 | |
systemtap-runtime-virtguest | 4.5-2.el8 | |
systemtap-runtime-virthost | 4.5-2.el8 | |
systemtap-sdt-devel | 4.5-2.el8 | |
systemtap-server | 4.5-2.el8 | |
thunderbird | 78.13.0-1.el8_4 | |
tomcatjss | 7.7.0-1.module_el8.5.0+876+d4bb8aa6 | |
toolbox | 0.0.99.3-1.module_el8.5.0+877+1c30e0c9 | |
toolbox-tests | 0.0.99.3-1.module_el8.5.0+877+1c30e0c9 | |
udica | 0.2.4-2.module_el8.5.0+877+1c30e0c9 | |
utf8proc | 2.1.1-5.module_el8.3.0+703+ba2f61b7 | |
virt-who | 1.30.7-1.el8 | |
virtio-win | 1.9.17-4.el8_4 | [RHBA-2021:3069](https://access.redhat.com/errata/RHBA-2021:3069) | <div class="adv_b">Bug Fix Advisory</div>
WALinuxAgent | 2.3.0.2-2.el8 | |
WALinuxAgent-udev | 2.3.0.2-2.el8 | |
xorg-x11-server-Xwayland | 21.1.1-6.el8 | |
zziplib | 0.13.68-9.el8 | |
zziplib-utils | 0.13.68-9.el8 | |

### PowerTools x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
corosync-vqsim | 3.1.5-1.el8 | |
device-mapper-devel | 1.02.177-5.el8 | |
device-mapper-event-devel | 1.02.177-5.el8 | |
dwarves | 1.21-0.el8 | |
dyninst-devel | 11.0.0-3.el8 | |
dyninst-doc | 11.0.0-3.el8 | |
dyninst-static | 11.0.0-3.el8 | |
dyninst-testsuite | 11.0.0-3.el8 | |
exiv2-devel | 0.27.4-2.el8 | |
exiv2-doc | 0.27.4-2.el8 | |
iproute-devel | 5.12.0-2.el8 | |
iscsi-initiator-utils-devel | 6.2.1.4-4.git095f59c.el8 | |
libdnf-devel | 0.63.0-2.el8 | |
libdwarves1 | 1.21-0.el8 | |
libsolv-devel | 0.7.19-1.el8 | |
libsolv-tools | 0.7.19-1.el8 | |
libsss_nss_idmap-devel | 2.5.2-2.el8 | |
libuv-devel | 1.41.1-1.el8_4 | [RHSA-2021:3075](https://access.redhat.com/errata/RHSA-2021:3075) | <div class="adv_s">Security Advisory</div>
lvm2-devel | 2.03.12-5.el8 | |
mobile-broadband-provider-info-devel | 20210805-1.el8 | |
NetworkManager-libnm-devel | 1.32.6-1.el8 | |
OpenIPMI-devel | 2.0.31-2.el8 | |
openscap-engine-sce-devel | 1.3.5-6.el8 | |
poppler-cpp | 20.11.0-3.el8 | |
poppler-cpp-devel | 20.11.0-3.el8 | |
poppler-devel | 20.11.0-3.el8 | |
poppler-glib-devel | 20.11.0-3.el8 | |
poppler-qt5 | 20.11.0-3.el8 | |
poppler-qt5-devel | 20.11.0-3.el8 | |
sblim-gather-provider | 2.2.9-23.el8 | |
zziplib-devel | 0.13.68-9.el8 | |

### HighAvailability x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
corosync | 3.1.5-1.el8 | |
corosynclib-devel | 3.1.5-1.el8 | |
pacemaker | 2.1.0-5.el8 | |
pacemaker-cli | 2.1.0-5.el8 | |
pacemaker-cts | 2.1.0-5.el8 | |
pacemaker-doc | 2.1.0-5.el8 | |
pacemaker-libs-devel | 2.1.0-5.el8 | |
pacemaker-nagios-plugins-metadata | 2.1.0-5.el8 | |
pacemaker-remote | 2.1.0-5.el8 | |
spausedd | 3.1.5-1.el8 | |

### RT x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
rteval | 3.2-2.el8 | |

### BaseOS aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
cockpit | 250-1.el8 | |
cockpit-bridge | 250-1.el8 | |
cockpit-doc | 250-1.el8 | |
cockpit-system | 250-1.el8 | |
cockpit-ws | 250-1.el8 | |
device-mapper | 1.02.177-5.el8 | |
device-mapper-event | 1.02.177-5.el8 | |
device-mapper-event-libs | 1.02.177-5.el8 | |
device-mapper-libs | 1.02.177-5.el8 | |
dnf | 4.7.0-2.el8 | |
dnf-automatic | 4.7.0-2.el8 | |
dnf-data | 4.7.0-2.el8 | |
dnf-plugin-subscription-manager | 1.28.20-1.el8 | |
dnf-plugins-core | 4.0.21-2.el8 | |
dracut | 049-188.git20210802.el8 | |
dracut-caps | 049-188.git20210802.el8 | |
dracut-config-generic | 049-188.git20210802.el8 | |
dracut-config-rescue | 049-188.git20210802.el8 | |
dracut-live | 049-188.git20210802.el8 | |
dracut-network | 049-188.git20210802.el8 | |
dracut-squash | 049-188.git20210802.el8 | |
dracut-tools | 049-188.git20210802.el8 | |
emacs-filesystem | 26.1-7.el8 | |
firewalld | 0.9.3-7.el8 | |
firewalld-filesystem | 0.9.3-7.el8 | |
iproute | 5.12.0-2.el8 | |
iproute-tc | 5.12.0-2.el8 | |
iscsi-initiator-utils | 6.2.1.4-4.git095f59c.el8 | |
iscsi-initiator-utils-iscsiuio | 6.2.1.4-4.git095f59c.el8 | |
iwl100-firmware | 39.31.5.1-103.el8.1 | |
iwl1000-firmware | 39.31.5.1-103.el8.1 | |
iwl105-firmware | 18.168.6.1-103.el8.1 | |
iwl135-firmware | 18.168.6.1-103.el8.1 | |
iwl2000-firmware | 18.168.6.1-103.el8.1 | |
iwl2030-firmware | 18.168.6.1-103.el8.1 | |
iwl3160-firmware | 25.30.13.0-103.el8.1 | |
iwl3945-firmware | 15.32.2.9-103.el8.1 | |
iwl4965-firmware | 228.61.2.24-103.el8.1 | |
iwl5000-firmware | 8.83.5.1_1-103.el8.1 | |
iwl5150-firmware | 8.24.2.2-103.el8.1 | |
iwl6000-firmware | 9.221.4.1-103.el8.1 | |
iwl6000g2a-firmware | 18.168.6.1-103.el8.1 | |
iwl6000g2b-firmware | 18.168.6.1-103.el8.1 | |
iwl6050-firmware | 41.28.5.1-103.el8.1 | |
iwl7260-firmware | 25.30.13.0-103.el8.1 | |
kexec-tools | 2.0.20-56.el8 | |
libdnf | 0.63.0-2.el8 | |
libertas-sd8686-firmware | 20210702-103.gitd79c2677.el8 | |
libertas-sd8787-firmware | 20210702-103.gitd79c2677.el8 | |
libertas-usb8388-firmware | 20210702-103.gitd79c2677.el8 | |
libertas-usb8388-olpc-firmware | 20210702-103.gitd79c2677.el8 | |
libipa_hbac | 2.5.2-2.el8 | |
libqmi | 1.24.0-3.el8 | |
libqmi-utils | 1.24.0-3.el8 | |
libsolv | 0.7.19-1.el8 | |
libsss_autofs | 2.5.2-2.el8 | |
libsss_certmap | 2.5.2-2.el8 | |
libsss_idmap | 2.5.2-2.el8 | |
libsss_nss_idmap | 2.5.2-2.el8 | |
libsss_simpleifp | 2.5.2-2.el8 | |
libsss_sudo | 2.5.2-2.el8 | |
linux-firmware | 20210702-103.gitd79c2677.el8 | |
lvm2 | 2.03.12-5.el8 | |
lvm2-dbusd | 2.03.12-5.el8 | |
lvm2-libs | 2.03.12-5.el8 | |
lvm2-lockd | 2.03.12-5.el8 | |
lz4 | 1.8.3-3.el8_4 | [RHSA-2021:2575](https://access.redhat.com/errata/RHSA-2021:2575) | <div class="adv_s">Security Advisory</div>
lz4-devel | 1.8.3-3.el8_4 | [RHSA-2021:2575](https://access.redhat.com/errata/RHSA-2021:2575) | <div class="adv_s">Security Advisory</div>
lz4-libs | 1.8.3-3.el8_4 | [RHSA-2021:2575](https://access.redhat.com/errata/RHSA-2021:2575) | <div class="adv_s">Security Advisory</div>
mobile-broadband-provider-info | 20210805-1.el8 | |
NetworkManager | 1.32.6-1.el8 | |
NetworkManager-adsl | 1.32.6-1.el8 | |
NetworkManager-bluetooth | 1.32.6-1.el8 | |
NetworkManager-config-connectivity-redhat | 1.32.6-1.el8 | |
NetworkManager-config-server | 1.32.6-1.el8 | |
NetworkManager-dispatcher-routing-rules | 1.32.6-1.el8 | |
NetworkManager-libnm | 1.32.6-1.el8 | |
NetworkManager-ovs | 1.32.6-1.el8 | |
NetworkManager-ppp | 1.32.6-1.el8 | |
NetworkManager-team | 1.32.6-1.el8 | |
NetworkManager-tui | 1.32.6-1.el8 | |
NetworkManager-wifi | 1.32.6-1.el8 | |
NetworkManager-wwan | 1.32.6-1.el8 | |
OpenIPMI | 2.0.31-2.el8 | |
OpenIPMI-lanserv | 2.0.31-2.el8 | |
OpenIPMI-libs | 2.0.31-2.el8 | |
OpenIPMI-perl | 2.0.31-2.el8 | |
openldap | 2.4.46-18.el8 | |
openldap-clients | 2.4.46-18.el8 | |
openldap-devel | 2.4.46-18.el8 | |
postfix | 3.5.8-2.el8 | |
python3-cloud-what | 1.28.20-1.el8 | |
python3-dnf | 4.7.0-2.el8 | |
python3-dnf-plugin-post-transaction-actions | 4.0.21-2.el8 | |
python3-dnf-plugin-versionlock | 4.0.21-2.el8 | |
python3-dnf-plugins-core | 4.0.21-2.el8 | |
python3-firewall | 0.9.3-7.el8 | |
python3-hawkey | 0.63.0-2.el8 | |
python3-iscsi-initiator-utils | 6.2.1.4-4.git095f59c.el8 | |
python3-libdnf | 0.63.0-2.el8 | |
python3-libipa_hbac | 2.5.2-2.el8 | |
python3-libsss_nss_idmap | 2.5.2-2.el8 | |
python3-openipmi | 2.0.31-2.el8 | |
python3-solv | 0.7.19-1.el8 | |
python3-sss | 2.5.2-2.el8 | |
python3-sss-murmur | 2.5.2-2.el8 | |
python3-sssdconfig | 2.5.2-2.el8 | |
python3-subscription-manager-rhsm | 1.28.20-1.el8 | |
python3-syspurpose | 1.28.20-1.el8 | |
rhsm-icons | 1.28.20-1.el8 | |
rng-tools | 6.13-1.git.d207e0b6.el8 | |
sssd | 2.5.2-2.el8 | |
sssd-ad | 2.5.2-2.el8 | |
sssd-client | 2.5.2-2.el8 | |
sssd-common | 2.5.2-2.el8 | |
sssd-common-pac | 2.5.2-2.el8 | |
sssd-dbus | 2.5.2-2.el8 | |
sssd-ipa | 2.5.2-2.el8 | |
sssd-kcm | 2.5.2-2.el8 | |
sssd-krb5 | 2.5.2-2.el8 | |
sssd-krb5-common | 2.5.2-2.el8 | |
sssd-ldap | 2.5.2-2.el8 | |
sssd-nfs-idmap | 2.5.2-2.el8 | |
sssd-polkit-rules | 2.5.2-2.el8 | |
sssd-proxy | 2.5.2-2.el8 | |
sssd-tools | 2.5.2-2.el8 | |
sssd-winbind-idmap | 2.5.2-2.el8 | |
subscription-manager | 1.28.20-1.el8 | |
subscription-manager-cockpit | 1.28.20-1.el8 | |
subscription-manager-plugin-ostree | 1.28.20-1.el8 | |
subscription-manager-rhsm-certificates | 1.28.20-1.el8 | |
tpm2-tools | 4.1.1-5.el8 | |
yum | 4.7.0-2.el8 | |
yum-utils | 4.0.21-2.el8 | |

### AppStream aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
anaconda | 33.16.5.4-1.el8 | |
anaconda-core | 33.16.5.4-1.el8 | |
anaconda-dracut | 33.16.5.4-1.el8 | |
anaconda-gui | 33.16.5.4-1.el8 | |
anaconda-install-env-deps | 33.16.5.4-1.el8 | |
anaconda-tui | 33.16.5.4-1.el8 | |
anaconda-widgets | 33.16.5.4-1.el8 | |
ansible-collection-microsoft-sql | 1.1.0-1.el8 | |
apr | 1.6.3-12.el8 | |
apr-devel | 1.6.3-12.el8 | |
blivet-data | 3.4.0-4.el8 | |
buildah | 1.22.0-2.module_el8.5.0+877+1c30e0c9 | |
buildah-tests | 1.22.0-2.module_el8.5.0+877+1c30e0c9 | |
cockpit-machines | 250-1.el8 | |
cockpit-packagekit | 250-1.el8 | |
cockpit-pcp | 250-1.el8 | |
cockpit-podman | 33-1.module_el8.5.0+877+1c30e0c9 | |
cockpit-storaged | 250-1.el8 | |
compat-exiv2-026 | 0.26-5.el8 | |
conmon | 2.0.29-1.module_el8.5.0+877+1c30e0c9 | |
container-selinux | 2.164.2-1.module_el8.5.0+877+1c30e0c9 | |
containernetworking-plugins | 0.9.1-1.module_el8.5.0+877+1c30e0c9 | |
containers-common | 1.4.0-5.module_el8.5.0+878+851f435b | |
coreos-installer-bootinfra | 0.9.1-17.el8 | |
corosynclib | 3.1.5-1.el8 | |
createrepo_c | 0.17.2-2.el8 | |
createrepo_c-devel | 0.17.2-2.el8 | |
createrepo_c-libs | 0.17.2-2.el8 | |
crit | 3.15-2.module_el8.5.0+877+1c30e0c9 | |
criu | 3.15-2.module_el8.5.0+877+1c30e0c9 | |
criu-devel | 3.15-2.module_el8.5.0+877+1c30e0c9 | |
criu-libs | 3.15-2.module_el8.5.0+877+1c30e0c9 | |
crun | 0.21-3.module_el8.5.0+878+851f435b | |
dnsmasq | 2.79-19.el8 | |
dnsmasq-utils | 2.79-19.el8 | |
dyninst | 11.0.0-3.el8 | |
edk2-aarch64 | 20210527gite1999b264f1f-3.el8 | |
emacs | 26.1-7.el8 | |
emacs-common | 26.1-7.el8 | |
emacs-lucid | 26.1-7.el8 | |
emacs-nox | 26.1-7.el8 | |
emacs-terminal | 26.1-7.el8 | |
exiv2 | 0.27.4-2.el8 | |
exiv2-libs | 0.27.4-2.el8 | |
firefox | 78.13.0-2.el8_4 | |
firewall-applet | 0.9.3-7.el8 | |
firewall-config | 0.9.3-7.el8 | |
fuse-overlayfs | 1.7-1.module_el8.5.0+877+1c30e0c9 | |
gcc-toolset-10-elfutils | 0.182-5.el8_4 | [RHBA-2021:3082](https://access.redhat.com/errata/RHBA-2021:3082) | <div class="adv_b">Bug Fix Advisory</div>
gcc-toolset-10-elfutils-debuginfod-client | 0.182-5.el8_4 | [RHSA-2020:1665](https://access.redhat.com/errata/RHSA-2020:1665) | <div class="adv_s">Security Advisory</div>
gcc-toolset-10-elfutils-debuginfod-client-devel | 0.182-5.el8_4 | [RHSA-2020:1665](https://access.redhat.com/errata/RHSA-2020:1665) | <div class="adv_s">Security Advisory</div>
gcc-toolset-10-elfutils-devel | 0.182-5.el8_4 | [RHBA-2021:3082](https://access.redhat.com/errata/RHBA-2021:3082) | <div class="adv_b">Bug Fix Advisory</div>
gcc-toolset-10-elfutils-libelf | 0.182-5.el8_4 | [RHBA-2021:3082](https://access.redhat.com/errata/RHBA-2021:3082) | <div class="adv_b">Bug Fix Advisory</div>
gcc-toolset-10-elfutils-libelf-devel | 0.182-5.el8_4 | [RHBA-2021:3082](https://access.redhat.com/errata/RHBA-2021:3082) | <div class="adv_b">Bug Fix Advisory</div>
gcc-toolset-10-elfutils-libs | 0.182-5.el8_4 | [RHBA-2021:3082](https://access.redhat.com/errata/RHBA-2021:3082) | <div class="adv_b">Bug Fix Advisory</div>
gcc-toolset-10-valgrind | 3.16.0-6.el8_4 | [RHBA-2021:3083](https://access.redhat.com/errata/RHBA-2021:3083) | <div class="adv_b">Bug Fix Advisory</div>
gcc-toolset-10-valgrind-devel | 3.16.0-6.el8_4 | [RHBA-2021:3083](https://access.redhat.com/errata/RHBA-2021:3083) | <div class="adv_b">Bug Fix Advisory</div>
gdb | 8.2-16.el8 | |
gdb-doc | 8.2-16.el8 | |
gdb-gdbserver | 8.2-16.el8 | |
gdb-headless | 8.2-16.el8 | |
gnome-session | 3.28.1-13.el8 | |
gnome-session-kiosk-session | 3.28.1-13.el8 | |
gnome-session-wayland-session | 3.28.1-13.el8 | |
gnome-session-xsession | 3.28.1-13.el8 | |
gnome-shell | 3.32.2-38.el8 | |
java-17-openjdk | 17.0.0.0.26-0.2.ea.el8 | |
java-17-openjdk-headless | 17.0.0.0.26-0.2.ea.el8 | |
jss | 4.9.0-1.module_el8.5.0+876+d4bb8aa6 | |
jss-javadoc | 4.9.0-1.module_el8.5.0+876+d4bb8aa6 | |
ldapjdk | 4.23.0-1.module_el8.5.0+876+d4bb8aa6 | |
ldapjdk-javadoc | 4.23.0-1.module_el8.5.0+876+d4bb8aa6 | |
libserf | 1.3.9-9.module_el8.3.0+703+ba2f61b7 | |
libslirp | 4.4.0-1.module_el8.5.0+877+1c30e0c9 | |
libslirp-devel | 4.4.0-1.module_el8.5.0+877+1c30e0c9 | |
libuv | 1.41.1-1.el8_4 | [RHSA-2021:3075](https://access.redhat.com/errata/RHSA-2021:3075) | <div class="adv_s">Security Advisory</div>
linuxptp | 3.1.1-1.el8 | |
mod_dav_svn | 1.10.2-4.module_el8.3.0+703+ba2f61b7 | |
NetworkManager-cloud-setup | 1.32.6-1.el8 | |
oci-seccomp-bpf-hook | 1.2.3-3.module_el8.5.0+877+1c30e0c9 | |
openscap | 1.3.5-6.el8 | |
openscap-devel | 1.3.5-6.el8 | |
openscap-engine-sce | 1.3.5-6.el8 | |
openscap-python3 | 1.3.5-6.el8 | |
openscap-scanner | 1.3.5-6.el8 | |
openscap-utils | 1.3.5-6.el8 | |
oscap-anaconda-addon | 1.2.1-1.el8 | |
pacemaker-cluster-libs | 2.1.0-5.el8 | |
pacemaker-libs | 2.1.0-5.el8 | |
pacemaker-schemas | 2.1.0-5.el8 | |
pki-acme | 10.11.0-1.module_el8.5.0+876+d4bb8aa6 | |
pki-base | 10.11.0-1.module_el8.5.0+876+d4bb8aa6 | |
pki-base-java | 10.11.0-1.module_el8.5.0+876+d4bb8aa6 | |
pki-ca | 10.11.0-1.module_el8.5.0+876+d4bb8aa6 | |
pki-kra | 10.11.0-1.module_el8.5.0+876+d4bb8aa6 | |
pki-server | 10.11.0-1.module_el8.5.0+876+d4bb8aa6 | |
pki-symkey | 10.11.0-1.module_el8.5.0+876+d4bb8aa6 | |
pki-tools | 10.11.0-1.module_el8.5.0+876+d4bb8aa6 | |
podman | 3.3.0-2.module_el8.5.0+877+1c30e0c9 | |
podman-catatonit | 3.3.0-2.module_el8.5.0+877+1c30e0c9 | |
podman-docker | 3.3.0-2.module_el8.5.0+877+1c30e0c9 | |
podman-plugins | 3.3.0-2.module_el8.5.0+877+1c30e0c9 | |
podman-remote | 3.3.0-2.module_el8.5.0+877+1c30e0c9 | |
podman-tests | 3.3.0-2.module_el8.5.0+877+1c30e0c9 | |
poppler | 20.11.0-3.el8 | |
poppler-glib | 20.11.0-3.el8 | |
poppler-utils | 20.11.0-3.el8 | |
postfix-cdb | 3.5.8-2.el8 | |
postfix-ldap | 3.5.8-2.el8 | |
postfix-mysql | 3.5.8-2.el8 | |
postfix-pcre | 3.5.8-2.el8 | |
postfix-perl-scripts | 3.5.8-2.el8 | |
postfix-pgsql | 3.5.8-2.el8 | |
postfix-sqlite | 3.5.8-2.el8 | |
python3-blivet | 3.4.0-4.el8 | |
python3-createrepo_c | 0.17.2-2.el8 | |
python3-criu | 3.15-2.module_el8.5.0+877+1c30e0c9 | |
python3-pillow | 5.1.1-16.el8 | |
python3-pki | 10.11.0-1.module_el8.5.0+876+d4bb8aa6 | |
python3-podman | 3.2.0-2.module_el8.5.0+877+1c30e0c9 | |
rhel-system-roles | 1.6.6-1.el8 | |
rsyslog | 8.2102.0-5.el8 | |
rsyslog-crypto | 8.2102.0-5.el8 | |
rsyslog-doc | 8.2102.0-5.el8 | |
rsyslog-elasticsearch | 8.2102.0-5.el8 | |
rsyslog-gnutls | 8.2102.0-5.el8 | |
rsyslog-gssapi | 8.2102.0-5.el8 | |
rsyslog-kafka | 8.2102.0-5.el8 | |
rsyslog-mmaudit | 8.2102.0-5.el8 | |
rsyslog-mmjsonparse | 8.2102.0-5.el8 | |
rsyslog-mmkubernetes | 8.2102.0-5.el8 | |
rsyslog-mmnormalize | 8.2102.0-5.el8 | |
rsyslog-mmsnmptrapd | 8.2102.0-5.el8 | |
rsyslog-mysql | 8.2102.0-5.el8 | |
rsyslog-omamqp1 | 8.2102.0-5.el8 | |
rsyslog-pgsql | 8.2102.0-5.el8 | |
rsyslog-relp | 8.2102.0-5.el8 | |
rsyslog-snmp | 8.2102.0-5.el8 | |
rsyslog-udpspoof | 8.2102.0-5.el8 | |
runc | 1.0.1-5.module_el8.5.0+878+851f435b | |
sblim-gather | 2.2.9-23.el8 | |
scap-security-guide | 0.1.57-1.el8 | |
scap-security-guide-doc | 0.1.57-1.el8 | |
skopeo | 1.4.0-5.module_el8.5.0+878+851f435b | |
skopeo-tests | 1.4.0-5.module_el8.5.0+878+851f435b | |
slirp4netns | 1.1.8-1.module_el8.5.0+877+1c30e0c9 | |
spamassassin | 3.4.4-4.el8 | |
subscription-manager-migration | 1.28.20-1.el8 | |
subversion | 1.10.2-4.module_el8.3.0+703+ba2f61b7 | |
subversion-devel | 1.10.2-4.module_el8.3.0+703+ba2f61b7 | |
subversion-gnome | 1.10.2-4.module_el8.3.0+703+ba2f61b7 | |
subversion-javahl | 1.10.2-4.module_el8.3.0+703+ba2f61b7 | |
subversion-libs | 1.10.2-4.module_el8.3.0+703+ba2f61b7 | |
subversion-perl | 1.10.2-4.module_el8.3.0+703+ba2f61b7 | |
subversion-tools | 1.10.2-4.module_el8.3.0+703+ba2f61b7 | |
systemtap | 4.5-2.el8 | |
systemtap-client | 4.5-2.el8 | |
systemtap-devel | 4.5-2.el8 | |
systemtap-exporter | 4.5-2.el8 | |
systemtap-initscript | 4.5-2.el8 | |
systemtap-runtime | 4.5-2.el8 | |
systemtap-runtime-java | 4.5-2.el8 | |
systemtap-runtime-python3 | 4.5-2.el8 | |
systemtap-runtime-virtguest | 4.5-2.el8 | |
systemtap-sdt-devel | 4.5-2.el8 | |
systemtap-server | 4.5-2.el8 | |
thunderbird | 78.13.0-1.el8_4 | |
tomcatjss | 7.7.0-1.module_el8.5.0+876+d4bb8aa6 | |
toolbox | 0.0.99.3-1.module_el8.5.0+877+1c30e0c9 | |
toolbox-tests | 0.0.99.3-1.module_el8.5.0+877+1c30e0c9 | |
udica | 0.2.4-2.module_el8.5.0+877+1c30e0c9 | |
utf8proc | 2.1.1-5.module_el8.3.0+703+ba2f61b7 | |
virt-who | 1.30.7-1.el8 | |
WALinuxAgent | 2.3.0.2-2.el8 | |
WALinuxAgent-udev | 2.3.0.2-2.el8 | |
xorg-x11-server-Xwayland | 21.1.1-6.el8 | |
zziplib | 0.13.68-9.el8 | |
zziplib-utils | 0.13.68-9.el8 | |

### PowerTools aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
corosync-vqsim | 3.1.5-1.el8 | |
device-mapper-devel | 1.02.177-5.el8 | |
device-mapper-event-devel | 1.02.177-5.el8 | |
dwarves | 1.21-0.el8 | |
dyninst-devel | 11.0.0-3.el8 | |
dyninst-doc | 11.0.0-3.el8 | |
dyninst-static | 11.0.0-3.el8 | |
dyninst-testsuite | 11.0.0-3.el8 | |
exiv2-devel | 0.27.4-2.el8 | |
exiv2-doc | 0.27.4-2.el8 | |
iproute-devel | 5.12.0-2.el8 | |
iscsi-initiator-utils-devel | 6.2.1.4-4.git095f59c.el8 | |
libdnf-devel | 0.63.0-2.el8 | |
libdwarves1 | 1.21-0.el8 | |
libsolv-devel | 0.7.19-1.el8 | |
libsolv-tools | 0.7.19-1.el8 | |
libsss_nss_idmap-devel | 2.5.2-2.el8 | |
libuv-devel | 1.41.1-1.el8_4 | [RHSA-2021:3075](https://access.redhat.com/errata/RHSA-2021:3075) | <div class="adv_s">Security Advisory</div>
lvm2-devel | 2.03.12-5.el8 | |
mobile-broadband-provider-info-devel | 20210805-1.el8 | |
NetworkManager-libnm-devel | 1.32.6-1.el8 | |
OpenIPMI-devel | 2.0.31-2.el8 | |
openscap-engine-sce-devel | 1.3.5-6.el8 | |
poppler-cpp | 20.11.0-3.el8 | |
poppler-cpp-devel | 20.11.0-3.el8 | |
poppler-devel | 20.11.0-3.el8 | |
poppler-glib-devel | 20.11.0-3.el8 | |
poppler-qt5 | 20.11.0-3.el8 | |
poppler-qt5-devel | 20.11.0-3.el8 | |
sblim-gather-provider | 2.2.9-23.el8 | |
zziplib-devel | 0.13.68-9.el8 | |

### HighAvailability aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
corosync | 3.1.5-1.el8 | |
corosynclib-devel | 3.1.5-1.el8 | |
pacemaker | 2.1.0-5.el8 | |
pacemaker-cli | 2.1.0-5.el8 | |
pacemaker-cts | 2.1.0-5.el8 | |
pacemaker-doc | 2.1.0-5.el8 | |
pacemaker-libs-devel | 2.1.0-5.el8 | |
pacemaker-nagios-plugins-metadata | 2.1.0-5.el8 | |
pacemaker-remote | 2.1.0-5.el8 | |
spausedd | 3.1.5-1.el8 | |

