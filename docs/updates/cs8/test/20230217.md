## 2023-02-17

### cloud x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
openstack-swift-account | 2.28.1-1.el8 | |
openstack-swift-container | 2.28.1-1.el8 | |
openstack-swift-doc | 2.28.1-1.el8 | |
openstack-swift-object | 2.28.1-1.el8 | |
openstack-swift-proxy | 2.28.1-1.el8 | |
python3-swift | 2.28.1-1.el8 | |
python3-swift-tests | 2.28.1-1.el8 | |

### cloud aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
openstack-swift-account | 2.28.1-1.el8 | |
openstack-swift-container | 2.28.1-1.el8 | |
openstack-swift-doc | 2.28.1-1.el8 | |
openstack-swift-object | 2.28.1-1.el8 | |
openstack-swift-proxy | 2.28.1-1.el8 | |
python3-swift | 2.28.1-1.el8 | |
python3-swift-tests | 2.28.1-1.el8 | |

