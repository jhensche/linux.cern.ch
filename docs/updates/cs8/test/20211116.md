## 2021-11-16

### cloud x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
ansible-collection-containers-podman | 1.8.2-1.el8 | |

### cloud aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
ansible-collection-containers-podman | 1.8.2-1.el8 | |

