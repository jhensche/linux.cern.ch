## 2021-11-05

### BaseOS x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
bpftool | 4.18.0-305.25.1.el8_4 | [RHSA-2021:4056](https://access.redhat.com/errata/RHSA-2021:4056) | <div class="adv_s">Security Advisory</div>
kernel | 4.18.0-305.25.1.el8_4 | [RHSA-2021:4056](https://access.redhat.com/errata/RHSA-2021:4056) | <div class="adv_s">Security Advisory</div>
kernel-abi-stablelists | 4.18.0-305.25.1.el8_4 | [RHSA-2021:4056](https://access.redhat.com/errata/RHSA-2021:4056) | <div class="adv_s">Security Advisory</div>
kernel-core | 4.18.0-305.25.1.el8_4 | [RHSA-2021:4056](https://access.redhat.com/errata/RHSA-2021:4056) | <div class="adv_s">Security Advisory</div>
kernel-cross-headers | 4.18.0-305.25.1.el8_4 | [RHSA-2021:4056](https://access.redhat.com/errata/RHSA-2021:4056) | <div class="adv_s">Security Advisory</div>
kernel-debug | 4.18.0-305.25.1.el8_4 | [RHSA-2021:4056](https://access.redhat.com/errata/RHSA-2021:4056) | <div class="adv_s">Security Advisory</div>
kernel-debug-core | 4.18.0-305.25.1.el8_4 | [RHSA-2021:4056](https://access.redhat.com/errata/RHSA-2021:4056) | <div class="adv_s">Security Advisory</div>
kernel-debug-devel | 4.18.0-305.25.1.el8_4 | [RHSA-2021:4056](https://access.redhat.com/errata/RHSA-2021:4056) | <div class="adv_s">Security Advisory</div>
kernel-debug-modules | 4.18.0-305.25.1.el8_4 | [RHSA-2021:4056](https://access.redhat.com/errata/RHSA-2021:4056) | <div class="adv_s">Security Advisory</div>
kernel-debug-modules-extra | 4.18.0-305.25.1.el8_4 | [RHSA-2021:4056](https://access.redhat.com/errata/RHSA-2021:4056) | <div class="adv_s">Security Advisory</div>
kernel-devel | 4.18.0-305.25.1.el8_4 | [RHSA-2021:4056](https://access.redhat.com/errata/RHSA-2021:4056) | <div class="adv_s">Security Advisory</div>
kernel-doc | 4.18.0-305.25.1.el8_4 | [RHSA-2021:4056](https://access.redhat.com/errata/RHSA-2021:4056) | <div class="adv_s">Security Advisory</div>
kernel-headers | 4.18.0-305.25.1.el8_4 | [RHSA-2021:4056](https://access.redhat.com/errata/RHSA-2021:4056) | <div class="adv_s">Security Advisory</div>
kernel-modules | 4.18.0-305.25.1.el8_4 | [RHSA-2021:4056](https://access.redhat.com/errata/RHSA-2021:4056) | <div class="adv_s">Security Advisory</div>
kernel-modules-extra | 4.18.0-305.25.1.el8_4 | [RHSA-2021:4056](https://access.redhat.com/errata/RHSA-2021:4056) | <div class="adv_s">Security Advisory</div>
kernel-rt-core | 4.18.0-348.rt7.130.el8 | |
kernel-tools | 4.18.0-305.25.1.el8_4 | [RHSA-2021:4056](https://access.redhat.com/errata/RHSA-2021:4056) | <div class="adv_s">Security Advisory</div>
kernel-tools-libs | 4.18.0-305.25.1.el8_4 | [RHSA-2021:4056](https://access.redhat.com/errata/RHSA-2021:4056) | <div class="adv_s">Security Advisory</div>
perf | 4.18.0-305.25.1.el8_4 | [RHSA-2021:4056](https://access.redhat.com/errata/RHSA-2021:4056) | <div class="adv_s">Security Advisory</div>
python3-perf | 4.18.0-305.25.1.el8_4 | [RHSA-2021:4056](https://access.redhat.com/errata/RHSA-2021:4056) | <div class="adv_s">Security Advisory</div>

### AppStream x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
annobin | 10.21-3.el8 | |
annobin-annocheck | 10.21-3.el8 | |
delve | 1.7.2-1.module_el8.6.0+962+0036b8f3 | |
gcc-toolset-11-annobin-annocheck | 9.85-1.el8 | |
gcc-toolset-11-annobin-docs | 9.85-1.el8 | |
gcc-toolset-11-annobin-plugin-gcc | 9.85-1.el8 | |
gcc-toolset-11-dyninst | 11.0.0-2.el8 | |
gcc-toolset-11-dyninst-devel | 11.0.0-2.el8 | |
gcc-toolset-11-elfutils | 0.185-3.el8 | |
gcc-toolset-11-elfutils-debuginfod-client | 0.185-3.el8 | [RHSA-2020:1665](https://access.redhat.com/errata/RHSA-2020:1665) | <div class="adv_s">Security Advisory</div>
gcc-toolset-11-elfutils-debuginfod-client-devel | 0.185-3.el8 | [RHSA-2020:1665](https://access.redhat.com/errata/RHSA-2020:1665) | <div class="adv_s">Security Advisory</div>
gcc-toolset-11-elfutils-devel | 0.185-3.el8 | |
gcc-toolset-11-elfutils-libelf | 0.185-3.el8 | |
gcc-toolset-11-elfutils-libelf-devel | 0.185-3.el8 | |
gcc-toolset-11-elfutils-libs | 0.185-3.el8 | |
gcc-toolset-11-gcc | 11.2.1-1.1.el8 | |
gcc-toolset-11-gcc-c++ | 11.2.1-1.1.el8 | |
gcc-toolset-11-gcc-gdb-plugin | 11.2.1-1.1.el8 | |
gcc-toolset-11-gcc-gfortran | 11.2.1-1.1.el8 | |
gcc-toolset-11-gcc-plugin-devel | 11.2.1-1.1.el8 | |
gcc-toolset-11-libasan-devel | 11.2.1-1.1.el8 | |
gcc-toolset-11-libatomic-devel | 11.2.1-1.1.el8 | |
gcc-toolset-11-libgccjit | 11.2.1-1.1.el8 | |
gcc-toolset-11-libgccjit-devel | 11.2.1-1.1.el8 | |
gcc-toolset-11-libgccjit-docs | 11.2.1-1.1.el8 | |
gcc-toolset-11-libitm-devel | 11.2.1-1.1.el8 | |
gcc-toolset-11-liblsan-devel | 11.2.1-1.1.el8 | |
gcc-toolset-11-libquadmath-devel | 11.2.1-1.1.el8 | |
gcc-toolset-11-libstdc++-devel | 11.2.1-1.1.el8 | |
gcc-toolset-11-libstdc++-docs | 11.2.1-1.1.el8 | |
gcc-toolset-11-libtsan-devel | 11.2.1-1.1.el8 | |
gcc-toolset-11-libubsan-devel | 11.2.1-1.1.el8 | |
gcc-toolset-11-strace | 5.13-4.el8 | |
gcc-toolset-11-systemtap | 4.5-4.el8 | |
gcc-toolset-11-systemtap-client | 4.5-4.el8 | |
gcc-toolset-11-systemtap-devel | 4.5-4.el8 | |
gcc-toolset-11-systemtap-initscript | 4.5-4.el8 | |
gcc-toolset-11-systemtap-runtime | 4.5-4.el8 | |
gcc-toolset-11-systemtap-sdt-devel | 4.5-4.el8 | |
gcc-toolset-11-systemtap-server | 4.5-4.el8 | |
gcc-toolset-11-valgrind | 3.17.0-6.el8 | |
gcc-toolset-11-valgrind-devel | 3.17.0-6.el8 | |
go-toolset | 1.17.2-1.module_el8.6.0+962+0036b8f3 | |
golang | 1.17.2-2.module_el8.6.0+963+7827afaa | |
golang-bin | 1.17.2-2.module_el8.6.0+963+7827afaa | |
golang-docs | 1.17.2-2.module_el8.6.0+963+7827afaa | |
golang-misc | 1.17.2-2.module_el8.6.0+963+7827afaa | |
golang-race | 1.17.2-2.module_el8.6.0+963+7827afaa | |
golang-src | 1.17.2-2.module_el8.6.0+963+7827afaa | |
golang-tests | 1.17.2-2.module_el8.6.0+963+7827afaa | |
ibus | 1.5.19-14.el8 | |
ibus-gtk2 | 1.5.19-14.el8 | |
ibus-gtk3 | 1.5.19-14.el8 | |
ibus-libs | 1.5.19-14.el8 | |
ibus-setup | 1.5.19-14.el8 | |
ibus-wayland | 1.5.19-14.el8 | |
libasan6 | 11.2.1-1.1.el8 | |
openblas | 0.3.15-1.el8 | |
openblas-threads | 0.3.15-1.el8 | |
pcp | 5.3.4-2.el8 | |
pcp-conf | 5.3.4-2.el8 | |
pcp-devel | 5.3.4-2.el8 | |
pcp-doc | 5.3.4-2.el8 | |
pcp-export-pcp2elasticsearch | 5.3.4-2.el8 | |
pcp-export-pcp2graphite | 5.3.4-2.el8 | |
pcp-export-pcp2influxdb | 5.3.4-2.el8 | |
pcp-export-pcp2json | 5.3.4-2.el8 | |
pcp-export-pcp2spark | 5.3.4-2.el8 | |
pcp-export-pcp2xml | 5.3.4-2.el8 | |
pcp-export-pcp2zabbix | 5.3.4-2.el8 | |
pcp-export-zabbix-agent | 5.3.4-2.el8 | |
pcp-gui | 5.3.4-2.el8 | |
pcp-import-collectl2pcp | 5.3.4-2.el8 | |
pcp-import-ganglia2pcp | 5.3.4-2.el8 | |
pcp-import-iostat2pcp | 5.3.4-2.el8 | |
pcp-import-mrtg2pcp | 5.3.4-2.el8 | |
pcp-import-sar2pcp | 5.3.4-2.el8 | |
pcp-libs | 5.3.4-2.el8 | |
pcp-libs-devel | 5.3.4-2.el8 | |
pcp-pmda-activemq | 5.3.4-2.el8 | |
pcp-pmda-apache | 5.3.4-2.el8 | |
pcp-pmda-bash | 5.3.4-2.el8 | |
pcp-pmda-bcc | 5.3.4-2.el8 | |
pcp-pmda-bind2 | 5.3.4-2.el8 | |
pcp-pmda-bonding | 5.3.4-2.el8 | |
pcp-pmda-bpftrace | 5.3.4-2.el8 | |
pcp-pmda-cifs | 5.3.4-2.el8 | |
pcp-pmda-cisco | 5.3.4-2.el8 | |
pcp-pmda-dbping | 5.3.4-2.el8 | |
pcp-pmda-denki | 5.3.4-2.el8 | |
pcp-pmda-dm | 5.3.4-2.el8 | |
pcp-pmda-docker | 5.3.4-2.el8 | |
pcp-pmda-ds389 | 5.3.4-2.el8 | |
pcp-pmda-ds389log | 5.3.4-2.el8 | |
pcp-pmda-elasticsearch | 5.3.4-2.el8 | |
pcp-pmda-gfs2 | 5.3.4-2.el8 | |
pcp-pmda-gluster | 5.3.4-2.el8 | |
pcp-pmda-gpfs | 5.3.4-2.el8 | |
pcp-pmda-gpsd | 5.3.4-2.el8 | |
pcp-pmda-hacluster | 5.3.4-2.el8 | |
pcp-pmda-haproxy | 5.3.4-2.el8 | |
pcp-pmda-infiniband | 5.3.4-2.el8 | |
pcp-pmda-json | 5.3.4-2.el8 | |
pcp-pmda-libvirt | 5.3.4-2.el8 | |
pcp-pmda-lio | 5.3.4-2.el8 | |
pcp-pmda-lmsensors | 5.3.4-2.el8 | |
pcp-pmda-logger | 5.3.4-2.el8 | |
pcp-pmda-lustre | 5.3.4-2.el8 | |
pcp-pmda-lustrecomm | 5.3.4-2.el8 | |
pcp-pmda-mailq | 5.3.4-2.el8 | |
pcp-pmda-memcache | 5.3.4-2.el8 | |
pcp-pmda-mic | 5.3.4-2.el8 | |
pcp-pmda-mounts | 5.3.4-2.el8 | |
pcp-pmda-mssql | 5.3.4-2.el8 | |
pcp-pmda-mysql | 5.3.4-2.el8 | |
pcp-pmda-named | 5.3.4-2.el8 | |
pcp-pmda-netcheck | 5.3.4-2.el8 | |
pcp-pmda-netfilter | 5.3.4-2.el8 | |
pcp-pmda-news | 5.3.4-2.el8 | |
pcp-pmda-nfsclient | 5.3.4-2.el8 | |
pcp-pmda-nginx | 5.3.4-2.el8 | |
pcp-pmda-nvidia-gpu | 5.3.4-2.el8 | |
pcp-pmda-openmetrics | 5.3.4-2.el8 | |
pcp-pmda-openvswitch | 5.3.4-2.el8 | |
pcp-pmda-oracle | 5.3.4-2.el8 | |
pcp-pmda-pdns | 5.3.4-2.el8 | |
pcp-pmda-perfevent | 5.3.4-2.el8 | |
pcp-pmda-podman | 5.3.4-2.el8 | |
pcp-pmda-postfix | 5.3.4-2.el8 | |
pcp-pmda-postgresql | 5.3.4-2.el8 | |
pcp-pmda-rabbitmq | 5.3.4-2.el8 | |
pcp-pmda-redis | 5.3.4-2.el8 | |
pcp-pmda-roomtemp | 5.3.4-2.el8 | |
pcp-pmda-rsyslog | 5.3.4-2.el8 | |
pcp-pmda-samba | 5.3.4-2.el8 | |
pcp-pmda-sendmail | 5.3.4-2.el8 | |
pcp-pmda-shping | 5.3.4-2.el8 | |
pcp-pmda-slurm | 5.3.4-2.el8 | |
pcp-pmda-smart | 5.3.4-2.el8 | |
pcp-pmda-snmp | 5.3.4-2.el8 | |
pcp-pmda-sockets | 5.3.4-2.el8 | |
pcp-pmda-statsd | 5.3.4-2.el8 | |
pcp-pmda-summary | 5.3.4-2.el8 | |
pcp-pmda-systemd | 5.3.4-2.el8 | |
pcp-pmda-trace | 5.3.4-2.el8 | |
pcp-pmda-unbound | 5.3.4-2.el8 | |
pcp-pmda-vmware | 5.3.4-2.el8 | |
pcp-pmda-weblog | 5.3.4-2.el8 | |
pcp-pmda-zimbra | 5.3.4-2.el8 | |
pcp-pmda-zswap | 5.3.4-2.el8 | |
pcp-selinux | 5.3.4-2.el8 | |
pcp-system-tools | 5.3.4-2.el8 | |
pcp-testsuite | 5.3.4-2.el8 | |
pcp-zeroconf | 5.3.4-2.el8 | |
perl-Data-Dump | 1.23-7.module_el8.3.0+416+9a1a0b3f | |
perl-Digest-HMAC | 1.03-17.module_el8.3.0+416+9a1a0b3f | |
perl-Encode-Locale | 1.05-10.module_el8.3.0+416+9a1a0b3f | |
perl-File-Listing | 6.04-17.module_el8.3.0+416+9a1a0b3f | |
perl-HTML-Parser | 3.72-15.module_el8.3.0+416+9a1a0b3f | |
perl-HTML-Tagset | 3.20-34.module_el8.3.0+416+9a1a0b3f | |
perl-HTTP-Cookies | 6.04-2.module_el8.3.0+416+9a1a0b3f | |
perl-HTTP-Date | 6.02-19.module_el8.3.0+416+9a1a0b3f | |
perl-HTTP-Message | 6.18-1.module_el8.3.0+416+9a1a0b3f | |
perl-HTTP-Negotiate | 6.01-19.module_el8.3.0+416+9a1a0b3f | |
perl-IO-HTML | 1.001-11.module_el8.3.0+416+9a1a0b3f | |
perl-libwww-perl | 6.34-1.module_el8.3.0+416+9a1a0b3f | |
perl-LWP-MediaTypes | 6.02-15.module_el8.3.0+416+9a1a0b3f | |
perl-LWP-Protocol-https | 6.07-4.module_el8.3.0+416+9a1a0b3f | |
perl-Mozilla-CA | 20160104-7.module_el8.3.0+416+9a1a0b3f | |
perl-Net-HTTP | 6.17-2.module_el8.3.0+416+9a1a0b3f | |
perl-NTLM | 1.09-17.module_el8.3.0+416+9a1a0b3f | |
perl-PCP-LogImport | 5.3.4-2.el8 | |
perl-PCP-LogSummary | 5.3.4-2.el8 | |
perl-PCP-MMV | 5.3.4-2.el8 | |
perl-PCP-PMDA | 5.3.4-2.el8 | |
perl-TimeDate | 2.30-15.module_el8.3.0+416+9a1a0b3f | |
perl-Try-Tiny | 0.30-7.module_el8.3.0+416+9a1a0b3f | |
perl-WWW-RobotRules | 6.02-18.module_el8.3.0+416+9a1a0b3f | |
python3-pcp | 5.3.4-2.el8 | |

### PowerTools x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
ibus-devel | 1.5.19-14.el8 | |
ibus-devel-docs | 1.5.19-14.el8 | |
kernel-tools-libs-devel | 4.18.0-305.25.1.el8_4 | [RHSA-2021:4056](https://access.redhat.com/errata/RHSA-2021:4056) | <div class="adv_s">Security Advisory</div>
openblas-devel | 0.3.15-1.el8 | |
openblas-openmp | 0.3.15-1.el8 | |
openblas-openmp64 | 0.3.15-1.el8 | |
openblas-openmp64_ | 0.3.15-1.el8 | |
openblas-Rblas | 0.3.15-1.el8 | |
openblas-serial64 | 0.3.15-1.el8 | |
openblas-serial64_ | 0.3.15-1.el8 | |
openblas-static | 0.3.15-1.el8 | |
openblas-threads64 | 0.3.15-1.el8 | |
openblas-threads64_ | 0.3.15-1.el8 | |

### HighAvailability x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
resource-agents | 4.9.0-7.el8 | |
resource-agents-aliyun | 4.9.0-7.el8 | |
resource-agents-gcp | 4.9.0-7.el8 | |

### RT x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
rteval-loads | 1.4-14.el8 | |

### ResilientStorage x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
resource-agents | 4.9.0-7.el8 | |
resource-agents-aliyun | 4.9.0-7.el8 | |
resource-agents-gcp | 4.9.0-7.el8 | |
resource-agents-paf | 4.9.0-7.el8 | |

### cloud x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
python-oslo-context-doc | 3.1.2-1.el8 | |
python3-oslo-context | 3.1.2-1.el8 | |
python3-oslo-context-tests | 3.1.2-1.el8 | |

### virt x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
libvirt | 7.6.0-6.el8s | |
libvirt-client | 7.6.0-6.el8s | |
libvirt-daemon | 7.6.0-6.el8s | |
libvirt-daemon-config-network | 7.6.0-6.el8s | |
libvirt-daemon-config-nwfilter | 7.6.0-6.el8s | |
libvirt-daemon-driver-interface | 7.6.0-6.el8s | |
libvirt-daemon-driver-network | 7.6.0-6.el8s | |
libvirt-daemon-driver-nodedev | 7.6.0-6.el8s | |
libvirt-daemon-driver-nwfilter | 7.6.0-6.el8s | |
libvirt-daemon-driver-qemu | 7.6.0-6.el8s | |
libvirt-daemon-driver-secret | 7.6.0-6.el8s | |
libvirt-daemon-driver-storage | 7.6.0-6.el8s | |
libvirt-daemon-driver-storage-core | 7.6.0-6.el8s | |
libvirt-daemon-driver-storage-disk | 7.6.0-6.el8s | |
libvirt-daemon-driver-storage-gluster | 7.6.0-6.el8s | |
libvirt-daemon-driver-storage-iscsi | 7.6.0-6.el8s | |
libvirt-daemon-driver-storage-iscsi-direct | 7.6.0-6.el8s | |
libvirt-daemon-driver-storage-logical | 7.6.0-6.el8s | |
libvirt-daemon-driver-storage-mpath | 7.6.0-6.el8s | |
libvirt-daemon-driver-storage-rbd | 7.6.0-6.el8s | |
libvirt-daemon-driver-storage-scsi | 7.6.0-6.el8s | |
libvirt-daemon-kvm | 7.6.0-6.el8s | |
libvirt-devel | 7.6.0-6.el8s | |
libvirt-docs | 7.6.0-6.el8s | |
libvirt-libs | 7.6.0-6.el8s | |
libvirt-lock-sanlock | 7.6.0-6.el8s | |
libvirt-nss | 7.6.0-6.el8s | |
libvirt-wireshark | 7.6.0-6.el8s | |

### BaseOS aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
bpftool | 4.18.0-305.25.1.el8_4 | [RHSA-2021:4056](https://access.redhat.com/errata/RHSA-2021:4056) | <div class="adv_s">Security Advisory</div>
kernel | 4.18.0-305.25.1.el8_4 | [RHSA-2021:4056](https://access.redhat.com/errata/RHSA-2021:4056) | <div class="adv_s">Security Advisory</div>
kernel-abi-stablelists | 4.18.0-305.25.1.el8_4 | [RHSA-2021:4056](https://access.redhat.com/errata/RHSA-2021:4056) | <div class="adv_s">Security Advisory</div>
kernel-core | 4.18.0-305.25.1.el8_4 | [RHSA-2021:4056](https://access.redhat.com/errata/RHSA-2021:4056) | <div class="adv_s">Security Advisory</div>
kernel-cross-headers | 4.18.0-305.25.1.el8_4 | [RHSA-2021:4056](https://access.redhat.com/errata/RHSA-2021:4056) | <div class="adv_s">Security Advisory</div>
kernel-debug | 4.18.0-305.25.1.el8_4 | [RHSA-2021:4056](https://access.redhat.com/errata/RHSA-2021:4056) | <div class="adv_s">Security Advisory</div>
kernel-debug-core | 4.18.0-305.25.1.el8_4 | [RHSA-2021:4056](https://access.redhat.com/errata/RHSA-2021:4056) | <div class="adv_s">Security Advisory</div>
kernel-debug-devel | 4.18.0-305.25.1.el8_4 | [RHSA-2021:4056](https://access.redhat.com/errata/RHSA-2021:4056) | <div class="adv_s">Security Advisory</div>
kernel-debug-modules | 4.18.0-305.25.1.el8_4 | [RHSA-2021:4056](https://access.redhat.com/errata/RHSA-2021:4056) | <div class="adv_s">Security Advisory</div>
kernel-debug-modules-extra | 4.18.0-305.25.1.el8_4 | [RHSA-2021:4056](https://access.redhat.com/errata/RHSA-2021:4056) | <div class="adv_s">Security Advisory</div>
kernel-devel | 4.18.0-305.25.1.el8_4 | [RHSA-2021:4056](https://access.redhat.com/errata/RHSA-2021:4056) | <div class="adv_s">Security Advisory</div>
kernel-doc | 4.18.0-305.25.1.el8_4 | [RHSA-2021:4056](https://access.redhat.com/errata/RHSA-2021:4056) | <div class="adv_s">Security Advisory</div>
kernel-headers | 4.18.0-305.25.1.el8_4 | [RHSA-2021:4056](https://access.redhat.com/errata/RHSA-2021:4056) | <div class="adv_s">Security Advisory</div>
kernel-modules | 4.18.0-305.25.1.el8_4 | [RHSA-2021:4056](https://access.redhat.com/errata/RHSA-2021:4056) | <div class="adv_s">Security Advisory</div>
kernel-modules-extra | 4.18.0-305.25.1.el8_4 | [RHSA-2021:4056](https://access.redhat.com/errata/RHSA-2021:4056) | <div class="adv_s">Security Advisory</div>
kernel-tools | 4.18.0-305.25.1.el8_4 | [RHSA-2021:4056](https://access.redhat.com/errata/RHSA-2021:4056) | <div class="adv_s">Security Advisory</div>
kernel-tools-libs | 4.18.0-305.25.1.el8_4 | [RHSA-2021:4056](https://access.redhat.com/errata/RHSA-2021:4056) | <div class="adv_s">Security Advisory</div>
perf | 4.18.0-305.25.1.el8_4 | [RHSA-2021:4056](https://access.redhat.com/errata/RHSA-2021:4056) | <div class="adv_s">Security Advisory</div>
python3-perf | 4.18.0-305.25.1.el8_4 | [RHSA-2021:4056](https://access.redhat.com/errata/RHSA-2021:4056) | <div class="adv_s">Security Advisory</div>

### AppStream aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
annobin | 10.21-3.el8 | |
annobin-annocheck | 10.21-3.el8 | |
gcc-toolset-11-annobin-annocheck | 9.85-1.el8 | |
gcc-toolset-11-annobin-docs | 9.85-1.el8 | |
gcc-toolset-11-annobin-plugin-gcc | 9.85-1.el8 | |
gcc-toolset-11-dyninst | 11.0.0-2.el8 | |
gcc-toolset-11-dyninst-devel | 11.0.0-2.el8 | |
gcc-toolset-11-elfutils | 0.185-3.el8 | |
gcc-toolset-11-elfutils-debuginfod-client | 0.185-3.el8 | [RHSA-2020:1665](https://access.redhat.com/errata/RHSA-2020:1665) | <div class="adv_s">Security Advisory</div>
gcc-toolset-11-elfutils-debuginfod-client-devel | 0.185-3.el8 | [RHSA-2020:1665](https://access.redhat.com/errata/RHSA-2020:1665) | <div class="adv_s">Security Advisory</div>
gcc-toolset-11-elfutils-devel | 0.185-3.el8 | |
gcc-toolset-11-elfutils-libelf | 0.185-3.el8 | |
gcc-toolset-11-elfutils-libelf-devel | 0.185-3.el8 | |
gcc-toolset-11-elfutils-libs | 0.185-3.el8 | |
gcc-toolset-11-gcc | 11.2.1-1.1.el8 | |
gcc-toolset-11-gcc-c++ | 11.2.1-1.1.el8 | |
gcc-toolset-11-gcc-gdb-plugin | 11.2.1-1.1.el8 | |
gcc-toolset-11-gcc-gfortran | 11.2.1-1.1.el8 | |
gcc-toolset-11-gcc-plugin-devel | 11.2.1-1.1.el8 | |
gcc-toolset-11-libasan-devel | 11.2.1-1.1.el8 | |
gcc-toolset-11-libatomic-devel | 11.2.1-1.1.el8 | |
gcc-toolset-11-libgccjit | 11.2.1-1.1.el8 | |
gcc-toolset-11-libgccjit-devel | 11.2.1-1.1.el8 | |
gcc-toolset-11-libgccjit-docs | 11.2.1-1.1.el8 | |
gcc-toolset-11-libitm-devel | 11.2.1-1.1.el8 | |
gcc-toolset-11-liblsan-devel | 11.2.1-1.1.el8 | |
gcc-toolset-11-libstdc++-devel | 11.2.1-1.1.el8 | |
gcc-toolset-11-libstdc++-docs | 11.2.1-1.1.el8 | |
gcc-toolset-11-libtsan-devel | 11.2.1-1.1.el8 | |
gcc-toolset-11-libubsan-devel | 11.2.1-1.1.el8 | |
gcc-toolset-11-strace | 5.13-4.el8 | |
gcc-toolset-11-systemtap | 4.5-4.el8 | |
gcc-toolset-11-systemtap-client | 4.5-4.el8 | |
gcc-toolset-11-systemtap-devel | 4.5-4.el8 | |
gcc-toolset-11-systemtap-initscript | 4.5-4.el8 | |
gcc-toolset-11-systemtap-runtime | 4.5-4.el8 | |
gcc-toolset-11-systemtap-sdt-devel | 4.5-4.el8 | |
gcc-toolset-11-systemtap-server | 4.5-4.el8 | |
gcc-toolset-11-valgrind | 3.17.0-6.el8 | |
gcc-toolset-11-valgrind-devel | 3.17.0-6.el8 | |
go-toolset | 1.17.2-1.module_el8.6.0+962+0036b8f3 | |
golang | 1.17.2-2.module_el8.6.0+963+7827afaa | |
golang-bin | 1.17.2-2.module_el8.6.0+963+7827afaa | |
golang-docs | 1.17.2-2.module_el8.6.0+963+7827afaa | |
golang-misc | 1.17.2-2.module_el8.6.0+963+7827afaa | |
golang-src | 1.17.2-2.module_el8.6.0+963+7827afaa | |
golang-tests | 1.17.2-2.module_el8.6.0+963+7827afaa | |
ibus | 1.5.19-14.el8 | |
ibus-gtk2 | 1.5.19-14.el8 | |
ibus-gtk3 | 1.5.19-14.el8 | |
ibus-libs | 1.5.19-14.el8 | |
ibus-setup | 1.5.19-14.el8 | |
ibus-wayland | 1.5.19-14.el8 | |
libasan6 | 11.2.1-1.1.el8 | |
openblas | 0.3.15-1.el8 | |
openblas-threads | 0.3.15-1.el8 | |
pcp | 5.3.4-2.el8 | |
pcp-conf | 5.3.4-2.el8 | |
pcp-devel | 5.3.4-2.el8 | |
pcp-doc | 5.3.4-2.el8 | |
pcp-export-pcp2elasticsearch | 5.3.4-2.el8 | |
pcp-export-pcp2graphite | 5.3.4-2.el8 | |
pcp-export-pcp2influxdb | 5.3.4-2.el8 | |
pcp-export-pcp2json | 5.3.4-2.el8 | |
pcp-export-pcp2spark | 5.3.4-2.el8 | |
pcp-export-pcp2xml | 5.3.4-2.el8 | |
pcp-export-pcp2zabbix | 5.3.4-2.el8 | |
pcp-export-zabbix-agent | 5.3.4-2.el8 | |
pcp-gui | 5.3.4-2.el8 | |
pcp-import-collectl2pcp | 5.3.4-2.el8 | |
pcp-import-ganglia2pcp | 5.3.4-2.el8 | |
pcp-import-iostat2pcp | 5.3.4-2.el8 | |
pcp-import-mrtg2pcp | 5.3.4-2.el8 | |
pcp-import-sar2pcp | 5.3.4-2.el8 | |
pcp-libs | 5.3.4-2.el8 | |
pcp-libs-devel | 5.3.4-2.el8 | |
pcp-pmda-activemq | 5.3.4-2.el8 | |
pcp-pmda-apache | 5.3.4-2.el8 | |
pcp-pmda-bash | 5.3.4-2.el8 | |
pcp-pmda-bcc | 5.3.4-2.el8 | |
pcp-pmda-bind2 | 5.3.4-2.el8 | |
pcp-pmda-bonding | 5.3.4-2.el8 | |
pcp-pmda-bpftrace | 5.3.4-2.el8 | |
pcp-pmda-cifs | 5.3.4-2.el8 | |
pcp-pmda-cisco | 5.3.4-2.el8 | |
pcp-pmda-dbping | 5.3.4-2.el8 | |
pcp-pmda-denki | 5.3.4-2.el8 | |
pcp-pmda-dm | 5.3.4-2.el8 | |
pcp-pmda-docker | 5.3.4-2.el8 | |
pcp-pmda-ds389 | 5.3.4-2.el8 | |
pcp-pmda-ds389log | 5.3.4-2.el8 | |
pcp-pmda-elasticsearch | 5.3.4-2.el8 | |
pcp-pmda-gfs2 | 5.3.4-2.el8 | |
pcp-pmda-gluster | 5.3.4-2.el8 | |
pcp-pmda-gpfs | 5.3.4-2.el8 | |
pcp-pmda-gpsd | 5.3.4-2.el8 | |
pcp-pmda-hacluster | 5.3.4-2.el8 | |
pcp-pmda-haproxy | 5.3.4-2.el8 | |
pcp-pmda-infiniband | 5.3.4-2.el8 | |
pcp-pmda-json | 5.3.4-2.el8 | |
pcp-pmda-libvirt | 5.3.4-2.el8 | |
pcp-pmda-lio | 5.3.4-2.el8 | |
pcp-pmda-lmsensors | 5.3.4-2.el8 | |
pcp-pmda-logger | 5.3.4-2.el8 | |
pcp-pmda-lustre | 5.3.4-2.el8 | |
pcp-pmda-lustrecomm | 5.3.4-2.el8 | |
pcp-pmda-mailq | 5.3.4-2.el8 | |
pcp-pmda-memcache | 5.3.4-2.el8 | |
pcp-pmda-mic | 5.3.4-2.el8 | |
pcp-pmda-mounts | 5.3.4-2.el8 | |
pcp-pmda-mysql | 5.3.4-2.el8 | |
pcp-pmda-named | 5.3.4-2.el8 | |
pcp-pmda-netcheck | 5.3.4-2.el8 | |
pcp-pmda-netfilter | 5.3.4-2.el8 | |
pcp-pmda-news | 5.3.4-2.el8 | |
pcp-pmda-nfsclient | 5.3.4-2.el8 | |
pcp-pmda-nginx | 5.3.4-2.el8 | |
pcp-pmda-nvidia-gpu | 5.3.4-2.el8 | |
pcp-pmda-openmetrics | 5.3.4-2.el8 | |
pcp-pmda-openvswitch | 5.3.4-2.el8 | |
pcp-pmda-oracle | 5.3.4-2.el8 | |
pcp-pmda-pdns | 5.3.4-2.el8 | |
pcp-pmda-perfevent | 5.3.4-2.el8 | |
pcp-pmda-podman | 5.3.4-2.el8 | |
pcp-pmda-postfix | 5.3.4-2.el8 | |
pcp-pmda-postgresql | 5.3.4-2.el8 | |
pcp-pmda-rabbitmq | 5.3.4-2.el8 | |
pcp-pmda-redis | 5.3.4-2.el8 | |
pcp-pmda-roomtemp | 5.3.4-2.el8 | |
pcp-pmda-rsyslog | 5.3.4-2.el8 | |
pcp-pmda-samba | 5.3.4-2.el8 | |
pcp-pmda-sendmail | 5.3.4-2.el8 | |
pcp-pmda-shping | 5.3.4-2.el8 | |
pcp-pmda-slurm | 5.3.4-2.el8 | |
pcp-pmda-smart | 5.3.4-2.el8 | |
pcp-pmda-snmp | 5.3.4-2.el8 | |
pcp-pmda-sockets | 5.3.4-2.el8 | |
pcp-pmda-statsd | 5.3.4-2.el8 | |
pcp-pmda-summary | 5.3.4-2.el8 | |
pcp-pmda-systemd | 5.3.4-2.el8 | |
pcp-pmda-trace | 5.3.4-2.el8 | |
pcp-pmda-unbound | 5.3.4-2.el8 | |
pcp-pmda-vmware | 5.3.4-2.el8 | |
pcp-pmda-weblog | 5.3.4-2.el8 | |
pcp-pmda-zimbra | 5.3.4-2.el8 | |
pcp-pmda-zswap | 5.3.4-2.el8 | |
pcp-selinux | 5.3.4-2.el8 | |
pcp-system-tools | 5.3.4-2.el8 | |
pcp-testsuite | 5.3.4-2.el8 | |
pcp-zeroconf | 5.3.4-2.el8 | |
perl-Data-Dump | 1.23-7.module_el8.3.0+416+9a1a0b3f | |
perl-Digest-HMAC | 1.03-17.module_el8.3.0+416+9a1a0b3f | |
perl-Encode-Locale | 1.05-10.module_el8.3.0+416+9a1a0b3f | |
perl-File-Listing | 6.04-17.module_el8.3.0+416+9a1a0b3f | |
perl-HTML-Parser | 3.72-15.module_el8.3.0+416+9a1a0b3f | |
perl-HTML-Tagset | 3.20-34.module_el8.3.0+416+9a1a0b3f | |
perl-HTTP-Cookies | 6.04-2.module_el8.3.0+416+9a1a0b3f | |
perl-HTTP-Date | 6.02-19.module_el8.3.0+416+9a1a0b3f | |
perl-HTTP-Message | 6.18-1.module_el8.3.0+416+9a1a0b3f | |
perl-HTTP-Negotiate | 6.01-19.module_el8.3.0+416+9a1a0b3f | |
perl-IO-HTML | 1.001-11.module_el8.3.0+416+9a1a0b3f | |
perl-libwww-perl | 6.34-1.module_el8.3.0+416+9a1a0b3f | |
perl-LWP-MediaTypes | 6.02-15.module_el8.3.0+416+9a1a0b3f | |
perl-LWP-Protocol-https | 6.07-4.module_el8.3.0+416+9a1a0b3f | |
perl-Mozilla-CA | 20160104-7.module_el8.3.0+416+9a1a0b3f | |
perl-Net-HTTP | 6.17-2.module_el8.3.0+416+9a1a0b3f | |
perl-NTLM | 1.09-17.module_el8.3.0+416+9a1a0b3f | |
perl-PCP-LogImport | 5.3.4-2.el8 | |
perl-PCP-LogSummary | 5.3.4-2.el8 | |
perl-PCP-MMV | 5.3.4-2.el8 | |
perl-PCP-PMDA | 5.3.4-2.el8 | |
perl-TimeDate | 2.30-15.module_el8.3.0+416+9a1a0b3f | |
perl-Try-Tiny | 0.30-7.module_el8.3.0+416+9a1a0b3f | |
perl-WWW-RobotRules | 6.02-18.module_el8.3.0+416+9a1a0b3f | |
python3-pcp | 5.3.4-2.el8 | |

### PowerTools aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
ibus-devel | 1.5.19-14.el8 | |
ibus-devel-docs | 1.5.19-14.el8 | |
kernel-tools-libs-devel | 4.18.0-305.25.1.el8_4 | [RHSA-2021:4056](https://access.redhat.com/errata/RHSA-2021:4056) | <div class="adv_s">Security Advisory</div>
openblas-devel | 0.3.15-1.el8 | |
openblas-openmp | 0.3.15-1.el8 | |
openblas-openmp64 | 0.3.15-1.el8 | |
openblas-openmp64_ | 0.3.15-1.el8 | |
openblas-Rblas | 0.3.15-1.el8 | |
openblas-serial64 | 0.3.15-1.el8 | |
openblas-serial64_ | 0.3.15-1.el8 | |
openblas-static | 0.3.15-1.el8 | |
openblas-threads64 | 0.3.15-1.el8 | |
openblas-threads64_ | 0.3.15-1.el8 | |

### HighAvailability aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
resource-agents | 4.9.0-7.el8 | |

### cloud aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
python-oslo-context-doc | 3.1.2-1.el8 | |
python3-oslo-context | 3.1.2-1.el8 | |
python3-oslo-context-tests | 3.1.2-1.el8 | |

### virt aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
libvirt | 7.6.0-6.el8s | |
libvirt-client | 7.6.0-6.el8s | |
libvirt-daemon | 7.6.0-6.el8s | |
libvirt-daemon-config-network | 7.6.0-6.el8s | |
libvirt-daemon-config-nwfilter | 7.6.0-6.el8s | |
libvirt-daemon-driver-interface | 7.6.0-6.el8s | |
libvirt-daemon-driver-network | 7.6.0-6.el8s | |
libvirt-daemon-driver-nodedev | 7.6.0-6.el8s | |
libvirt-daemon-driver-nwfilter | 7.6.0-6.el8s | |
libvirt-daemon-driver-qemu | 7.6.0-6.el8s | |
libvirt-daemon-driver-secret | 7.6.0-6.el8s | |
libvirt-daemon-driver-storage | 7.6.0-6.el8s | |
libvirt-daemon-driver-storage-core | 7.6.0-6.el8s | |
libvirt-daemon-driver-storage-disk | 7.6.0-6.el8s | |
libvirt-daemon-driver-storage-gluster | 7.6.0-6.el8s | |
libvirt-daemon-driver-storage-iscsi | 7.6.0-6.el8s | |
libvirt-daemon-driver-storage-iscsi-direct | 7.6.0-6.el8s | |
libvirt-daemon-driver-storage-logical | 7.6.0-6.el8s | |
libvirt-daemon-driver-storage-mpath | 7.6.0-6.el8s | |
libvirt-daemon-driver-storage-rbd | 7.6.0-6.el8s | |
libvirt-daemon-driver-storage-scsi | 7.6.0-6.el8s | |
libvirt-daemon-kvm | 7.6.0-6.el8s | |
libvirt-devel | 7.6.0-6.el8s | |
libvirt-docs | 7.6.0-6.el8s | |
libvirt-libs | 7.6.0-6.el8s | |
libvirt-lock-sanlock | 7.6.0-6.el8s | |
libvirt-nss | 7.6.0-6.el8s | |
libvirt-wireshark | 7.6.0-6.el8s | |

