## 2021-09-21

### BaseOS x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
bpftool | 4.18.0-305.17.1.el8_4 | [RHSA-2021:3447](https://access.redhat.com/errata/RHSA-2021:3447) | <div class="adv_s">Security Advisory</div>
bpftool | 4.18.0-305.19.1.el8_4 | [RHSA-2021:3548](https://access.redhat.com/errata/RHSA-2021:3548) | <div class="adv_s">Security Advisory</div>
centos-gpg-keys | 8-3.el8 | |
centos-stream-release | 8.6-1.el8 | |
centos-stream-repos | 8-3.el8 | |
cockpit | 251.1-1.el8 | |
cockpit-bridge | 251.1-1.el8 | |
cockpit-doc | 251.1-1.el8 | |
cockpit-system | 251.1-1.el8 | |
cockpit-ws | 251.1-1.el8 | |
device-mapper | 1.02.177-8.el8 | |
device-mapper-event | 1.02.177-8.el8 | |
device-mapper-event-libs | 1.02.177-8.el8 | |
device-mapper-libs | 1.02.177-8.el8 | |
dnf-plugin-subscription-manager | 1.28.21-3.el8 | |
kernel | 4.18.0-305.17.1.el8_4 | [RHSA-2021:3447](https://access.redhat.com/errata/RHSA-2021:3447) | <div class="adv_s">Security Advisory</div>
kernel | 4.18.0-305.19.1.el8_4 | [RHSA-2021:3548](https://access.redhat.com/errata/RHSA-2021:3548) | <div class="adv_s">Security Advisory</div>
kernel-abi-stablelists | 4.18.0-305.17.1.el8_4 | [RHSA-2021:3447](https://access.redhat.com/errata/RHSA-2021:3447) | <div class="adv_s">Security Advisory</div>
kernel-abi-stablelists | 4.18.0-305.19.1.el8_4 | [RHSA-2021:3548](https://access.redhat.com/errata/RHSA-2021:3548) | <div class="adv_s">Security Advisory</div>
kernel-core | 4.18.0-305.17.1.el8_4 | [RHSA-2021:3447](https://access.redhat.com/errata/RHSA-2021:3447) | <div class="adv_s">Security Advisory</div>
kernel-core | 4.18.0-305.19.1.el8_4 | [RHSA-2021:3548](https://access.redhat.com/errata/RHSA-2021:3548) | <div class="adv_s">Security Advisory</div>
kernel-cross-headers | 4.18.0-305.17.1.el8_4 | [RHSA-2021:3447](https://access.redhat.com/errata/RHSA-2021:3447) | <div class="adv_s">Security Advisory</div>
kernel-cross-headers | 4.18.0-305.19.1.el8_4 | [RHSA-2021:3548](https://access.redhat.com/errata/RHSA-2021:3548) | <div class="adv_s">Security Advisory</div>
kernel-debug | 4.18.0-305.17.1.el8_4 | [RHSA-2021:3447](https://access.redhat.com/errata/RHSA-2021:3447) | <div class="adv_s">Security Advisory</div>
kernel-debug | 4.18.0-305.19.1.el8_4 | [RHSA-2021:3548](https://access.redhat.com/errata/RHSA-2021:3548) | <div class="adv_s">Security Advisory</div>
kernel-debug-core | 4.18.0-305.17.1.el8_4 | [RHSA-2021:3447](https://access.redhat.com/errata/RHSA-2021:3447) | <div class="adv_s">Security Advisory</div>
kernel-debug-core | 4.18.0-305.19.1.el8_4 | [RHSA-2021:3548](https://access.redhat.com/errata/RHSA-2021:3548) | <div class="adv_s">Security Advisory</div>
kernel-debug-devel | 4.18.0-305.17.1.el8_4 | [RHSA-2021:3447](https://access.redhat.com/errata/RHSA-2021:3447) | <div class="adv_s">Security Advisory</div>
kernel-debug-devel | 4.18.0-305.19.1.el8_4 | [RHSA-2021:3548](https://access.redhat.com/errata/RHSA-2021:3548) | <div class="adv_s">Security Advisory</div>
kernel-debug-modules | 4.18.0-305.17.1.el8_4 | [RHSA-2021:3447](https://access.redhat.com/errata/RHSA-2021:3447) | <div class="adv_s">Security Advisory</div>
kernel-debug-modules | 4.18.0-305.19.1.el8_4 | [RHSA-2021:3548](https://access.redhat.com/errata/RHSA-2021:3548) | <div class="adv_s">Security Advisory</div>
kernel-debug-modules-extra | 4.18.0-305.17.1.el8_4 | [RHSA-2021:3447](https://access.redhat.com/errata/RHSA-2021:3447) | <div class="adv_s">Security Advisory</div>
kernel-debug-modules-extra | 4.18.0-305.19.1.el8_4 | [RHSA-2021:3548](https://access.redhat.com/errata/RHSA-2021:3548) | <div class="adv_s">Security Advisory</div>
kernel-devel | 4.18.0-305.17.1.el8_4 | [RHSA-2021:3447](https://access.redhat.com/errata/RHSA-2021:3447) | <div class="adv_s">Security Advisory</div>
kernel-devel | 4.18.0-305.19.1.el8_4 | [RHSA-2021:3548](https://access.redhat.com/errata/RHSA-2021:3548) | <div class="adv_s">Security Advisory</div>
kernel-doc | 4.18.0-305.17.1.el8_4 | [RHSA-2021:3447](https://access.redhat.com/errata/RHSA-2021:3447) | <div class="adv_s">Security Advisory</div>
kernel-doc | 4.18.0-305.19.1.el8_4 | [RHSA-2021:3548](https://access.redhat.com/errata/RHSA-2021:3548) | <div class="adv_s">Security Advisory</div>
kernel-headers | 4.18.0-305.17.1.el8_4 | [RHSA-2021:3447](https://access.redhat.com/errata/RHSA-2021:3447) | <div class="adv_s">Security Advisory</div>
kernel-headers | 4.18.0-305.19.1.el8_4 | [RHSA-2021:3548](https://access.redhat.com/errata/RHSA-2021:3548) | <div class="adv_s">Security Advisory</div>
kernel-modules | 4.18.0-305.17.1.el8_4 | [RHSA-2021:3447](https://access.redhat.com/errata/RHSA-2021:3447) | <div class="adv_s">Security Advisory</div>
kernel-modules | 4.18.0-305.19.1.el8_4 | [RHSA-2021:3548](https://access.redhat.com/errata/RHSA-2021:3548) | <div class="adv_s">Security Advisory</div>
kernel-modules-extra | 4.18.0-305.17.1.el8_4 | [RHSA-2021:3447](https://access.redhat.com/errata/RHSA-2021:3447) | <div class="adv_s">Security Advisory</div>
kernel-modules-extra | 4.18.0-305.19.1.el8_4 | [RHSA-2021:3548](https://access.redhat.com/errata/RHSA-2021:3548) | <div class="adv_s">Security Advisory</div>
kernel-rt-core | 4.18.0-338.rt7.119.el8 | |
kernel-tools | 4.18.0-305.17.1.el8_4 | [RHSA-2021:3447](https://access.redhat.com/errata/RHSA-2021:3447) | <div class="adv_s">Security Advisory</div>
kernel-tools | 4.18.0-305.19.1.el8_4 | [RHSA-2021:3548](https://access.redhat.com/errata/RHSA-2021:3548) | <div class="adv_s">Security Advisory</div>
kernel-tools-libs | 4.18.0-305.17.1.el8_4 | [RHSA-2021:3447](https://access.redhat.com/errata/RHSA-2021:3447) | <div class="adv_s">Security Advisory</div>
kernel-tools-libs | 4.18.0-305.19.1.el8_4 | [RHSA-2021:3548](https://access.redhat.com/errata/RHSA-2021:3548) | <div class="adv_s">Security Advisory</div>
libdb | 5.3.28-42.el8_4 | |
libdb-utils | 5.3.28-42.el8_4 | |
lvm2 | 2.03.12-8.el8 | |
lvm2-dbusd | 2.03.12-8.el8 | |
lvm2-libs | 2.03.12-8.el8 | |
lvm2-lockd | 2.03.12-8.el8 | |
perf | 4.18.0-305.17.1.el8_4 | [RHSA-2021:3447](https://access.redhat.com/errata/RHSA-2021:3447) | <div class="adv_s">Security Advisory</div>
perf | 4.18.0-305.19.1.el8_4 | [RHSA-2021:3548](https://access.redhat.com/errata/RHSA-2021:3548) | <div class="adv_s">Security Advisory</div>
platform-python | 3.6.8-41.el8 | |
python3-cloud-what | 1.28.21-3.el8 | |
python3-libs | 3.6.8-41.el8 | |
python3-perf | 4.18.0-305.17.1.el8_4 | [RHSA-2021:3447](https://access.redhat.com/errata/RHSA-2021:3447) | <div class="adv_s">Security Advisory</div>
python3-perf | 4.18.0-305.19.1.el8_4 | [RHSA-2021:3548](https://access.redhat.com/errata/RHSA-2021:3548) | <div class="adv_s">Security Advisory</div>
python3-subscription-manager-rhsm | 1.28.21-3.el8 | |
python3-syspurpose | 1.28.21-3.el8 | |
python3-test | 3.6.8-41.el8 | |
rhsm-icons | 1.28.21-3.el8 | |
subscription-manager | 1.28.21-3.el8 | |
subscription-manager-cockpit | 1.28.21-3.el8 | |
subscription-manager-plugin-ostree | 1.28.21-3.el8 | |
subscription-manager-rhsm-certificates | 1.28.21-3.el8 | |

### AppStream x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
aspnetcore-runtime-3.1 | 3.1.19-1.el8_4 | [RHBA-2021:3538](https://access.redhat.com/errata/RHBA-2021:3538) | <div class="adv_b">Bug Fix Advisory</div>
aspnetcore-runtime-5.0 | 5.0.10-1.el8_4 | [RHBA-2021:3540](https://access.redhat.com/errata/RHBA-2021:3540) | <div class="adv_b">Bug Fix Advisory</div>
aspnetcore-targeting-pack-3.1 | 3.1.19-1.el8_4 | [RHBA-2021:3538](https://access.redhat.com/errata/RHBA-2021:3538) | <div class="adv_b">Bug Fix Advisory</div>
aspnetcore-targeting-pack-5.0 | 5.0.10-1.el8_4 | [RHBA-2021:3540](https://access.redhat.com/errata/RHBA-2021:3540) | <div class="adv_b">Bug Fix Advisory</div>
cockpit-machines | 251.1-1.el8 | |
cockpit-packagekit | 251.1-1.el8 | |
cockpit-pcp | 251.1-1.el8 | |
cockpit-storaged | 251.1-1.el8 | |
dotnet | 5.0.207-1.el8_4 | [RHBA-2021:3540](https://access.redhat.com/errata/RHBA-2021:3540) | <div class="adv_b">Bug Fix Advisory</div>
dotnet-apphost-pack-3.1 | 3.1.19-1.el8_4 | [RHBA-2021:3538](https://access.redhat.com/errata/RHBA-2021:3538) | <div class="adv_b">Bug Fix Advisory</div>
dotnet-apphost-pack-5.0 | 5.0.10-1.el8_4 | [RHBA-2021:3540](https://access.redhat.com/errata/RHBA-2021:3540) | <div class="adv_b">Bug Fix Advisory</div>
dotnet-host | 5.0.10-1.el8_4 | [RHBA-2021:3540](https://access.redhat.com/errata/RHBA-2021:3540) | <div class="adv_b">Bug Fix Advisory</div>
dotnet-hostfxr-3.1 | 3.1.19-1.el8_4 | [RHBA-2021:3538](https://access.redhat.com/errata/RHBA-2021:3538) | <div class="adv_b">Bug Fix Advisory</div>
dotnet-hostfxr-5.0 | 5.0.10-1.el8_4 | [RHBA-2021:3540](https://access.redhat.com/errata/RHBA-2021:3540) | <div class="adv_b">Bug Fix Advisory</div>
dotnet-runtime-3.1 | 3.1.19-1.el8_4 | [RHBA-2021:3538](https://access.redhat.com/errata/RHBA-2021:3538) | <div class="adv_b">Bug Fix Advisory</div>
dotnet-runtime-5.0 | 5.0.10-1.el8_4 | [RHBA-2021:3540](https://access.redhat.com/errata/RHBA-2021:3540) | <div class="adv_b">Bug Fix Advisory</div>
dotnet-sdk-3.1 | 3.1.119-1.el8_4 | [RHBA-2021:3538](https://access.redhat.com/errata/RHBA-2021:3538) | <div class="adv_b">Bug Fix Advisory</div>
dotnet-sdk-5.0 | 5.0.207-1.el8_4 | [RHBA-2021:3540](https://access.redhat.com/errata/RHBA-2021:3540) | <div class="adv_b">Bug Fix Advisory</div>
dotnet-targeting-pack-3.1 | 3.1.19-1.el8_4 | [RHBA-2021:3538](https://access.redhat.com/errata/RHBA-2021:3538) | <div class="adv_b">Bug Fix Advisory</div>
dotnet-targeting-pack-5.0 | 5.0.10-1.el8_4 | [RHBA-2021:3540](https://access.redhat.com/errata/RHBA-2021:3540) | <div class="adv_b">Bug Fix Advisory</div>
dotnet-templates-3.1 | 3.1.119-1.el8_4 | [RHBA-2021:3538](https://access.redhat.com/errata/RHBA-2021:3538) | <div class="adv_b">Bug Fix Advisory</div>
dotnet-templates-5.0 | 5.0.207-1.el8_4 | [RHBA-2021:3540](https://access.redhat.com/errata/RHBA-2021:3540) | <div class="adv_b">Bug Fix Advisory</div>
firefox | 78.14.0-1.el8_4 | [RHSA-2021:3497](https://access.redhat.com/errata/RHSA-2021:3497) | <div class="adv_s">Security Advisory</div>
libdb-devel | 5.3.28-42.el8_4 | |
netstandard-targeting-pack-2.1 | 5.0.207-1.el8_4 | [RHBA-2021:3540](https://access.redhat.com/errata/RHBA-2021:3540) | <div class="adv_b">Bug Fix Advisory</div>
osinfo-db | 20210903-1.el8 | |
platform-python-debug | 3.6.8-41.el8 | |
platform-python-devel | 3.6.8-41.el8 | |
pykickstart | 3.16.14-1.el8 | |
python3-idle | 3.6.8-41.el8 | |
python3-kickstart | 3.16.14-1.el8 | |
python3-tkinter | 3.6.8-41.el8 | |
spirv-tools | 2021.3-1.20210825.git1fbed83.el8 | |
spirv-tools-libs | 2021.3-1.20210825.git1fbed83.el8 | |
subscription-manager-migration | 1.28.21-3.el8 | |
thunderbird | 78.14.0-1.el8_4 | [RHSA-2021:3499](https://access.redhat.com/errata/RHSA-2021:3499) | <div class="adv_s">Security Advisory</div>
virtio-win | 1.9.19-1.el8 | |
vulkan-headers | 1.2.189.0-1.el8 | |
vulkan-loader | 1.2.189.0-1.el8 | |
vulkan-loader-devel | 1.2.189.0-1.el8 | |
vulkan-tools | 1.2.189.0-1.el8 | |
vulkan-validation-layers | 1.2.189.0-2.el8 | |

### PowerTools x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
device-mapper-devel | 1.02.177-8.el8 | |
device-mapper-event-devel | 1.02.177-8.el8 | |
kernel-tools-libs-devel | 4.18.0-305.17.1.el8_4 | [RHSA-2021:3447](https://access.redhat.com/errata/RHSA-2021:3447) | <div class="adv_s">Security Advisory</div>
kernel-tools-libs-devel | 4.18.0-305.19.1.el8_4 | [RHSA-2021:3548](https://access.redhat.com/errata/RHSA-2021:3548) | <div class="adv_s">Security Advisory</div>
libdb-cxx | 5.3.28-42.el8_4 | |
libdb-cxx-devel | 5.3.28-42.el8_4 | |
libdb-devel-doc | 5.3.28-42.el8_4 | |
libdb-sql | 5.3.28-42.el8_4 | |
libdb-sql-devel | 5.3.28-42.el8_4 | |
lvm2-devel | 2.03.12-8.el8 | |
spirv-tools-devel | 2021.3-1.20210825.git1fbed83.el8 | |

### HighAvailability x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
pcs | 0.10.10-3.el8 | |
pcs-snmp | 0.10.10-3.el8 | |

### ResilientStorage x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
cmirror | 2.03.12-8.el8 | |
pcs | 0.10.10-3.el8 | |
pcs-snmp | 0.10.10-3.el8 | |

### cloud x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
openstack-glance | 20.2.0-1.el8 | |
openstack-glance | 21.1.0-1.el8 | |
openstack-glance | 22.1.0-1.el8 | |
openstack-glance-doc | 20.2.0-1.el8 | |
openstack-glance-doc | 21.1.0-1.el8 | |
openstack-glance-doc | 22.1.0-1.el8 | |
python3-glance | 20.2.0-1.el8 | |
python3-glance | 21.1.0-1.el8 | |
python3-glance | 22.1.0-1.el8 | |
python3-glance-tests | 20.2.0-1.el8 | |
python3-glance-tests | 21.1.0-1.el8 | |
python3-glance-tests | 22.1.0-1.el8 | |

### BaseOS aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
bpftool | 4.18.0-305.17.1.el8_4 | [RHSA-2021:3447](https://access.redhat.com/errata/RHSA-2021:3447) | <div class="adv_s">Security Advisory</div>
bpftool | 4.18.0-305.19.1.el8_4 | [RHSA-2021:3548](https://access.redhat.com/errata/RHSA-2021:3548) | <div class="adv_s">Security Advisory</div>
centos-gpg-keys | 8-3.el8 | |
centos-stream-release | 8.6-1.el8 | |
centos-stream-repos | 8-3.el8 | |
cockpit | 251.1-1.el8 | |
cockpit-bridge | 251.1-1.el8 | |
cockpit-doc | 251.1-1.el8 | |
cockpit-system | 251.1-1.el8 | |
cockpit-ws | 251.1-1.el8 | |
device-mapper | 1.02.177-8.el8 | |
device-mapper-event | 1.02.177-8.el8 | |
device-mapper-event-libs | 1.02.177-8.el8 | |
device-mapper-libs | 1.02.177-8.el8 | |
dnf-plugin-subscription-manager | 1.28.21-3.el8 | |
kernel | 4.18.0-305.17.1.el8_4 | [RHSA-2021:3447](https://access.redhat.com/errata/RHSA-2021:3447) | <div class="adv_s">Security Advisory</div>
kernel | 4.18.0-305.19.1.el8_4 | [RHSA-2021:3548](https://access.redhat.com/errata/RHSA-2021:3548) | <div class="adv_s">Security Advisory</div>
kernel-abi-stablelists | 4.18.0-305.17.1.el8_4 | [RHSA-2021:3447](https://access.redhat.com/errata/RHSA-2021:3447) | <div class="adv_s">Security Advisory</div>
kernel-abi-stablelists | 4.18.0-305.19.1.el8_4 | [RHSA-2021:3548](https://access.redhat.com/errata/RHSA-2021:3548) | <div class="adv_s">Security Advisory</div>
kernel-core | 4.18.0-305.17.1.el8_4 | [RHSA-2021:3447](https://access.redhat.com/errata/RHSA-2021:3447) | <div class="adv_s">Security Advisory</div>
kernel-core | 4.18.0-305.19.1.el8_4 | [RHSA-2021:3548](https://access.redhat.com/errata/RHSA-2021:3548) | <div class="adv_s">Security Advisory</div>
kernel-cross-headers | 4.18.0-305.17.1.el8_4 | [RHSA-2021:3447](https://access.redhat.com/errata/RHSA-2021:3447) | <div class="adv_s">Security Advisory</div>
kernel-cross-headers | 4.18.0-305.19.1.el8_4 | [RHSA-2021:3548](https://access.redhat.com/errata/RHSA-2021:3548) | <div class="adv_s">Security Advisory</div>
kernel-debug | 4.18.0-305.17.1.el8_4 | [RHSA-2021:3447](https://access.redhat.com/errata/RHSA-2021:3447) | <div class="adv_s">Security Advisory</div>
kernel-debug | 4.18.0-305.19.1.el8_4 | [RHSA-2021:3548](https://access.redhat.com/errata/RHSA-2021:3548) | <div class="adv_s">Security Advisory</div>
kernel-debug-core | 4.18.0-305.17.1.el8_4 | [RHSA-2021:3447](https://access.redhat.com/errata/RHSA-2021:3447) | <div class="adv_s">Security Advisory</div>
kernel-debug-core | 4.18.0-305.19.1.el8_4 | [RHSA-2021:3548](https://access.redhat.com/errata/RHSA-2021:3548) | <div class="adv_s">Security Advisory</div>
kernel-debug-devel | 4.18.0-305.17.1.el8_4 | [RHSA-2021:3447](https://access.redhat.com/errata/RHSA-2021:3447) | <div class="adv_s">Security Advisory</div>
kernel-debug-devel | 4.18.0-305.19.1.el8_4 | [RHSA-2021:3548](https://access.redhat.com/errata/RHSA-2021:3548) | <div class="adv_s">Security Advisory</div>
kernel-debug-modules | 4.18.0-305.17.1.el8_4 | [RHSA-2021:3447](https://access.redhat.com/errata/RHSA-2021:3447) | <div class="adv_s">Security Advisory</div>
kernel-debug-modules | 4.18.0-305.19.1.el8_4 | [RHSA-2021:3548](https://access.redhat.com/errata/RHSA-2021:3548) | <div class="adv_s">Security Advisory</div>
kernel-debug-modules-extra | 4.18.0-305.17.1.el8_4 | [RHSA-2021:3447](https://access.redhat.com/errata/RHSA-2021:3447) | <div class="adv_s">Security Advisory</div>
kernel-debug-modules-extra | 4.18.0-305.19.1.el8_4 | [RHSA-2021:3548](https://access.redhat.com/errata/RHSA-2021:3548) | <div class="adv_s">Security Advisory</div>
kernel-devel | 4.18.0-305.17.1.el8_4 | [RHSA-2021:3447](https://access.redhat.com/errata/RHSA-2021:3447) | <div class="adv_s">Security Advisory</div>
kernel-devel | 4.18.0-305.19.1.el8_4 | [RHSA-2021:3548](https://access.redhat.com/errata/RHSA-2021:3548) | <div class="adv_s">Security Advisory</div>
kernel-doc | 4.18.0-305.17.1.el8_4 | [RHSA-2021:3447](https://access.redhat.com/errata/RHSA-2021:3447) | <div class="adv_s">Security Advisory</div>
kernel-doc | 4.18.0-305.19.1.el8_4 | [RHSA-2021:3548](https://access.redhat.com/errata/RHSA-2021:3548) | <div class="adv_s">Security Advisory</div>
kernel-headers | 4.18.0-305.17.1.el8_4 | [RHSA-2021:3447](https://access.redhat.com/errata/RHSA-2021:3447) | <div class="adv_s">Security Advisory</div>
kernel-headers | 4.18.0-305.19.1.el8_4 | [RHSA-2021:3548](https://access.redhat.com/errata/RHSA-2021:3548) | <div class="adv_s">Security Advisory</div>
kernel-modules | 4.18.0-305.17.1.el8_4 | [RHSA-2021:3447](https://access.redhat.com/errata/RHSA-2021:3447) | <div class="adv_s">Security Advisory</div>
kernel-modules | 4.18.0-305.19.1.el8_4 | [RHSA-2021:3548](https://access.redhat.com/errata/RHSA-2021:3548) | <div class="adv_s">Security Advisory</div>
kernel-modules-extra | 4.18.0-305.17.1.el8_4 | [RHSA-2021:3447](https://access.redhat.com/errata/RHSA-2021:3447) | <div class="adv_s">Security Advisory</div>
kernel-modules-extra | 4.18.0-305.19.1.el8_4 | [RHSA-2021:3548](https://access.redhat.com/errata/RHSA-2021:3548) | <div class="adv_s">Security Advisory</div>
kernel-tools | 4.18.0-305.17.1.el8_4 | [RHSA-2021:3447](https://access.redhat.com/errata/RHSA-2021:3447) | <div class="adv_s">Security Advisory</div>
kernel-tools | 4.18.0-305.19.1.el8_4 | [RHSA-2021:3548](https://access.redhat.com/errata/RHSA-2021:3548) | <div class="adv_s">Security Advisory</div>
kernel-tools-libs | 4.18.0-305.17.1.el8_4 | [RHSA-2021:3447](https://access.redhat.com/errata/RHSA-2021:3447) | <div class="adv_s">Security Advisory</div>
kernel-tools-libs | 4.18.0-305.19.1.el8_4 | [RHSA-2021:3548](https://access.redhat.com/errata/RHSA-2021:3548) | <div class="adv_s">Security Advisory</div>
libdb | 5.3.28-42.el8_4 | |
libdb-utils | 5.3.28-42.el8_4 | |
lvm2 | 2.03.12-8.el8 | |
lvm2-dbusd | 2.03.12-8.el8 | |
lvm2-libs | 2.03.12-8.el8 | |
lvm2-lockd | 2.03.12-8.el8 | |
perf | 4.18.0-305.17.1.el8_4 | [RHSA-2021:3447](https://access.redhat.com/errata/RHSA-2021:3447) | <div class="adv_s">Security Advisory</div>
perf | 4.18.0-305.19.1.el8_4 | [RHSA-2021:3548](https://access.redhat.com/errata/RHSA-2021:3548) | <div class="adv_s">Security Advisory</div>
platform-python | 3.6.8-41.el8 | |
python3-cloud-what | 1.28.21-3.el8 | |
python3-libs | 3.6.8-41.el8 | |
python3-perf | 4.18.0-305.17.1.el8_4 | [RHSA-2021:3447](https://access.redhat.com/errata/RHSA-2021:3447) | <div class="adv_s">Security Advisory</div>
python3-perf | 4.18.0-305.19.1.el8_4 | [RHSA-2021:3548](https://access.redhat.com/errata/RHSA-2021:3548) | <div class="adv_s">Security Advisory</div>
python3-subscription-manager-rhsm | 1.28.21-3.el8 | |
python3-syspurpose | 1.28.21-3.el8 | |
python3-test | 3.6.8-41.el8 | |
rhsm-icons | 1.28.21-3.el8 | |
subscription-manager | 1.28.21-3.el8 | |
subscription-manager-cockpit | 1.28.21-3.el8 | |
subscription-manager-plugin-ostree | 1.28.21-3.el8 | |
subscription-manager-rhsm-certificates | 1.28.21-3.el8 | |

### AppStream aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
cockpit-machines | 251.1-1.el8 | |
cockpit-packagekit | 251.1-1.el8 | |
cockpit-pcp | 251.1-1.el8 | |
cockpit-storaged | 251.1-1.el8 | |
firefox | 78.14.0-1.el8_4 | [RHSA-2021:3497](https://access.redhat.com/errata/RHSA-2021:3497) | <div class="adv_s">Security Advisory</div>
libdb-devel | 5.3.28-42.el8_4 | |
osinfo-db | 20210903-1.el8 | |
platform-python-debug | 3.6.8-41.el8 | |
platform-python-devel | 3.6.8-41.el8 | |
pykickstart | 3.16.14-1.el8 | |
python3-idle | 3.6.8-41.el8 | |
python3-kickstart | 3.16.14-1.el8 | |
python3-tkinter | 3.6.8-41.el8 | |
spirv-tools | 2021.3-1.20210825.git1fbed83.el8 | |
spirv-tools-libs | 2021.3-1.20210825.git1fbed83.el8 | |
subscription-manager-migration | 1.28.21-3.el8 | |
thunderbird | 78.14.0-1.el8_4 | [RHSA-2021:3499](https://access.redhat.com/errata/RHSA-2021:3499) | <div class="adv_s">Security Advisory</div>
vulkan-headers | 1.2.189.0-1.el8 | |
vulkan-loader | 1.2.189.0-1.el8 | |
vulkan-loader-devel | 1.2.189.0-1.el8 | |
vulkan-tools | 1.2.189.0-1.el8 | |
vulkan-validation-layers | 1.2.189.0-2.el8 | |

### PowerTools aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
device-mapper-devel | 1.02.177-8.el8 | |
device-mapper-event-devel | 1.02.177-8.el8 | |
kernel-tools-libs-devel | 4.18.0-305.17.1.el8_4 | [RHSA-2021:3447](https://access.redhat.com/errata/RHSA-2021:3447) | <div class="adv_s">Security Advisory</div>
kernel-tools-libs-devel | 4.18.0-305.19.1.el8_4 | [RHSA-2021:3548](https://access.redhat.com/errata/RHSA-2021:3548) | <div class="adv_s">Security Advisory</div>
libdb-cxx | 5.3.28-42.el8_4 | |
libdb-cxx-devel | 5.3.28-42.el8_4 | |
libdb-devel-doc | 5.3.28-42.el8_4 | |
libdb-sql | 5.3.28-42.el8_4 | |
libdb-sql-devel | 5.3.28-42.el8_4 | |
lvm2-devel | 2.03.12-8.el8 | |
spirv-tools-devel | 2021.3-1.20210825.git1fbed83.el8 | |

### HighAvailability aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
pcs | 0.10.10-3.el8 | |
pcs-snmp | 0.10.10-3.el8 | |

### cloud aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
openstack-glance | 20.2.0-1.el8 | |
openstack-glance | 21.1.0-1.el8 | |
openstack-glance | 22.1.0-1.el8 | |
openstack-glance-doc | 20.2.0-1.el8 | |
openstack-glance-doc | 21.1.0-1.el8 | |
openstack-glance-doc | 22.1.0-1.el8 | |
python3-glance | 20.2.0-1.el8 | |
python3-glance | 21.1.0-1.el8 | |
python3-glance | 22.1.0-1.el8 | |
python3-glance-tests | 20.2.0-1.el8 | |
python3-glance-tests | 21.1.0-1.el8 | |
python3-glance-tests | 22.1.0-1.el8 | |

