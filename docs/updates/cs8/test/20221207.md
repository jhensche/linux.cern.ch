## 2022-12-07

### virt x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
ovirt-hosted-engine-setup | 2.7.0-1.el8 | |

### virt aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
ovirt-hosted-engine-setup | 2.7.0-1.el8 | |

