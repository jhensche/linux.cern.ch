## 2021-06-29

### CERN x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
cern-krb5-conf | 1.3-7.el8s.cern | |
cern-krb5-conf-atlas | 1.3-7.el8s.cern | |
cern-krb5-conf-cms | 1.3-7.el8s.cern | |
cern-krb5-conf-defaults-cernch | 1.3-7.el8s.cern | |
cern-krb5-conf-defaults-ipadev | 1.3-7.el8s.cern | |
cern-krb5-conf-ipadev | 1.3-7.el8s.cern | |
cern-krb5-conf-realm-cernch | 1.3-7.el8s.cern | |
cern-krb5-conf-realm-cernch-atlas | 1.3-7.el8s.cern | |
cern-krb5-conf-realm-cernch-cms | 1.3-7.el8s.cern | |
cern-krb5-conf-realm-cernch-tn | 1.3-7.el8s.cern | |
cern-krb5-conf-realm-ipadev | 1.3-7.el8s.cern | |
cern-krb5-conf-tn | 1.3-7.el8s.cern | |

### CERN aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
cern-krb5-conf | 1.3-7.el8s.cern | |
cern-krb5-conf-atlas | 1.3-7.el8s.cern | |
cern-krb5-conf-cms | 1.3-7.el8s.cern | |
cern-krb5-conf-defaults-cernch | 1.3-7.el8s.cern | |
cern-krb5-conf-defaults-ipadev | 1.3-7.el8s.cern | |
cern-krb5-conf-ipadev | 1.3-7.el8s.cern | |
cern-krb5-conf-realm-cernch | 1.3-7.el8s.cern | |
cern-krb5-conf-realm-cernch-atlas | 1.3-7.el8s.cern | |
cern-krb5-conf-realm-cernch-cms | 1.3-7.el8s.cern | |
cern-krb5-conf-realm-cernch-tn | 1.3-7.el8s.cern | |
cern-krb5-conf-realm-ipadev | 1.3-7.el8s.cern | |
cern-krb5-conf-tn | 1.3-7.el8s.cern | |

