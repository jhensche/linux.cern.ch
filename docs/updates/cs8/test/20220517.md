## 2022-05-17

### cloud x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
ansible-collections-openstack | 1.8.0-1.el8 | |
python-sushy-doc | 3.12.1-1.el8 | |
python3-sushy | 3.12.1-1.el8 | |
python3-sushy-tests | 3.12.1-1.el8 | |

### cloud aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
ansible-collections-openstack | 1.8.0-1.el8 | |
python-sushy-doc | 3.12.1-1.el8 | |
python3-sushy | 3.12.1-1.el8 | |
python3-sushy-tests | 3.12.1-1.el8 | |

