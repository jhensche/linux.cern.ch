## 2023-04-12

### openafs x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
kmod-openafs | 1.8.9.0-2.4.18.0_485.el8.el8s.cern | |
openafs-debugsource | 1.8.9.0_4.18.0_485.el8-2.el8s.cern | |

### BaseOS x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
bpftool | 4.18.0-485.el8 | |
kernel | 4.18.0-485.el8 | |
kernel-abi-stablelists | 4.18.0-485.el8 | |
kernel-core | 4.18.0-485.el8 | |
kernel-cross-headers | 4.18.0-485.el8 | |
kernel-debug | 4.18.0-485.el8 | |
kernel-debug-core | 4.18.0-485.el8 | |
kernel-debug-devel | 4.18.0-485.el8 | |
kernel-debug-modules | 4.18.0-485.el8 | |
kernel-debug-modules-extra | 4.18.0-485.el8 | |
kernel-devel | 4.18.0-485.el8 | |
kernel-doc | 4.18.0-485.el8 | |
kernel-headers | 4.18.0-485.el8 | |
kernel-modules | 4.18.0-485.el8 | |
kernel-modules-extra | 4.18.0-485.el8 | |
kernel-tools | 4.18.0-485.el8 | |
kernel-tools-libs | 4.18.0-485.el8 | |
perf | 4.18.0-485.el8 | |
python3-perf | 4.18.0-485.el8 | |

### AppStream x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
cockpit-machines | 286.1-1.el8 | |
cockpit-packagekit | 286.1-1.el8 | |
cockpit-pcp | 286.1-1.el8 | |
cockpit-storaged | 286.1-1.el8 | |
edk2-ovmf | 20220126gitbb1bba3d77-5.el8 | |
libblockdev | 2.28-3.el8 | |
libblockdev-crypto | 2.28-3.el8 | |
libblockdev-dm | 2.28-3.el8 | |
libblockdev-fs | 2.28-3.el8 | |
libblockdev-kbd | 2.28-3.el8 | |
libblockdev-loop | 2.28-3.el8 | |
libblockdev-lvm | 2.28-3.el8 | |
libblockdev-lvm-dbus | 2.28-3.el8 | |
libblockdev-mdraid | 2.28-3.el8 | |
libblockdev-mpath | 2.28-3.el8 | |
libblockdev-nvdimm | 2.28-3.el8 | |
libblockdev-part | 2.28-3.el8 | |
libblockdev-plugins-all | 2.28-3.el8 | |
libblockdev-swap | 2.28-3.el8 | |
libblockdev-utils | 2.28-3.el8 | |
libblockdev-vdo | 2.28-3.el8 | |
mpich | 3.4.2-2.el8 | |
mpich-devel | 3.4.2-2.el8 | |
mpich-doc | 3.4.2-2.el8 | |
osbuild-composer | 79-1.el8 | |
osbuild-composer-core | 79-1.el8 | |
osbuild-composer-dnf-json | 79-1.el8 | |
osbuild-composer-worker | 79-1.el8 | |
python3-blockdev | 2.28-3.el8 | |

### PowerTools x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
kernel-tools-libs-devel | 4.18.0-485.el8 | |
libblockdev-crypto-devel | 2.28-3.el8 | |
libblockdev-devel | 2.28-3.el8 | |
libblockdev-fs-devel | 2.28-3.el8 | |
libblockdev-loop-devel | 2.28-3.el8 | |
libblockdev-lvm-devel | 2.28-3.el8 | |
libblockdev-mdraid-devel | 2.28-3.el8 | |
libblockdev-part-devel | 2.28-3.el8 | |
libblockdev-swap-devel | 2.28-3.el8 | |
libblockdev-utils-devel | 2.28-3.el8 | |
libblockdev-vdo-devel | 2.28-3.el8 | |
python3-mpich | 3.4.2-2.el8 | |

### RT x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
kernel-rt | 4.18.0-484.rt7.273.el8 | |
kernel-rt-core | 4.18.0-484.rt7.273.el8 | |
kernel-rt-debug | 4.18.0-484.rt7.273.el8 | |
kernel-rt-debug-core | 4.18.0-484.rt7.273.el8 | |
kernel-rt-debug-devel | 4.18.0-484.rt7.273.el8 | |
kernel-rt-debug-modules | 4.18.0-484.rt7.273.el8 | |
kernel-rt-debug-modules-extra | 4.18.0-484.rt7.273.el8 | |
kernel-rt-devel | 4.18.0-484.rt7.273.el8 | |
kernel-rt-modules | 4.18.0-484.rt7.273.el8 | |
kernel-rt-modules-extra | 4.18.0-484.rt7.273.el8 | |

### openafs aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
kmod-openafs | 1.8.9.0-2.4.18.0_485.el8.el8s.cern | |
openafs-debugsource | 1.8.9.0_4.18.0_485.el8-2.el8s.cern | |

### BaseOS aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
bpftool | 4.18.0-485.el8 | |
kernel | 4.18.0-485.el8 | |
kernel-abi-stablelists | 4.18.0-485.el8 | |
kernel-core | 4.18.0-485.el8 | |
kernel-cross-headers | 4.18.0-485.el8 | |
kernel-debug | 4.18.0-485.el8 | |
kernel-debug-core | 4.18.0-485.el8 | |
kernel-debug-devel | 4.18.0-485.el8 | |
kernel-debug-modules | 4.18.0-485.el8 | |
kernel-debug-modules-extra | 4.18.0-485.el8 | |
kernel-devel | 4.18.0-485.el8 | |
kernel-doc | 4.18.0-485.el8 | |
kernel-headers | 4.18.0-485.el8 | |
kernel-modules | 4.18.0-485.el8 | |
kernel-modules-extra | 4.18.0-485.el8 | |
kernel-tools | 4.18.0-485.el8 | |
kernel-tools-libs | 4.18.0-485.el8 | |
perf | 4.18.0-485.el8 | |
python3-perf | 4.18.0-485.el8 | |

### AppStream aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
cockpit-machines | 286.1-1.el8 | |
cockpit-packagekit | 286.1-1.el8 | |
cockpit-pcp | 286.1-1.el8 | |
cockpit-storaged | 286.1-1.el8 | |
edk2-aarch64 | 20220126gitbb1bba3d77-5.el8 | |
libblockdev | 2.28-3.el8 | |
libblockdev-crypto | 2.28-3.el8 | |
libblockdev-dm | 2.28-3.el8 | |
libblockdev-fs | 2.28-3.el8 | |
libblockdev-kbd | 2.28-3.el8 | |
libblockdev-loop | 2.28-3.el8 | |
libblockdev-lvm | 2.28-3.el8 | |
libblockdev-lvm-dbus | 2.28-3.el8 | |
libblockdev-mdraid | 2.28-3.el8 | |
libblockdev-mpath | 2.28-3.el8 | |
libblockdev-nvdimm | 2.28-3.el8 | |
libblockdev-part | 2.28-3.el8 | |
libblockdev-plugins-all | 2.28-3.el8 | |
libblockdev-swap | 2.28-3.el8 | |
libblockdev-utils | 2.28-3.el8 | |
libblockdev-vdo | 2.28-3.el8 | |
mpich | 3.4.2-2.el8 | |
mpich-devel | 3.4.2-2.el8 | |
mpich-doc | 3.4.2-2.el8 | |
osbuild-composer | 79-1.el8 | |
osbuild-composer-core | 79-1.el8 | |
osbuild-composer-dnf-json | 79-1.el8 | |
osbuild-composer-worker | 79-1.el8 | |
python3-blockdev | 2.28-3.el8 | |

### PowerTools aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
kernel-tools-libs-devel | 4.18.0-485.el8 | |
libblockdev-crypto-devel | 2.28-3.el8 | |
libblockdev-devel | 2.28-3.el8 | |
libblockdev-fs-devel | 2.28-3.el8 | |
libblockdev-loop-devel | 2.28-3.el8 | |
libblockdev-lvm-devel | 2.28-3.el8 | |
libblockdev-mdraid-devel | 2.28-3.el8 | |
libblockdev-part-devel | 2.28-3.el8 | |
libblockdev-swap-devel | 2.28-3.el8 | |
libblockdev-utils-devel | 2.28-3.el8 | |
libblockdev-vdo-devel | 2.28-3.el8 | |
python3-mpich | 3.4.2-2.el8 | |

