## 2023-03-21

### cloud x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
python-neutron-lib-doc | 2.20.1-1.el8 | |
python-ovsdbapp-doc | 1.15.3-1.el8 | |
python3-neutron-lib | 2.20.1-1.el8 | |
python3-neutron-lib-tests | 2.20.1-1.el8 | |
python3-ovsdbapp | 1.15.3-1.el8 | |
python3-ovsdbapp-tests | 1.15.3-1.el8 | |

### cloud aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
python-neutron-lib-doc | 2.20.1-1.el8 | |
python-ovsdbapp-doc | 1.15.3-1.el8 | |
python3-neutron-lib | 2.20.1-1.el8 | |
python3-neutron-lib-tests | 2.20.1-1.el8 | |
python3-ovsdbapp | 1.15.3-1.el8 | |
python3-ovsdbapp-tests | 1.15.3-1.el8 | |

