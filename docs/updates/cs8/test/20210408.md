## 2021-04-08

### BaseOS x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
centos-stream-release | 8.5-2.el8 | |
device-mapper-multipath | 0.8.4-11.el8 | |
device-mapper-multipath-libs | 0.8.4-11.el8 | |
kpartx | 0.8.4-11.el8 | |
libdmmp | 0.8.4-11.el8 | |

### AppStream x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
virt-viewer | 9.0-10.el8 | |

### PowerTools x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
device-mapper-multipath-devel | 0.8.4-11.el8 | |

### BaseOS aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
centos-stream-release | 8.5-2.el8 | |
device-mapper-multipath | 0.8.4-11.el8 | |
device-mapper-multipath-libs | 0.8.4-11.el8 | |
kpartx | 0.8.4-11.el8 | |
libdmmp | 0.8.4-11.el8 | |

### AppStream aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
virt-viewer | 9.0-10.el8 | |

### PowerTools aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
device-mapper-multipath-devel | 0.8.4-11.el8 | |

