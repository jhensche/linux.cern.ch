## 2022-09-07

### CERN x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
openafs-release | 1.3-1.el8s.cern | |

### CERN aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
openafs-release | 1.3-1.el8s.cern | |

