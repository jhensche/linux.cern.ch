## 2021-11-02

### cloud x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
openstack-keystone | 20.0.0-2.el8 | |
openstack-keystone-doc | 20.0.0-2.el8 | |
python3-keystone | 20.0.0-2.el8 | |
python3-keystone-tests | 20.0.0-2.el8 | |

### cloud aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
openstack-keystone | 20.0.0-2.el8 | |
openstack-keystone-doc | 20.0.0-2.el8 | |
python3-keystone | 20.0.0-2.el8 | |
python3-keystone-tests | 20.0.0-2.el8 | |

