## 2021-05-11

### CERN x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
centos-gpg-keys | 8-2.2.el8s.cern | |
centos-linux-repos | 8-2.2.el8s.cern | |
centos-stream-repos | 8-2.2.el8s.cern | |

### cloud x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
ansible-collection-ansible-netcommon | 1.5.0-1.el8 | |
ansible-collection-ansible-posix | 1.2.0-1.el8 | |
ansible-collection-community-general | 2.5.1-1.el8 | |
ansible-collection-containers-podman | 1.4.1-2.el8 | |
ansible-config_template | 1.1.1-1.el8 | |
ansible-pacemaker | 1.0.4-0.1.5847167git.el8 | |
ansible-role-atos-hsm | 1.0.0-1.el8 | |
ansible-role-chrony | 1.0.3-1.el8 | |
ansible-role-container-registry | 1.3.0-1.el8 | |
ansible-role-lunasa-hsm | 1.1.0-1.el8 | |
ansible-role-metalsmith-deployment | 1.4.2-1.el8 | |
ansible-role-openstack-operations | 0.0.1-0.1.09e0e81git.el8 | |
ansible-role-tripleo-modify-image | 1.2.1-1.el8 | |
ansible-tripleo-ipa | 0.2.2-1.el8 | |
ansible-tripleo-ipsec | 10.0.1-1.el8 | |
openstack-tripleo-common | 15.1.0-1.el8 | |
openstack-tripleo-common-container-base | 15.1.0-1.el8 | |
openstack-tripleo-common-containers | 15.1.0-1.el8 | |
openstack-tripleo-common-devtools | 15.1.0-1.el8 | |
openstack-tripleo-image-elements | 13.1.0-1.el8 | |
openstack-tripleo-puppet-elements | 14.1.0-1.el8 | |
openstack-tripleo-validations | 14.1.0-1.el8 | |
openstack-tripleo-validations-doc | 14.1.0-1.el8 | |
openstack-tripleo-validations-tests | 14.1.0-1.el8 | |
os-net-config | 14.1.0-1.el8 | |
puppet-tripleo | 14.1.0-1.el8 | |
python-metalsmith-doc | 1.4.2-1.el8 | |
python3-metalsmith | 1.4.2-1.el8 | |
python3-metalsmith-tests | 1.4.2-1.el8 | |
python3-tripleo-common | 15.1.0-1.el8 | |
python3-validations-libs | 1.1.0-1.el8 | |
validations-common | 1.1.1-1.el8 | |

### virt x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
hivex | 1.3.18-21.el8s | |
hivex-devel | 1.3.18-21.el8s | |
libvirt | 7.0.0-14.el8s | |
libvirt-admin | 7.0.0-14.el8s | |
libvirt-bash-completion | 7.0.0-14.el8s | |
libvirt-client | 7.0.0-14.el8s | |
libvirt-daemon | 7.0.0-14.el8s | |
libvirt-daemon-config-network | 7.0.0-14.el8s | |
libvirt-daemon-config-nwfilter | 7.0.0-14.el8s | |
libvirt-daemon-driver-interface | 7.0.0-14.el8s | |
libvirt-daemon-driver-network | 7.0.0-14.el8s | |
libvirt-daemon-driver-nodedev | 7.0.0-14.el8s | |
libvirt-daemon-driver-nwfilter | 7.0.0-14.el8s | |
libvirt-daemon-driver-qemu | 7.0.0-14.el8s | |
libvirt-daemon-driver-secret | 7.0.0-14.el8s | |
libvirt-daemon-driver-storage | 7.0.0-14.el8s | |
libvirt-daemon-driver-storage-core | 7.0.0-14.el8s | |
libvirt-daemon-driver-storage-disk | 7.0.0-14.el8s | |
libvirt-daemon-driver-storage-gluster | 7.0.0-14.el8s | |
libvirt-daemon-driver-storage-iscsi | 7.0.0-14.el8s | |
libvirt-daemon-driver-storage-iscsi-direct | 7.0.0-14.el8s | |
libvirt-daemon-driver-storage-logical | 7.0.0-14.el8s | |
libvirt-daemon-driver-storage-mpath | 7.0.0-14.el8s | |
libvirt-daemon-driver-storage-rbd | 7.0.0-14.el8s | |
libvirt-daemon-driver-storage-scsi | 7.0.0-14.el8s | |
libvirt-daemon-kvm | 7.0.0-14.el8s | |
libvirt-devel | 7.0.0-14.el8s | |
libvirt-docs | 7.0.0-14.el8s | |
libvirt-libs | 7.0.0-14.el8s | |
libvirt-lock-sanlock | 7.0.0-14.el8s | |
libvirt-nss | 7.0.0-14.el8s | |
libvirt-wireshark | 7.0.0-14.el8s | |
ocaml-hivex | 1.3.18-21.el8s | |
ocaml-hivex-devel | 1.3.18-21.el8s | |
perl-hivex | 1.3.18-21.el8s | |
python3-hivex | 1.3.18-21.el8s | |
ruby-hivex | 1.3.18-21.el8s | |

### CERN aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
centos-gpg-keys | 8-2.2.el8s.cern | |
centos-linux-repos | 8-2.2.el8s.cern | |
centos-stream-repos | 8-2.2.el8s.cern | |

### cloud aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
ansible-collection-ansible-netcommon | 1.5.0-1.el8 | |
ansible-collection-ansible-posix | 1.2.0-1.el8 | |
ansible-collection-community-general | 2.5.1-1.el8 | |
ansible-collection-containers-podman | 1.4.1-2.el8 | |
ansible-config_template | 1.1.1-1.el8 | |
ansible-pacemaker | 1.0.4-0.1.5847167git.el8 | |
ansible-role-atos-hsm | 1.0.0-1.el8 | |
ansible-role-chrony | 1.0.3-1.el8 | |
ansible-role-container-registry | 1.3.0-1.el8 | |
ansible-role-lunasa-hsm | 1.1.0-1.el8 | |
ansible-role-metalsmith-deployment | 1.4.2-1.el8 | |
ansible-role-openstack-operations | 0.0.1-0.1.09e0e81git.el8 | |
ansible-role-tripleo-modify-image | 1.2.1-1.el8 | |
ansible-tripleo-ipa | 0.2.2-1.el8 | |
ansible-tripleo-ipsec | 10.0.1-1.el8 | |
openstack-tripleo-common | 15.1.0-1.el8 | |
openstack-tripleo-common-container-base | 15.1.0-1.el8 | |
openstack-tripleo-common-containers | 15.1.0-1.el8 | |
openstack-tripleo-common-devtools | 15.1.0-1.el8 | |
openstack-tripleo-image-elements | 13.1.0-1.el8 | |
openstack-tripleo-puppet-elements | 14.1.0-1.el8 | |
openstack-tripleo-validations | 14.1.0-1.el8 | |
openstack-tripleo-validations-doc | 14.1.0-1.el8 | |
openstack-tripleo-validations-tests | 14.1.0-1.el8 | |
os-net-config | 14.1.0-1.el8 | |
puppet-tripleo | 14.1.0-1.el8 | |
python-metalsmith-doc | 1.4.2-1.el8 | |
python3-metalsmith | 1.4.2-1.el8 | |
python3-metalsmith-tests | 1.4.2-1.el8 | |
python3-tripleo-common | 15.1.0-1.el8 | |
python3-validations-libs | 1.1.0-1.el8 | |
validations-common | 1.1.1-1.el8 | |

### virt aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
hivex | 1.3.18-21.el8s | |
hivex-devel | 1.3.18-21.el8s | |
libvirt | 7.0.0-14.el8s | |
libvirt-admin | 7.0.0-14.el8s | |
libvirt-bash-completion | 7.0.0-14.el8s | |
libvirt-client | 7.0.0-14.el8s | |
libvirt-daemon | 7.0.0-14.el8s | |
libvirt-daemon-config-network | 7.0.0-14.el8s | |
libvirt-daemon-config-nwfilter | 7.0.0-14.el8s | |
libvirt-daemon-driver-interface | 7.0.0-14.el8s | |
libvirt-daemon-driver-network | 7.0.0-14.el8s | |
libvirt-daemon-driver-nodedev | 7.0.0-14.el8s | |
libvirt-daemon-driver-nwfilter | 7.0.0-14.el8s | |
libvirt-daemon-driver-qemu | 7.0.0-14.el8s | |
libvirt-daemon-driver-secret | 7.0.0-14.el8s | |
libvirt-daemon-driver-storage | 7.0.0-14.el8s | |
libvirt-daemon-driver-storage-core | 7.0.0-14.el8s | |
libvirt-daemon-driver-storage-disk | 7.0.0-14.el8s | |
libvirt-daemon-driver-storage-gluster | 7.0.0-14.el8s | |
libvirt-daemon-driver-storage-iscsi | 7.0.0-14.el8s | |
libvirt-daemon-driver-storage-iscsi-direct | 7.0.0-14.el8s | |
libvirt-daemon-driver-storage-logical | 7.0.0-14.el8s | |
libvirt-daemon-driver-storage-mpath | 7.0.0-14.el8s | |
libvirt-daemon-driver-storage-rbd | 7.0.0-14.el8s | |
libvirt-daemon-driver-storage-scsi | 7.0.0-14.el8s | |
libvirt-daemon-kvm | 7.0.0-14.el8s | |
libvirt-devel | 7.0.0-14.el8s | |
libvirt-docs | 7.0.0-14.el8s | |
libvirt-libs | 7.0.0-14.el8s | |
libvirt-lock-sanlock | 7.0.0-14.el8s | |
libvirt-nss | 7.0.0-14.el8s | |
libvirt-wireshark | 7.0.0-14.el8s | |
ocaml-hivex | 1.3.18-21.el8s | |
ocaml-hivex-devel | 1.3.18-21.el8s | |
perl-hivex | 1.3.18-21.el8s | |
python3-hivex | 1.3.18-21.el8s | |
ruby-hivex | 1.3.18-21.el8s | |

