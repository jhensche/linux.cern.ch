## 2021-10-29

### BaseOS x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
bpftool | 5.14.0-9.el9 | |
groff-base | 1.22.4-10.el9 | |
ibacm | 37.1-1.el9 | |
iwl100-firmware | 39.31.5.1-122.el9 | |
iwl1000-firmware | 39.31.5.1-122.el9 | |
iwl105-firmware | 18.168.6.1-122.el9 | |
iwl135-firmware | 18.168.6.1-122.el9 | |
iwl2000-firmware | 18.168.6.1-122.el9 | |
iwl2030-firmware | 18.168.6.1-122.el9 | |
iwl3160-firmware | 25.30.13.0-122.el9 | |
iwl5000-firmware | 8.83.5.1_1-122.el9 | |
iwl5150-firmware | 8.24.2.2-122.el9 | |
iwl6000g2a-firmware | 18.168.6.1-122.el9 | |
iwl6000g2b-firmware | 18.168.6.1-122.el9 | |
iwl6050-firmware | 41.28.5.1-122.el9 | |
iwl7260-firmware | 25.30.13.0-122.el9 | |
iwpmd | 37.1-1.el9 | |
kernel | 5.14.0-9.el9 | |
kernel-abi-stablelists | 5.14.0-9.el9 | |
kernel-core | 5.14.0-9.el9 | |
kernel-debug | 5.14.0-9.el9 | |
kernel-debug-core | 5.14.0-9.el9 | |
kernel-debug-modules | 5.14.0-9.el9 | |
kernel-debug-modules-extra | 5.14.0-9.el9 | |
kernel-modules | 5.14.0-9.el9 | |
kernel-modules-extra | 5.14.0-9.el9 | |
kernel-tools | 5.14.0-9.el9 | |
kernel-tools-libs | 5.14.0-9.el9 | |
krb5-libs | 1.19.1-12.el9 | |
krb5-pkinit | 1.19.1-12.el9 | |
krb5-server | 1.19.1-12.el9 | |
krb5-server-ldap | 1.19.1-12.el9 | |
krb5-workstation | 1.19.1-12.el9 | |
libertas-sd8787-firmware | 20210919-122.el9 | |
libibumad | 37.1-1.el9 | |
libibverbs | 37.1-1.el9 | |
libibverbs-utils | 37.1-1.el9 | |
libkadm5 | 1.19.1-12.el9 | |
librdmacm | 37.1-1.el9 | |
librdmacm-utils | 37.1-1.el9 | |
libselinux | 3.3-1.el9 | |
libselinux-utils | 3.3-1.el9 | |
libsemanage | 3.3-1.el9 | |
libsepol | 3.3-1.el9 | |
libxmlb | 0.3.3-1.el9 | |
linux-firmware | 20210919-122.el9 | |
linux-firmware-whence | 20210919-122.el9 | |
mcstrans | 3.3-1.el9 | |
netronome-firmware | 20210919-122.el9 | |
openssh | 8.7p1-4.el9 | |
openssh-clients | 8.7p1-4.el9 | |
openssh-keycat | 8.7p1-4.el9 | |
openssh-server | 8.7p1-4.el9 | |
policycoreutils | 3.3-1.el9 | |
policycoreutils-newrole | 3.3-1.el9 | |
policycoreutils-restorecond | 3.3-1.el9 | |
python-pip-wheel | 21.2.3-3.el9 | |
python3-perf | 5.14.0-9.el9 | |
rdma-core | 37.1-1.el9 | |
srp_daemon | 37.1-1.el9 | |
tzdata | 2021c-1.el9 | |
vim-filesystem | 8.2.2637-8.el9 | |
vim-minimal | 8.2.2637-8.el9 | |

### AppStream x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
annobin | 10.17-1.el9 | |
annobin-annocheck | 10.17-1.el9 | |
ansible-freeipa | 0.4.0-1.el9 | |
ansible-freeipa-tests | 0.4.0-1.el9 | |
appstream | 0.14.5-1.el9 | |
checkpolicy | 3.3-1.el9 | |
gnome-session | 40.1.1-5.el9 | |
gnome-session-wayland-session | 40.1.1-5.el9 | |
gnome-session-xsession | 40.1.1-5.el9 | |
groff | 1.22.4-10.el9 | |
infiniband-diags | 37.1-1.el9 | |
kernel-debug-devel | 5.14.0-9.el9 | |
kernel-debug-devel-matched | 5.14.0-9.el9 | |
kernel-devel | 5.14.0-9.el9 | |
kernel-devel-matched | 5.14.0-9.el9 | |
kernel-doc | 5.14.0-9.el9 | |
kernel-headers | 5.14.0-9.el9 | |
krb5-devel | 1.19.1-12.el9 | |
libinput | 1.19.2-1.el9 | |
libinput-utils | 1.19.2-1.el9 | |
libselinux-devel | 3.3-1.el9 | |
libselinux-ruby | 3.3-1.el9 | |
libsepol-devel | 3.3-1.el9 | |
nspr | 4.32.0-4.el9 | |
nspr-devel | 4.32.0-4.el9 | |
nss | 3.71.0-2.el9 | |
nss-devel | 3.71.0-2.el9 | |
nss-softokn | 3.71.0-2.el9 | |
nss-softokn-devel | 3.71.0-2.el9 | |
nss-softokn-freebl | 3.71.0-2.el9 | |
nss-softokn-freebl-devel | 3.71.0-2.el9 | |
nss-sysinit | 3.71.0-2.el9 | |
nss-tools | 3.71.0-2.el9 | |
nss-util | 3.71.0-2.el9 | |
nss-util-devel | 3.71.0-2.el9 | |
opa-fm | 10.11.0.2-1.el9 | |
openssh-askpass | 8.7p1-4.el9 | |
pam_ssh_agent_auth | 0.10.4-4.4.el9 | |
perf | 5.14.0-9.el9 | |
pipewire | 0.3.32-3.el9 | |
pipewire-alsa | 0.3.32-3.el9 | |
pipewire-devel | 0.3.32-3.el9 | |
pipewire-gstreamer | 0.3.32-3.el9 | |
pipewire-jack-audio-connection-kit | 0.3.32-3.el9 | |
pipewire-jack-audio-connection-kit-devel | 0.3.32-3.el9 | |
pipewire-libs | 0.3.32-3.el9 | |
pipewire-media-session | 0.3.32-3.el9 | |
pipewire-pulseaudio | 0.3.32-3.el9 | |
pipewire-utils | 0.3.32-3.el9 | |
policycoreutils-dbus | 3.3-1.el9 | |
policycoreutils-devel | 3.3-1.el9 | |
policycoreutils-gui | 3.3-1.el9 | |
policycoreutils-python-utils | 3.3-1.el9 | |
policycoreutils-sandbox | 3.3-1.el9 | |
python3-libselinux | 3.3-1.el9 | |
python3-libsemanage | 3.3-1.el9 | |
python3-pip | 21.2.3-3.el9 | |
python3-policycoreutils | 3.3-1.el9 | |
python3-pyverbs | 37.1-1.el9 | |
rdma-core-devel | 37.1-1.el9 | |
tcsh | 6.22.03-6.el9 | |
tzdata-java | 2021c-1.el9 | |
usbguard | 1.0.0-10.el9 | |
usbguard-dbus | 1.0.0-10.el9 | |
usbguard-notifier | 1.0.0-10.el9 | |
usbguard-selinux | 1.0.0-10.el9 | |
usbguard-tools | 1.0.0-10.el9 | |
vim-common | 8.2.2637-8.el9 | |
vim-enhanced | 8.2.2637-8.el9 | |
vim-X11 | 8.2.2637-8.el9 | |

### RT x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
kernel-rt | 5.14.0-9.rt21.9.el9 | |
kernel-rt-core | 5.14.0-9.rt21.9.el9 | |
kernel-rt-debug | 5.14.0-9.rt21.9.el9 | |
kernel-rt-debug-core | 5.14.0-9.rt21.9.el9 | |
kernel-rt-debug-devel | 5.14.0-9.rt21.9.el9 | |
kernel-rt-debug-modules | 5.14.0-9.rt21.9.el9 | |
kernel-rt-debug-modules-extra | 5.14.0-9.rt21.9.el9 | |
kernel-rt-devel | 5.14.0-9.rt21.9.el9 | |
kernel-rt-modules | 5.14.0-9.rt21.9.el9 | |
kernel-rt-modules-extra | 5.14.0-9.rt21.9.el9 | |

### CRB x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
kernel-cross-headers | 5.14.0-9.el9 | |
kernel-tools-libs-devel | 5.14.0-9.el9 | |
libinput-devel | 1.19.2-1.el9 | |
libsemanage-devel | 3.3-1.el9 | |
libsepol-static | 3.3-1.el9 | |
libxmlb-devel | 0.3.3-1.el9 | |
ruby-doc | 3.0.2-155.el9 | |
rubygem-mysql2-doc | 0.5.3-10.el9 | |
rubygem-pg-doc | 1.2.3-7.el9 | |

### NFV x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
kernel-rt | 5.14.0-9.rt21.9.el9 | |
kernel-rt-core | 5.14.0-9.rt21.9.el9 | |
kernel-rt-debug | 5.14.0-9.rt21.9.el9 | |
kernel-rt-debug-core | 5.14.0-9.rt21.9.el9 | |
kernel-rt-debug-devel | 5.14.0-9.rt21.9.el9 | |
kernel-rt-debug-kvm | 5.14.0-9.rt21.9.el9 | |
kernel-rt-debug-modules | 5.14.0-9.rt21.9.el9 | |
kernel-rt-debug-modules-extra | 5.14.0-9.rt21.9.el9 | |
kernel-rt-devel | 5.14.0-9.rt21.9.el9 | |
kernel-rt-kvm | 5.14.0-9.rt21.9.el9 | |
kernel-rt-modules | 5.14.0-9.rt21.9.el9 | |
kernel-rt-modules-extra | 5.14.0-9.rt21.9.el9 | |

### BaseOS aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
bpftool | 5.14.0-9.el9 | |
groff-base | 1.22.4-10.el9 | |
ibacm | 37.1-1.el9 | |
iwl100-firmware | 39.31.5.1-122.el9 | |
iwl1000-firmware | 39.31.5.1-122.el9 | |
iwl105-firmware | 18.168.6.1-122.el9 | |
iwl135-firmware | 18.168.6.1-122.el9 | |
iwl2000-firmware | 18.168.6.1-122.el9 | |
iwl2030-firmware | 18.168.6.1-122.el9 | |
iwl3160-firmware | 25.30.13.0-122.el9 | |
iwl5000-firmware | 8.83.5.1_1-122.el9 | |
iwl5150-firmware | 8.24.2.2-122.el9 | |
iwl6000g2a-firmware | 18.168.6.1-122.el9 | |
iwl6000g2b-firmware | 18.168.6.1-122.el9 | |
iwl6050-firmware | 41.28.5.1-122.el9 | |
iwl7260-firmware | 25.30.13.0-122.el9 | |
iwpmd | 37.1-1.el9 | |
kernel | 5.14.0-9.el9 | |
kernel-abi-stablelists | 5.14.0-9.el9 | |
kernel-core | 5.14.0-9.el9 | |
kernel-debug | 5.14.0-9.el9 | |
kernel-debug-core | 5.14.0-9.el9 | |
kernel-debug-modules | 5.14.0-9.el9 | |
kernel-debug-modules-extra | 5.14.0-9.el9 | |
kernel-modules | 5.14.0-9.el9 | |
kernel-modules-extra | 5.14.0-9.el9 | |
kernel-tools | 5.14.0-9.el9 | |
kernel-tools-libs | 5.14.0-9.el9 | |
krb5-libs | 1.19.1-12.el9 | |
krb5-pkinit | 1.19.1-12.el9 | |
krb5-server | 1.19.1-12.el9 | |
krb5-server-ldap | 1.19.1-12.el9 | |
krb5-workstation | 1.19.1-12.el9 | |
libertas-sd8787-firmware | 20210919-122.el9 | |
libibumad | 37.1-1.el9 | |
libibverbs | 37.1-1.el9 | |
libibverbs-utils | 37.1-1.el9 | |
libkadm5 | 1.19.1-12.el9 | |
librdmacm | 37.1-1.el9 | |
librdmacm-utils | 37.1-1.el9 | |
libselinux | 3.3-1.el9 | |
libselinux-utils | 3.3-1.el9 | |
libsemanage | 3.3-1.el9 | |
libsepol | 3.3-1.el9 | |
libxmlb | 0.3.3-1.el9 | |
linux-firmware | 20210919-122.el9 | |
linux-firmware-whence | 20210919-122.el9 | |
mcstrans | 3.3-1.el9 | |
netronome-firmware | 20210919-122.el9 | |
openssh | 8.7p1-4.el9 | |
openssh-clients | 8.7p1-4.el9 | |
openssh-keycat | 8.7p1-4.el9 | |
openssh-server | 8.7p1-4.el9 | |
policycoreutils | 3.3-1.el9 | |
policycoreutils-newrole | 3.3-1.el9 | |
policycoreutils-restorecond | 3.3-1.el9 | |
python-pip-wheel | 21.2.3-3.el9 | |
python3-perf | 5.14.0-9.el9 | |
rdma-core | 37.1-1.el9 | |
srp_daemon | 37.1-1.el9 | |
tzdata | 2021c-1.el9 | |
vim-filesystem | 8.2.2637-8.el9 | |
vim-minimal | 8.2.2637-8.el9 | |

### AppStream aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
annobin | 10.17-1.el9 | |
annobin-annocheck | 10.17-1.el9 | |
ansible-freeipa | 0.4.0-1.el9 | |
ansible-freeipa-tests | 0.4.0-1.el9 | |
appstream | 0.14.5-1.el9 | |
checkpolicy | 3.3-1.el9 | |
gnome-session | 40.1.1-5.el9 | |
gnome-session-wayland-session | 40.1.1-5.el9 | |
gnome-session-xsession | 40.1.1-5.el9 | |
groff | 1.22.4-10.el9 | |
infiniband-diags | 37.1-1.el9 | |
kernel-debug-devel | 5.14.0-9.el9 | |
kernel-debug-devel-matched | 5.14.0-9.el9 | |
kernel-devel | 5.14.0-9.el9 | |
kernel-devel-matched | 5.14.0-9.el9 | |
kernel-doc | 5.14.0-9.el9 | |
kernel-headers | 5.14.0-9.el9 | |
krb5-devel | 1.19.1-12.el9 | |
libinput | 1.19.2-1.el9 | |
libinput-utils | 1.19.2-1.el9 | |
libselinux-devel | 3.3-1.el9 | |
libselinux-ruby | 3.3-1.el9 | |
libsepol-devel | 3.3-1.el9 | |
nspr | 4.32.0-4.el9 | |
nspr-devel | 4.32.0-4.el9 | |
nss | 3.71.0-2.el9 | |
nss-devel | 3.71.0-2.el9 | |
nss-softokn | 3.71.0-2.el9 | |
nss-softokn-devel | 3.71.0-2.el9 | |
nss-softokn-freebl | 3.71.0-2.el9 | |
nss-softokn-freebl-devel | 3.71.0-2.el9 | |
nss-sysinit | 3.71.0-2.el9 | |
nss-tools | 3.71.0-2.el9 | |
nss-util | 3.71.0-2.el9 | |
nss-util-devel | 3.71.0-2.el9 | |
openssh-askpass | 8.7p1-4.el9 | |
pam_ssh_agent_auth | 0.10.4-4.4.el9 | |
perf | 5.14.0-9.el9 | |
pipewire | 0.3.32-3.el9 | |
pipewire-alsa | 0.3.32-3.el9 | |
pipewire-devel | 0.3.32-3.el9 | |
pipewire-gstreamer | 0.3.32-3.el9 | |
pipewire-jack-audio-connection-kit | 0.3.32-3.el9 | |
pipewire-jack-audio-connection-kit-devel | 0.3.32-3.el9 | |
pipewire-libs | 0.3.32-3.el9 | |
pipewire-media-session | 0.3.32-3.el9 | |
pipewire-pulseaudio | 0.3.32-3.el9 | |
pipewire-utils | 0.3.32-3.el9 | |
policycoreutils-dbus | 3.3-1.el9 | |
policycoreutils-devel | 3.3-1.el9 | |
policycoreutils-gui | 3.3-1.el9 | |
policycoreutils-python-utils | 3.3-1.el9 | |
policycoreutils-sandbox | 3.3-1.el9 | |
python3-libselinux | 3.3-1.el9 | |
python3-libsemanage | 3.3-1.el9 | |
python3-pip | 21.2.3-3.el9 | |
python3-policycoreutils | 3.3-1.el9 | |
python3-pyverbs | 37.1-1.el9 | |
rdma-core-devel | 37.1-1.el9 | |
tcsh | 6.22.03-6.el9 | |
tzdata-java | 2021c-1.el9 | |
usbguard | 1.0.0-10.el9 | |
usbguard-dbus | 1.0.0-10.el9 | |
usbguard-notifier | 1.0.0-10.el9 | |
usbguard-selinux | 1.0.0-10.el9 | |
usbguard-tools | 1.0.0-10.el9 | |
vim-common | 8.2.2637-8.el9 | |
vim-enhanced | 8.2.2637-8.el9 | |
vim-X11 | 8.2.2637-8.el9 | |

### CRB aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
kernel-cross-headers | 5.14.0-9.el9 | |
kernel-tools-libs-devel | 5.14.0-9.el9 | |
libinput-devel | 1.19.2-1.el9 | |
libsemanage-devel | 3.3-1.el9 | |
libsepol-static | 3.3-1.el9 | |
libxmlb-devel | 0.3.3-1.el9 | |
ruby-doc | 3.0.2-155.el9 | |
rubygem-mysql2-doc | 0.5.3-10.el9 | |
rubygem-pg-doc | 1.2.3-7.el9 | |

