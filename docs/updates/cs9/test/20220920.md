## 2022-09-20

### openafs x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
kmod-openafs | 1.8.8.1-0.5.14.0_165.el9.el9.cern | |
openafs-debugsource | 1.8.8.1_5.14.0_165.el9-0.el9.cern | |

### BaseOS x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
bpftool | 5.14.0-165.el9 | |
bpftool-debuginfo | 5.14.0-165.el9 | |
cockpit | 276.1-1.el9 | |
cockpit-bridge | 276.1-1.el9 | |
cockpit-debuginfo | 276.1-1.el9 | |
cockpit-debugsource | 276.1-1.el9 | |
cockpit-doc | 276.1-1.el9 | |
cockpit-system | 276.1-1.el9 | |
cockpit-ws | 276.1-1.el9 | |
device-mapper-multipath | 0.8.7-13.el9 | |
device-mapper-multipath-debuginfo | 0.8.7-13.el9 | |
device-mapper-multipath-debugsource | 0.8.7-13.el9 | |
device-mapper-multipath-libs | 0.8.7-13.el9 | |
device-mapper-multipath-libs-debuginfo | 0.8.7-13.el9 | |
dnf | 4.12.0-4.el9 | |
dnf-automatic | 4.12.0-4.el9 | |
dnf-data | 4.12.0-4.el9 | |
dnf-plugins-core | 4.1.0-3.el9 | |
kernel | 5.14.0-165.el9 | |
kernel-abi-stablelists | 5.14.0-165.el9 | |
kernel-core | 5.14.0-165.el9 | |
kernel-debug | 5.14.0-165.el9 | |
kernel-debug-core | 5.14.0-165.el9 | |
kernel-debug-debuginfo | 5.14.0-165.el9 | |
kernel-debug-modules | 5.14.0-165.el9 | |
kernel-debug-modules-extra | 5.14.0-165.el9 | |
kernel-debuginfo | 5.14.0-165.el9 | |
kernel-debuginfo-common-x86_64 | 5.14.0-165.el9 | |
kernel-modules | 5.14.0-165.el9 | |
kernel-modules-extra | 5.14.0-165.el9 | |
kernel-tools | 5.14.0-165.el9 | |
kernel-tools-debuginfo | 5.14.0-165.el9 | |
kernel-tools-libs | 5.14.0-165.el9 | |
kmod-kvdo | 8.2.0.18-46.el9_2 | |
kmod-kvdo-debuginfo | 8.2.0.18-46.el9_2 | |
kmod-kvdo-debugsource | 8.2.0.18-46.el9_2 | |
kpartx | 0.8.7-13.el9 | |
kpartx-debuginfo | 0.8.7-13.el9 | |
libdnf | 0.67.0-3.el9 | |
libdnf-debuginfo | 0.67.0-3.el9 | |
libdnf-debugsource | 0.67.0-3.el9 | |
mdadm | 4.2-6.el9 | |
mdadm-debuginfo | 4.2-6.el9 | |
mdadm-debugsource | 4.2-6.el9 | |
python3-dnf | 4.12.0-4.el9 | |
python3-dnf-plugin-post-transaction-actions | 4.1.0-3.el9 | |
python3-dnf-plugin-versionlock | 4.1.0-3.el9 | |
python3-dnf-plugins-core | 4.1.0-3.el9 | |
python3-hawkey | 0.67.0-3.el9 | |
python3-hawkey-debuginfo | 0.67.0-3.el9 | |
python3-libdnf | 0.67.0-3.el9 | |
python3-libdnf-debuginfo | 0.67.0-3.el9 | |
python3-perf | 5.14.0-165.el9 | |
python3-perf-debuginfo | 5.14.0-165.el9 | |
python3-setuptools | 53.0.0-11.el9 | |
python3-setuptools-wheel | 53.0.0-11.el9 | |
yum | 4.12.0-4.el9 | |
yum-utils | 4.1.0-3.el9 | |

### AppStream x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
cockpit-packagekit | 276.1-1.el9 | |
cockpit-pcp | 276.1-1.el9 | |
cockpit-podman | 53-1.el9 | |
cockpit-storaged | 276.1-1.el9 | |
java-1.8.0-openjdk | 1.8.0.345.b01-5.el9 | |
java-1.8.0-openjdk-debuginfo | 1.8.0.345.b01-5.el9 | |
java-1.8.0-openjdk-debugsource | 1.8.0.345.b01-5.el9 | |
java-1.8.0-openjdk-demo | 1.8.0.345.b01-5.el9 | |
java-1.8.0-openjdk-demo-debuginfo | 1.8.0.345.b01-5.el9 | |
java-1.8.0-openjdk-devel | 1.8.0.345.b01-5.el9 | |
java-1.8.0-openjdk-devel-debuginfo | 1.8.0.345.b01-5.el9 | |
java-1.8.0-openjdk-headless | 1.8.0.345.b01-5.el9 | |
java-1.8.0-openjdk-headless-debuginfo | 1.8.0.345.b01-5.el9 | |
java-1.8.0-openjdk-javadoc | 1.8.0.345.b01-5.el9 | |
java-1.8.0-openjdk-javadoc-zip | 1.8.0.345.b01-5.el9 | |
java-1.8.0-openjdk-src | 1.8.0.345.b01-5.el9 | |
kernel-debug-devel | 5.14.0-165.el9 | |
kernel-debug-devel-matched | 5.14.0-165.el9 | |
kernel-devel | 5.14.0-165.el9 | |
kernel-devel-matched | 5.14.0-165.el9 | |
kernel-doc | 5.14.0-165.el9 | |
kernel-headers | 5.14.0-165.el9 | |
man-db-cron | 2.9.3-6.el9 | |
mesa-debuginfo | 22.1.5-2.el9 | |
mesa-dri-drivers | 22.1.5-2.el9 | |
mesa-dri-drivers-debuginfo | 22.1.5-2.el9 | |
mesa-filesystem | 22.1.5-2.el9 | |
mesa-libEGL | 22.1.5-2.el9 | |
mesa-libEGL-debuginfo | 22.1.5-2.el9 | |
mesa-libEGL-devel | 22.1.5-2.el9 | |
mesa-libgbm | 22.1.5-2.el9 | |
mesa-libgbm-debuginfo | 22.1.5-2.el9 | |
mesa-libGL | 22.1.5-2.el9 | |
mesa-libGL-debuginfo | 22.1.5-2.el9 | |
mesa-libGL-devel | 22.1.5-2.el9 | |
mesa-libglapi | 22.1.5-2.el9 | |
mesa-libglapi-debuginfo | 22.1.5-2.el9 | |
mesa-libxatracker | 22.1.5-2.el9 | |
mesa-libxatracker-debuginfo | 22.1.5-2.el9 | |
mesa-vulkan-drivers | 22.1.5-2.el9 | |
mesa-vulkan-drivers-debuginfo | 22.1.5-2.el9 | |
mysql | 8.0.30-3.el9 | |
mysql-common | 8.0.30-3.el9 | |
mysql-debuginfo | 8.0.30-3.el9 | |
mysql-debugsource | 8.0.30-3.el9 | |
mysql-errmsg | 8.0.30-3.el9 | |
mysql-server | 8.0.30-3.el9 | |
mysql-server-debuginfo | 8.0.30-3.el9 | |
perf | 5.14.0-165.el9 | |
perf-debuginfo | 5.14.0-165.el9 | |
python3-dnf-plugin-modulesync | 4.1.0-3.el9 | |
rpm-ostree | 2022.13-1.el9 | |
rpm-ostree-debuginfo | 2022.13-1.el9 | |
rpm-ostree-debugsource | 2022.13-1.el9 | |
rpm-ostree-libs | 2022.13-1.el9 | |
rpm-ostree-libs-debuginfo | 2022.13-1.el9 | |

### RT x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
kernel-rt | 5.14.0-164.rt21.164.el9 | |
kernel-rt-core | 5.14.0-164.rt21.164.el9 | |
kernel-rt-debug | 5.14.0-164.rt21.164.el9 | |
kernel-rt-debug-core | 5.14.0-164.rt21.164.el9 | |
kernel-rt-debug-debuginfo | 5.14.0-164.rt21.164.el9 | |
kernel-rt-debug-devel | 5.14.0-164.rt21.164.el9 | |
kernel-rt-debug-modules | 5.14.0-164.rt21.164.el9 | |
kernel-rt-debug-modules-extra | 5.14.0-164.rt21.164.el9 | |
kernel-rt-debuginfo | 5.14.0-164.rt21.164.el9 | |
kernel-rt-debuginfo-common-x86_64 | 5.14.0-164.rt21.164.el9 | |
kernel-rt-devel | 5.14.0-164.rt21.164.el9 | |
kernel-rt-modules | 5.14.0-164.rt21.164.el9 | |
kernel-rt-modules-extra | 5.14.0-164.rt21.164.el9 | |

### CRB x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
autotrace | 0.31.1-65.el9 | |
autotrace-debuginfo | 0.31.1-65.el9 | |
autotrace-debugsource | 0.31.1-65.el9 | |
device-mapper-multipath-devel | 0.8.7-13.el9 | |
freeradius-mysql | 3.0.21-34.el9 | |
freeradius-mysql-debuginfo | 3.0.21-34.el9 | |
freeradius-perl | 3.0.21-34.el9 | |
freeradius-perl-debuginfo | 3.0.21-34.el9 | |
freeradius-postgresql | 3.0.21-34.el9 | |
freeradius-postgresql-debuginfo | 3.0.21-34.el9 | |
freeradius-rest | 3.0.21-34.el9 | |
freeradius-rest-debuginfo | 3.0.21-34.el9 | |
freeradius-sqlite | 3.0.21-34.el9 | |
freeradius-sqlite-debuginfo | 3.0.21-34.el9 | |
freeradius-unixODBC | 3.0.21-34.el9 | |
freeradius-unixODBC-debuginfo | 3.0.21-34.el9 | |
java-1.8.0-openjdk-demo-fastdebug | 1.8.0.345.b01-5.el9 | |
java-1.8.0-openjdk-demo-fastdebug-debuginfo | 1.8.0.345.b01-5.el9 | |
java-1.8.0-openjdk-demo-slowdebug | 1.8.0.345.b01-5.el9 | |
java-1.8.0-openjdk-demo-slowdebug-debuginfo | 1.8.0.345.b01-5.el9 | |
java-1.8.0-openjdk-devel-fastdebug | 1.8.0.345.b01-5.el9 | |
java-1.8.0-openjdk-devel-fastdebug-debuginfo | 1.8.0.345.b01-5.el9 | |
java-1.8.0-openjdk-devel-slowdebug | 1.8.0.345.b01-5.el9 | |
java-1.8.0-openjdk-devel-slowdebug-debuginfo | 1.8.0.345.b01-5.el9 | |
java-1.8.0-openjdk-fastdebug | 1.8.0.345.b01-5.el9 | |
java-1.8.0-openjdk-fastdebug-debuginfo | 1.8.0.345.b01-5.el9 | |
java-1.8.0-openjdk-headless-fastdebug | 1.8.0.345.b01-5.el9 | |
java-1.8.0-openjdk-headless-fastdebug-debuginfo | 1.8.0.345.b01-5.el9 | |
java-1.8.0-openjdk-headless-slowdebug | 1.8.0.345.b01-5.el9 | |
java-1.8.0-openjdk-headless-slowdebug-debuginfo | 1.8.0.345.b01-5.el9 | |
java-1.8.0-openjdk-slowdebug | 1.8.0.345.b01-5.el9 | |
java-1.8.0-openjdk-slowdebug-debuginfo | 1.8.0.345.b01-5.el9 | |
java-1.8.0-openjdk-src-fastdebug | 1.8.0.345.b01-5.el9 | |
java-1.8.0-openjdk-src-slowdebug | 1.8.0.345.b01-5.el9 | |
kernel-cross-headers | 5.14.0-165.el9 | |
kernel-tools-libs-devel | 5.14.0-165.el9 | |
libdnf-devel | 0.67.0-3.el9 | |
libwebp-tools | 1.2.0-3.el9 | |
libwebp-tools-debuginfo | 1.2.0-3.el9 | |
mesa-libgbm-devel | 22.1.5-2.el9 | |
mesa-libOSMesa | 22.1.5-2.el9 | |
mesa-libOSMesa-debuginfo | 22.1.5-2.el9 | |
mesa-libOSMesa-devel | 22.1.5-2.el9 | |
mysql-devel | 8.0.30-3.el9 | |
mysql-devel-debuginfo | 8.0.30-3.el9 | |
mysql-libs | 8.0.30-3.el9 | |
mysql-libs-debuginfo | 8.0.30-3.el9 | |
mysql-test | 8.0.30-3.el9 | |
mysql-test-debuginfo | 8.0.30-3.el9 | |

### NFV x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
kernel-rt | 5.14.0-164.rt21.164.el9 | |
kernel-rt-core | 5.14.0-164.rt21.164.el9 | |
kernel-rt-debug | 5.14.0-164.rt21.164.el9 | |
kernel-rt-debug-core | 5.14.0-164.rt21.164.el9 | |
kernel-rt-debug-debuginfo | 5.14.0-164.rt21.164.el9 | |
kernel-rt-debug-devel | 5.14.0-164.rt21.164.el9 | |
kernel-rt-debug-kvm | 5.14.0-164.rt21.164.el9 | |
kernel-rt-debug-modules | 5.14.0-164.rt21.164.el9 | |
kernel-rt-debug-modules-extra | 5.14.0-164.rt21.164.el9 | |
kernel-rt-debuginfo | 5.14.0-164.rt21.164.el9 | |
kernel-rt-debuginfo-common-x86_64 | 5.14.0-164.rt21.164.el9 | |
kernel-rt-devel | 5.14.0-164.rt21.164.el9 | |
kernel-rt-kvm | 5.14.0-164.rt21.164.el9 | |
kernel-rt-modules | 5.14.0-164.rt21.164.el9 | |
kernel-rt-modules-extra | 5.14.0-164.rt21.164.el9 | |

### openafs aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
kmod-openafs | 1.8.8.1-0.5.14.0_165.el9.el9.cern | |
openafs-debugsource | 1.8.8.1_5.14.0_165.el9-0.el9.cern | |

### BaseOS aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
bpftool | 5.14.0-165.el9 | |
bpftool-debuginfo | 5.14.0-165.el9 | |
cockpit | 276.1-1.el9 | |
cockpit-bridge | 276.1-1.el9 | |
cockpit-debuginfo | 276.1-1.el9 | |
cockpit-debugsource | 276.1-1.el9 | |
cockpit-doc | 276.1-1.el9 | |
cockpit-system | 276.1-1.el9 | |
cockpit-ws | 276.1-1.el9 | |
device-mapper-multipath | 0.8.7-13.el9 | |
device-mapper-multipath-debuginfo | 0.8.7-13.el9 | |
device-mapper-multipath-debugsource | 0.8.7-13.el9 | |
device-mapper-multipath-libs | 0.8.7-13.el9 | |
device-mapper-multipath-libs-debuginfo | 0.8.7-13.el9 | |
dnf | 4.12.0-4.el9 | |
dnf-automatic | 4.12.0-4.el9 | |
dnf-data | 4.12.0-4.el9 | |
dnf-plugins-core | 4.1.0-3.el9 | |
kernel | 5.14.0-165.el9 | |
kernel-abi-stablelists | 5.14.0-165.el9 | |
kernel-core | 5.14.0-165.el9 | |
kernel-debug | 5.14.0-165.el9 | |
kernel-debug-core | 5.14.0-165.el9 | |
kernel-debug-debuginfo | 5.14.0-165.el9 | |
kernel-debug-modules | 5.14.0-165.el9 | |
kernel-debug-modules-extra | 5.14.0-165.el9 | |
kernel-debuginfo | 5.14.0-165.el9 | |
kernel-debuginfo-common-aarch64 | 5.14.0-165.el9 | |
kernel-modules | 5.14.0-165.el9 | |
kernel-modules-extra | 5.14.0-165.el9 | |
kernel-tools | 5.14.0-165.el9 | |
kernel-tools-debuginfo | 5.14.0-165.el9 | |
kernel-tools-libs | 5.14.0-165.el9 | |
kmod-kvdo | 8.2.0.18-46.el9_2 | |
kmod-kvdo-debuginfo | 8.2.0.18-46.el9_2 | |
kmod-kvdo-debugsource | 8.2.0.18-46.el9_2 | |
kpartx | 0.8.7-13.el9 | |
kpartx-debuginfo | 0.8.7-13.el9 | |
libdnf | 0.67.0-3.el9 | |
libdnf-debuginfo | 0.67.0-3.el9 | |
libdnf-debugsource | 0.67.0-3.el9 | |
mdadm | 4.2-6.el9 | |
mdadm-debuginfo | 4.2-6.el9 | |
mdadm-debugsource | 4.2-6.el9 | |
python3-dnf | 4.12.0-4.el9 | |
python3-dnf-plugin-post-transaction-actions | 4.1.0-3.el9 | |
python3-dnf-plugin-versionlock | 4.1.0-3.el9 | |
python3-dnf-plugins-core | 4.1.0-3.el9 | |
python3-hawkey | 0.67.0-3.el9 | |
python3-hawkey-debuginfo | 0.67.0-3.el9 | |
python3-libdnf | 0.67.0-3.el9 | |
python3-libdnf-debuginfo | 0.67.0-3.el9 | |
python3-perf | 5.14.0-165.el9 | |
python3-perf-debuginfo | 5.14.0-165.el9 | |
python3-setuptools | 53.0.0-11.el9 | |
python3-setuptools-wheel | 53.0.0-11.el9 | |
yum | 4.12.0-4.el9 | |
yum-utils | 4.1.0-3.el9 | |

### AppStream aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
cockpit-packagekit | 276.1-1.el9 | |
cockpit-pcp | 276.1-1.el9 | |
cockpit-podman | 53-1.el9 | |
cockpit-storaged | 276.1-1.el9 | |
java-1.8.0-openjdk | 1.8.0.345.b01-5.el9 | |
java-1.8.0-openjdk-debuginfo | 1.8.0.345.b01-5.el9 | |
java-1.8.0-openjdk-debugsource | 1.8.0.345.b01-5.el9 | |
java-1.8.0-openjdk-demo | 1.8.0.345.b01-5.el9 | |
java-1.8.0-openjdk-demo-debuginfo | 1.8.0.345.b01-5.el9 | |
java-1.8.0-openjdk-devel | 1.8.0.345.b01-5.el9 | |
java-1.8.0-openjdk-devel-debuginfo | 1.8.0.345.b01-5.el9 | |
java-1.8.0-openjdk-headless | 1.8.0.345.b01-5.el9 | |
java-1.8.0-openjdk-headless-debuginfo | 1.8.0.345.b01-5.el9 | |
java-1.8.0-openjdk-javadoc | 1.8.0.345.b01-5.el9 | |
java-1.8.0-openjdk-javadoc-zip | 1.8.0.345.b01-5.el9 | |
java-1.8.0-openjdk-src | 1.8.0.345.b01-5.el9 | |
kernel-debug-devel | 5.14.0-165.el9 | |
kernel-debug-devel-matched | 5.14.0-165.el9 | |
kernel-devel | 5.14.0-165.el9 | |
kernel-devel-matched | 5.14.0-165.el9 | |
kernel-doc | 5.14.0-165.el9 | |
kernel-headers | 5.14.0-165.el9 | |
man-db-cron | 2.9.3-6.el9 | |
mesa-debuginfo | 22.1.5-2.el9 | |
mesa-dri-drivers | 22.1.5-2.el9 | |
mesa-dri-drivers-debuginfo | 22.1.5-2.el9 | |
mesa-filesystem | 22.1.5-2.el9 | |
mesa-libEGL | 22.1.5-2.el9 | |
mesa-libEGL-debuginfo | 22.1.5-2.el9 | |
mesa-libEGL-devel | 22.1.5-2.el9 | |
mesa-libgbm | 22.1.5-2.el9 | |
mesa-libgbm-debuginfo | 22.1.5-2.el9 | |
mesa-libGL | 22.1.5-2.el9 | |
mesa-libGL-debuginfo | 22.1.5-2.el9 | |
mesa-libGL-devel | 22.1.5-2.el9 | |
mesa-libglapi | 22.1.5-2.el9 | |
mesa-libglapi-debuginfo | 22.1.5-2.el9 | |
mysql | 8.0.30-3.el9 | |
mysql-common | 8.0.30-3.el9 | |
mysql-debuginfo | 8.0.30-3.el9 | |
mysql-debugsource | 8.0.30-3.el9 | |
mysql-errmsg | 8.0.30-3.el9 | |
mysql-server | 8.0.30-3.el9 | |
mysql-server-debuginfo | 8.0.30-3.el9 | |
perf | 5.14.0-165.el9 | |
perf-debuginfo | 5.14.0-165.el9 | |
python3-dnf-plugin-modulesync | 4.1.0-3.el9 | |
rpm-ostree | 2022.13-1.el9 | |
rpm-ostree-debuginfo | 2022.13-1.el9 | |
rpm-ostree-debugsource | 2022.13-1.el9 | |
rpm-ostree-libs | 2022.13-1.el9 | |
rpm-ostree-libs-debuginfo | 2022.13-1.el9 | |

### CRB aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
autotrace | 0.31.1-65.el9 | |
autotrace-debuginfo | 0.31.1-65.el9 | |
autotrace-debugsource | 0.31.1-65.el9 | |
device-mapper-multipath-devel | 0.8.7-13.el9 | |
freeradius-mysql | 3.0.21-34.el9 | |
freeradius-mysql-debuginfo | 3.0.21-34.el9 | |
freeradius-perl | 3.0.21-34.el9 | |
freeradius-perl-debuginfo | 3.0.21-34.el9 | |
freeradius-postgresql | 3.0.21-34.el9 | |
freeradius-postgresql-debuginfo | 3.0.21-34.el9 | |
freeradius-rest | 3.0.21-34.el9 | |
freeradius-rest-debuginfo | 3.0.21-34.el9 | |
freeradius-sqlite | 3.0.21-34.el9 | |
freeradius-sqlite-debuginfo | 3.0.21-34.el9 | |
freeradius-unixODBC | 3.0.21-34.el9 | |
freeradius-unixODBC-debuginfo | 3.0.21-34.el9 | |
java-1.8.0-openjdk-demo-fastdebug | 1.8.0.345.b01-5.el9 | |
java-1.8.0-openjdk-demo-fastdebug-debuginfo | 1.8.0.345.b01-5.el9 | |
java-1.8.0-openjdk-demo-slowdebug | 1.8.0.345.b01-5.el9 | |
java-1.8.0-openjdk-demo-slowdebug-debuginfo | 1.8.0.345.b01-5.el9 | |
java-1.8.0-openjdk-devel-fastdebug | 1.8.0.345.b01-5.el9 | |
java-1.8.0-openjdk-devel-fastdebug-debuginfo | 1.8.0.345.b01-5.el9 | |
java-1.8.0-openjdk-devel-slowdebug | 1.8.0.345.b01-5.el9 | |
java-1.8.0-openjdk-devel-slowdebug-debuginfo | 1.8.0.345.b01-5.el9 | |
java-1.8.0-openjdk-fastdebug | 1.8.0.345.b01-5.el9 | |
java-1.8.0-openjdk-fastdebug-debuginfo | 1.8.0.345.b01-5.el9 | |
java-1.8.0-openjdk-headless-fastdebug | 1.8.0.345.b01-5.el9 | |
java-1.8.0-openjdk-headless-fastdebug-debuginfo | 1.8.0.345.b01-5.el9 | |
java-1.8.0-openjdk-headless-slowdebug | 1.8.0.345.b01-5.el9 | |
java-1.8.0-openjdk-headless-slowdebug-debuginfo | 1.8.0.345.b01-5.el9 | |
java-1.8.0-openjdk-slowdebug | 1.8.0.345.b01-5.el9 | |
java-1.8.0-openjdk-slowdebug-debuginfo | 1.8.0.345.b01-5.el9 | |
java-1.8.0-openjdk-src-fastdebug | 1.8.0.345.b01-5.el9 | |
java-1.8.0-openjdk-src-slowdebug | 1.8.0.345.b01-5.el9 | |
kernel-cross-headers | 5.14.0-165.el9 | |
kernel-tools-libs-devel | 5.14.0-165.el9 | |
libdnf-devel | 0.67.0-3.el9 | |
libwebp-tools | 1.2.0-3.el9 | |
libwebp-tools-debuginfo | 1.2.0-3.el9 | |
mesa-libgbm-devel | 22.1.5-2.el9 | |
mesa-libOSMesa | 22.1.5-2.el9 | |
mesa-libOSMesa-debuginfo | 22.1.5-2.el9 | |
mesa-libOSMesa-devel | 22.1.5-2.el9 | |
mysql-devel | 8.0.30-3.el9 | |
mysql-devel-debuginfo | 8.0.30-3.el9 | |
mysql-libs | 8.0.30-3.el9 | |
mysql-libs-debuginfo | 8.0.30-3.el9 | |
mysql-test | 8.0.30-3.el9 | |
mysql-test-debuginfo | 8.0.30-3.el9 | |

