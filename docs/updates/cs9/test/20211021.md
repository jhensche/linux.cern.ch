## 2021-10-21

### BaseOS x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
bpftool | 5.14.0-8.el9 | |
cockpit | 255-1.el9 | |
cockpit-bridge | 255-1.el9 | |
cockpit-doc | 255-1.el9 | |
cockpit-system | 255-1.el9 | |
cockpit-ws | 255-1.el9 | |
gobject-introspection | 1.68.0-10.el9 | |
kernel | 5.14.0-8.el9 | |
kernel-abi-stablelists | 5.14.0-8.el9 | |
kernel-core | 5.14.0-8.el9 | |
kernel-debug | 5.14.0-8.el9 | |
kernel-debug-core | 5.14.0-8.el9 | |
kernel-debug-modules | 5.14.0-8.el9 | |
kernel-debug-modules-extra | 5.14.0-8.el9 | |
kernel-modules | 5.14.0-8.el9 | |
kernel-modules-extra | 5.14.0-8.el9 | |
kernel-tools | 5.14.0-8.el9 | |
kernel-tools-libs | 5.14.0-8.el9 | |
kmod-kvdo | 8.1.0.316-4.el9 | |
libbpf | 0.5.0-2.el9 | |
libgcrypt | 1.9.3-5.el9 | |
mcelog | 179-2.el9 | |
ModemManager | 1.18.2-1.el9 | |
ModemManager-glib | 1.18.2-1.el9 | |
python3-perf | 5.14.0-8.el9 | |
selinux-policy | 34.1.17-1.el9 | |
selinux-policy-doc | 34.1.17-1.el9 | |
selinux-policy-mls | 34.1.17-1.el9 | |
selinux-policy-sandbox | 34.1.17-1.el9 | |
selinux-policy-targeted | 34.1.17-1.el9 | |
vim-filesystem | 8.2.2637-6.el9 | |
vim-minimal | 8.2.2637-6.el9 | |

### AppStream x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
annobin | 10.16-1.el9 | |
annobin-annocheck | 10.16-1.el9 | |
cloud-init | 21.1-10.el9 | |
cockpit-packagekit | 255-1.el9 | |
cockpit-pcp | 255-1.el9 | |
cockpit-storaged | 255-1.el9 | |
daxio | 1.10.1-1.el9 | |
gdm | 40.1-8.el9 | |
gnome-shell | 40.4-4.el9 | |
kernel-debug-devel | 5.14.0-8.el9 | |
kernel-debug-devel-matched | 5.14.0-8.el9 | |
kernel-devel | 5.14.0-8.el9 | |
kernel-devel-matched | 5.14.0-8.el9 | |
kernel-doc | 5.14.0-8.el9 | |
kernel-headers | 5.14.0-8.el9 | |
libgcrypt-devel | 1.9.3-5.el9 | |
libguestfs | 1.46.0-1.el9 | |
libguestfs-appliance | 1.46.0-1.el9 | |
libguestfs-bash-completion | 1.46.0-1.el9 | |
libguestfs-inspect-icons | 1.46.0-1.el9 | |
libguestfs-rescue | 1.46.0-1.el9 | |
libguestfs-rsync | 1.46.0-1.el9 | |
libguestfs-xfs | 1.46.0-1.el9 | |
libical | 3.0.11-1.el9 | |
libical-devel | 3.0.11-1.el9 | |
libical-glib | 3.0.11-1.el9 | |
libical-glib-devel | 3.0.11-1.el9 | |
libpmem | 1.10.1-1.el9 | |
libpmem-debug | 1.10.1-1.el9 | |
libpmem-devel | 1.10.1-1.el9 | |
libpmem2 | 1.10.1-1.el9 | |
libpmem2-debug | 1.10.1-1.el9 | |
libpmem2-devel | 1.10.1-1.el9 | |
libpmemblk | 1.10.1-1.el9 | |
libpmemblk-debug | 1.10.1-1.el9 | |
libpmemblk-devel | 1.10.1-1.el9 | |
libpmemlog | 1.10.1-1.el9 | |
libpmemlog-debug | 1.10.1-1.el9 | |
libpmemlog-devel | 1.10.1-1.el9 | |
libpmemobj | 1.10.1-1.el9 | |
libpmemobj-debug | 1.10.1-1.el9 | |
libpmemobj-devel | 1.10.1-1.el9 | |
libpmempool | 1.10.1-1.el9 | |
libpmempool-debug | 1.10.1-1.el9 | |
libpmempool-devel | 1.10.1-1.el9 | |
libvirt | 7.8.0-1.el9 | |
libvirt-client | 7.8.0-1.el9 | |
libvirt-daemon | 7.8.0-1.el9 | |
libvirt-daemon-config-network | 7.8.0-1.el9 | |
libvirt-daemon-config-nwfilter | 7.8.0-1.el9 | |
libvirt-daemon-driver-interface | 7.8.0-1.el9 | |
libvirt-daemon-driver-network | 7.8.0-1.el9 | |
libvirt-daemon-driver-nodedev | 7.8.0-1.el9 | |
libvirt-daemon-driver-nwfilter | 7.8.0-1.el9 | |
libvirt-daemon-driver-qemu | 7.8.0-1.el9 | |
libvirt-daemon-driver-secret | 7.8.0-1.el9 | |
libvirt-daemon-driver-storage | 7.8.0-1.el9 | |
libvirt-daemon-driver-storage-core | 7.8.0-1.el9 | |
libvirt-daemon-driver-storage-disk | 7.8.0-1.el9 | |
libvirt-daemon-driver-storage-iscsi | 7.8.0-1.el9 | |
libvirt-daemon-driver-storage-logical | 7.8.0-1.el9 | |
libvirt-daemon-driver-storage-mpath | 7.8.0-1.el9 | |
libvirt-daemon-driver-storage-rbd | 7.8.0-1.el9 | |
libvirt-daemon-driver-storage-scsi | 7.8.0-1.el9 | |
libvirt-daemon-kvm | 7.8.0-1.el9 | |
libvirt-libs | 7.8.0-1.el9 | |
libvirt-nss | 7.8.0-1.el9 | |
open-vm-tools | 11.3.5-1.el9 | |
open-vm-tools-desktop | 11.3.5-1.el9 | |
open-vm-tools-sdmp | 11.3.5-1.el9 | |
open-vm-tools-test | 11.3.5-1.el9 | |
perf | 5.14.0-8.el9 | |
perl-Sys-Guestfs | 1.46.0-1.el9 | |
pigz | 2.5-4.el9 | |
pmempool | 1.10.1-1.el9 | |
qt5-qtwayland | 5.15.2-12.el9 | |
qt5-qtwayland-examples | 5.15.2-12.el9 | |
seabios | 1.14.0-7.el9 | |
seabios-bin | 1.14.0-7.el9 | |
seavgabios-bin | 1.14.0-7.el9 | |
selinux-policy-devel | 34.1.17-1.el9 | |
vim-common | 8.2.2637-6.el9 | |
vim-enhanced | 8.2.2637-6.el9 | |
vim-X11 | 8.2.2637-6.el9 | |

### RT x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
kernel-rt | 5.14.0-8.rt21.8.el9 | |
kernel-rt-core | 5.14.0-8.rt21.8.el9 | |
kernel-rt-debug | 5.14.0-8.rt21.8.el9 | |
kernel-rt-debug-core | 5.14.0-8.rt21.8.el9 | |
kernel-rt-debug-devel | 5.14.0-8.rt21.8.el9 | |
kernel-rt-debug-modules | 5.14.0-8.rt21.8.el9 | |
kernel-rt-debug-modules-extra | 5.14.0-8.rt21.8.el9 | |
kernel-rt-devel | 5.14.0-8.rt21.8.el9 | |
kernel-rt-modules | 5.14.0-8.rt21.8.el9 | |
kernel-rt-modules-extra | 5.14.0-8.rt21.8.el9 | |

### CRB x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
gdm-devel | 40.1-8.el9 | |
gdm-pam-extensions-devel | 40.1-8.el9 | |
gobject-introspection-devel | 1.68.0-10.el9 | |
kernel-cross-headers | 5.14.0-8.el9 | |
kernel-tools-libs-devel | 5.14.0-8.el9 | |
libbpf-devel | 0.5.0-2.el9 | |
libbpf-static | 0.5.0-2.el9 | |
libguestfs-devel | 1.46.0-1.el9 | |
libguestfs-gobject | 1.46.0-1.el9 | |
libguestfs-gobject-devel | 1.46.0-1.el9 | |
libguestfs-man-pages-ja | 1.46.0-1.el9 | |
libguestfs-man-pages-uk | 1.46.0-1.el9 | |
libvirt-devel | 7.8.0-1.el9 | |
libvirt-docs | 7.8.0-1.el9 | |
libvirt-lock-sanlock | 7.8.0-1.el9 | |
lua-guestfs | 1.46.0-1.el9 | |
ModemManager-devel | 1.18.2-1.el9 | |
ModemManager-glib-devel | 1.18.2-1.el9 | |
ocaml-libguestfs | 1.46.0-1.el9 | |
ocaml-libguestfs-devel | 1.46.0-1.el9 | |
python3-libguestfs | 1.46.0-1.el9 | |
qt5-qtwayland-devel | 5.15.2-12.el9 | |
ruby-libguestfs | 1.46.0-1.el9 | |
transfig | 3.2.7b-10.el9 | |

### NFV x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
kernel-rt | 5.14.0-8.rt21.8.el9 | |
kernel-rt-core | 5.14.0-8.rt21.8.el9 | |
kernel-rt-debug | 5.14.0-8.rt21.8.el9 | |
kernel-rt-debug-core | 5.14.0-8.rt21.8.el9 | |
kernel-rt-debug-devel | 5.14.0-8.rt21.8.el9 | |
kernel-rt-debug-kvm | 5.14.0-8.rt21.8.el9 | |
kernel-rt-debug-modules | 5.14.0-8.rt21.8.el9 | |
kernel-rt-debug-modules-extra | 5.14.0-8.rt21.8.el9 | |
kernel-rt-devel | 5.14.0-8.rt21.8.el9 | |
kernel-rt-kvm | 5.14.0-8.rt21.8.el9 | |
kernel-rt-modules | 5.14.0-8.rt21.8.el9 | |
kernel-rt-modules-extra | 5.14.0-8.rt21.8.el9 | |

### BaseOS aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
bpftool | 5.14.0-8.el9 | |
cockpit | 255-1.el9 | |
cockpit-bridge | 255-1.el9 | |
cockpit-doc | 255-1.el9 | |
cockpit-system | 255-1.el9 | |
cockpit-ws | 255-1.el9 | |
gobject-introspection | 1.68.0-10.el9 | |
kernel | 5.14.0-8.el9 | |
kernel-abi-stablelists | 5.14.0-8.el9 | |
kernel-core | 5.14.0-8.el9 | |
kernel-debug | 5.14.0-8.el9 | |
kernel-debug-core | 5.14.0-8.el9 | |
kernel-debug-modules | 5.14.0-8.el9 | |
kernel-debug-modules-extra | 5.14.0-8.el9 | |
kernel-modules | 5.14.0-8.el9 | |
kernel-modules-extra | 5.14.0-8.el9 | |
kernel-tools | 5.14.0-8.el9 | |
kernel-tools-libs | 5.14.0-8.el9 | |
kmod-kvdo | 8.1.0.316-4.el9 | |
libbpf | 0.5.0-2.el9 | |
libgcrypt | 1.9.3-5.el9 | |
ModemManager | 1.18.2-1.el9 | |
ModemManager-glib | 1.18.2-1.el9 | |
python3-perf | 5.14.0-8.el9 | |
selinux-policy | 34.1.17-1.el9 | |
selinux-policy-doc | 34.1.17-1.el9 | |
selinux-policy-mls | 34.1.17-1.el9 | |
selinux-policy-sandbox | 34.1.17-1.el9 | |
selinux-policy-targeted | 34.1.17-1.el9 | |
vim-filesystem | 8.2.2637-6.el9 | |
vim-minimal | 8.2.2637-6.el9 | |

### AppStream aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
annobin | 10.16-1.el9 | |
annobin-annocheck | 10.16-1.el9 | |
cloud-init | 21.1-10.el9 | |
cockpit-packagekit | 255-1.el9 | |
cockpit-pcp | 255-1.el9 | |
cockpit-storaged | 255-1.el9 | |
gdm | 40.1-8.el9 | |
gnome-shell | 40.4-4.el9 | |
kernel-debug-devel | 5.14.0-8.el9 | |
kernel-debug-devel-matched | 5.14.0-8.el9 | |
kernel-devel | 5.14.0-8.el9 | |
kernel-devel-matched | 5.14.0-8.el9 | |
kernel-doc | 5.14.0-8.el9 | |
kernel-headers | 5.14.0-8.el9 | |
libgcrypt-devel | 1.9.3-5.el9 | |
libguestfs | 1.46.0-1.el9 | |
libguestfs-appliance | 1.46.0-1.el9 | |
libguestfs-bash-completion | 1.46.0-1.el9 | |
libguestfs-inspect-icons | 1.46.0-1.el9 | |
libguestfs-rescue | 1.46.0-1.el9 | |
libguestfs-rsync | 1.46.0-1.el9 | |
libguestfs-xfs | 1.46.0-1.el9 | |
libical | 3.0.11-1.el9 | |
libical-devel | 3.0.11-1.el9 | |
libical-glib | 3.0.11-1.el9 | |
libical-glib-devel | 3.0.11-1.el9 | |
libvirt | 7.8.0-1.el9 | |
libvirt-client | 7.8.0-1.el9 | |
libvirt-daemon | 7.8.0-1.el9 | |
libvirt-daemon-config-network | 7.8.0-1.el9 | |
libvirt-daemon-config-nwfilter | 7.8.0-1.el9 | |
libvirt-daemon-driver-interface | 7.8.0-1.el9 | |
libvirt-daemon-driver-network | 7.8.0-1.el9 | |
libvirt-daemon-driver-nodedev | 7.8.0-1.el9 | |
libvirt-daemon-driver-nwfilter | 7.8.0-1.el9 | |
libvirt-daemon-driver-qemu | 7.8.0-1.el9 | |
libvirt-daemon-driver-secret | 7.8.0-1.el9 | |
libvirt-daemon-driver-storage | 7.8.0-1.el9 | |
libvirt-daemon-driver-storage-core | 7.8.0-1.el9 | |
libvirt-daemon-driver-storage-disk | 7.8.0-1.el9 | |
libvirt-daemon-driver-storage-iscsi | 7.8.0-1.el9 | |
libvirt-daemon-driver-storage-logical | 7.8.0-1.el9 | |
libvirt-daemon-driver-storage-mpath | 7.8.0-1.el9 | |
libvirt-daemon-driver-storage-rbd | 7.8.0-1.el9 | |
libvirt-daemon-driver-storage-scsi | 7.8.0-1.el9 | |
libvirt-daemon-kvm | 7.8.0-1.el9 | |
libvirt-libs | 7.8.0-1.el9 | |
libvirt-nss | 7.8.0-1.el9 | |
open-vm-tools | 11.3.5-1.el9 | |
open-vm-tools-test | 11.3.5-1.el9 | |
perf | 5.14.0-8.el9 | |
perl-Sys-Guestfs | 1.46.0-1.el9 | |
pigz | 2.5-4.el9 | |
qt5-qtwayland | 5.15.2-12.el9 | |
qt5-qtwayland-examples | 5.15.2-12.el9 | |
selinux-policy-devel | 34.1.17-1.el9 | |
vim-common | 8.2.2637-6.el9 | |
vim-enhanced | 8.2.2637-6.el9 | |
vim-X11 | 8.2.2637-6.el9 | |

### CRB aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
gdm-devel | 40.1-8.el9 | |
gdm-pam-extensions-devel | 40.1-8.el9 | |
gobject-introspection-devel | 1.68.0-10.el9 | |
kernel-cross-headers | 5.14.0-8.el9 | |
kernel-tools-libs-devel | 5.14.0-8.el9 | |
libbpf-devel | 0.5.0-2.el9 | |
libbpf-static | 0.5.0-2.el9 | |
libguestfs-devel | 1.46.0-1.el9 | |
libguestfs-gobject | 1.46.0-1.el9 | |
libguestfs-gobject-devel | 1.46.0-1.el9 | |
libguestfs-man-pages-ja | 1.46.0-1.el9 | |
libguestfs-man-pages-uk | 1.46.0-1.el9 | |
libvirt-devel | 7.8.0-1.el9 | |
libvirt-docs | 7.8.0-1.el9 | |
libvirt-lock-sanlock | 7.8.0-1.el9 | |
lua-guestfs | 1.46.0-1.el9 | |
ModemManager-devel | 1.18.2-1.el9 | |
ModemManager-glib-devel | 1.18.2-1.el9 | |
ocaml-libguestfs | 1.46.0-1.el9 | |
ocaml-libguestfs-devel | 1.46.0-1.el9 | |
python3-libguestfs | 1.46.0-1.el9 | |
qt5-qtwayland-devel | 5.15.2-12.el9 | |
ruby-libguestfs | 1.46.0-1.el9 | |
transfig | 3.2.7b-10.el9 | |

