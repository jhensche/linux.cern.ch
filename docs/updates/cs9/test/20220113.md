## 2022-01-13

### CERN x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
pyphonebook | 2.1.4-1.el9.cern | |

### CERN aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
pyphonebook | 2.1.4-1.el9.cern | |

