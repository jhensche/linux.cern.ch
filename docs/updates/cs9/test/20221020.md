## 2022-10-20

### openafs x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
afs_tools | 2.3-0.el9.cern | |
afs_tools_standalone | 2.3-0.el9.cern | |

### openafs aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
afs_tools | 2.3-0.el9.cern | |
afs_tools_standalone | 2.3-0.el9.cern | |

