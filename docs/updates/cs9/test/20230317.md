## 2023-03-17

### cloud x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
openstack-neutron | 20.3.0-1.el9s | |
openstack-neutron | 21.1.0-1.el9s | |
openstack-neutron-bgp-dragent | 20.0.1-1.el9s | |
openstack-neutron-common | 20.3.0-1.el9s | |
openstack-neutron-common | 21.1.0-1.el9s | |
openstack-neutron-dynamic-routing-common | 20.0.1-1.el9s | |
openstack-neutron-linuxbridge | 20.3.0-1.el9s | |
openstack-neutron-linuxbridge | 21.1.0-1.el9s | |
openstack-neutron-macvtap-agent | 20.3.0-1.el9s | |
openstack-neutron-macvtap-agent | 21.1.0-1.el9s | |
openstack-neutron-metering-agent | 20.3.0-1.el9s | |
openstack-neutron-metering-agent | 21.1.0-1.el9s | |
openstack-neutron-ml2 | 20.3.0-1.el9s | |
openstack-neutron-ml2 | 21.1.0-1.el9s | |
openstack-neutron-ml2ovn-trace | 20.3.0-1.el9s | |
openstack-neutron-ml2ovn-trace | 21.1.0-1.el9s | |
openstack-neutron-openvswitch | 20.3.0-1.el9s | |
openstack-neutron-openvswitch | 21.1.0-1.el9s | |
openstack-neutron-ovn-metadata-agent | 20.3.0-1.el9s | |
openstack-neutron-ovn-metadata-agent | 21.1.0-1.el9s | |
openstack-neutron-ovn-migration-tool | 20.3.0-1.el9s | |
openstack-neutron-ovn-migration-tool | 21.1.0-1.el9s | |
openstack-neutron-rpc-server | 20.3.0-1.el9s | |
openstack-neutron-rpc-server | 21.1.0-1.el9s | |
openstack-neutron-sriov-nic-agent | 20.3.0-1.el9s | |
openstack-neutron-sriov-nic-agent | 21.1.0-1.el9s | |
python3-neutron | 20.3.0-1.el9s | |
python3-neutron | 21.1.0-1.el9s | |
python3-neutron-dynamic-routing | 20.0.1-1.el9s | |
python3-neutron-dynamic-routing-tests | 20.0.1-1.el9s | |
python3-neutron-tests | 20.3.0-1.el9s | |
python3-neutron-tests | 21.1.0-1.el9s | |
python3-ovn-octavia-provider | 2.1.0-1.el9s | |
python3-ovn-octavia-provider | 3.1.0-1.el9s | |
python3-ovn-octavia-provider-tests | 2.1.0-1.el9s | |
python3-ovn-octavia-provider-tests | 3.1.0-1.el9s | |

### cloud aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
openstack-neutron | 20.3.0-1.el9s | |
openstack-neutron | 21.1.0-1.el9s | |
openstack-neutron-bgp-dragent | 20.0.1-1.el9s | |
openstack-neutron-common | 20.3.0-1.el9s | |
openstack-neutron-common | 21.1.0-1.el9s | |
openstack-neutron-dynamic-routing-common | 20.0.1-1.el9s | |
openstack-neutron-linuxbridge | 20.3.0-1.el9s | |
openstack-neutron-linuxbridge | 21.1.0-1.el9s | |
openstack-neutron-macvtap-agent | 20.3.0-1.el9s | |
openstack-neutron-macvtap-agent | 21.1.0-1.el9s | |
openstack-neutron-metering-agent | 20.3.0-1.el9s | |
openstack-neutron-metering-agent | 21.1.0-1.el9s | |
openstack-neutron-ml2 | 20.3.0-1.el9s | |
openstack-neutron-ml2 | 21.1.0-1.el9s | |
openstack-neutron-ml2ovn-trace | 20.3.0-1.el9s | |
openstack-neutron-ml2ovn-trace | 21.1.0-1.el9s | |
openstack-neutron-openvswitch | 20.3.0-1.el9s | |
openstack-neutron-openvswitch | 21.1.0-1.el9s | |
openstack-neutron-ovn-metadata-agent | 20.3.0-1.el9s | |
openstack-neutron-ovn-metadata-agent | 21.1.0-1.el9s | |
openstack-neutron-ovn-migration-tool | 20.3.0-1.el9s | |
openstack-neutron-ovn-migration-tool | 21.1.0-1.el9s | |
openstack-neutron-rpc-server | 20.3.0-1.el9s | |
openstack-neutron-rpc-server | 21.1.0-1.el9s | |
openstack-neutron-sriov-nic-agent | 20.3.0-1.el9s | |
openstack-neutron-sriov-nic-agent | 21.1.0-1.el9s | |
python3-neutron | 20.3.0-1.el9s | |
python3-neutron | 21.1.0-1.el9s | |
python3-neutron-dynamic-routing | 20.0.1-1.el9s | |
python3-neutron-dynamic-routing-tests | 20.0.1-1.el9s | |
python3-neutron-tests | 20.3.0-1.el9s | |
python3-neutron-tests | 21.1.0-1.el9s | |
python3-ovn-octavia-provider | 2.1.0-1.el9s | |
python3-ovn-octavia-provider | 3.1.0-1.el9s | |
python3-ovn-octavia-provider-tests | 2.1.0-1.el9s | |
python3-ovn-octavia-provider-tests | 3.1.0-1.el9s | |

