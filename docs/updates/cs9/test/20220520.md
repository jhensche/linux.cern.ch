## 2022-05-20

### CERN x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
lpadmincern | 1.4.1-1.el9.cern | |

### CERN aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
lpadmincern | 1.4.1-1.el9.cern | |

