## 2022-03-16

### openafs x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
dkms-openafs | 1.8.8-1.el9.cern | |
kmod-openafs | 1.8.8-1.5.14.0_30.el9.el9.cern | |
kmod-openafs | 1.8.8-1.5.14.0_34.el9.el9.cern | |
kmod-openafs | 1.8.8-1.5.14.0_39.el9.el9.cern | |
kmod-openafs | 1.8.8-1.5.14.0_41.el9.el9.cern | |
kmod-openafs | 1.8.8-1.5.14.0_43.el9.el9.cern | |
kmod-openafs | 1.8.8-1.5.14.0_44.el9.el9.cern | |
kmod-openafs | 1.8.8-1.5.14.0_47.el9.el9.cern | |
kmod-openafs | 1.8.8-1.5.14.0_52.el9.el9.cern | |
kmod-openafs | 1.8.8-1.5.14.0_55.el9.el9.cern | |
kmod-openafs | 1.8.8-1.5.14.0_58.el9.el9.cern | |
kmod-openafs | 1.8.8-1.5.14.0_62.el9.el9.cern | |
kmod-openafs | 1.8.8-1.5.14.0_66.el9.el9.cern | |
kmod-openafs | 1.8.8-1.5.14.0_70.el9.el9.cern | |
kmod-openafs | 1.8.8-1.5.14.0_71.el9.el9.cern | |
openafs | 1.8.8-1.el9.cern | |
openafs-authlibs | 1.8.8-1.el9.cern | |
openafs-authlibs-devel | 1.8.8-1.el9.cern | |
openafs-client | 1.8.8-1.el9.cern | |
openafs-compat | 1.8.8-1.el9.cern | |
openafs-debugsource | 1.8.8-1.el9.cern | |
openafs-debugsource | 1.8.8_5.14.0_30.el9-1.el9.cern | |
openafs-debugsource | 1.8.8_5.14.0_34.el9-1.el9.cern | |
openafs-debugsource | 1.8.8_5.14.0_39.el9-1.el9.cern | |
openafs-debugsource | 1.8.8_5.14.0_41.el9-1.el9.cern | |
openafs-debugsource | 1.8.8_5.14.0_43.el9-1.el9.cern | |
openafs-debugsource | 1.8.8_5.14.0_44.el9-1.el9.cern | |
openafs-debugsource | 1.8.8_5.14.0_47.el9-1.el9.cern | |
openafs-debugsource | 1.8.8_5.14.0_52.el9-1.el9.cern | |
openafs-debugsource | 1.8.8_5.14.0_55.el9-1.el9.cern | |
openafs-debugsource | 1.8.8_5.14.0_58.el9-1.el9.cern | |
openafs-debugsource | 1.8.8_5.14.0_62.el9-1.el9.cern | |
openafs-debugsource | 1.8.8_5.14.0_66.el9-1.el9.cern | |
openafs-debugsource | 1.8.8_5.14.0_70.el9-1.el9.cern | |
openafs-debugsource | 1.8.8_5.14.0_71.el9-1.el9.cern | |
openafs-devel | 1.8.8-1.el9.cern | |
openafs-docs | 1.8.8-1.el9.cern | |
openafs-kernel-source | 1.8.8-1.el9.cern | |
openafs-krb5 | 1.8.8-1.el9.cern | |
openafs-server | 1.8.8-1.el9.cern | |
pam_afs_session | 2.6-3.el9.cern | |
pam_afs_session-debugsource | 2.6-3.el9.cern | |

### BaseOS x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
audispd-plugins | 3.0.7-102.el9 | |
audispd-plugins-zos | 3.0.7-102.el9 | |
audit | 3.0.7-102.el9 | |
audit-libs | 3.0.7-102.el9 | |
bpftool | 5.14.0-71.el9 | |
dmidecode | 3.3-7.el9 | |
kernel | 5.14.0-71.el9 | |
kernel-abi-stablelists | 5.14.0-71.el9 | |
kernel-core | 5.14.0-71.el9 | |
kernel-debug | 5.14.0-71.el9 | |
kernel-debug-core | 5.14.0-71.el9 | |
kernel-debug-modules | 5.14.0-71.el9 | |
kernel-debug-modules-extra | 5.14.0-71.el9 | |
kernel-modules | 5.14.0-71.el9 | |
kernel-modules-extra | 5.14.0-71.el9 | |
kernel-tools | 5.14.0-71.el9 | |
kernel-tools-libs | 5.14.0-71.el9 | |
mcelog | 180-0.el9 | |
NetworkManager | 1.37.2-1.el9 | |
NetworkManager-adsl | 1.37.2-1.el9 | |
NetworkManager-bluetooth | 1.37.2-1.el9 | |
NetworkManager-config-server | 1.37.2-1.el9 | |
NetworkManager-initscripts-updown | 1.37.2-1.el9 | |
NetworkManager-libnm | 1.37.2-1.el9 | |
NetworkManager-team | 1.37.2-1.el9 | |
NetworkManager-tui | 1.37.2-1.el9 | |
NetworkManager-wifi | 1.37.2-1.el9 | |
NetworkManager-wwan | 1.37.2-1.el9 | |
openssl | 3.0.1-17.el9 | |
openssl-libs | 3.0.1-17.el9 | |
python3-perf | 5.14.0-71.el9 | |

### AppStream x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
annobin | 10.58-1.el9 | |
annobin-annocheck | 10.58-1.el9 | |
ansible-core | 2.12.3-1.el9 | |
ansible-test | 2.12.3-1.el9 | |
audit-libs-devel | 3.0.7-102.el9 | |
containernetworking-plugins | 1.1.1-1.el9 | |
flatpak | 1.12.5-2.el9 | |
flatpak-libs | 1.12.5-2.el9 | |
flatpak-selinux | 1.12.5-2.el9 | |
flatpak-session-helper | 1.12.5-2.el9 | |
gdm | 40.1-13.el9 | |
glslc | 2022.1-1.el9 | |
kernel-debug-devel | 5.14.0-71.el9 | |
kernel-debug-devel-matched | 5.14.0-71.el9 | |
kernel-devel | 5.14.0-71.el9 | |
kernel-devel-matched | 5.14.0-71.el9 | |
kernel-doc | 5.14.0-71.el9 | |
kernel-headers | 5.14.0-71.el9 | |
libshaderc | 2022.1-1.el9 | |
NetworkManager-cloud-setup | 1.37.2-1.el9 | |
NetworkManager-config-connectivity-redhat | 1.37.2-1.el9 | |
NetworkManager-dispatcher-routing-rules | 1.37.2-1.el9 | |
NetworkManager-ovs | 1.37.2-1.el9 | |
NetworkManager-ppp | 1.37.2-1.el9 | |
openssl-devel | 3.0.1-17.el9 | |
openssl-perl | 3.0.1-17.el9 | |
osbuild | 52-1.el9 | |
osbuild-luks2 | 52-1.el9 | |
osbuild-lvm2 | 52-1.el9 | |
osbuild-ostree | 52-1.el9 | |
osbuild-selinux | 52-1.el9 | |
perf | 5.14.0-71.el9 | |
pipewire | 0.3.47-2.el9 | |
pipewire-alsa | 0.3.47-2.el9 | |
pipewire-devel | 0.3.47-2.el9 | |
pipewire-gstreamer | 0.3.47-2.el9 | |
pipewire-jack-audio-connection-kit | 0.3.47-2.el9 | |
pipewire-jack-audio-connection-kit-devel | 0.3.47-2.el9 | |
pipewire-libs | 0.3.47-2.el9 | |
pipewire-pulseaudio | 0.3.47-2.el9 | |
pipewire-utils | 0.3.47-2.el9 | |
podman | 4.0.2-2.el9 | |
podman-catatonit | 4.0.2-2.el9 | |
podman-docker | 4.0.2-2.el9 | |
podman-gvproxy | 4.0.2-2.el9 | |
podman-plugins | 4.0.2-2.el9 | |
podman-remote | 4.0.2-2.el9 | |
podman-tests | 4.0.2-2.el9 | |
python3-audit | 3.0.7-102.el9 | |
python3-osbuild | 52-1.el9 | |
skopeo | 1.6.1-4.el9 | |
skopeo-tests | 1.6.1-4.el9 | |

### CRB x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
flatpak-devel | 1.12.5-2.el9 | |
gdm-devel | 40.1-13.el9 | |
gdm-pam-extensions-devel | 40.1-13.el9 | |
kernel-cross-headers | 5.14.0-71.el9 | |
kernel-tools-libs-devel | 5.14.0-71.el9 | |
NetworkManager-libnm-devel | 1.37.2-1.el9 | |
ocaml | 4.11.1-5.el9.2 | |
ocaml-calendar | 2.04-40.el9 | |
ocaml-calendar-devel | 2.04-40.el9 | |
ocaml-camomile | 1.0.2-19.el9 | |
ocaml-camomile-data | 1.0.2-19.el9 | |
ocaml-camomile-devel | 1.0.2-19.el9 | |
ocaml-compiler-libs | 4.11.1-5.el9.2 | |
ocaml-cppo | 1.6.6-15.el9 | |
ocaml-csexp | 1.3.2-6.el9 | |
ocaml-csexp-devel | 1.3.2-6.el9 | |
ocaml-csv | 1.7-29.el9 | |
ocaml-csv-devel | 1.7-29.el9 | |
ocaml-curses | 1.0.4-24.el9 | |
ocaml-curses-devel | 1.0.4-24.el9 | |
ocaml-docs | 4.11.1-5.el9.2 | |
ocaml-dune | 2.8.5-6.el9 | |
ocaml-dune-devel | 2.8.5-6.el9 | |
ocaml-dune-doc | 2.8.5-6.el9 | |
ocaml-dune-emacs | 2.8.5-6.el9 | |
ocaml-extlib | 1.7.8-7.el9 | |
ocaml-extlib-devel | 1.7.8-7.el9 | |
ocaml-fileutils | 0.5.2-29.el9 | |
ocaml-fileutils-devel | 0.5.2-29.el9 | |
ocaml-findlib | 1.8.1-28.el9 | |
ocaml-findlib-devel | 1.8.1-28.el9 | |
ocaml-gettext | 0.4.2-6.el9 | |
ocaml-gettext-devel | 0.4.2-6.el9 | |
ocaml-labltk | 8.06.5-25.el9 | |
ocaml-labltk-devel | 8.06.5-25.el9 | |
ocaml-libvirt | 0.6.1.5-19.el9 | |
ocaml-libvirt-devel | 0.6.1.5-19.el9 | |
ocaml-ocamlbuild | 0.14.0-27.el9 | |
ocaml-ocamlbuild-devel | 0.14.0-27.el9 | |
ocaml-ocamlbuild-doc | 0.14.0-27.el9 | |
ocaml-ocamldoc | 4.11.1-5.el9.2 | |
ocaml-runtime | 4.11.1-5.el9.2 | |
ocaml-source | 4.11.1-5.el9.2 | |
ocaml-xml-light | 2.3-0.56.svn234.el9 | |
ocaml-xml-light-devel | 2.3-0.56.svn234.el9 | |

### openafs aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
dkms-openafs | 1.8.8-1.el9.cern | |
kmod-openafs | 1.8.8-1.5.14.0_30.el9.el9.cern | |
kmod-openafs | 1.8.8-1.5.14.0_34.el9.el9.cern | |
kmod-openafs | 1.8.8-1.5.14.0_39.el9.el9.cern | |
kmod-openafs | 1.8.8-1.5.14.0_41.el9.el9.cern | |
kmod-openafs | 1.8.8-1.5.14.0_43.el9.el9.cern | |
kmod-openafs | 1.8.8-1.5.14.0_44.el9.el9.cern | |
kmod-openafs | 1.8.8-1.5.14.0_47.el9.el9.cern | |
kmod-openafs | 1.8.8-1.5.14.0_52.el9.el9.cern | |
kmod-openafs | 1.8.8-1.5.14.0_55.el9.el9.cern | |
kmod-openafs | 1.8.8-1.5.14.0_58.el9.el9.cern | |
kmod-openafs | 1.8.8-1.5.14.0_62.el9.el9.cern | |
kmod-openafs | 1.8.8-1.5.14.0_66.el9.el9.cern | |
kmod-openafs | 1.8.8-1.5.14.0_70.el9.el9.cern | |
kmod-openafs | 1.8.8-1.5.14.0_71.el9.el9.cern | |
openafs | 1.8.8-1.el9.cern | |
openafs-authlibs | 1.8.8-1.el9.cern | |
openafs-authlibs-devel | 1.8.8-1.el9.cern | |
openafs-client | 1.8.8-1.el9.cern | |
openafs-compat | 1.8.8-1.el9.cern | |
openafs-debugsource | 1.8.8-1.el9.cern | |
openafs-debugsource | 1.8.8_5.14.0_30.el9-1.el9.cern | |
openafs-debugsource | 1.8.8_5.14.0_34.el9-1.el9.cern | |
openafs-debugsource | 1.8.8_5.14.0_39.el9-1.el9.cern | |
openafs-debugsource | 1.8.8_5.14.0_41.el9-1.el9.cern | |
openafs-debugsource | 1.8.8_5.14.0_43.el9-1.el9.cern | |
openafs-debugsource | 1.8.8_5.14.0_44.el9-1.el9.cern | |
openafs-debugsource | 1.8.8_5.14.0_47.el9-1.el9.cern | |
openafs-debugsource | 1.8.8_5.14.0_52.el9-1.el9.cern | |
openafs-debugsource | 1.8.8_5.14.0_55.el9-1.el9.cern | |
openafs-debugsource | 1.8.8_5.14.0_58.el9-1.el9.cern | |
openafs-debugsource | 1.8.8_5.14.0_62.el9-1.el9.cern | |
openafs-debugsource | 1.8.8_5.14.0_66.el9-1.el9.cern | |
openafs-debugsource | 1.8.8_5.14.0_70.el9-1.el9.cern | |
openafs-debugsource | 1.8.8_5.14.0_71.el9-1.el9.cern | |
openafs-devel | 1.8.8-1.el9.cern | |
openafs-docs | 1.8.8-1.el9.cern | |
openafs-kernel-source | 1.8.8-1.el9.cern | |
openafs-krb5 | 1.8.8-1.el9.cern | |
openafs-server | 1.8.8-1.el9.cern | |
pam_afs_session | 2.6-3.el9.cern | |
pam_afs_session-debugsource | 2.6-3.el9.cern | |

### BaseOS aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
audispd-plugins | 3.0.7-102.el9 | |
audispd-plugins-zos | 3.0.7-102.el9 | |
audit | 3.0.7-102.el9 | |
audit-libs | 3.0.7-102.el9 | |
bpftool | 5.14.0-71.el9 | |
dmidecode | 3.3-7.el9 | |
kernel | 5.14.0-71.el9 | |
kernel-abi-stablelists | 5.14.0-71.el9 | |
kernel-core | 5.14.0-71.el9 | |
kernel-debug | 5.14.0-71.el9 | |
kernel-debug-core | 5.14.0-71.el9 | |
kernel-debug-modules | 5.14.0-71.el9 | |
kernel-debug-modules-extra | 5.14.0-71.el9 | |
kernel-modules | 5.14.0-71.el9 | |
kernel-modules-extra | 5.14.0-71.el9 | |
kernel-tools | 5.14.0-71.el9 | |
kernel-tools-libs | 5.14.0-71.el9 | |
NetworkManager | 1.37.2-1.el9 | |
NetworkManager-adsl | 1.37.2-1.el9 | |
NetworkManager-bluetooth | 1.37.2-1.el9 | |
NetworkManager-config-server | 1.37.2-1.el9 | |
NetworkManager-initscripts-updown | 1.37.2-1.el9 | |
NetworkManager-libnm | 1.37.2-1.el9 | |
NetworkManager-team | 1.37.2-1.el9 | |
NetworkManager-tui | 1.37.2-1.el9 | |
NetworkManager-wifi | 1.37.2-1.el9 | |
NetworkManager-wwan | 1.37.2-1.el9 | |
openssl | 3.0.1-17.el9 | |
openssl-libs | 3.0.1-17.el9 | |
python3-perf | 5.14.0-71.el9 | |

### AppStream aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
annobin | 10.58-1.el9 | |
annobin-annocheck | 10.58-1.el9 | |
ansible-core | 2.12.3-1.el9 | |
ansible-test | 2.12.3-1.el9 | |
audit-libs-devel | 3.0.7-102.el9 | |
containernetworking-plugins | 1.1.1-1.el9 | |
flatpak | 1.12.5-2.el9 | |
flatpak-libs | 1.12.5-2.el9 | |
flatpak-selinux | 1.12.5-2.el9 | |
flatpak-session-helper | 1.12.5-2.el9 | |
gdm | 40.1-13.el9 | |
glslc | 2022.1-1.el9 | |
kernel-debug-devel | 5.14.0-71.el9 | |
kernel-debug-devel-matched | 5.14.0-71.el9 | |
kernel-devel | 5.14.0-71.el9 | |
kernel-devel-matched | 5.14.0-71.el9 | |
kernel-doc | 5.14.0-71.el9 | |
kernel-headers | 5.14.0-71.el9 | |
libshaderc | 2022.1-1.el9 | |
NetworkManager-cloud-setup | 1.37.2-1.el9 | |
NetworkManager-config-connectivity-redhat | 1.37.2-1.el9 | |
NetworkManager-dispatcher-routing-rules | 1.37.2-1.el9 | |
NetworkManager-ovs | 1.37.2-1.el9 | |
NetworkManager-ppp | 1.37.2-1.el9 | |
openssl-devel | 3.0.1-17.el9 | |
openssl-perl | 3.0.1-17.el9 | |
osbuild | 52-1.el9 | |
osbuild-luks2 | 52-1.el9 | |
osbuild-lvm2 | 52-1.el9 | |
osbuild-ostree | 52-1.el9 | |
osbuild-selinux | 52-1.el9 | |
perf | 5.14.0-71.el9 | |
pipewire | 0.3.47-2.el9 | |
pipewire-alsa | 0.3.47-2.el9 | |
pipewire-devel | 0.3.47-2.el9 | |
pipewire-gstreamer | 0.3.47-2.el9 | |
pipewire-jack-audio-connection-kit | 0.3.47-2.el9 | |
pipewire-jack-audio-connection-kit-devel | 0.3.47-2.el9 | |
pipewire-libs | 0.3.47-2.el9 | |
pipewire-pulseaudio | 0.3.47-2.el9 | |
pipewire-utils | 0.3.47-2.el9 | |
podman | 4.0.2-2.el9 | |
podman-catatonit | 4.0.2-2.el9 | |
podman-docker | 4.0.2-2.el9 | |
podman-gvproxy | 4.0.2-2.el9 | |
podman-plugins | 4.0.2-2.el9 | |
podman-remote | 4.0.2-2.el9 | |
podman-tests | 4.0.2-2.el9 | |
python3-audit | 3.0.7-102.el9 | |
python3-osbuild | 52-1.el9 | |
skopeo | 1.6.1-4.el9 | |
skopeo-tests | 1.6.1-4.el9 | |

### CRB aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
flatpak-devel | 1.12.5-2.el9 | |
gdm-devel | 40.1-13.el9 | |
gdm-pam-extensions-devel | 40.1-13.el9 | |
kernel-cross-headers | 5.14.0-71.el9 | |
kernel-tools-libs-devel | 5.14.0-71.el9 | |
NetworkManager-libnm-devel | 1.37.2-1.el9 | |
ocaml | 4.11.1-5.el9.2 | |
ocaml-calendar | 2.04-40.el9 | |
ocaml-calendar-devel | 2.04-40.el9 | |
ocaml-camomile | 1.0.2-19.el9 | |
ocaml-camomile-data | 1.0.2-19.el9 | |
ocaml-camomile-devel | 1.0.2-19.el9 | |
ocaml-compiler-libs | 4.11.1-5.el9.2 | |
ocaml-cppo | 1.6.6-15.el9 | |
ocaml-csexp | 1.3.2-6.el9 | |
ocaml-csexp-devel | 1.3.2-6.el9 | |
ocaml-csv | 1.7-29.el9 | |
ocaml-csv-devel | 1.7-29.el9 | |
ocaml-curses | 1.0.4-24.el9 | |
ocaml-curses-devel | 1.0.4-24.el9 | |
ocaml-docs | 4.11.1-5.el9.2 | |
ocaml-dune | 2.8.5-6.el9 | |
ocaml-dune-devel | 2.8.5-6.el9 | |
ocaml-dune-doc | 2.8.5-6.el9 | |
ocaml-dune-emacs | 2.8.5-6.el9 | |
ocaml-extlib | 1.7.8-7.el9 | |
ocaml-extlib-devel | 1.7.8-7.el9 | |
ocaml-fileutils | 0.5.2-29.el9 | |
ocaml-fileutils-devel | 0.5.2-29.el9 | |
ocaml-findlib | 1.8.1-28.el9 | |
ocaml-findlib-devel | 1.8.1-28.el9 | |
ocaml-gettext | 0.4.2-6.el9 | |
ocaml-gettext-devel | 0.4.2-6.el9 | |
ocaml-labltk | 8.06.5-25.el9 | |
ocaml-labltk-devel | 8.06.5-25.el9 | |
ocaml-libvirt | 0.6.1.5-19.el9 | |
ocaml-libvirt-devel | 0.6.1.5-19.el9 | |
ocaml-ocamlbuild | 0.14.0-27.el9 | |
ocaml-ocamlbuild-devel | 0.14.0-27.el9 | |
ocaml-ocamlbuild-doc | 0.14.0-27.el9 | |
ocaml-ocamldoc | 4.11.1-5.el9.2 | |
ocaml-runtime | 4.11.1-5.el9.2 | |
ocaml-source | 4.11.1-5.el9.2 | |
ocaml-xml-light | 2.3-0.56.svn234.el9 | |
ocaml-xml-light-devel | 2.3-0.56.svn234.el9 | |

