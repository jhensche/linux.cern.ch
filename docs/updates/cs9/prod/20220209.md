## 2022-02-09

### CERN x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
cern-config-users | 1.8.2-4.el9.cern | |
cern-linuxsupport-access | 1.7-1.el9.cern | |

### BaseOS x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
cryptsetup | 2.4.3-1.el9 | |
cryptsetup-libs | 2.4.3-1.el9 | |
integritysetup | 2.4.3-1.el9 | |
libatomic | 11.2.1-7.6.el9 | |
libgcc | 11.2.1-7.6.el9 | |
libgfortran | 11.2.1-7.6.el9 | |
libgomp | 11.2.1-7.6.el9 | |
libquadmath | 11.2.1-7.6.el9 | |
libstdc++ | 11.2.1-7.6.el9 | |
veritysetup | 2.4.3-1.el9 | |

### AppStream x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
anaconda | 34.25.0.25-1.el9 | |
anaconda-core | 34.25.0.25-1.el9 | |
anaconda-dracut | 34.25.0.25-1.el9 | |
anaconda-gui | 34.25.0.25-1.el9 | |
anaconda-install-env-deps | 34.25.0.25-1.el9 | |
anaconda-install-img-deps | 34.25.0.25-1.el9 | |
anaconda-tui | 34.25.0.25-1.el9 | |
anaconda-widgets | 34.25.0.25-1.el9 | |
annobin | 10.51-1.el9 | |
annobin-annocheck | 10.51-1.el9 | |
clevis | 18-101.el9 | |
clevis-dracut | 18-101.el9 | |
clevis-luks | 18-101.el9 | |
clevis-systemd | 18-101.el9 | |
clevis-udisks2 | 18-101.el9 | |
coreos-installer | 0.11.0-1.el9 | |
coreos-installer-bootinfra | 0.11.0-1.el9 | |
coreos-installer-dracut | 0.11.0-1.el9 | |
cpp | 11.2.1-7.6.el9 | |
crun | 1.4.2-1.el9 | |
freeradius | 3.0.21-24.el9 | |
freeradius-devel | 3.0.21-24.el9 | |
freeradius-doc | 3.0.21-24.el9 | |
freeradius-krb5 | 3.0.21-24.el9 | |
freeradius-ldap | 3.0.21-24.el9 | |
freeradius-utils | 3.0.21-24.el9 | |
gcc | 11.2.1-7.6.el9 | |
gcc-c++ | 11.2.1-7.6.el9 | |
gcc-gfortran | 11.2.1-7.6.el9 | |
gcc-offload-nvptx | 11.2.1-7.6.el9 | |
hostapd | 2.10-1.el9 | |
libgccjit | 11.2.1-7.6.el9 | |
libgccjit-devel | 11.2.1-7.6.el9 | |
libgomp-offload-nvptx | 11.2.1-7.6.el9 | |
libitm | 11.2.1-7.6.el9 | |
libitm-devel | 11.2.1-7.6.el9 | |
liblsan | 11.2.1-7.6.el9 | |
libpmemobj++-devel | 1.12-7.el9 | |
libpmemobj++-doc | 1.12-7.el9 | |
libquadmath-devel | 11.2.1-7.6.el9 | |
libstdc++-devel | 11.2.1-7.6.el9 | |
libstdc++-docs | 11.2.1-7.6.el9 | |
libtool | 2.4.6-45.el9 | |
libtool-ltdl | 2.4.6-45.el9 | |
libtsan | 11.2.1-7.6.el9 | |
libvirt | 8.0.0-2.el9 | |
libvirt-client | 8.0.0-2.el9 | |
libvirt-daemon | 8.0.0-2.el9 | |
libvirt-daemon-config-network | 8.0.0-2.el9 | |
libvirt-daemon-config-nwfilter | 8.0.0-2.el9 | |
libvirt-daemon-driver-interface | 8.0.0-2.el9 | |
libvirt-daemon-driver-network | 8.0.0-2.el9 | |
libvirt-daemon-driver-nodedev | 8.0.0-2.el9 | |
libvirt-daemon-driver-nwfilter | 8.0.0-2.el9 | |
libvirt-daemon-driver-qemu | 8.0.0-2.el9 | |
libvirt-daemon-driver-secret | 8.0.0-2.el9 | |
libvirt-daemon-driver-storage | 8.0.0-2.el9 | |
libvirt-daemon-driver-storage-core | 8.0.0-2.el9 | |
libvirt-daemon-driver-storage-disk | 8.0.0-2.el9 | |
libvirt-daemon-driver-storage-iscsi | 8.0.0-2.el9 | |
libvirt-daemon-driver-storage-logical | 8.0.0-2.el9 | |
libvirt-daemon-driver-storage-mpath | 8.0.0-2.el9 | |
libvirt-daemon-driver-storage-rbd | 8.0.0-2.el9 | |
libvirt-daemon-driver-storage-scsi | 8.0.0-2.el9 | |
libvirt-daemon-kvm | 8.0.0-2.el9 | |
libvirt-libs | 8.0.0-2.el9 | |
libvirt-nss | 8.0.0-2.el9 | |
lorax | 34.9.11-1.el9 | |
lorax-docs | 34.9.11-1.el9 | |
lorax-lmc-novirt | 34.9.11-1.el9 | |
lorax-lmc-virt | 34.9.11-1.el9 | |
lorax-templates-generic | 34.9.11-1.el9 | |
lorax-templates-rhel | 9.0-32.el9 | |
python-rpm-macros | 3.9-49.el9 | |
python-srpm-macros | 3.9-49.el9 | |
python3-freeradius | 3.0.21-24.el9 | |
python3-rpm-macros | 3.9-49.el9 | |
rsyslog-mmfields | 8.2102.0-101.el9 | |
setroubleshoot | 3.3.27-1.el9 | |
setroubleshoot-server | 3.3.27-1.el9 | |
virt-v2v | 1.45.97-3.el9 | |
virt-v2v-bash-completion | 1.45.97-3.el9 | |

### HighAvailability x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
libtool-ltdl-devel | 2.4.6-45.el9 | |
pcs | 0.11.1-9.el9 | |
pcs-snmp | 0.11.1-9.el9 | |

### ResilientStorage x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
libtool-ltdl-devel | 2.4.6-45.el9 | |
pcs | 0.11.1-9.el9 | |
pcs-snmp | 0.11.1-9.el9 | |

### CRB x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
cryptsetup-devel | 2.4.3-1.el9 | |
dejagnu | 1.6.3-2.el9 | |
gcc-plugin-devel | 11.2.1-7.6.el9 | |
gsl-devel | 2.6-6.el9 | |
libstdc++-static | 11.2.1-7.6.el9 | |
libtool-ltdl-devel | 2.4.6-45.el9 | |
libvirt-devel | 8.0.0-2.el9 | |
libvirt-docs | 8.0.0-2.el9 | |
libvirt-lock-sanlock | 8.0.0-2.el9 | |
virt-v2v-man-pages-ja | 1.45.97-3.el9 | |
virt-v2v-man-pages-uk | 1.45.97-3.el9 | |

### extras x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
centos-release-ceph-pacific | 1.0-2.el9s | |
centos-release-ceph-quincy | 1.0-2.el9s | |
centos-release-kmods | 2-4.el9s | |
centos-release-nfs-ganesha4 | 1.0-2.el9s | |
centos-release-storage-common | 2-4.el9s | |

### CERN aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
cern-config-users | 1.8.2-4.el9.cern | |
cern-linuxsupport-access | 1.7-1.el9.cern | |

### BaseOS aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
cryptsetup | 2.4.3-1.el9 | |
cryptsetup-libs | 2.4.3-1.el9 | |
integritysetup | 2.4.3-1.el9 | |
libatomic | 11.2.1-7.6.el9 | |
libgcc | 11.2.1-7.6.el9 | |
libgfortran | 11.2.1-7.6.el9 | |
libgomp | 11.2.1-7.6.el9 | |
libstdc++ | 11.2.1-7.6.el9 | |
veritysetup | 2.4.3-1.el9 | |

### AppStream aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
anaconda | 34.25.0.25-1.el9 | |
anaconda-core | 34.25.0.25-1.el9 | |
anaconda-dracut | 34.25.0.25-1.el9 | |
anaconda-gui | 34.25.0.25-1.el9 | |
anaconda-install-env-deps | 34.25.0.25-1.el9 | |
anaconda-install-img-deps | 34.25.0.25-1.el9 | |
anaconda-tui | 34.25.0.25-1.el9 | |
anaconda-widgets | 34.25.0.25-1.el9 | |
annobin | 10.51-1.el9 | |
annobin-annocheck | 10.51-1.el9 | |
Box2D | 2.4.1-7.el9 | |
clevis | 18-101.el9 | |
clevis-dracut | 18-101.el9 | |
clevis-luks | 18-101.el9 | |
clevis-systemd | 18-101.el9 | |
clevis-udisks2 | 18-101.el9 | |
clucene-contribs-lib | 2.3.3.4-42.20130812.e8e3d20git.el9 | |
coreos-installer | 0.11.0-1.el9 | |
coreos-installer-bootinfra | 0.11.0-1.el9 | |
coreos-installer-dracut | 0.11.0-1.el9 | |
cpp | 11.2.1-7.6.el9 | |
crun | 1.4.2-1.el9 | |
freeradius | 3.0.21-24.el9 | |
freeradius-devel | 3.0.21-24.el9 | |
freeradius-doc | 3.0.21-24.el9 | |
freeradius-krb5 | 3.0.21-24.el9 | |
freeradius-ldap | 3.0.21-24.el9 | |
freeradius-utils | 3.0.21-24.el9 | |
gcc | 11.2.1-7.6.el9 | |
gcc-c++ | 11.2.1-7.6.el9 | |
gcc-gfortran | 11.2.1-7.6.el9 | |
gpgmepp | 1.15.1-5.el9 | |
hostapd | 2.10-1.el9 | |
libasan | 11.2.1-7.6.el9 | |
libcmis | 0.5.2-12.el9 | |
libgccjit | 11.2.1-7.6.el9 | |
libgccjit-devel | 11.2.1-7.6.el9 | |
libitm | 11.2.1-7.6.el9 | |
libitm-devel | 11.2.1-7.6.el9 | |
liblangtag | 0.6.3-8.el9.1 | |
liblangtag-data | 0.6.3-8.el9.1 | |
liblsan | 11.2.1-7.6.el9 | |
liborcus | 0.16.1-8.el9 | |
libreoffice-core | 7.1.8.1-2.el9 | |
libreoffice-data | 7.1.8.1-2.el9 | |
libreoffice-help-en | 7.1.8.1-2.el9 | |
libreoffice-langpack-en | 7.1.8.1-2.el9 | |
libreoffice-opensymbol-fonts | 7.1.8.1-2.el9 | |
libreoffice-ure | 7.1.8.1-2.el9 | |
libreoffice-ure-common | 7.1.8.1-2.el9 | |
libreoffice-x11 | 7.1.8.1-2.el9 | |
libstdc++-devel | 11.2.1-7.6.el9 | |
libstdc++-docs | 11.2.1-7.6.el9 | |
libtool | 2.4.6-45.el9 | |
libtool-ltdl | 2.4.6-45.el9 | |
libtsan | 11.2.1-7.6.el9 | |
libubsan | 11.2.1-7.6.el9 | |
libvirt | 8.0.0-2.el9 | |
libvirt-client | 8.0.0-2.el9 | |
libvirt-daemon | 8.0.0-2.el9 | |
libvirt-daemon-config-network | 8.0.0-2.el9 | |
libvirt-daemon-config-nwfilter | 8.0.0-2.el9 | |
libvirt-daemon-driver-interface | 8.0.0-2.el9 | |
libvirt-daemon-driver-network | 8.0.0-2.el9 | |
libvirt-daemon-driver-nodedev | 8.0.0-2.el9 | |
libvirt-daemon-driver-nwfilter | 8.0.0-2.el9 | |
libvirt-daemon-driver-qemu | 8.0.0-2.el9 | |
libvirt-daemon-driver-secret | 8.0.0-2.el9 | |
libvirt-daemon-driver-storage | 8.0.0-2.el9 | |
libvirt-daemon-driver-storage-core | 8.0.0-2.el9 | |
libvirt-daemon-driver-storage-disk | 8.0.0-2.el9 | |
libvirt-daemon-driver-storage-iscsi | 8.0.0-2.el9 | |
libvirt-daemon-driver-storage-logical | 8.0.0-2.el9 | |
libvirt-daemon-driver-storage-mpath | 8.0.0-2.el9 | |
libvirt-daemon-driver-storage-rbd | 8.0.0-2.el9 | |
libvirt-daemon-driver-storage-scsi | 8.0.0-2.el9 | |
libvirt-daemon-kvm | 8.0.0-2.el9 | |
libvirt-libs | 8.0.0-2.el9 | |
libvirt-nss | 8.0.0-2.el9 | |
lorax | 34.9.11-1.el9 | |
lorax-docs | 34.9.11-1.el9 | |
lorax-lmc-novirt | 34.9.11-1.el9 | |
lorax-lmc-virt | 34.9.11-1.el9 | |
lorax-templates-generic | 34.9.11-1.el9 | |
lorax-templates-rhel | 9.0-32.el9 | |
mythes | 1.2.4-18.el9 | |
mythes-en | 3.0-33.el9 | |
neon | 0.31.2-11.el9 | |
python-rpm-macros | 3.9-49.el9 | |
python-srpm-macros | 3.9-49.el9 | |
python3-freeradius | 3.0.21-24.el9 | |
python3-rpm-macros | 3.9-49.el9 | |
raptor2 | 2.0.15-30.el9 | |
rasqal | 0.9.33-18.el9 | |
redland | 1.0.17-29.el9 | |
rsyslog-mmfields | 8.2102.0-101.el9 | |
setroubleshoot | 3.3.27-1.el9 | |
setroubleshoot-server | 3.3.27-1.el9 | |
xmlsec1-nss | 1.2.29-9.el9 | |

### HighAvailability aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
libtool-ltdl-devel | 2.4.6-45.el9 | |
pcs | 0.11.1-9.el9 | |
pcs-snmp | 0.11.1-9.el9 | |

### CRB aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
cryptsetup-devel | 2.4.3-1.el9 | |
dejagnu | 1.6.3-2.el9 | |
gcc-plugin-devel | 11.2.1-7.6.el9 | |
gsl-devel | 2.6-6.el9 | |
libstdc++-static | 11.2.1-7.6.el9 | |
libtool-ltdl-devel | 2.4.6-45.el9 | |
libvirt-devel | 8.0.0-2.el9 | |
libvirt-docs | 8.0.0-2.el9 | |
libvirt-lock-sanlock | 8.0.0-2.el9 | |

### extras aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
centos-release-ceph-pacific | 1.0-2.el9s | |
centos-release-ceph-quincy | 1.0-2.el9s | |
centos-release-kmods | 2-4.el9s | |
centos-release-nfs-ganesha4 | 1.0-2.el9s | |
centos-release-storage-common | 2-4.el9s | |

