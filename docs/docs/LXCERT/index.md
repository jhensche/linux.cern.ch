<!--#include virtual="/linux/layout/header" -->

<h2>Linux certification coordination</h2>

<h2>Objectives and scope of the "Linux certification coordination group"</h2>
<ul>
<li>To monitor the Linux certification process and adjust it as
  necessary.

<li>To implement the CERN Linux certification process. Members of the
    group will
    <ul>
    <li>jointly take decisions on which vendor release to certify
    <li>jointly declare start and end of certifications
	<li>individually ensure that the interests of the CERN Linux user
		      communities they are representing are taken into account in the certification
</ul>
<li>to decide on CERN-wide Linux issues, after identifying and consulting with stakeholders outside of this committee.
</ul>

<h2>Members</h2>
<!--#include virtual="membership" -->

<h2>Next meeting</h2>
<p>(no date has been set for the next meeting)</p>
<!--
<table border="0">
<tr><th>Date: <td>Wednesday 29.08.2007, 14:00
<tr><th>Place: <td>513-1-23
<tr><th>VRVS:  <td>(none requested yet)
<tr><th valign="top">Topic: <td>
<ol>
 <li>Presentation: Overview of possible options for SLC5 by Linux Support
 <li>Discussion
</ol>
</table>
-->


<h2>Past meetings</h2>
<ul>
<li><a href="minutes-20021017.txt">Minutes of the meeting on October 17th 2002</a>, <a href="lxcert-coord-proposal.txt">Proposals</a> (most which were accepted)
<li><a href="minutes-20021031.txt">Minutes of the meeting on October 31st 2002</a>
<li><a href="minutes-20021114.txt">Minutes of the meeting on November 14th 2002</a> (updated after correction from M.Cattaneo and N.de Metz-Noblat, updated again after correction from A.Bland)
<li><a href="minutes-20031002.txt">Minutes of the meeting on October 2nd 2003</a> 
<li><a href="20040309/minutes">Minutes of the meeting on March 9th 2004</a>
<li><a href="20040504/minutes">Minutes of the meeting on May 4th
2004</a>,
  <a href="20040504/ORACLE-on-CEL3.ppt">Slides<a> from J.Shiers'
presentation (<a href="20040504/ORACLE-on-CEL3/ORACLE-on-CEL3.html">HTML</a>)

<li><a href="20040706/minutes">Minutes of the meeting on July 6th 2004</a>
<li><a href="20050421/minutes">Minutes of the meeting on April 21st 2005</a>, <a href="20050421/postmortem/">SLC3 post-mortem slides</a></li>
<li><a href="20051004/minutes">Minutes of the meeting on October 04th 2005</a>, <a href="20051004/SLC4-status/">SLC4 status slides</a></li>
<li><a href="20060207/minutes">Minutes of the meeting on February 07th 2006</a>, <a href="20060207/SLC4+5-status/SLC4+5-status.html">SLC4+5 status slides</a> (<a href="20060207/SLC4+5-status/SLC4+5-status.pdf">PDF</a>, <a href="20060207/SLC4+5-status/SLC4+5-status.ppt">PPT</a>)</li>
<li><a href="20060321/minutes">Minutes of the meeting on March 21st 2006</a></li>
<li><a href="20070829/minutes">Minutes of the meeting on August 29th 2007</a></li>
<li><a href="20080428/lxcert080428.txt">Minutes of the meeting on April 28th 2008</a></li>
</ul>

<h2>Mailing list</h2>
<p>For general discussions, the existing <a href="mailto:linux-certification@cern.ch">linux-certification@cern.ch</a> mailing list  ( <a
href="https://mmm.cern.ch/public/archive-list/l/linux-certification/" >
archive</a>, <a
href="https://simba2.cern.ch/"
>subscription</a> ) should be used.

<p>A separate list, <a
href="mailto:linux-certification-coordination@cern.ch"
>linux-certification-coordination@cern.ch</a> ( <a
href=" https://mmm.cern.ch/public/archive-list/l/linux-certification-coordination/">
archive</a>) is for discussions on the certification process itself. Posting to this list is restricted.



