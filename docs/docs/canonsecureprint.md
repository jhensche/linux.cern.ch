<!--#include virtual="/linux/layout/header" -->
# Using Canon 'SecurePrint' on Linux

Most Canon multifunction printers installed on CERN site do support
'SecurePrint' feature: a document is sent to printer, where one needs
to confirm the printout by specifying a passcode (PIN).
<p>

<h2>Software installation</h2>

As root on your CC7 or SLC6 system run:
<pre>
&#35; yum install lpadmincern
</pre>
(it may already be preinstalled on your system: please make sure that the version is at least: <b>1.3.8-1</b>)

once installation of required software packages finishes, configure your printers using <a href="../printing">lpadmincern</a></b>.


<h2>Usage</h2>
<table>
  <tbody>
   <tr>
    <td><h4>Most system tools (including Firefox / Thunderbird)</h4></td>
    <td>&nbsp;</td>
   </tr>
   <tr>
    <td> <a target="snapwindow" href="../secprint/secprint1.png"><img src="../secprint/secprint1.png" width="300"></a> </td>
    <td>Select <b>Print</b> then choose one of Canon printers - <b>XX-XXX-CAN</b></td>
   </tr>
   <tr>
    <td> <a target="snapwindow" href="../secprint/secprint2.png"><img src="../secprint/secprint2.png" width="300"></a> </td>
    <td>In <b>Advanced</b> tab<br>
        Enter your  <b>Secured Password</b> as maximum <b>7 digits</b> and click <b>Print</b><br>
   </tr>
   <tr>
    <td><h4>Command line printing</h4></td>
    <td>&nbsp;</td>
   </tr>
   <tr>
    <td>&nbsp;</td>
    <td>Use <b>SecuredPassword</b> option:
      <pre>
         lpr -P 31-2COR-CAN -o SecuredPassword=1234 yourfile.pdf
      </pre>
    </td>
   </tr>
 </tbody>
 </table>
  </tbody>
</table>
<p>
<em>Note:</em> It is not possible to use 'SecurePrint' directly in current version of LibreOffice, as workaround
please first 'print to file' and print resulting file using above mentioned command line.

<div style="text-align: right;">
 <a href="mailto:%20Jaroslaw.Polok@cern.ch">Jaroslaw.Polok@cern.ch</a><br>
</div>

