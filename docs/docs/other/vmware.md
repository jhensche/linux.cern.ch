<!--#include virtual="/linux/redhat7/header-redhat7" -->
<h3><font color="#ff0000">UNSUPPORTED PROJECT DOCUMENTATION provided "AS-IS"</font></h3>
<hr>

<h2>VmWare status and setup on CERN RedHat Linux</h2>

<p> VMware is a commercial product and there is <b>no</b> site licence at
CERN. One needs an individual licence for each workstation on which it
runs.

<h3>Status</h3>

<p>Following a pilot project and user feedback, IT support for VmWare
is limited to handling the purchases of licenses.

<p>Please contact the CERN <a
href="http://product-support.web.cern.ch/product-support/SLO.html">Software
Licence Office</a> which you can ask for the latest price and through
which (with your budget code) purchase licenses and media
kits. Otherwise users can do an individual purchase themselves.

<p>All support questions should be directed to <a
href="http://vmware.com">VmWare.com</a>, from where you can also
download RPMs. Current IT policy recommends to use NICE2k on the
desktops, and to connect to central LINUX servers using Exceed.





<h3>Support</h3>

This page and all the software available are provided "AS IS". IT does
<b>not</b> provide any support for the VmWare application on Linux,
and neither does IT support the NICE2000/XP installation inside a
VmWare virtual machine. A widespread deployment at CERN would also cause
problem with the IP address allocation, which is based on the number
of physical ports per building.

<p>VmWare support three kinds of networking for the virtual machine:
<ul>
<li>Host-only: the virtual machine can only connect to its host. This
is secure, but severly limits functionality (e.g. no DFS)

<li>NAT: the virtual machine is hidden behind a NATted firewall, and
can access other machines on the network. This breaks Win2k SMS,
wherefore these machines can not be upgraded automatically in case of
security problems. This option is <u>not acceptable</u> on the CERN network.

<li>full IP connectivity: this enables SMS to work, but requires a
separate IP address for the virtual machine. Due to the way IP
addresses are distributed over the CERN site, this means that most IP
addresses taken up by a virtual machine will leave a physical network
outlet unusable, therefore this prevents widespread usage at CERN.
</ul>

<p>If you have purchased a license, here are some tips to get you
started:
<ul>
<li>the VmWare kernel modules need recompiling against the CERN
kernel: install the kernel-headers package and run
<pre>
&#35; /usr/bin/vmware-config.pl
</pre>
<li>a dual-boot configuration (Win2k installed in a separate
partition) has the advantage that in case of problems you can reboot
into a native Win2k environment
<li>The Win2k OpenAFS client provides a convenient way to share
files. In this case, you do not have to set up a Samba server on your
host. This can easily be installed from the NICE2k software
installation dialogue.

<li>The newsgroups
<ul>
<li>news.vmware.com:vmware.for-Linux.configuration
<li>news.vmware.com:vmware.for-Linux.experimental
<li>news.vmware.com:vmware.for-Linux.general
</ul>
have been reported to be fairly useful when solving problems.

<li>Thanks to J.Lewis, we have a <a href="../vmware-support.txt">mail</a>
from VMware support for dealing with crashing "natd"s -- but please
remember that NAT is not recommended for use at CERN.

</ul>


<h3>History</h3>

<p>In Summer 2001, IT-PDP ran a pilot project for VmWare at CERN,
including ready-to-install RPMs with a NICE2000 environment. Despite
announcement through official channels (CLUG,DTF,FOCUS), user participation
was disappointingly low (rounded avg. = 0, see <a
href="http://asis.web.cern.ch/asis/stats/applications/last-30-days.html">ASIS
usage monitoring</a>).

<p>This pilot project has since been stopped (the documentation is
still <a
href="http://linux.web.cern.ch/linux/winlin/pilot/pilot.html">available</a>).
Both project members have since left CERN.

<p>A status summary as presented to the <a
href="http://mgt-desktop-forum.web.cern.ch/mgt-desktop-forum/">DTF</a>
on March 27th 2002 can be found at <a href="http://home.cern.ch/iven/presentations/dtf-27.03.02/">http//home.cern.ch/iven/presentations/dtf-27.03.02/</a>.

<p>An update for the <a
href="http://mgt-desktop-forum.web.cern.ch/mgt-desktop-forum/">DTF</a> on June 26th is at <a href="http://home.cern.ch/iven/presentations/dtf-26.06.02/">http//home.cern.ch/iven/presentations/dtf-26.06.02/</a>.

<hr>
<h3><font color="#ff0000">UNSUPPORTED PROJECT DOCUMENTATION provided "AS-IS"</font></h3>
<!--#include virtual="/linux/redhat7/footer-redhat7" -->
