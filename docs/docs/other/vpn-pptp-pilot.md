<!--#include virtual="/linux/layout/header" -->
# CERN VPN pilot setup on CERN Linux 7.2.1/7.3.1

<h3>Prerequisites</h3>
<ul>
<li>
Read <a href="http://vpn.web.cern.ch/vpn/">CERN VPN pilot documentation</a>.
</li>
<li>
VPN pilot registration (<a href="http://vpn.web.cern.ch/vpn/registration/">VPN registration page</a>)
</li>
<li>
Kernel support for MPPE protocol (Microsoft Point-to-Point Encryption)
<br>
<li>
PPP daemon support for MS-CHAPv2 (Microsoft Challenge Handshake Authentication Protocol version 2) and MPPE
<br>
</li>
<li>
PPTP software (Point-to-Point Tunneling Protocol)
</li>
<li>
Working Internet connection ;-)
<br>
We cannot provide that, you must setup it yourself ;-)... (Please note that 33Kbps modem connection will work ... but SLOW, MPPE encryption adds significient overhead ..)
</li>
</ul>
<br>
<br>
Precompiled (for Red Hat Linux 7.2) kernel, pptp-linux and ppp rpms are available at:
<a href="http://cern.ch/linux/pilot/vpn-pptp/RPMS/" target="files">/afs/cern.ch/project/linux/redhat/pilot/vpn-pptp/RPMS/</a>
<br>
<br>
Source rpms for above are available at:
<a
href="http://cern.ch/linux/pilot/vpn-pptp/SRPMS/" target="files">/afs/cern.ch/project/linux/redhat/pilot/vpn-pptp/SRPMS/</a>.
<br>
<br>
Patches applied to the standard Red Hat packages are here: <a
href="http://cern.ch/linux/pilot/vpn-pptp/patches/"
target="files">/afs/cern.ch/project/linux/redhat/pilot/vpn-pptp/patches/</a>
<br>
(In case you would like to try on a system different than CERN Linux 7.2.1/7.3.1)
<br>

<h3>Setup</h3>
<center>
<font color="red">Please note: CERN Linux 7.3.1 distribution includes all necessary software<br>
so if you are using this distribution please skip to <a href="#pptpconfig">PPTP configuration</a> section.</font>
</center>
<br>
Once you've downloaded all above software proceed (as root
on your machine) to the
<h4>Software installation</h4>
Install the kernel:
<pre>
&#35; rpm -ivh yourkernel-version.rpm
</pre>
(It will be installed in addition to your current kernel)<br>
Install the pppd:
<pre>
&#35; rpm -Uvh ppp-*.cernmppe.i386.rpm
</pre>
(It will REPLACE your current ppp software)<br>
Install pptp-linux:
<pre>
&#35; rpm -ivh pptp-linux*rpm
</pre>
<br>
Don't forget to edit your <i>/etc/modules.conf</i> file: Add a line saying:
<pre>alias ppp-compress-18 ppp_mppe</pre> there.
<br>
Next reboot your machine and select 2.4.9-31.1.cernmppe kernel on GRUB
boot selection screen. (you can make this kernel the default one editing
<i>/etc/grub.conf</i>).
<br>
<a name="pptpconfig"></a><h4>PPTP configuration</h4>
Following is a screen dump of configuration utility. User input is marked
in <font color="#00ff00">green</font>.
<pre>
[root@zlom root]&#35; /usr/sbin/pptp-command
1.) start
2.) stop
3.) setup
4.) quit
What task would you like to do?: <font color="#00ff00">3</font>
1.) Manage CHAP secrets
2.) Manage PAP secrets
3.) List PPTP Tunnels
4.) Add a NEW PPTP Tunnel
5.) Delete a PPTP Tunnel
6.) Configure resolv.conf
7.) Select a default tunnel
8.) Quit
?: <font color="#00ff00">1</font>
1.) List CHAP secrets
2.) Add a New CHAP secret
3.) Delete a CHAP secret
4.) Quit
?: <font color="#00ff00">2</font>
Add a NEW CHAP secret.

NOTE: Any backslashes (\) must be doubled (\\).

Local Name:

This is the 'local' identifier for CHAP authentication.

NOTE: If the server is a Windows NT machine, the local name
          should be your Windows NT username including domain.
          For example:

                  domain\\username

Local Name: <font color="#00ff00">CERN\\nicelogin</font>

Remote Name:

This is the 'remote' identifier for CHAP authentication.
In most cases, this can be left as the default. It must be
set if you have multiple CHAP secrets with the same local name
and different passwords. Just press ENTER to keep the default.

Remote Name [PPTP]:<font color="#00ff00">PPTP</font>

Password:

This is the password or CHAP secret for the account specified. The
password will not be echoed.

Password: <font color="#00ff00">************* (your NICE password)</font>
Adding secret CERN\\nicelogin PPTP ***********

1.) List CHAP secrets
2.) Add a New CHAP secret
3.) Delete a CHAP secret
4.) Quit
?: <font color="#00ff00">4</font>
1.) Manage CHAP secrets
2.) Manage PAP secrets
3.) List PPTP Tunnels
4.) Add a NEW PPTP Tunnel
5.) Delete a PPTP Tunnel
6.) Configure resolv.conf
7.) Select a default tunnel
8.) Quit
?: <font color="#00ff00">4</font>

Add a NEW PPTP Tunnel.

1.) Other
Which configuration would you like to use?: <font color="#00ff00">1</font>
Tunnel Name: <font color="#00ff00">CERNVPN</font>
Server IP: <font color="#00ff00">cernvpn.cern.ch</font>
What route(s) would you like to add when the tunnel comes up?
This is usually a route to your internal network behind the PPTP server.
You can use TUNNEL_DEV and DEF_GW as in /etc/pptp.d/ config file
TUNNEL_DEV is replaced by the device of the tunnel interface.
DEF_GW is replaced by the existing default gateway.
The syntax to use is the same as the route(8) command.
Enter a blank line to stop.
route: <font color="#00ff00">add -host cernvpn.cern.ch gw DEF_GW</font>
route: <font color="#00ff00">add -net 137.138.0.0 netmask 255.255.0.0 TUNNEL_DEV</font>
route: <font color="#00ff00">add -net 128.141.0.0 netmask 255.255.0.0 TUNNEL_DEV</font>
route: <font color="#00ff00">add -net 128.142.0.0 netmask 255.255.0.0 TUNNEL_DEV</font>
route: <font color="#00ff00">add -net 172.17.0.0 netmask 255.255.0.0 TUNNEL_DEV</font>
route:
Local Name and Remote Name should match a configured CHAP or PAP secret.
Local Name is probably your NT domain\username.
NOTE: Any backslashes (\) must be doubled (\\).

Local Name: <font color="#00ff00">CERN\\nicelogin</font>
Remote Name [PPTP]: <font color="#00ff00">PPTP</font>
Adding CERNVPN - cernvpn.cern.ch - CERN\\nicelogin - PPTP
Added tunnel CERNVPN
1.) Manage CHAP secrets
2.) Manage PAP secrets
3.) List PPTP Tunnels
4.) Add a NEW PPTP Tunnel
5.) Delete a PPTP Tunnel
6.) Configure resolv.conf
7.) Select a default tunnel
8.) Quit
?: <font color="#00ff00">7</font>
1.) CERNVPN
2.) cancel
Which tunnel do you want to be the default?: <font color="#00ff00">1</font>
1.) Manage CHAP secrets
2.) Manage PAP secrets
3.) List PPTP Tunnels
4.) Add a NEW PPTP Tunnel
5.) Delete a PPTP Tunnel
6.) Configure resolv.conf
7.) Select a default tunnel
8.) Quit
?: <font color="#00ff00">8</font>
[root@zlom root]#

</pre>
Notes:
<ul>
<li>Above configuration encrypts and routes through the tunnel
ONLY your communication channels to certain CERN networks.Please make sure
you route to all CERN networks that have services you are interested in.
The full list of CERN networks can be found on the
<a href="http://network.cern.ch/sc/fcgi/sc.fcgi?Action=GetFile&file=ip_networks.html">IT-CS groub web page</a>.
<br>All other traffic
from your machines goes unencrypted over your internet connection
to the provider.

<li>some of the CERN networks use the non-routed reserved IP address blocks
(10.X.X.X, 192.168.X.X). These may overlap with the IP address assigned to
you by your ISP, and we recommend against adding them to your list above as
results will be unpredictable.

<li>Your NICE password is stored in CLEARTEXT in /etc/ppp/chap-secrets. (This
could be a security problem on multiuser machines)
</ul>

<h4>Test it</h4>
Run
<pre>
[root@zlom root]&#35; /usr/sbin/pptp-command start
</pre>
You should see the output similar to the following:
<pre>
Route: add -host 137.138.143.164 gw 80.13.182.1 added
Route: add -net 137.138.0.0 netmask 255.255.0.0 ppp1 added
Route: add -net 128.141.0.0 netmask 255.255.0.0 ppp1 added
Route: add -net 128.142.0.0 netmask 255.255.0.0 ppp1 added
All routes added.
Tunnel CERNVPN is active on ppp1.  IP Address: 137.138.143.183
</pre>

To verify that your tunnel is running you may try:
<pre>
root@zlom root]&#35; /usr/sbin/traceroute www.cern.ch
traceroute to webr2.cern.ch (137.138.28.230), 30 hops max, 38 byte packets
 1  cernvpn01-001.cern.ch (137.138.143.180)  66.175 ms  68.493 ms  71.940 ms
 2  b513-c-rca86-2-ip72.cern.ch (137.138.143.129)  71.880 ms  68.527 ms  74.645 ms
 3  b513-b-rca86-1-bb2.cern.ch (194.12.131.9)  77.874 ms  67.394 ms  76.174 ms
 4  b513-c-rca86-1-bb1.cern.ch (194.12.131.6)  71.495 ms  68.100 ms  75.457 ms
 5  webr2.cern.ch (137.138.28.230)  74.147 ms  68.103 ms  73.500 ms
</pre>

Your first hop on the route should be named  cernvpn01-XXX.
<br>
To stop the tunnel use:
<pre>
[root@zlom root]&#35; /usr/sbin/pptp-command stop
Sending HUP signal to PPTP processes...
[root@zlom root]&#35;
</pre>
To see the tunnel state use:
<pre>
[root@zlom root]&#35; /usr/sbin/pptp-command status
There is probably not a pptp tunnel up
[root@zlom root]&#35;
</pre>
(As you may see even on the above output the detection is somehow flaky ...)
<br>
<b>NOTE:</b>pptp-command is not very clever: watchout for multiple starting
of the tunnel ...

<h3>Troubleshooting</h3>

- Are you registered ? <br>
- Have you supplied correct NICE userid and password ? <br>
- Is your underlying internet connection working ?<br>
- Remeber: VPN pilot server serves only a COUPLE of connections simultaneously:  retry later ..<br>
- Maybe the server isn't working at the time you try ? REMEMBER: this is PILOT not regular service<br>
- Debug your connection attempts:<br>
  Add <i>debug</i> keyword in <i>/etc/ppp/options.pptp</i><br>
  Edit your <i>/etc/syslog.conf</i> to contain line: <i>*.* /var/log/messages</i><br>
  restart syslog: <i>/sbin/service syslog restart</i><br>
Watch the debug output:<i>tail -f /var/log/messages</i><br> during subsequent attempts<br>
<br>
- Consult the documentation: <a href="http://pptpclient.sourceforge.net/documentation.phtml" target="_new">http://pptpclient.sourceforge.net</a>.
<br>

- What works over the tunnel?: Everything using IP should work - let me know about any exceptions you find ..

<h3>Support</h3>
In case of problems please report to <a href="mailto: linux.support@cern.ch">linux.support@cern.ch</a> - and please include output from <i>/var/log/messages</i> - after switching on debugging following the recipe above !):

<div align="right"><a href="mailto:Jaroslaw.Polok@cern.ch">Jaroslaw.Polok@cern.ch</a></div>

