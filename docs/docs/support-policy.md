<!--#include virtual="/linux/layout/header" -->
# Support policy
<h2>CERN Linux Support policy</h2>

<h2>Mandate</h2>
<p>To provide a secure version of the Linux operating environment
that allows CERN staff and users to perform their (physics-related) work on
Linux, according to the goals of the organization.</p>
<p>Tasks:</p>
<ul>
  <li><b>Assemble and certify a CERN Linux distribution</b> together with the <br>
  CERN Linux user community (represented by Linux Certification Committee)</li>
  <li><b>Run central Linux installation servers</b> to make this
distribution available on the CERN site.</li>
  <li><b>Provide security updates and a deployment mechanism</b> for
the CERN Linux distribution, suitable for servers and managed desktops.
  <li><b>Provide general Linux support</b> as detailed below.</li>
</ul>
<h2>Support for CERN Linux distributions</h2>
<p>Support calls will enter the Problem Tracking system via <a href="mailto:helpdesk@cern.ch">helpdesk</a>.
<br>
Urgent production issues inside IT department may be reported directly to the Linux Support 3<sup>rd</sup> level team.
<br>
Shortcutting the support structure (e.g. direct phone calls to 3<sup>rd</sup> level team members) is discouraged.</p>
<p>Linux system installations (including documented workarounds) and recurring documented issues will be handled by the (outsourced) 2<sup>nd</sup> level support, who will forward other <a href="../required_for_support">completed</a> calls to Linux Support 3rd level team.</p>
<p>Support calls will be assigned a priority that takes into account
the impact of the problem for CERN (the reporter's opinion on the
priority will be considered, but may be overridden). Priorities affect
the order in which calls are handled, low-priority calls may be closed
without a satisfactory resolution if resources to answer them properly
are not available.</p>
<p>The following will <b>increase</b> the priority: </p>
<ul>
  <li>Issues directly affecting operation of accelerators and data-taking.</li>
  <li>Issues (potentially) affecting a large number of users.
  <li>Problems related to Servers and Managed Non-personal Desktops.</li>
  <li>Recurring and reproducible issues.</li>
</ul>
<p>Each of the following will <b>lower</b> the priority: </p>
<ul>
  <li>Non-standard PC hardware purchased outside CERN stores. Please note that even machines bought in the CERN Stores have not been tested for Linux compatibility by the Linux Support Team.</li>
  <li>Problems with add-on hardware or gadgets (local printers, Webcams, PDAs, USB storage, CD/DVD burners, ...)</li>
  <li>Problems with add-on software or different versions than deployed in the CERN distribution</li>
  <li>Per-user configuration issues</li>
  <li>Languages or fonts beyond official CERN languages (English and French).</li>
  <li>Requests from outside of CERN (CERN does not support outside institutes, even if they are running our Linux distribution. In case this isn't clearly stated otherwise in the call, a non-CERN mail address will be taken as a request from outside of CERN.<br>
  </li>
</ul>
<strong>Escalation:</strong> In case of problems with the support
service, please contact the IT <a href="mailto:service-manager-on-duty@cern.ch">Manager On Duty</a>.

<!--
<hr>
<h2>Support for Red Hat licensed products (Enterprise/Advanced server)</h2>
<p>The only reason to run a licensed Red Hat product at CERN is <u>formal
support</u> for 3<sup>rd</sup>-party products. The CERN Linux
distributions are technically equivalent to the Red Hat products as
they (to a large degree) contain the same packages or recompiled
versions of the same packages. Software running on a Red Hat product
will run typically on the CERN Linux distribution (otherwise please
file a bug report against the CERN version). Non-Red Hat kernel modules
(OpenAFS, NVIDIA graphics card driver, updated vendor drivers like
3w-xxxx or e1000) will void the support agreement and have to be
avoided on these machines.</p>
<p>If the vendor insists on tying support to a valid OS license or
distribution name, a "test system" can usually be used to reproduce
bugs on the officially supported platform.</p>
<p>Therefore, IT-FIO <b>does not offer a general support</b> for Red
Hat products. The following exceptions exist:</p>
<ul>
  <li>ORACLE production servers, run by IT-FIO/IT-DES<br>
(ORACLE support insists on either SuSE or Red Hat as the operating
system, since ORACLE acts as central point of contact and uses the
vendor support itself). IT-FIO is involved due to the
number of machines.
    <ul>
      <li>currently running Red Hat Advanced server 2.1</li>
      <li>IT-FIO provides the RedHat installation tree locally on <tt>linuxsoft.cern.ch/enterprise</tt>
(NFS, access restricted by IP address)</li>
      <li>IT-FIO installs these servers and applies updates via Quattor
tools.</li>
      <li>all support issues <a
 href="http://www.redhat.com/solutions/partners/oracle/page3.html">are
to be addressed via the ORACLE support contract</a></li>
    </ul>
  </li>
  <li>RHE3-WS certification systems for EGEE (supposedly temporary, de-facto for EGEE lifetime)
    <ul>
      <li>temporary machines to allow EGEE development to start,
assumed to switch to SLC3 after its release</li>
      <li>distribution is available on <tt>linuxsoft</tt></li>
      <li>Bug reports: directly to Red Hat, escalations: via
IT-FIO-LA/TAM</li>
      <li>no update service, machine are assumed to go directly to RHN
for updates.</li>
    </ul>
  </li>
</ul>
<p>All currently available Red Hat products require a valid license
("entitlement"). <b>Do not install these products unless you have such
a license.</b> This license allows you to retrieve security updates
directly from the <a href="https://rhn.redhat.com">Red Hat</a>, and
bugs can be reported via <a href="https://bugzilla.redhat.com">Red Hat
Bugzilla</a>, even without a SLA-based support agreement.</p>
<p>Red Hat licenses may be purchased centrally through IT's <a
 href="http://product-support.web.cern.ch/product-support/SLO.html">Software
License Office</a>. </p>
-->

<hr>
<h2>Support for other Linux distributions</h2>

<p>IT-CM does only support <b>CentOS and RHEL Linux distributions on servers and managed desktops</b>. <b>No support at all</b> will be provided for support calls for other Linux distributions opened via Helpdesk or direct mail to Linux.Support@cern.ch. Users of CERN unsupported Linux distributions, as well as users of Laptops and non-managed Desktops, will be encouraged to report their issues in the <a href="https://linux-community.web.cern.ch/">CERN Linux Community forum</a>.

If resources permit, calls for recent Linux distributions may be
answered, but substantial effort to diagnose the problem by the user
is assumed to occur before, and such calls may be closed anytime
without a satisfactory resolution.</p> <p>Users are instead invited to
ask their questions on <a
href="https://cern.ch/cern.linux">cern.linux</a>, in the hope that
other CERN users may be able to help, or use the mailing lists more
appropriate to the distribution they are running.</p>

<hr>
<h2>Security</h2>
<p>As a reminder, the <a href="http://cern.ch/ComputingRules/">CERN
computing rules</a> requires a user to </p>
<pre>[...] take the necessary precautions to protect his personal computer<br>or work station against unauthorized access.</pre>
System administrators of "file servers" (FTP, HTTP, NFS, AFS, etc)
<pre> [...] must take proactive and adequate measures to protect the operating system and<br>applications from known security weaknesses.</pre>
This responsibility stays with the owner/user/persons defined in <a
 href="http://cern.ch/network">LanDB</a>. Running a CERN-supported
Linux version with automatic updates enabled will be considered to be
sufficient to protect a machine, as long as requests for special
actions (e.g. manual kernel upgrades) are followed timely. Such
requests will be sent to <tt>root</tt> on the machine concerned
(please configure your machine so that some human will read these
mails), as well as published in the <a
 href="https://e-groups.cern.ch/e-groups/Egroup.do?egroupId=161390">linux-announce@cern.ch</a>
mailing list
and on the <a href="http://cern.ch/linux/news">CERN
Linux homepage</a>.

