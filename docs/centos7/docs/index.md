# CERN CentOS 7 Documentation

<ul>
    <li><a href="./install">Installation instructions</a>
        <ul>
            <li><a href="./bootmedia">Boot Media preparation</a>
            <li><a href="./kickstart">Simple Kickstart file example</a>
        </ul>
    <li><a href="../hardware">Supported hardware</a>
    <li><a href="./docker">Using Docker on CentOS 7</a>
    <li><a href="./softwarecollections">Software Collections - addtional software for CentOS 7</a>
</ul>
Please check <a href="../../docs/">Documentation</a> for additional documentation.
