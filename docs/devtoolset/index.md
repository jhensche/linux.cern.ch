<!--#include virtual="/linux/layout/header" -->
# Developer Toolset
<p>
<hr>
<center><em>Please note</em>: Developer Toolset v. 2.0/2.1 has been superceeded by <a href="../other">Developer Toolset 3.1 on SLC6/CC7</a><p>This documentation page applies only to SLC5 as of Dec 2015.</center>
<hr>
<ul>
<li><a href="#dts30">Developer Toolset 3.0</a>
<li><a href="#dts21">Developer Toolset 2.1</a>
<li><a href="#dts2">Developer Toolset 2.0</a>
<li><a href="#dts11">Developer Toolset 1.1</a>
<li><a href="#install">Installation</a>
</ul>
<hr>
Developer Toolset is an offering for developers on the Scientific Linux CERN 5 and 6 platforms. Using a framework called Software Collections, an additional set of tools is installed into the /opt directory, as recommended by the UNIX Filesystem Hierarchy Standard. These tools are enabled by the user on demand using the supplied <b>scl</b> utility.
<p>
Develper Toolset packages are provided here for <em>TEST PURPOSE ONLY</em>.
</p>
(More information about <a href="https://access.redhat.com/knowledge/docs/Red_Hat_Developer_Toolset/">Developer Toolset</a> on Red Hat site)
</p>
<p>
Please note that <em>ONLY</em> support we provide for Developer Toolset packages is related
to installation / packaging problems. This product is provided AS-IS - without support.
</p>
<p>
For everything else, <b>please read the documentation</b>.
</p>
<hr><a name="dts30"></a>
<h2>Developer Toolset 3.0</h2>

Developer Toolset <b>3.0</b> is distributed as part of Software Collections, please
read the documentation for <a href="/scl/">Software Collections</a>.

<hr><a name="dts21"></a>
<h2>Developer Toolset 2.0 updated to 2.1</h2>

Developer Toolset <b>2.1</b> provides new tools:
<ul>
<li><b>git</b> - GIT - version <b>1.8.4</b></li>
</ul>
Developer Toolset <b>2.1</b> provides also updated tools:
<ul>
<li><b>gcc</b>/<b>g++</b>/<b>gfortran</b> - GNU Compiler Collection - version <b>4.8.2</b></li>
<li><b>eclipse</b> - An Integrated Development Environment - version <b>4.3.1</b> (Kepler)</li>
</ul>

<b>Note:</b> Please note 2.1 is an upgrade of 2.0 and can't be installed in parrallel.

<h3>Documentation</h3>
<h4>Developer Toolset 2.1 documentation (Local Copy) </h4>
<ul>
<li><a href="Red_Hat_Developer_Toolset-2-2.1_Release_Notes-en-US.pdf">2.1 Release Notes</a></li>
<li><a href="Red_Hat_Developer_Toolset-2-Software_Collections_Guide-en-US.pdf">Software Collections Guide</a></li>
<li><a href="Red_Hat_Developer_Toolset-2-User_Guide-en-US.pdf">User Guide</a></li>
</ul>
Full Developer Toolset documentation can be found <a href="https://access.redhat.com/knowledge/docs/Red_Hat_Developer_Toolset/">on RedHat site</a>.

<hr><a name="dts2"></a>
<h2>Developer Toolset 2.0</h2>

Developer Toolset <b>2.0</b> provides following tools:
<ul>
<li><b>gcc</b>/<b>g++</b>/<b>gfortran</b> - GNU Compiler Collection - version <b>4.8.1</b></li>
<li><b>gdb</b> - GNU Debugger - version <b>7.6.34</b></li>
<li><b>binutils</b> - A GNU collection of binary utilities - version <b>2.23.52</b></li>
<li><b>elfutils</b> - A collection of utilities and DSOs to handle compiled objects - version <b>0.155</b></li>
<li><b>dwz</b> - DWARF optimization and duplicate removal tool - version <b>0.11</b></li>
<li><b>systemtap</b> - Programmable system-wide instrumentation system - version <b>2.1</b></li>
<li><b>valgrind</b> - Tool for finding memory management bugs in programs - version <b>3.8.1</b></li>
<li><b>oprofile</b> - System wide profiler - version <b>0.9.8</b></li>
<li><b>eclipse</b> - An Integrated Development Environment - version <b>4.3</b> (Kepler)</li>
</ul>
<b>Note:</b> Eclipse IDE is available <b>ONLY</b> on SLC6.

<h3>Documentation</h3>
<h4>Developer Toolset 2.0 documentation (Local Copy) </h4>
<ul>
<li><a href="Red_Hat_Developer_Toolset-2-2.0_Release_Notes-en-US.pdf">2.0 Release Notes</a></li>
<li><a href="Red_Hat_Developer_Toolset-2-Software_Collections_Guide-en-US.pdf">Software Collections Guide</a></li>
<li><a href="Red_Hat_Developer_Toolset-2-User_Guide-en-US.pdf">User Guide</a></li>
</ul>
Full Developer Toolset documentation can be found <a href="https://access.redhat.com/knowledge/docs/Red_Hat_Developer_Toolset/">on RedHat site</a>.

<hr><a name="dts11"></a>
<h2> Developer Toolset 1.1</h2>

Developer Toolset <b>1.1</b> provides following tools:
<ul>
<li><b>gcc</b>/<b>g++</b>/<b>gfortran</b> - GNU Compiler Collection - version <b>4.7.2</b></li>
<li><b>gdb</b> - GNU Debugger - version <b>7.5</b></li>
<li><b>binutils</b> - A GNU collection of binary utilities - version <b>2.23.51</b></li>
<li><b>elfutils</b> - A collection of utilities and DSOs to handle compiled objects - version <b>0.154</b></li>
<li><b>dwz</b> - DWARF optimization and duplicate removal tool - version <b>0.7</b></li>
<li><b>systemtap</b> - Programmable system-wide instrumentation system - version <b>1.8</b></li>
<li><b>valgrind</b> - Tool for finding memory management bugs in programs - version <b>3.8.1</b></li>
<li><b>oprofile</b> - System wide profiler - version <b>0.9.7</b></li>
</ul>
<h3>Documentation</h3>
<h4>Developer Toolset 1.1 documentation (Local Copy) </h4>
<ul>
<li><a href="Red_Hat_Developer_Toolset-1-1.1_Release_Notes-en-US.pdf">1.1 Release Notes</a></li>
<li><a href="Red_Hat_Developer_Toolset-1-Software_Collections_Guide-en-US.pdf">Software Collections Guide</a></li>
<li><a href="Red_Hat_Developer_Toolset-1-User_Guide-en-US.pdf">User Guide</a></li>
</ul>
Full Developer Toolset documentation can be found <a href="https://access.redhat.com/knowledge/docs/Red_Hat_Developer_Toolset/">on RedHat site</a>.

<hr><a name="install"></a>
<h2>Installation</h2>


<ul>
 <li>Scientific Linux 6 (SLC6)<br>
     Save repository information as <a href="http://linuxsoft.cern.ch/cern/devtoolset/slc6-devtoolset.repo">/etc/yum.repos.d/slc6-devtoolset.repo</a> on your system:<pre>
wget -O /etc/yum.repos.d/slc6-devtoolset.repo http://linuxsoft.cern.ch/cern/devtoolset/slc6-devtoolset.repo
</pre>
 <li>Scientific Linux 5 (SLC5)<br>
    Save repository information as <a href="http://linuxsoft.cern.ch/cern/devtoolset/slc5-devtoolset.repo">/etc/yum.repos.d/slc5-devtoolset.repo</a> on your system:<pre>
wget -O /etc/yum.repos.d/slc5-devtoolset.repo http://linuxsoft.cern.ch/cern/devtoolset/slc5-devtoolset.repo
</pre>
</ul>

To install the developer toolset 2.0 run:
<pre>
yum install devtoolset-2
</pre>
To test installed environment:
<pre>
$ scl enable devtoolset-2 bash
$ gcc --version
gcc (GCC) 4.8.1 20130715 (Red Hat 4.8.1-4)
Copyright (C) 2013 Free Software Foundation, Inc.
This is free software; see the source for copying conditions.  There is NO
warranty; not even for MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
</pre>

To install the developer toolset 1.1 run:
</p>
<pre>
yum install devtoolset-1.1
</pre>

To test installed environment:
<pre>
$ scl enable devtoolset-1.1 bash
$ gcc --version
gcc (GCC) 4.7.2 20121015 (Red Hat 4.7.2-5)
Copyright (C) 2012 Free Software Foundation, Inc.
This is free software; see the source for copying conditions.  There is NO
warranty; not even for MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
</pre>

<div align="right"><a href="mailto:Linux.Support@cern.ch">Linux.Support@cern.ch</a></div>

