<!--#include virtual="/linux/layout/header" -->
# LibreOffice for CERN CentOS 7 / Scientific Linux CERN 6

## LibreOffice version 6.1

SLC6 and CC7 distributions include older version of LibreOffice (4.3 and 5.3)
This document explains how to install latest stable version of LibreOffice on your system.

<h2>Installation</h2>
As root on your system run:
<pre>
&#35; yum install libreoffice-release
&#35; yum groupinstall "LibreOffice 6"
</pre>
Above commands will install default set of LibreOffice packages, additional
packages can be installed individually by running:
<pre>
&#35; yum install <b>packagename</b>
</pre>
to see the list of all available packages run:
<pre>
&#35; yum search libreoffice6 libobasis6
</pre>
LibreOffice 6 is installed on the system in parallel
to preexisting LibreOffice 4/5.
<h2>Usage</h2>
Select <b>LibreOffice 6.1</b> from system <b>Applications</b> menu, <b>Office</b> submenu.
Or start it from command line:
<pre>
&#35; libreoffice6.1
</pre>

