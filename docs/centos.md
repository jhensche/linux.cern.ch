# CentOS

## What is CentOS ?

From [CentOS web site](https://centos.org/):

> Community-driven free software effort focused around the goal of providing a rich platform for open source communities to build upon.

The CentOS project currently provides two Linux variants:

CentOS Linux (version 7)

> Consistent, manageable platform that suits a wide variety of deployments 

CentOS Stream (version 8 and 9)

> Continously delivered distribution that tracks just ahead of Red Hat Enterprise Linux (RHEL), positioned as a midstream between Fedora Linux and RHEL

# CentOS Linux

CentOS Linux has always been a 'rebuild' of RedHat Enterprise Linux (RHEL), meaning that the distribution was built by taking the source code from RHEL and repackaging and releasing it as CentOS Linux. For this reason CentOS Linux was always a direct copy of what existed in RHEL, without the support that is usually included with RHEL.

Previously CentOS Linux followed the same life as the upstream RHEL product, this being a 10 year life cycle.

In December 2020, the CentOS project changed their operating model and announced:

* CentOS Linux 8 would have it's end of life changed from this historic 10 year life cycle to instead 2 years (thus setting the end of life to be 31.12.2021)
* The life cycle of CentOS Linux 7 was not affected by this annoucement
* There will not be a "CentOS Linux 9", and instead all efforts from the CentOS project will be focused into CentOS Stream 

# CentOS Stream

With CentOS Stream, Red Hat is doing their development work out in the open. They will continue to take Fedora releases and perform their QA on them, but now they will assemble all their changes publicly as CentOS Stream. Every 6 months or so, Red Hat will take CentOS Stream and rebuild it as a new RHEL point release

CentOS Stream will have a life cycle of 5 years

CentOS Stream will be more up-to-date than RHEL, whereas CentOS Linux has traditionally lagged behind


## CentOS versions at CERN?

For the 7 family: **CERN CentOS (CC7)** is CERN customized distribution that is built on top of the CentOS Linux 7 and it is tailored to integrate within the CERN computing environment.

* CC7 was made available at CERN on 01.04.2015 and support will end on 30.06.2024
* More information on CC7 can be found on the dedicated [CC7](/centos7) page

For the 8 family: **CentOS Stream 8 (CS8)** is provided by using the upstream content. Integration to the CERN computing environment is still possible via the addon 'CERN' repository

* CS8 was made available at CERN on 01.04.2021
* CS8 support at CERN is [slated for decommission 30.09.2023](https://cern.service-now.com/service-portal?id=outage&n=OTG0074647). We recommend all new installs to use either AlmaLinux or Red Hat Enterprise Linux. Please refer to [Which distribution should I use](/which) if you are not sure which distribution to use.
* More information on CS8 can be found on the dedicated [CS8](/centos8) page

For the 9 family: **CentOS Stream 9 (CS9)** is provided by using the upstream content. Integration to the CERN computing environment is still possible via the addon 'CERN' repository

* CS9 was made available at CERN on 09.02.2022
* CS9 support at CERN is [slated for decommission 30.06.2023](https://cern.service-now.com/service-portal?id=outage&n=OTG0074647). We recommend all new installs to use either AlmaLinux or Red Hat Enterprise Linux. Please refer to [Which distribution should I use](/which) if you are not sure which distribution to use.
* More information on CS9 can be found on the dedicated [CS9](/centos9) page

## What happened to Scientific Linux CERN (SLC) ?

Scientific Linux CERN (SLC) was the distribution that CERN used prior to using CentOS Linux. SLC is no longer used at CERN
