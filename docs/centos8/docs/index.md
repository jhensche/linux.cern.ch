# CentOS Stream 8 Documentation
  
* [Documentation pages](/centos8/docs)
    * [Installation instructions](install.md)
    * [Step by step installation guide](stepbystep.md)
    * [Locmap installation](locmap.md)
    * [Booting into single user mode](singleuser.md)

Please check <a href="../../docs/">Documentation</a> for additional documentation.
