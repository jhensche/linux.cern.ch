# CentOS Stream 8

!!! danger "CentOS Stream 8 is due to be deprecated within the CERN environment on 30.09.2023, see [OTG0074647](https://cern.service-now.com/service-portal?id=outage&n=OTG0074647)"

## Release status

* CentOS Stream 8 was made available at CERN on 01.04.2021
* CentOS Stream 8 support at CERN is [slated for decommission on 30.09.2023](https://cern.service-now.com/service-portal?id=outage&n=OTG0074647). We recommend all new installs to use either AlmaLinux or Red Hat Enterprise Linux. Please refer to [Which distribution should I use](/which/) if you are not sure

## How does CERN use CentOS Stream 8?

### System updates

CentOS Stream 8 updates are staged

* A 'production' [repository](http://linuxsoft.cern.ch/cern/centos/s8/) is provided, which most systems will use and is updated once per week (except for critical security vulnerability patches)
* A 'testing' [repository](http://linuxsoft.cern.ch/cern/centos/s8-testing/) is also provided which is updated daily
* Snapshots of CentOS repositories are performed daily and can be found at [this URL](http://linuxsoft.cern.ch/cern/centos/s8-snapshots/)
* The 'cern-dnf-tool' is a new package that can be used to easily switch between 'production', 'testing' or 'snapshot' repositories
* The 'dnf-autoupdate' package is provided for automatic (distro-sync) updates (the same as CC7).

### Modified packages

* centos-stream-release:
    * DNF repositories files changed to use <http://linuxsoft.cern.ch/cern/centos/s8/>
* centos-gpg-keys:
    * Added CERN Koji RPM signing keys
* epel-release:
    * DNF repositories files changed to use <http://linuxsoft.cern.ch/>

**Note**: Unless otherwise stated above, CentOS Stream 8 packages (rpms) are the very same packages released by the upstream CentOS team.

## Deployment / Howto

* [Documentation pages](/centos8/docs/)
    * [Installation instructions](/centos8/docs/install/)
    * [Booting into single user mode](/centos8/docs/singleuser/)
    * [CentOS Linux 8 to CentOS Stream 8 migration](/centos8/docs/migration/).

## What's the difference between CentOS Linux and CentOS Stream?

Currently, CentOS is a rebuild of Red Hat Enterprise Linux (RHEL). In order to build RHEL, Red Hat engineers take a Fedora release, perform all their QA tests and assemble all the changes they want to make in their internal repositories. They rebuild all their packages and produce a new release of RHEL. Once this process is done, they push their sources to the CentOS public git repository. At this point, the CentOS project takes these source RPMs, rebuilds them and produces a new release of CentOS.

With CentOS Stream, Red Hat is doing their development work out in the open. They will continue to take Fedora releases and perform their QA on them, but now they will assemble all their changes publicly as CentOS Stream. Every 6 months or so, Red Hat will take CentOS Stream and rebuild it as a new RHEL point release. Once that is done, the sources of this new RHEL point release are rebuilt to produce a new CentOS release. This last step will now stop at the end of 2021.

CentOS Stream will be more up-to-date than RHEL, whereas CentOS has traditionally lagged behind.

## Does this mean that CentOS Stream is RHEL's QA?

No, Red Hat will continue their internal QA workflow. CentOS Stream will get the updates that have passed that QA process.

## Does CentOS Stream have the same 10 years of support as previous CentOS releases?

No, the major implication of this change is that support for major releases is shortend to 5 years. New major CentOS Stream releases should come out regularly with an overlap of 2-3 years, providing an upgrade path.

# References:
* [CentOS Project announcement](https://blog.centos.org/2020/12/future-is-centos-stream/)
* [CentOS Project FAQ](https://centos.org/distro-faq/)
* [Red Hat announcement](https://www.redhat.com/en/blog/centos-stream-building-innovative-future-enterprise-linux)
* [CentOS build process](https://wiki.centos.org/FAQ/General/RebuildReleaseProcess#How_is_CentOS_built.3F)
