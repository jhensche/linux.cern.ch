# CentOS Linux 8

!!! danger "Deprecated as of 31.12.2021"

## Release status
* CentOS Linux 8 @ CERN was initially introduced via an [ASDF session](https://indico.cern.ch/event/866058/) on the 12.12.2019
* CentOS Linux 8 reached it's end of life on the 31.12.2021. It is **no longer supported at CERN**

## What was CentOS Linux 8?

The CentOS Project is a community-driven free software effort focused on delivering a robust open source ecosystem around a Linux platform. Packages contained in CentOS Linux are built from source code from the RedHat Enterprise Linux (RHEL) distribution. More information on the CentOS project can be found upstream at the [CentOS website](https://www.centos.org/).

## Change of end-of-life of CentOS Linux 8

On the 8th of December 2020, the CentOS Project [announced](https://blog.centos.org/2020/12/future-is-centos-stream/) it was shifting its focus from CentOS Linux to CentOS Stream, starting with the latest version, CentOS Linux 8. Support for CentOS Linux 8 will end at the end of 2021, and support for CentOS Stream 8 will continue until 2024. Prior to this announcement, CentOS Linux 8 was slated to be supported until 2029.

For more information about CentOS Stream, please refer to the [CentOS](/centos8) page.
