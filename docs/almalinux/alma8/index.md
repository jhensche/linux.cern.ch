# AlmaLinux 8 Documentation

* [Documentation pages](/almalinux/alma8/)
    * [Installation instructions](install.md)
    * [Locmap installation](locmap.md)

Please check [Documentation](/docs/) for additional documentation.