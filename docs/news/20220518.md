<div class="admonition news_date">2022-05-18</div>

## Red Hat Enterprise Linux 9 available

Red Hat Enterprise Linux (RHEL) 9 is now available for installation at CERN.

AIMS2 PXE boot targets:

```
   RHEL_9_0_X86_64
   RHEL_9_0_AARCH64
```

Installation path:

  [http://linuxsoft.cern.ch/cern/rhel/9/baseos/x86_64/](http://linuxsoft.cern.ch/cern/rhel/9/baseos/x86_64/)

For more information about RHEL, please refer to [this page](/rhel).
