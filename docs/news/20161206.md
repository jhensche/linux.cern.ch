<div class="admonition news_date">2016-12-06</div>

## CERN CentOS 7.3 testing available.

Dear Linux users.

We have a pleasure to announce that updated CERN CentOS Linux
(CC7) version:

            *******************************
            CC 7.3 is available for testing
            *******************************
                as of Tuesday 06.12.2016

and will become default CC7 production version

                    on Monday 09.01.2017.

For information about CC73, including installation
instructions please visit:

        <a href="http://cern.ch/linux/centos7/">http://cern.ch/linux/centos7/</a>


* As announced at ITUM [1] `lcm` configuration tool will be
replaced by `locmap`, for more information please visit:

        <a href="https://linux.web.cern.ch/linux/centos7/docs/locmap">https://linux.web.cern.ch/linux/centos7/docs/locmap</a>

* Updating CERN CentOS 7.2 to 7.3:

Due to CERN annual closure, all updates will be
released to production repositories only on 09.01.2017.

Until then CC7 system can be updated by running:

&#35; yum --enablerepo=cr \
    --enablerepo={updates,extras,cern}-testing \
    clean all

&#35; yum --enablerepo=cr \
    --enablerepo={updates,extras,cern}-testing \
    update



Thomas Oulevey for <a href="mailto:Linux.Support@cern.ch">Linux.Support@cern.ch</a>


