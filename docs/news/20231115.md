<div class="admonition news_date">2023-11-15</div>

## Red Hat Enterprise Linux 9.3 is available in production

We are pleased to announce that Red Hat Enterprise Linux 9.3 is available in production as of today.

The list of updated packages can be found [here](/updates/rhel9/prod/2023/11/monthly_updates/#2023-11-15).

The release notes will be published [here](/rhel/rhel9/release_notes/).
<div class="admonition news_date">2023-11-15</div>

## Red Hat Enterprise Linux 8.9 is available in testing

We are pleased to announce that Red Hat Enterprise Linux 8.9 is available in testing as of today.

The list of updated packages can be found [here](/updates/rhel8/test/2023/11/monthly_updates/#2023-11-15).

The release notes will be published [here](/rhel/rhel8/release_notes/).
