<div class="admonition news_date">2017-09-18</div>

## CERN CentOS 7.4 available.

Dear Linux users.

We have a pleasure to announce that updated CERN CentOS Linux (CC7) version:

            *********************************************
            CC 7.4 becomes default CC7 production version
            *********************************************

                      as of Monday 18.09.2017

For information about CC74, including installation
instructions please visit:

        <a href="http://cern.ch/linux/centos7/">http://cern.ch/linux/centos7/</a>

* Updating CERN CentOS 7.3 to 7.4:

All updates are released to production repositories as of 18.09.2017,
in order to update your system please run as root:

&#35; yum clean all
&#35; yum update

Jaroslaw Polok for <a href="mailto:Linux.Support@cern.ch">Linux.Support@cern.ch</a>


