<div class="admonition news_date">2018-05-24</div>

## CERN CentOS 7.5 available.

Dear Linux users.

We have a pleasure to announce that updated CERN Centos Linux (CC7) version:

                    *******************
                    CC 7.5 is available
                    *******************
                as of Thursday 24.05.2018

and is now default CC7 production version.

For information about CC75, including installation
instructions please visit:

        <a href="http://cern.ch/linux/centos7/">http://cern.ch/linux/centos7/</a>

* Installation:

    - via PXE network boot menu (<a href="http://cern.ch/linux/install/">http://cern.ch/linux/install/</a>)

    - non-PXE installation:

    <a href="http://linuxsoft.cern.ch/cern/centos/7.5/os/x86_64/images/">http://linuxsoft.cern.ch/cern/centos/7.5/os/x86_64/images/</a>

    and corresponding installation path is:

    <a href="http://linuxsoft.cern.ch/cern/centos/7.5/os/x86_64/images/">http://linuxsoft.cern.ch/cern/centos/7.5/os/x86_64/images/</a>

    - Automated PXE installations using AIMS2

    Boot targets are:

        CC7_X86_64
        CC75_X86_64

* Upgrading from previous CERN CentOS 7.X release:

    CC7 system can be updated by running:

    &#35; yum --enablerepo={updates,extras,cern,cernonly} clean all
    &#35; yum --enablerepo={updates,extras,cern,cernonly} update

--
Thomas Oulevey on behalf of the Linux Team


