# RHEL 9 - Install instructions

The following methods are currently available to install RHEL 9

* AIMS interactive - select the menu item `Install Red Hat Enterprise Linux 9 (RHEL9) x86_64` or `Install Red Hat Enterprise Linux 9 (RHEL9) aarch64`
* AIMS host registration - define your host to use the `RHEL9_X86_64` or `RHEL9_AARCH64`
* OpenStack VM - boot from the image `RHEL9 - x86_64` or `RHEL9 - aarch64`
* ai-bs puppet managed VM - use `ai-bs --rhel9`

For a detailed installation of a Linux Desktop, check the [step by step guide](/rhel/rhel9/stepbystep)

## Software repositories

Install software repository definitions on a RHEL9 previously-installation done without using our kickstart/image, by running as root:

```
# curl -o /etc/yum.repos.d/rhel9.repo https://linux.web.cern.ch/rhel/repofiles/rhel9.repo
```

You may also want to consider adding the 'CERN' repository

```
# dnf --repofrompath='tmpcern,https://linuxsoft.cern.ch/cern/rhel/$releasever/CERN/$basearch/' install redhat-release --nogpgcheck
```

Direct download: [rhel9.repo](/rhel/repofiles/rhel9.repo) and [rhel9-cern.repo](/rhel/repofiles/rhel9-cern.repo).
