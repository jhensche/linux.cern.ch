#### RHEL 9.2

* Installation target: **RHEL_9_2_X86_64**
* Installation path:   **http://linuxsoft.cern.ch/cern/rhel/9.2/baseos/x86_64/**
* Release notes:       **[RELEASE-NOTES-9.2-x86_64](Red_Hat_Enterprise_Linux-9-9.2_Release_Notes-en-US.pdf)**
