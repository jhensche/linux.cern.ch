#### RHEL 8.4

* Installation target: **RHEL_8_4_X86_64**
* Installation path:   **http://linuxsoft.cern.ch/enterprise/rhel/server/8/8.4/x86_64/**
* Release notes:       **[RELEASE-NOTES-8.4-x86_64](Red_Hat_Enterprise_Linux-8-8.4_Release_Notes-en-US.pdf)**
