<!--#include virtual="/linux/layout/header" -->
# Software Collections @ CERN
<h2>Software Collections for Scientific Linux CERN 6 / CERN CentOS 7</h2>

Software Collections provide a set of dynamic programming languages, database servers, and various related packages that are either more recent than their equivalent versions included in the base system or are available for this system for the first time.

<ul>
 <li><a href="/centos7/docs/softwarecollections">Software Collections for CentOS 7</a>.
 <li><a href="/scientific6/docs/softwarecollections">Software Collections for Scientific Linux CERN 6</a>.
</ul>


